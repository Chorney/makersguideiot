#include "ugui.h"
#include "ST7789dvr.h"
//#include "fonts.h"
#include <stdarg.h>

#define ST7789_TFTWIDTH  240
#define ST7789_TFTHEIGHT 320

#define DEBUG_BREAK 			__asm__("BKPT #0");

//extern unsigned char font_8x14;
extern __UG_FONT_DATA unsigned char font_8x14[256][14];

void set_drawing_window(uint16_t x_start, uint16_t y_start, uint16_t x_end,
                        uint16_t y_end)
{
      if (x_start > ST7789_TFTWIDTH || x_end > ST7789_TFTWIDTH ||
          y_start > ST7789_TFTHEIGHT || y_end > ST7789_TFTHEIGHT)
      {
            DEBUG_BREAK;
      }

      // Swap things if they are backwards
      if (x_start > x_end)
      {
            uint16_t tmp;
            tmp = x_end;
            x_end = x_start;
            x_start = tmp;
      }

      // Swap things if they are backwards
      if (y_start > y_end)
      {
            uint16_t tmp;
            tmp = y_end;
            y_end = y_start;
            y_start = tmp;
      }

      TFT_24_7789_Write_Command(ST7789_CASET); // Column addr set
      TFT_24_7789_Write_Data(x_start >> 8);
      TFT_24_7789_Write_Data(x_start);
      TFT_24_7789_Write_Data(x_end >> 8);
      TFT_24_7789_Write_Data(x_end );

      TFT_24_7789_Write_Command(ST7789_RASET); // Page (row) addr set
      TFT_24_7789_Write_Data(y_start >> 8);
      TFT_24_7789_Write_Data(y_start);
      TFT_24_7789_Write_Data(y_end >> 8);
      TFT_24_7789_Write_Data(y_end);
}

void draw_vertical_line(uint16_t x_start, uint16_t y_start, uint16_t y_end,
                        uint16_t color)
{
      set_drawing_window(x_start, y_start, x_start, y_end);
      uint16_t length = y_end - y_start + 1;
      TFT_24_7789_Write_Command(0x002C);
      for (int j = 0; j < length; j++)
      {
    	  TFT_24_7789_Write_Data( color >> 16);
    	  TFT_24_7789_Write_Data( color >> 8);
    	  TFT_24_7789_Write_Data( color );
      }
}

void draw_filled_rectangle(uint16_t x_start, uint16_t y_start, uint16_t x_end,
                           uint16_t y_end, uint16_t color)
{
      set_drawing_window(x_start, y_start, x_end, y_end);
      TFT_24_7789_Write_Command(0x002C);
      for (int i = 0; i < x_end; i++)
      {
            for (int j = 0; j < y_end; j++)
            {
            	TFT_24_7789_Write_Data( color >> 16);
            	TFT_24_7789_Write_Data( color >> 8);
            	TFT_24_7789_Write_Data( color );
            }
      }
}

void draw_horizontal_line(uint16_t x_start, uint16_t y_start, uint16_t length,
                          int16_t thickness, uint16_t color)
{
      uint16_t x_end = x_start + length;
      uint16_t y_end = y_start + thickness;
      draw_filled_rectangle(x_start, y_start, x_end, y_end, color);
}

void draw_vertical_line_thick(uint16_t x_start, uint16_t y_start, uint16_t length, int16_t thickness, uint16_t color)

{

	uint16_t x_end = x_start + thickness;

	uint16_t y_end = y_start + length;

	draw_filled_rectangle(x_start, y_start, x_end, y_end, color);

}

void draw_unfilled_rectangle(uint16_t x_start, uint16_t y_start, uint16_t width,
                        uint16_t height, uint16_t line_thickness, uint16_t color)
{
      // Draw left side
      draw_vertical_line_thick(x_start, y_start, height, line_thickness, color);

      // Draw bottom side
      draw_horizontal_line(x_start, y_start+height, width, -line_thickness, color);

      // Draw top
      draw_horizontal_line(x_start, y_start, width, line_thickness, color);

      // Draw right side
      draw_vertical_line_thick(x_start+width, y_start, height, -line_thickness, color);
}

#define FONT_WIDTH      8
#define FONT_HEIGHT 14

void draw_char(uint16_t x_start, uint16_t y_start, char single_char,
               uint16_t color)
{
      uint16_t pixel_buffer[FONT_HEIGHT * FONT_WIDTH];
      uint16_t index = (uint16_t) single_char;

      uint16_t row_offset = 0;
      for (int i=0; i<FONT_HEIGHT; i++)
      {
            uint8_t row_data = font_8x14[index][i];
            for (int j=0; j<FONT_WIDTH; j++)
            {
                  if (row_data & 0x1)
                  {
                        pixel_buffer[row_offset+j] = color;
                  }
                  else
                  {
                        // Set the pixel to black
                        // This assumes a black background!
                        pixel_buffer[row_offset+j] = ST7789_BLACK;
                  }

                  // Shift the row of the text char over by one for the next pixel
                  row_data = (row_data >> 1);
            }
            row_offset += FONT_WIDTH;
      }

      // Now, send the pixel_buffer to the screen
      set_drawing_window(x_start, y_start, x_start+FONT_WIDTH-1,
                         y_start+FONT_HEIGHT-1);
      TFT_24_7789_Write_Command(ST7789_WR);
      for (int i = 0; i < (FONT_HEIGHT * FONT_WIDTH); i++)
      {
    	  TFT_24_7789_Write_Data( pixel_buffer[i] >> 8);
    	  TFT_24_7789_Write_Data( pixel_buffer[i] );
      }
}

void tft_print(uint16_t x_start, uint16_t y_start, uint16_t color,
               const char* format, ...)
{
      // This stuff here deals with the elipsis (...) input
    char       msg[ST7789_TFTWIDTH / FONT_WIDTH];
    va_list    args;

    va_start(args, format);
    vsnprintf(msg, sizeof(msg), format, args);
    va_end(args);

    // Get a pointer to the message
    char * string = msg;
    while (*string != 0)
    {
      draw_char(x_start, y_start, *string++, color);
      x_start += FONT_WIDTH;
    }
}
