#include "em_device.h"
#include "em_chip.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "ST7789dvr.h"

//https://github.com/achimdoebler/UGUI - mappings for characters

extern uint32_t msTicks;
extern uint32_t tenusTicks;
#define DEBUG_BREAK 			__asm__("BKPT #0");
#define  USE_FONT_8X14
//#define ONE_SECOND_TIMER_COUNT 1000
#define ONE_MILI_SECOND_TIMER_COUNT 38400

//38.4 = 1 us , 10us count = 384

int TFT_24_7789_demo(void)
{

	//GPIO_ResetBits(GPIOC, IM0);		//16-bit mode
	TFT_24_7789_Write_Command(0x002C);				//Memory write

	int n,i;
//	GPIO_PinOutSet(PORT_BATCH1, ST7789_DC);
	for (n=0;n<3;n++){
//		memset(RGB16,0x0000,sizeof(RGB16));
		for (i=0;i<25600;i++)					//for each 24-bit pixel...
		{
//			f_read(&File1, &blue, 1, &blen);	//read the blue 8-bits
//			f_read(&File1, &green, 1, &blen);	//read the green 8-bits
//			f_read(&File1, &red, 1, &blen);		//read the red 8-bits

			uint8_t red = 0xff;
			uint8_t blue = 0xae;
			uint8_t green = 0xff;

	/* un-comment below for 8-bit interface */
			GPIO_PinOutSet(PORT_BATCH1, ST7789_DC);

			TFT_24_7789_Write(red);

			TFT_24_7789_Write(green);

			TFT_24_7789_Write(blue);

	/* END of 8-bit interface */

	/* un-comment below for 16-bit interface */
	//
	//		red=red>>3;						   	//shift down to 5-bits
	//		green=green>>2;						//shift down to 6-bits
	//		blue=blue>>3;						//shift down to 5-bits
	//		RGB16[i]= (RGB16[i] | red);			//put red 5-bits into int
	//		RGB16[i]= (RGB16[i] << 6);			//move red bits over, make room for green
	//		RGB16[i]= (RGB16[i] | green);		//put green 6-bits into int
	//		RGB16[i]= (RGB16[i] << 5);			//move red and green bits over, make room for blue
	//		RGB16[i]= (RGB16[i] | blue);		//put blue 5-bits into int
	//		GPIO_Write(GPIOB, RGB16[i]);
	//		GPIO_SetBits(GPIOC, RS);
	//		GPIO_ResetBits(GPIOC, nWR);
	//		GPIO_SetBits(GPIOC, nWR);
	//
	/* END of 16-bit interface */

		}
	}

	TFT_24_7789_Write_Command(0x0029);				//display ON
//	TFT_24_7789_Write_Command(0x0028);				//display OFF

	return 1;
}


int main(void)
{
  /* Chip errata */
  CHIP_Init();


  CMU_OscillatorEnable(cmuOsc_HFXO,true,true);
  CMU_ClockSelectSet(cmuClock_HF,cmuSelect_HFXO);



  CMU_ClockEnable(cmuClock_GPIO, true);

//  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000))
//        {
//              DEBUG_BREAK;
//        }

  //every 1 microsecond interupt handler executed


   CMU_ClockEnable(cmuClock_TIMER1, true);

	// Create a timerInit object, based on the API default
	TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
	timerInit.prescale = timerPrescale1;

	TIMER_IntEnable(TIMER1, TIMER_IF_OF);

	// Enable TIMER0 interrupt vector in NVIC
	NVIC_EnableIRQ(TIMER1_IRQn);

	// Set TIMER Top value
	TIMER_TopSet(TIMER1, ONE_MILI_SECOND_TIMER_COUNT);

	TIMER_Init(TIMER1, &timerInit);

	// Wait for the timer to get going
	while (TIMER1->CNT == 0);

	uint32_t blah;
	uint32_t blah2;

	blah = CMU_ClockFreqGet(cmuClock_TIMER1);
	blah2 = CMU_ClockFreqGet(cmuClock_CORE);


//	uint8_t n = 2;
//	delay(10000);
//	DEBUG_BREAK;

	//38,400,000

  TFT_24_7789_Init();
//
//  GPIO_PinOutClear(PORT_BATCH1, ST7789_CS);
//  GPIO_PinOutSet(PORT_BATCH1, ST7789_RD);
//  GPIO_PinOutSet(PORT_BATCH1,ST7789_DC);
//
//
////  GPIO_PinOutClear(PORT_BATCH3,ST7789_RES);
//  	GPIO_PinOutSet(PORT_BATCH3, ST7789_RES);
//  //GPIO_ResetBits(GPIOC, IM0);		//16-bit mode
//  	TFT_24_7789_Write(0x0);
//  	GPIO_PinOutSet(PORT_BATCH2, ST7789_DB8);


  TFT_24_7789_demo();

  draw_vertical_line(48, 0, 320, ST7789_RED);

//  TFT_24_7789_Write_Command(0x0029);


  //**// DRAW SOME IMAGES
  // Fill the screen with true black
//	draw_filled_rectangle(0,0, ILI9341_TFTWIDTH, ILI9341_TFTHEIGHT,
//						   ILI9341_BLACK);
//
//	//draw_vertical_line(48, 0, 320, 1, ST7789_RED);
//
//	draw_vertical_line(48, 0, 320, 10, ST7789_RED);
//
//	draw_vertical_line(88, 0, 120, 10, ST7789_RED);
//
	draw_horizontal_line(0, 160, 240, 30, ST7789_GREEN);
//
	draw_horizontal_line(40, 200, 160, 10, ST7789_YELLOW);
//
//	draw_unfilled_rectangle(10, 10, 220, 300, 30, ST7789_PURPLE);
//
//	draw_filled_rectangle(0,0, ILI9341_TFTWIDTH, ILI9341_TFTHEIGHT,
//	                            ST7789_BLACK);
//	tft_print(60,160, ST7789_RED, "EFM32 Rocks: %d!", 2);

  /* Infinite loop */
//  	DEBUG_BREAK;
  while (1) {
  }
}

void TIMER1_IRQHandler(void)
{
		msTicks++;
//      DEBUG_BREAK;
      TIMER_IntClear(TIMER1, TIMER_IF_OF);
}



void SysTick_Handler(void)
{
	msTicks++;
}
