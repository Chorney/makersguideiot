
#include "ST7789dvr.h"

uint32_t msTicks;
uint32_t tenusTicks;

void delay_original(uint32_t milliseconds)
{
	uint32_t start = msTicks;
	while ((start + milliseconds) > msTicks)
		;
}

void delay(uint32_t milliseconds)
{

	uint32_t start = msTicks;
	while ((start + milliseconds) > msTicks)
			;
}

/*******************************************************************************
* Function Name  : TFT_24_7789_Init
* Description    : Initializes LCD with built-in ST7789S controller.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void TFT_24_7789_Init(void)
{
//	int n;
//	GPIO_ResetBits(GPIOC, CS1);
//	GPIO_SetBits(GPIOC, nRD);

	GPIO_PinOutSet(PORT_BATCH3, ST7789_IM0);		//8-bit mode

	GPIO_PinOutSet(PORT_BATCH1, ST7789_RD);
	GPIO_PinOutSet(PORT_BATCH1, ST7789_WR);

//	GPIO_WriteBit(GPIOC, RES, Bit_RESET);
//	GPIO_PinOutClear(PORT_BATCH3,ST7789_RES);
	GPIO_PinModeSet(PORT_BATCH3,ST7789_RES, gpioModePushPull,0);

//	TFT_delay(100);
	delay(100);

//	GPIO_WriteBit(GPIOC, RES, Bit_SET);
	GPIO_PinOutSet(PORT_BATCH3, ST7789_RES);
//	TFT_delay(100);
	delay(100);

	TFT_24_7789_Write_Command(0x28);
	TFT_24_7789_Write_Command(0x0011);//exit SLEEP mode
//	TFT_delay(100);
//	delay(10);

	TFT_24_7789_Write_Command(0x0036);TFT_24_7789_Write_Data(0x0080);//MADCTL: memory data access control
	TFT_24_7789_Write_Command(0x003A);TFT_24_7789_Write_Data(0x0066);//COLMOD: Interface Pixel format *** I use 262K-colors in 18bit/pixel format when using 8-bit interface to allow 3-bytes per pixel
	//TFT_24_7789_Write_Command(0x003A);TFT_24_7789_Write_Data(0x0055);//COLMOD: Interface Pixel format  *** I use 65K-colors in 16bit/pixel (5-6-5) format when using 16-bit interface to allow 1-byte per pixel
	TFT_24_7789_Write_Command(0x00B2);TFT_24_7789_Write_Data(0x000C);TFT_24_7789_Write_Data(0x0C);TFT_24_7789_Write_Data(0x00);TFT_24_7789_Write_Data(0x33);TFT_24_7789_Write_Data(0x33);//PORCTRK: Porch setting
	TFT_24_7789_Write_Command(0x00B7);TFT_24_7789_Write_Data(0x0035);//GCTRL: Gate Control
	TFT_24_7789_Write_Command(0x00BB);TFT_24_7789_Write_Data(0x002B);//VCOMS: VCOM setting
	TFT_24_7789_Write_Command(0x00C0);TFT_24_7789_Write_Data(0x002C);//LCMCTRL: LCM Control
	TFT_24_7789_Write_Command(0x00C2);TFT_24_7789_Write_Data(0x0001);TFT_24_7789_Write_Data(0xFF);//VDVVRHEN: VDV and VRH Command Enable
	TFT_24_7789_Write_Command(0x00C3);TFT_24_7789_Write_Data(0x0011);//VRHS: VRH Set
	TFT_24_7789_Write_Command(0x00C4);TFT_24_7789_Write_Data(0x0020);//VDVS: VDV Set
	TFT_24_7789_Write_Command(0x00C6);TFT_24_7789_Write_Data(0x000F);//FRCTRL2: Frame Rate control in normal mode
	TFT_24_7789_Write_Command(0x00D0);TFT_24_7789_Write_Data(0x00A4);TFT_24_7789_Write_Data(0xA1);//PWCTRL1: Power Control 1
	TFT_24_7789_Write_Command(0x00E0);TFT_24_7789_Write_Data(0x00D0);
//	  TFT_24_7789_Write_Data(0x0000);
//	  TFT_24_7789_Write_Data(0x0005);
//	  TFT_24_7789_Write_Data(0x000E);
//	  TFT_24_7789_Write_Data(0x0015);
//	  TFT_24_7789_Write_Data(0x000D);
//	  TFT_24_7789_Write_Data(0x0037);
//	  TFT_24_7789_Write_Data(0x0043);
//	  TFT_24_7789_Write_Data(0x0047);
//	  TFT_24_7789_Write_Data(0x0009);
//	  TFT_24_7789_Write_Data(0x0015);
//	  TFT_24_7789_Write_Data(0x0012);
//	  TFT_24_7789_Write_Data(0x0016);
//	  TFT_24_7789_Write_Data(0x0019);//PVGAMCTRL: Positive Voltage Gamma control
//	  TFT_24_7789_Write_Command(0x00E1);TFT_24_7789_Write_Data(0x00D0);
//	  TFT_24_7789_Write_Data(0x0000);
//	  TFT_24_7789_Write_Data(0x0005);
//	  TFT_24_7789_Write_Data(0x000D);
//	  TFT_24_7789_Write_Data(0x000C);
//	  TFT_24_7789_Write_Data(0x0006);
//	  TFT_24_7789_Write_Data(0x002D);
//	  TFT_24_7789_Write_Data(0x0044);
//	  TFT_24_7789_Write_Data(0x0040);
//	  TFT_24_7789_Write_Data(0x000E);
//	  TFT_24_7789_Write_Data(0x001C);
//	  TFT_24_7789_Write_Data(0x0018);
//	  TFT_24_7789_Write_Data(0x0016);
//	  TFT_24_7789_Write_Data(0x0019);//NVGAMCTRL: Negative Voltage Gamma control
	  TFT_24_7789_Write_Command(0x002A);TFT_24_7789_Write_Data(0x0000);TFT_24_7789_Write_Data(0x0000);TFT_24_7789_Write_Data(0x0000);TFT_24_7789_Write_Data(0x00EF);//X address set
	  TFT_24_7789_Write_Command(0x002B);TFT_24_7789_Write_Data(0x0000);TFT_24_7789_Write_Data(0x0000);TFT_24_7789_Write_Data(0x0001);TFT_24_7789_Write_Data(0x003F);//Y address set

	  TFT_24_7789_Write_Command(0x29);
//TFT_delay(10);
	  delay(10);
}


void TFT_24_7789_Write(unsigned int data1)
{
//	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB8, gpioModePushPull, 1);
//	GPIO_PinOutSet(PORT_BATCH2, ST7789_DB8);
	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB8, gpioModePushPull, data1 & 0b1);
	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB9, gpioModePushPull, data1 & 0b10);
	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB10, gpioModePushPull, data1 & 0b100);
	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB11, gpioModePushPull, data1 & 0b1000);
	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB12, gpioModePushPull, data1 & 0b10000);
	GPIO_PinModeSet(PORT_BATCH2, ST7789_DB13, gpioModePushPull, data1 & 0b100000);
	GPIO_PinModeSet(PORT_BATCH3, ST7789_DB14, gpioModePushPull, data1 & 0b1000000);
	GPIO_PinModeSet(PORT_BATCH3, ST7789_DB15, gpioModePushPull, data1 & 0b10000000);

	GPIO_PinModeSet(PORT_BATCH1,ST7789_WR, gpioModePushPull,0);

//	GPIO_PinOutClear(PORT_BATCH1,ST7789_WR);
	int n;
	for (n=1; n<=5; n++);
//	delay(1);
	GPIO_PinOutSet(PORT_BATCH1,ST7789_WR);
//	GPIO_PinModeSet(PORT_BATCH1,ST7789_WR, gpioModePushPull,1);
//	delay(1);
//	int n;
	for (n=1; n<=5; n++);
}

/*******************************************************************************
* Function Name  : TFT_24_7789_Write_Command
* Description    : writes a 1 byte command to 2.4" TFT.
* Input          : command = one byte command (register address)
* Output         : None
* Return         : None
*******************************************************************************/
void TFT_24_7789_Write_Command(unsigned int command)
{
//	GPIO_PinOutClear(PORT_BATCH1,ST7789_DC);
	GPIO_PinModeSet(PORT_BATCH1,ST7789_DC, gpioModePushPull,0);
	TFT_24_7789_Write(command);
}

/*******************************************************************************
* Function Name  : TFT_24_7789_Write_Data
* Description    : writes 1 byte of data to 2.4" TFT.
* Input          : data1 = one byte of display data or command parameter
* Output         : None
* Return         : None
*******************************************************************************/
void TFT_24_7789_Write_Data(unsigned int data1)
{
	GPIO_PinOutSet(PORT_BATCH1,ST7789_DC);
	TFT_24_7789_Write(data1);
}



