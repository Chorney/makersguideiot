/*
 * ST7789dvr.h
 *
 *  Created on: Jan 22, 2018
 *      Author: starrover
 */

#include "em_gpio.h"

#ifndef SRC_ST7789DVR_H_
#define SRC_ST7789DVR_H_

#define PORT_BATCH1 gpioPortA
#define ST7789_CS 0  // Pin 10   - efr pin 9 - or tied to ground
#define ST7789_DC 1  // Pin 11  - efr pin 11
#define ST7789_WR 2 // Pin 12  - efr pin 0
#define ST7789_RD 3 // Pin 13   - efr pin 2

//use DB8-DB15 Pin 22-29

#define PORT_BATCH2 gpioPortD
#define ST7789_DB8 10 // 22 - efr pin 4
#define ST7789_DB9 11 // 23 - efr pin 6
#define ST7789_DB10 12 // 24 - efr pin 8
#define ST7789_DB11 13 // 25 - efr pin 31
#define ST7789_DB12 14 // 26 - efr pin 33
#define ST7789_DB13 15 // 27 - efr pin 35

#define PORT_BATCH3 gpioPortC
#define ST7789_DB14 6// 28 - efr pin 1
#define ST7789_DB15 7 // 29 - efr pin 3

#define ST7789_RES 8 //30 - efr pin 5
#define ST7789_IM0 9  //31 - efr pin 7

// Color definitions

#define ST7789_BLACK       0x0000      /*   0,   0,   0 */
#define ST7789_NAVY        0x000F      /*   0,   0, 128 */
#define ST7789_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define ST7789_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define ST7789_MAROON      0x7800      /* 128,   0,   0 */
#define ST7789_PURPLE      0x780F      /* 128,   0, 128 */
#define ST7789_OLIVE       0x7BE0      /* 128, 128,   0 */
#define ST7789_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define ST7789_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define ST7789_BLUE        0x001F      /*   0,   0, 255 */
#define ST7789_GREEN       0x07E0      /*   0, 255,   0 */
#define ST7789_CYAN        0x07FF      /*   0, 255, 255 */
#define ST7789_RED         0xF800      /* 255,   0,   0 */
#define ST7789_MAGENTA     0xF81F      /* 255,   0, 255 */
#define ST7789_YELLOW      0xFFE0      /* 255, 255,   0 */
#define ST7789_WHITE       0xFFFF      /* 255, 255, 255 */
#define ST7789_ORANGE      0xFD20      /* 255, 165,   0 */
#define ST7789_GREENYELLOW 0xAFE5      /* 173, 255,  47 */
#define ST7789_PINK        0xF81F

#define ST7789_CASET 0x2A
#define ST7789_RASET 0x2B

#endif /* SRC_ST7789DVR_H_ */
