import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import serial

port = '/dev/ttyUSB0'
baudrate = 115200
serial_port = serial.Serial(port,baudrate=baudrate, timeout = 0)
serial_port.flushInput()

pg.setConfigOption('background','w')
pg.setConfigOption('foreground','k')

win = pg.GraphicsWindow()
win.setWindowTitle('Cpacitive sense plot')

plot = win.addPlot()
data = [0 for x in range(300)]

curve_pen = pg.mkPen(width=1,color='b')
curve = plot.plot(data,pen=curve_pen,symbolBrush='b',symbolPen='b',symbolSize=5)

value_string = ""

def update():
    #Tell Python that these variables are to be tied to the outside global scope
    global data, curve, value_string

    #only do something when the serial port has data
    while serial_port.inWaiting():

        #read a single character
        sample = serial_port.read(1)

        #add this sample to the value_string if it is not whitespace
        if sample.strip():
            value_string += sample
        else:

            if len(value_string) > 0:

                #convert the ASCII base 16 string to an integer
                value = int(value_string,16)

                #shift the whole datalist by one sample to the left
                data[:-1] = data[1:]

                #add the new element to the end of the list
                data[-1] = value

                #set the curve with the data
                curve.setData(data)

                #Clear the string for the next value
                value_string = ""


    return

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)

#start qt event loop unless running in interactive mode or using pyside

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive !=1) or not hasattr(QtCore,'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
