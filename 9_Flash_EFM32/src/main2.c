/*
 * main2.c
 *
 *  Created on: Feb 13, 2018
 *      Author: starrover
 */

#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "utilities.h"

#define JEDEC_ID_CMD	0x9F

#include "spidrv.h"

SPIDRV_HandleData_t handleData;
SPIDRV_Handle_t handle = &handleData;

void spidrv_setup()
{
      // Set up the necessary peripheral clocks
      CMU_ClockEnable(cmuClock_GPIO, true);

      GPIO_DriveModeSet(gpioPortD, gpioDriveModeLow);

      // Enable the GPIO pins for the misc signals, leave pulled high
      GPIO_PinModeSet(gpioPortD, 4, gpioModePushPullDrive, 1);          // WP#
      GPIO_PinModeSet(gpioPortD, 5, gpioModePushPullDrive, 1);          // HOLD#

      // Initialize and enable the SPIDRV
      SPIDRV_Init_t initData = SPIDRV_MASTER_USART1;
      initData.clockMode = spidrvClockMode3;

      // Initialize a SPI driver instance
      SPIDRV_Init( handle, &initData );
}

int main(void)
{
      CHIP_Init();

      spidrv_setup();

      setup_utilities();

      delay(100);

      uint8_t result[4];
      uint8_t tx_data[4];

      tx_data[0] = JEDEC_ID_CMD;

//      while (1) {
      SPIDRV_MTransferB( handle, &tx_data, &result, 4);
//      }
      // Check the result for what is expected from the Spansion spec
      if (result[1] != 1 || result[2] != 0x40 || result[3] != 0x13)
      {
            DEBUG_BREAK
      }

      while (1)
            ;
}
