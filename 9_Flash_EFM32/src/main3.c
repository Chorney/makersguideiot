/*
 * main3.c
 *
 *  Created on: Feb 13, 2018
 *      Author: starrover
 *      This code works with the external flash drive AT45DB021E from adesto
 */
#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "utilities.h"

#define JEDEC_ID_CMD	0x9F

#include "spidrv.h"

SPIDRV_HandleData_t handleData;
SPIDRV_Handle_t handle = &handleData;

#define STATUS                0xD7
#define WRITE_BUFFER          0x84
#define BUFFER_TO_MEMORY	  0x83
#define READ_MEMORY           0xD2

#define CHIP_ERASE_BYTE1      0xC7
#define CHIP_ERASE_BYTE2 	  0x94
#define CHIP_ERASE_BYTE3 	  0x80
#define CHIP_ERASE_BYTE4	  0x9A

#define CONT_ARRAY_READ		  0xE8

#define PAGE_SIZE             256

//status register returns two bytes
typedef struct StatusBytes
{
  uint8_t            statusbyte1;          /// byte1 (MSB)
  uint8_t            statusbyte2;      /// byte2
} StatusByte;

// Writes a single byte
//void config_write(uint8_t command)
//{
//      const int size = 1;
//      uint8_t result[size];
//      uint8_t tx_data[size];
//
//      tx_data[0] = command;
//
//      SPIDRV_MTransferB( handle, &tx_data, &result, size);
//}

// Reads the status
uint8_t read_status(StatusByte *status)
{
      const int size = 3;
      uint8_t result[size];
      uint8_t tx_data[size];

      tx_data[0] = STATUS;

      SPIDRV_MTransferB( handle, &tx_data, &result, size);

      status->statusbyte1 = result[1];
      status->statusbyte2 = result[2];

      return 0;
}

void block_status_ready()
{
	StatusByte status_result;
	uint8_t byte1;
	uint8_t byte2;

	//8th most significant bit, if ==0, then operation is still busy
	read_status(&status_result);
	byte1 = status_result.statusbyte1;
	byte2 = status_result.statusbyte2;
	while (byte1>>7==0)
	{
		read_status(&status_result);
		byte1 = status_result.statusbyte1;
		byte2 = status_result.statusbyte2;
	}
}


void write_buffer_address(uint8_t address, uint8_t value)
{
	block_status_ready();

	const int size = PAGE_SIZE+4;
    uint8_t result[size];
    uint8_t tx_data[size];

    tx_data[0] = WRITE_BUFFER;
    tx_data[1] = 0;
    tx_data[2] = 0;
    tx_data[3] = address;
    tx_data[4] = value;

    SPIDRV_MTransferB( handle, &tx_data, &result,5);

}

// pagenumber has to be between 0 and 1024
void buffer_to_main_mem(uint16_t pagenumber)
{
	block_status_ready();

	const int size = 4;
	uint8_t result[size];
	uint8_t tx_data[size];

	tx_data[0] = BUFFER_TO_MEMORY;
	tx_data[1] = (pagenumber >> 8) & 0x3;
	tx_data[2] = pagenumber & 0xff;
	tx_data[3] = 0;

	SPIDRV_MTransferB( handle, &tx_data, &result,size);
}

void chip_erase()
{
	  block_status_ready();

      const int size = 4;
	  uint8_t result[size];
	  uint8_t tx_data[size];

	  tx_data[0] = CHIP_ERASE_BYTE1;
	  tx_data[1] = CHIP_ERASE_BYTE2;
	  tx_data[2] = CHIP_ERASE_BYTE3;
	  tx_data[3] = CHIP_ERASE_BYTE4;

	  SPIDRV_MTransferB( handle, &tx_data, &result,size);
}

uint8_t read_memory_address(uint16_t pagenumber, uint8_t address)
{
	block_status_ready();

    const int size = 9;
    uint8_t result[size];
    uint8_t tx_data[size];

    tx_data[0] = READ_MEMORY;
    tx_data[1] = (pagenumber >> 8) & 0x3;
    tx_data[2] = pagenumber & 0xff;
    tx_data[3] = address;
    //four dummy bytes
    tx_data[4] = 0;
	tx_data[5] = 0;
	tx_data[6] = 0;
	tx_data[7] = 0;

	SPIDRV_MTransferB( handle, &tx_data, &result,size);

	return result[8];
}

//writes memory from address forward.
void write_memory(uint32_t address, uint8_t data_buffer[], uint32_t num_of_bytes)
{

	uint32_t page_number;
	uint8_t page_address;
	page_number = address >> 8;
	page_address = address & 0xff;

	if (page_number > 1024) {
		DEBUG_BREAK
	}

	if (address + num_of_bytes >  262144) DEBUG_BREAK; //  1024 (number of pages)* 256 (num of bytes per page)


	const int size = PAGE_SIZE+4;
	uint8_t result[size];
	uint8_t tx_data[size];

	tx_data[0] = WRITE_BUFFER;
	tx_data[1] = 0;
	tx_data[2] = 0;
	tx_data[3] = page_address;

	int i=0;
	uint8_t local_page_address = page_address;
	uint32_t page_number_counter = page_number;
	while (i < num_of_bytes)
	{
		int counter = 0;
		while (local_page_address + counter< PAGE_SIZE)
		{
			if (i == num_of_bytes) {
				break;
			}
			tx_data[counter+4] = data_buffer[i];
			counter++;
			i++;
		}

		block_status_ready();
		SPIDRV_MTransferB( handle, &tx_data, &result,counter+4);
		block_status_ready();
		buffer_to_main_mem(page_number_counter);

		local_page_address = 0;
		page_number_counter++;
	}
}


void read_memory(uint32_t address, uint8_t *result, uint32_t num_of_bytes)
{
	uint32_t page_number;
	uint8_t page_address;
	page_number = address >> 8;
	page_address = address & 0xff;


	const int size = num_of_bytes+8;
	uint8_t tx_data[size];
	uint8_t rcv_data[size];

	tx_data[0] = CONT_ARRAY_READ;
	// 3 address bytes
	tx_data[1] = (page_number >> 8) & 0x3;
	tx_data[2] = page_number & 0xff;
	tx_data[3] = page_address;
	//one dummy byte
	tx_data[4] = 0;
	tx_data[5] = 0;
	tx_data[6] = 0;
	tx_data[7] = 0;

	block_status_ready();
	SPIDRV_MTransferB( handle, &tx_data, &rcv_data,size);

	int i;
	for (i=0;i<num_of_bytes;i++) {
		result[i] = rcv_data[i+8];
	}
}

void spidrv_setup()
{
      // Set up the necessary peripheral clocks
      CMU_ClockEnable(cmuClock_GPIO, true);

      GPIO_DriveModeSet(gpioPortD, gpioDriveModeLow);

      // Enable the GPIO pins for the misc signals, leave pulled high
      GPIO_PinModeSet(gpioPortD, 4, gpioModePushPullDrive, 1);          // WP#
      GPIO_PinModeSet(gpioPortD, 5, gpioModePushPullDrive, 1);          // HOLD#

      // Initialize and enable the SPIDRV
      SPIDRV_Init_t initData = SPIDRV_MASTER_USART1;
      initData.clockMode = spidrvClockMode3;

      // Initialize a SPI driver instance
      SPIDRV_Init( handle, &initData );
}

int main(void)
{
      CHIP_Init();

      spidrv_setup();

      setup_utilities();

      delay(100);

      uint8_t result[PAGE_SIZE];
      uint8_t tx_data[PAGE_SIZE];

      tx_data[0] = JEDEC_ID_CMD;

      SPIDRV_MTransferB( handle, &tx_data, &result, 4);

      // Check the result for what is expected from the Spansion spec
      if (result[1] != 1 || result[2] != 0x40 || result[3] != 0x13)
      {
//            DEBUG_BREAK
      }

//	  chip_erase();
      uint8_t valuebefore;
      uint8_t valueafter;
      valuebefore = read_memory_address(0,18); //pagenumber,address
//      DEBUG_BREAK;
//      write_buffer_address(0,22);
//      buffer_to_main_mem(0);
//      valueafter = read_memory_address(0,0);

//      DEBUG_BREAK

//	  uint8_t input_array[10];
//
//      input_array[0] = 15;
//      input_array[1] = 15;
//      input_array[2] = 15;
//      input_array[3] = 15;
//      input_array[4] = 15;
//      input_array[5] = 15;
//      input_array[6] = 11;
//      input_array[7] = 11;
//      input_array[8] = 13;
//      input_array[9] = 13;

      uint8_t input_array[20] = {12,12,12,12,12,10,10,10,15,15,15,15,12,11,9,1,1,1,1,1};

//      DEBUG_BREAK;

      uint8_t read_array_before[41];
      uint8_t read_array_after[20];

//      read_memory(0,&read_array_before,41);
      write_memory(0,input_array,20);
      read_memory(0,&read_array_after,20);


      DEBUG_BREAK;



      // Never enter here except with debugger and �Move to Line�
//      if (result[1] == 0x2)
//      {
//            for (int i=0; i < PAGE_SIZE; i++) tx_data[i] = i;
//
//            chip_erase();
//            read_memory(0, result, PAGE_SIZE);
//            write_memory(0, tx_data, PAGE_SIZE);
//            read_memory(0, result, PAGE_SIZE);
//      }
//
//      read_memory(0, result, PAGE_SIZE);

      while (1)
            ;
}
