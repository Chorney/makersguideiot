/*
 * main2.c
 *
 *  Created on: Feb 14, 2018
 *      Author: starrover
 */


#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_gpio.h"
#include "dac_helpers.h"
#include "utilities.h"

int main(void)
{
      CHIP_Init();

      // Need to boost the clock to get above 7kHz
      CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);

      CMU_ClockEnable(cmuClock_GPIO, true);
      CMU_ClockEnable(cmuClock_DMA, true);

      // Enable GPIO output for Timer3, Compare Channel 2 on PE2
      GPIO_PinModeSet(gpioPortE, 2, gpioModePushPull, 0);

      // Show the sample rate on PE1 for debug
      GPIO_PinModeSet(gpioPortE, 1, gpioModePushPull, 0);

      // Get the systick running for delay() functions
      if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000))
      {
            DEBUG_BREAK;
      }

      play_sound(TEST_SOUND4);
      DAC_setup();
      DAC_TIMER_setup();

      while (1) ;
}
