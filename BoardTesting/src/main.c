#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"




int main(void)
{
  /* Chip errata */
  CHIP_Init();

  CMU_ClockEnable(cmuClock_GPIO, true);


  //A Ports
  GPIO_PinModeSet(gpioPortA, 0, gpioModePushPull, 1); //P9
  GPIO_PinModeSet(gpioPortA, 1, gpioModePushPull, 1); //P11
  GPIO_PinModeSet(gpioPortA, 2, gpioModePushPull, 1); //P0
  GPIO_PinModeSet(gpioPortA, 3, gpioModePushPull, 1); //P2
  GPIO_PinModeSet(gpioPortA, 4, gpioModePushPull, 1); //14
  GPIO_PinModeSet(gpioPortA, 5, gpioModePushPull, 1); //16

  //B Ports
  GPIO_PinModeSet(gpioPortB, 11, gpioModePushPull, 1); //P18
  GPIO_PinModeSet(gpioPortB, 12, gpioModePushPull, 1); //P20
  GPIO_PinModeSet(gpioPortB, 13, gpioModePushPull, 1); //P22

  //C Ports
  GPIO_PinModeSet(gpioPortC, 10, gpioModePushPull, 1); //P12
  GPIO_PinModeSet(gpioPortC, 11, gpioModePushPull, 1); //P13
  GPIO_PinModeSet(gpioPortC, 6, gpioModePushPull, 1); //P1
  GPIO_PinModeSet(gpioPortC, 7, gpioModePushPull, 1); //P3
  GPIO_PinModeSet(gpioPortC, 8, gpioModePushPull, 1); //P5
  GPIO_PinModeSet(gpioPortC, 9, gpioModePushPull, 1); //P7

  //D Ports
  GPIO_PinModeSet(gpioPortD, 10, gpioModePushPull, 1); //P4
  GPIO_PinModeSet(gpioPortD, 11, gpioModePushPull, 1); //P6
  GPIO_PinModeSet(gpioPortD, 12, gpioModePushPull, 1); //P8
  GPIO_PinModeSet(gpioPortD, 13, gpioModePushPull, 1); //P31
  GPIO_PinModeSet(gpioPortD, 14, gpioModePushPull, 1); //P33
  GPIO_PinModeSet(gpioPortD, 15, gpioModePushPull, 1); //P35

  //F Ports
  GPIO_PinModeSet(gpioPortF, 0, gpioModePushPull, 1); //P24
  GPIO_PinModeSet(gpioPortF, 1, gpioModePushPull, 1); //P26
  GPIO_PinModeSet(gpioPortF, 2, gpioModePushPull, 1); //P28
  GPIO_PinModeSet(gpioPortF, 3, gpioModePushPull, 1); //P10
  GPIO_PinModeSet(gpioPortF, 4, gpioModePushPull, 1); //P30
  GPIO_PinModeSet(gpioPortF, 5, gpioModePushPull, 1); //P32
  GPIO_PinModeSet(gpioPortF, 6, gpioModePushPull, 1); //P34




  /* Infinite loop */
  while (1) {
  }
}
