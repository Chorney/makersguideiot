


//*
// * File:   pca9685.h
// * Author: DevXP
// *
// * GNU GENERAL PUBLIC LICENSE!
// * Created on April 2, 2012, 8:28 AM
// */
//
//#use I2C(master, slow, i2c1, force_hw)
#include "pca9685_reg.h"
#include <stdio.h>
#include <stdarg.h>


uint8_t write_data[10];

long PWMData[16]= {
    //MSB        LSB
    0b000001000000,    // Channel 0
    0b111111111111,    // Channel 1
    0b000011000000,    // Channel 2
    0b000001100000,    // Channel 3
    0b000001011010,    // Channel 4
    0b000000000000,    // Channel 5
    0b000000000000,    // Channel 6
    0b000000000000,    // Channel 7
    0b000000000000,    // Channel 8
    0b000000000000,    // Channel 9
    0b000000000000,    // Channel 10
    0b000000000000,    // Channel 11
    0b000000000000,    // Channel 12
    0b000000000000,    // Channel 13
    0b000000000000,    // Channel 14
    0b000000000000     // Channel 15
};

void pca9685_init()
{
/********************************************/
/* Init code for the PCA9685                */
/* Input:[address of the pc9685 to init]    */
/* Output:[void]                            */
/********************************************/
//    i2c_start();                // Start
//    i2c_write(address);         // Slave address
//    i2c_write(MODE1);           // Mode 1 address
//    i2c_write(0b00110001);      // Setting mode to sleep so we can change teh default PWM frequency
//    i2c_stop();                 // Stop

    i2c_write_register(MODE1, 0b00110001);


//    i2c_start();                // Start
//    i2c_write(address);         // Slave address
//    i2c_write(0xfe);            // PWM frequency PRE_SCALE address
//    i2c_write(0x04);            // osc_clk/(4096*update_rate) // 25000000/(4096*1500)= 4.069 ~4
//    i2c_stop();                 // Stop

    i2c_write_register(0xfe, 0x04);

//    i2c_start();                // Start
//    i2c_write(address);         // Slave address
//    i2c_write(MODE1);           // Mode 1 register address
//    i2c_write(0xa1);            // Set to our prefered mode[ Reset, INT_CLK, Auto-Increment, Normal Mode]
//    i2c_stop();                 // Stop

    i2c_write_register(MODE1, 0xa1);

//    i2c_start();                // Start
//    i2c_write(address);         // Slave Address
//    i2c_write(MODE2);           // Mode2 register address
//    i2c_write(0b00000100);      // Set to our prefered mode[Output logic state not inverted, Outputs change on STOP,
//    i2c_stop();                 // totem pole structure, When OE = 1 (output drivers not enabled), LEDn = 0]

    i2c_write_register(MODE2, 0b00000100);

}

void pca9685_send_all()
{
/********************************************/
/* Update all PWM register of PCA9685       */
/* Input:[address of the pc9685 to update]  */
/* Output:[void]                            */
/********************************************/
    int i = 0;                  // temp regiter for LEDCOUNT
//    int pwm;                    // temp register for PWM
    for(i=0; i<=LEDCOUNT; i++)  // cycle thru all 16 LED
    {
//        i2c_start();            // Start
//        i2c_write(address);     // write to selected pca9685
//        i2c_write(LED0 + 4 * i);// start from LED0 address, each pwm constructed from
//        i2c_write(0x00);        // 4 12bit register, LED_ON_L
//        i2c_write(0x00);        // LED_ON_H
//        pwm = PWMData[i];       // update selected LED data in the array
//        i2c_write(pwm);         // LED_OFF_L
//        pwm = PWMData[i]>>8;    // updtae selected LED data in the array
//        i2c_write(pwm);         // LED_OFF_H


        uint8_t cmd = LED0 + 4 * i;
        write_data[0] = 0x00;
		write_data[1] = 0x00;
		write_data[2] = PWMData[i];
		write_data[3] = PWMData[i]>>8;

		i2c_write_register_multd(cmd,write_data,4);

//        i2c_stop();             // Stop
    }
}

void pca9685_send(long value, int led)
{
/********************************************/
/* pca9685_send(long value, int led)        */
/* Send the 12 bit PWM data to the register */
/* Input[ 0to4095 pwm, 0to15LED channel]    */
/* Output[void]                             */
/********************************************/
//    int pwm;                    // temp variable for PWM
    if(value > 4095)            // if larger than 4095 than full on
            value = 4095;       // cant be larger than 4095
    if(led > 15)                // if LED larger than 15 than on other chip
            led = 15;           //***** need to implement to selecet next pcs9685
//    i2c_start();                // Start
//    i2c_write(address);         // Address of selected pca9685
//    i2c_write(LED0 + 4 * led);  // select slected LED address
//    i2c_write(0x00);            // LED_ON_L
//    i2c_write(0x00);            // LED_ON_H
//    pwm = value;                // PWM value lo byte
//    i2c_write(pwm);             // LED_OFF_L
//    pwm = value>>8;             // pwm 16 bit long, now shift upper 8 to lower 8
//    i2c_write(pwm);             // LED_OFF_H

    uint8_t cmd = LED0 + 4 * led;
	write_data[0] = 0x00;
	write_data[1] = 0x00;
	write_data[2] = value;
	write_data[3] = value>>8;

	i2c_write_register_multd(cmd,write_data,4);

//    i2c_stop();                 // STop
}


//void pca9685_brightness(int address, int percent, int led)
//{
///********************************************/
///* Calculate the register values for a      */
///* given percentage and dupdate pca9685     */
///* Input:[address of the chip where LED is_ */
///* percent of PWM on period 0%to100%      _ */
///* LED to set brightness 0to15]             */
///* Output:[void]                            */
///********************************************/
//    long x;                             // temp variable
//    float off;                          // temp variable
//    const float onePercent = 40.96;
//    if (percent < 1) {                  // if % less than 1 than LED OFF
//    PWMData[led] = PCA9685_LED_OFF>>8;  // update data in array in case we use update all LED next
//    pca9685_send(address,0,led);        // update selected LED
//    return;                             // return from function
//    }
//    if (percent >= 100) {               // if % greather than 100 than LED ON
//    PWMData[led] = PCA9685_LED_ON>>8;   // update data in array
//    pca9685_send(address,4095,led);     // update selected LED
//    return;                             // return from function
//    }
//    off = onePercent * percent;         // different aproach with float need to check if code faster than int32 way ?
////    off = (int32)4096 * percent;        // calculate percent (max*percent)/100
////    off = off / 100;                    // ex (4096*50%)=204800/100=2048
////    x = make16(off>>8,off);             // make 16 of 32 ?! why.. dont care at this time
//    PWMData[led] = off;                 // update datat array in case we update all LED next
//    pca9685_send(address,off,led);      // send it to pca9685
//}
void PCA9685AllLedOff()
{
//    i2c_start();                        // Start
//    i2c_write(address);                 // select pca9685
//    i2c_write(0xfc);         // AllLED Off regiter
//    i2c_write(0b00000000);              // data
//    i2c_write(0b00010000);              // data
//    i2c_stop();                         // Stop

    write_data[0] = 0b00000000;
    write_data[1] = 0b00010000;

    i2c_write_register_multd(0xfc,write_data,2);
}
