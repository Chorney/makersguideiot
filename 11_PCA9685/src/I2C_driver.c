/*
 * I2C_driver.c
 *
 *  Created on: Aug 7, 2017
 *      Author: starrover
 */


#include "I2C_driver.h"
#include "em_i2c.h"
//#include "pca9685_reg.h"


// Globals for persistent storage
uint8_t cmd_array[CMD_ARRAY_SIZE];
uint8_t data_array[DATA_ARRAY_SIZE];


void i2c_transfer(uint16_t device_addr, uint8_t cmd_array[], uint8_t data_array[], uint16_t cmd_len, uint16_t data_len, uint8_t flag)
{
      // Transfer structure
      I2C_TransferSeq_TypeDef i2cTransfer;

      // Initialize I2C transfer
      I2C_TransferReturn_TypeDef result;
      i2cTransfer.addr          = device_addr;
      i2cTransfer.flags         = flag;
      i2cTransfer.buf[0].data   = cmd_array;
      i2cTransfer.buf[0].len    = cmd_len;

      // Note that WRITE_WRITE this is tx2 data
      i2cTransfer.buf[1].data   = data_array;
      i2cTransfer.buf[1].len    = data_len;

      // Set up the transfer
      result = I2C_TransferInit(I2C0, &i2cTransfer);

      // Do it until the transfer is done
      while (result != i2cTransferDone)
      {
            if (result != i2cTransferInProgress)
            {
//                  DEBUG_BREAK;
            }
            result = I2C_Transfer(I2C0);
      }
}

// Read a config register on an I2C device
// Tailored for the ADX345 device only i.e. 1 byte of TX
uint8_t i2c_read_register(uint8_t reg_offset)
{
      cmd_array[0] = reg_offset;
      i2c_transfer(PCA9685_ADDRESS, cmd_array, data_array, 1, 1, I2C_FLAG_WRITE_READ);
      return data_array[0];
}

// Write a config register on an I2C device
// Tailored for the ADX345 device only i.e. 1 byte of TX
void i2c_write_register(uint8_t reg_offset, uint8_t write_data)
{
      cmd_array[0] = reg_offset;
      data_array[0] = write_data;
      i2c_transfer(PCA9685_ADDRESS, cmd_array, data_array, 1, 1, I2C_FLAG_WRITE_WRITE);
}

void i2c_write_register_multd(uint8_t reg_offset, uint8_t write_data[], uint8_t data_length)
{
	int i;
	cmd_array[0] = reg_offset;
	for(i=0; i<data_length; i++) {
		data_array[i] = write_data[i];
	}
	i2c_transfer(PCA9685_ADDRESS, cmd_array, data_array, 1, data_length, I2C_FLAG_WRITE_WRITE);
}

void i2c_init_d(void)
{
	I2C_Init_TypeDef i2cInit = I2C_INIT_DEFAULT;
	I2C_Init(I2C0, &i2cInit);
}
