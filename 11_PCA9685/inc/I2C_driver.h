/*
 * I2C_driver.h
 *
 *  Created on: Aug 7, 2017
 *      Author: starrover
 */

#ifndef INC_I2C_DRIVER_H_
#define INC_I2C_DRIVER_H_

#include <stdio.h>
#include <stdarg.h>

#define PCA9685_ADDRESS       0x80
#define DEVICE_ID             0x00
#define CMD_ARRAY_SIZE        1
#define DATA_ARRAY_SIZE       8

#define PRESACLE 	0xfe

#define DEBUG_BREAK		__asm__("BKPT #0");

void i2c_transfer(uint16_t device_addr, uint8_t cmd_array[], uint8_t data_array[], uint16_t cmd_len, uint16_t data_len, uint8_t flag);
uint8_t i2c_read_register(uint8_t reg_offset);
void i2c_write_register(uint8_t reg_offset, uint8_t write_data);
void i2c_init_d(void);

#endif /* INC_I2C_DRIVER_H_ */
