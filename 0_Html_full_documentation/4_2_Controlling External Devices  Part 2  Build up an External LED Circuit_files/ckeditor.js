var CKEDITOR_BASEPATH="/etc/clientlibs/ckeditor/";
var contextPath=CQ.shared.HTTP.getContextPath();
if(contextPath!==null&&contextPath.length>0){CKEDITOR_BASEPATH=contextPath+CKEDITOR_BASEPATH
}(function(){if(window.CKEDITOR&&window.CKEDITOR.dom){return
}window.CKEDITOR||(window.CKEDITOR=function(){var f=/(^|.*[\\\/])ckeditor\.js(?:\?.*|;.*)?$/i,g={timestamp:"F969",version:"4.5.4",revision:"d4677a3",rnd:Math.floor(900*Math.random())+100,_:{pending:[],basePathSrcPattern:f},status:"unloaded",basePath:function(){var a=window.CKEDITOR_BASEPATH||"";
if(!a){for(var l=document.getElementsByTagName("script"),j=0;
j<l.length;
j++){var i=l[j].src.match(f);
if(i){a=i[1];
break
}}}-1==a.indexOf(":/")&&"//"!=a.slice(0,2)&&(a=0===a.indexOf("/")?location.href.match(/^.*?:\/\/[^\/]*/)[0]+a:location.href.match(/^[^\?]*\/(?:)/)[0]+a);
if(!a){throw'The CKEditor installation path could not be automatically detected. Please set the global variable "CKEDITOR_BASEPATH" before creating editor instances.'
}return a
}(),getUrl:function(b){-1==b.indexOf(":/")&&0!==b.indexOf("/")&&(b=this.basePath+b);
this.timestamp&&"/"!=b.charAt(b.length-1)&&!/[&?]t=/.test(b)&&(b+=(0<=b.indexOf("?")?"\x26":"?")+"t\x3d"+this.timestamp);
return b
},domReady:function(){function i(){try{document.addEventListener?(document.removeEventListener("DOMContentLoaded",i,!1),d()):document.attachEvent&&"complete"===document.readyState&&(document.detachEvent("onreadystatechange",i),d())
}catch(a){}}function d(){for(var b;
b=j.shift();
){b()
}}var j=[];
return function(a){function k(){try{document.documentElement.doScroll("left")
}catch(b){setTimeout(k,1);
return
}i()
}j.push(a);
"complete"===document.readyState&&setTimeout(i,1);
if(1==j.length){if(document.addEventListener){document.addEventListener("DOMContentLoaded",i,!1),window.addEventListener("load",i,!1)
}else{if(document.attachEvent){document.attachEvent("onreadystatechange",i);
window.attachEvent("onload",i);
a=!1;
try{a=!window.frameElement
}catch(c){}document.documentElement.doScroll&&a&&k()
}}}}
}()},e=window.CKEDITOR_GETURL;
if(e){var h=g.getUrl;
g.getUrl=function(b){return e.call(g,b)||h.call(g,b)
}
}return g
}());
CKEDITOR.event||(CKEDITOR.event=function(){},CKEDITOR.event.implementOn=function(e){var f=CKEDITOR.event.prototype,c;
for(c in f){null==e[c]&&(e[c]=f[c])
}},CKEDITOR.event.prototype=function(){function e(b){var d=f(this);
return d[b]||(d[b]=new c(b))
}var f=function(b){b=b.getPrivate&&b.getPrivate()||b._||(b._={});
return b.events||(b.events={})
},c=function(b){this.name=b;
this.listeners=[]
};
c.prototype={getListenerIndex:function(h){for(var g=0,i=this.listeners;
g<i.length;
g++){if(i[g].fn==h){return g
}}return -1
}};
return{define:function(a,h){var g=e.call(this,a);
CKEDITOR.tools.extend(g,h,!0)
},on:function(r,o,n,l,j){function a(b,h,d,q){b={name:r,sender:this,editor:b,data:h,listenerData:l,stop:d,cancel:q,removeListener:s};
return !1===o.call(n,b)?!1:b.data
}function s(){p.removeListener(r,o)
}var m=e.call(this,r);
if(0>m.getListenerIndex(o)){m=m.listeners;
n||(n=this);
isNaN(j)&&(j=10);
var p=this;
a.fn=o;
a.priority=j;
for(var i=m.length-1;
0<=i;
i--){if(m[i].priority<=j){return m.splice(i+1,0,a),{removeListener:s}
}}m.unshift(a)
}return{removeListener:s}
},once:function(){var g=Array.prototype.slice.call(arguments),d=g[1];
g[1]=function(b){b.removeListener();
return d.apply(this,arguments)
};
return this.on.apply(this,g)
},capture:function(){CKEDITOR.event.useCapture=1;
var b=this.on.apply(this,arguments);
CKEDITOR.event.useCapture=0;
return b
},fire:function(){var g=0,d=function(){g=1
},j=0,i=function(){j=1
};
return function(m,b,s){var o=f(this)[m];
m=g;
var p=j;
g=j=0;
if(o){var h=o.listeners;
if(h.length){for(var h=h.slice(0),r,a=0;
a<h.length;
a++){if(o.errorProof){try{r=h[a].call(this,s,b,d,i)
}catch(l){}}else{r=h[a].call(this,s,b,d,i)
}!1===r?j=1:"undefined"!=typeof r&&(b=r);
if(g||j){break
}}}}b=j?!1:"undefined"==typeof b?!0:b;
g=m;
j=p;
return b
}
}(),fireOnce:function(g,d,h){d=this.fire(g,d,h);
delete f(this)[g];
return d
},removeListener:function(g,d){var j=f(this)[g];
if(j){var i=j.getListenerIndex(d);
0<=i&&j.listeners.splice(i,1)
}},removeAllListeners:function(){var g=f(this),d;
for(d in g){delete g[d]
}},hasListeners:function(b){return(b=f(this)[b])&&0<b.listeners.length
}}
}());
CKEDITOR.editor||(CKEDITOR.editor=function(){CKEDITOR._.pending.push([this,arguments]);
CKEDITOR.event.call(this)
},CKEDITOR.editor.prototype.fire=function(b,c){b in {instanceReady:1,loaded:1}&&(this[b]=!0);
return CKEDITOR.event.prototype.fire.call(this,b,c,this)
},CKEDITOR.editor.prototype.fireOnce=function(b,c){b in {instanceReady:1,loaded:1}&&(this[b]=!0);
return CKEDITOR.event.prototype.fireOnce.call(this,b,c,this)
},CKEDITOR.event.implementOn(CKEDITOR.editor.prototype));
CKEDITOR.env||(CKEDITOR.env=function(){var f=navigator.userAgent.toLowerCase(),g=f.match(/edge[ \/](\d+.?\d*)/),e=-1<f.indexOf("trident/"),e=!(!g&&!e),e={ie:e,edge:!!g,webkit:!e&&-1<f.indexOf(" applewebkit/"),air:-1<f.indexOf(" adobeair/"),mac:-1<f.indexOf("macintosh"),quirks:"BackCompat"==document.compatMode&&(!document.documentMode||10>document.documentMode),mobile:-1<f.indexOf("mobile"),iOS:/(ipad|iphone|ipod)/.test(f),isCustomDomain:function(){if(!this.ie){return !1
}var d=document.domain,c=window.location.hostname;
return d!=c&&d!="["+c+"]"
},secure:"https:"==location.protocol};
e.gecko="Gecko"==navigator.product&&!e.webkit&&!e.ie;
e.webkit&&(-1<f.indexOf("chrome")?e.chrome=!0:e.safari=!0);
var h=0;
e.ie&&(h=g?parseFloat(g[1]):e.quirks||!document.documentMode?parseFloat(f.match(/msie (\d+)/)[1]):document.documentMode,e.ie9Compat=9==h,e.ie8Compat=8==h,e.ie7Compat=7==h,e.ie6Compat=7>h||e.quirks);
e.gecko&&(g=f.match(/rv:([\d\.]+)/))&&(g=g[1].split("."),h=10000*g[0]+100*(g[1]||0)+1*(g[2]||0));
e.air&&(h=parseFloat(f.match(/ adobeair\/(\d+)/)[1]));
e.webkit&&(h=parseFloat(f.match(/ applewebkit\/(\d+)/)[1]));
e.version=h;
e.isCompatible=!(e.ie&&7>h)&&!(e.gecko&&40000>h)&&!(e.webkit&&534>h);
e.hidpi=2<=window.devicePixelRatio;
e.needsBrFiller=e.gecko||e.webkit||e.ie&&10<h;
e.needsNbspFiller=e.ie&&11>h;
e.cssClass="cke_browser_"+(e.ie?"ie":e.gecko?"gecko":e.webkit?"webkit":"unknown");
e.quirks&&(e.cssClass+=" cke_browser_quirks");
e.ie&&(e.cssClass+=" cke_browser_ie"+(e.quirks?"6 cke_browser_iequirks":e.version));
e.air&&(e.cssClass+=" cke_browser_air");
e.iOS&&(e.cssClass+=" cke_browser_ios");
e.hidpi&&(e.cssClass+=" cke_hidpi");
return e
}());
"unloaded"==CKEDITOR.status&&function(){CKEDITOR.event.implementOn(CKEDITOR);
CKEDITOR.loadFullCore=function(){if("basic_ready"!=CKEDITOR.status){CKEDITOR.loadFullCore._load=1
}else{delete CKEDITOR.loadFullCore;
var b=document.createElement("script");
b.type="text/javascript";
b.src=CKEDITOR.basePath+"ckeditor.js";
document.getElementsByTagName("head")[0].appendChild(b)
}};
CKEDITOR.loadFullCoreTimeout=0;
CKEDITOR.add=function(b){(this._.pending||(this._.pending=[])).push(b)
};
(function(){CKEDITOR.domReady(function(){var b=CKEDITOR.loadFullCore,c=CKEDITOR.loadFullCoreTimeout;
b&&(CKEDITOR.status="basic_ready",b&&b._load?b():c&&setTimeout(function(){CKEDITOR.loadFullCore&&CKEDITOR.loadFullCore()
},1000*c))
})
})();
CKEDITOR.status="basic_loaded"
}();
"use strict";
CKEDITOR.VERBOSITY_WARN=1;
CKEDITOR.VERBOSITY_ERROR=2;
CKEDITOR.verbosity=CKEDITOR.VERBOSITY_WARN|CKEDITOR.VERBOSITY_ERROR;
CKEDITOR.warn=function(b,c){CKEDITOR.verbosity&CKEDITOR.VERBOSITY_WARN&&CKEDITOR.fire("log",{type:"warn",errorCode:b,additionalData:c})
};
CKEDITOR.error=function(b,c){CKEDITOR.verbosity&CKEDITOR.VERBOSITY_ERROR&&CKEDITOR.fire("log",{type:"error",errorCode:b,additionalData:c})
};
CKEDITOR.on("log",function(e){if(window.console&&window.console.log){var f=console[e.data.type]?e.data.type:"log",c=e.data.errorCode;
if(e=e.data.additionalData){console[f]("[CKEDITOR] "+c,e)
}else{console[f]("[CKEDITOR] "+c)
}console[f]("[CKEDITOR] For more information go to http://docs.ckeditor.com/#!/guide/dev_errors-section-"+c)
}},null,null,999);
CKEDITOR.dom={};
(function(){var r=[],n=CKEDITOR.env.gecko?"-moz-":CKEDITOR.env.webkit?"-webkit-":CKEDITOR.env.ie?"-ms-":"",p=/&/g,o=/>/g,m=/</g,l=/"/g,j=/&(lt|gt|amp|quot|nbsp|shy|#\d{1,5});/g,i={lt:"\x3c",gt:"\x3e",amp:"\x26",quot:'"',nbsp:" ",shy:""},g=function(b,c){return"#"==c[0]?String.fromCharCode(parseInt(c.slice(1),10)):i[c]
};
CKEDITOR.on("reset",function(){r=[]
});
CKEDITOR.tools={arrayCompare:function(d,e){if(!d&&!e){return !0
}if(!d||!e||d.length!=e.length){return !1
}for(var c=0;
c<d.length;
c++){if(d[c]!=e[c]){return !1
}}return !0
},getIndex:function(d,e){for(var c=0;
c<d.length;
++c){if(e(d[c])){return c
}}return -1
},clone:function(d){var e;
if(d&&d instanceof Array){e=[];
for(var c=0;
c<d.length;
c++){e[c]=CKEDITOR.tools.clone(d[c])
}return e
}if(null===d||"object"!=typeof d||d instanceof String||d instanceof Number||d instanceof Boolean||d instanceof Date||d instanceof RegExp||d.nodeType||d.window===d){return d
}e=new d.constructor;
for(c in d){e[c]=CKEDITOR.tools.clone(d[c])
}return e
},capitalize:function(b,c){return b.charAt(0).toUpperCase()+(c?b.slice(1):b.slice(1).toLowerCase())
},extend:function(f){var q=arguments.length,e,u;
"boolean"==typeof(e=arguments[q-1])?q--:"boolean"==typeof(e=arguments[q-2])&&(u=arguments[q-1],q-=2);
for(var h=1;
h<q;
h++){var s=arguments[h],t;
for(t in s){if(!0===e||null==f[t]){if(!u||t in u){f[t]=s[t]
}}}}return f
},prototypedCopy:function(b){var c=function(){};
c.prototype=b;
return new c
},copy:function(d){var e={},c;
for(c in d){e[c]=d[c]
}return e
},isArray:function(b){return"[object Array]"==Object.prototype.toString.call(b)
},isEmpty:function(b){for(var c in b){if(b.hasOwnProperty(c)){return !1
}}return !0
},cssVendorPrefix:function(d,e,c){if(c){return n+d+":"+e+";"+d+":"+e
}c={};
c[d]=e;
c[n+d]=e;
return c
},cssStyleToDomStyle:function(){var b=document.createElement("div").style,c="undefined"!=typeof b.cssFloat?"cssFloat":"undefined"!=typeof b.styleFloat?"styleFloat":"float";
return function(d){return"float"==d?c:d.replace(/-./g,function(e){return e.substr(1).toUpperCase()
})
}
}(),buildStyleHtml:function(e){e=[].concat(e);
for(var f,d=[],h=0;
h<e.length;
h++){if(f=e[h]){/@import|[{}]/.test(f)?d.push("\x3cstyle\x3e"+f+"\x3c/style\x3e"):d.push('\x3clink type\x3d"text/css" rel\x3dstylesheet href\x3d"'+f+'"\x3e')
}}return d.join("")
},htmlEncode:function(b){return void 0===b||null===b?"":String(b).replace(p,"\x26amp;").replace(o,"\x26gt;").replace(m,"\x26lt;")
},htmlDecode:function(b){return b.replace(j,g)
},htmlEncodeAttr:function(b){return CKEDITOR.tools.htmlEncode(b).replace(l,"\x26quot;")
},htmlDecodeAttr:function(b){return CKEDITOR.tools.htmlDecode(b)
},transformPlainTextToHtml:function(f,s){var e=s==CKEDITOR.ENTER_BR,w=this.htmlEncode(f.replace(/\r\n/g,"\n")),w=w.replace(/\t/g,"\x26nbsp;\x26nbsp; \x26nbsp;"),h=s==CKEDITOR.ENTER_P?"p":"div";
if(!e){var u=/\n{2}/g;
if(u.test(w)){var v="\x3c"+h+"\x3e",t="\x3c/"+h+"\x3e",w=v+w.replace(u,function(){return t+v
})+t
}}w=w.replace(/\n/g,"\x3cbr\x3e");
e||(w=w.replace(new RegExp("\x3cbr\x3e(?\x3d\x3c/"+h+"\x3e)"),function(b){return CKEDITOR.tools.repeat(b,2)
}));
w=w.replace(/^ | $/g,"\x26nbsp;");
return w=w.replace(/(>|\s) /g,function(d,c){return c+"\x26nbsp;"
}).replace(/ (?=<)/g,"\x26nbsp;")
},getNextNumber:function(){var b=0;
return function(){return ++b
}
}(),getNextId:function(){return"cke_"+this.getNextNumber()
},getUniqueId:function(){for(var d="e",c=0;
8>c;
c++){d+=Math.floor(65536*(1+Math.random())).toString(16).substring(1)
}return d
},override:function(e,d){var f=d(e);
f.prototype=e.prototype;
return f
},setTimeout:function(f,e,s,h,q){q||(q=window);
s||(s=q);
return q.setTimeout(function(){h?f.apply(s,[].concat(h)):f.apply(s)
},e||0)
},trim:function(){var b=/(?:^[ \t\n\r]+)|(?:[ \t\n\r]+$)/g;
return function(a){return a.replace(b,"")
}
}(),ltrim:function(){var b=/^[ \t\n\r]+/g;
return function(a){return a.replace(b,"")
}
}(),rtrim:function(){var b=/[ \t\n\r]+$/g;
return function(a){return a.replace(b,"")
}
}(),indexOf:function(e,d){if("function"==typeof d){for(var h=0,f=e.length;
h<f;
h++){if(d(e[h])){return h
}}}else{if(e.indexOf){return e.indexOf(d)
}h=0;
for(f=e.length;
h<f;
h++){if(e[h]===d){return h
}}}return -1
},search:function(e,d){var f=CKEDITOR.tools.indexOf(e,d);
return 0<=f?e[f]:null
},bind:function(d,c){return function(){return d.apply(c,arguments)
}
},createClass:function(f){var e=f.$,u=f.base,h=f.privates||f._,t=f.proto;
f=f.statics;
!e&&(e=function(){u&&this.base.apply(this,arguments)
});
if(h){var s=e,e=function(){var d=this._||(this._={}),c;
for(c in h){var k=h[c];
d[c]="function"==typeof k?CKEDITOR.tools.bind(k,this):k
}s.apply(this,arguments)
}
}u&&(e.prototype=this.prototypedCopy(u.prototype),e.prototype.constructor=e,e.base=u,e.baseProto=u.prototype,e.prototype.base=function(){this.base=u.prototype.base;
u.apply(this,arguments);
this.base=arguments.callee
});
t&&this.extend(e.prototype,t,!0);
f&&this.extend(e,f,!0);
return e
},addFunction:function(a,c){return r.push(function(){return a.apply(c||this,arguments)
})-1
},removeFunction:function(a){r[a]=null
},callFunction:function(a){var c=r[a];
return c&&c.apply(window,Array.prototype.slice.call(arguments,1))
},cssLength:function(){var d=/^-?\d+\.?\d*px$/,c;
return function(a){c=CKEDITOR.tools.trim(a+"")+"px";
return d.test(c)?c:a||""
}
}(),convertToPx:function(){var b;
return function(a){b||(b=CKEDITOR.dom.element.createFromHtml('\x3cdiv style\x3d"position:absolute;left:-9999px;top:-9999px;margin:0px;padding:0px;border:0px;"\x3e\x3c/div\x3e',CKEDITOR.document),CKEDITOR.document.getBody().append(b));
return/%$/.test(a)?a:(b.setStyle("width",a),b.$.clientWidth)
}
}(),repeat:function(d,c){return Array(c+1).join(d)
},tryThese:function(){for(var f,e=0,s=arguments.length;
e<s;
e++){var h=arguments[e];
try{f=h();
break
}catch(q){}}return f
},genKey:function(){return Array.prototype.slice.call(arguments).join("-")
},defer:function(b){return function(){var a=arguments,d=this;
window.setTimeout(function(){b.apply(d,a)
},0)
}
},normalizeCssText:function(f,e){var s=[],h,q=CKEDITOR.tools.parseCssText(f,!0,e);
for(h in q){s.push(h+":"+q[h])
}s.sort();
return s.length?s.join(";")+";":""
},convertRgbToHex:function(b){return b.replace(/(?:rgb\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\))/gi,function(e,d,h,f){e=[d,h,f];
for(d=0;
3>d;
d++){e[d]=("0"+parseInt(e[d],10).toString(16)).slice(-2)
}return"#"+e.join("")
})
},parseCssText:function(e,d,h){var f={};
h&&(h=new CKEDITOR.dom.element("span"),h.setAttribute("style",e),e=CKEDITOR.tools.convertRgbToHex(h.getAttribute("style")||""));
if(!e||";"==e){return f
}e.replace(/&quot;/g,'"').replace(/\s*([^:;\s]+)\s*:\s*([^;]+)\s*(?=;|$)/g,function(b,q,k){d&&(q=q.toLowerCase(),"font-family"==q&&(k=k.toLowerCase().replace(/["']/g,"").replace(/\s*,\s*/g,",")),k=CKEDITOR.tools.trim(k));
f[q]=k
});
return f
},writeCssText:function(e,d){var h,f=[];
for(h in e){f.push(h+":"+e[h])
}d&&f.sort();
return f.join("; ")
},objectCompare:function(e,d,h){var f;
if(!e&&!d){return !0
}if(!e||!d){return !1
}for(f in e){if(e[f]!=d[f]){return !1
}}if(!h){for(f in d){if(e[f]!=d[f]){return !1
}}}return !0
},objectKeys:function(e){var d=[],f;
for(f in e){d.push(f)
}return d
},convertArrayToObject:function(f,e){var s={};
1==arguments.length&&(e=!0);
for(var h=0,q=f.length;
h<q;
++h){s[f[h]]=e
}return s
},fixDomain:function(){for(var d;
;
){try{d=window.parent.document.domain;
break
}catch(c){d=d?d.replace(/.+?(?:\.|$)/,""):document.domain;
if(!d){break
}document.domain=d
}}return !!d
},eventsBuffer:function(f,e,u){function h(){s=(new Date).getTime();
t=!1;
u?e.call(u):e()
}var t,s=0;
return{input:function(){if(!t){var a=(new Date).getTime()-s;
a<f?t=setTimeout(h,f-a):h()
}},reset:function(){t&&clearTimeout(t);
t=s=0
}}
},enableHtml5Elements:function(f,e){for(var s="abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup main mark meter nav output progress section summary time video".split(" "),h=s.length,q;
h--;
){q=f.createElement(s[h]),e&&f.appendChild(q)
}},checkIfAnyArrayItemMatches:function(e,d){for(var h=0,f=e.length;
h<f;
++h){if(e[h].match(d)){return !0
}}return !1
},checkIfAnyObjectPropertyMatches:function(e,d){for(var f in e){if(f.match(d)){return !0
}}return !1
},transparentImageData:"data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw\x3d\x3d"}
})();
CKEDITOR.dtd=function(){var r=CKEDITOR.tools.extend,n=function(f,e){for(var s=CKEDITOR.tools.clone(f),h=1;
h<arguments.length;
h++){e=arguments[h];
for(var q in e){delete s[q]
}}return s
},p={},o={},m={address:1,article:1,aside:1,blockquote:1,details:1,div:1,dl:1,fieldset:1,figure:1,footer:1,form:1,h1:1,h2:1,h3:1,h4:1,h5:1,h6:1,header:1,hgroup:1,hr:1,main:1,menu:1,nav:1,ol:1,p:1,pre:1,section:1,table:1,ul:1},l={command:1,link:1,meta:1,noscript:1,script:1,style:1},j={},i={"#":1},g={center:1,dir:1,noframes:1};
r(p,{a:1,abbr:1,area:1,audio:1,b:1,bdi:1,bdo:1,br:1,button:1,canvas:1,cite:1,code:1,command:1,datalist:1,del:1,dfn:1,em:1,embed:1,i:1,iframe:1,img:1,input:1,ins:1,kbd:1,keygen:1,label:1,map:1,mark:1,meter:1,noscript:1,object:1,output:1,progress:1,q:1,ruby:1,s:1,samp:1,script:1,select:1,small:1,span:1,strong:1,sub:1,sup:1,textarea:1,time:1,u:1,"var":1,video:1,wbr:1},i,{acronym:1,applet:1,basefont:1,big:1,font:1,isindex:1,strike:1,style:1,tt:1});
r(o,m,p,g);
n={a:n(p,{a:1,button:1}),abbr:p,address:o,area:j,article:o,aside:o,audio:r({source:1,track:1},o),b:p,base:j,bdi:p,bdo:p,blockquote:o,body:o,br:j,button:n(p,{a:1,button:1}),canvas:p,caption:o,cite:p,code:p,col:j,colgroup:{col:1},command:j,datalist:r({option:1},p),dd:o,del:p,details:r({summary:1},o),dfn:p,div:o,dl:{dt:1,dd:1},dt:o,em:p,embed:j,fieldset:r({legend:1},o),figcaption:o,figure:r({figcaption:1},o),footer:o,form:o,h1:p,h2:p,h3:p,h4:p,h5:p,h6:p,head:r({title:1,base:1},l),header:o,hgroup:{h1:1,h2:1,h3:1,h4:1,h5:1,h6:1},hr:j,html:r({head:1,body:1},o,l),i:p,iframe:i,img:j,input:j,ins:p,kbd:p,keygen:j,label:p,legend:p,li:o,link:j,main:o,map:o,mark:p,menu:r({li:1},o),meta:j,meter:n(p,{meter:1}),nav:o,noscript:r({link:1,meta:1,style:1},p),object:r({param:1},p),ol:{li:1},optgroup:{option:1},option:i,output:p,p:p,param:j,pre:p,progress:n(p,{progress:1}),q:p,rp:p,rt:p,ruby:r({rp:1,rt:1},p),s:p,samp:p,script:i,section:o,select:{optgroup:1,option:1},small:p,source:j,span:p,strong:p,style:i,sub:p,summary:r({h1:1,h2:1,h3:1,h4:1,h5:1,h6:1},p),sup:p,table:{caption:1,colgroup:1,thead:1,tfoot:1,tbody:1,tr:1},tbody:{tr:1},td:o,textarea:i,tfoot:{tr:1},th:o,thead:{tr:1},time:n(p,{time:1}),title:i,tr:{th:1,td:1},track:j,u:p,ul:{li:1},"var":p,video:r({source:1,track:1},o),wbr:j,acronym:p,applet:r({param:1},o),basefont:j,big:p,center:o,dialog:j,dir:{li:1},font:p,isindex:j,noframes:o,strike:p,tt:p};
r(n,{$block:r({audio:1,dd:1,dt:1,figcaption:1,li:1,video:1},m,g),$blockLimit:{article:1,aside:1,audio:1,body:1,caption:1,details:1,dir:1,div:1,dl:1,fieldset:1,figcaption:1,figure:1,footer:1,form:1,header:1,hgroup:1,main:1,menu:1,nav:1,ol:1,section:1,table:1,td:1,th:1,tr:1,ul:1,video:1},$cdata:{script:1,style:1},$editable:{address:1,article:1,aside:1,blockquote:1,body:1,details:1,div:1,fieldset:1,figcaption:1,footer:1,form:1,h1:1,h2:1,h3:1,h4:1,h5:1,h6:1,header:1,hgroup:1,main:1,nav:1,p:1,pre:1,section:1},$empty:{area:1,base:1,basefont:1,br:1,col:1,command:1,dialog:1,embed:1,hr:1,img:1,input:1,isindex:1,keygen:1,link:1,meta:1,param:1,source:1,track:1,wbr:1},$inline:p,$list:{dl:1,ol:1,ul:1},$listItem:{dd:1,dt:1,li:1},$nonBodyContent:r({body:1,head:1,html:1},n.head),$nonEditable:{applet:1,audio:1,button:1,embed:1,iframe:1,map:1,object:1,option:1,param:1,script:1,textarea:1,video:1},$object:{applet:1,audio:1,button:1,hr:1,iframe:1,img:1,input:1,object:1,select:1,table:1,textarea:1,video:1},$removeEmpty:{abbr:1,acronym:1,b:1,bdi:1,bdo:1,big:1,cite:1,code:1,del:1,dfn:1,em:1,font:1,i:1,ins:1,label:1,kbd:1,mark:1,meter:1,output:1,q:1,ruby:1,s:1,samp:1,small:1,span:1,strike:1,strong:1,sub:1,sup:1,time:1,tt:1,u:1,"var":1},$tabIndex:{a:1,area:1,button:1,input:1,object:1,select:1,textarea:1},$tableContent:{caption:1,col:1,colgroup:1,tbody:1,td:1,tfoot:1,th:1,thead:1,tr:1},$transparent:{a:1,audio:1,canvas:1,del:1,ins:1,map:1,noscript:1,object:1,video:1},$intermediate:{caption:1,colgroup:1,dd:1,dt:1,figcaption:1,legend:1,li:1,optgroup:1,option:1,rp:1,rt:1,summary:1,tbody:1,td:1,tfoot:1,th:1,thead:1,tr:1}});
return n
}();
CKEDITOR.dom.event=function(b){this.$=b
};
CKEDITOR.dom.event.prototype={getKey:function(){return this.$.keyCode||this.$.which
},getKeystroke:function(){var b=this.getKey();
if(this.$.ctrlKey||this.$.metaKey){b+=CKEDITOR.CTRL
}this.$.shiftKey&&(b+=CKEDITOR.SHIFT);
this.$.altKey&&(b+=CKEDITOR.ALT);
return b
},preventDefault:function(b){var c=this.$;
c.preventDefault?c.preventDefault():c.returnValue=!1;
b&&this.stopPropagation()
},stopPropagation:function(){var b=this.$;
b.stopPropagation?b.stopPropagation():b.cancelBubble=!0
},getTarget:function(){var b=this.$.target||this.$.srcElement;
return b?new CKEDITOR.dom.node(b):null
},getPhase:function(){return this.$.eventPhase||2
},getPageOffset:function(){var b=this.getTarget().getDocument().$;
return{x:this.$.pageX||this.$.clientX+(b.documentElement.scrollLeft||b.body.scrollLeft),y:this.$.pageY||this.$.clientY+(b.documentElement.scrollTop||b.body.scrollTop)}
}};
CKEDITOR.CTRL=1114112;
CKEDITOR.SHIFT=2228224;
CKEDITOR.ALT=4456448;
CKEDITOR.EVENT_PHASE_CAPTURING=1;
CKEDITOR.EVENT_PHASE_AT_TARGET=2;
CKEDITOR.EVENT_PHASE_BUBBLING=3;
CKEDITOR.dom.domObject=function(b){b&&(this.$=b)
};
CKEDITOR.dom.domObject.prototype=function(){var b=function(d,c){return function(a){"undefined"!=typeof CKEDITOR&&d.fire(c,new CKEDITOR.dom.event(a))
}
};
return{getPrivate:function(){var c;
(c=this.getCustomData("_"))||this.setCustomData("_",c={});
return c
},on:function(c){var a=this.getCustomData("_cke_nativeListeners");
a||(a={},this.setCustomData("_cke_nativeListeners",a));
a[c]||(a=a[c]=b(this,c),this.$.addEventListener?this.$.addEventListener(c,a,!!CKEDITOR.event.useCapture):this.$.attachEvent&&this.$.attachEvent("on"+c,a));
return CKEDITOR.event.prototype.on.apply(this,arguments)
},removeListener:function(e){CKEDITOR.event.prototype.removeListener.apply(this,arguments);
if(!this.hasListeners(e)){var d=this.getCustomData("_cke_nativeListeners"),f=d&&d[e];
f&&(this.$.removeEventListener?this.$.removeEventListener(e,f,!1):this.$.detachEvent&&this.$.detachEvent("on"+e,f),delete d[e])
}},removeAllListeners:function(){var e=this.getCustomData("_cke_nativeListeners"),d;
for(d in e){var f=e[d];
this.$.detachEvent?this.$.detachEvent("on"+d,f):this.$.removeEventListener&&this.$.removeEventListener(d,f,!1);
delete e[d]
}CKEDITOR.event.prototype.removeAllListeners.call(this)
}}
}();
(function(b){var c={};
CKEDITOR.on("reset",function(){c={}
});
b.equals=function(d){try{return d&&d.$===this.$
}catch(e){return !1
}};
b.setCustomData=function(d,g){var f=this.getUniqueId();
(c[f]||(c[f]={}))[d]=g;
return this
};
b.getCustomData=function(d){var e=this.$["data-cke-expando"];
return(e=e&&c[e])&&d in e?e[d]:null
};
b.removeCustomData=function(d){var i=this.$["data-cke-expando"],i=i&&c[i],h,g;
i&&(h=i[d],g=d in i,delete i[d]);
return g?h:null
};
b.clearCustomData=function(){this.removeAllListeners();
var d=this.$["data-cke-expando"];
d&&delete c[d]
};
b.getUniqueId=function(){return this.$["data-cke-expando"]||(this.$["data-cke-expando"]=CKEDITOR.tools.getNextNumber())
};
CKEDITOR.event.implementOn(b)
})(CKEDITOR.dom.domObject.prototype);
CKEDITOR.dom.node=function(b){return b?new CKEDITOR.dom[b.nodeType==CKEDITOR.NODE_DOCUMENT?"document":b.nodeType==CKEDITOR.NODE_ELEMENT?"element":b.nodeType==CKEDITOR.NODE_TEXT?"text":b.nodeType==CKEDITOR.NODE_COMMENT?"comment":b.nodeType==CKEDITOR.NODE_DOCUMENT_FRAGMENT?"documentFragment":"domObject"](b):this
};
CKEDITOR.dom.node.prototype=new CKEDITOR.dom.domObject;
CKEDITOR.NODE_ELEMENT=1;
CKEDITOR.NODE_DOCUMENT=9;
CKEDITOR.NODE_TEXT=3;
CKEDITOR.NODE_COMMENT=8;
CKEDITOR.NODE_DOCUMENT_FRAGMENT=11;
CKEDITOR.POSITION_IDENTICAL=0;
CKEDITOR.POSITION_DISCONNECTED=1;
CKEDITOR.POSITION_FOLLOWING=2;
CKEDITOR.POSITION_PRECEDING=4;
CKEDITOR.POSITION_IS_CONTAINED=8;
CKEDITOR.POSITION_CONTAINS=16;
CKEDITOR.tools.extend(CKEDITOR.dom.node.prototype,{appendTo:function(b,c){b.append(this,c);
return b
},clone:function(g,i){function f(b){b["data-cke-expando"]&&(b["data-cke-expando"]=!1);
if(b.nodeType==CKEDITOR.NODE_ELEMENT||b.nodeType==CKEDITOR.NODE_DOCUMENT_FRAGMENT){if(i||b.nodeType!=CKEDITOR.NODE_ELEMENT||b.removeAttribute("id",!1),g){b=b.childNodes;
for(var a=0;
a<b.length;
a++){f(b[a])
}}}}function j(a){if(a.type==CKEDITOR.NODE_ELEMENT||a.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT){if(a.type!=CKEDITOR.NODE_DOCUMENT_FRAGMENT){var c=a.getName();
":"==c[0]&&a.renameNode(c.substring(1))
}if(g){for(c=0;
c<a.getChildCount();
c++){j(a.getChild(c))
}}}}var h=this.$.cloneNode(g);
f(h);
h=new CKEDITOR.dom.node(h);
CKEDITOR.env.ie&&9>CKEDITOR.env.version&&(this.type==CKEDITOR.NODE_ELEMENT||this.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT)&&j(h);
return h
},hasPrevious:function(){return !!this.$.previousSibling
},hasNext:function(){return !!this.$.nextSibling
},insertAfter:function(b){b.$.parentNode.insertBefore(this.$,b.$.nextSibling);
return b
},insertBefore:function(b){b.$.parentNode.insertBefore(this.$,b.$);
return b
},insertBeforeMe:function(b){this.$.parentNode.insertBefore(b.$,this.$);
return b
},getAddress:function(g){for(var i=[],f=this.getDocument().$.documentElement,j=this.$;
j&&j!=f;
){var h=j.parentNode;
h&&i.unshift(this.getIndex.call({$:j},g));
j=h
}return i
},getDocument:function(){return new CKEDITOR.dom.document(this.$.ownerDocument||this.$.parentNode.ownerDocument)
},getIndex:function(g){function i(e,d){var k=d?e.nextSibling:e.previousSibling;
return k&&k.nodeType==CKEDITOR.NODE_TEXT?k.nodeValue?k:i(k,d):null
}var f=this.$,j=-1,h;
if(!this.$.parentNode||g&&f.nodeType==CKEDITOR.NODE_TEXT&&!f.nodeValue&&!i(f)&&!i(f,!0)){return -1
}do{if(!g||f==this.$||f.nodeType!=CKEDITOR.NODE_TEXT||!h&&f.nodeValue){j++,h=f.nodeType==CKEDITOR.NODE_TEXT
}}while(f=f.previousSibling);
return j
},getNextSourceNode:function(g,i,f){if(f&&!f.call){var j=f;
f=function(b){return !b.equals(j)
}
}g=!g&&this.getFirst&&this.getFirst();
var h;
if(!g){if(this.type==CKEDITOR.NODE_ELEMENT&&f&&!1===f(this,!0)){return null
}g=this.getNext()
}for(;
!g&&(h=(h||this).getParent());
){if(f&&!1===f(h,!0)){return null
}g=h.getNext()
}return !g||f&&!1===f(g)?null:i&&i!=g.type?g.getNextSourceNode(!1,i,f):g
},getPreviousSourceNode:function(g,i,f){if(f&&!f.call){var j=f;
f=function(b){return !b.equals(j)
}
}g=!g&&this.getLast&&this.getLast();
var h;
if(!g){if(this.type==CKEDITOR.NODE_ELEMENT&&f&&!1===f(this,!0)){return null
}g=this.getPrevious()
}for(;
!g&&(h=(h||this).getParent());
){if(f&&!1===f(h,!0)){return null
}g=h.getPrevious()
}return !g||f&&!1===f(g)?null:i&&g.type!=i?g.getPreviousSourceNode(!1,i,f):g
},getPrevious:function(e){var f=this.$,c;
do{c=(f=f.previousSibling)&&10!=f.nodeType&&new CKEDITOR.dom.node(f)
}while(c&&e&&!e(c));
return c
},getNext:function(e){var f=this.$,c;
do{c=(f=f.nextSibling)&&new CKEDITOR.dom.node(f)
}while(c&&e&&!e(c));
return c
},getParent:function(b){var c=this.$.parentNode;
return c&&(c.nodeType==CKEDITOR.NODE_ELEMENT||b&&c.nodeType==CKEDITOR.NODE_DOCUMENT_FRAGMENT)?new CKEDITOR.dom.node(c):null
},getParents:function(e){var f=this,c=[];
do{c[e?"push":"unshift"](f)
}while(f=f.getParent());
return c
},getCommonAncestor:function(b){if(b.equals(this)){return this
}if(b.contains&&b.contains(this)){return b
}var c=this.contains?this:this.getParent();
do{if(c.contains(b)){return c
}}while(c=c.getParent());
return null
},getPosition:function(f){var g=this.$,e=f.$;
if(g.compareDocumentPosition){return g.compareDocumentPosition(e)
}if(g==e){return CKEDITOR.POSITION_IDENTICAL
}if(this.type==CKEDITOR.NODE_ELEMENT&&f.type==CKEDITOR.NODE_ELEMENT){if(g.contains){if(g.contains(e)){return CKEDITOR.POSITION_CONTAINS+CKEDITOR.POSITION_PRECEDING
}if(e.contains(g)){return CKEDITOR.POSITION_IS_CONTAINED+CKEDITOR.POSITION_FOLLOWING
}}if("sourceIndex" in g){return 0>g.sourceIndex||0>e.sourceIndex?CKEDITOR.POSITION_DISCONNECTED:g.sourceIndex<e.sourceIndex?CKEDITOR.POSITION_PRECEDING:CKEDITOR.POSITION_FOLLOWING
}}g=this.getAddress();
f=f.getAddress();
for(var e=Math.min(g.length,f.length),h=0;
h<e;
h++){if(g[h]!=f[h]){return g[h]<f[h]?CKEDITOR.POSITION_PRECEDING:CKEDITOR.POSITION_FOLLOWING
}}return g.length<f.length?CKEDITOR.POSITION_CONTAINS+CKEDITOR.POSITION_PRECEDING:CKEDITOR.POSITION_IS_CONTAINED+CKEDITOR.POSITION_FOLLOWING
},getAscendant:function(h,k){var g=this.$,l,j;
k||(g=g.parentNode);
"function"==typeof h?(j=!0,l=h):(j=!1,l=function(a){a="string"==typeof a.nodeName?a.nodeName.toLowerCase():"";
return"string"==typeof h?a==h:a in h
});
for(;
g;
){if(l(j?new CKEDITOR.dom.node(g):g)){return new CKEDITOR.dom.node(g)
}try{g=g.parentNode
}catch(i){g=null
}}return null
},hasAscendant:function(e,f){var c=this.$;
f||(c=c.parentNode);
for(;
c;
){if(c.nodeName&&c.nodeName.toLowerCase()==e){return !0
}c=c.parentNode
}return !1
},move:function(b,c){b.append(this.remove(),c)
},remove:function(e){var f=this.$,c=f.parentNode;
if(c){if(e){for(;
e=f.firstChild;
){c.insertBefore(f.removeChild(e),f)
}}c.removeChild(f)
}return this
},replace:function(b){this.insertBefore(b);
b.remove()
},trim:function(){this.ltrim();
this.rtrim()
},ltrim:function(){for(var e;
this.getFirst&&(e=this.getFirst());
){if(e.type==CKEDITOR.NODE_TEXT){var f=CKEDITOR.tools.ltrim(e.getText()),c=e.getLength();
if(f){f.length<c&&(e.split(c-f.length),this.$.removeChild(this.$.firstChild))
}else{e.remove();
continue
}}break
}},rtrim:function(){for(var e;
this.getLast&&(e=this.getLast());
){if(e.type==CKEDITOR.NODE_TEXT){var f=CKEDITOR.tools.rtrim(e.getText()),c=e.getLength();
if(f){f.length<c&&(e.split(f.length),this.$.lastChild.parentNode.removeChild(this.$.lastChild))
}else{e.remove();
continue
}}break
}CKEDITOR.env.needsBrFiller&&(e=this.$.lastChild)&&1==e.type&&"br"==e.nodeName.toLowerCase()&&e.parentNode.removeChild(e)
},isReadOnly:function(b){var c=this;
this.type!=CKEDITOR.NODE_ELEMENT&&(c=this.getParent());
CKEDITOR.env.edge&&c&&c.is("textarea")&&(b=!0);
if(!b&&c&&"undefined"!=typeof c.$.isContentEditable){return !(c.$.isContentEditable||c.data("cke-editable"))
}for(;
c;
){if(c.data("cke-editable")){return !1
}if(c.hasAttribute("contenteditable")){return"false"==c.getAttribute("contenteditable")
}c=c.getParent()
}return !0
}});
CKEDITOR.dom.window=function(b){CKEDITOR.dom.domObject.call(this,b)
};
CKEDITOR.dom.window.prototype=new CKEDITOR.dom.domObject;
CKEDITOR.tools.extend(CKEDITOR.dom.window.prototype,{focus:function(){this.$.focus()
},getViewPaneSize:function(){var b=this.$.document,c="CSS1Compat"==b.compatMode;
return{width:(c?b.documentElement.clientWidth:b.body.clientWidth)||0,height:(c?b.documentElement.clientHeight:b.body.clientHeight)||0}
},getScrollPosition:function(){var b=this.$;
if("pageXOffset" in b){return{x:b.pageXOffset||0,y:b.pageYOffset||0}
}b=b.document;
return{x:b.documentElement.scrollLeft||b.body.scrollLeft||0,y:b.documentElement.scrollTop||b.body.scrollTop||0}
},getFrame:function(){var b=this.$.frameElement;
return b?new CKEDITOR.dom.element.get(b):null
}});
CKEDITOR.dom.document=function(b){CKEDITOR.dom.domObject.call(this,b)
};
CKEDITOR.dom.document.prototype=new CKEDITOR.dom.domObject;
CKEDITOR.tools.extend(CKEDITOR.dom.document.prototype,{type:CKEDITOR.NODE_DOCUMENT,appendStyleSheet:function(b){if(this.$.createStyleSheet){this.$.createStyleSheet(b)
}else{var c=new CKEDITOR.dom.element("link");
c.setAttributes({rel:"stylesheet",type:"text/css",href:b});
this.getHead().append(c)
}},appendStyleText:function(e){if(this.$.createStyleSheet){var f=this.$.createStyleSheet("");
f.cssText=e
}else{var c=new CKEDITOR.dom.element("style",this);
c.append(new CKEDITOR.dom.text(e,this));
this.getHead().append(c)
}return f||c.$.sheet
},createElement:function(e,f){var c=new CKEDITOR.dom.element(e,this);
f&&(f.attributes&&c.setAttributes(f.attributes),f.styles&&c.setStyles(f.styles));
return c
},createText:function(b){return new CKEDITOR.dom.text(b,this)
},focus:function(){this.getWindow().focus()
},getActive:function(){var b;
try{b=this.$.activeElement
}catch(c){return null
}return new CKEDITOR.dom.element(b)
},getById:function(b){return(b=this.$.getElementById(b))?new CKEDITOR.dom.element(b):null
},getByAddress:function(i,o){for(var g=this.$.documentElement,p=0;
g&&p<i.length;
p++){var n=i[p];
if(o){for(var m=-1,l=0;
l<g.childNodes.length;
l++){var j=g.childNodes[l];
if(!0!==o||3!=j.nodeType||!j.previousSibling||3!=j.previousSibling.nodeType){if(m++,m==n){g=j;
break
}}}}else{g=g.childNodes[n]
}}return g?new CKEDITOR.dom.node(g):null
},getElementsByTag:function(b,c){CKEDITOR.env.ie&&8>=document.documentMode||!c||(b=c+":"+b);
return new CKEDITOR.dom.nodeList(this.$.getElementsByTagName(b))
},getHead:function(){var b=this.$.getElementsByTagName("head")[0];
return b=b?new CKEDITOR.dom.element(b):this.getDocumentElement().append(new CKEDITOR.dom.element("head"),!0)
},getBody:function(){return new CKEDITOR.dom.element(this.$.body)
},getDocumentElement:function(){return new CKEDITOR.dom.element(this.$.documentElement)
},getWindow:function(){return new CKEDITOR.dom.window(this.$.parentWindow||this.$.defaultView)
},write:function(b){this.$.open("text/html","replace");
CKEDITOR.env.ie&&(b=b.replace(/(?:^\s*<!DOCTYPE[^>]*?>)|^/i,'$\x26\n\x3cscript data-cke-temp\x3d"1"\x3e('+CKEDITOR.tools.fixDomain+")();\x3c/script\x3e"));
this.$.write(b);
this.$.close()
},find:function(b){return new CKEDITOR.dom.nodeList(this.$.querySelectorAll(b))
},findOne:function(b){return(b=this.$.querySelector(b))?new CKEDITOR.dom.element(b):null
},_getHtml5ShivFrag:function(){var b=this.getCustomData("html5ShivFrag");
b||(b=this.$.createDocumentFragment(),CKEDITOR.tools.enableHtml5Elements(b,!0),this.setCustomData("html5ShivFrag",b));
return b
}});
CKEDITOR.dom.nodeList=function(b){this.$=b
};
CKEDITOR.dom.nodeList.prototype={count:function(){return this.$.length
},getItem:function(b){return 0>b||b>=this.$.length?null:(b=this.$[b])?new CKEDITOR.dom.node(b):null
}};
CKEDITOR.dom.element=function(b,c){"string"==typeof b&&(b=(c?c.$:document).createElement(b));
CKEDITOR.dom.domObject.call(this,b)
};
CKEDITOR.dom.element.get=function(b){return(b="string"==typeof b?document.getElementById(b)||document.getElementsByName(b)[0]:b)&&(b.$?b:new CKEDITOR.dom.element(b))
};
CKEDITOR.dom.element.prototype=new CKEDITOR.dom.node;
CKEDITOR.dom.element.createFromHtml=function(e,f){var c=new CKEDITOR.dom.element("div",f);
c.setHtml(e);
return c.getFirst().remove()
};
CKEDITOR.dom.element.setMarker=function(h,k,g,l){var j=k.getCustomData("list_marker_id")||k.setCustomData("list_marker_id",CKEDITOR.tools.getNextNumber()).getCustomData("list_marker_id"),i=k.getCustomData("list_marker_names")||k.setCustomData("list_marker_names",{}).getCustomData("list_marker_names");
h[j]=k;
i[g]=1;
return k.setCustomData(g,l)
};
CKEDITOR.dom.element.clearAllMarkers=function(b){for(var c in b){CKEDITOR.dom.element.clearMarkers(b,b[c],1)
}};
CKEDITOR.dom.element.clearMarkers=function(h,k,g){var l=k.getCustomData("list_marker_names"),j=k.getCustomData("list_marker_id"),i;
for(i in l){k.removeCustomData(i)
}k.removeCustomData("list_marker_names");
g&&(k.removeCustomData("list_marker_id"),delete h[j])
};
(function(){function i(d,c){return -1<(" "+d+" ").replace(k," ").indexOf(" "+c+" ")
}function m(d){var c=!0;
d.$.id||(d.$.id="cke_tmp_"+CKEDITOR.tools.getNextNumber(),c=!1);
return function(){c||d.removeAttribute("id")
}
}function g(d,c){return"#"+d.$.id+" "+c.split(/,\s*/).join(", #"+d.$.id+" ")
}function n(e){for(var d=0,h=0,f=j[e].length;
h<f;
h++){d+=parseInt(this.getComputedStyle(j[e][h])||0,10)||0
}return d
}var l=!!document.createElement("span").classList,k=/[\n\t\r]/g;
CKEDITOR.tools.extend(CKEDITOR.dom.element.prototype,{type:CKEDITOR.NODE_ELEMENT,addClass:l?function(b){this.$.classList.add(b);
return this
}:function(a){var d=this.$.className;
d&&(i(d,a)||(d+=" "+a));
this.$.className=d||a;
return this
},removeClass:l?function(d){var c=this.$;
c.classList.remove(d);
c.className||c.removeAttribute("class");
return this
}:function(a){var d=this.getAttribute("class");
d&&i(d,a)&&((d=d.replace(new RegExp("(?:^|\\s+)"+a+"(?\x3d\\s|$)"),"").replace(/^\s+/,""))?this.setAttribute("class",d):this.removeAttribute("class"));
return this
},hasClass:function(a){return i(this.$.className,a)
},append:function(d,c){"string"==typeof d&&(d=this.getDocument().createElement(d));
c?this.$.insertBefore(d.$,this.$.firstChild):this.$.appendChild(d.$);
return d
},appendHtml:function(d){if(this.$.childNodes.length){var c=new CKEDITOR.dom.element("div",this.getDocument());
c.setHtml(d);
c.moveChildren(this)
}else{this.setHtml(d)
}},appendText:function(b){null!=this.$.text&&CKEDITOR.env.ie&&9>CKEDITOR.env.version?this.$.text+=b:this.append(new CKEDITOR.dom.text(b))
},appendBogus:function(b){if(b||CKEDITOR.env.needsBrFiller){for(b=this.getLast();
b&&b.type==CKEDITOR.NODE_TEXT&&!CKEDITOR.tools.rtrim(b.getText());
){b=b.getPrevious()
}b&&b.is&&b.is("br")||(b=this.getDocument().createElement("br"),CKEDITOR.env.gecko&&b.setAttribute("type","_moz"),this.append(b))
}},breakParent:function(e,d){var h=new CKEDITOR.dom.range(this.getDocument());
h.setStartAfter(this);
h.setEndAfter(e);
var f=h.extractContents(!1,d||!1);
h.insertNode(this.remove());
f.insertAfterNode(this)
},contains:document.compareDocumentPosition?function(b){return !!(this.$.compareDocumentPosition(b.$)&16)
}:function(d){var c=this.$;
return d.type!=CKEDITOR.NODE_ELEMENT?c.contains(d.getParent().$):c!=d.$&&c.contains(d.$)
},focus:function(){function b(){try{this.$.focus()
}catch(a){}}return function(a){a?CKEDITOR.tools.setTimeout(b,100,this):b.call(this)
}
}(),getHtml:function(){var b=this.$.innerHTML;
return CKEDITOR.env.ie?b.replace(/<\?[^>]*>/g,""):b
},getOuterHtml:function(){if(this.$.outerHTML){return this.$.outerHTML.replace(/<\?[^>]*>/,"")
}var b=this.$.ownerDocument.createElement("div");
b.appendChild(this.$.cloneNode(!0));
return b.innerHTML
},getClientRect:function(){var b=CKEDITOR.tools.extend({},this.$.getBoundingClientRect());
!b.width&&(b.width=b.right-b.left);
!b.height&&(b.height=b.bottom-b.top);
return b
},setHtml:CKEDITOR.env.ie&&9>CKEDITOR.env.version?function(e){try{var d=this.$;
if(this.getParent()){return d.innerHTML=e
}var h=this.getDocument()._getHtml5ShivFrag();
h.appendChild(d);
d.innerHTML=e;
h.removeChild(d);
return e
}catch(f){this.$.innerHTML="";
d=new CKEDITOR.dom.element("body",this.getDocument());
d.$.innerHTML=e;
for(d=d.getChildren();
d.count();
){this.append(d.getItem(0))
}return e
}}:function(b){return this.$.innerHTML=b
},setText:function(){var b=document.createElement("p");
b.innerHTML="x";
b=b.textContent;
return function(a){this.$[b?"textContent":"innerText"]=a
}
}(),getAttribute:function(){var b=function(c){return this.$.getAttribute(c,2)
};
return CKEDITOR.env.ie&&(CKEDITOR.env.ie7Compat||CKEDITOR.env.quirks)?function(c){switch(c){case"class":c="className";
break;
case"http-equiv":c="httpEquiv";
break;
case"name":return this.$.name;
case"tabindex":return c=this.$.getAttribute(c,2),0!==c&&0===this.$.tabIndex&&(c=null),c;
case"checked":return c=this.$.attributes.getNamedItem(c),(c.specified?c.nodeValue:this.$.checked)?"checked":null;
case"hspace":case"value":return this.$[c];
case"style":return this.$.style.cssText;
case"contenteditable":case"contentEditable":return this.$.attributes.getNamedItem("contentEditable").specified?this.$.getAttribute("contentEditable"):null
}return this.$.getAttribute(c,2)
}:b
}(),getChildren:function(){return new CKEDITOR.dom.nodeList(this.$.childNodes)
},getComputedStyle:document.defaultView&&document.defaultView.getComputedStyle?function(d){var c=this.getWindow().$.getComputedStyle(this.$,null);
return c?c.getPropertyValue(d):""
}:function(b){return this.$.currentStyle[CKEDITOR.tools.cssStyleToDomStyle(b)]
},getDtd:function(){var b=CKEDITOR.dtd[this.getName()];
this.getDtd=function(){return b
};
return b
},getElementsByTag:CKEDITOR.dom.document.prototype.getElementsByTag,getTabIndex:function(){var b=this.$.tabIndex;
return 0!==b||CKEDITOR.dtd.$tabIndex[this.getName()]||0===parseInt(this.getAttribute("tabindex"),10)?b:-1
},getText:function(){return this.$.textContent||this.$.innerText||""
},getWindow:function(){return this.getDocument().getWindow()
},getId:function(){return this.$.id||null
},getNameAtt:function(){return this.$.name||null
},getName:function(){var d=this.$.nodeName.toLowerCase();
if(CKEDITOR.env.ie&&8>=document.documentMode){var c=this.$.scopeName;
"HTML"!=c&&(d=c.toLowerCase()+":"+d)
}this.getName=function(){return d
};
return this.getName()
},getValue:function(){return this.$.value
},getFirst:function(d){var c=this.$.firstChild;
(c=c&&new CKEDITOR.dom.node(c))&&d&&!d(c)&&(c=c.getNext(d));
return c
},getLast:function(d){var c=this.$.lastChild;
(c=c&&new CKEDITOR.dom.node(c))&&d&&!d(c)&&(c=c.getPrevious(d));
return c
},getStyle:function(b){return this.$.style[CKEDITOR.tools.cssStyleToDomStyle(b)]
},is:function(){var d=this.getName();
if("object"==typeof arguments[0]){return !!arguments[0][d]
}for(var c=0;
c<arguments.length;
c++){if(arguments[c]==d){return !0
}}return !1
},isEditable:function(d){var c=this.getName();
return this.isReadOnly()||"none"==this.getComputedStyle("display")||"hidden"==this.getComputedStyle("visibility")||CKEDITOR.dtd.$nonEditable[c]||CKEDITOR.dtd.$empty[c]||this.is("a")&&(this.data("cke-saved-name")||this.hasAttribute("name"))&&!this.getChildCount()?!1:!1!==d?(d=CKEDITOR.dtd[c]||CKEDITOR.dtd.span,!(!d||!d["#"])):!0
},isIdentical:function(e){var d=this.clone(0,1);
e=e.clone(0,1);
d.removeAttributes(["_moz_dirty","data-cke-expando","data-cke-saved-href","data-cke-saved-name"]);
e.removeAttributes(["_moz_dirty","data-cke-expando","data-cke-saved-href","data-cke-saved-name"]);
if(d.$.isEqualNode){return d.$.style.cssText=CKEDITOR.tools.normalizeCssText(d.$.style.cssText),e.$.style.cssText=CKEDITOR.tools.normalizeCssText(e.$.style.cssText),d.$.isEqualNode(e.$)
}d=d.getOuterHtml();
e=e.getOuterHtml();
if(CKEDITOR.env.ie&&9>CKEDITOR.env.version&&this.is("a")){var f=this.getParent();
f.type==CKEDITOR.NODE_ELEMENT&&(f=f.clone(),f.setHtml(d),d=f.getHtml(),f.setHtml(e),e=f.getHtml())
}return d==e
},isVisible:function(){var e=(this.$.offsetHeight||this.$.offsetWidth)&&"hidden"!=this.getComputedStyle("visibility"),d,f;
e&&CKEDITOR.env.webkit&&(d=this.getWindow(),!d.equals(CKEDITOR.document.getWindow())&&(f=d.$.frameElement)&&(e=(new CKEDITOR.dom.element(f)).isVisible()));
return !!e
},isEmptyInlineRemoveable:function(){if(!CKEDITOR.dtd.$removeEmpty[this.getName()]){return !1
}for(var e=this.getChildren(),d=0,h=e.count();
d<h;
d++){var f=e.getItem(d);
if(f.type!=CKEDITOR.NODE_ELEMENT||!f.data("cke-bookmark")){if(f.type==CKEDITOR.NODE_ELEMENT&&!f.isEmptyInlineRemoveable()||f.type==CKEDITOR.NODE_TEXT&&CKEDITOR.tools.trim(f.getText())){return !1
}}}return !0
},hasAttributes:CKEDITOR.env.ie&&(CKEDITOR.env.ie7Compat||CKEDITOR.env.quirks)?function(){for(var e=this.$.attributes,d=0;
d<e.length;
d++){var f=e[d];
switch(f.nodeName){case"class":if(this.getAttribute("class")){return !0
}case"data-cke-expando":continue;
default:if(f.specified){return !0
}}}return !1
}:function(){var e=this.$.attributes,d=e.length,f={"data-cke-expando":1,_moz_dirty:1};
return 0<d&&(2<d||!f[e[0].nodeName]||2==d&&!f[e[1].nodeName])
},hasAttribute:function(){function b(a){var d=this.$.attributes.getNamedItem(a);
if("input"==this.getName()){switch(a){case"class":return 0<this.$.className.length;
case"checked":return !!this.$.checked;
case"value":return a=this.getAttribute("type"),"checkbox"==a||"radio"==a?"on"!=this.$.value:!!this.$.value
}}return d?d.specified:!1
}return CKEDITOR.env.ie?8>CKEDITOR.env.version?function(a){return"name"==a?!!this.$.name:b.call(this,a)
}:b:function(c){return !!this.$.attributes.getNamedItem(c)
}
}(),hide:function(){this.setStyle("display","none")
},moveChildren:function(e,d){var h=this.$;
e=e.$;
if(h!=e){var f;
if(d){for(;
f=h.lastChild;
){e.insertBefore(h.removeChild(f),e.firstChild)
}}else{for(;
f=h.firstChild;
){e.appendChild(h.removeChild(f))
}}}},mergeSiblings:function(){function b(a,o,f){if(o&&o.type==CKEDITOR.NODE_ELEMENT){for(var h=[];
o.data("cke-bookmark")||o.isEmptyInlineRemoveable();
){if(h.push(o),o=f?o.getNext():o.getPrevious(),!o||o.type!=CKEDITOR.NODE_ELEMENT){return
}}if(a.isIdentical(o)){for(var e=f?a.getLast():a.getFirst();
h.length;
){h.shift().move(a,!f)
}o.moveChildren(a,!f);
o.remove();
e&&e.type==CKEDITOR.NODE_ELEMENT&&e.mergeSiblings()
}}}return function(a){if(!1===a||CKEDITOR.dtd.$removeEmpty[this.getName()]||this.is("a")){b(this,this.getNext(),!0),b(this,this.getPrevious())
}}
}(),show:function(){this.setStyles({display:"",visibility:""})
},setAttribute:function(){var b=function(d,c){this.$.setAttribute(d,c);
return this
};
return CKEDITOR.env.ie&&(CKEDITOR.env.ie7Compat||CKEDITOR.env.quirks)?function(a,d){"class"==a?this.$.className=d:"style"==a?this.$.style.cssText=d:"tabindex"==a?this.$.tabIndex=d:"checked"==a?this.$.checked=d:"contenteditable"==a?b.call(this,"contentEditable",d):b.apply(this,arguments);
return this
}:CKEDITOR.env.ie8Compat&&CKEDITOR.env.secure?function(a,e){if("src"==a&&e.match(/^http:\/\//)){try{b.apply(this,arguments)
}catch(d){}}else{b.apply(this,arguments)
}return this
}:b
}(),setAttributes:function(d){for(var c in d){this.setAttribute(c,d[c])
}return this
},setValue:function(b){this.$.value=b;
return this
},removeAttribute:function(){var b=function(c){this.$.removeAttribute(c)
};
return CKEDITOR.env.ie&&(CKEDITOR.env.ie7Compat||CKEDITOR.env.quirks)?function(c){"class"==c?c="className":"tabindex"==c?c="tabIndex":"contenteditable"==c&&(c="contentEditable");
this.$.removeAttribute(c)
}:b
}(),removeAttributes:function(d){if(CKEDITOR.tools.isArray(d)){for(var c=0;
c<d.length;
c++){this.removeAttribute(d[c])
}}else{for(c in d){d.hasOwnProperty(c)&&this.removeAttribute(c)
}}},removeStyle:function(h){var f=this.$.style;
if(f.removeProperty||"border"!=h&&"margin"!=h&&"padding"!=h){f.removeProperty?f.removeProperty(h):f.removeAttribute(CKEDITOR.tools.cssStyleToDomStyle(h)),this.$.style.cssText||this.removeAttribute("style")
}else{var q=["top","left","right","bottom"],o;
"border"==h&&(o=["color","style","width"]);
for(var f=[],p=0;
p<q.length;
p++){if(o){for(var e=0;
e<o.length;
e++){f.push([h,q[p],o[e]].join("-"))
}}else{f.push([h,q[p]].join("-"))
}}for(h=0;
h<f.length;
h++){this.removeStyle(f[h])
}}},setStyle:function(d,c){this.$.style[CKEDITOR.tools.cssStyleToDomStyle(d)]=c;
return this
},setStyles:function(d){for(var c in d){this.setStyle(c,d[c])
}return this
},setOpacity:function(b){CKEDITOR.env.ie&&9>CKEDITOR.env.version?(b=Math.round(100*b),this.setStyle("filter",100<=b?"":"progid:DXImageTransform.Microsoft.Alpha(opacity\x3d"+b+")")):this.setStyle("opacity",b)
},unselectable:function(){this.setStyles(CKEDITOR.tools.cssVendorPrefix("user-select","none"));
if(CKEDITOR.env.ie){this.setAttribute("unselectable","on");
for(var e,d=this.getElementsByTag("*"),h=0,f=d.count();
h<f;
h++){e=d.getItem(h),e.setAttribute("unselectable","on")
}}},getPositionedAncestor:function(){for(var b=this;
"html"!=b.getName();
){if("static"!=b.getComputedStyle("position")){return b
}b=b.getParent()
}return null
},getDocumentPosition:function(z){var x=0,v=0,r=this.getDocument(),u=r.getBody(),o="BackCompat"==r.$.compatMode;
if(document.documentElement.getBoundingClientRect){var t=this.$.getBoundingClientRect(),s=r.$.documentElement,p=s.clientTop||u.$.clientTop||0,w=s.clientLeft||u.$.clientLeft||0,q=!0;
CKEDITOR.env.ie&&(q=r.getDocumentElement().contains(this),r=r.getBody().contains(this),q=o&&r||!o&&q);
q&&(CKEDITOR.env.webkit||CKEDITOR.env.ie&&12<=CKEDITOR.env.version?(x=u.$.scrollLeft||s.scrollLeft,v=u.$.scrollTop||s.scrollTop):(v=o?u.$:s,x=v.scrollLeft,v=v.scrollTop),x=t.left+x-w,v=t.top+v-p)
}else{for(p=this,w=null;
p&&"body"!=p.getName()&&"html"!=p.getName();
){x+=p.$.offsetLeft-p.$.scrollLeft;
v+=p.$.offsetTop-p.$.scrollTop;
p.equals(this)||(x+=p.$.clientLeft||0,v+=p.$.clientTop||0);
for(;
w&&!w.equals(p);
){x-=w.$.scrollLeft,v-=w.$.scrollTop,w=w.getParent()
}w=p;
p=(t=p.$.offsetParent)?new CKEDITOR.dom.element(t):null
}}z&&(t=this.getWindow(),p=z.getWindow(),!t.equals(p)&&t.$.frameElement&&(z=(new CKEDITOR.dom.element(t.$.frameElement)).getDocumentPosition(z),x+=z.x,v+=z.y));
document.documentElement.getBoundingClientRect||!CKEDITOR.env.gecko||o||(x+=this.$.clientLeft?1:0,v+=this.$.clientTop?1:0);
return{x:x,y:v}
},scrollIntoView:function(f){var e=this.getParent();
if(e){do{if((e.$.clientWidth&&e.$.clientWidth<e.$.scrollWidth||e.$.clientHeight&&e.$.clientHeight<e.$.scrollHeight)&&!e.is("body")&&this.scrollIntoParent(e,f,1),e.is("html")){var p=e.getWindow();
try{var h=p.$.frameElement;
h&&(e=new CKEDITOR.dom.element(h))
}catch(o){}}}while(e=e.getParent())
}},scrollIntoParent:function(E,D,B){var t,A,z,G;
function x(a,d){/body|html/.test(E.getName())?E.getWindow().$.scrollBy(a,d):(E.$.scrollLeft+=a,E.$.scrollTop+=d)
}function q(e,d){var h={x:0,y:0};
if(!e.is(s?"body":"html")){var f=e.$.getBoundingClientRect();
h.x=f.left;
h.y=f.top
}f=e.getWindow();
f.equals(d)||(f=q(CKEDITOR.dom.element.get(f.$.frameElement),d),h.x+=f.x,h.y+=f.y);
return h
}function C(d,c){return parseInt(d.getComputedStyle("margin-"+c)||0,10)||0
}!E&&(E=this.getWindow());
z=E.getDocument();
var s="BackCompat"==z.$.compatMode;
E instanceof CKEDITOR.dom.window&&(E=s?z.getBody():z.getDocumentElement());
z=E.getWindow();
A=q(this,z);
var F=q(E,z),H=this.$.offsetHeight;
t=this.$.offsetWidth;
var r=E.$.clientHeight,o=E.$.clientWidth;
z=A.x-C(this,"left")-F.x||0;
G=A.y-C(this,"top")-F.y||0;
t=A.x+t+C(this,"right")-(F.x+o)||0;
A=A.y+H+C(this,"bottom")-(F.y+r)||0;
(0>G||0<A)&&x(0,!0===D?G:!1===D?A:0>G?G:A);
B&&(0>z||0<t)&&x(0>z?z:t,0)
},setState:function(e,d,f){d=d||"cke";
switch(e){case CKEDITOR.TRISTATE_ON:this.addClass(d+"_on");
this.removeClass(d+"_off");
this.removeClass(d+"_disabled");
f&&this.setAttribute("aria-pressed",!0);
f&&this.removeAttribute("aria-disabled");
break;
case CKEDITOR.TRISTATE_DISABLED:this.addClass(d+"_disabled");
this.removeClass(d+"_off");
this.removeClass(d+"_on");
f&&this.setAttribute("aria-disabled",!0);
f&&this.removeAttribute("aria-pressed");
break;
default:this.addClass(d+"_off"),this.removeClass(d+"_on"),this.removeClass(d+"_disabled"),f&&this.removeAttribute("aria-pressed"),f&&this.removeAttribute("aria-disabled")
}},getFrameDocument:function(){var d=this.$;
try{d.contentWindow.document
}catch(c){d.src=d.src
}return d&&new CKEDITOR.dom.document(d.contentWindow.document)
},copyAttributes:function(h,f){var s=this.$.attributes;
f=f||{};
for(var p=0;
p<s.length;
p++){var r=s[p],q=r.nodeName.toLowerCase(),o;
if(!(q in f)){if("checked"==q&&(o=this.getAttribute(q))){h.setAttribute(q,o)
}else{if(!CKEDITOR.env.ie||this.hasAttribute(q)){o=this.getAttribute(q),null===o&&(o=r.nodeValue),h.setAttribute(q,o)
}}}}""!==this.$.style.cssText&&(h.$.style.cssText=this.$.style.cssText)
},renameNode:function(d){if(this.getName()!=d){var c=this.getDocument();
d=new CKEDITOR.dom.element(d,c);
this.copyAttributes(d);
this.moveChildren(d);
this.getParent(!0)&&this.$.parentNode.replaceChild(d.$,this.$);
d.$["data-cke-expando"]=this.$["data-cke-expando"];
this.$=d.$;
delete this.getName
}},getChild:function(){function b(a,e){var d=a.childNodes;
if(0<=e&&e<d.length){return d[e]
}}return function(a){var d=this.$;
if(a.slice){for(a=a.slice();
0<a.length&&d;
){d=b(d,a.shift())
}}else{d=b(d,a)
}return d?new CKEDITOR.dom.node(d):null
}
}(),getChildCount:function(){return this.$.childNodes.length
},disableContextMenu:function(){function b(a){return a.type==CKEDITOR.NODE_ELEMENT&&a.hasClass("cke_enable_context_menu")
}this.on("contextmenu",function(a){a.data.getTarget().getAscendant(b,!0)||a.data.preventDefault()
})
},getDirection:function(b){return b?this.getComputedStyle("direction")||this.getDirection()||this.getParent()&&this.getParent().getDirection(1)||this.getDocument().$.dir||"ltr":this.getStyle("direction")||this.getAttribute("dir")
},data:function(d,c){d="data-"+d;
if(void 0===c){return this.getAttribute(d)
}!1===c?this.removeAttribute(d):this.setAttribute(d,c);
return null
},getEditor:function(){var e=CKEDITOR.instances,d,f;
for(d in e){if(f=e[d],f.element.equals(this)&&f.elementMode!=CKEDITOR.ELEMENT_MODE_APPENDTO){return f
}}return null
},find:function(b){var d=m(this);
b=new CKEDITOR.dom.nodeList(this.$.querySelectorAll(g(this,b)));
d();
return b
},findOne:function(b){var d=m(this);
b=this.$.querySelector(g(this,b));
d();
return b?new CKEDITOR.dom.element(b):null
},forEach:function(f,e,p){if(!(p||e&&this.type!=e)){var h=f(this)
}if(!1!==h){p=this.getChildren();
for(var o=0;
o<p.count();
o++){h=p.getItem(o),h.type==CKEDITOR.NODE_ELEMENT?h.forEach(f,e):e&&h.type!=e||f(h)
}}}});
var j={width:["border-left-width","border-right-width","padding-left","padding-right"],height:["border-top-width","border-bottom-width","padding-top","padding-bottom"]};
CKEDITOR.dom.element.prototype.setSize=function(e,c,f){"number"==typeof c&&(!f||CKEDITOR.env.ie&&CKEDITOR.env.quirks||(c-=n.call(this,e)),this.setStyle(e,c+"px"))
};
CKEDITOR.dom.element.prototype.getSize=function(e,c){var f=Math.max(this.$["offset"+CKEDITOR.tools.capitalize(e)],this.$["client"+CKEDITOR.tools.capitalize(e)])||0;
c&&(f-=n.call(this,e));
return f
}
})();
CKEDITOR.dom.documentFragment=function(b){b=b||CKEDITOR.document;
this.$=b.type==CKEDITOR.NODE_DOCUMENT?b.$.createDocumentFragment():b
};
CKEDITOR.tools.extend(CKEDITOR.dom.documentFragment.prototype,CKEDITOR.dom.element.prototype,{type:CKEDITOR.NODE_DOCUMENT_FRAGMENT,insertAfterNode:function(b){b=b.$;
b.parentNode.insertBefore(this.$,b.nextSibling)
},getHtml:function(){var b=new CKEDITOR.dom.element("div");
this.clone(1,1).appendTo(b);
return b.getHtml().replace(/\s*data-cke-expando=".*?"/g,"")
}},!0,{append:1,appendBogus:1,clone:1,getFirst:1,getHtml:1,getLast:1,getParent:1,getNext:1,getPrevious:1,appendTo:1,moveChildren:1,insertBefore:1,insertAfterNode:1,replace:1,trim:1,type:1,ltrim:1,rtrim:1,getDocument:1,getChildCount:1,getChild:1,getChildren:1});
(function(){function v(K,J){var H=this.range;
if(this._.end){return null
}if(!this._.start){this._.start=1;
if(H.collapsed){return this.end(),null
}H.optimize()
}var B,F=H.startContainer;
B=H.endContainer;
var E=H.startOffset,I=H.endOffset,M,y=this.guard,t=this.type,x=K?"getPreviousSourceNode":"getNextSourceNode";
if(!K&&!this._.guardLTR){var q=B.type==CKEDITOR.NODE_ELEMENT?B:B.getParent(),k=B.type==CKEDITOR.NODE_ELEMENT?B.getChild(I):B.getNext();
this._.guardLTR=function(d,c){return(!c||!q.equals(d))&&(!k||!d.equals(k))&&(d.type!=CKEDITOR.NODE_ELEMENT||!c||!d.equals(H.root))
}
}if(K&&!this._.guardRTL){var L=F.type==CKEDITOR.NODE_ELEMENT?F:F.getParent(),C=F.type==CKEDITOR.NODE_ELEMENT?E?F.getChild(E-1):null:F.getPrevious();
this._.guardRTL=function(d,c){return(!c||!L.equals(d))&&(!C||!d.equals(C))&&(d.type!=CKEDITOR.NODE_ELEMENT||!c||!d.equals(H.root))
}
}var A=K?this._.guardRTL:this._.guardLTR;
M=y?function(d,c){return !1===A(d,c)?!1:y(d,c)
}:A;
this.current?B=this.current[x](!1,t,M):(K?B.type==CKEDITOR.NODE_ELEMENT&&(B=0<I?B.getChild(I-1):!1===M(B,!0)?null:B.getPreviousSourceNode(!0,t,M)):(B=F,B.type==CKEDITOR.NODE_ELEMENT&&((B=B.getChild(E))||(B=!1===M(F,!0)?null:F.getNextSourceNode(!0,t,M)))),B&&!1===M(B)&&(B=null));
for(;
B&&!this._.end;
){this.current=B;
if(!this.evaluator||!1!==this.evaluator(B)){if(!J){return B
}}else{if(J&&this.evaluator){return !1
}}B=B[x](!1,t,M)
}this.end();
return this.current=null
}function p(a){for(var e,d=null;
e=v.call(this,a);
){d=e
}return d
}CKEDITOR.dom.walker=CKEDITOR.tools.createClass({$:function(b){this.range=b;
this._={}
},proto:{end:function(){this._.end=1
},next:function(){return v.call(this)
},previous:function(){return v.call(this,1)
},checkForward:function(){return !1!==v.call(this,0,1)
},checkBackward:function(){return !1!==v.call(this,1,1)
},lastForward:function(){return p.call(this)
},lastBackward:function(){return p.call(this,1)
},reset:function(){delete this.current;
this._={}
}}});
var u={block:1,"list-item":1,table:1,"table-row-group":1,"table-header-group":1,"table-footer-group":1,"table-row":1,"table-column-group":1,"table-column":1,"table-cell":1,"table-caption":1},s={absolute:1,fixed:1};
CKEDITOR.dom.element.prototype.isBlockBoundary=function(b){return"none"!=this.getComputedStyle("float")||this.getComputedStyle("position") in s||!u[this.getComputedStyle("display")]?!!(this.is(CKEDITOR.dtd.$block)||b&&this.is(b)):!0
};
CKEDITOR.dom.walker.blockBoundary=function(b){return function(a){return !(a.type==CKEDITOR.NODE_ELEMENT&&a.isBlockBoundary(b))
}
};
CKEDITOR.dom.walker.listItemBoundary=function(){return this.blockBoundary({br:1})
};
CKEDITOR.dom.walker.bookmark=function(e,d){function f(b){return b&&b.getName&&"span"==b.getName()&&b.data("cke-bookmark")
}return function(a){var c,b;
c=a&&a.type!=CKEDITOR.NODE_ELEMENT&&(b=a.getParent())&&f(b);
c=e?c:c||f(a);
return !!(d^c)
}
};
CKEDITOR.dom.walker.whitespaces=function(b){return function(a){var d;
a&&a.type==CKEDITOR.NODE_TEXT&&(d=!CKEDITOR.tools.trim(a.getText())||CKEDITOR.env.webkit&&""==a.getText());
return !!(b^d)
}
};
CKEDITOR.dom.walker.invisible=function(e){var d=CKEDITOR.dom.walker.whitespaces(),f=CKEDITOR.env.webkit?1:0;
return function(a){d(a)?a=1:(a.type==CKEDITOR.NODE_TEXT&&(a=a.getParent()),a=a.$.offsetWidth<=f);
return !!(e^a)
}
};
CKEDITOR.dom.walker.nodeType=function(d,c){return function(a){return !!(c^a.type==d)
}
};
CKEDITOR.dom.walker.bogus=function(d){function c(b){return !n(b)&&!l(b)
}return function(b){var a=CKEDITOR.env.needsBrFiller?b.is&&b.is("br"):b.getText&&o.test(b.getText());
a&&(a=b.getParent(),b=b.getNext(c),a=a.isBlockBoundary()&&(!b||b.type==CKEDITOR.NODE_ELEMENT&&b.isBlockBoundary()));
return !!(d^a)
}
};
CKEDITOR.dom.walker.temp=function(b){return function(a){a.type!=CKEDITOR.NODE_ELEMENT&&(a=a.getParent());
a=a&&a.hasAttribute("data-cke-temp");
return !!(b^a)
}
};
var o=/^[\t\r\n ]*(?:&nbsp;|\xa0)$/,n=CKEDITOR.dom.walker.whitespaces(),l=CKEDITOR.dom.walker.bookmark(),j=CKEDITOR.dom.walker.temp(),i=function(b){return l(b)||n(b)||b.type==CKEDITOR.NODE_ELEMENT&&b.is(CKEDITOR.dtd.$inline)&&!b.is(CKEDITOR.dtd.$empty)
};
CKEDITOR.dom.walker.ignored=function(b){return function(a){a=n(a)||l(a)||j(a);
return !!(b^a)
}
};
var w=CKEDITOR.dom.walker.ignored();
CKEDITOR.dom.walker.empty=function(b){return function(a){for(var e=0,d=a.getChildCount();
e<d;
++e){if(!w(a.getChild(e))){return !!b
}}return !b
}
};
var m=CKEDITOR.dom.walker.empty(),r=CKEDITOR.dom.walker.validEmptyBlockContainers=CKEDITOR.tools.extend(function(e){var d={},f;
for(f in e){CKEDITOR.dtd[f]["#"]&&(d[f]=1)
}return d
}(CKEDITOR.dtd.$block),{caption:1,td:1,th:1});
CKEDITOR.dom.walker.editable=function(b){return function(a){a=w(a)?!1:a.type==CKEDITOR.NODE_TEXT||a.type==CKEDITOR.NODE_ELEMENT&&(a.is(CKEDITOR.dtd.$inline)||a.is("hr")||"false"==a.getAttribute("contenteditable")||!CKEDITOR.env.needsBrFiller&&a.is(r)&&m(a))?!0:!1;
return !!(b^a)
}
};
CKEDITOR.dom.element.prototype.getBogus=function(){var b=this;
do{b=b.getPreviousSourceNode()
}while(i(b));
return b&&(CKEDITOR.env.needsBrFiller?b.is&&b.is("br"):b.getText&&o.test(b.getText()))?b:!1
}
})();
CKEDITOR.dom.range=function(b){this.endOffset=this.endContainer=this.startOffset=this.startContainer=null;
this.collapsed=!0;
var c=b instanceof CKEDITOR.dom.document;
this.document=c?b:b.getDocument();
this.root=c?b.getBody():b
};
(function(){function r(b){b.collapsed=b.startContainer&&b.endContainer&&b.startContainer.equals(b.endContainer)&&b.startOffset==b.endOffset
}function n(ao,an,am,al,ak){function ae(f,e,q,h){var k=q?f.getPrevious():f.getNext();
if(h&&ah){return k
}ag||h?e.append(f.clone(!0,ak),q):(f.remove(),T&&e.append(f));
return k
}function aj(){var e,d,h,f=Math.min(N.length,Q.length);
for(e=0;
e<f;
e++){if(d=N[e],h=Q[e],!d.equals(h)){return e
}}return e-1
}function ai(){var a=M-1,d=F&&V&&!ac.equals(af);
a<U-1||a<C-1||d?(d?ao.moveToPosition(af,CKEDITOR.POSITION_BEFORE_START):C==a+1&&ab?ao.moveToPosition(Q[a],CKEDITOR.POSITION_BEFORE_END):ao.moveToPosition(Q[a+1],CKEDITOR.POSITION_BEFORE_START),al&&(a=N[a+1])&&a.type==CKEDITOR.NODE_ELEMENT&&(d=CKEDITOR.dom.element.createFromHtml('\x3cspan data-cke-bookmark\x3d"1" style\x3d"display:none"\x3e\x26nbsp;\x3c/span\x3e',ao.document),d.insertAfter(a),a.mergeSiblings(!1),ao.moveToBookmark({startNode:d}))):ao.collapse(!0)
}ao.optimizeBookmark();
var ah=0===an,T=1==an,ag=2==an;
an=ag||T;
var ac=ao.startContainer,af=ao.endContainer,X=ao.startOffset,Y=ao.endOffset,R,ab,F,V,Z,w;
if(ag&&af.type==CKEDITOR.NODE_TEXT&&ac.equals(af)){ac=ao.document.createText(ac.substring(X,Y)),am.append(ac)
}else{af.type==CKEDITOR.NODE_TEXT?ag?w=!0:af=af.split(Y):0<af.getChildCount()?Y>=af.getChildCount()?(af=af.getChild(Y-1),ab=!0):af=af.getChild(Y):V=ab=!0;
ac.type==CKEDITOR.NODE_TEXT?ag?Z=!0:ac.split(X):0<ac.getChildCount()?0===X?(ac=ac.getChild(X),R=!0):ac=ac.getChild(X-1):F=R=!0;
for(var N=ac.getParents(),Q=af.getParents(),M=aj(),U=N.length-1,C=Q.length-1,z=am,A,ap,v,ad=-1,y=M;
y<=U;
y++){ap=N[y];
v=ap.getNext();
for(y!=U||ap.equals(Q[y])&&U<C?an&&(A=z.append(ap.clone(0,ak))):R?ae(ap,z,!1,F):Z&&z.append(ao.document.createText(ap.substring(X)));
v;
){if(v.equals(Q[y])){ad=y;
break
}v=ae(v,z)
}z=A
}z=am;
for(y=M;
y<=C;
y++){if(am=Q[y],v=am.getPrevious(),am.equals(N[y])){an&&(z=z.getChild(0))
}else{y!=C||am.equals(N[y])&&C<U?an&&(A=z.append(am.clone(0,ak))):ab?ae(am,z,!1,V):w&&z.append(ao.document.createText(am.substring(0,Y)));
if(y>ad){for(;
v;
){v=ae(v,z,!0)
}}z=A
}}ag||ai()
}}function p(){var f=!1,e=CKEDITOR.dom.walker.whitespaces(),k=CKEDITOR.dom.walker.bookmark(!0),h=CKEDITOR.dom.walker.bogus();
return function(a){return k(a)||e(a)?!0:h(a)&&!f?f=!0:a.type==CKEDITOR.NODE_TEXT&&(a.hasAscendant("pre")||CKEDITOR.tools.trim(a.getText()).length)||a.type==CKEDITOR.NODE_ELEMENT&&!a.is(l)?!1:!0
}
}function o(e){var d=CKEDITOR.dom.walker.whitespaces(),f=CKEDITOR.dom.walker.bookmark(1);
return function(a){return f(a)||d(a)?!0:!e&&j(a)||a.type==CKEDITOR.NODE_ELEMENT&&a.is(CKEDITOR.dtd.$removeEmpty)
}
}function m(b){return function(){var a;
return this[b?"getPreviousNode":"getNextNode"](function(c){!a&&s(c)&&(a=c);
return g(c)&&!(j(c)&&c.equals(a))
})
}
}var l={abbr:1,acronym:1,b:1,bdo:1,big:1,cite:1,code:1,del:1,dfn:1,em:1,font:1,i:1,ins:1,label:1,kbd:1,q:1,samp:1,small:1,span:1,strike:1,strong:1,sub:1,sup:1,tt:1,u:1,"var":1},j=CKEDITOR.dom.walker.bogus(),i=/^[\t\r\n ]*(?:&nbsp;|\xa0)$/,g=CKEDITOR.dom.walker.editable(),s=CKEDITOR.dom.walker.ignored(!0);
CKEDITOR.dom.range.prototype={clone:function(){var b=new CKEDITOR.dom.range(this.root);
b._setStartContainer(this.startContainer);
b.startOffset=this.startOffset;
b._setEndContainer(this.endContainer);
b.endOffset=this.endOffset;
b.collapsed=this.collapsed;
return b
},collapse:function(b){b?(this._setEndContainer(this.startContainer),this.endOffset=this.startOffset):(this._setStartContainer(this.endContainer),this.startOffset=this.endOffset);
this.collapsed=!0
},cloneContents:function(d){var c=new CKEDITOR.dom.documentFragment(this.document);
this.collapsed||n(this,2,c,!1,"undefined"==typeof d?!0:d);
return c
},deleteContents:function(b){this.collapsed||n(this,0,null,b)
},extractContents:function(e,d){var f=new CKEDITOR.dom.documentFragment(this.document);
this.collapsed||n(this,1,f,e,"undefined"==typeof d?!0:d);
return f
},createBookmark:function(h){var f,u,q,k,t=this.collapsed;
f=this.document.createElement("span");
f.data("cke-bookmark",1);
f.setStyle("display","none");
f.setHtml("\x26nbsp;");
h&&(q="cke_bm_"+CKEDITOR.tools.getNextNumber(),f.setAttribute("id",q+(t?"C":"S")));
t||(u=f.clone(),u.setHtml("\x26nbsp;"),h&&u.setAttribute("id",q+"E"),k=this.clone(),k.collapse(),k.insertNode(u));
k=this.clone();
k.collapse(!0);
k.insertNode(f);
u?(this.setStartAfter(f),this.setEndBefore(u)):this.moveToPosition(f,CKEDITOR.POSITION_AFTER_END);
return{startNode:h?q+(t?"C":"S"):f,endNode:h?q+"E":u,serializable:h,collapsed:t}
},createBookmark2:function(){function d(q){var a=q.container,k=q.offset,h;
h=a;
var b=k;
h=h.type!=CKEDITOR.NODE_ELEMENT||0===b||b==h.getChildCount()?0:h.getChild(b-1).type==CKEDITOR.NODE_TEXT&&h.getChild(b).type==CKEDITOR.NODE_TEXT;
h&&(a=a.getChild(k-1),k=a.getLength());
a.type==CKEDITOR.NODE_ELEMENT&&1<k&&(k=a.getChild(k-1).getIndex(!0)+1);
if(a.type==CKEDITOR.NODE_TEXT){h=a;
for(b=0;
(h=h.getPrevious())&&h.type==CKEDITOR.NODE_TEXT;
){b+=h.getLength()
}h=b;
a.getText()?k+=h:(b=a.getPrevious(c),h?(k=h,a=b?b.getNext():a.getParent().getFirst()):(a=a.getParent(),k=b?b.getIndex(!0)+1:0))
}q.container=a;
q.offset=k
}var c=CKEDITOR.dom.walker.nodeType(CKEDITOR.NODE_TEXT,!0);
return function(a){var k=this.collapsed,h={container:this.startContainer,offset:this.startOffset},f={container:this.endContainer,offset:this.endOffset};
a&&(d(h),k||d(f));
return{start:h.container.getAddress(a),end:k?null:f.container.getAddress(a),startOffset:h.offset,endOffset:f.offset,normalized:a,collapsed:k,is2:!0}
}
}(),moveToBookmark:function(f){if(f.is2){var e=this.document.getByAddress(f.start,f.normalized),k=f.startOffset,h=f.end&&this.document.getByAddress(f.end,f.normalized);
f=f.endOffset;
this.setStart(e,k);
h?this.setEnd(h,f):this.collapse(!0)
}else{e=(k=f.serializable)?this.document.getById(f.startNode):f.startNode,f=k?this.document.getById(f.endNode):f.endNode,this.setStartBefore(e),e.remove(),f?(this.setEndBefore(f),f.remove()):this.collapse(!0)
}},getBoundaryNodes:function(){var h=this.startContainer,f=this.endContainer,t=this.startOffset,q=this.endOffset,k;
if(h.type==CKEDITOR.NODE_ELEMENT){if(k=h.getChildCount(),k>t){h=h.getChild(t)
}else{if(1>k){h=h.getPreviousSourceNode()
}else{for(h=h.$;
h.lastChild;
){h=h.lastChild
}h=new CKEDITOR.dom.node(h);
h=h.getNextSourceNode()||h
}}}if(f.type==CKEDITOR.NODE_ELEMENT){if(k=f.getChildCount(),k>q){f=f.getChild(q).getPreviousSourceNode(!0)
}else{if(1>k){f=f.getPreviousSourceNode()
}else{for(f=f.$;
f.lastChild;
){f=f.lastChild
}f=new CKEDITOR.dom.node(f)
}}}h.getPosition(f)&CKEDITOR.POSITION_FOLLOWING&&(h=f);
return{startNode:h,endNode:f}
},getCommonAncestor:function(f,e){var k=this.startContainer,h=this.endContainer,k=k.equals(h)?f&&k.type==CKEDITOR.NODE_ELEMENT&&this.startOffset==this.endOffset-1?k.getChild(this.startOffset):k:k.getCommonAncestor(h);
return e&&!k.is?k.getParent():k
},optimize:function(){var d=this.startContainer,c=this.startOffset;
d.type!=CKEDITOR.NODE_ELEMENT&&(c?c>=d.getLength()&&this.setStartAfter(d):this.setStartBefore(d));
d=this.endContainer;
c=this.endOffset;
d.type!=CKEDITOR.NODE_ELEMENT&&(c?c>=d.getLength()&&this.setEndAfter(d):this.setEndBefore(d))
},optimizeBookmark:function(){var d=this.startContainer,c=this.endContainer;
d.is&&d.is("span")&&d.data("cke-bookmark")&&this.setStartAt(d,CKEDITOR.POSITION_BEFORE_START);
c&&c.is&&c.is("span")&&c.data("cke-bookmark")&&this.setEndAt(c,CKEDITOR.POSITION_AFTER_END)
},trim:function(h,f){var u=this.startContainer,q=this.startOffset,k=this.collapsed;
if((!h||k)&&u&&u.type==CKEDITOR.NODE_TEXT){if(q){if(q>=u.getLength()){q=u.getIndex()+1,u=u.getParent()
}else{var t=u.split(q),q=u.getIndex()+1,u=u.getParent();
this.startContainer.equals(this.endContainer)?this.setEnd(t,this.endOffset-this.startOffset):u.equals(this.endContainer)&&(this.endOffset+=1)
}}else{q=u.getIndex(),u=u.getParent()
}this.setStart(u,q);
if(k){this.collapse(!0);
return
}}u=this.endContainer;
q=this.endOffset;
f||k||!u||u.type!=CKEDITOR.NODE_TEXT||(q?(q>=u.getLength()||u.split(q),q=u.getIndex()+1):q=u.getIndex(),u=u.getParent(),this.setEnd(u,q))
},enlarge:function(ae,ad){function ac(b){return b&&b.type==CKEDITOR.NODE_ELEMENT&&b.hasAttribute("contenteditable")?null:b
}var ab=new RegExp(/[^\s\ufeff]/);
switch(ae){case CKEDITOR.ENLARGE_INLINE:var aa=1;
case CKEDITOR.ENLARGE_ELEMENT:var U=function(e,d){var h=new CKEDITOR.dom.range(Y);
h.setStart(e,d);
h.setEndAt(Y,CKEDITOR.POSITION_BEFORE_END);
var h=new CKEDITOR.dom.walker(h),f;
for(h.guard=function(b){return !(b.type==CKEDITOR.NODE_ELEMENT&&b.isBlockBoundary())
};
f=h.next();
){if(f.type!=CKEDITOR.NODE_TEXT){return !1
}F=f!=e?f.getText():f.substring(d);
if(ab.test(F)){return !1
}}return !0
};
if(this.collapsed){break
}var Z=this.getCommonAncestor(),Y=this.root,X,J,W,T,V,O=!1,P,F;
P=this.startContainer;
var R=this.startOffset;
P.type==CKEDITOR.NODE_TEXT?(R&&(P=!CKEDITOR.tools.trim(P.substring(0,R)).length&&P,O=!!P),P&&((T=P.getPrevious())||(W=P.getParent()))):(R&&(T=P.getChild(R-1)||P.getLast()),T||(W=P));
for(W=ac(W);
W||T;
){if(W&&!T){!V&&W.equals(Z)&&(V=!0);
if(aa?W.isBlockBoundary():!Y.contains(W)){break
}O&&"inline"==W.getComputedStyle("display")||(O=!1,V?X=W:this.setStartBefore(W));
T=W.getPrevious()
}for(;
T;
){if(P=!1,T.type==CKEDITOR.NODE_COMMENT){T=T.getPrevious()
}else{if(T.type==CKEDITOR.NODE_TEXT){F=T.getText(),ab.test(F)&&(T=null),P=/[\s\ufeff]$/.test(F)
}else{if((T.$.offsetWidth>(CKEDITOR.env.webkit?1:0)||ad&&T.is("br"))&&!T.data("cke-bookmark")){if(O&&CKEDITOR.dtd.$removeEmpty[T.getName()]){F=T.getText();
if(ab.test(F)){T=null
}else{for(var R=T.$.getElementsByTagName("*"),N=0,Q;
Q=R[N++];
){if(!CKEDITOR.dtd.$removeEmpty[Q.nodeName.toLowerCase()]){T=null;
break
}}}T&&(P=!!F.length)
}else{T=null
}}}P&&(O?V?X=W:W&&this.setStartBefore(W):O=!0);
if(T){P=T.getPrevious();
if(!W&&!P){W=T;
T=null;
break
}T=P
}else{W=null
}}}W&&(W=ac(W.getParent()))
}P=this.endContainer;
R=this.endOffset;
W=T=null;
V=O=!1;
P.type==CKEDITOR.NODE_TEXT?CKEDITOR.tools.trim(P.substring(R)).length?O=!0:(O=!P.getLength(),R==P.getLength()?(T=P.getNext())||(W=P.getParent()):U(P,R)&&(W=P.getParent())):(T=P.getChild(R))||(W=P);
for(;
W||T;
){if(W&&!T){!V&&W.equals(Z)&&(V=!0);
if(aa?W.isBlockBoundary():!Y.contains(W)){break
}O&&"inline"==W.getComputedStyle("display")||(O=!1,V?J=W:W&&this.setEndAfter(W));
T=W.getNext()
}for(;
T;
){P=!1;
if(T.type==CKEDITOR.NODE_TEXT){F=T.getText(),U(T,0)||(T=null),P=/^[\s\ufeff]/.test(F)
}else{if(T.type==CKEDITOR.NODE_ELEMENT){if((0<T.$.offsetWidth||ad&&T.is("br"))&&!T.data("cke-bookmark")){if(O&&CKEDITOR.dtd.$removeEmpty[T.getName()]){F=T.getText();
if(ab.test(F)){T=null
}else{for(R=T.$.getElementsByTagName("*"),N=0;
Q=R[N++];
){if(!CKEDITOR.dtd.$removeEmpty[Q.nodeName.toLowerCase()]){T=null;
break
}}}T&&(P=!!F.length)
}else{T=null
}}}else{P=1
}}P&&O&&(V?J=W:this.setEndAfter(W));
if(T){P=T.getNext();
if(!W&&!P){W=T;
T=null;
break
}T=P
}else{W=null
}}W&&(W=ac(W.getParent()))
}X&&J&&(Z=X.contains(J)?J:X,this.setStartBefore(Z),this.setEndAfter(Z));
break;
case CKEDITOR.ENLARGE_BLOCK_CONTENTS:case CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS:W=new CKEDITOR.dom.range(this.root);
Y=this.root;
W.setStartAt(Y,CKEDITOR.POSITION_AFTER_START);
W.setEnd(this.startContainer,this.startOffset);
W=new CKEDITOR.dom.walker(W);
var w,v,A=CKEDITOR.dom.walker.blockBoundary(ae==CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS?{br:1}:null),C=null,z=function(d){if(d.type==CKEDITOR.NODE_ELEMENT&&"false"==d.getAttribute("contenteditable")){if(C){if(C.equals(d)){C=null;
return
}}else{C=d
}}else{if(C){return
}}var c=A(d);
c||(w=d);
return c
},aa=function(d){var c=z(d);
!c&&d.is&&d.is("br")&&(v=d);
return c
};
W.guard=z;
W=W.lastBackward();
w=w||Y;
this.setStartAt(w,!w.is("br")&&(!W&&this.checkStartOfBlock()||W&&w.contains(W))?CKEDITOR.POSITION_AFTER_START:CKEDITOR.POSITION_AFTER_END);
if(ae==CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS){W=this.clone();
W=new CKEDITOR.dom.walker(W);
var L=CKEDITOR.dom.walker.whitespaces(),y=CKEDITOR.dom.walker.bookmark();
W.evaluator=function(b){return !L(b)&&!y(b)
};
if((W=W.previous())&&W.type==CKEDITOR.NODE_ELEMENT&&W.is("br")){break
}}W=this.clone();
W.collapse();
W.setEndAt(Y,CKEDITOR.POSITION_BEFORE_END);
W=new CKEDITOR.dom.walker(W);
W.guard=ae==CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS?aa:z;
w=C=v=null;
W=W.lastForward();
w=w||Y;
this.setEndAt(w,!W&&this.checkEndOfBlock()||W&&w.contains(W)?CKEDITOR.POSITION_BEFORE_END:CKEDITOR.POSITION_BEFORE_START);
v&&this.setEndAfter(v)
}},shrink:function(D,C,B){if(!this.collapsed){D=D||CKEDITOR.SHRINK_TEXT;
var A=this.clone(),z=this.startContainer,t=this.endContainer,y=this.startOffset,x=this.endOffset,w=1,E=1;
z&&z.type==CKEDITOR.NODE_TEXT&&(y?y>=z.getLength()?A.setStartAfter(z):(A.setStartBefore(z),w=0):A.setStartBefore(z));
t&&t.type==CKEDITOR.NODE_TEXT&&(x?x>=t.getLength()?A.setEndAfter(t):(A.setEndAfter(t),E=0):A.setEndBefore(t));
var A=new CKEDITOR.dom.walker(A),v=CKEDITOR.dom.walker.bookmark();
A.evaluator=function(a){return a.type==(D==CKEDITOR.SHRINK_ELEMENT?CKEDITOR.NODE_ELEMENT:CKEDITOR.NODE_TEXT)
};
var q;
A.guard=function(a,c){if(v(a)){return !0
}if(D==CKEDITOR.SHRINK_ELEMENT&&a.type==CKEDITOR.NODE_TEXT||c&&a.equals(q)||!1===B&&a.type==CKEDITOR.NODE_ELEMENT&&a.isBlockBoundary()||a.type==CKEDITOR.NODE_ELEMENT&&a.hasAttribute("contenteditable")){return !1
}c||a.type!=CKEDITOR.NODE_ELEMENT||(q=a);
return !0
};
w&&(z=A[D==CKEDITOR.SHRINK_ELEMENT?"lastForward":"next"]())&&this.setStartAt(z,C?CKEDITOR.POSITION_AFTER_START:CKEDITOR.POSITION_BEFORE_START);
E&&(A.reset(),(A=A[D==CKEDITOR.SHRINK_ELEMENT?"lastBackward":"previous"]())&&this.setEndAt(A,C?CKEDITOR.POSITION_BEFORE_END:CKEDITOR.POSITION_AFTER_END));
return !(!w&&!E)
}},insertNode:function(e){this.optimizeBookmark();
this.trim(!1,!0);
var d=this.startContainer,f=d.getChild(this.startOffset);
f?e.insertBefore(f):d.append(e);
e.getParent()&&e.getParent().equals(this.endContainer)&&this.endOffset++;
this.setStartBefore(e)
},moveToPosition:function(d,c){this.setStartAt(d,c);
this.collapse(!0)
},moveToRange:function(b){this.setStart(b.startContainer,b.startOffset);
this.setEnd(b.endContainer,b.endOffset)
},selectNodeContents:function(b){this.setStart(b,0);
this.setEnd(b,b.type==CKEDITOR.NODE_TEXT?b.getLength():b.getChildCount())
},setStart:function(a,d){a.type==CKEDITOR.NODE_ELEMENT&&CKEDITOR.dtd.$empty[a.getName()]&&(d=a.getIndex(),a=a.getParent());
this._setStartContainer(a);
this.startOffset=d;
this.endContainer||(this._setEndContainer(a),this.endOffset=d);
r(this)
},setEnd:function(a,d){a.type==CKEDITOR.NODE_ELEMENT&&CKEDITOR.dtd.$empty[a.getName()]&&(d=a.getIndex()+1,a=a.getParent());
this._setEndContainer(a);
this.endOffset=d;
this.startContainer||(this._setStartContainer(a),this.startOffset=d);
r(this)
},setStartAfter:function(b){this.setStart(b.getParent(),b.getIndex()+1)
},setStartBefore:function(b){this.setStart(b.getParent(),b.getIndex())
},setEndAfter:function(b){this.setEnd(b.getParent(),b.getIndex()+1)
},setEndBefore:function(b){this.setEnd(b.getParent(),b.getIndex())
},setStartAt:function(a,d){switch(d){case CKEDITOR.POSITION_AFTER_START:this.setStart(a,0);
break;
case CKEDITOR.POSITION_BEFORE_END:a.type==CKEDITOR.NODE_TEXT?this.setStart(a,a.getLength()):this.setStart(a,a.getChildCount());
break;
case CKEDITOR.POSITION_BEFORE_START:this.setStartBefore(a);
break;
case CKEDITOR.POSITION_AFTER_END:this.setStartAfter(a)
}r(this)
},setEndAt:function(a,d){switch(d){case CKEDITOR.POSITION_AFTER_START:this.setEnd(a,0);
break;
case CKEDITOR.POSITION_BEFORE_END:a.type==CKEDITOR.NODE_TEXT?this.setEnd(a,a.getLength()):this.setEnd(a,a.getChildCount());
break;
case CKEDITOR.POSITION_BEFORE_START:this.setEndBefore(a);
break;
case CKEDITOR.POSITION_AFTER_END:this.setEndAfter(a)
}r(this)
},fixBlock:function(h,f){var t=this.createBookmark(),q=this.document.createElement(f);
this.collapse(h);
this.enlarge(CKEDITOR.ENLARGE_BLOCK_CONTENTS);
this.extractContents().appendTo(q);
q.trim();
this.insertNode(q);
var k=q.getBogus();
k&&k.remove();
q.appendBogus();
this.moveToBookmark(t);
return q
},splitBlock:function(k,h){var w=new CKEDITOR.dom.elementPath(this.startContainer,this.root),u=new CKEDITOR.dom.elementPath(this.endContainer,this.root),t=w.block,v=u.block,q=null;
if(!w.blockLimit.equals(u.blockLimit)){return null
}"br"!=k&&(t||(t=this.fixBlock(!0,k),v=(new CKEDITOR.dom.elementPath(this.endContainer,this.root)).block),v||(v=this.fixBlock(!1,k)));
w=t&&this.checkStartOfBlock();
u=v&&this.checkEndOfBlock();
this.deleteContents();
t&&t.equals(v)&&(u?(q=new CKEDITOR.dom.elementPath(this.startContainer,this.root),this.moveToPosition(v,CKEDITOR.POSITION_AFTER_END),v=null):w?(q=new CKEDITOR.dom.elementPath(this.startContainer,this.root),this.moveToPosition(t,CKEDITOR.POSITION_BEFORE_START),t=null):(v=this.splitElement(t,h||!1),t.is("ul","ol")||t.appendBogus()));
return{previousBlock:t,nextBlock:v,wasStartOfBlock:w,wasEndOfBlock:u,elementPath:q}
},splitElement:function(f,e){if(!this.collapsed){return null
}this.setEndAt(f,CKEDITOR.POSITION_BEFORE_END);
var k=this.extractContents(!1,e||!1),h=f.clone(!1,e||!1);
k.appendTo(h);
h.insertAfter(f);
this.moveToPosition(f,CKEDITOR.POSITION_AFTER_END);
return h
},removeEmptyBlocksAtEnd:function(){function e(a){return function(b){return d(b)||f(b)||b.type==CKEDITOR.NODE_ELEMENT&&b.isEmptyInlineRemoveable()||a.is("table")&&b.is("caption")?!1:!0
}
}var d=CKEDITOR.dom.walker.whitespaces(),f=CKEDITOR.dom.walker.bookmark(!1);
return function(a){for(var t=this.createBookmark(),q=this[a?"endPath":"startPath"](),k=q.block||q.blockLimit,h;
k&&!k.equals(q.root)&&!k.getFirst(e(k));
){h=k.getParent(),this[a?"setEndAt":"setStartAt"](k,CKEDITOR.POSITION_AFTER_END),k.remove(1),k=h
}this.moveToBookmark(t)
}
}(),startPath:function(){return new CKEDITOR.dom.elementPath(this.startContainer,this.root)
},endPath:function(){return new CKEDITOR.dom.elementPath(this.endContainer,this.root)
},checkBoundaryOfElement:function(f,c){var k=c==CKEDITOR.START,h=this.clone();
h.collapse(k);
h[k?"setStartAt":"setEndAt"](f,k?CKEDITOR.POSITION_AFTER_START:CKEDITOR.POSITION_BEFORE_END);
h=new CKEDITOR.dom.walker(h);
h.evaluator=o(k);
return h[k?"checkBackward":"checkForward"]()
},checkStartOfBlock:function(){var b=this.startContainer,d=this.startOffset;
CKEDITOR.env.ie&&d&&b.type==CKEDITOR.NODE_TEXT&&(b=CKEDITOR.tools.ltrim(b.substring(0,d)),i.test(b)&&this.trim(0,1));
this.trim();
b=new CKEDITOR.dom.elementPath(this.startContainer,this.root);
d=this.clone();
d.collapse(!0);
d.setStartAt(b.block||b.blockLimit,CKEDITOR.POSITION_AFTER_START);
b=new CKEDITOR.dom.walker(d);
b.evaluator=p();
return b.checkBackward()
},checkEndOfBlock:function(){var b=this.endContainer,d=this.endOffset;
CKEDITOR.env.ie&&b.type==CKEDITOR.NODE_TEXT&&(b=CKEDITOR.tools.rtrim(b.substring(d)),i.test(b)&&this.trim(1,0));
this.trim();
b=new CKEDITOR.dom.elementPath(this.endContainer,this.root);
d=this.clone();
d.collapse(!1);
d.setEndAt(b.block||b.blockLimit,CKEDITOR.POSITION_BEFORE_END);
b=new CKEDITOR.dom.walker(d);
b.evaluator=p();
return b.checkForward()
},getPreviousNode:function(f,e,k){var h=this.clone();
h.collapse(1);
h.setStartAt(k||this.root,CKEDITOR.POSITION_AFTER_START);
k=new CKEDITOR.dom.walker(h);
k.evaluator=f;
k.guard=e;
return k.previous()
},getNextNode:function(f,e,k){var h=this.clone();
h.collapse();
h.setEndAt(k||this.root,CKEDITOR.POSITION_BEFORE_END);
k=new CKEDITOR.dom.walker(h);
k.evaluator=f;
k.guard=e;
return k.next()
},checkReadOnly:function(){function b(a,d){for(;
a;
){if(a.type==CKEDITOR.NODE_ELEMENT){if("false"==a.getAttribute("contentEditable")&&!a.data("cke-editable")){return 0
}if(a.is("html")||"true"==a.getAttribute("contentEditable")&&(a.contains(d)||a.equals(d))){break
}}a=a.getParent()
}return 1
}return function(){var a=this.startContainer,d=this.endContainer;
return !(b(a,d)&&b(d,a))
}
}(),moveToElementEditablePosition:function(h,f){if(h.type==CKEDITOR.NODE_ELEMENT&&!h.isEditable(!1)){return this.moveToPosition(h,f?CKEDITOR.POSITION_AFTER_END:CKEDITOR.POSITION_BEFORE_START),!0
}for(var u=0;
h;
){if(h.type==CKEDITOR.NODE_TEXT){f&&this.endContainer&&this.checkEndOfBlock()&&i.test(h.getText())?this.moveToPosition(h,CKEDITOR.POSITION_BEFORE_START):this.moveToPosition(h,f?CKEDITOR.POSITION_AFTER_END:CKEDITOR.POSITION_BEFORE_START);
u=1;
break
}if(h.type==CKEDITOR.NODE_ELEMENT){if(h.isEditable()){this.moveToPosition(h,f?CKEDITOR.POSITION_BEFORE_END:CKEDITOR.POSITION_AFTER_START),u=1
}else{if(f&&h.is("br")&&this.endContainer&&this.checkEndOfBlock()){this.moveToPosition(h,CKEDITOR.POSITION_BEFORE_START)
}else{if("false"==h.getAttribute("contenteditable")&&h.is(CKEDITOR.dtd.$block)){return this.setStartBefore(h),this.setEndAfter(h),!0
}}}}var q=h,k=u,t=void 0;
q.type==CKEDITOR.NODE_ELEMENT&&q.isEditable(!1)&&(t=q[f?"getLast":"getFirst"](s));
k||t||(t=q[f?"getPrevious":"getNext"](s));
h=t
}return !!u
},moveToClosestEditablePosition:function(k,h){var w,u=0,t,v,q=[CKEDITOR.POSITION_AFTER_END,CKEDITOR.POSITION_BEFORE_START];
k?(w=new CKEDITOR.dom.range(this.root),w.moveToPosition(k,q[h?0:1])):w=this.clone();
if(k&&!k.is(CKEDITOR.dtd.$block)){u=1
}else{if(t=w[h?"getNextEditableNode":"getPreviousEditableNode"]()){u=1,(v=t.type==CKEDITOR.NODE_ELEMENT)&&t.is(CKEDITOR.dtd.$block)&&"false"==t.getAttribute("contenteditable")?(w.setStartAt(t,CKEDITOR.POSITION_BEFORE_START),w.setEndAt(t,CKEDITOR.POSITION_AFTER_END)):!CKEDITOR.env.needsBrFiller&&v&&t.is(CKEDITOR.dom.walker.validEmptyBlockContainers)?(w.setEnd(t,0),w.collapse()):w.moveToPosition(t,q[h?1:0])
}}u&&this.moveToRange(w);
return !!u
},moveToElementEditStart:function(b){return this.moveToElementEditablePosition(b)
},moveToElementEditEnd:function(b){return this.moveToElementEditablePosition(b,!0)
},getEnclosedNode:function(){var f=this.clone();
f.optimize();
if(f.startContainer.type!=CKEDITOR.NODE_ELEMENT||f.endContainer.type!=CKEDITOR.NODE_ELEMENT){return null
}var f=new CKEDITOR.dom.walker(f),e=CKEDITOR.dom.walker.bookmark(!1,!0),k=CKEDITOR.dom.walker.whitespaces(!0);
f.evaluator=function(b){return k(b)&&e(b)
};
var h=f.next();
f.reset();
return h&&h.equals(f.previous())?h:null
},getTouchedStartNode:function(){var b=this.startContainer;
return this.collapsed||b.type!=CKEDITOR.NODE_ELEMENT?b:b.getChild(this.startOffset)||b
},getTouchedEndNode:function(){var b=this.endContainer;
return this.collapsed||b.type!=CKEDITOR.NODE_ELEMENT?b:b.getChild(this.endOffset-1)||b
},getNextEditableNode:m(),getPreviousEditableNode:m(1),scrollIntoView:function(){var h=new CKEDITOR.dom.element.createFromHtml("\x3cspan\x3e\x26nbsp;\x3c/span\x3e",this.document),f,t,q,k=this.clone();
k.optimize();
(q=k.startContainer.type==CKEDITOR.NODE_TEXT)?(t=k.startContainer.getText(),f=k.startContainer.split(k.startOffset),h.insertAfter(k.startContainer)):k.insertNode(h);
h.scrollIntoView();
q&&(k.startContainer.setText(t),f.remove());
h.remove()
},_setStartContainer:function(b){this.startContainer=b
},_setEndContainer:function(b){this.endContainer=b
}}
})();
CKEDITOR.POSITION_AFTER_START=1;
CKEDITOR.POSITION_BEFORE_END=2;
CKEDITOR.POSITION_BEFORE_START=3;
CKEDITOR.POSITION_AFTER_END=4;
CKEDITOR.ENLARGE_ELEMENT=1;
CKEDITOR.ENLARGE_BLOCK_CONTENTS=2;
CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS=3;
CKEDITOR.ENLARGE_INLINE=4;
CKEDITOR.START=1;
CKEDITOR.END=2;
CKEDITOR.SHRINK_ELEMENT=1;
CKEDITOR.SHRINK_TEXT=2;
"use strict";
(function(){function r(b){1>arguments.length||(this.range=b,this.forceBrBreak=0,this.enlargeBr=1,this.enforceRealBlocks=0,this._||(this._={}))
}function n(d){var c=[];
d.forEach(function(b){if("true"==b.getAttribute("contenteditable")){return c.push(b),!1
}},CKEDITOR.NODE_ELEMENT,!0);
return c
}function p(b,s,q,k){b:{null==k&&(k=n(q));
for(var d;
d=k.shift();
){if(d.getDtd().p){k={element:d,remaining:k};
break b
}}k=null
}if(!k){return 0
}if((d=CKEDITOR.filter.instances[k.element.data("cke-filter")])&&!d.check(s)){return p(b,s,q,k.remaining)
}s=new CKEDITOR.dom.range(k.element);
s.selectNodeContents(k.element);
s=s.createIterator();
s.enlargeBr=b.enlargeBr;
s.enforceRealBlocks=b.enforceRealBlocks;
s.activeFilter=s.filter=d;
b._.nestedEditable={element:k.element,container:q,remaining:k.remaining,iterator:s};
return 1
}function o(e,d,f){if(!d){return !1
}e=e.clone();
e.collapse(!f);
return e.checkBoundaryOfElement(d,f?CKEDITOR.START:CKEDITOR.END)
}var m=/^[\r\n\t ]+$/,l=CKEDITOR.dom.walker.bookmark(!1,!0),j=CKEDITOR.dom.walker.whitespaces(!0),i=function(b){return l(b)&&j(b)
},g={dd:1,dt:1,li:1};
r.prototype={getNextParagraph:function(x){var s,q,e,B,b;
x=x||"p";
if(this._.nestedEditable){if(s=this._.nestedEditable.iterator.getNextParagraph(x)){return this.activeFilter=this._.nestedEditable.iterator.activeFilter,s
}this.activeFilter=this.filter;
if(p(this,x,this._.nestedEditable.container,this._.nestedEditable.remaining)){return this.activeFilter=this._.nestedEditable.iterator.activeFilter,this._.nestedEditable.iterator.getNextParagraph(x)
}this._.nestedEditable=null
}if(!this.range.root.getDtd()[x]){return null
}if(!this._.started){var f=this.range.clone();
q=f.startPath();
var t=f.endPath(),E=!f.collapsed&&o(f,q.block),z=!f.collapsed&&o(f,t.block,1);
f.shrink(CKEDITOR.SHRINK_ELEMENT,!0);
E&&f.setStartAt(q.block,CKEDITOR.POSITION_BEFORE_END);
z&&f.setEndAt(t.block,CKEDITOR.POSITION_AFTER_START);
q=f.endContainer.hasAscendant("pre",!0)||f.startContainer.hasAscendant("pre",!0);
f.enlarge(this.forceBrBreak&&!q||!this.enlargeBr?CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS:CKEDITOR.ENLARGE_BLOCK_CONTENTS);
f.collapsed||(q=new CKEDITOR.dom.walker(f.clone()),t=CKEDITOR.dom.walker.bookmark(!0,!0),q.evaluator=t,this._.nextNode=q.next(),q=new CKEDITOR.dom.walker(f.clone()),q.evaluator=t,q=q.previous(),this._.lastNode=q.getNextSourceNode(!0,null,f.root),this._.lastNode&&this._.lastNode.type==CKEDITOR.NODE_TEXT&&!CKEDITOR.tools.trim(this._.lastNode.getText())&&this._.lastNode.getParent().isBlockBoundary()&&(t=this.range.clone(),t.moveToPosition(this._.lastNode,CKEDITOR.POSITION_AFTER_END),t.checkEndOfBlock()&&(t=new CKEDITOR.dom.elementPath(t.endContainer,t.root),this._.lastNode=(t.block||t.blockLimit).getNextSourceNode(!0))),this._.lastNode&&f.root.contains(this._.lastNode)||(this._.lastNode=this._.docEndMarker=f.document.createText(""),this._.lastNode.insertAfter(q)),f=null);
this._.started=1;
q=f
}t=this._.nextNode;
f=this._.lastNode;
for(this._.nextNode=null;
t;
){var E=0,z=t.hasAscendant("pre"),D=t.type!=CKEDITOR.NODE_ELEMENT,k=0;
if(D){t.type==CKEDITOR.NODE_TEXT&&m.test(t.getText())&&(D=0)
}else{var c=t.getName();
if(CKEDITOR.dtd.$block[c]&&"false"==t.getAttribute("contenteditable")){s=t;
p(this,x,s);
break
}else{if(t.isBlockBoundary(this.forceBrBreak&&!z&&{br:1})){if("br"==c){D=1
}else{if(!q&&!t.getChildCount()&&"hr"!=c){s=t;
e=t.equals(f);
break
}}q&&(q.setEndAt(t,CKEDITOR.POSITION_BEFORE_START),"br"!=c&&(this._.nextNode=t));
E=1
}else{if(t.getFirst()){q||(q=this.range.clone(),q.setStartAt(t,CKEDITOR.POSITION_BEFORE_START));
t=t.getFirst();
continue
}D=1
}}}D&&!q&&(q=this.range.clone(),q.setStartAt(t,CKEDITOR.POSITION_BEFORE_START));
e=(!E||D)&&t.equals(f);
if(q&&!E){for(;
!t.getNext(i)&&!e;
){c=t.getParent();
if(c.isBlockBoundary(this.forceBrBreak&&!z&&{br:1})){E=1;
D=0;
e||c.equals(f);
q.setEndAt(c,CKEDITOR.POSITION_BEFORE_END);
break
}t=c;
D=1;
e=t.equals(f);
k=1
}}D&&q.setEndAt(t,CKEDITOR.POSITION_AFTER_END);
t=this._getNextSourceNode(t,k,f);
if((e=!t)||E&&q){break
}}if(!s){if(!q){return this._.docEndMarker&&this._.docEndMarker.remove(),this._.nextNode=null
}s=new CKEDITOR.dom.elementPath(q.startContainer,q.root);
t=s.blockLimit;
E={div:1,th:1,td:1};
s=s.block;
!s&&t&&!this.enforceRealBlocks&&E[t.getName()]&&q.checkStartOfBlock()&&q.checkEndOfBlock()&&!t.equals(q.root)?s=t:!s||this.enforceRealBlocks&&s.is(g)?(s=this.range.document.createElement(x),q.extractContents().appendTo(s),s.trim(),q.insertNode(s),B=b=!0):"li"!=s.getName()?q.checkStartOfBlock()&&q.checkEndOfBlock()||(s=s.clone(!1),q.extractContents().appendTo(s),s.trim(),b=q.splitBlock(),B=!b.wasStartOfBlock,b=!b.wasEndOfBlock,q.insertNode(s)):e||(this._.nextNode=s.equals(f)?null:this._getNextSourceNode(q.getBoundaryNodes().endNode,1,f))
}B&&(B=s.getPrevious())&&B.type==CKEDITOR.NODE_ELEMENT&&("br"==B.getName()?B.remove():B.getLast()&&"br"==B.getLast().$.nodeName.toLowerCase()&&B.getLast().remove());
b&&(B=s.getLast())&&B.type==CKEDITOR.NODE_ELEMENT&&"br"==B.getName()&&(!CKEDITOR.env.needsBrFiller||B.getPrevious(l)||B.getNext(l))&&B.remove();
this._.nextNode||(this._.nextNode=e||s.equals(f)||!f?null:this._getNextSourceNode(s,1,f));
return s
},_getNextSourceNode:function(h,f,s){function q(b){return !(b.equals(s)||b.equals(k))
}var k=this.range.root;
for(h=h.getNextSourceNode(f,null,q);
!l(h);
){h=h.getNextSourceNode(f,null,q)
}return h
}};
CKEDITOR.dom.range.prototype.createIterator=function(){return new r(this)
}
})();
CKEDITOR.command=function(e,f){this.uiItems=[];
this.exec=function(a){if(this.state==CKEDITOR.TRISTATE_DISABLED||!this.checkAllowed()){return !1
}this.editorFocus&&e.focus();
return !1===this.fire("exec")?!0:!1!==f.exec.call(this,e,a)
};
this.refresh=function(g,d){if(!this.readOnly&&g.readOnly){return !0
}if(this.context&&!d.isContextFor(this.context)||!this.checkAllowed(!0)){return this.disable(),!0
}this.startDisabled||this.enable();
this.modes&&!this.modes[g.mode]&&this.disable();
return !1===this.fire("refresh",{editor:g,path:d})?!0:f.refresh&&!1!==f.refresh.apply(this,arguments)
};
var c;
this.checkAllowed=function(a){return a||"boolean"!=typeof c?c=e.activeFilter.checkFeature(this):c
};
CKEDITOR.tools.extend(this,f,{modes:{wysiwyg:1},editorFocus:1,contextSensitive:!!f.context,state:CKEDITOR.TRISTATE_DISABLED});
CKEDITOR.event.call(this)
};
CKEDITOR.command.prototype={enable:function(){this.state==CKEDITOR.TRISTATE_DISABLED&&this.checkAllowed()&&this.setState(this.preserveState&&"undefined"!=typeof this.previousState?this.previousState:CKEDITOR.TRISTATE_OFF)
},disable:function(){this.setState(CKEDITOR.TRISTATE_DISABLED)
},setState:function(b){if(this.state==b||b!=CKEDITOR.TRISTATE_DISABLED&&!this.checkAllowed()){return !1
}this.previousState=this.state;
this.state=b;
this.fire("state");
return !0
},toggleState:function(){this.state==CKEDITOR.TRISTATE_OFF?this.setState(CKEDITOR.TRISTATE_ON):this.state==CKEDITOR.TRISTATE_ON&&this.setState(CKEDITOR.TRISTATE_OFF)
}};
CKEDITOR.event.implementOn(CKEDITOR.command.prototype);
CKEDITOR.ENTER_P=1;
CKEDITOR.ENTER_BR=2;
CKEDITOR.ENTER_DIV=3;
CKEDITOR.config={customConfig:"config.js",autoUpdateElement:!0,language:"",defaultLanguage:"en",contentsLangDirection:"",enterMode:CKEDITOR.ENTER_P,forceEnterMode:!1,shiftEnterMode:CKEDITOR.ENTER_BR,docType:"\x3c!DOCTYPE html\x3e",bodyId:"",bodyClass:"",fullPage:!1,height:200,extraPlugins:"",removePlugins:"",protectedSource:[],tabIndex:0,width:"",baseFloatZIndex:10000,blockedKeystrokes:[CKEDITOR.CTRL+66,CKEDITOR.CTRL+73,CKEDITOR.CTRL+85]};
(function(){function aw(S,O,M,J,B){var I,y;
S=[];
for(I in O){y=O[I];
y="boolean"==typeof y?{}:"function"==typeof y?{match:y}:N(y);
"$"!=I.charAt(0)&&(y.elements=I);
M&&(y.featureName=M.toLowerCase());
var A=y;
A.elements=ao(A.elements,/\s+/)||null;
A.propertiesOnly=A.propertiesOnly||!0===A.elements;
var x=/\s*,\s*/,z=void 0;
for(z in j){A[z]=ao(A[z],x)||null;
var v=A,t=ac[z],H=ao(A[ac[z]],x),w=A[z],F=[],q=!0,h=void 0;
H?q=!1:H={};
for(h in w){"!"==h.charAt(0)&&(h=h.slice(1),F.push(h),H[h]=!0,q=!1)
}for(;
h=F.pop();
){w[h]=w["!"+h],delete w["!"+h]
}v[t]=(q?!1:H)||null
}A.match=A.match||null;
J.push(y);
S.push(y)
}O=B.elements;
B=B.generic;
var C;
M=0;
for(J=S.length;
M<J;
++M){I=N(S[M]);
y=!0===I.classes||!0===I.styles||!0===I.attributes;
A=I;
z=t=x=void 0;
for(x in j){A[x]=U(A[x])
}v=!0;
for(z in ac){x=ac[z];
t=A[x];
H=[];
w=void 0;
for(w in t){-1<w.indexOf("*")?H.push(new RegExp("^"+w.replace(/\*/g,".*")+"$")):H.push(w)
}t=H;
t.length&&(A[x]=t,v=!1)
}A.nothingRequired=v;
A.noProperties=!(A.attributes||A.classes||A.styles);
if(!0===I.elements||null===I.elements){B[y?"unshift":"push"](I)
}else{for(C in A=I.elements,delete I.elements,A){if(O[C]){O[C][y?"unshift":"push"](I)
}else{O[C]=[I]
}}}}}function at(g,q,n,k){if(!g.match||g.match(q)){if(k||an(g,q)){if(g.propertiesOnly||(n.valid=!0),n.allAttributes||(n.allAttributes=av(g.attributes,q.attributes,n.validAttributes)),n.allStyles||(n.allStyles=av(g.styles,q.styles,n.validStyles)),!n.allClasses){g=g.classes;
q=q.classes;
k=n.validClasses;
if(g){if(!0===g){g=!0
}else{for(var f=0,h=q.length,b;
f<h;
++f){b=q[f],k[b]||(k[b]=g(b))
}g=!1
}}else{g=!1
}n.allClasses=g
}}}}function av(f,e,h){if(!f){return !1
}if(!0===f){return !0
}for(var g in e){h[g]||(h[g]=f(g))
}return !1
}function au(g,e,m){if(!g.match||g.match(e)){if(g.noProperties){return !1
}m.hadInvalidAttribute=ar(g.attributes,e.attributes)||m.hadInvalidAttribute;
m.hadInvalidStyle=ar(g.styles,e.styles)||m.hadInvalidStyle;
g=g.classes;
e=e.classes;
if(g){for(var k=!1,f=!0===g,h=e.length;
h--;
){if(f||g(e[h])){e.splice(h,1),k=!0
}}g=k
}else{g=!1
}m.hadInvalidClass=g||m.hadInvalidClass
}}function ar(g,f){if(!g){return !1
}var l=!1,k=!0===g,h;
for(h in f){if(k||g(h)){delete f[h],l=!0
}}return l
}function aq(e,d,f){if(e.disabled||e.customConfig&&!f||!d){return !1
}e._.cachedChecks={};
return !0
}function ao(g,f){if(!g){return !1
}if(!0===g){return g
}if("string"==typeof g){return g=i(g),"*"==g?!0:CKEDITOR.tools.convertArrayToObject(g.split(f))
}if(CKEDITOR.tools.isArray(g)){return g.length?CKEDITOR.tools.convertArrayToObject(g):!1
}var l={},k=0,h;
for(h in g){l[h]=g[h],k++
}return k?l:!1
}function an(h,f){if(h.nothingRequired){return !0
}var n,m,k,g;
if(k=h.requiredClasses){for(g=f.classes,n=0;
n<k.length;
++n){if(m=k[n],"string"==typeof m){if(-1==CKEDITOR.tools.indexOf(g,m)){return !1
}}else{if(!CKEDITOR.tools.checkIfAnyArrayItemMatches(g,m)){return !1
}}}}return ai(f.styles,h.requiredStyles)&&ai(f.attributes,h.requiredAttributes)
}function ai(f,e){if(!e){return !0
}for(var h=0,g;
h<e.length;
++h){if(g=e[h],"string"==typeof g){if(!(g in f)){return !1
}}else{if(!CKEDITOR.tools.checkIfAnyObjectPropertyMatches(f,g)){return !1
}}}return !0
}function ag(d){if(!d){return{}
}d=d.split(/\s*,\s*/).sort();
for(var c={};
d.length;
){c[d.shift()]="cke-test"
}return c
}function ap(h){var f,q,n,m,g={},k=1;
for(h=i(h);
f=h.match(Q);
){(q=f[2])?(n=aa(q,"styles"),m=aa(q,"attrs"),q=aa(q,"classes")):n=m=q=null,g["$"+k++]={elements:f[1],classes:q,styles:n,attributes:m},h=h.slice(f[0].length)
}return g
}function aa(e,d){var f=e.match(P[d]);
return f?i(f[1]):null
}function Z(e){var d=e.styleBackup=e.attributes.style,f=e.classBackup=e.attributes["class"];
e.styles||(e.styles=CKEDITOR.tools.parseCssText(d||"",1));
e.classes||(e.classes=f?f.split(/\s+/):[])
}function ae(ax,T,S,C){var A=0,y;
C.toHtml&&(T.name=T.name.replace(V,"$1"));
if(C.doCallbacks&&ax.elementCallbacks){ax:{y=ax.elementCallbacks;
for(var v=0,M=y.length,w;
v<M;
++v){if(w=y[v](T)){y=w;
break ax
}}y=void 0
}if(y){return y
}}if(C.doTransform&&(y=ax._.transformations[T.name])){Z(T);
for(v=0;
v<y.length;
++v){aj(ax,T,y[v])
}ak(T)
}if(C.doFilter){ax:{v=T.name;
M=ax._;
ax=M.allowedRules.elements[v];
y=M.allowedRules.generic;
v=M.disallowedRules.elements[v];
M=M.disallowedRules.generic;
w=C.skipRequired;
var K={valid:!1,validAttributes:{},validClasses:{},validStyles:{},allAttributes:!1,allClasses:!1,allStyles:!1,hadInvalidAttribute:!1,hadInvalidClass:!1,hadInvalidStyle:!1},p,F;
if(ax||y){Z(T);
if(v){for(p=0,F=v.length;
p<F;
++p){if(!1===au(v[p],T,K)){ax=null;
break ax
}}}if(M){for(p=0,F=M.length;
p<F;
++p){au(M[p],T,K)
}}if(ax){for(p=0,F=ax.length;
p<F;
++p){at(ax[p],T,K,w)
}}if(y){for(p=0,F=y.length;
p<F;
++p){at(y[p],T,K,w)
}}ax=K
}else{ax=null
}}if(!ax||!ax.valid){return S.push(T),1
}F=ax.validAttributes;
var t=ax.validStyles;
y=ax.validClasses;
var v=T.attributes,E=T.styles,M=T.classes;
w=T.classBackup;
var n=T.styleBackup,z,q,c=[],K=[],d=/^data-cke-/;
p=!1;
delete v.style;
delete v["class"];
delete T.classBackup;
delete T.styleBackup;
if(!ax.allAttributes){for(z in v){F[z]||(d.test(z)?z==(q=z.replace(/^data-cke-saved-/,""))||F[q]||(delete v[z],p=!0):(delete v[z],p=!0))
}}if(!ax.allStyles||ax.hadInvalidStyle){for(z in E){ax.allStyles||t[z]?c.push(z+":"+E[z]):p=!0
}c.length&&(v.style=c.sort().join("; "))
}else{n&&(v.style=n)
}if(!ax.allClasses||ax.hadInvalidClass){for(z=0;
z<M.length;
++z){(ax.allClasses||y[M[z]])&&K.push(M[z])
}K.length&&(v["class"]=K.sort().join(" "));
w&&K.length<w.split(/\s+/).length&&(p=!0)
}else{w&&(v["class"]=w)
}p&&(A=1);
if(!C.skipFinalValidation&&!ab(T)){return S.push(T),1
}}C.toHtml&&(T.name=T.name.replace(L,"cke:$1"));
return A
}function X(e){var d=[],f;
for(f in e){-1<f.indexOf("*")&&d.push(f.replace(/\*/g,".*"))
}return d.length?new RegExp("^(?:"+d.join("|")+")$"):null
}function ak(e){var d=e.attributes,f;
delete d.style;
delete d["class"];
if(f=CKEDITOR.tools.writeCssText(e.styles,!0)){d.style=f
}e.classes.length&&(d["class"]=e.classes.sort().join(" "))
}function ab(b){switch(b.name){case"a":if(!(b.children.length||b.attributes.name||b.attributes.id)){return !1
}break;
case"img":if(!b.attributes.src){return !1
}}return !0
}function U(d){if(!d){return !1
}if(!0===d){return !0
}var c=X(d);
return function(a){return a in d||c&&a.match(c)
}
}function ad(){return new CKEDITOR.htmlParser.element("br")
}function af(b){return b.type==CKEDITOR.NODE_ELEMENT&&("br"==b.name||Y.$block[b.name])
}function am(x,w,v){var u=x.name;
if(Y.$empty[u]||!x.children.length){"hr"==u&&"br"==w?x.replaceWith(ad()):(x.parent&&v.push({check:"it",el:x.parent}),x.remove())
}else{if(Y.$block[u]||"tr"==u){if("br"==w){x.previous&&!af(x.previous)&&(w=ad(),w.insertBefore(x)),x.next&&!af(x.next)&&(w=ad(),w.insertAfter(x)),x.replaceWithChildren()
}else{var u=x.children,t;
w:{t=Y[w];
for(var q=0,g=u.length,k;
q<g;
++q){if(k=u[q],k.type==CKEDITOR.NODE_ELEMENT&&!t[k.name]){t=!1;
break w
}}t=!0
}if(t){x.name=w,x.attributes={},v.push({check:"parent-down",el:x})
}else{t=x.parent;
for(var q=t.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT||"body"==t.name,f,h,g=u.length;
0<g;
){k=u[--g],q&&(k.type==CKEDITOR.NODE_TEXT||k.type==CKEDITOR.NODE_ELEMENT&&Y.$inline[k.name])?(f||(f=new CKEDITOR.htmlParser.element(w),f.insertAfter(x),v.push({check:"parent-down",el:f})),f.add(k,0)):(f=null,h=Y[t.name]||Y.span,k.insertAfter(x),t.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT||k.type!=CKEDITOR.NODE_ELEMENT||h[k.name]||v.push({check:"el-up",el:k}))
}x.remove()
}}}else{u in {style:1,script:1}?x.remove():(x.parent&&v.push({check:"it",el:x.parent}),x.replaceWithChildren())
}}}function aj(g,f,l){var k,h;
for(k=0;
k<l.length;
++k){if(h=l[k],!(h.check&&!g.check(h.check,!1)||h.left&&!h.left(f))){h.right(f,o);
break
}}}function al(v,u){var t=u.getDefinition(),q=t.attributes,n=t.styles,k,g,h,f;
if(v.name!=t.element){return !1
}for(k in q){if("class"==k){for(t=q[k].split(/\s+/),h=v.classes.join("|");
f=t.pop();
){if(-1==h.indexOf(f)){return !1
}}}else{if(v.attributes[k]!=q[k]){return !1
}}}for(g in n){if(v.styles[g]!=n[g]){return !1
}}return !0
}function W(f,e){var h,g;
"string"==typeof f?h=f:f instanceof CKEDITOR.style?g=f:(h=f[0],g=f[1]);
return[{element:h,left:g,right:function(b,d){d.transform(b,e)
}}]
}function ah(b){return function(a){return al(a,b)
}
}function R(b){return function(a,d){d[b](a)
}
}var Y=CKEDITOR.dtd,N=CKEDITOR.tools.copy,i=CKEDITOR.tools.trim,s=["","p","br","div"];
CKEDITOR.FILTER_SKIP_TREE=2;
CKEDITOR.filter=function(d){this.allowedContent=[];
this.disallowedContent=[];
this.elementCallbacks=null;
this.disabled=!1;
this.editor=null;
this.id=CKEDITOR.tools.getNextNumber();
this._={allowedRules:{elements:{},generic:[]},disallowedRules:{elements:{},generic:[]},transformations:{},cachedTests:{}};
CKEDITOR.filter.instances[this.id]=this;
if(d instanceof CKEDITOR.editor){d=this.editor=d;
this.customConfig=!0;
var c=d.config.allowedContent;
!0===c?this.disabled=!0:(c||(this.customConfig=!1),this.allow(c,"config",1),this.allow(d.config.extraAllowedContent,"extra",1),this.allow(s[d.enterMode]+" "+s[d.shiftEnterMode],"default",1),this.disallow(d.config.disallowedContent))
}else{this.customConfig=!1,this.allow(d,"default",1)
}};
CKEDITOR.filter.instances={};
CKEDITOR.filter.prototype={allow:function(a,k,h){if(!aq(this,a,h)){return !1
}var g,f;
if("string"==typeof a){a=ap(a)
}else{if(a instanceof CKEDITOR.style){if(a.toAllowedContentRules){return this.allow(a.toAllowedContentRules(this.editor),k,h)
}g=a.getDefinition();
a={};
h=g.attributes;
a[g.element]=g={styles:g.styles,requiredStyles:g.styles&&CKEDITOR.tools.objectKeys(g.styles)};
h&&(h=N(h),g.classes=h["class"]?h["class"].split(/\s+/):null,g.requiredClasses=g.classes,delete h["class"],g.attributes=h,g.requiredAttributes=h&&CKEDITOR.tools.objectKeys(h))
}else{if(CKEDITOR.tools.isArray(a)){for(g=0;
g<a.length;
++g){f=this.allow(a[g],k,h)
}return f
}}}aw(this,a,k,this.allowedContent,this._.allowedRules);
return !0
},applyTo:function(y,x,w,v){if(this.disabled){return !1
}var t=this,h=[],l=this.editor&&this.editor.config.protectedSource,g,z=!1,k={doFilter:!w,doTransform:!0,doCallbacks:!0,toHtml:x};
y.forEach(function(e){if(e.type==CKEDITOR.NODE_ELEMENT){if("off"==e.attributes["data-cke-filter"]){return !1
}if(!x||"span"!=e.name||!~CKEDITOR.tools.objectKeys(e.attributes).join("|").indexOf("data-cke-")){if(g=ae(t,e,h,k),g&1){z=!0
}else{if(g&2){return !1
}}}}else{if(e.type==CKEDITOR.NODE_COMMENT&&e.value.match(/^\{cke_protected\}(?!\{C\})/)){var r;
e:{var p=decodeURIComponent(e.value.replace(/^\{cke_protected\}/,""));
r=[];
var b,n,m;
if(l){for(n=0;
n<l.length;
++n){if((m=p.match(l[n]))&&m[0].length==p.length){r=!0;
break e
}}}p=CKEDITOR.htmlParser.fragment.fromHtml(p);
1==p.children.length&&(b=p.children[0]).type==CKEDITOR.NODE_ELEMENT&&ae(t,b,r,k);
r=!r.length
}r||h.push(e)
}}},null,!0);
h.length&&(z=!0);
var q;
y=[];
v=s[v||(this.editor?this.editor.enterMode:CKEDITOR.ENTER_P)];
for(var A;
w=h.pop();
){w.type==CKEDITOR.NODE_ELEMENT?am(w,v,y):w.remove()
}for(;
q=y.pop();
){if(w=q.el,w.parent){switch(A=Y[w.parent.name]||Y.span,q.check){case"it":Y.$removeEmpty[w.name]&&!w.children.length?am(w,v,y):ab(w)||am(w,v,y);
break;
case"el-up":w.parent.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT||A[w.name]||am(w,v,y);
break;
case"parent-down":w.parent.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT||A[w.name]||am(w.parent,v,y)
}}}return z
},checkFeature:function(b){if(this.disabled||!b){return !0
}b.toFeature&&(b=b.toFeature(this.editor));
return !b.requiredContent||this.check(b.requiredContent)
},disable:function(){this.disabled=!0
},disallow:function(a){if(!aq(this,a,!0)){return !1
}"string"==typeof a&&(a=ap(a));
aw(this,a,null,this.disallowedContent,this._.disallowedRules);
return !0
},addContentForms:function(g){if(!this.disabled&&g){var f,l,k=[],h;
for(f=0;
f<g.length&&!h;
++f){l=g[f],("string"==typeof l||l instanceof CKEDITOR.style)&&this.check(l)&&(h=l)
}if(h){for(f=0;
f<g.length;
++f){k.push(W(g[f],h))
}this.addTransformations(k)
}}},addElementCallback:function(b){this.elementCallbacks||(this.elementCallbacks=[]);
this.elementCallbacks.push(b)
},addFeature:function(b){if(this.disabled||!b){return !0
}b.toFeature&&(b=b.toFeature(this.editor));
this.allow(b.allowedContent,b.name);
this.addTransformations(b.contentTransformations);
this.addContentForms(b.contentForms);
return b.requiredContent&&(this.customConfig||this.disallowedContent.length)?this.check(b.requiredContent):!0
},addTransformations:function(y){var x,w;
if(!this.disabled&&y){var v=this._.transformations,t;
for(t=0;
t<y.length;
++t){x=y[t];
var q=void 0,g=void 0,k=void 0,z=void 0,h=void 0,r=void 0;
w=[];
for(g=0;
g<x.length;
++g){k=x[g],"string"==typeof k?(k=k.split(/\s*:\s*/),z=k[0],h=null,r=k[1]):(z=k.check,h=k.left,r=k.right),q||(q=k,q=q.element?q.element:z?z.match(/^([a-z0-9]+)/i)[0]:q.left.getDefinition().element),h instanceof CKEDITOR.style&&(h=ah(h)),w.push({check:z==q?null:z,left:h,right:"string"==typeof r?R(r):r})
}x=q;
v[x]||(v[x]=[]);
v[x].push(w)
}}},check:function(t,q,p){if(this.disabled){return !0
}if(CKEDITOR.tools.isArray(t)){for(var n=t.length;
n--;
){if(this.check(t[n],q,p)){return !0
}}return !1
}var k,h;
if("string"==typeof t){h=t+"\x3c"+(!1===q?"0":"1")+(p?"1":"0")+"\x3e";
if(h in this._.cachedChecks){return this._.cachedChecks[h]
}n=ap(t).$1;
k=n.styles;
var g=n.classes;
n.name=n.elements;
n.classes=g=g?g.split(/\s*,\s*/):[];
n.styles=ag(k);
n.attributes=ag(n.attributes);
n.children=[];
g.length&&(n.attributes["class"]=g.join(" "));
k&&(n.attributes.style=CKEDITOR.tools.writeCssText(n.styles));
k=n
}else{n=t.getDefinition(),k=n.styles,g=n.attributes||{},k?(k=N(k),g.style=CKEDITOR.tools.writeCssText(k,!0)):k={},k={name:n.element,attributes:g,classes:g["class"]?g["class"].split(/\s+/):[],styles:k,children:[]}
}var g=CKEDITOR.tools.clone(k),f=[],v;
if(!1!==q&&(v=this._.transformations[k.name])){for(n=0;
n<v.length;
++n){aj(this,k,v[n])
}ak(k)
}ae(this,g,f,{doFilter:!0,doTransform:!1!==q,skipRequired:!p,skipFinalValidation:!p});
q=0<f.length?!1:CKEDITOR.tools.objectCompare(k.attributes,g.attributes,!0)?!0:!1;
"string"==typeof t&&(this._.cachedChecks[h]=q);
return q
},getAllowedEnterMode:function(){var d=["p","div","br"],c={p:CKEDITOR.ENTER_P,div:CKEDITOR.ENTER_DIV,br:CKEDITOR.ENTER_BR};
return function(g,f){var b=d.slice(),a;
if(this.check(s[g])){return g
}for(f||(b=b.reverse());
a=b.pop();
){if(this.check(a)){return c[a]
}}return CKEDITOR.ENTER_BR
}
}(),destroy:function(){delete CKEDITOR.filter.instances[this.id];
delete this._;
delete this.allowedContent;
delete this.disallowedContent
}};
var j={styles:1,attributes:1,classes:1},ac={styles:"requiredStyles",attributes:"requiredAttributes",classes:"requiredClasses"},Q=/^([a-z0-9\-*\s]+)((?:\s*\{[!\w\-,\s\*]+\}\s*|\s*\[[!\w\-,\s\*]+\]\s*|\s*\([!\w\-,\s\*]+\)\s*){0,3})(?:;\s*|$)/i,P={styles:/{([^}]+)}/,attrs:/\[([^\]]+)\]/,classes:/\(([^\)]+)\)/},V=/^cke:(object|embed|param)$/,L=/^(object|embed|param)$/,o=CKEDITOR.filter.transformationsTools={sizeToStyle:function(b){this.lengthToStyle(b,"width");
this.lengthToStyle(b,"height")
},sizeToAttribute:function(b){this.lengthToAttribute(b,"width");
this.lengthToAttribute(b,"height")
},lengthToStyle:function(f,e,h){h=h||e;
if(!(h in f.styles)){var g=f.attributes[e];
g&&(/^\d+$/.test(g)&&(g+="px"),f.styles[h]=g)
}delete f.attributes[e]
},lengthToAttribute:function(g,f,l){l=l||f;
if(!(l in g.attributes)){var k=g.styles[f],h=k&&k.match(/^(\d+)(?:\.\d*)?px$/);
h?g.attributes[l]=h[1]:"cke-test"==k&&(g.attributes[l]="cke-test")
}delete g.styles[f]
},alignmentToStyle:function(d){if(!("float" in d.styles)){var c=d.attributes.align;
if("left"==c||"right"==c){d.styles["float"]=c
}}delete d.attributes.align
},alignmentToAttribute:function(d){if(!("align" in d.attributes)){var c=d.styles["float"];
if("left"==c||"right"==c){d.attributes.align=c
}}delete d.styles["float"]
},matchesStyle:al,transform:function(v,u){if("string"==typeof u){v.name=u
}else{var t=u.getDefinition(),q=t.styles,n=t.attributes,k,g,h,f;
v.name=t.element;
for(k in n){if("class"==k){for(t=v.classes.join("|"),h=n[k].split(/\s+/);
f=h.pop();
){-1==t.indexOf(f)&&v.classes.push(f)
}}else{v.attributes[k]=n[k]
}}for(g in q){v.styles[g]=q[g]
}}}}
})();
(function(){CKEDITOR.focusManager=function(b){if(b.focusManager){return b.focusManager
}this.hasFocus=!1;
this.currentActive=null;
this._={editor:b};
return this
};
CKEDITOR.focusManager._={blurDelay:200};
CKEDITOR.focusManager.prototype={focus:function(b){this._.timer&&clearTimeout(this._.timer);
b&&(this.currentActive=b);
this.hasFocus||this._.locked||((b=CKEDITOR.currentInstance)&&b.focusManager.blur(1),this.hasFocus=!0,(b=this._.editor.container)&&b.addClass("cke_focus"),this._.editor.fire("focus"))
},lock:function(){this._.locked=1
},unlock:function(){delete this._.locked
},blur:function(e){function f(){if(this.hasFocus){this.hasFocus=!1;
var b=this._.editor.container;
b&&b.removeClass("cke_focus");
this._.editor.fire("blur")
}}if(!this._.locked){this._.timer&&clearTimeout(this._.timer);
var c=CKEDITOR.focusManager._.blurDelay;
e||!c?f.call(this):this._.timer=CKEDITOR.tools.setTimeout(function(){delete this._.timer;
f.call(this)
},c,this)
}},add:function(g,i){var f=g.getCustomData("focusmanager");
if(!f||f!=this){f&&f.remove(g);
var f="focus",j="blur";
i&&(CKEDITOR.env.ie?(f="focusin",j="focusout"):CKEDITOR.event.useCapture=1);
var h={blur:function(){g.equals(this.currentActive)&&this.blur()
},focus:function(){this.focus(g)
}};
g.on(f,h.focus,this);
g.on(j,h.blur,this);
i&&(CKEDITOR.event.useCapture=0);
g.setCustomData("focusmanager",this);
g.setCustomData("focusmanager_handlers",h)
}},remove:function(b){b.removeCustomData("focusmanager");
var c=b.removeCustomData("focusmanager_handlers");
b.removeListener("blur",c.blur);
b.removeListener("focus",c.focus)
}}
})();
CKEDITOR.keystrokeHandler=function(b){if(b.keystrokeHandler){return b.keystrokeHandler
}this.keystrokes={};
this.blockedKeystrokes={};
this._={editor:b};
return this
};
(function(){var e,f=function(a){a=a.data;
var j=a.getKeystroke(),i=this.keystrokes[j],g=this._.editor;
e=!1===g.fire("key",{keyCode:j,domEvent:a});
e||(i&&(e=!1!==g.execCommand(i,{from:"keystrokeHandler"})),e||(e=!!this.blockedKeystrokes[j]));
e&&a.preventDefault(!0);
return !e
},c=function(a){e&&(e=!1,a.data.preventDefault(!0))
};
CKEDITOR.keystrokeHandler.prototype={attach:function(b){b.on("keydown",f,this);
if(CKEDITOR.env.gecko&&CKEDITOR.env.mac){b.on("keypress",c,this)
}}}
})();
(function(){CKEDITOR.lang={languages:{af:1,ar:1,bg:1,bn:1,bs:1,ca:1,cs:1,cy:1,da:1,de:1,el:1,"en-au":1,"en-ca":1,"en-gb":1,en:1,eo:1,es:1,et:1,eu:1,fa:1,fi:1,fo:1,"fr-ca":1,fr:1,gl:1,gu:1,he:1,hi:1,hr:1,hu:1,id:1,is:1,it:1,ja:1,ka:1,km:1,ko:1,ku:1,lt:1,lv:1,mk:1,mn:1,ms:1,nb:1,nl:1,no:1,pl:1,"pt-br":1,pt:1,ro:1,ru:1,si:1,sk:1,sl:1,sq:1,"sr-latn":1,sr:1,sv:1,th:1,tr:1,tt:1,ug:1,uk:1,vi:1,"zh-cn":1,zh:1},rtl:{ar:1,fa:1,he:1,ku:1,ug:1},load:function(f,g,e){f&&CKEDITOR.lang.languages[f]||(f=this.detect(g,f));
var h=this;
g=function(){h[f].dir=h.rtl[f]?"rtl":"ltr";
e(f,h[f])
};
this[f]?g():CKEDITOR.scriptLoader.load(CKEDITOR.getUrl("lang/"+f+".js"),g,this)
},detect:function(g,i){var f=this.languages;
i=i||navigator.userLanguage||navigator.language||g;
var j=i.toLowerCase().match(/([a-z]+)(?:-([a-z]+))?/),h=j[1],j=j[2];
f[h+"-"+j]?h=h+"-"+j:f[h]||(h=null);
CKEDITOR.lang.detect=h?function(){return h
}:function(b){return b
};
return h||g
}}
})();
CKEDITOR.scriptLoader=function(){var b={},c={};
return{load:function(s,r,o,n){var l="string"==typeof s;
l&&(s=[s]);
o||(o=CKEDITOR);
var j=s.length,d=[],w=[],m=function(e){r&&(l?r.call(o,e):r.call(o,d,w))
};
if(0===j){m(!0)
}else{var p=function(f,e){(e?d:w).push(f);
0>=--j&&(n&&CKEDITOR.document.getDocumentElement().removeStyle("cursor"),m(e))
},i=function(g,q){b[g]=1;
var k=c[g];
delete c[g];
for(var h=0;
h<k.length;
h++){k[h](g,q)
}},u=function(g){if(b[g]){p(g,!0)
}else{var k=c[g]||(c[g]=[]);
k.push(p);
if(!(1<k.length)){var h=new CKEDITOR.dom.element("script");
h.setAttributes({type:"text/javascript",src:g});
r&&(CKEDITOR.env.ie&&11>CKEDITOR.env.version?h.$.onreadystatechange=function(){if("loaded"==h.$.readyState||"complete"==h.$.readyState){h.$.onreadystatechange=null,i(g,!0)
}}:(h.$.onload=function(){setTimeout(function(){i(g,!0)
},0)
},h.$.onerror=function(){i(g,!1)
}));
h.appendTo(CKEDITOR.document.getHead())
}}};
n&&CKEDITOR.document.getDocumentElement().setStyle("cursor","wait");
for(var a=0;
a<j;
a++){u(s[a])
}}},queue:function(){function d(){var a;
(a=e[0])&&this.load(a.scriptUrl,a.callback,CKEDITOR,0)
}var e=[];
return function(i,g){var a=this;
e.push({scriptUrl:i,callback:function(){g&&g.apply(this,arguments);
e.shift();
d.call(a)
}});
1==e.length&&d.call(this)
}
}()}
}();
CKEDITOR.resourceManager=function(b,c){this.basePath=b;
this.fileName=c;
this.registered={};
this.loaded={};
this.externals={};
this._={waitingList:{}}
};
CKEDITOR.resourceManager.prototype={add:function(e,f){if(this.registered[e]){throw Error('[CKEDITOR.resourceManager.add] The resource name "'+e+'" is already registered.')
}var c=this.registered[e]=f||{};
c.name=e;
c.path=this.getPath(e);
CKEDITOR.fire(e+CKEDITOR.tools.capitalize(this.fileName)+"Ready",c);
return this.get(e)
},get:function(b){return this.registered[b]||null
},getPath:function(b){var c=this.externals[b];
return CKEDITOR.getUrl(c&&c.dir||this.basePath+b+"/")
},getFilePath:function(b){var c=this.externals[b];
return CKEDITOR.getUrl(this.getPath(b)+(c?c.file:this.fileName+".js"))
},addExternal:function(g,i,f){g=g.split(",");
for(var j=0;
j<g.length;
j++){var h=g[j];
f||(i=i.replace(/[^\/]+$/,function(b){f=b;
return""
}));
this.externals[h]={dir:i,file:f||this.fileName+".js"}
}},load:function(u,p,s){CKEDITOR.tools.isArray(u)||(u=u?[u]:[]);
for(var r=this.loaded,o=this.registered,n=[],l={},j={},i=0;
i<u.length;
i++){var v=u[i];
if(v){if(r[v]||o[v]){j[v]=this.get(v)
}else{var m=this.getFilePath(v);
n.push(m);
m in l||(l[m]=[]);
l[m].push(v)
}}}CKEDITOR.scriptLoader.load(n,function(b,h){if(h.length){throw Error('[CKEDITOR.resourceManager.load] Resource name "'+l[h[0]].join(",")+'" was not found at "'+h[0]+'".')
}for(var d=0;
d<b.length;
d++){for(var c=l[b[d]],q=0;
q<c.length;
q++){var k=c[q];
j[k]=this.get(k);
r[k]=1
}}p.call(s,j)
},this)
}};
CKEDITOR.plugins=new CKEDITOR.resourceManager("plugins/","plugin");
CKEDITOR.plugins.load=CKEDITOR.tools.override(CKEDITOR.plugins.load,function(b){var c={};
return function(a,j,i){var g={},d=function(e){b.call(this,e,function(l){CKEDITOR.tools.extend(g,l);
var h=[],o;
for(o in l){var n=l[o],f=n&&n.requires;
if(!c[o]){if(n.icons){for(var m=n.icons.split(","),p=m.length;
p--;
){CKEDITOR.skin.addIcon(m[p],n.path+"icons/"+(CKEDITOR.env.hidpi&&n.hidpi?"hidpi/":"")+m[p]+".png")
}}c[o]=1
}if(f){for(f.split&&(f=f.split(",")),n=0;
n<f.length;
n++){g[f[n]]||h.push(f[n])
}}}if(h.length){d.call(this,h)
}else{for(o in g){n=g[o],n.onLoad&&!n.onLoad._called&&(!1===n.onLoad()&&delete g[o],n.onLoad._called=1)
}j&&j.call(i||window,g)
}},this)
};
d.call(this,a)
}
});
CKEDITOR.plugins.setLang=function(f,g,e){var h=this.get(f);
f=h.langEntries||(h.langEntries={});
h=h.lang||(h.lang=[]);
h.split&&(h=h.split(","));
-1==CKEDITOR.tools.indexOf(h,g)&&h.push(g);
f[g]=e
};
CKEDITOR.ui=function(b){if(b.ui){return b.ui
}this.items={};
this.instances={};
this.editor=b;
this._={handlers:{}};
return this
};
CKEDITOR.ui.prototype={add:function(f,g,e){e.name=f.toLowerCase();
var h=this.items[f]={type:g,command:e.command||null,args:Array.prototype.slice.call(arguments,2)};
CKEDITOR.tools.extend(h,e)
},get:function(b){return this.instances[b]
},create:function(f){var g=this.items[f],e=g&&this._.handlers[g.type],h=g&&g.command&&this.editor.getCommand(g.command),e=e&&e.create.apply(this,g.args);
this.instances[f]=e;
h&&h.uiItems.push(e);
e&&!e.type&&(e.type=g.type);
return e
},addHandler:function(b,c){this._.handlers[b]=c
},space:function(b){return CKEDITOR.document.getById(this.spaceId(b))
},spaceId:function(b){return this.editor.id+"_"+b
}};
CKEDITOR.event.implementOn(CKEDITOR.ui);
(function(){function y(b,d,c){CKEDITOR.event.call(this);
b=b&&CKEDITOR.tools.clone(b);
if(void 0!==d){if(!(d instanceof CKEDITOR.dom.element)){throw Error("Expect element of type CKEDITOR.dom.element.")
}if(!c){throw Error("One of the element modes must be specified.")
}if(CKEDITOR.env.ie&&CKEDITOR.env.quirks&&c==CKEDITOR.ELEMENT_MODE_INLINE){throw Error("Inline element mode is not supported on IE quirks.")
}if(!x(d,c)){throw Error('The specified element mode is not supported on element: "'+d.getName()+'".')
}this.element=d;
this.elementMode=c;
this.name=this.elementMode!=CKEDITOR.ELEMENT_MODE_APPENDTO&&(d.getId()||d.getNameAtt())
}else{this.elementMode=CKEDITOR.ELEMENT_MODE_NONE
}this._={};
this.commands={};
this.templates={};
this.name=this.name||s();
this.id=CKEDITOR.tools.getNextId();
this.status="unloaded";
this.config=CKEDITOR.tools.prototypedCopy(CKEDITOR.config);
this.ui=new CKEDITOR.ui(this);
this.focusManager=new CKEDITOR.focusManager(this);
this.keystrokeHandler=new CKEDITOR.keystrokeHandler(this);
this.on("readOnly",w);
this.on("selectionChange",function(e){p(this,e.data.path)
});
this.on("activeFilterChange",function(){p(this,this.elementPath(),!0)
});
this.on("mode",w);
this.on("instanceReady",function(){this.config.startupFocus&&this.focus()
});
CKEDITOR.fire("instanceCreated",null,this);
CKEDITOR.add(this);
CKEDITOR.tools.setTimeout(function(){m(this,b)
},0,this)
}function s(){do{var b="editor"+ ++B
}while(CKEDITOR.instances[b]);
return b
}function x(d,c){return c==CKEDITOR.ELEMENT_MODE_INLINE?d.is(CKEDITOR.dtd.$editable)||d.is("textarea"):c==CKEDITOR.ELEMENT_MODE_REPLACE?!d.is(CKEDITOR.dtd.$nonBodyContent):1
}function w(){var d=this.commands,c;
for(c in d){r(this,d[c])
}}function r(d,c){c[c.startDisabled?"disable":d.readOnly&&!c.readOnly?"disable":c.modes[d.mode]?"enable":"disable"]()
}function p(h,f,t){if(f){var q,k,g=h.commands;
for(k in g){q=g[k],(t||q.contextSensitive)&&q.refresh(h,f)
}}}function n(e){var d=e.config.customConfig;
if(!d){return !1
}var d=CKEDITOR.getUrl(d),f=i[d]||(i[d]={});
f.fn?(f.fn.call(e,e.config),CKEDITOR.getUrl(e.config.customConfig)!=d&&n(e)||e.fireOnce("customConfigLoaded")):CKEDITOR.scriptLoader.queue(d,function(){f.fn=CKEDITOR.editorConfig?CKEDITOR.editorConfig:function(){};
n(e)
});
return !0
}function m(d,c){d.on("customConfigLoaded",function(){if(c){if(c.on){for(var a in c.on){d.on(a,c.on[a])
}}CKEDITOR.tools.extend(d.config,c,!0);
delete d.config.on
}a=d.config;
d.readOnly=a.readOnly?!0:d.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?d.element.is("textarea")?d.element.hasAttribute("disabled")||d.element.hasAttribute("readonly"):d.element.isReadOnly():d.elementMode==CKEDITOR.ELEMENT_MODE_REPLACE?d.element.hasAttribute("disabled")||d.element.hasAttribute("readonly"):!1;
d.blockless=d.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?!(d.element.is("textarea")||CKEDITOR.dtd[d.element.getName()].p):!1;
d.tabIndex=a.tabIndex||d.element&&d.element.getAttribute("tabindex")||0;
d.activeEnterMode=d.enterMode=d.blockless?CKEDITOR.ENTER_BR:a.enterMode;
d.activeShiftEnterMode=d.shiftEnterMode=d.blockless?CKEDITOR.ENTER_BR:a.shiftEnterMode;
a.skin&&(CKEDITOR.skinName=a.skin);
d.fireOnce("configLoaded");
d.dataProcessor=new CKEDITOR.htmlDataProcessor(d);
d.filter=d.activeFilter=new CKEDITOR.filter(d);
j(d)
});
c&&null!=c.customConfig&&(d.config.customConfig=c.customConfig);
n(d)||d.fireOnce("customConfigLoaded")
}function j(b){CKEDITOR.skin.loadPart("editor",function(){D(b)
})
}function D(b){CKEDITOR.lang.load(b.config.language,b.config.defaultLanguage,function(a,f){var e=b.config.title;
b.langCode=a;
b.lang=CKEDITOR.tools.prototypedCopy(f);
b.title="string"==typeof e||!1===e?e:[b.lang.editor,b.name].join(", ");
b.config.contentsLangDirection||(b.config.contentsLangDirection=b.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?b.element.getDirection(1):b.lang.dir);
b.fire("langLoaded");
o(b)
})
}function o(b){b.getStylesSet(function(a){b.once("loaded",function(){b.fire("stylesSet",{styles:a})
},null,null,1);
u(b)
})
}function u(h){var f=h.config,v=f.plugins,t=f.extraPlugins,q=f.removePlugins;
if(t){var g=new RegExp("(?:^|,)(?:"+t.replace(/\s*,\s*/g,"|")+")(?\x3d,|$)","g"),v=v.replace(g,""),v=v+(","+t)
}if(q){var k=new RegExp("(?:^|,)(?:"+q.replace(/\s*,\s*/g,"|")+")(?\x3d,|$)","g"),v=v.replace(k,"")
}CKEDITOR.env.air&&(v+=",adobeair");
CKEDITOR.plugins.load(v.split(","),function(G){var F=[],E=[],a=[];
h.plugins=G;
for(var C in G){var I=G[C],A=I.lang,z=null,b=I.requires,H;
CKEDITOR.tools.isArray(b)&&(b=b.join(","));
if(b&&(H=b.match(k))){for(;
b=H.pop();
){CKEDITOR.error("editor-plugin-required",{plugin:b.replace(",",""),requiredBy:C})
}}A&&!h.lang[C]&&(A.split&&(A=A.split(",")),0<=CKEDITOR.tools.indexOf(A,h.langCode)?z=h.langCode:(z=h.langCode.replace(/-.*/,""),z=z!=h.langCode&&0<=CKEDITOR.tools.indexOf(A,z)?z:0<=CKEDITOR.tools.indexOf(A,"en")?"en":A[0]),I.langEntries&&I.langEntries[z]?(h.lang[C]=I.langEntries[z],z=null):a.push(CKEDITOR.getUrl(I.path+"lang/"+z+".js")));
E.push(z);
F.push(I)
}CKEDITOR.scriptLoader.load(a,function(){for(var K=["beforeInit","init","afterInit"],e=0;
e<K.length;
e++){for(var J=0;
J<F.length;
J++){var d=F[J];
0===e&&E[J]&&d.lang&&d.langEntries&&(h.lang[d.name]=d.langEntries[E[J]]);
if(d[K[e]]){d[K[e]](h)
}}}h.fireOnce("pluginsLoaded");
f.keystrokes&&h.setKeystroke(h.config.keystrokes);
for(J=0;
J<h.config.blockedKeystrokes.length;
J++){h.keystrokeHandler.blockedKeystrokes[h.config.blockedKeystrokes[J]]=1
}h.status="loaded";
h.fireOnce("loaded");
CKEDITOR.fire("instanceLoaded",null,h)
})
})
}function l(){var d=this.element;
if(d&&this.elementMode!=CKEDITOR.ELEMENT_MODE_APPENDTO){var c=this.getData();
this.config.htmlEncodeOutput&&(c=CKEDITOR.tools.htmlEncode(c));
d.is("textarea")?d.setValue(c):d.setHtml(c);
return !0
}return !1
}y.prototype=CKEDITOR.editor.prototype;
CKEDITOR.editor=y;
var B=0,i={};
CKEDITOR.tools.extend(CKEDITOR.editor.prototype,{addCommand:function(e,d){d.name=e.toLowerCase();
var f=new CKEDITOR.command(this,d);
this.mode&&r(this,f);
return this.commands[e]=f
},_attachToForm:function(){function g(a){q.updateElement();
q._.required&&!k.getValue()&&!1===q.fire("required")&&a.data.preventDefault()
}function f(b){return !!(b&&b.call&&b.apply)
}var q=this,k=q.element,h=new CKEDITOR.dom.element(k.$.form);
k.is("textarea")&&h&&(h.on("submit",g),f(h.$.submit)&&(h.$.submit=CKEDITOR.tools.override(h.$.submit,function(a){return function(){g();
a.apply?a.apply(this):a()
}
})),q.on("destroy",function(){h.removeListener("submit",g)
}))
},destroy:function(b){this.fire("beforeDestroy");
!b&&l.call(this);
this.editable(null);
this.filter.destroy();
delete this.filter;
delete this.activeFilter;
this.status="destroyed";
this.fire("destroy");
this.removeAllListeners();
CKEDITOR.remove(this);
CKEDITOR.fire("instanceDestroyed",null,this)
},elementPath:function(b){if(!b){b=this.getSelection();
if(!b){return null
}b=b.getStartElement()
}return b?new CKEDITOR.dom.elementPath(b,this.editable()):null
},createRange:function(){var b=this.editable();
return b?new CKEDITOR.dom.range(b):null
},execCommand:function(f,e){var h=this.getCommand(f),g={name:f,commandData:e,command:h};
return h&&h.state!=CKEDITOR.TRISTATE_DISABLED&&!1!==this.fire("beforeCommandExec",g)&&(g.returnValue=h.exec(g.commandData),!h.async&&!1!==this.fire("afterCommandExec",g))?g.returnValue:!1
},getCommand:function(b){return this.commands[b]
},getData:function(d){!d&&this.fire("beforeGetData");
var c=this._.data;
"string"!=typeof c&&(c=(c=this.element)&&this.elementMode==CKEDITOR.ELEMENT_MODE_REPLACE?c.is("textarea")?c.getValue():c.getHtml():"");
c={dataValue:c};
!d&&this.fire("getData",c);
return c.dataValue
},getSnapshot:function(){var b=this.fire("getSnapshot");
"string"!=typeof b&&(b=(b=this.element)&&this.elementMode==CKEDITOR.ELEMENT_MODE_REPLACE?b.is("textarea")?b.getValue():b.getHtml():"");
return b
},loadSnapshot:function(b){this.fire("loadSnapshot",b)
},setData:function(g,f,q){var k=!0,h=f;
f&&"object"==typeof f&&(q=f.internal,h=f.callback,k=!f.noSnapshot);
!q&&k&&this.fire("saveSnapshot");
if(h||!q){this.once("dataReady",function(b){!q&&k&&this.fire("saveSnapshot");
h&&h.call(b.editor)
})
}g={dataValue:g};
!q&&this.fire("setData",g);
this._.data=g.dataValue;
!q&&this.fire("afterSetData",g)
},setReadOnly:function(b){b=null==b||b;
this.readOnly!=b&&(this.readOnly=b,this.keystrokeHandler.blockedKeystrokes[8]=+b,this.editable().setReadOnly(b),this.fire("readOnly"))
},insertHtml:function(e,d,f){this.fire("insertHtml",{dataValue:e,mode:d,range:f})
},insertText:function(b){this.fire("insertText",b)
},insertElement:function(b){this.fire("insertElement",b)
},getSelectedHtml:function(e){var d=this.editable(),f=this.getSelection(),f=f&&f.getRanges();
if(!d||!f||0===f.length){return null
}d=d.getHtmlFromRange(f[0]);
return e?d.getHtml():d
},extractSelectedHtml:function(f,e){var h=this.editable(),g=this.getSelection().getRanges();
if(!h||0===g.length){return null
}g=g[0];
h=h.extractHtmlFromRange(g,e);
e||this.getSelection().selectRanges([g]);
return f?h.getHtml():h
},focus:function(){this.fire("beforeFocus")
},checkDirty:function(){return"ready"==this.status&&this._.previousValue!==this.getSnapshot()
},resetDirty:function(){this._.previousValue=this.getSnapshot()
},updateElement:function(){return l.call(this)
},setKeystroke:function(){for(var g=this.keystrokeHandler.keystrokes,f=CKEDITOR.tools.isArray(arguments[0])?arguments[0]:[[].slice.call(arguments,0)],q,k,h=f.length;
h--;
){q=f[h],k=0,CKEDITOR.tools.isArray(q)&&(k=q[1],q=q[0]),k?g[q]=k:delete g[q]
}},addFeature:function(b){return this.filter.addFeature(b)
},setActiveFilter:function(b){b||(b=this.filter);
this.activeFilter!==b&&(this.activeFilter=b,this.fire("activeFilterChange"),b===this.filter?this.setActiveEnterMode(null,null):this.setActiveEnterMode(b.getAllowedEnterMode(this.enterMode),b.getAllowedEnterMode(this.shiftEnterMode,!0)))
},setActiveEnterMode:function(d,c){d=d?this.blockless?CKEDITOR.ENTER_BR:d:this.enterMode;
c=c?this.blockless?CKEDITOR.ENTER_BR:c:this.shiftEnterMode;
if(this.activeEnterMode!=d||this.activeShiftEnterMode!=c){this.activeEnterMode=d,this.activeShiftEnterMode=c,this.fire("activeEnterModeChange")
}},showNotification:function(b){alert(b)
}})
})();
CKEDITOR.ELEMENT_MODE_NONE=0;
CKEDITOR.ELEMENT_MODE_REPLACE=1;
CKEDITOR.ELEMENT_MODE_APPENDTO=2;
CKEDITOR.ELEMENT_MODE_INLINE=3;
CKEDITOR.htmlParser=function(){this._={htmlPartsRegex:/<(?:(?:\/([^>]+)>)|(?:!--([\S|\s]*?)--\x3e)|(?:([^\/\s>]+)((?:\s+[\w\-:.]+(?:\s*=\s*?(?:(?:"[^"]*")|(?:'[^']*')|[^\s"'\/>]+))?)*)[\S\s]*?(\/?)>))/g}
};
(function(){var b=/([\w\-:.]+)(?:(?:\s*=\s*(?:(?:"([^"]*)")|(?:'([^']*)')|([^\s>]+)))|(?=\s|$))/g,c={checked:1,compact:1,declare:1,defer:1,disabled:1,ismap:1,multiple:1,nohref:1,noresize:1,noshade:1,nowrap:1,readonly:1,selected:1};
CKEDITOR.htmlParser.prototype={onTagOpen:function(){},onTagClose:function(){},onText:function(){},onCDATA:function(){},onComment:function(){},parse:function(o){for(var n,m,l=0,i;
n=this._.htmlPartsRegex.exec(o);
){m=n.index;
if(m>l){if(l=o.substring(l,m),i){i.push(l)
}else{this.onText(l)
}}l=this._.htmlPartsRegex.lastIndex;
if(m=n[1]){if(m=m.toLowerCase(),i&&CKEDITOR.dtd.$cdata[m]&&(this.onCDATA(i.join("")),i=null),!i){this.onTagClose(m);
continue
}}if(i){i.push(n[0])
}else{if(m=n[3]){if(m=m.toLowerCase(),!/="/.test(m)){var d={},a,p=n[4];
n=!!n[5];
if(p){for(;
a=b.exec(p);
){var j=a[1].toLowerCase();
a=a[2]||a[3]||a[4]||"";
d[j]=!a&&c[j]?j:CKEDITOR.tools.htmlDecodeAttr(a)
}}this.onTagOpen(m,d,n);
!i&&CKEDITOR.dtd.$cdata[m]&&(i=[])
}}else{if(m=n[2]){this.onComment(m)
}}}}if(o.length>l){this.onText(o.substring(l,o.length))
}}}
})();
CKEDITOR.htmlParser.basicWriter=CKEDITOR.tools.createClass({$:function(){this._={output:[]}
},proto:{openTag:function(b){this._.output.push("\x3c",b)
},openTagClose:function(b,c){c?this._.output.push(" /\x3e"):this._.output.push("\x3e")
},attribute:function(b,c){"string"==typeof c&&(c=CKEDITOR.tools.htmlEncodeAttr(c));
this._.output.push(" ",b,'\x3d"',c,'"')
},closeTag:function(b){this._.output.push("\x3c/",b,"\x3e")
},text:function(b){this._.output.push(b)
},comment:function(b){this._.output.push("\x3c!--",b,"--\x3e")
},write:function(b){this._.output.push(b)
},reset:function(){this._.output=[];
this._.indent=!1
},getHtml:function(b){var c=this._.output.join("");
b&&this.reset();
return c
}}});
"use strict";
(function(){CKEDITOR.htmlParser.node=function(){};
CKEDITOR.htmlParser.node.prototype={remove:function(){var f=this.parent.children,g=CKEDITOR.tools.indexOf(f,this),e=this.previous,h=this.next;
e&&(e.next=h);
h&&(h.previous=e);
f.splice(g,1);
this.parent=null
},replaceWith:function(g){var i=this.parent.children,f=CKEDITOR.tools.indexOf(i,this),j=g.previous=this.previous,h=g.next=this.next;
j&&(j.next=g);
h&&(h.previous=g);
i[f]=g;
g.parent=this.parent;
this.parent=null
},insertAfter:function(f){var g=f.parent.children,e=CKEDITOR.tools.indexOf(g,f),h=f.next;
g.splice(e+1,0,this);
this.next=f.next;
this.previous=f;
f.next=this;
h&&(h.previous=this);
this.parent=f.parent
},insertBefore:function(e){var f=e.parent.children,c=CKEDITOR.tools.indexOf(f,e);
f.splice(c,0,this);
this.next=e;
(this.previous=e.previous)&&(e.previous.next=this);
e.previous=this;
this.parent=e.parent
},getAscendant:function(e){var f="function"==typeof e?e:"string"==typeof e?function(a){return a.name==e
}:function(a){return a.name in e
},c=this.parent;
for(;
c&&c.type==CKEDITOR.NODE_ELEMENT;
){if(f(c)){return c
}c=c.parent
}return null
},wrapWith:function(b){this.replaceWith(b);
b.add(this);
return b
},getIndex:function(){return CKEDITOR.tools.indexOf(this.parent.children,this)
},getFilterContext:function(b){return b||{}
}}
})();
"use strict";
CKEDITOR.htmlParser.comment=function(b){this.value=b;
this._={isBlockLike:!1}
};
CKEDITOR.htmlParser.comment.prototype=CKEDITOR.tools.extend(new CKEDITOR.htmlParser.node,{type:CKEDITOR.NODE_COMMENT,filter:function(e,f){var c=this.value;
if(!(c=e.onComment(f,c,this))){return this.remove(),!1
}if("string"!=typeof c){return this.replaceWith(c),!1
}this.value=c;
return !0
},writeHtml:function(b,c){c&&this.filter(c);
b.comment(this.value)
}});
"use strict";
(function(){CKEDITOR.htmlParser.text=function(b){this.value=b;
this._={isBlockLike:!1}
};
CKEDITOR.htmlParser.text.prototype=CKEDITOR.tools.extend(new CKEDITOR.htmlParser.node,{type:CKEDITOR.NODE_TEXT,filter:function(b,c){if(!(this.value=b.onText(c,this.value,this))){return this.remove(),!1
}},writeHtml:function(b,c){c&&this.filter(c);
b.text(this.value)
}})
})();
"use strict";
(function(){CKEDITOR.htmlParser.cdata=function(b){this.value=b
};
CKEDITOR.htmlParser.cdata.prototype=CKEDITOR.tools.extend(new CKEDITOR.htmlParser.node,{type:CKEDITOR.NODE_TEXT,filter:function(){},writeHtml:function(b){b.write(this.value)
}})
})();
"use strict";
CKEDITOR.htmlParser.fragment=function(){this.children=[];
this.parent=null;
this._={isBlockLike:!0,hasInlineStarted:!1}
};
(function(){function g(b){return b.attributes["data-cke-survive"]?!1:"a"==b.name&&b.attributes.href||CKEDITOR.dtd.$removeEmpty[b.name]
}var i=CKEDITOR.tools.extend({table:1,ul:1,ol:1,dl:1},CKEDITOR.dtd.table,CKEDITOR.dtd.ul,CKEDITOR.dtd.ol,CKEDITOR.dtd.dl),f={ol:1,ul:1},j=CKEDITOR.tools.extend({},{html:1},CKEDITOR.dtd.html,CKEDITOR.dtd.body,CKEDITOR.dtd.head,{style:1,script:1}),h={ul:"li",ol:"li",dl:"dd",table:"tbody",tbody:"tr",thead:"tr",tfoot:"tr",tr:"td"};
CKEDITOR.htmlParser.fragment.fromHtml=function(r,o,m){function b(q){var k;
if(0<x.length){for(var w=0;
w<x.length;
w++){var v=x[w],u=v.name,n=CKEDITOR.dtd[u],t=B.name&&CKEDITOR.dtd[B.name];
t&&!t[u]||q&&n&&!n[q]&&CKEDITOR.dtd[q]?u==B.name&&(s(B,B.parent,1),w--):(k||(H(),k=1),v=v.clone(),v.parent=B,B=v,x.splice(w,1),w--)
}}}function H(){for(;
G.length;
){s(G.shift(),B)
}}function p(l){if(l._.isBlockLike&&"pre"!=l.name&&"textarea"!=l.name){var k=l.children.length,q=l.children[k-1],n;
q&&q.type==CKEDITOR.NODE_TEXT&&((n=CKEDITOR.tools.rtrim(q.value))?q.value=n:l.children.length=k-1)
}}function s(k,q,n){q=q||B||d;
var l=B;
void 0===k.previous&&(c(q,k)&&(B=q,a.onTagOpen(m,{}),k.returnPoint=q=B),p(k),g(k)&&!k.children.length||q.add(k),"pre"==k.name&&(e=!1),"textarea"==k.name&&(E=!1));
k.returnPoint?(B=k.returnPoint,delete k.returnPoint):B=n?q:l
}function c(l,k){if((l==d||"body"==l.name)&&m&&(!l.name||CKEDITOR.dtd[l.name][m])){var q,n;
return(q=k.attributes&&(n=k.attributes["data-cke-real-element-type"])?n:k.name)&&q in CKEDITOR.dtd.$inline&&!(q in CKEDITOR.dtd.head)&&!k.isOrphan||k.type==CKEDITOR.NODE_TEXT
}}function D(l,k){return l in CKEDITOR.dtd.$listItem||l in CKEDITOR.dtd.$tableContent?l==k||"dt"==l&&"dd"==k||"dd"==l&&"dt"==k:!1
}var a=new CKEDITOR.htmlParser,d=o instanceof CKEDITOR.htmlParser.element?o:"string"==typeof o?new CKEDITOR.htmlParser.element(o):new CKEDITOR.htmlParser.fragment,x=[],G=[],B=d,E="textarea"==d.name,e="pre"==d.name;
a.onTagOpen=function(q,k,n,l){k=new CKEDITOR.htmlParser.element(q,k);
k.isUnknown&&n&&(k.isEmpty=!0);
k.isOptionalClose=l;
if(g(k)){x.push(k)
}else{if("pre"==q){e=!0
}else{if("br"==q&&e){B.add(new CKEDITOR.htmlParser.text("\n"));
return
}"textarea"==q&&(E=!0)
}if("br"==q){G.push(k)
}else{for(;
!(l=(n=B.name)?CKEDITOR.dtd[n]||(B._.isBlockLike?CKEDITOR.dtd.div:CKEDITOR.dtd.span):j,k.isUnknown||B.isUnknown||l[q]);
){if(B.isOptionalClose){a.onTagClose(n)
}else{if(q in f&&n in f){n=B.children,(n=n[n.length-1])&&"li"==n.name||s(n=new CKEDITOR.htmlParser.element("li"),B),!k.returnPoint&&(k.returnPoint=B),B=n
}else{if(q in CKEDITOR.dtd.$listItem&&!D(q,n)){a.onTagOpen("li"==q?"ul":"dl",{},0,1)
}else{if(n in i&&!D(q,n)){!k.returnPoint&&(k.returnPoint=B),B=B.parent
}else{if(n in CKEDITOR.dtd.$inline&&x.unshift(B),B.parent){s(B,B.parent,1)
}else{k.isOrphan=1;
break
}}}}}}b(q);
H();
k.parent=B;
k.isEmpty?s(k):B=k
}}};
a.onTagClose=function(q){for(var k=x.length-1;
0<=k;
k--){if(q==x[k].name){x.splice(k,1);
return
}}for(var v=[],u=[],t=B;
t!=d&&t.name!=q;
){t._.isBlockLike||u.unshift(t),v.push(t),t=t.returnPoint||t.parent
}if(t!=d){for(k=0;
k<v.length;
k++){var n=v[k];
s(n,n.parent)
}B=t;
t._.isBlockLike&&H();
s(t,t.parent);
t==B&&(B=B.parent);
x=x.concat(u)
}"body"==q&&(m=!1)
};
a.onText=function(l){if(!(B._.hasInlineStarted&&!G.length||e||E)&&(l=CKEDITOR.tools.ltrim(l),0===l.length)){return
}var k=B.name,n=k?CKEDITOR.dtd[k]||(B._.isBlockLike?CKEDITOR.dtd.div:CKEDITOR.dtd.span):j;
if(!E&&!n["#"]&&k in i){a.onTagOpen(h[k]||""),a.onText(l)
}else{H();
b();
e||E||(l=l.replace(/[\t\r\n ]{2,}|[\t\r\n]/g," "));
l=new CKEDITOR.htmlParser.text(l);
if(c(B,l)){this.onTagOpen(m,{},0,1)
}B.add(l)
}};
a.onCDATA=function(k){B.add(new CKEDITOR.htmlParser.cdata(k))
};
a.onComment=function(k){H();
b();
B.add(new CKEDITOR.htmlParser.comment(k))
};
a.parse(r);
for(H();
B!=d;
){s(B,B.parent,1)
}p(d);
return d
};
CKEDITOR.htmlParser.fragment.prototype={type:CKEDITOR.NODE_DOCUMENT_FRAGMENT,add:function(e,d){isNaN(d)&&(d=this.children.length);
var k=0<d?this.children[d-1]:null;
if(k){if(e._.isBlockLike&&k.type==CKEDITOR.NODE_TEXT&&(k.value=CKEDITOR.tools.rtrim(k.value),0===k.value.length)){this.children.pop();
this.add(e);
return
}k.next=e
}e.previous=k;
e.parent=this;
this.children.splice(d,0,e);
this._.hasInlineStarted||(this._.hasInlineStarted=e.type==CKEDITOR.NODE_TEXT||e.type==CKEDITOR.NODE_ELEMENT&&!e._.isBlockLike)
},filter:function(d,c){c=this.getFilterContext(c);
d.onRoot(c,this);
this.filterChildren(d,!1,c)
},filterChildren:function(e,d,k){if(this.childrenFilteredBy!=e.id){k=this.getFilterContext(k);
if(d&&!this.parent){e.onRoot(k,this)
}this.childrenFilteredBy=e.id;
for(d=0;
d<this.children.length;
d++){!1===this.children[d].filter(e,k)&&d--
}}},writeHtml:function(d,c){c&&this.filter(c);
this.writeChildrenHtml(d)
},writeChildrenHtml:function(k,e,m){var l=this.getFilterContext();
if(m&&!this.parent&&e){e.onRoot(l,this)
}e&&this.filterChildren(e,!1,l);
e=0;
m=this.children;
for(l=m.length;
e<l;
e++){m[e].writeHtml(k)
}},forEach:function(l,k,o){if(!(o||k&&this.type!=k)){var n=l(this)
}if(!1!==n){o=this.children;
for(var m=0;
m<o.length;
m++){n=o[m],n.type==CKEDITOR.NODE_ELEMENT?n.forEach(l,k):k&&n.type!=k||l(n)
}}},getFilterContext:function(b){return b||{}
}}
})();
"use strict";
(function(){function b(){this.rules=[]
}function c(a,l,j,i){var g,e;
for(g in l){(e=a[g])||(e=a[g]=new b),e.add(l[g],j,i)
}}CKEDITOR.htmlParser.filter=CKEDITOR.tools.createClass({$:function(a){this.id=CKEDITOR.tools.getNextNumber();
this.elementNameRules=new b;
this.attributeNameRules=new b;
this.elementsRules={};
this.attributesRules={};
this.textRules=new b;
this.commentRules=new b;
this.rootRules=new b;
a&&this.addRules(a,10)
},proto:{addRules:function(d,g){var f;
"number"==typeof g?f=g:g&&"priority" in g&&(f=g.priority);
"number"!=typeof f&&(f=10);
"object"!=typeof g&&(g={});
d.elementNames&&this.elementNameRules.addMany(d.elementNames,f,g);
d.attributeNames&&this.attributeNameRules.addMany(d.attributeNames,f,g);
d.elements&&c(this.elementsRules,d.elements,f,g);
d.attributes&&c(this.attributesRules,d.attributes,f,g);
d.text&&this.textRules.add(d.text,f,g);
d.comment&&this.commentRules.add(d.comment,f,g);
d.root&&this.rootRules.add(d.root,f,g)
},applyTo:function(d){d.filter(this)
},onElementName:function(d,e){return this.elementNameRules.execOnName(d,e)
},onAttributeName:function(d,e){return this.attributeNameRules.execOnName(d,e)
},onText:function(e,g,f){return this.textRules.exec(e,g,f)
},onComment:function(e,g,f){return this.commentRules.exec(e,g,f)
},onRoot:function(d,e){return this.rootRules.exec(d,e)
},onElement:function(e,k){for(var j=[this.elementsRules["^"],this.elementsRules[k.name],this.elementsRules.$],i,g=0;
3>g;
g++){if(i=j[g]){i=i.exec(e,k,this);
if(!1===i){return null
}if(i&&i!=k){return this.onNode(e,i)
}if(k.parent&&!k.name){break
}}}return k
},onNode:function(e,g){var f=g.type;
return f==CKEDITOR.NODE_ELEMENT?this.onElement(e,g):f==CKEDITOR.NODE_TEXT?new CKEDITOR.htmlParser.text(this.onText(e,g.value)):f==CKEDITOR.NODE_COMMENT?new CKEDITOR.htmlParser.comment(this.onComment(e,g.value)):null
},onAttribute:function(e,i,h,g){return(h=this.attributesRules[h])?h.exec(e,g,i,this):g
}}});
CKEDITOR.htmlParser.filterRulesGroup=b;
b.prototype={add:function(e,g,f){this.rules.splice(this.findIndex(g),0,{value:e,priority:g,options:f})
},addMany:function(e,m,l){for(var j=[this.findIndex(m),0],i=0,g=e.length;
i<g;
i++){j.push({value:e[i],priority:m,options:l})
}this.rules.splice.apply(this.rules,j)
},findIndex:function(e){for(var g=this.rules,f=g.length-1;
0<=f&&e<g[f].priority;
){f--
}return f+1
},exec:function(r,p){var n=p instanceof CKEDITOR.htmlParser.node||p instanceof CKEDITOR.htmlParser.fragment,m=Array.prototype.slice.call(arguments,1),j=this.rules,i=j.length,e,s,l,o;
for(o=0;
o<i;
o++){if(n&&(e=p.type,s=p.name),l=j[o],!(r.nonEditable&&!l.options.applyToAll||r.nestedEditable&&l.options.excludeNestedEditable)){l=l.value.apply(null,m);
if(!1===l||n&&l&&(l.name!=s||l.type!=e)){return l
}null!=l&&(m[0]=p=l)
}}return p
},execOnName:function(e,m){for(var l=0,j=this.rules,i=j.length,g;
m&&l<i;
l++){g=j[l],e.nonEditable&&!g.options.applyToAll||e.nestedEditable&&g.options.excludeNestedEditable||(m=m.replace(g.value[0],g.value[1]))
}return m
}}
})();
(function(){function ap(l,k){function f(d){return d||CKEDITOR.env.needsNbspFiller?new CKEDITOR.htmlParser.text(" "):new CKEDITOR.htmlParser.element("br",{"data-cke-bogus":1})
}function e(g,m){return function(v){if(v.type!=CKEDITOR.NODE_DOCUMENT_FRAGMENT){var a=[],t=ao(v),d,r;
if(t){for(c(t,1)&&a.push(t);
t;
){ak(t)&&(d=an(t))&&c(d)&&((r=an(d))&&!ak(r)?a.push(d):(f(h).insertAfter(d),d.remove())),t=t.previous
}}for(t=0;
t<a.length;
t++){a[t].remove()
}if(a=!g||!1!==("function"==typeof m?m(v):m)){h||CKEDITOR.env.needsBrFiller||v.type!=CKEDITOR.NODE_DOCUMENT_FRAGMENT?h||CKEDITOR.env.needsBrFiller||!(7<document.documentMode||v.name in CKEDITOR.dtd.tr||v.name in CKEDITOR.dtd.$listItem)?(a=ao(v),a=!a||"form"==v.name&&"input"==a.name):a=!1:a=!1
}a&&v.add(f(g))
}}
}function c(g,d){if((!h||CKEDITOR.env.needsBrFiller)&&g.type==CKEDITOR.NODE_ELEMENT&&"br"==g.name&&!g.attributes["data-cke-eol"]){return !0
}var m;
return g.type==CKEDITOR.NODE_TEXT&&(m=g.value.match(K))&&(m.index&&((new CKEDITOR.htmlParser.text(g.value.substring(0,m.index))).insertBefore(g),g.value=m[0]),!CKEDITOR.env.needsBrFiller&&h&&(!d||g.parent.name in n)||!h&&((m=g.previous)&&"br"==m.name||!m||ak(m)))?!0:!1
}var q={elements:{}},h="html"==k,n=CKEDITOR.tools.extend({},ag),b;
for(b in n){"#" in P[b]||delete n[b]
}for(b in n){q.elements[b]=e(h,l.config.fillEmptyBlocks)
}q.root=e(h,!1);
q.elements.br=function(d){return function(a){if(a.parent.type!=CKEDITOR.NODE_DOCUMENT_FRAGMENT){var m=a.attributes;
if("data-cke-bogus" in m||"data-cke-eol" in m){delete m["data-cke-bogus"]
}else{for(m=a.next;
m&&al(m);
){m=m.next
}var g=an(a);
!m&&ak(a.parent)?ai(a.parent,f(d)):ak(m)&&g&&!ak(g)&&f(d).insertBefore(m)
}}}
}(h);
return q
}function am(d,c){return d!=CKEDITOR.ENTER_BR&&!1!==c?d==CKEDITOR.ENTER_DIV?"div":"p":!1
}function ao(b){for(b=b.children[b.children.length-1];
b&&al(b);
){b=b.previous
}return b
}function an(b){for(b=b.previous;
b&&al(b);
){b=b.previous
}return b
}function al(b){return b.type==CKEDITOR.NODE_TEXT&&!CKEDITOR.tools.trim(b.value)||b.type==CKEDITOR.NODE_ELEMENT&&b.attributes["data-cke-bookmark"]
}function ak(b){return b&&(b.type==CKEDITOR.NODE_ELEMENT&&b.name in ag||b.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT)
}function ai(e,d){var f=e.children[e.children.length-1];
e.children.push(d);
d.parent=e;
f&&(f.next=d,d.previous=f)
}function ah(b){b=b.attributes;
"false"!=b.contenteditable&&(b["data-cke-editable"]=b.contenteditable?"true":1);
b.contenteditable="false"
}function ab(b){b=b.attributes;
switch(b["data-cke-editable"]){case"true":b.contenteditable="true";
break;
case"1":delete b.contenteditable
}}function W(b){return b.replace(Q,function(e,d,f){return"\x3c"+d+f.replace(aa,function(g,c){return E.test(c)&&-1==f.indexOf("data-cke-saved-"+c)?" data-cke-saved-"+g+" data-cke-"+CKEDITOR.rnd+"-"+g:g
})+"\x3e"
})
}function aj(d,c){return d.replace(c,function(f,e,g){0===f.indexOf("\x3ctextarea")&&(f=e+R(g).replace(/</g,"\x26lt;").replace(/>/g,"\x26gt;")+"\x3c/textarea\x3e");
return"\x3ccke:encoded\x3e"+encodeURIComponent(f)+"\x3c/cke:encoded\x3e"
})
}function s(b){return b.replace(j,function(d,c){return decodeURIComponent(c)
})
}function ad(b){return b.replace(/\x3c!--(?!{cke_protected})[\s\S]+?--\x3e/g,function(c){return"\x3c!--"+V+"{C}"+encodeURIComponent(c).replace(/--/g,"%2D%2D")+"--\x3e"
})
}function R(b){return b.replace(/\x3c!--\{cke_protected\}\{C\}([\s\S]+?)--\x3e/g,function(d,c){return decodeURIComponent(c)
})
}function Z(e,d){var f=d._.dataStore;
return e.replace(/\x3c!--\{cke_protected\}([\s\S]+?)--\x3e/g,function(g,c){return decodeURIComponent(c)
}).replace(/\{cke_protected_(\d+)\}/g,function(g,c){return f&&f[c]||""
})
}function ae(k,g){var q=[],p=g.config.protectedSource,n=g._.dataStore||(g._.dataStore={id:1}),h=/<\!--\{cke_temp(comment)?\}(\d*?)--\x3e/g,p=[/<script[\s\S]*?(<\/script>|$)/gi,/<noscript[\s\S]*?<\/noscript>/gi,/<meta[\s\S]*?\/?>/gi].concat(p);
k=k.replace(/\x3c!--[\s\S]*?--\x3e/g,function(b){return"\x3c!--{cke_tempcomment}"+(q.push(b)-1)+"--\x3e"
});
for(var f=0;
f<p.length;
f++){k=k.replace(p[f],function(b){b=b.replace(h,function(e,c,l){return q[l]
});
return/cke_temp(comment)?/.test(b)?b:"\x3c!--{cke_temp}"+(q.push(b)-1)+"--\x3e"
})
}k=k.replace(h,function(e,c,l){return"\x3c!--"+V+(c?"{C}":"")+encodeURIComponent(q[l]).replace(/--/g,"%2D%2D")+"--\x3e"
});
k=k.replace(/<\w+(?:\s+(?:(?:[^\s=>]+\s*=\s*(?:[^'"\s>]+|'[^']*'|"[^"]*"))|[^\s=\/>]+))+\s*\/?>/g,function(b){return b.replace(/\x3c!--\{cke_protected\}([^>]*)--\x3e/g,function(d,c){n[n.id]=decodeURIComponent(c);
return"{cke_protected_"+n.id+++"}"
})
});
return k=k.replace(/<(title|iframe|textarea)([^>]*)>([\s\S]*?)<\/\1>/g,function(b,r,m,l){return"\x3c"+r+m+"\x3e"+Z(R(l),g)+"\x3c/"+r+"\x3e"
})
}CKEDITOR.htmlDataProcessor=function(a){var g,f,d=this;
this.editor=a;
this.dataFilter=g=new CKEDITOR.htmlParser.filter;
this.htmlFilter=f=new CKEDITOR.htmlParser.filter;
this.writer=new CKEDITOR.htmlParser.basicWriter;
g.addRules(ac);
g.addRules(af,{applyToAll:!0});
g.addRules(ap(a,"data"),{applyToAll:!0});
f.addRules(X);
f.addRules(Y,{applyToAll:!0});
f.addRules(ap(a,"html"),{applyToAll:!0});
a.on("toHtml",function(h){h=h.data;
var m=h.dataValue,k,m=ae(m,a),m=aj(m,o),m=W(m),m=aj(m,i),m=m.replace(N,"$1cke:$2"),m=m.replace(L,"\x3ccke:$1$2\x3e\x3c/cke:$1\x3e"),m=m.replace(/(<pre\b[^>]*>)(\r\n|\n)/g,"$1$2$2"),m=m.replace(/([^a-z0-9<\-])(on\w{3,})(?!>)/gi,"$1data-cke-"+CKEDITOR.rnd+"-$2");
k=h.context||a.editable().getName();
var b;
CKEDITOR.env.ie&&9>CKEDITOR.env.version&&"pre"==k&&(k="div",m="\x3cpre\x3e"+m+"\x3c/pre\x3e",b=1);
k=a.document.createElement(k);
k.setHtml("a"+m);
m=k.getHtml().substr(1);
m=m.replace(new RegExp("data-cke-"+CKEDITOR.rnd+"-","ig"),"");
b&&(m=m.replace(/^<pre>|<\/pre>$/gi,""));
m=m.replace(O,"$1$2");
m=s(m);
m=R(m);
k=!1===h.fixForBody?!1:am(h.enterMode,a.config.autoParagraph);
m=CKEDITOR.htmlParser.fragment.fromHtml(m,h.context,k);
k&&(b=m,!b.children.length&&CKEDITOR.dtd[b.name][k]&&(k=new CKEDITOR.htmlParser.element(k),b.add(k)));
h.dataValue=m
},null,null,5);
a.on("toHtml",function(b){b.data.filter.applyTo(b.data.dataValue,!0,b.data.dontFilter,b.data.enterMode)&&a.fire("dataFiltered")
},null,null,6);
a.on("toHtml",function(b){b.data.dataValue.filterChildren(d.dataFilter,!0)
},null,null,10);
a.on("toHtml",function(h){h=h.data;
var e=h.dataValue,k=new CKEDITOR.htmlParser.basicWriter;
e.writeChildrenHtml(k);
e=k.getHtml(!0);
h.dataValue=ad(e)
},null,null,15);
a.on("toDataFormat",function(b){var e=b.data.dataValue;
b.data.enterMode!=CKEDITOR.ENTER_BR&&(e=e.replace(/^<br *\/?>/i,""));
b.data.dataValue=CKEDITOR.htmlParser.fragment.fromHtml(e,b.data.context,am(b.data.enterMode,a.config.autoParagraph))
},null,null,5);
a.on("toDataFormat",function(b){b.data.dataValue.filterChildren(d.htmlFilter,!0)
},null,null,10);
a.on("toDataFormat",function(b){b.data.filter.applyTo(b.data.dataValue,!1,!0)
},null,null,11);
a.on("toDataFormat",function(b){var h=b.data.dataValue,e=d.writer;
e.reset();
h.writeChildrenHtml(e);
h=e.getHtml(!0);
h=R(h);
h=Z(h,a);
b.data.dataValue=h
},null,null,15)
};
CKEDITOR.htmlDataProcessor.prototype={toHtml:function(v,u,t,q){var n=this.editor,k,h,g,f;
u&&"object"==typeof u?(k=u.context,t=u.fixForBody,q=u.dontFilter,h=u.filter,g=u.enterMode,f=u.protectedWhitespaces):k=u;
k||null===k||(k=n.editable().getName());
return n.fire("toHtml",{dataValue:v,context:k,fixForBody:t,dontFilter:q,filter:h||n.filter,enterMode:g||n.enterMode,protectedWhitespaces:f}).dataValue
},toDataFormat:function(g,f){var l,k,h;
f&&(l=f.context,k=f.filter,h=f.enterMode);
l||null===l||(l=this.editor.editable().getName());
return this.editor.fire("toDataFormat",{dataValue:g,filter:k||this.editor.filter,context:l,enterMode:h||this.editor.enterMode}).dataValue
}};
var K=/(?:&nbsp;|\xa0)$/,V="{cke_protected}",P=CKEDITOR.dtd,U="caption colgroup col thead tfoot tbody".split(" "),ag=CKEDITOR.tools.extend({},P.$blockLimit,P.$block),ac={elements:{input:ah,textarea:ah}},af={attributeNames:[[/^on/,"data-cke-pa-on"],[/^data-cke-expando$/,""]]},X={elements:{embed:function(e){var d=e.parent;
if(d&&"object"==d.name){var f=d.attributes.width,d=d.attributes.height;
f&&(e.attributes.width=f);
d&&(e.attributes.height=d)
}},a:function(d){var c=d.attributes;
if(!(d.children.length||c.name||c.id||d.attributes["data-cke-saved-name"])){return !1
}}}},Y={elementNames:[[/^cke:/,""],[/^\?xml:namespace$/,""]],attributeNames:[[/^data-cke-(saved|pa)-/,""],[/^data-cke-.*/,""],["hidefocus",""]],elements:{$:function(g){var f=g.attributes;
if(f){if(f["data-cke-temp"]){return !1
}for(var l=["name","href","src"],k,h=0;
h<l.length;
h++){k="data-cke-saved-"+l[h],k in f&&delete f[l[h]]
}}return g
},table:function(b){b.children.slice(0).sort(function(f,e){var h,g;
f.type==CKEDITOR.NODE_ELEMENT&&e.type==f.type&&(h=CKEDITOR.tools.indexOf(U,f.name),g=CKEDITOR.tools.indexOf(U,e.name));
-1<h&&-1<g&&h!=g||(h=f.parent?f.getIndex():-1,g=e.parent?e.getIndex():-1);
return h>g?1:-1
})
},param:function(b){b.children=[];
b.isEmpty=!0;
return b
},span:function(b){"Apple-style-span"==b.attributes["class"]&&delete b.name
},html:function(b){delete b.attributes.contenteditable;
delete b.attributes["class"]
},body:function(b){delete b.attributes.spellcheck;
delete b.attributes.contenteditable
},style:function(d){var c=d.children[0];
c&&c.value&&(c.value=CKEDITOR.tools.trim(c.value));
d.attributes.type||(d.attributes.type="text/css")
},title:function(d){var c=d.children[0];
!c&&ai(d,c=new CKEDITOR.htmlParser.text);
c.value=d.attributes["data-cke-title"]||""
},input:ab,textarea:ab},attributes:{"class":function(b){return CKEDITOR.tools.ltrim(b.replace(/(?:^|\s+)cke_[^\s]*/g,""))||!1
}}};
CKEDITOR.env.ie&&(Y.attributes.style=function(b){return b.replace(/(^|;)([^\:]+)/g,function(c){return c.toLowerCase()
})
});
var Q=/<(a|area|img|input|source)\b([^>]*)>/gi,aa=/([\w-:]+)\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|(?:[^ "'>]+))/gi,E=/^(href|src|name)$/i,i=/(?:<style(?=[ >])[^>]*>[\s\S]*?<\/style>)|(?:<(:?link|meta|base)[^>]*>)/gi,o=/(<textarea(?=[ >])[^>]*>)([\s\S]*?)(?:<\/textarea>)/gi,j=/<cke:encoded>([^<]*)<\/cke:encoded>/gi,N=/(<\/?)((?:object|embed|param|html|body|head|title)[^>]*>)/gi,O=/(<\/?)cke:((?:html|body|head|title)[^>]*>)/gi,L=/<cke:(param|embed)([^>]*?)\/?>(?!\s*<\/cke:\1)/gi
})();
"use strict";
CKEDITOR.htmlParser.element=function(f,g){this.name=f;
this.attributes=g||{};
this.children=[];
var e=f||"",h=e.match(/^cke:(.*)/);
h&&(e=h[1]);
e=!!(CKEDITOR.dtd.$nonBodyContent[e]||CKEDITOR.dtd.$block[e]||CKEDITOR.dtd.$listItem[e]||CKEDITOR.dtd.$tableContent[e]||CKEDITOR.dtd.$nonEditable[e]||"br"==e);
this.isEmpty=!!CKEDITOR.dtd.$empty[f];
this.isUnknown=!CKEDITOR.dtd[f];
this._={isBlockLike:e,hasInlineStarted:this.isEmpty||!e}
};
CKEDITOR.htmlParser.cssStyle=function(b){var c={};
((b instanceof CKEDITOR.htmlParser.element?b.attributes.style:b)||"").replace(/&quot;/g,'"').replace(/\s*([^ :;]+)\s*:\s*([^;]+)\s*(?=;|$)/g,function(d,g,f){"font-family"==g&&(f=f.replace(/["']/g,""));
c[g.toLowerCase()]=f
});
return{rules:c,populate:function(d){var e=this.toString();
e&&(d instanceof CKEDITOR.dom.element?d.setAttribute("style",e):d instanceof CKEDITOR.htmlParser.element?d.attributes.style=e:d.style=e)
},toString:function(){var d=[],e;
for(e in c){c[e]&&d.push(e,":",c[e],";")
}return d.join("")
}}
};
(function(){function e(b){return function(a){return a.type==CKEDITOR.NODE_ELEMENT&&("string"==typeof b?a.name==b:a.name in b)
}
}var f=function(g,d){g=g[0];
d=d[0];
return g<d?-1:g>d?1:0
},c=CKEDITOR.htmlParser.fragment.prototype;
CKEDITOR.htmlParser.element.prototype=CKEDITOR.tools.extend(new CKEDITOR.htmlParser.node,{type:CKEDITOR.NODE_ELEMENT,add:c.add,clone:function(){return new CKEDITOR.htmlParser.element(this.name,this.attributes)
},filter:function(i,g){var o=this,m,j;
g=o.getFilterContext(g);
if(g.off){return !0
}if(!o.parent){i.onRoot(g,o)
}for(;
;
){m=o.name;
if(!(j=i.onElementName(g,m))){return this.remove(),!1
}o.name=j;
if(!(o=i.onElement(g,o))){return this.remove(),!1
}if(o!==this){return this.replaceWith(o),!1
}if(o.name==m){break
}if(o.type!=CKEDITOR.NODE_ELEMENT){return this.replaceWith(o),!1
}if(!o.name){return this.replaceWithChildren(),!1
}}m=o.attributes;
var n,l;
for(n in m){for(j=m[n];
;
){if(l=i.onAttributeName(g,n)){if(l!=n){delete m[n],n=l
}else{break
}}else{delete m[n];
break
}}l&&(!1===(j=i.onAttribute(g,o,l,j))?delete m[l]:m[l]=j)
}o.isEmpty||this.filterChildren(i,!1,g);
return !0
},filterChildren:c.filterChildren,writeHtml:function(g,d){d&&this.filter(d);
var n=this.name,l=[],i=this.attributes,m,j;
g.openTag(n,i);
for(m in i){l.push([m,i[m]])
}g.sortAttributes&&l.sort(f);
m=0;
for(j=l.length;
m<j;
m++){i=l[m],g.attribute(i[0],i[1])
}g.openTagClose(n,this.isEmpty);
this.writeChildrenHtml(g);
this.isEmpty||g.closeTag(n)
},writeChildrenHtml:c.writeChildrenHtml,replaceWithChildren:function(){for(var g=this.children,d=g.length;
d;
){g[--d].insertAfter(this)
}this.remove()
},forEach:c.forEach,getFirst:function(a){if(!a){return this.children.length?this.children[0]:null
}"function"!=typeof a&&(a=e(a));
for(var h=0,g=this.children.length;
h<g;
++h){if(a(this.children[h])){return this.children[h]
}}return null
},getHtml:function(){var b=new CKEDITOR.htmlParser.basicWriter;
this.writeChildrenHtml(b);
return b.getHtml()
},setHtml:function(h){h=this.children=CKEDITOR.htmlParser.fragment.fromHtml(h).children;
for(var g=0,i=h.length;
g<i;
++g){h[g].parent=this
}},getOuterHtml:function(){var b=new CKEDITOR.htmlParser.basicWriter;
this.writeHtml(b);
return b.getHtml()
},split:function(i){for(var g=this.children.splice(i,this.children.length-i),k=this.clone(),j=0;
j<g.length;
++j){g[j].parent=k
}k.children=g;
g[0]&&(g[0].previous=null);
0<i&&(this.children[i-1].next=null);
this.parent.add(k,this.getIndex()+1);
return k
},addClass:function(g){if(!this.hasClass(g)){var d=this.attributes["class"]||"";
this.attributes["class"]=d+(d?" ":"")+g
}},removeClass:function(g){var d=this.attributes["class"];
d&&((d=CKEDITOR.tools.trim(d.replace(new RegExp("(?:\\s+|^)"+g+"(?:\\s+|$)")," ")))?this.attributes["class"]=d:delete this.attributes["class"])
},hasClass:function(g){var d=this.attributes["class"];
return d?(new RegExp("(?:^|\\s)"+g+"(?\x3d\\s|$)")).test(d):!1
},getFilterContext:function(h){var g=[];
h||(h={off:!1,nonEditable:!1,nestedEditable:!1});
h.off||"off"!=this.attributes["data-cke-processor"]||g.push("off",!0);
h.nonEditable||"false"!=this.attributes.contenteditable?h.nonEditable&&!h.nestedEditable&&"true"==this.attributes.contenteditable&&g.push("nestedEditable",!0):g.push("nonEditable",!0);
if(g.length){h=CKEDITOR.tools.copy(h);
for(var i=0;
i<g.length;
i+=2){h[g[i]]=g[i+1]
}}return h
}},!0)
})();
(function(){var g={},i=/{([^}]+)}/g,f=/([\\'])/g,j=/\n/g,h=/\r/g;
CKEDITOR.template=function(b){if(g[b]){this.output=g[b]
}else{var a=b.replace(f,"\\$1").replace(j,"\\n").replace(h,"\\r").replace(i,function(d,c){return"',data['"+c+"']\x3d\x3dundefined?'{"+c+"}':data['"+c+"'],'"
});
this.output=g[b]=Function("data","buffer","return buffer?buffer.push('"+a+"'):['"+a+"'].join('');")
}}
})();
delete CKEDITOR.loadFullCore;
CKEDITOR.instances={};
CKEDITOR.document=new CKEDITOR.dom.document(document);
CKEDITOR.add=function(b){CKEDITOR.instances[b.name]=b;
b.on("focus",function(){CKEDITOR.currentInstance!=b&&(CKEDITOR.currentInstance=b,CKEDITOR.fire("currentInstance"))
});
b.on("blur",function(){CKEDITOR.currentInstance==b&&(CKEDITOR.currentInstance=null,CKEDITOR.fire("currentInstance"))
});
CKEDITOR.fire("instance",null,b)
};
CKEDITOR.remove=function(b){delete CKEDITOR.instances[b.name]
};
(function(){var b={};
CKEDITOR.addTemplate=function(e,a){var f=b[e];
if(f){return f
}f={name:e,source:a};
CKEDITOR.fire("template",f);
return b[e]=new CKEDITOR.template(f.source)
};
CKEDITOR.getTemplate=function(a){return b[a]
}
})();
(function(){var b=[];
CKEDITOR.addCss=function(a){b.push(a)
};
CKEDITOR.getCss=function(){return b.join("\n")
}
})();
CKEDITOR.on("instanceDestroyed",function(){CKEDITOR.tools.isEmpty(this.instances)&&CKEDITOR.fire("reset")
});
CKEDITOR.TRISTATE_ON=1;
CKEDITOR.TRISTATE_OFF=2;
CKEDITOR.TRISTATE_DISABLED=0;
(function(){CKEDITOR.inline=function(f,g){if(!CKEDITOR.env.isCompatible){return null
}f=CKEDITOR.dom.element.get(f);
if(f.getEditor()){throw'The editor instance "'+f.getEditor().name+'" is already attached to the provided element.'
}var e=new CKEDITOR.editor(g,f,CKEDITOR.ELEMENT_MODE_INLINE),h=f.is("textarea")?f:null;
h?(e.setData(h.getValue(),null,!0),f=CKEDITOR.dom.element.createFromHtml('\x3cdiv contenteditable\x3d"'+!!e.readOnly+'" class\x3d"cke_textarea_inline"\x3e'+h.getValue()+"\x3c/div\x3e",CKEDITOR.document),f.insertAfter(h),h.hide(),h.$.form&&e._attachToForm()):e.setData(f.getHtml(),null,!0);
e.on("loaded",function(){e.fire("uiReady");
e.editable(f);
e.container=f;
e.ui.contentsElement=f;
e.setData(e.getData(1));
e.resetDirty();
e.fire("contentDom");
e.mode="wysiwyg";
e.fire("mode");
e.status="ready";
e.fireOnce("instanceReady");
CKEDITOR.fire("instanceReady",null,e)
},null,null,10000);
e.on("destroy",function(){h&&(e.container.clearCustomData(),e.container.remove(),h.show());
e.element.clearCustomData();
delete e.element
});
return e
};
CKEDITOR.inlineAll=function(){var h,k,g;
for(g in CKEDITOR.dtd.$editable){for(var l=CKEDITOR.document.getElementsByTag(g),j=0,i=l.count();
j<i;
j++){h=l.getItem(j),"true"==h.getAttribute("contenteditable")&&(k={element:h,config:{}},!1!==CKEDITOR.fire("inline",k)&&CKEDITOR.inline(h,k.config))
}}};
CKEDITOR.domReady(function(){!CKEDITOR.disableAutoInline&&CKEDITOR.inlineAll()
})
})();
CKEDITOR.replaceClass="ckeditor";
(function(){function e(b,j,i,g){if(!CKEDITOR.env.isCompatible){return null
}b=CKEDITOR.dom.element.get(b);
if(b.getEditor()){throw'The editor instance "'+b.getEditor().name+'" is already attached to the provided element.'
}var d=new CKEDITOR.editor(j,b,g);
g==CKEDITOR.ELEMENT_MODE_REPLACE&&(b.setStyle("visibility","hidden"),d._.required=b.hasAttribute("required"),b.removeAttribute("required"));
i&&d.setData(i,null,!0);
d.on("loaded",function(){c(d);
g==CKEDITOR.ELEMENT_MODE_REPLACE&&d.config.autoUpdateElement&&b.$.form&&d._attachToForm();
d.setMode(d.config.startupMode,function(){d.resetDirty();
d.status="ready";
d.fireOnce("instanceReady");
CKEDITOR.fire("instanceReady",null,d)
})
});
d.on("destroy",f);
return d
}function f(){var g=this.container,d=this.element;
g&&(g.clearCustomData(),g.remove());
d&&(d.clearCustomData(),this.elementMode==CKEDITOR.ELEMENT_MODE_REPLACE&&(d.show(),this._.required&&d.setAttribute("required","required")),delete this.element)
}function c(i){var g=i.name,o=i.element,m=i.elementMode,j=i.fire("uiSpace",{space:"top",html:""}).html,n=i.fire("uiSpace",{space:"bottom",html:""}).html,l=new CKEDITOR.template('\x3c{outerEl} id\x3d"cke_{name}" class\x3d"{id} cke cke_reset cke_chrome cke_editor_{name} cke_{langDir} '+CKEDITOR.env.cssClass+'"  dir\x3d"{langDir}" lang\x3d"{langCode}" role\x3d"application"'+(i.title?' aria-labelledby\x3d"cke_{name}_arialbl"':"")+"\x3e"+(i.title?'\x3cspan id\x3d"cke_{name}_arialbl" class\x3d"cke_voice_label"\x3e{voiceLabel}\x3c/span\x3e':"")+'\x3c{outerEl} class\x3d"cke_inner cke_reset" role\x3d"presentation"\x3e{topHtml}\x3c{outerEl} id\x3d"{contentId}" class\x3d"cke_contents cke_reset" role\x3d"presentation"\x3e\x3c/{outerEl}\x3e{bottomHtml}\x3c/{outerEl}\x3e\x3c/{outerEl}\x3e'),g=CKEDITOR.dom.element.createFromHtml(l.output({id:i.id,name:g,langDir:i.lang.dir,langCode:i.langCode,voiceLabel:i.title,topHtml:j?'\x3cspan id\x3d"'+i.ui.spaceId("top")+'" class\x3d"cke_top cke_reset_all" role\x3d"presentation" style\x3d"height:auto"\x3e'+j+"\x3c/span\x3e":"",contentId:i.ui.spaceId("contents"),bottomHtml:n?'\x3cspan id\x3d"'+i.ui.spaceId("bottom")+'" class\x3d"cke_bottom cke_reset_all" role\x3d"presentation"\x3e'+n+"\x3c/span\x3e":"",outerEl:CKEDITOR.env.ie?"span":"div"}));
m==CKEDITOR.ELEMENT_MODE_REPLACE?(o.hide(),g.insertAfter(o)):o.append(g);
i.container=g;
i.ui.contentsElement=i.ui.space("contents");
j&&i.ui.space("top").unselectable();
n&&i.ui.space("bottom").unselectable();
o=i.config.width;
m=i.config.height;
o&&g.setStyle("width",CKEDITOR.tools.cssLength(o));
m&&i.ui.space("contents").setStyle("height",CKEDITOR.tools.cssLength(m));
g.disableContextMenu();
CKEDITOR.env.webkit&&g.on("focus",function(){i.focus()
});
i.fireOnce("uiReady")
}CKEDITOR.replace=function(a,g){return e(a,g,null,CKEDITOR.ELEMENT_MODE_REPLACE)
};
CKEDITOR.appendTo=function(a,h,g){return e(a,h,g,CKEDITOR.ELEMENT_MODE_APPENDTO)
};
CKEDITOR.replaceAll=function(){for(var i=document.getElementsByTagName("textarea"),g=0;
g<i.length;
g++){var k=null,j=i[g];
if(j.name||j.id){if("string"==typeof arguments[0]){if(!(new RegExp("(?:^|\\s)"+arguments[0]+"(?:$|\\s)")).test(j.className)){continue
}}else{if("function"==typeof arguments[0]&&(k={},!1===arguments[0](j,k))){continue
}}this.replace(j,k)
}}};
CKEDITOR.editor.prototype.addMode=function(g,d){(this._.modes||(this._.modes={}))[g]=d
};
CKEDITOR.editor.prototype.setMode=function(i,g){var o=this,m=this._.modes;
if(i!=o.mode&&m&&m[i]){o.fire("beforeSetMode",i);
if(o.mode){var j=o.checkDirty(),m=o._.previousModeData,n,l=0;
o.fire("beforeModeUnload");
o.editable(0);
o._.previousMode=o.mode;
o._.previousModeData=n=o.getData(1);
"source"==o.mode&&m==n&&(o.fire("lockSnapshot",{forceUpdate:!0}),l=1);
o.ui.space("contents").setHtml("");
o.mode=""
}else{o._.previousModeData=o.getData(1)
}this._.modes[i](function(){o.mode=i;
void 0!==j&&!j&&o.resetDirty();
l?o.fire("unlockSnapshot"):"wysiwyg"==i&&o.fire("saveSnapshot");
setTimeout(function(){o.fire("mode");
g&&g.call(o)
},0)
})
}};
CKEDITOR.editor.prototype.resize=function(j,i,r,n){var l=this.container,p=this.ui.space("contents"),m=CKEDITOR.env.webkit&&this.document&&this.document.getWindow().$.frameElement;
n=n?this.container.getFirst(function(b){return b.type==CKEDITOR.NODE_ELEMENT&&b.hasClass("cke_inner")
}):l;
n.setSize("width",j,!0);
m&&(m.style.width="1%");
var o=(n.$.offsetHeight||0)-(p.$.clientHeight||0),l=Math.max(i-(r?0:o),0);
i=r?i+o:i;
p.setStyle("height",l+"px");
m&&(m.style.width="100%");
this.fire("resize",{outerHeight:i,contentsHeight:l,outerWidth:j||n.getSize("width")})
};
CKEDITOR.editor.prototype.getResizable=function(b){return b?this.ui.space("contents"):this.container
};
CKEDITOR.domReady(function(){CKEDITOR.replaceClass&&CKEDITOR.replaceAll(CKEDITOR.replaceClass)
})
})();
CKEDITOR.config.startupMode="wysiwyg";
(function(){function L(h){var d=h.editor,t=h.data.path,c=t.blockLimit,n=h.data.selection,k=n.getRanges()[0],q;
if(CKEDITOR.env.gecko||CKEDITOR.env.ie&&CKEDITOR.env.needsBrFiller){if(n=I(n,t)){n.appendBogus(),q=CKEDITOR.env.ie
}}D(d,t.block,c)&&k.collapsed&&!k.getCommonAncestor().isReadOnly()&&(t=k.clone(),t.enlarge(CKEDITOR.ENLARGE_BLOCK_CONTENTS),c=new CKEDITOR.dom.walker(t),c.guard=function(b){return !J(b)||b.type==CKEDITOR.NODE_COMMENT||b.isReadOnly()
},!c.checkForward()||t.checkStartOfBlock()&&t.checkEndOfBlock())&&(d=k.fixBlock(!0,d.activeEnterMode==CKEDITOR.ENTER_DIV?"div":"p"),CKEDITOR.env.needsBrFiller||(d=d.getFirst(J))&&d.type==CKEDITOR.NODE_TEXT&&CKEDITOR.tools.trim(d.getText()).match(/^(?:&nbsp;|\xa0)$/)&&d.remove(),q=1,h.cancel());
q&&k.select()
}function I(f,c){if(f.isFake){return 0
}var h=c.block||c.blockLimit,g=h&&h.getLast(J);
if(!(!h||!h.isBlockBoundary()||g&&g.type==CKEDITOR.NODE_ELEMENT&&g.isBlockBoundary()||h.is("pre")||h.getBogus())){return h
}}function K(d){var c=d.data.getTarget();
c.is("input")&&(c=c.getAttribute("type"),"submit"!=c&&"reset"!=c||d.data.preventDefault())
}function J(b){return E(b)&&i(b)
}function H(d,c){return function(b){var a=b.data.$.toElement||b.data.$.fromElement||b.data.$.relatedTarget;
(a=a&&a.nodeType==CKEDITOR.NODE_ELEMENT?new CKEDITOR.dom.element(a):null)&&(c.equals(a)||c.contains(a))||d.call(this,b)
}
}function G(f){function c(b){return function(a,d){d&&a.type==CKEDITOR.NODE_ELEMENT&&a.is(k)&&(q=a);
if(!(d||!J(a)||b&&m(a))){return !1
}}
}var q,n=f.getRanges()[0];
f=f.root;
var k={table:1,ul:1,ol:1,dl:1};
if(n.startPath().contains(k)){var h=n.clone();
h.collapse(1);
h.setStartAt(f,CKEDITOR.POSITION_AFTER_START);
f=new CKEDITOR.dom.walker(h);
f.guard=c();
f.checkBackward();
if(q){return h=n.clone(),h.collapse(),h.setEndAt(q,CKEDITOR.POSITION_AFTER_END),f=new CKEDITOR.dom.walker(h),f.guard=c(!0),q=!1,f.checkForward(),q
}}return null
}function D(e,d,f){return !1!==e.config.autoParagraph&&e.activeEnterMode!=CKEDITOR.ENTER_BR&&(e.editable().equals(f)&&!d||d&&"true"==d.getAttribute("contenteditable"))
}function B(b){return b.activeEnterMode!=CKEDITOR.ENTER_BR&&!1!==b.config.autoParagraph?b.activeEnterMode==CKEDITOR.ENTER_DIV?"div":"p":!1
}function s(d){var c=d.editor;
c.getSelection().scrollIntoView();
setTimeout(function(){c.fire("saveSnapshot")
},0)
}function p(f,e,h){var g=f.getCommonAncestor(e);
for(e=f=h?e:f;
(f=f.getParent())&&!g.equals(f)&&1==f.getChildCount();
){e=f
}e.remove()
}CKEDITOR.editable=CKEDITOR.tools.createClass({base:CKEDITOR.dom.element,$:function(d,c){this.base(c.$||c);
this.editor=d;
this.status="unloaded";
this.hasFocus=!1;
this.setup()
},proto:{focus:function(){var d;
if(CKEDITOR.env.webkit&&!this.hasFocus&&(d=this.editor._.previousActive||this.getDocument().getActive(),this.contains(d))){d.focus();
return
}try{this.$[CKEDITOR.env.ie&&this.getDocument().equals(CKEDITOR.document)?"setActive":"focus"]()
}catch(c){if(!CKEDITOR.env.ie){throw c
}}CKEDITOR.env.safari&&!this.isInline()&&(d=CKEDITOR.document.getActive(),d.equals(this.getWindow().getFrame())||this.getWindow().focus())
},on:function(e,d){var f=Array.prototype.slice.call(arguments,0);
CKEDITOR.env.ie&&/^focus|blur$/.exec(e)&&(e="focus"==e?"focusin":"focusout",d=H(d,this),f[0]=e,f[1]=d);
return CKEDITOR.dom.element.prototype.on.apply(this,f)
},attachListener:function(d){!this._.listeners&&(this._.listeners=[]);
var c=Array.prototype.slice.call(arguments,1),c=d.on.apply(d,c);
this._.listeners.push(c);
return c
},clearListeners:function(){var d=this._.listeners;
try{for(;
d.length;
){d.pop().removeListener()
}}catch(c){}},restoreAttrs:function(){var e=this._.attrChanges,d,f;
for(f in e){e.hasOwnProperty(f)&&(d=e[f],null!==d?this.setAttribute(f,d):this.removeAttribute(f))
}},attachClass:function(d){var c=this.getCustomData("classes");
this.hasClass(d)||(!c&&(c=[]),c.push(d),this.setCustomData("classes",c),this.addClass(d))
},changeAttr:function(e,d){var f=this.getAttribute(e);
d!==f&&(!this._.attrChanges&&(this._.attrChanges={}),e in this._.attrChanges||(this._.attrChanges[e]=f),this.setAttribute(e,d))
},insertText:function(b){this.editor.focus();
this.insertHtml(this.transformPlainTextToHtml(b),"text")
},transformPlainTextToHtml:function(d){var c=this.editor.getSelection().getStartElement().hasAscendant("pre",!0)?CKEDITOR.ENTER_BR:this.editor.activeEnterMode;
return CKEDITOR.tools.transformPlainTextToHtml(d,c)
},insertHtml:function(f,e,h){var g=this.editor;
g.focus();
g.fire("saveSnapshot");
h||(h=g.getSelection().getRanges()[0]);
x(this,e||"html",f,h);
h.select();
s(this);
this.editor.fire("afterInsertHtml",{})
},insertHtmlIntoRange:function(e,d,f){x(this,f||"html",e,d);
this.editor.fire("afterInsertHtml",{intoRange:d})
},insertElement:function(f,c){var n=this.editor;
n.focus();
n.fire("saveSnapshot");
var k=n.activeEnterMode,n=n.getSelection(),h=f.getName(),h=CKEDITOR.dtd.$block[h];
c||(c=n.getRanges()[0]);
this.insertElementIntoRange(f,c)&&(c.moveToPosition(f,CKEDITOR.POSITION_AFTER_END),h&&((h=f.getNext(function(b){return J(b)&&!m(b)
}))&&h.type==CKEDITOR.NODE_ELEMENT&&h.is(CKEDITOR.dtd.$block)?h.getDtd()["#"]?c.moveToElementEditStart(h):c.moveToElementEditEnd(f):h||k==CKEDITOR.ENTER_BR||(h=c.fixBlock(!0,k==CKEDITOR.ENTER_DIV?"div":"p"),c.moveToElementEditStart(h))));
n.selectRanges([c]);
s(this)
},insertElementIntoSelection:function(b){this.insertElement(b)
},insertElementIntoRange:function(k,h){var y=this.editor,w=y.config.enterMode,v=k.getName(),q=CKEDITOR.dtd.$block[v];
if(h.checkReadOnly()){return !1
}h.deleteContents(1);
h.startContainer.type==CKEDITOR.NODE_ELEMENT&&h.startContainer.is({tr:1,table:1,tbody:1,thead:1,tfoot:1})&&j(h);
var n,t;
if(q){for(;
(n=h.getCommonAncestor(0,1))&&(t=CKEDITOR.dtd[n.getName()])&&(!t||!t[v]);
){n.getName() in CKEDITOR.dtd.span?h.splitElement(n):h.checkStartOfBlock()&&h.checkEndOfBlock()?(h.setStartBefore(n),h.collapse(!0),n.remove()):h.splitBlock(w==CKEDITOR.ENTER_DIV?"div":"p",y.editable())
}}h.insertNode(k);
return !0
},setData:function(d,c){c||(d=this.editor.dataProcessor.toHtml(d));
this.setHtml(d);
this.fixInitialSelection();
"unloaded"==this.status&&(this.status="ready");
this.editor.fire("dataReady")
},getData:function(d){var c=this.getHtml();
d||(c=this.editor.dataProcessor.toDataFormat(c));
return c
},setReadOnly:function(b){this.setAttribute("contenteditable",!b)
},detach:function(){this.removeClass("cke_editable");
this.status="detached";
var b=this.editor;
this._.detach();
delete b.document;
delete b.window
},isInline:function(){return this.getDocument().equals(CKEDITOR.document)
},fixInitialSelection:function(){function e(){var a=f.getDocument().$,g=a.getSelection(),c;
e:if(g.anchorNode&&g.anchorNode==f.$){c=!0
}else{if(CKEDITOR.env.webkit&&(c=f.getDocument().getActive())&&c.equals(f)&&!g.anchorNode){c=!0;
break e
}c=void 0
}c&&(c=new CKEDITOR.dom.range(f),c.moveToElementEditStart(f),a=a.createRange(),a.setStart(c.startContainer.$,c.startOffset),a.collapse(!0),g.removeAllRanges(),g.addRange(a))
}function d(){var b=f.getDocument().$,g=b.selection,c=f.getDocument().getActive();
"None"==g.type&&c.equals(f)&&(g=new CKEDITOR.dom.range(f),b=b.body.createTextRange(),g.moveToElementEditStart(f),g=g.startContainer,g.type!=CKEDITOR.NODE_ELEMENT&&(g=g.getParent()),b.moveToElementText(g.$),b.collapse(!0),b.select())
}var f=this;
if(CKEDITOR.env.ie&&(9>CKEDITOR.env.version||CKEDITOR.env.quirks)){this.hasFocus&&(this.focus(),d())
}else{if(this.hasFocus){this.focus(),e()
}else{this.once("focus",function(){e()
},null,null,-999)
}}},getHtmlFromRange:function(b){if(b.collapsed){return new CKEDITOR.dom.documentFragment(b.document)
}b={doc:this.getDocument(),range:b.clone()};
o.eol.detect(b,this);
o.bogus.exclude(b);
o.cell.shrink(b);
b.fragment=b.range.cloneContents();
o.tree.rebuild(b,this);
o.eol.fix(b,this);
return new CKEDITOR.dom.documentFragment(b.fragment.$)
},extractHtmlFromRange:function(k,h){var w=l,v={range:k,doc:k.document},t=this.getHtmlFromRange(k);
if(k.collapsed){return k.optimize(),t
}k.enlarge(CKEDITOR.ENLARGE_INLINE,1);
w.table.detectPurge(v);
v.bookmark=k.createBookmark();
delete v.range;
var n=this.editor.createRange();
n.moveToPosition(v.bookmark.startNode,CKEDITOR.POSITION_BEFORE_START);
v.targetBookmark=n.createBookmark();
w.list.detectMerge(v,this);
w.table.detectRanges(v,this);
w.block.detectMerge(v,this);
v.tableContentsRanges?(w.table.deleteRanges(v),k.moveToBookmark(v.bookmark),v.range=k):(k.moveToBookmark(v.bookmark),v.range=k,k.extractContents(w.detectExtractMerge(v)));
k.moveToBookmark(v.targetBookmark);
k.optimize();
w.fixUneditableRangePosition(k);
w.list.merge(v,this);
w.table.purge(v,this);
w.block.merge(v,this);
if(h){w=k.startPath();
if(v=k.checkStartOfBlock()&&k.checkEndOfBlock()&&w.block&&!k.root.equals(w.block)){k:{var v=w.block.getElementsByTag("span"),n=0,q;
if(v){for(;
q=v.getItem(n++);
){if(!i(q)){v=!0;
break k
}}}v=!1
}v=!v
}v&&(k.moveToPosition(w.block,CKEDITOR.POSITION_BEFORE_START),w.block.remove())
}else{w.autoParagraph(this.editor,k),u(k.startContainer)&&k.startContainer.appendBogus()
}k.startContainer.mergeSiblings();
return t
},setup:function(){var c=this.editor;
this.attachListener(c,"beforeGetData",function(){var a=this.getData();
this.is("textarea")||!1!==c.config.ignoreEmptyParagraph&&(a=a.replace(r,function(e,d){return d
}));
c.setData(a,null,1)
},this);
this.attachListener(c,"getSnapshot",function(d){d.data=this.getData(1)
},this);
this.attachListener(c,"afterSetData",function(){this.setData(c.getData(1))
},this);
this.attachListener(c,"loadSnapshot",function(d){this.setData(d.data,1)
},this);
this.attachListener(c,"beforeFocus",function(){var a=c.getSelection();
(a=a&&a.getNative())&&"Control"==a.type||this.focus()
},this);
this.attachListener(c,"insertHtml",function(d){this.insertHtml(d.data.dataValue,d.data.mode,d.data.range)
},this);
this.attachListener(c,"insertElement",function(d){this.insertElement(d.data)
},this);
this.attachListener(c,"insertText",function(d){this.insertText(d.data)
},this);
this.setReadOnly(c.readOnly);
this.attachClass("cke_editable");
c.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?this.attachClass("cke_editable_inline"):c.elementMode!=CKEDITOR.ELEMENT_MODE_REPLACE&&c.elementMode!=CKEDITOR.ELEMENT_MODE_APPENDTO||this.attachClass("cke_editable_themed");
this.attachClass("cke_contents_"+c.config.contentsLangDirection);
c.keystrokeHandler.blockedKeystrokes[8]=+c.readOnly;
c.keystrokeHandler.attach(this);
this.on("blur",function(){this.hasFocus=!1
},null,null,-1);
this.on("focus",function(){this.hasFocus=!0
},null,null,-1);
c.focusManager.add(this);
this.equals(CKEDITOR.document.getActive())&&(this.hasFocus=!0,c.once("contentDom",function(){c.focusManager.focus(this)
},this));
this.isInline()&&this.changeAttr("tabindex",c.tabIndex);
if(!this.is("textarea")){c.document=this.getDocument();
c.window=this.getWindow();
var k=c.document;
this.changeAttr("spellcheck",!c.config.disableNativeSpellChecker);
var g=c.config.contentsLangDirection;
this.getDirection(1)!=g&&this.changeAttr("dir",g);
var b=CKEDITOR.getCss();
b&&(g=k.getHead(),g.getCustomData("stylesheet")||(b=k.appendStyleText(b),b=new CKEDITOR.dom.element(b.ownerNode||b.owningElement),g.setCustomData("stylesheet",b),b.data("cke-temp",1)));
g=k.getCustomData("stylesheet_ref")||0;
k.setCustomData("stylesheet_ref",g+1);
this.setCustomData("cke_includeReadonly",!c.config.disableReadonlyStyling);
this.attachListener(this,"click",function(e){e=e.data;
var d=(new CKEDITOR.dom.elementPath(e.getTarget(),this)).contains("a");
d&&2!=e.$.button&&d.isReadOnly()&&e.preventDefault()
});
var f={8:1,46:1};
this.attachListener(c,"key",function(y){if(c.readOnly){return !0
}var w=y.data.domEvent.getKey(),v;
if(w in f){y=c.getSelection();
var t,n=y.getRanges()[0],h=n.startPath(),a,z,q,w=8==w;
CKEDITOR.env.ie&&11>CKEDITOR.env.version&&(t=y.getSelectedElement())||(t=G(y))?(c.fire("saveSnapshot"),n.moveToPosition(t,CKEDITOR.POSITION_BEFORE_START),t.remove(),n.select(),c.fire("saveSnapshot"),v=1):n.collapsed&&((a=h.block)&&(q=a[w?"getPrevious":"getNext"](E))&&q.type==CKEDITOR.NODE_ELEMENT&&q.is("table")&&n[w?"checkStartOfBlock":"checkEndOfBlock"]()?(c.fire("saveSnapshot"),n[w?"checkEndOfBlock":"checkStartOfBlock"]()&&a.remove(),n["moveToElementEdit"+(w?"End":"Start")](q),n.select(),c.fire("saveSnapshot"),v=1):h.blockLimit&&h.blockLimit.is("td")&&(z=h.blockLimit.getAscendant("table"))&&n.checkBoundaryOfElement(z,w?CKEDITOR.START:CKEDITOR.END)&&(q=z[w?"getPrevious":"getNext"](E))?(c.fire("saveSnapshot"),n["moveToElementEdit"+(w?"End":"Start")](q),n.checkStartOfBlock()&&n.checkEndOfBlock()?q.remove():n.select(),c.fire("saveSnapshot"),v=1):(z=h.contains(["td","th","caption"]))&&n.checkBoundaryOfElement(z,w?CKEDITOR.START:CKEDITOR.END)&&(v=1))
}return !v
});
c.blockless&&CKEDITOR.env.ie&&CKEDITOR.env.needsBrFiller&&this.attachListener(this,"keyup",function(a){a.data.getKeystroke() in f&&!this.getFirst(J)&&(this.appendBogus(),a=c.createRange(),a.moveToPosition(this,CKEDITOR.POSITION_AFTER_START),a.select())
});
this.attachListener(this,"dblclick",function(a){if(c.readOnly){return !1
}a={element:a.data.getTarget()};
c.fire("doubleclick",a)
});
CKEDITOR.env.ie&&this.attachListener(this,"click",K);
CKEDITOR.env.ie&&!CKEDITOR.env.edge||this.attachListener(this,"mousedown",function(a){var d=a.data.getTarget();
d.is("img","hr","input","textarea","select")&&!d.isReadOnly()&&(c.getSelection().selectElement(d),d.is("input","textarea","select")&&a.data.preventDefault())
});
CKEDITOR.env.edge&&this.attachListener(this,"mouseup",function(a){(a=a.data.getTarget())&&a.is("img")&&c.getSelection().selectElement(a)
});
CKEDITOR.env.gecko&&this.attachListener(this,"mouseup",function(a){if(2==a.data.$.button&&(a=a.data.getTarget(),!a.getOuterHtml().replace(r,""))){var d=c.createRange();
d.moveToElementEditStart(a);
d.select(!0)
}});
CKEDITOR.env.webkit&&(this.attachListener(this,"click",function(d){d.data.getTarget().is("input","select")&&d.data.preventDefault()
}),this.attachListener(this,"mouseup",function(d){d.data.getTarget().is("input","textarea")&&d.data.preventDefault()
}));
CKEDITOR.env.webkit&&this.attachListener(c,"key",function(h){if(c.readOnly){return !0
}h=h.data.domEvent.getKey();
if(h in f){var v=8==h,t=c.getSelection().getRanges()[0];
h=t.startPath();
if(t.collapsed){c:{var q=h.block;
if(q&&t[v?"checkStartOfBlock":"checkEndOfBlock"]()&&t.moveToClosestEditablePosition(q,!v)&&t.collapsed){if(t.startContainer.type==CKEDITOR.NODE_ELEMENT){var n=t.startContainer.getChild(t.startOffset-(v?1:0));
if(n&&n.type==CKEDITOR.NODE_ELEMENT&&n.is("hr")){c.fire("saveSnapshot");
n.remove();
h=!0;
break c
}}t=t.startPath().block;
if(!t||t&&t.contains(q)){h=void 0
}else{c.fire("saveSnapshot");
var a;
(a=(v?t:q).getBogus())&&a.remove();
a=c.getSelection();
n=a.createBookmarks();
(v?q:t).moveChildren(v?t:q,!1);
h.lastElement.mergeSiblings();
p(q,t,!v);
a.selectBookmarks(n);
h=!0
}}else{h=!1
}}}else{v=t,a=h.block,t=v.endPath().block,a&&t&&!a.equals(t)?(c.fire("saveSnapshot"),(q=a.getBogus())&&q.remove(),v.enlarge(CKEDITOR.ENLARGE_INLINE),v.deleteContents(),t.getParent()&&(t.moveChildren(a,!1),h.lastElement.mergeSiblings(),p(a,t,!0)),v=c.getSelection().getRanges()[0],v.collapse(1),v.optimize(),""===v.startContainer.getHtml()&&v.startContainer.appendBogus(),v.select(),h=!0):h=!1
}if(!h){return
}c.getSelection().scrollIntoView();
c.fire("saveSnapshot");
return !1
}},this,null,100)
}}},_:{detach:function(){this.editor.setData(this.editor.getData(),0,1);
this.clearListeners();
this.restoreAttrs();
var e;
if(e=this.removeCustomData("classes")){for(;
e.length;
){this.removeClass(e.pop())
}}if(!this.is("textarea")){e=this.getDocument();
var d=e.getHead();
if(d.getCustomData("stylesheet")){var f=e.getCustomData("stylesheet_ref");
--f?e.setCustomData("stylesheet_ref",f):(e.removeCustomData("stylesheet_ref"),d.removeCustomData("stylesheet").remove())
}}this.editor.fire("contentDomUnload");
delete this.editor
}}});
CKEDITOR.editor.prototype.editable=function(d){var c=this._.editable;
if(c&&d){return 0
}arguments.length&&(c=this._.editable=d?d instanceof CKEDITOR.editable?d:new CKEDITOR.editable(this,d):(c&&c.detach(),null));
return c
};
CKEDITOR.on("instanceLoaded",function(a){var d=a.editor;
d.on("insertElement",function(b){b=b.data;
b.type==CKEDITOR.NODE_ELEMENT&&(b.is("input")||b.is("textarea"))&&("false"!=b.getAttribute("contentEditable")&&b.data("cke-editable",b.hasAttribute("contenteditable")?"true":"1"),b.setAttribute("contentEditable",!1))
});
d.on("selectionChange",function(c){if(!d.readOnly){var e=d.getSelection();
e&&!e.isLocked&&(e=d.checkDirty(),d.fire("lockSnapshot"),L(c),d.fire("unlockSnapshot"),!e&&d.resetDirty())
}})
});
CKEDITOR.on("instanceCreated",function(d){var c=d.editor;
c.on("mode",function(){var b=c.editable();
if(b&&b.isInline()){var h=c.title;
b.changeAttr("role","textbox");
b.changeAttr("aria-label",h);
h&&b.changeAttr("title",h);
var g=c.fire("ariaEditorHelpLabel",{}).label;
if(g&&(h=this.ui.space(this.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?"top":"contents"))){var f=CKEDITOR.tools.getNextId(),g=CKEDITOR.dom.element.createFromHtml('\x3cspan id\x3d"'+f+'" class\x3d"cke_voice_label"\x3e'+g+"\x3c/span\x3e");
h.append(g);
b.changeAttr("aria-describedby",f)
}}})
});
CKEDITOR.addCss(".cke_editable{cursor:text}.cke_editable img,.cke_editable input,.cke_editable textarea{cursor:default}");
var E=CKEDITOR.dom.walker.whitespaces(!0),i=CKEDITOR.dom.walker.bookmark(!1,!0),u=CKEDITOR.dom.walker.empty(),m=CKEDITOR.dom.walker.bogus(),r=/(^|<body\b[^>]*>)\s*<(p|div|address|h\d|center|pre)[^>]*>\s*(?:<br[^>]*>|&nbsp;|\u00A0|&#160;)?\s*(:?<\/\2>)?\s*(?=$|<\/body>)/gi,x=function(){function C(a){return a.type==CKEDITOR.NODE_ELEMENT
}function A(V,U){var T,O,R,q,S=[],Q=U.range.startContainer;
T=U.range.startPath();
for(var Q=h[Q.getName()],b=0,a=V.getChildren(),P=a.count(),X=-1,N=-1,Y=0,W=T.contains(h.$list);
b<P;
++b){T=a.getItem(b),C(T)?(R=T.getName(),W&&R in CKEDITOR.dtd.$list?S=S.concat(A(T,U)):(q=!!Q[R],"br"!=R||!T.data("cke-eol")||b&&b!=P-1||(Y=(O=b?S[b-1].node:a.getItem(b+1))&&(!C(O)||!O.is("br")),O=O&&C(O)&&h.$block[O.getName()]),-1!=X||q||(X=b),q||(N=b),S.push({isElement:1,isLineBreak:Y,isBlock:T.isBlockBoundary(),hasBlockSibling:O,node:T,name:R,allowed:q}),O=Y=0)):S.push({isElement:0,node:T,allowed:1})
}-1<X&&(S[X].firstNotAllowed=1);
-1<N&&(S[N].lastNotAllowed=1);
return S
}function z(Q,P){var O=[],n=Q.getChildren(),d=n.count(),t,N=0,q=h[P],a=!Q.is(h.$inline)||Q.is("br");
for(a&&O.push(" ");
N<d;
N++){t=n.getItem(N),C(t)&&!t.is(q)?O=O.concat(z(t,P)):O.push(t)
}a&&O.push(" ");
return O
}function y(a){return C(a.startContainer)&&a.startContainer.getChild(a.startOffset-1)
}function v(a){return a&&C(a)&&(a.is(h.$removeEmpty)||a.is("a")&&!a.isBlockBoundary())
}function w(f,O,N,t){var n=f.clone(),a,q;
n.setEndAt(O,CKEDITOR.POSITION_BEFORE_END);
(a=(new CKEDITOR.dom.walker(n)).next())&&C(a)&&k[a.getName()]&&(q=a.getPrevious())&&C(q)&&!q.getParent().equals(f.startContainer)&&N.contains(q)&&t.contains(a)&&a.isIdentical(q)&&(a.moveChildren(q),a.remove(),w(f,O,N,t))
}function F(a,q){function n(d,e){if(e.isBlock&&e.isElement&&!e.node.is("br")&&C(d)&&d.is("br")){return d.remove(),1
}}var g=q.endContainer.getChild(q.endOffset),f=q.endContainer.getChild(q.endOffset-1);
g&&n(g,a[a.length-1]);
f&&n(f,a[0])&&(q.setEnd(q.endContainer,q.endOffset-1),q.collapse())
}var h=CKEDITOR.dtd,k={p:1,div:1,h1:1,h2:1,h3:1,h4:1,h5:1,h6:1,ul:1,ol:1,li:1,pre:1,dl:1,blockquote:1},M={p:1,div:1,h1:1,h2:1,h3:1,h4:1,h5:1,h6:1},c=CKEDITOR.tools.extend({},h.$inline);
delete c.br;
return function(am,ad,ae,ac){var ai=am.editor,T=!1;
"unfiltered_html"==ad&&(ad="html",T=!0);
if(!ac.checkReadOnly()){var aa=(new CKEDITOR.dom.elementPath(ac.startContainer,ac.root)).blockLimit||ac.root;
am={type:ad,dontFilter:T,editable:am,editor:ai,range:ac,blockLimit:aa,mergeCandidates:[],zombies:[]};
ad=am.range;
ac=am.mergeCandidates;
var S,ag;
"text"==am.type&&ad.shrink(CKEDITOR.SHRINK_ELEMENT,!0,!1)&&(S=CKEDITOR.dom.element.createFromHtml("\x3cspan\x3e\x26nbsp;\x3c/span\x3e",ad.document),ad.insertNode(S),ad.setStartAfter(S));
T=new CKEDITOR.dom.elementPath(ad.startContainer);
am.endPath=aa=new CKEDITOR.dom.elementPath(ad.endContainer);
if(!ad.collapsed){var ai=aa.block||aa.blockLimit,ab=ad.getCommonAncestor();
ai&&!ai.equals(ab)&&!ai.contains(ab)&&ad.checkEndOfBlock()&&am.zombies.push(ai);
ad.deleteContents()
}for(;
(ag=y(ad))&&C(ag)&&ag.isBlockBoundary()&&T.contains(ag);
){ad.moveToPosition(ag,CKEDITOR.POSITION_BEFORE_END)
}w(ad,am.blockLimit,T,aa);
S&&(ad.setEndBefore(S),ad.collapse(),S.remove());
S=ad.startPath();
if(ai=S.contains(v,!1,1)){ad.splitElement(ai),am.inlineStylesRoot=ai,am.inlineStylesPeak=S.lastElement
}S=ad.createBookmark();
(ai=S.startNode.getPrevious(J))&&C(ai)&&v(ai)&&ac.push(ai);
(ai=S.startNode.getNext(J))&&C(ai)&&v(ai)&&ac.push(ai);
for(ai=S.startNode;
(ai=ai.getParent())&&v(ai);
){ac.push(ai)
}ad.moveToBookmark(S);
if(S=ae){S=am.range;
if("text"==am.type&&am.inlineStylesRoot){ag=am.inlineStylesPeak;
ad=ag.getDocument().createText("{cke-peak}");
for(ac=am.inlineStylesRoot.getParent();
!ag.equals(ac);
){ad=ad.appendTo(ag.clone()),ag=ag.getParent()
}ae=ad.getOuterHtml().split("{cke-peak}").join(ae)
}ag=am.blockLimit.getName();
if(/^\s+|\s+$/.test(ae)&&"span" in CKEDITOR.dtd[ag]){var ak='\x3cspan data-cke-marker\x3d"1"\x3e\x26nbsp;\x3c/span\x3e';
ae=ak+ae+ak
}ae=am.editor.dataProcessor.toHtml(ae,{context:null,fixForBody:!1,protectedWhitespaces:!!ak,dontFilter:am.dontFilter,filter:am.editor.activeFilter,enterMode:am.editor.activeEnterMode});
ag=S.document.createElement("body");
ag.setHtml(ae);
ak&&(ag.getFirst().remove(),ag.getLast().remove());
if((ak=S.startPath().block)&&(1!=ak.getChildCount()||!ak.getBogus())){C:{var W;
if(1==ag.getChildCount()&&C(W=ag.getFirst())&&W.is(M)&&!W.hasAttribute("contenteditable")){ak=W.getElementsByTag("*");
S=0;
for(ac=ak.count();
S<ac;
S++){if(ad=ak.getItem(S),!ad.is(c)){break C
}}W.moveChildren(W.getParent(1));
W.remove()
}}}am.dataWrapper=ag;
S=ae
}if(S){W=am.range;
S=W.document;
var af;
ag=am.blockLimit;
ac=0;
var aj,ak=[],P,ah;
ae=ai=0;
var g,d;
ad=W.startContainer;
var T=am.endPath.elements[0],b,aa=T.getPosition(ad),ab=!!T.getCommonAncestor(ad)&&aa!=CKEDITOR.POSITION_IDENTICAL&&!(aa&CKEDITOR.POSITION_CONTAINS+CKEDITOR.POSITION_IS_CONTAINED);
ad=A(am.dataWrapper,am);
for(F(ad,W);
ac<ad.length;
ac++){aa=ad[ac];
if(af=aa.isLineBreak){af=W;
g=ag;
var f=void 0,t=void 0;
aa.hasBlockSibling?af=1:(f=af.startContainer.getAscendant(h.$block,1))&&f.is({div:1,p:1})?(t=f.getPosition(g),t==CKEDITOR.POSITION_IDENTICAL||t==CKEDITOR.POSITION_CONTAINS?af=0:(g=af.splitElement(f),af.moveToPosition(g,CKEDITOR.POSITION_AFTER_START),af=1)):af=0
}if(af){ae=0<ac
}else{af=W.startPath();
!aa.isBlock&&D(am.editor,af.block,af.blockLimit)&&(ah=B(am.editor))&&(ah=S.createElement(ah),ah.appendBogus(),W.insertNode(ah),CKEDITOR.env.needsBrFiller&&(aj=ah.getBogus())&&aj.remove(),W.moveToPosition(ah,CKEDITOR.POSITION_BEFORE_END));
if((af=W.startPath().block)&&!af.equals(P)){if(aj=af.getBogus()){aj.remove(),ak.push(af)
}P=af
}aa.firstNotAllowed&&(ai=1);
if(ai&&aa.isElement){af=W.startContainer;
for(g=null;
af&&!h[af.getName()][aa.name];
){if(af.equals(ag)){af=null;
break
}g=af;
af=af.getParent()
}if(af){g&&(d=W.splitElement(g),am.zombies.push(d),am.zombies.push(g))
}else{g=ag.getName();
b=!ac;
af=ac==ad.length-1;
g=z(aa.node,g);
for(var f=[],t=g.length,al=0,O=void 0,a=0,e=-1;
al<t;
al++){O=g[al]," "==O?(a||b&&!al||(f.push(new CKEDITOR.dom.text(" ")),e=f.length),a=1):(f.push(O),a=0)
}af&&e==f.length&&f.pop();
b=f
}}if(b){for(;
af=b.pop();
){W.insertNode(af)
}b=0
}else{W.insertNode(aa.node)
}aa.lastNotAllowed&&ac<ad.length-1&&((d=ab?T:d)&&W.setEndAt(d,CKEDITOR.POSITION_AFTER_START),ai=0);
W.collapse()
}}1!=ad.length?aj=!1:(aj=ad[0],aj=aj.isElement&&"false"==aj.node.getAttribute("contenteditable"));
aj&&(ae=!0,af=ad[0].node,W.setStartAt(af,CKEDITOR.POSITION_BEFORE_START),W.setEndAt(af,CKEDITOR.POSITION_AFTER_END));
am.dontMoveCaret=ae;
am.bogusNeededBlocks=ak
}aj=am.range;
var q;
d=am.bogusNeededBlocks;
for(b=aj.createBookmark();
P=am.zombies.pop();
){P.getParent()&&(ah=aj.clone(),ah.moveToElementEditStart(P),ah.removeEmptyBlocksAtEnd())
}if(d){for(;
P=d.pop();
){CKEDITOR.env.needsBrFiller?P.appendBogus():P.append(aj.document.createText(" "))
}}for(;
P=am.mergeCandidates.pop();
){P.mergeSiblings()
}aj.moveToBookmark(b);
if(!am.dontMoveCaret){for(P=y(aj);
P&&C(P)&&!P.is(h.$empty);
){if(P.isBlockBoundary()){aj.moveToPosition(P,CKEDITOR.POSITION_BEFORE_END)
}else{if(v(P)&&P.getHtml().match(/(\s|&nbsp;)$/g)){q=null;
break
}q=aj.clone();
q.moveToPosition(P,CKEDITOR.POSITION_BEFORE_END)
}P=P.getLast(J)
}q&&aj.moveToRange(q)
}}}
}(),j=function(){function e(a){a=new CKEDITOR.dom.walker(a);
a.guard=function(g,c){if(c){return !1
}if(g.type==CKEDITOR.NODE_ELEMENT){return g.is(CKEDITOR.dtd.$tableContent)
}};
a.evaluator=function(b){return b.type==CKEDITOR.NODE_ELEMENT
};
return a
}function d(b,h,g){h=b.getDocument().createElement(h);
b.append(h,g);
return h
}function f(g){var c=g.count(),h;
for(c;
0<c--;
){h=g.getItem(c),CKEDITOR.tools.trim(h.getHtml())||(h.appendBogus(),CKEDITOR.env.ie&&9>CKEDITOR.env.version&&h.getChildCount()&&h.getFirst().remove())
}}return function(h){var c=h.startContainer,a=c.getAscendant("table",1),b=!1;
f(a.getElementsByTag("td"));
f(a.getElementsByTag("th"));
a=h.clone();
a.setStart(c,0);
a=e(a).lastBackward();
a||(a=h.clone(),a.setEndAt(c,CKEDITOR.POSITION_BEFORE_END),a=e(a).lastForward(),b=!0);
a||(a=c);
a.is("table")?(h.setStartAt(a,CKEDITOR.POSITION_BEFORE_START),h.collapse(!0),a.remove()):(a.is({tbody:1,thead:1,tfoot:1})&&(a=d(a,"tr",b)),a.is("tr")&&(a=d(a,a.getParent().is("thead")?"th":"td",b)),(c=a.getBogus())&&c.remove(),h.moveToPosition(a,b?CKEDITOR.POSITION_AFTER_START:CKEDITOR.POSITION_BEFORE_END))
}
}(),o={eol:{detect:function(k,h){var w=k.range,v=w.clone(),t=w.clone(),n=new CKEDITOR.dom.elementPath(w.startContainer,h),q=new CKEDITOR.dom.elementPath(w.endContainer,h);
v.collapse(1);
t.collapse();
n.block&&v.checkBoundaryOfElement(n.block,CKEDITOR.END)&&(w.setStartAfter(n.block),k.prependEolBr=1);
q.block&&t.checkBoundaryOfElement(q.block,CKEDITOR.START)&&(w.setEndBefore(q.block),k.appendEolBr=1)
},fix:function(f,e){var h=e.getDocument(),g;
f.appendEolBr&&(g=this.createEolBr(h),f.fragment.append(g));
!f.prependEolBr||g&&!g.getPrevious()||f.fragment.append(this.createEolBr(h),1)
},createEolBr:function(b){return b.createElement("br",{attributes:{"data-cke-eol":1}})
}},bogus:{exclude:function(e){var d=e.range.getBoundaryNodes(),f=d.startNode,d=d.endNode;
!d||!m(d)||f&&f.equals(d)||e.range.setEndBefore(d)
}},tree:{rebuild:function(C,A){var z=C.range,y=z.getCommonAncestor(),w=new CKEDITOR.dom.elementPath(y,A),t=new CKEDITOR.dom.elementPath(z.startContainer,A),z=new CKEDITOR.dom.elementPath(z.endContainer,A),v;
y.type==CKEDITOR.NODE_TEXT&&(y=y.getParent());
if(w.blockLimit.is({tr:1,table:1})){var q=w.contains("table").getParent();
v=function(b){return !b.equals(q)
}
}else{if(w.block&&w.block.is(CKEDITOR.dtd.$listItem)&&(t=t.contains(CKEDITOR.dtd.$list),z=z.contains(CKEDITOR.dtd.$list),!t.equals(z))){var n=w.contains(CKEDITOR.dtd.$list).getParent();
v=function(b){return !b.equals(n)
}
}}v||(v=function(b){return !b.equals(w.block)&&!b.equals(w.blockLimit)
});
this.rebuildFragment(C,A,y,v)
},rebuildFragment:function(g,f,n,k){for(var h;
n&&!n.equals(f)&&k(n);
){h=n.clone(0,1),g.fragment.appendTo(h),g.fragment=h,n=n.getParent()
}}},cell:{shrink:function(g){g=g.range;
var f=g.startContainer,n=g.endContainer,k=g.startOffset,h=g.endOffset;
f.type==CKEDITOR.NODE_ELEMENT&&f.equals(n)&&f.is("tr")&&++k==h&&g.shrink(CKEDITOR.SHRINK_TEXT)
}}},l=function(){function e(a,h){var g=a.getParent();
if(g.is(CKEDITOR.dtd.$inline)){a[h?"insertBefore":"insertAfter"](g)
}}function d(k,h,b){e(h);
e(b,1);
for(var a;
a=b.getNext();
){a.insertAfter(h),h=a
}u(k)&&k.remove()
}function f(g,c){var h=new CKEDITOR.dom.range(g);
h.setStartAfter(c.startNode);
h.setEndBefore(c.endNode);
return h
}return{list:{detectMerge:function(k,c){var v=f(c,k.bookmark),t=v.startPath(),n=v.endPath(),h=t.contains(CKEDITOR.dtd.$list),q=n.contains(CKEDITOR.dtd.$list);
k.mergeList=h&&q&&h.getParent().equals(q.getParent())&&!h.equals(q);
k.mergeListItems=t.block&&n.block&&t.block.is(CKEDITOR.dtd.$listItem)&&n.block.is(CKEDITOR.dtd.$listItem);
if(k.mergeList||k.mergeListItems){v=v.clone(),v.setStartBefore(k.bookmark.startNode),v.setEndAfter(k.bookmark.endNode),k.mergeListBookmark=v.createBookmark()
}},merge:function(b,y){if(b.mergeListBookmark){var w=b.mergeListBookmark.startNode,v=b.mergeListBookmark.endNode,n=new CKEDITOR.dom.elementPath(w,y),q=new CKEDITOR.dom.elementPath(v,y);
if(b.mergeList){var t=n.contains(CKEDITOR.dtd.$list),k=q.contains(CKEDITOR.dtd.$list);
t.equals(k)||(k.moveChildren(t),k.remove())
}b.mergeListItems&&(n=n.contains(CKEDITOR.dtd.$listItem),q=q.contains(CKEDITOR.dtd.$listItem),n.equals(q)||d(q,w,v));
w.remove();
v.remove()
}}},block:{detectMerge:function(h,g){if(!h.tableContentsRanges&&!h.mergeListBookmark){var k=new CKEDITOR.dom.range(g);
k.setStartBefore(h.bookmark.startNode);
k.setEndAfter(h.bookmark.endNode);
h.mergeBlockBookmark=k.createBookmark()
}},merge:function(b,t){if(b.mergeBlockBookmark&&!b.purgeTableBookmark){var q=b.mergeBlockBookmark.startNode,n=b.mergeBlockBookmark.endNode,h=new CKEDITOR.dom.elementPath(q,t),k=new CKEDITOR.dom.elementPath(n,t),h=h.block,k=k.block;
h&&k&&!h.equals(k)&&d(k,q,n);
q.remove();
n.remove()
}}},table:function(){function g(v){var t=[],k,b=new CKEDITOR.dom.walker(v),a=v.startPath().contains(h),n=v.endPath().contains(h),q={};
b.guard=function(y,w){if(y.type==CKEDITOR.NODE_ELEMENT){var z="visited_"+(w?"out":"in");
if(y.getCustomData(z)){return
}CKEDITOR.dom.element.setMarker(q,y,z,1)
}if(w&&a&&y.equals(a)){k=v.clone(),k.setEndAt(a,CKEDITOR.POSITION_BEFORE_END),t.push(k)
}else{if(!w&&n&&y.equals(n)){k=v.clone(),k.setStartAt(n,CKEDITOR.POSITION_AFTER_START),t.push(k)
}else{if(z=!w){z=y.type==CKEDITOR.NODE_ELEMENT&&y.is(h)&&(!a||c(y,a))&&(!n||c(y,n))
}z&&(k=v.clone(),k.selectNodeContents(y),t.push(k))
}}};
b.lastForward();
CKEDITOR.dom.element.clearAllMarkers(q);
return t
}function c(b,q){var n=CKEDITOR.POSITION_CONTAINS+CKEDITOR.POSITION_IS_CONTAINED,k=b.getPosition(q);
return k===CKEDITOR.POSITION_IDENTICAL?!1:0===(k&n)
}var h={td:1,th:1,caption:1};
return{detectPurge:function(n){var k=n.range,v=k.clone();
v.enlarge(CKEDITOR.ENLARGE_ELEMENT);
var v=new CKEDITOR.dom.walker(v),t=0;
v.evaluator=function(b){b.type==CKEDITOR.NODE_ELEMENT&&b.is(h)&&++t
};
v.checkForward();
if(1<t){var v=k.startPath().contains("table"),q=k.endPath().contains("table");
v&&q&&k.checkBoundaryOfElement(v,CKEDITOR.START)&&k.checkBoundaryOfElement(q,CKEDITOR.END)&&(k=n.range.clone(),k.setStartBefore(v),k.setEndAfter(q),n.purgeTableBookmark=k.createBookmark())
}},detectRanges:function(w,t){var b=f(t,w.bookmark),v=b.clone(),q,n,a=b.getCommonAncestor();
a.is(CKEDITOR.dtd.$tableContent)&&!a.is(h)&&(a=a.getAscendant("table",!0));
n=a;
a=new CKEDITOR.dom.elementPath(b.startContainer,n);
n=new CKEDITOR.dom.elementPath(b.endContainer,n);
a=a.contains("table");
n=n.contains("table");
if(a||n){a&&n&&c(a,n)?(w.tableSurroundingRange=v,v.setStartAt(a,CKEDITOR.POSITION_AFTER_END),v.setEndAt(n,CKEDITOR.POSITION_BEFORE_START),v=b.clone(),v.setEndAt(a,CKEDITOR.POSITION_AFTER_END),q=b.clone(),q.setStartAt(n,CKEDITOR.POSITION_BEFORE_START),q=g(v).concat(g(q))):a?n||(w.tableSurroundingRange=v,v.setStartAt(a,CKEDITOR.POSITION_AFTER_END),b.setEndAt(a,CKEDITOR.POSITION_AFTER_END)):(w.tableSurroundingRange=v,v.setEndAt(n,CKEDITOR.POSITION_BEFORE_START),b.setStartAt(n,CKEDITOR.POSITION_AFTER_START)),w.tableContentsRanges=q?q:g(b)
}},deleteRanges:function(n){for(var k;
k=n.tableContentsRanges.pop();
){k.extractContents(),u(k.startContainer)&&k.startContainer.appendBogus()
}n.tableSurroundingRange&&n.tableSurroundingRange.extractContents()
},purge:function(n){if(n.purgeTableBookmark){var k=n.doc,q=n.range.clone(),k=k.createElement("p");
k.insertBefore(n.purgeTableBookmark.startNode);
q.moveToBookmark(n.purgeTableBookmark);
q.deleteContents();
n.range.moveToPosition(k,CKEDITOR.POSITION_AFTER_START)
}}}
}(),detectExtractMerge:function(b){return !(b.range.startPath().contains(CKEDITOR.dtd.$listItem)&&b.range.endPath().contains(CKEDITOR.dtd.$listItem))
},fixUneditableRangePosition:function(b){b.startContainer.getDtd()["#"]||b.moveToClosestEditablePosition(null,!0)
},autoParagraph:function(h,g){var n=g.startPath(),k;
D(h,n.block,n.blockLimit)&&(k=B(h))&&(k=g.document.createElement(k),k.appendBogus(),g.insertNode(k),g.moveToPosition(k,CKEDITOR.POSITION_AFTER_START))
}}
}()
})();
(function(){function N(){var d=this._.fakeSelection,c;
d&&(c=this.getSelection(1),c&&c.isHidden()||(d.reset(),d=0));
if(!d&&(d=c||this.getSelection(1),!d||d.getType()==CKEDITOR.SELECTION_NONE)){return
}this.fire("selectionCheck",d);
c=this.elementPath();
c.compare(this._.selectionPreviousPath)||(CKEDITOR.env.webkit&&(this._.previousActive=this.document.getActive()),this._.selectionPreviousPath=c,this.fire("selectionChange",{selection:d,path:c}))
}function K(){s=!0;
m||(M.call(this),m=CKEDITOR.tools.setTimeout(M,200,this))
}function M(){m=null;
s&&(CKEDITOR.tools.setTimeout(N,0,this),s=!1)
}function L(b){return D(b)||b.type==CKEDITOR.NODE_ELEMENT&&!b.is(CKEDITOR.dtd.$empty)?!0:!1
}function J(f){function c(b,a){return b&&b.type!=CKEDITOR.NODE_TEXT?f.clone()["moveToElementEdit"+(a?"End":"Start")](b):!1
}if(!(f.root instanceof CKEDITOR.editable)){return !1
}var n=f.startContainer,k=f.getPreviousNode(L,null,n),h=f.getNextNode(L,null,n);
return c(k)||c(h,1)||!(k||h||n.type==CKEDITOR.NODE_ELEMENT&&n.isBlockBoundary()&&n.getBogus())?!0:!1
}function I(b){return b.getCustomData("cke-fillingChar")
}function G(h,f){var t=h&&h.removeCustomData("cke-fillingChar");
if(t){if(!1!==f){var q,n=h.getDocument().getSelection().getNative(),k=n&&"None"!=n.type&&n.getRangeAt(0);
1<t.getLength()&&k&&k.intersectsNode(t.$)&&(q=x(n),k=n.focusNode==t.$&&0<n.focusOffset,n.anchorNode==t.$&&0<n.anchorOffset&&q[0].offset--,k&&q[1].offset--)
}t.setText(E(t.getText()));
q&&r(h.getDocument().$,q)
}}function E(b){return b.replace(/\u200B( )?/g,function(c){return c[1]?" ":""
})
}function x(b){return[{node:b.anchorNode,offset:b.anchorOffset},{node:b.focusNode,offset:b.focusOffset}]
}function r(f,e){var h=f.getSelection(),g=f.createRange();
g.setStart(e[0].node,e[0].offset);
g.collapse(!0);
h.removeAllRanges();
h.addRange(g);
h.extend(e[1].node,e[1].offset)
}function H(g){var f=CKEDITOR.dom.element.createFromHtml('\x3cdiv data-cke-hidden-sel\x3d"1" data-cke-temp\x3d"1" style\x3d"'+(CKEDITOR.env.ie?"display:none":"position:fixed;top:0;left:-1000px")+'"\x3e\x26nbsp;\x3c/div\x3e',g.document);
g.fire("lockSnapshot");
g.editable().append(f);
var n=g.getSelection(1),k=g.createRange(),h=n.root.on("selectionchange",function(b){b.cancel()
},null,null,0);
k.setStartAt(f,CKEDITOR.POSITION_AFTER_START);
k.setEndAt(f,CKEDITOR.POSITION_BEFORE_END);
n.selectRanges([k]);
h.removeListener();
g.fire("unlockSnapshot");
g._.hiddenSelectionContainer=f
}function i(d){var c={37:1,39:1,8:1,46:1};
return function(h){var f=h.data.getKeystroke();
if(c[f]){var b=d.getSelection().getRanges(),a=b[0];
1==b.length&&a.collapsed&&(f=a[38>f?"getPreviousEditableNode":"getNextEditableNode"]())&&f.type==CKEDITOR.NODE_ELEMENT&&"false"==f.getAttribute("contenteditable")&&(d.getSelection().fake(f),h.data.preventDefault(),h.cancel())
}}
}function B(n){for(var k=0;
k<n.length;
k++){var y=n[k];
y.getCommonAncestor().isReadOnly()&&n.splice(k,1);
if(!y.collapsed){if(y.startContainer.isReadOnly()){for(var w=y.startContainer,v;
w&&!((v=w.type==CKEDITOR.NODE_ELEMENT)&&w.is("body")||!w.isReadOnly());
){v&&"false"==w.getAttribute("contentEditable")&&y.setStartAfter(w),w=w.getParent()
}}w=y.startContainer;
v=y.endContainer;
var t=y.startOffset,u=y.endOffset,q=y.clone();
w&&w.type==CKEDITOR.NODE_TEXT&&(t>=w.getLength()?q.setStartAfter(w):q.setStartBefore(w));
v&&v.type==CKEDITOR.NODE_TEXT&&(u?q.setEndAfter(v):q.setEndBefore(v));
w=new CKEDITOR.dom.walker(q);
w.evaluator=function(b){if(b.type==CKEDITOR.NODE_ELEMENT&&b.isReadOnly()){var a=y.clone();
y.setEndBefore(b);
y.collapsed&&n.splice(k--,1);
b.getPosition(q.endContainer)&CKEDITOR.POSITION_CONTAINS||(a.setStartAfter(b),a.collapsed||n.splice(k+1,0,a));
return !0
}return !1
};
w.next()
}}return n
}var m,s,D=CKEDITOR.dom.walker.invisible(1),j=function(){function f(a){return function(b){var d=b.editor.createRange();
d.moveToClosestEditablePosition(b.selected,a)&&b.editor.getSelection().selectRanges([d]);
return !1
}
}function e(b){return function(a){var q=a.editor,n=q.createRange(),k;
(k=n.moveToClosestEditablePosition(a.selected,b))||(k=n.moveToClosestEditablePosition(a.selected,!b));
k&&q.getSelection().selectRanges([n]);
q.fire("saveSnapshot");
a.selected.remove();
k||(n.moveToElementEditablePosition(q.editable()),q.getSelection().selectRanges([n]));
q.fire("saveSnapshot");
return !1
}
}var h=f(),g=f(1);
return{37:h,38:h,39:g,40:g,8:e(),46:e(1)}
}();
CKEDITOR.on("instanceCreated",function(a){function f(){var b=d.getSelection();
b&&b.removeAllRanges()
}var d=a.editor;
d.on("contentDom",function(){function y(){C=new CKEDITOR.dom.selection(d.getSelection());
C.lock()
}function w(){q.removeListener("mouseup",w);
t.removeListener("mouseup",w);
var g=CKEDITOR.document.$.selection,c=g.createRange();
"None"!=g.type&&c.parentElement().ownerDocument==u.$&&c.select()
}var u=d.document,q=CKEDITOR.document,v=d.editable(),e=u.getBody(),t=u.getDocumentElement(),h=v.isInline(),z,C;
CKEDITOR.env.gecko&&v.attachListener(v,"focus",function(b){b.removeListener();
0!==z&&(b=d.getSelection().getNative())&&b.isCollapsed&&b.anchorNode==v.$&&(b=d.createRange(),b.moveToElementEditStart(v),b.select())
},null,null,-2);
v.attachListener(v,CKEDITOR.env.webkit?"DOMFocusIn":"focus",function(){z&&CKEDITOR.env.webkit&&(z=d._.previousActive&&d._.previousActive.equals(u.getActive()));
d.unlockSelection(z);
z=0
},null,null,-1);
v.attachListener(v,"mousedown",function(){z=0
});
if(CKEDITOR.env.ie||h){p?v.attachListener(v,"beforedeactivate",y,null,null,-1):v.attachListener(d,"selectionCheck",y,null,null,-1),v.attachListener(v,CKEDITOR.env.webkit?"DOMFocusOut":"blur",function(){d.lockSelection(C);
z=1
},null,null,-1),v.attachListener(v,"mousedown",function(){z=0
})
}if(CKEDITOR.env.ie&&!h){var A;
v.attachListener(v,"mousedown",function(b){2==b.data.$.button&&((b=d.document.getSelection())&&b.getType()!=CKEDITOR.SELECTION_NONE||(A=d.window.getScrollPosition()))
});
v.attachListener(v,"mouseup",function(b){2==b.data.$.button&&A&&(d.document.$.documentElement.scrollLeft=A.x,d.document.$.documentElement.scrollTop=A.y);
A=null
});
if("BackCompat"!=u.$.compatMode){if(CKEDITOR.env.ie7Compat||CKEDITOR.env.ie6Compat){t.on("mousedown",function(n){function k(b){b=b.data.$;
if(Q){var S=e.$.createTextRange();
try{S.moveToPoint(b.clientX,b.clientY)
}catch(g){}Q.setEndPoint(0>O.compareEndPoints("StartToStart",S)?"EndToEnd":"StartToStart",S);
Q.select()
}}function R(){t.removeListener("mousemove",k);
q.removeListener("mouseup",R);
t.removeListener("mouseup",R);
Q.select()
}n=n.data;
if(n.getTarget().is("html")&&n.$.y<t.$.clientHeight&&n.$.x<t.$.clientWidth){var Q=e.$.createTextRange();
try{Q.moveToPoint(n.$.clientX,n.$.clientY)
}catch(P){}var O=Q.duplicate();
t.on("mousemove",k);
q.on("mouseup",R);
t.on("mouseup",R)
}})
}if(7<CKEDITOR.env.version&&11>CKEDITOR.env.version){t.on("mousedown",function(b){b.data.getTarget().is("html")&&(q.on("mouseup",w),t.on("mouseup",w))
})
}}}v.attachListener(v,"selectionchange",N,d);
v.attachListener(v,"keyup",K,d);
v.attachListener(v,CKEDITOR.env.webkit?"DOMFocusIn":"focus",function(){d.forceNextSelectionCheck();
d.selectionChange(1)
});
if(h&&(CKEDITOR.env.webkit||CKEDITOR.env.gecko)){var F;
v.attachListener(v,"mousedown",function(){F=1
});
v.attachListener(u.getDocumentElement(),"mouseup",function(){F&&K.call(d);
F=0
})
}else{v.attachListener(CKEDITOR.env.ie?v:u.getDocumentElement(),"mouseup",K,d)
}CKEDITOR.env.webkit&&v.attachListener(u,"keydown",function(b){switch(b.data.getKey()){case 13:case 33:case 34:case 35:case 36:case 37:case 39:case 8:case 45:case 46:G(v)
}},null,null,-1);
v.attachListener(v,"keydown",i(d),null,null,-1)
});
d.on("setData",function(){d.unlockSelection();
CKEDITOR.env.webkit&&f()
});
d.on("contentDomUnload",function(){d.unlockSelection()
});
if(CKEDITOR.env.ie9Compat){d.on("beforeDestroy",f,null,null,9)
}d.on("dataReady",function(){delete d._.fakeSelection;
delete d._.hiddenSelectionContainer;
d.selectionChange(1)
});
d.on("loadSnapshot",function(){var e=CKEDITOR.dom.walker.nodeType(CKEDITOR.NODE_ELEMENT),c=d.editable().getLast(e);
c&&c.hasAttribute("data-cke-hidden-sel")&&(c.remove(),CKEDITOR.env.gecko&&(e=d.editable().getFirst(e))&&e.is("br")&&e.getAttribute("_moz_editor_bogus_node")&&e.remove())
},null,null,100);
d.on("key",function(g){if("wysiwyg"==d.mode){var e=d.getSelection();
if(e.isFake){var h=j[g.data.keyCode];
if(h){return h({editor:d,selected:e.getSelectedElement(),selection:e,keyEvent:g})
}}}})
});
CKEDITOR.on("instanceReady",function(h){function f(){var b=q.editable();
if(b&&(b=I(b))){var d=q.document.$.getSelection();
"None"==d.type||d.anchorNode!=b.$&&d.focusNode!=b.$||(k=x(d));
n=b.getText();
b.setText(E(n))
}}function t(){var b=q.editable();
b&&(b=I(b))&&(b.setText(n),k&&(r(q.document.$,k),k=null))
}var q=h.editor,n,k;
CKEDITOR.env.webkit&&(q.on("selectionChange",function(){var d=q.editable(),c=I(d);
c&&(c.getCustomData("ready")?G(d):c.setCustomData("ready",1))
},null,null,-1),q.on("beforeSetMode",function(){G(q.editable())
},null,null,-1),q.on("beforeUndoImage",f),q.on("afterUndoImage",t),q.on("beforeGetData",f,null,null,0),q.on("getData",t))
});
CKEDITOR.editor.prototype.selectionChange=function(a){(a?N:K).call(this)
};
CKEDITOR.editor.prototype.getSelection=function(b){return !this._.savedSelection&&!this._.fakeSelection||b?(b=this.editable())&&"wysiwyg"==this.mode?new CKEDITOR.dom.selection(b):null:this._.savedSelection||this._.fakeSelection
};
CKEDITOR.editor.prototype.lockSelection=function(b){b=b||this.getSelection(1);
return b.getType()!=CKEDITOR.SELECTION_NONE?(!b.isLocked&&b.lock(),this._.savedSelection=b,!0):!1
};
CKEDITOR.editor.prototype.unlockSelection=function(d){var c=this._.savedSelection;
return c?(c.unlock(d),delete this._.savedSelection,!0):!1
};
CKEDITOR.editor.prototype.forceNextSelectionCheck=function(){delete this._.selectionPreviousPath
};
CKEDITOR.dom.document.prototype.getSelection=function(){return new CKEDITOR.dom.selection(this)
};
CKEDITOR.dom.range.prototype.select=function(){var b=this.root instanceof CKEDITOR.editable?this.root.editor.getSelection():new CKEDITOR.dom.selection(this.root);
b.selectRanges([this]);
return b
};
CKEDITOR.SELECTION_NONE=1;
CKEDITOR.SELECTION_TEXT=2;
CKEDITOR.SELECTION_ELEMENT=3;
var p="function"!=typeof window.getSelection,l=1;
CKEDITOR.dom.selection=function(h){if(h instanceof CKEDITOR.dom.selection){var f=h;
h=h.root
}var t=h instanceof CKEDITOR.dom.element;
this.rev=f?f.rev:l++;
this.document=h instanceof CKEDITOR.dom.document?h:h.getDocument();
this.root=t?h:this.document.getBody();
this.isLocked=0;
this._={cache:{}};
if(f){return CKEDITOR.tools.extend(this._.cache,f._.cache),this.isFake=f.isFake,this.isLocked=f.isLocked,this
}h=this.getNative();
var q,n;
if(h){if(h.getRangeAt){q=(n=h.rangeCount&&h.getRangeAt(0))&&new CKEDITOR.dom.node(n.commonAncestorContainer)
}else{try{n=h.createRange()
}catch(k){}q=n&&CKEDITOR.dom.element.get(n.item&&n.item(0)||n.parentElement())
}}if(!q||q.type!=CKEDITOR.NODE_ELEMENT&&q.type!=CKEDITOR.NODE_TEXT||!this.root.equals(q)&&!this.root.contains(q)){this._.cache.type=CKEDITOR.SELECTION_NONE,this._.cache.startElement=null,this._.cache.selectedElement=null,this._.cache.selectedText="",this._.cache.ranges=new CKEDITOR.dom.rangeList
}return this
};
var o={img:1,hr:1,li:1,table:1,tr:1,td:1,th:1,embed:1,object:1,ol:1,ul:1,a:1,input:1,form:1,select:1,textarea:1,button:1,fieldset:1,thead:1,tfoot:1};
CKEDITOR.dom.selection.prototype={getNative:function(){return void 0!==this._.cache.nativeSel?this._.cache.nativeSel:this._.cache.nativeSel=p?this.document.$.selection:this.document.getWindow().$.getSelection()
},getType:p?function(){var g=this._.cache;
if(g.type){return g.type
}var f=CKEDITOR.SELECTION_NONE;
try{var n=this.getNative(),k=n.type;
"Text"==k&&(f=CKEDITOR.SELECTION_TEXT);
"Control"==k&&(f=CKEDITOR.SELECTION_ELEMENT);
n.createRange().parentElement()&&(f=CKEDITOR.SELECTION_TEXT)
}catch(h){}return g.type=f
}:function(){var f=this._.cache;
if(f.type){return f.type
}var e=CKEDITOR.SELECTION_TEXT,h=this.getNative();
if(!h||!h.rangeCount){e=CKEDITOR.SELECTION_NONE
}else{if(1==h.rangeCount){var h=h.getRangeAt(0),g=h.startContainer;
g==h.endContainer&&1==g.nodeType&&1==h.endOffset-h.startOffset&&o[g.childNodes[h.startOffset].nodeName.toLowerCase()]&&(e=CKEDITOR.SELECTION_ELEMENT)
}}return f.type=e
},getRanges:function(){var b=p?function(){function d(a){return(new CKEDITOR.dom.node(a)).getIndex()
}var c=function(O,C){O=O.duplicate();
O.collapse(C);
var A=O.parentElement();
if(!A.hasChildNodes()){return{container:A,offset:0}
}for(var z=A.children,w,y,t=O.duplicate(),v=0,P=z.length-1,u=-1,q,a;
v<=P;
){if(u=Math.floor((v+P)/2),w=z[u],t.moveToElementText(w),q=t.compareEndPoints("StartToStart",O),0<q){P=u-1
}else{if(0>q){v=u+1
}else{return{container:A,offset:d(w)}
}}}if(-1==u||u==z.length-1&&0>q){t.moveToElementText(A);
t.setEndPoint("StartToStart",O);
t=t.text.replace(/(\r\n|\r)/g,"\n").length;
z=A.childNodes;
if(!t){return w=z[z.length-1],w.nodeType!=CKEDITOR.NODE_TEXT?{container:A,offset:z.length}:{container:w,offset:w.nodeValue.length}
}for(A=z.length;
0<t&&0<A;
){y=z[--A],y.nodeType==CKEDITOR.NODE_TEXT&&(a=y,t-=y.nodeValue.length)
}return{container:a,offset:-t}
}t.collapse(0<q?!0:!1);
t.setEndPoint(0<q?"StartToStart":"EndToStart",O);
t=t.text.replace(/(\r\n|\r)/g,"\n").length;
if(!t){return{container:A,offset:d(w)+(0<q?0:1)}
}for(;
0<t;
){try{y=w[0<q?"previousSibling":"nextSibling"],y.nodeType==CKEDITOR.NODE_TEXT&&(t-=y.nodeValue.length,a=y),w=y
}catch(F){return{container:A,offset:d(w)}
}}return{container:a,offset:0<q?-t:a.nodeValue.length+t}
};
return function(){var k=this.getNative(),v=k&&k.createRange(),u=this.getType();
if(!k){return[]
}if(u==CKEDITOR.SELECTION_TEXT){return k=new CKEDITOR.dom.range(this.root),u=c(v,!0),k.setStart(new CKEDITOR.dom.node(u.container),u.offset),u=c(v),k.setEnd(new CKEDITOR.dom.node(u.container),u.offset),k.endContainer.getPosition(k.startContainer)&CKEDITOR.POSITION_PRECEDING&&k.endOffset<=k.startContainer.getIndex()&&k.collapse(),[k]
}if(u==CKEDITOR.SELECTION_ELEMENT){for(var u=[],t=0;
t<v.length;
t++){for(var n=v.item(t),q=n.parentNode,h=0,k=new CKEDITOR.dom.range(this.root);
h<q.childNodes.length&&q.childNodes[h]!=n;
h++){}k.setStart(new CKEDITOR.dom.node(q),h);
k.setEnd(new CKEDITOR.dom.node(q),h+1);
u.push(k)
}return u
}return[]
}
}():function(){var g=[],f,n=this.getNative();
if(!n){return g
}for(var k=0;
k<n.rangeCount;
k++){var h=n.getRangeAt(k);
f=new CKEDITOR.dom.range(this.root);
f.setStart(new CKEDITOR.dom.node(h.startContainer),h.startOffset);
f.setEnd(new CKEDITOR.dom.node(h.endContainer),h.endOffset);
g.push(f)
}return g
};
return function(a){var f=this._.cache,e=f.ranges;
e||(f.ranges=e=new CKEDITOR.dom.rangeList(b.call(this)));
return a?B(new CKEDITOR.dom.rangeList(e.slice())):e
}
}(),getStartElement:function(){var e=this._.cache;
if(void 0!==e.startElement){return e.startElement
}var d;
switch(this.getType()){case CKEDITOR.SELECTION_ELEMENT:return this.getSelectedElement();
case CKEDITOR.SELECTION_TEXT:var f=this.getRanges()[0];
if(f){if(f.collapsed){d=f.startContainer,d.type!=CKEDITOR.NODE_ELEMENT&&(d=d.getParent())
}else{for(f.optimize();
d=f.startContainer,f.startOffset==(d.getChildCount?d.getChildCount():d.getLength())&&!d.isBlockBoundary();
){f.setStartAfter(d)
}d=f.startContainer;
if(d.type!=CKEDITOR.NODE_ELEMENT){return d.getParent()
}if((d=d.getChild(f.startOffset))&&d.type==CKEDITOR.NODE_ELEMENT){for(f=d.getFirst();
f&&f.type==CKEDITOR.NODE_ELEMENT;
){d=f,f=f.getFirst()
}}else{d=f.startContainer
}}d=d.$
}}return e.startElement=d?new CKEDITOR.dom.element(d):null
},getSelectedElement:function(){var e=this._.cache;
if(void 0!==e.selectedElement){return e.selectedElement
}var d=this,f=CKEDITOR.tools.tryThese(function(){return d.getNative().createRange().item(0)
},function(){for(var b=d.getRanges()[0].clone(),k,h,g=2;
g&&!((k=b.getEnclosedNode())&&k.type==CKEDITOR.NODE_ELEMENT&&o[k.getName()]&&(h=k));
g--){b.shrink(CKEDITOR.SHRINK_ELEMENT)
}return h&&h.$
});
return e.selectedElement=f?new CKEDITOR.dom.element(f):null
},getSelectedText:function(){var d=this._.cache;
if(void 0!==d.selectedText){return d.selectedText
}var c=this.getNative(),c=p?"Control"==c.type?"":c.createRange().text:c.toString();
return d.selectedText=c
},lock:function(){this.getRanges();
this.getStartElement();
this.getSelectedElement();
this.getSelectedText();
this._.cache.nativeSel=null;
this.isLocked=1
},unlock:function(f){if(this.isLocked){if(f){var e=this.getSelectedElement(),h=!e&&this.getRanges(),g=this.isFake
}this.isLocked=0;
this.reset();
f&&(f=e||h[0]&&h[0].getCommonAncestor())&&f.getAscendant("body",1)&&(g?this.fake(e):e?this.selectElement(e):this.selectRanges(h))
}},reset:function(){this._.cache={};
this.isFake=0;
var e=this.root.editor;
if(e&&e._.fakeSelection){if(this.rev==e._.fakeSelection.rev){delete e._.fakeSelection;
var d=e._.hiddenSelectionContainer;
if(d){var f=e.checkDirty();
e.fire("lockSnapshot");
d.remove();
e.fire("unlockSnapshot");
!f&&e.resetDirty()
}delete e._.hiddenSelectionContainer
}else{CKEDITOR.warn("selection-fake-reset")
}}this.rev=l++
},selectElement:function(d){var c=new CKEDITOR.dom.range(this.root);
c.setStartBefore(d);
c.setEndAfter(d);
this.selectRanges([c])
},selectRanges:function(O){var C=this.root.editor,C=C&&C._.hiddenSelectionContainer;
this.reset();
if(C){for(var C=this.root,A,z=0;
z<O.length;
++z){A=O[z],A.endContainer.equals(C)&&(A.endOffset=Math.min(A.endOffset,C.getChildCount()))
}}if(O.length){if(this.isLocked){var w=CKEDITOR.document.getActive();
this.unlock();
this.selectRanges(O);
this.lock();
w&&!w.equals(this.root)&&w.focus()
}else{var y;
O:{var u,h;
if(1==O.length&&!(h=O[0]).collapsed&&(y=h.getEnclosedNode())&&y.type==CKEDITOR.NODE_ELEMENT&&(h=h.clone(),h.shrink(CKEDITOR.SHRINK_ELEMENT,!0),(u=h.getEnclosedNode())&&u.type==CKEDITOR.NODE_ELEMENT&&(y=u),"false"==y.getAttribute("contenteditable"))){break O
}y=void 0
}if(y){this.fake(y)
}else{if(p){h=CKEDITOR.dom.walker.whitespaces(!0);
u=/\ufeff|\u00a0/;
C={table:1,tbody:1,tr:1};
1<O.length&&(y=O[O.length-1],O[0].setEnd(y.endContainer,y.endOffset));
y=O[0];
O=y.collapsed;
var R,e,Q;
if((A=y.getEnclosedNode())&&A.type==CKEDITOR.NODE_ELEMENT&&A.getName() in o&&(!A.is("a")||!A.getText())){try{Q=A.$.createControlRange();
Q.addElement(A.$);
Q.select();
return
}catch(F){}}if(y.startContainer.type==CKEDITOR.NODE_ELEMENT&&y.startContainer.getName() in C||y.endContainer.type==CKEDITOR.NODE_ELEMENT&&y.endContainer.getName() in C){y.shrink(CKEDITOR.NODE_ELEMENT,!0),O=y.collapsed
}Q=y.createBookmark();
C=Q.startNode;
O||(w=Q.endNode);
Q=y.document.$.body.createTextRange();
Q.moveToElementText(C.$);
Q.moveStart("character",1);
w?(u=y.document.$.body.createTextRange(),u.moveToElementText(w.$),Q.setEndPoint("EndToEnd",u),Q.moveEnd("character",-1)):(R=C.getNext(h),e=C.hasAscendant("pre"),R=!(R&&R.getText&&R.getText().match(u))&&(e||!C.hasPrevious()||C.getPrevious().is&&C.getPrevious().is("br")),e=y.document.createElement("span"),e.setHtml("\x26#65279;"),e.insertBefore(C),R&&y.document.createText("").insertBefore(C));
y.setStartBefore(C);
C.remove();
O?(R?(Q.moveStart("character",-1),Q.select(),y.document.$.selection.clear()):Q.select(),y.moveToPosition(e,CKEDITOR.POSITION_BEFORE_START),e.remove()):(y.setEndBefore(w),w.remove(),Q.select())
}else{w=this.getNative();
if(!w){return
}this.removeAllRanges();
for(Q=0;
Q<O.length;
Q++){if(Q<O.length-1&&(R=O[Q],e=O[Q+1],u=R.clone(),u.setStart(R.endContainer,R.endOffset),u.setEnd(e.startContainer,e.startOffset),!u.collapsed&&(u.shrink(CKEDITOR.NODE_ELEMENT,!0),y=u.getCommonAncestor(),u=u.getEnclosedNode(),y.isReadOnly()||u&&u.isReadOnly()))){e.setStart(R.startContainer,R.startOffset);
O.splice(Q--,1);
continue
}y=O[Q];
e=this.document.$.createRange();
y.collapsed&&CKEDITOR.env.webkit&&J(y)&&(R=this.root,G(R,!1),u=R.getDocument().createText(""),R.setCustomData("cke-fillingChar",u),y.insertNode(u),(R=u.getNext())&&!u.getPrevious()&&R.type==CKEDITOR.NODE_ELEMENT&&"br"==R.getName()?(G(this.root),y.moveToPosition(R,CKEDITOR.POSITION_BEFORE_START)):y.moveToPosition(u,CKEDITOR.POSITION_AFTER_END));
e.setStart(y.startContainer.$,y.startOffset);
try{e.setEnd(y.endContainer.$,y.endOffset)
}catch(P){if(0<=P.toString().indexOf("NS_ERROR_ILLEGAL_VALUE")){y.collapse(1),e.setEnd(y.endContainer.$,y.endOffset)
}else{throw P
}}w.addRange(e)
}}this.reset();
this.root.fire("selectionchange")
}}}},fake:function(f){var e=this.root.editor;
this.reset();
H(e);
var h=this._.cache,g=new CKEDITOR.dom.range(this.root);
g.setStartBefore(f);
g.setEndAfter(f);
h.ranges=new CKEDITOR.dom.rangeList(g);
h.selectedElement=h.startElement=f;
h.type=CKEDITOR.SELECTION_ELEMENT;
h.selectedText=h.nativeSel=null;
this.isFake=1;
this.rev=l++;
e._.fakeSelection=this;
this.root.fire("selectionchange")
},isHidden:function(){var b=this.getCommonAncestor();
b&&b.type==CKEDITOR.NODE_TEXT&&(b=b.getParent());
return !(!b||!b.data("cke-hidden-sel"))
},createBookmarks:function(b){b=this.getRanges().createBookmarks(b);
this.isFake&&(b.isFake=1);
return b
},createBookmarks2:function(b){b=this.getRanges().createBookmarks2(b);
this.isFake&&(b.isFake=1);
return b
},selectBookmarks:function(g){for(var f=[],n,k=0;
k<g.length;
k++){var h=new CKEDITOR.dom.range(this.root);
h.moveToBookmark(g[k]);
f.push(h)
}g.isFake&&(n=f[0].getEnclosedNode(),n&&n.type==CKEDITOR.NODE_ELEMENT||(CKEDITOR.warn("selection-not-fake"),g.isFake=0));
g.isFake?this.fake(n):this.selectRanges(f);
return this
},getCommonAncestor:function(){var b=this.getRanges();
return b.length?b[0].startContainer.getCommonAncestor(b[b.length-1].endContainer):null
},scrollIntoView:function(){this.type!=CKEDITOR.SELECTION_NONE&&this.getRanges()[0].scrollIntoView()
},removeAllRanges:function(){if(this.getType()!=CKEDITOR.SELECTION_NONE){var d=this.getNative();
try{d&&d[p?"empty":"removeAllRanges"]()
}catch(c){}this.reset()
}}}
})();
"use strict";
CKEDITOR.STYLE_BLOCK=1;
CKEDITOR.STYLE_INLINE=2;
CKEDITOR.STYLE_OBJECT=3;
(function(){function aj(g,f){for(var l,k;
(g=g.getParent())&&!g.equals(f);
){if(g.getAttribute("data-nostyle")){l=g
}else{if(!k){var h=g.getAttribute("contentEditable");
"false"==h?l=g:"true"==h&&(k=1)
}}}return l
}function ag(f,e,h,g){return(f.getPosition(e)|g)==g&&(!h.childRule||h.childRule(f))
}function ai(aA){var ay=aA.document;
if(aA.collapsed){ay=N(this,ay),aA.insertNode(ay),aA.moveToPosition(ay,CKEDITOR.POSITION_BEFORE_END)
}else{var az=this.element,ax=this._.definition,av,aw=ax.ignoreReadonly,at=aw||ax.includeReadonly;
null==at&&(at=aA.root.getCustomData("cke_includeReadonly"));
var au=CKEDITOR.dtd[az];
au||(av=!0,au=CKEDITOR.dtd.span);
aA.enlarge(CKEDITOR.ENLARGE_INLINE,1);
aA.trim();
var ar=aA.createBookmark(),al=ar.startNode,ap=ar.endNode,an=al,S;
if(!aw){var ak=aA.getCommonAncestor(),aw=aj(al,ak),ak=aj(ap,ak);
aw&&(an=aw.getNextSourceNode(!0));
ak&&(ap=ak)
}for(an.getPosition(ap)==CKEDITOR.POSITION_FOLLOWING&&(an=0);
an;
){aw=!1;
if(an.equals(ap)){an=null,aw=!0
}else{var J=an.type==CKEDITOR.NODE_ELEMENT?an.getName():null,ak=J&&"false"==an.getAttribute("contentEditable"),C=J&&an.getAttribute("data-nostyle");
if(J&&an.data("cke-bookmark")){an=an.getNextSourceNode(!0);
continue
}if(ak&&at&&CKEDITOR.dtd.$block[J]){for(var aq=an,F=af(aq),ao=void 0,am=F.length,M=0,aq=am&&new CKEDITOR.dom.range(aq.getDocument());
M<am;
++M){var ao=F[M],d=CKEDITOR.filter.instances[ao.data("cke-filter")];
if(d?d.check(this):1){aq.selectNodeContents(ao),ai.call(this,aq)
}}}F=J?!au[J]||C?0:ak&&!at?0:ag(an,ap,ax,o):1;
if(F){if(ao=an.getParent(),F=ax,am=az,M=av,!ao||!(ao.getDtd()||CKEDITOR.dtd.span)[am]&&!M||F.parentRule&&!F.parentRule(ao)){aw=!0
}else{if(S||J&&CKEDITOR.dtd.$removeEmpty[J]&&(an.getPosition(ap)|o)!=o||(S=aA.clone(),S.setStartBefore(an)),J=an.type,J==CKEDITOR.NODE_TEXT||ak||J==CKEDITOR.NODE_ELEMENT&&!an.getChildCount()){for(var J=an,a;
(aw=!J.getNext(x))&&(a=J.getParent(),au[a.getName()])&&ag(a,al,ax,j);
){J=a
}S.setEndAfter(J)
}}}else{aw=!0
}an=an.getNextSourceNode(C||ak)
}if(aw&&S&&!S.collapsed){for(var aw=N(this,ay),ak=aw.hasAttributes(),C=S.getCommonAncestor(),J={},F={},ao={},am={},b,e,aB;
aw&&C;
){if(C.getName()==az){for(b in ax.attributes){!am[b]&&(aB=C.getAttribute(e))&&(aw.getAttribute(b)==aB?F[b]=1:am[b]=1)
}for(e in ax.styles){!ao[e]&&(aB=C.getStyle(e))&&(aw.getStyle(e)==aB?J[e]=1:ao[e]=1)
}}C=C.getParent()
}for(b in F){aw.removeAttribute(b)
}for(e in J){aw.removeStyle(e)
}ak&&!aw.hasAttributes()&&(aw=null);
aw?(S.extractContents().appendTo(aw),S.insertNode(aw),R.call(this,aw),aw.mergeSiblings(),CKEDITOR.env.ie||aw.$.normalize()):(aw=new CKEDITOR.dom.element("span"),S.extractContents().appendTo(aw),S.insertNode(aw),R.call(this,aw),aw.remove(!0));
S=null
}}aA.moveToBookmark(ar);
aA.shrink(CKEDITOR.SHRINK_TEXT);
aA.shrink(CKEDITOR.NODE_ELEMENT,!0)
}}function ah(A){function z(){for(var b=new CKEDITOR.dom.elementPath(w.getParent()),B=new CKEDITOR.dom.elementPath(n.getParent()),p=null,k=null,m=0;
m<b.elements.length;
m++){var d=b.elements[m];
if(d==b.block||d==b.blockLimit){break
}l.checkElementRemovable(d,!0)&&(p=d)
}for(m=0;
m<B.elements.length;
m++){d=B.elements[m];
if(d==B.block||d==B.blockLimit){break
}l.checkElementRemovable(d,!0)&&(k=d)
}k&&n.breakParent(k);
p&&w.breakParent(p)
}A.enlarge(CKEDITOR.ENLARGE_INLINE,1);
var y=A.createBookmark(),w=y.startNode;
if(A.collapsed){for(var v=new CKEDITOR.dom.elementPath(w.getParent(),A.root),t,u=0,r;
u<v.elements.length&&(r=v.elements[u])&&r!=v.block&&r!=v.blockLimit;
u++){if(this.checkElementRemovable(r)){var q;
A.collapsed&&(A.checkBoundaryOfElement(r,CKEDITOR.END)||(q=A.checkBoundaryOfElement(r,CKEDITOR.START)))?(t=r,t.match=q?"start":"end"):(r.mergeSiblings(),r.is(this.element)?K.call(this,r):Y(r,aa(this)[r.getName()]))
}}if(t){r=w;
for(u=0;
;
u++){q=v.elements[u];
if(q.equals(t)){break
}else{if(q.match){continue
}else{q=q.clone()
}}q.append(r);
r=q
}r["start"==t.match?"insertBefore":"insertAfter"](t)
}}else{var n=y.endNode,l=this;
z();
for(v=w;
!v.equals(n);
){t=v.getNextSourceNode(),v.type==CKEDITOR.NODE_ELEMENT&&this.checkElementRemovable(v)&&(v.getName()==this.element?K.call(this,v):Y(v,aa(this)[v.getName()]),t.type==CKEDITOR.NODE_ELEMENT&&t.contains(w)&&(z(),t=w.getNext())),v=t
}}A.moveToBookmark(y);
A.shrink(CKEDITOR.NODE_ELEMENT,!0)
}function af(d){var c=[];
d.forEach(function(b){if("true"==b.getAttribute("contenteditable")){return c.push(b),!1
}},CKEDITOR.NODE_ELEMENT,!0);
return c
}function ae(d){var c=d.getEnclosedNode()||d.getCommonAncestor(!1,!0);
(d=(new CKEDITOR.dom.elementPath(c,d.root)).contains(this.element,1))&&!d.isReadOnly()&&H(d,this)
}function ac(g){var f=g.getCommonAncestor(!0,!0);
if(g=(new CKEDITOR.dom.elementPath(f,g.root)).contains(this.element,1)){var f=this._.definition,l=f.attributes;
if(l){for(var k in l){g.removeAttribute(k,l[k])
}}if(f.styles){for(var h in f.styles){f.styles.hasOwnProperty(h)&&g.removeStyle(h)
}}}}function ab(h){var f=h.createBookmark(!0),n=h.createIterator();
n.enforceRealBlocks=!0;
this._.enterMode&&(n.enlargeBr=this._.enterMode!=CKEDITOR.ENTER_BR);
for(var m,l=h.document,k;
m=n.getNextParagraph();
){!m.isReadOnly()&&(n.activeFilter?n.activeFilter.check(this):1)&&(k=N(this,l,m),O(m,k))
}h.moveToBookmark(f)
}function V(g){var f=g.createBookmark(1),l=g.createIterator();
l.enforceRealBlocks=!0;
l.enlargeBr=this._.enterMode!=CKEDITOR.ENTER_BR;
for(var k,h;
k=l.getNextParagraph();
){this.checkElementRemovable(k)&&(k.is("pre")?((h=this._.enterMode==CKEDITOR.ENTER_BR?null:g.document.createElement(this._.enterMode==CKEDITOR.ENTER_P?"p":"div"))&&k.copyAttributes(h),O(k,h)):K.call(this,k))
}g.moveToBookmark(f)
}function O(l,g){var t=!g;
t&&(g=l.getDocument().createElement("div"),l.copyAttributes(g));
var r=g&&g.is("pre"),q=l.is("pre"),p=!r&&q;
if(r&&!q){q=g;
(p=l.getBogus())&&p.remove();
p=l.getHtml();
p=s(p,/(?:^[ \t\n\r]+)|(?:[ \t\n\r]+$)/g,"");
p=p.replace(/[ \t\r\n]*(<br[^>]*>)[ \t\r\n]*/gi,"$1");
p=p.replace(/([ \t\n\r]+|&nbsp;)/g," ");
p=p.replace(/<br\b[^>]*>/gi,"\n");
if(CKEDITOR.env.ie){var n=l.getDocument().createElement("div");
n.append(q);
q.$.outerHTML="\x3cpre\x3e"+p+"\x3c/pre\x3e";
q.copyAttributes(n.getFirst());
q=n.getFirst().remove()
}else{q.setHtml(p)
}g=q
}else{p?g=X(t?[l.getHtml()]:ad(l),g):l.moveChildren(g)
}g.replace(l);
if(r){var t=g,m;
(m=t.getPrevious(i))&&m.type==CKEDITOR.NODE_ELEMENT&&m.is("pre")&&(r=s(m.getHtml(),/\n$/,"")+"\n\n"+s(t.getHtml(),/^\n/,""),CKEDITOR.env.ie?t.$.outerHTML="\x3cpre\x3e"+r+"\x3c/pre\x3e":t.setHtml(r),m.remove())
}else{t&&E(g)
}}function ad(d){var c=[];
s(d.getOuterHtml(),/(\S\s*)\n(?:\s|(<span[^>]+data-cke-bookmark.*?\/span>))*\n(?!$)/gi,function(f,e,g){return e+"\x3c/pre\x3e"+g+"\x3cpre\x3e"
}).replace(/<pre\b.*?>([\s\S]*?)<\/pre>/gi,function(b,e){c.push(e)
});
return c
}function s(g,f,l){var k="",h="";
g=g.replace(/(^<span[^>]+data-cke-bookmark.*?\/span>)|(<span[^>]+data-cke-bookmark.*?\/span>$)/gi,function(e,d,m){d&&(k=d);
m&&(h=m);
return""
});
return k+g.replace(f,l)+h
}function X(h,f){var n;
1<h.length&&(n=new CKEDITOR.dom.documentFragment(f.getDocument()));
for(var m=0;
m<h.length;
m++){var l=h[m],l=l.replace(/(\r\n|\r)/g,"\n"),l=s(l,/^[ \t]*\n/,""),l=s(l,/\n$/,""),l=s(l,/^[ \t]+|[ \t]+$/g,function(d,c){return 1==d.length?"\x26nbsp;":c?" "+CKEDITOR.tools.repeat("\x26nbsp;",d.length-1):CKEDITOR.tools.repeat("\x26nbsp;",d.length-1)+" "
}),l=l.replace(/\n/g,"\x3cbr\x3e"),l=l.replace(/[ \t]{2,}/g,function(b){return CKEDITOR.tools.repeat("\x26nbsp;",b.length-1)+" "
});
if(n){var k=f.clone();
k.setHtml(l);
n.append(k)
}else{f.setHtml(l)
}}return n||f
}function K(l,k){var t=this._.definition,r=t.attributes,t=t.styles,q=aa(this)[l.getName()],n=CKEDITOR.tools.isEmpty(r)&&CKEDITOR.tools.isEmpty(t),p;
for(p in r){if("class"!=p&&!this._.definition.fullMatch||l.getAttribute(p)==W(p,r[p])){k&&"data-"==p.slice(0,5)||(n=l.hasAttribute(p),l.removeAttribute(p))
}}for(var m in t){this._.definition.fullMatch&&l.getStyle(m)!=W(m,t[m],!0)||(n=n||!!l.getStyle(m),l.removeStyle(m))
}Y(l,q,P[l.getName()]);
n&&(this._.definition.alwaysRemoveElement?E(l,1):!CKEDITOR.dtd.$block[l.getName()]||this._.enterMode==CKEDITOR.ENTER_BR&&!l.hasAttributes()?E(l):l.renameNode(this._.enterMode==CKEDITOR.ENTER_P?"p":"div"))
}function R(h){for(var f=aa(this),n=h.getElementsByTag(this.element),m,l=n.count();
0<=--l;
){m=n.getItem(l),m.isReadOnly()||K.call(this,m,!0)
}for(var k in f){if(k!=this.element){for(n=h.getElementsByTag(k),l=n.count()-1;
0<=l;
l--){m=n.getItem(l),m.isReadOnly()||Y(m,f[k])
}}}}function Y(k,h,q){if(h=h&&h.attributes){for(var p=0;
p<h.length;
p++){var n=h[p][0],l;
if(l=k.getAttribute(n)){var m=h[p][1];
(null===m||m.test&&m.test(l)||"string"==typeof m&&l==m)&&k.removeAttribute(n)
}}}q||E(k)
}function E(f,e){if(!f.hasAttributes()||e){if(CKEDITOR.dtd.$block[f.getName()]){var h=f.getPrevious(i),g=f.getNext(i);
!h||h.type!=CKEDITOR.NODE_TEXT&&h.isBlockBoundary({br:1})||f.append("br",1);
!g||g.type!=CKEDITOR.NODE_TEXT&&g.isBlockBoundary({br:1})||f.append("br");
f.remove(!0)
}else{h=f.getFirst(),g=f.getLast(),f.remove(!0),h&&(h.type==CKEDITOR.NODE_ELEMENT&&h.mergeSiblings(),g&&!h.equals(g)&&g.type==CKEDITOR.NODE_ELEMENT&&g.mergeSiblings())
}}}function N(f,e,h){var g;
g=f.element;
"*"==g&&(g="span");
g=new CKEDITOR.dom.element(g,e);
h&&h.copyAttributes(g);
g=H(g,f);
e.getCustomData("doc_processing_style")&&g.hasAttribute("id")?g.removeAttribute("id"):e.setCustomData("doc_processing_style",1);
return g
}function H(g,f){var l=f._.definition,k=l.attributes,l=CKEDITOR.style.getStyleText(l);
if(k){for(var h in k){g.setAttribute(h,k[h])
}}l&&g.setAttribute("style",l);
return g
}function L(e,d){for(var f in e){e[f]=e[f].replace(U,function(b,g){return d[g]
})
}}function aa(l){if(l._.overrides){return l._.overrides
}var k=l._.overrides={},t=l._.definition.overrides;
if(t){CKEDITOR.tools.isArray(t)||(t=[t]);
for(var r=0;
r<t.length;
r++){var q=t[r],n,p;
"string"==typeof q?n=q.toLowerCase():(n=q.element?q.element.toLowerCase():l.element,p=q.attributes);
q=k[n]||(k[n]={});
if(p){var q=q.attributes=q.attributes||[],m;
for(m in p){q.push([m.toLowerCase(),p[m]])
}}}}return k
}function W(f,e,h){var g=new CKEDITOR.dom.element("span");
g[h?"setStyle":"setAttribute"](f,e);
return g[h?"getStyle":"getAttribute"](f)
}function Z(k,h,q){var p=k.document,n=k.getRanges();
h=h?this.removeFromRange:this.applyToRange;
for(var l,m=n.createIterator();
l=m.getNextRange();
){h.call(this,l,q)
}k.selectRanges(n);
p.removeCustomData("doc_processing_style")
}var P={address:1,div:1,h1:1,h2:1,h3:1,h4:1,h5:1,h6:1,p:1,pre:1,section:1,header:1,footer:1,nav:1,article:1,aside:1,figure:1,dialog:1,hgroup:1,time:1,meter:1,menu:1,command:1,keygen:1,output:1,progress:1,details:1,datagrid:1,datalist:1},Q={a:1,blockquote:1,embed:1,hr:1,img:1,li:1,object:1,ol:1,table:1,td:1,tr:1,th:1,ul:1,dl:1,dt:1,dd:1,form:1,audio:1,video:1},I=/\s*(?:;\s*|$)/,U=/#\((.+?)\)/g,x=CKEDITOR.dom.walker.bookmark(0,1),i=CKEDITOR.dom.walker.whitespaces(1);
CKEDITOR.style=function(e,d){if("string"==typeof e.type){return new CKEDITOR.style.customHandlers[e.type](e)
}var f=e.attributes;
f&&f.style&&(e.styles=CKEDITOR.tools.extend({},e.styles,CKEDITOR.tools.parseCssText(f.style)),delete f.style);
d&&(e=CKEDITOR.tools.clone(e),L(e.attributes,d),L(e.styles,d));
f=this.element=e.element?"string"==typeof e.element?e.element.toLowerCase():e.element:"*";
this.type=e.type||(P[f]?CKEDITOR.STYLE_BLOCK:Q[f]?CKEDITOR.STYLE_OBJECT:CKEDITOR.STYLE_INLINE);
"object"==typeof this.element&&(this.type=CKEDITOR.STYLE_OBJECT);
this._={definition:e}
};
CKEDITOR.style.prototype={apply:function(d){if(d instanceof CKEDITOR.dom.document){return Z.call(this,d.getSelection())
}if(this.checkApplicable(d.elementPath(),d)){var c=this._.enterMode;
c||(this._.enterMode=d.activeEnterMode);
Z.call(this,d.getSelection(),0,d);
this._.enterMode=c
}},remove:function(d){if(d instanceof CKEDITOR.dom.document){return Z.call(this,d.getSelection(),1)
}if(this.checkApplicable(d.elementPath(),d)){var c=this._.enterMode;
c||(this._.enterMode=d.activeEnterMode);
Z.call(this,d.getSelection(),1,d);
this._.enterMode=c
}},applyToRange:function(b){this.applyToRange=this.type==CKEDITOR.STYLE_INLINE?ai:this.type==CKEDITOR.STYLE_BLOCK?ab:this.type==CKEDITOR.STYLE_OBJECT?ae:null;
return this.applyToRange(b)
},removeFromRange:function(b){this.removeFromRange=this.type==CKEDITOR.STYLE_INLINE?ah:this.type==CKEDITOR.STYLE_BLOCK?V:this.type==CKEDITOR.STYLE_OBJECT?ac:null;
return this.removeFromRange(b)
},applyToObject:function(b){H(b,this)
},checkActive:function(h,f){switch(this.type){case CKEDITOR.STYLE_BLOCK:return this.checkElementRemovable(h.block||h.blockLimit,!0,f);
case CKEDITOR.STYLE_OBJECT:case CKEDITOR.STYLE_INLINE:for(var n=h.elements,m=0,l;
m<n.length;
m++){if(l=n[m],this.type!=CKEDITOR.STYLE_INLINE||l!=h.block&&l!=h.blockLimit){if(this.type==CKEDITOR.STYLE_OBJECT){var k=l.getName();
if(!("string"==typeof this.element?k==this.element:k in this.element)){continue
}}if(this.checkElementRemovable(l,!0,f)){return !0
}}}}return !1
},checkApplicable:function(e,d,f){d&&d instanceof CKEDITOR.filter&&(f=d);
if(f&&!f.check(this)){return !1
}switch(this.type){case CKEDITOR.STYLE_OBJECT:return !!e.contains(this.element);
case CKEDITOR.STYLE_BLOCK:return !!e.blockLimit.getDtd()[this.element]
}return !0
},checkElementMatch:function(l,k){var t=this._.definition;
if(!l||!t.ignoreReadonly&&l.isReadOnly()){return !1
}var r=l.getName();
if("string"==typeof this.element?r==this.element:r in this.element){if(!k&&!l.hasAttributes()){return !0
}if(r=t._AC){t=r
}else{var r={},q=0,n=t.attributes;
if(n){for(var p in n){q++,r[p]=n[p]
}}if(p=CKEDITOR.style.getStyleText(t)){r.style||q++,r.style=p
}r._length=q;
t=t._AC=r
}if(t._length){for(var m in t){if("_length"!=m){q=l.getAttribute(m)||"";
if("style"==m){l:{r=t[m];
"string"==typeof r&&(r=CKEDITOR.tools.parseCssText(r));
"string"==typeof q&&(q=CKEDITOR.tools.parseCssText(q,!0));
p=void 0;
for(p in r){if(!(p in q)||q[p]!=r[p]&&"inherit"!=r[p]&&"inherit"!=q[p]){r=!1;
break l
}}r=!0
}}else{r=t[m]==q
}if(r){if(!k){return !0
}}else{if(k){return !1
}}}}if(k){return !0
}}else{return !0
}}return !1
},checkElementRemovable:function(g,f,l){if(this.checkElementMatch(g,f,l)){return !0
}if(f=aa(this)[g.getName()]){var k;
if(!(f=f.attributes)){return !0
}for(l=0;
l<f.length;
l++){if(k=f[l][0],k=g.getAttribute(k)){var h=f[l][1];
if(null===h){return !0
}if("string"==typeof h){if(k==h){return !0
}}else{if(h.test(k)){return !0
}}}}}return !1
},buildPreview:function(h){var f=this._.definition,n=[],m=f.element;
"bdo"==m&&(m="span");
var n=["\x3c",m],l=f.attributes;
if(l){for(var k in l){n.push(" ",k,'\x3d"',l[k],'"')
}}(l=CKEDITOR.style.getStyleText(f))&&n.push(' style\x3d"',l,'"');
n.push("\x3e",h||f.name,"\x3c/",m,"\x3e");
return n.join("")
},getDefinition:function(){return this._.definition
}};
CKEDITOR.style.getStyleText=function(k){var h=k._ST;
if(h){return h
}var h=k.styles,q=k.attributes&&k.attributes.style||"",p="";
q.length&&(q=q.replace(I,";"));
for(var n in h){var l=h[n],m=(n+":"+l).replace(I,";");
"inherit"==l?p+=m:q+=m
}q.length&&(q=CKEDITOR.tools.normalizeCssText(q,!0));
return k._ST=q+p
};
CKEDITOR.style.customHandlers={};
CKEDITOR.style.addCustomHandler=function(d){var c=function(b){this._={definition:b};
this.setup&&this.setup(b)
};
c.prototype=CKEDITOR.tools.extend(CKEDITOR.tools.prototypedCopy(CKEDITOR.style.prototype),{assignedTo:CKEDITOR.STYLE_OBJECT},d,!0);
return this.customHandlers[d.type]=c
};
var o=CKEDITOR.POSITION_PRECEDING|CKEDITOR.POSITION_IDENTICAL|CKEDITOR.POSITION_IS_CONTAINED,j=CKEDITOR.POSITION_FOLLOWING|CKEDITOR.POSITION_IDENTICAL|CKEDITOR.POSITION_IS_CONTAINED
})();
CKEDITOR.styleCommand=function(b,c){this.requiredContent=this.allowedContent=this.style=b;
CKEDITOR.tools.extend(this,c,!0)
};
CKEDITOR.styleCommand.prototype.exec=function(b){b.focus();
this.state==CKEDITOR.TRISTATE_OFF?b.applyStyle(this.style):this.state==CKEDITOR.TRISTATE_ON&&b.removeStyle(this.style)
};
CKEDITOR.stylesSet=new CKEDITOR.resourceManager("","stylesSet");
CKEDITOR.addStylesSet=CKEDITOR.tools.bind(CKEDITOR.stylesSet.add,CKEDITOR.stylesSet);
CKEDITOR.loadStylesSet=function(e,f,c){CKEDITOR.stylesSet.addExternal(e,f,"");
CKEDITOR.stylesSet.load(e,c)
};
CKEDITOR.tools.extend(CKEDITOR.editor.prototype,{attachStyleStateChange:function(e,f){var c=this._.styleStateChangeCallbacks;
c||(c=this._.styleStateChangeCallbacks=[],this.on("selectionChange",function(b){for(var j=0;
j<c.length;
j++){var i=c[j],g=i.style.checkActive(b.data.path,this)?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF;
i.fn.call(this,g)
}}));
c.push({style:e,fn:f})
},applyStyle:function(b){b.apply(this)
},removeStyle:function(b){b.remove(this)
},getStylesSet:function(f){if(this._.stylesDefinitions){f(this._.stylesDefinitions)
}else{var g=this,e=g.config.stylesCombo_stylesSet||g.config.stylesSet;
if(!1===e){f(null)
}else{if(e instanceof Array){g._.stylesDefinitions=e,f(e)
}else{e||(e="default");
var e=e.split(":"),h=e[0];
CKEDITOR.stylesSet.addExternal(h,e[1]?e.slice(1).join(":"):CKEDITOR.getUrl("styles.js"),"");
CKEDITOR.stylesSet.load(h,function(a){g._.stylesDefinitions=a[h];
f(g._.stylesDefinitions)
})
}}}}});
CKEDITOR.dom.comment=function(b,c){"string"==typeof b&&(b=(c?c.$:document).createComment(b));
CKEDITOR.dom.domObject.call(this,b)
};
CKEDITOR.dom.comment.prototype=new CKEDITOR.dom.node;
CKEDITOR.tools.extend(CKEDITOR.dom.comment.prototype,{type:CKEDITOR.NODE_COMMENT,getOuterHtml:function(){return"\x3c!--"+this.$.nodeValue+"--\x3e"
}});
"use strict";
(function(){var e={},f={},c;
for(c in CKEDITOR.dtd.$blockLimit){c in CKEDITOR.dtd.$list||(e[c]=1)
}for(c in CKEDITOR.dtd.$block){c in CKEDITOR.dtd.$blockLimit||c in CKEDITOR.dtd.$empty||(f[c]=1)
}CKEDITOR.dom.elementPath=function(p,n){var m=null,j=null,i=[],a=p,r;
n=n||p.getDocument().getBody();
do{if(a.type==CKEDITOR.NODE_ELEMENT){i.push(a);
if(!this.lastElement&&(this.lastElement=a,a.is(CKEDITOR.dtd.$object)||"false"==a.getAttribute("contenteditable"))){continue
}if(a.equals(n)){break
}if(!j&&(r=a.getName(),"true"==a.getAttribute("contenteditable")?j=a:!m&&f[r]&&(m=a),e[r])){if(r=!m&&"div"==r){e:{r=a.getChildren();
for(var l=0,o=r.count();
l<o;
l++){var d=r.getItem(l);
if(d.type==CKEDITOR.NODE_ELEMENT&&CKEDITOR.dtd.$block[d.getName()]){r=!0;
break e
}}r=!1
}r=!r
}r?m=a:j=a
}}}while(a=a.getParent());
j||(j=n);
this.block=m;
this.blockLimit=j;
this.root=n;
this.elements=i
}
})();
CKEDITOR.dom.elementPath.prototype={compare:function(e){var f=this.elements;
e=e&&e.elements;
if(!e||f.length!=e.length){return !1
}for(var c=0;
c<f.length;
c++){if(!f[c].equals(e[c])){return !1
}}return !0
},contains:function(h,k,g){var l;
"string"==typeof h&&(l=function(a){return a.getName()==h
});
h instanceof CKEDITOR.dom.element?l=function(a){return a.equals(h)
}:CKEDITOR.tools.isArray(h)?l=function(a){return -1<CKEDITOR.tools.indexOf(h,a.getName())
}:"function"==typeof h?l=h:"object"==typeof h&&(l=function(a){return a.getName() in h
});
var j=this.elements,i=j.length;
k&&i--;
g&&(j=Array.prototype.slice.call(j,0),j.reverse());
for(k=0;
k<i;
k++){if(l(j[k])){return j[k]
}}return null
},isContextFor:function(b){var c;
return b in CKEDITOR.dtd.$block?(c=this.contains(CKEDITOR.dtd.$intermediate)||this.root.equals(this.block)&&this.block||this.blockLimit,!!c.getDtd()[b]):!0
},direction:function(){return(this.block||this.blockLimit||this.root).getDirection(1)
}};
CKEDITOR.dom.text=function(b,c){"string"==typeof b&&(b=(c?c.$:document).createTextNode(b));
this.$=b
};
CKEDITOR.dom.text.prototype=new CKEDITOR.dom.node;
CKEDITOR.tools.extend(CKEDITOR.dom.text.prototype,{type:CKEDITOR.NODE_TEXT,getLength:function(){return this.$.nodeValue.length
},getText:function(){return this.$.nodeValue
},setText:function(b){this.$.nodeValue=b
},split:function(h){var k=this.$.parentNode,g=k.childNodes.length,l=this.getLength(),j=this.getDocument(),i=new CKEDITOR.dom.text(this.$.splitText(h),j);
k.childNodes.length==g&&(h>=l?(i=j.createText(""),i.insertAfter(this)):(h=j.createText(""),h.insertAfter(i),h.remove()));
return i
},substring:function(b,c){return"number"!=typeof c?this.$.nodeValue.substr(b):this.$.nodeValue.substring(b,c)
}});
(function(){function b(e,n,m){var l=e.serializable,i=n[m?"endContainer":"startContainer"],g=m?"endOffset":"startOffset",j=l?n.document.getById(e.startNode):e.startNode;
e=l?n.document.getById(e.endNode):e.endNode;
i.equals(j.getPrevious())?(n.startOffset=n.startOffset-i.getLength()-e.getPrevious().getLength(),i=e.getNext()):i.equals(e.getPrevious())&&(n.startOffset-=i.getLength(),i=e.getNext());
i.equals(j.getParent())&&n[g]++;
i.equals(e.getParent())&&n[g]++;
n[m?"endContainer":"startContainer"]=i;
return n
}CKEDITOR.dom.rangeList=function(d){if(d instanceof CKEDITOR.dom.rangeList){return d
}d?d instanceof CKEDITOR.dom.range&&(d=[d]):d=[];
return CKEDITOR.tools.extend(d,c)
};
var c={createIterator:function(){var e=this,i=CKEDITOR.dom.walker.bookmark(),h=[],g;
return{getNextRange:function(f){g=void 0===g?0:g+1;
var a=e[g];
if(a&&1<e.length){if(!g){for(var l=e.length-1;
0<=l;
l--){h.unshift(e[l].createBookmark(!0))
}}if(f){for(var d=0;
e[g+d+1];
){var j=a.document;
f=0;
l=j.getById(h[d].endNode);
for(j=j.getById(h[d+1].startNode);
;
){l=l.getNextSourceNode(!1);
if(j.equals(l)){f=1
}else{if(i(l)||l.type==CKEDITOR.NODE_ELEMENT&&l.isBlockBoundary()){continue
}}break
}if(!f){break
}d++
}}for(a.moveToBookmark(h.shift());
d--;
){l=e[++g],l.moveToBookmark(h.shift()),a.setEnd(l.endContainer,l.endOffset)
}}return a
}}
},createBookmarks:function(a){for(var j=[],i,g=0;
g<this.length;
g++){j.push(i=this[g].createBookmark(a,!0));
for(var e=g+1;
e<this.length;
e++){this[e]=b(i,this[e]),this[e]=b(i,this[e],!0)
}}return j
},createBookmarks2:function(e){for(var g=[],f=0;
f<this.length;
f++){g.push(this[f].createBookmark2(e))
}return g
},moveToBookmarks:function(d){for(var e=0;
e<this.length;
e++){this[e].moveToBookmark(d[e])
}}}
})();
(function(){function r(){return CKEDITOR.getUrl(CKEDITOR.skinName.split(",")[1]||"skins/"+CKEDITOR.skinName.split(",")[0]+"/")
}function n(a){var s=CKEDITOR.skin["ua_"+a],q=CKEDITOR.env;
if(s){for(var s=s.split(",").sort(function(d,c){return d>c?-1:1
}),k=0,h;
k<s.length;
k++){if(h=s[k],q.ie&&(h.replace(/^ie/,"")==q.version||q.quirks&&"iequirks"==h)&&(h="ie"),q[h]){a+="_"+s[k];
break
}}}return CKEDITOR.getUrl(r()+a+".css")
}function p(d,c){l[d]||(CKEDITOR.document.appendStyleSheet(n(d)),l[d]=1);
c&&c()
}function o(d){var c=d.getById(j);
c||(c=d.getHead().append("style"),c.setAttribute("id",j),c.setAttribute("type","text/css"));
return c
}function m(q,k,w){var v,u,t;
if(CKEDITOR.env.webkit){for(k=k.split("}").slice(0,-1),u=0;
u<k.length;
u++){k[u]=k[u].split("{")
}}for(var s=0;
s<q.length;
s++){if(CKEDITOR.env.webkit){for(u=0;
u<k.length;
u++){t=k[u][1];
for(v=0;
v<w.length;
v++){t=t.replace(w[v][0],w[v][1])
}q[s].$.sheet.addRule(k[u][0],t)
}}else{t=k;
for(v=0;
v<w.length;
v++){t=t.replace(w[v][0],w[v][1])
}CKEDITOR.env.ie&&11>CKEDITOR.env.version?q[s].$.styleSheet.cssText+=t:q[s].$.innerHTML+=t
}}}var l={};
CKEDITOR.skin={path:r,loadPart:function(b,a){CKEDITOR.skin.name!=CKEDITOR.skinName.split(",")[0]?CKEDITOR.scriptLoader.load(CKEDITOR.getUrl(r()+"skin.js"),function(){p(b,a)
}):p(b,a)
},getPath:function(b){return CKEDITOR.getUrl(n(b))
},icons:{},addIcon:function(f,e,k,h){f=f.toLowerCase();
this.icons[f]||(this.icons[f]={path:e,offset:k||0,bgsize:h||"16px"})
},getIconStyle:function(k,h,u,t,s){var q;
k&&(k=k.toLowerCase(),h&&(q=this.icons[k+"-rtl"]),q||(q=this.icons[k]));
k=u||q&&q.path||"";
t=t||q&&q.offset;
s=s||q&&q.bgsize||"16px";
return k&&"background-image:url("+CKEDITOR.getUrl(k)+");background-position:0 "+t+"px;background-size:"+s+";"
}};
CKEDITOR.tools.extend(CKEDITOR.editor.prototype,{getUiColor:function(){return this.uiColor
},setUiColor:function(d){var c=o(CKEDITOR.document);
return(this.setUiColor=function(b){this.uiColor=b;
var k=CKEDITOR.skin.chameleon,h="",e="";
"function"==typeof k&&(h=k(this,"editor"),e=k(this,"panel"));
b=[[g,b]];
m([c],h,b);
m(i,e,b)
}).call(this,d)
}});
var j="cke_ui_color",i=[],g=/\$color/g;
CKEDITOR.on("instanceLoaded",function(d){if(!CKEDITOR.env.ie||!CKEDITOR.env.quirks){var c=d.editor;
d=function(b){b=(b.data[0]||b.data).element.getElementsByTag("iframe").getItem(0).getFrameDocument();
if(!b.getById("cke_ui_color")){b=o(b);
i.push(b);
var e=c.getUiColor();
e&&m([b],CKEDITOR.skin.chameleon(c,"panel"),[[g,e]])
}};
c.on("panelShow",d);
c.on("menuShow",d);
c.config.uiColor&&c.setUiColor(c.config.uiColor)
}})
})();
(function(){if(CKEDITOR.env.webkit){CKEDITOR.env.hc=!1
}else{var f=CKEDITOR.dom.element.createFromHtml('\x3cdiv style\x3d"width:0;height:0;position:absolute;left:-10000px;border:1px solid;border-color:red blue"\x3e\x3c/div\x3e',CKEDITOR.document);
f.appendTo(CKEDITOR.document.getHead());
try{var g=f.getComputedStyle("border-top-color"),e=f.getComputedStyle("border-right-color");
CKEDITOR.env.hc=!(!g||g!=e)
}catch(h){CKEDITOR.env.hc=!1
}f.remove()
}CKEDITOR.env.hc&&(CKEDITOR.env.cssClass+=" cke_hc");
CKEDITOR.document.appendStyleText(".cke{visibility:hidden;}");
CKEDITOR.status="loaded";
CKEDITOR.fireOnce("loaded");
if(f=CKEDITOR._.pending){for(delete CKEDITOR._.pending,g=0;
g<f.length;
g++){CKEDITOR.editor.prototype.constructor.apply(f[g][0],f[g][1]),CKEDITOR.add(f[g][0])
}}})();
CKEDITOR.skin.name="moono";
CKEDITOR.skin.ua_editor="ie,iequirks,ie7,ie8,gecko";
CKEDITOR.skin.ua_dialog="ie,iequirks,ie7,ie8";
CKEDITOR.skin.chameleon=function(){var a=function(){return function(g,j){for(var h=g.match(/[^#]./g),l=0;
3>l;
l++){var i=l,k;
k=parseInt(h[l],16);
k=("0"+(0>j?0|k*(1+j):0|k+(255-k)*j).toString(16)).slice(-2);
h[i]=k
}return"#"+h.join("")
}
}(),e=function(){var c=new CKEDITOR.template("background:#{to};background-image:linear-gradient(to bottom,{from},{to});filter:progid:DXImageTransform.Microsoft.gradient(gradientType\x3d0,startColorstr\x3d'{from}',endColorstr\x3d'{to}');");
return function(f,b){return c.output({from:f,to:b})
}
}(),d={editor:new CKEDITOR.template("{id}.cke_chrome [border-color:{defaultBorder};] {id} .cke_top [ {defaultGradient}border-bottom-color:{defaultBorder};] {id} .cke_bottom [{defaultGradient}border-top-color:{defaultBorder};] {id} .cke_resizer [border-right-color:{ckeResizer}] {id} .cke_dialog_title [{defaultGradient}border-bottom-color:{defaultBorder};] {id} .cke_dialog_footer [{defaultGradient}outline-color:{defaultBorder};border-top-color:{defaultBorder};] {id} .cke_dialog_tab [{lightGradient}border-color:{defaultBorder};] {id} .cke_dialog_tab:hover [{mediumGradient}] {id} .cke_dialog_contents [border-top-color:{defaultBorder};] {id} .cke_dialog_tab_selected, {id} .cke_dialog_tab_selected:hover [background:{dialogTabSelected};border-bottom-color:{dialogTabSelectedBorder};] {id} .cke_dialog_body [background:{dialogBody};border-color:{defaultBorder};] {id} .cke_toolgroup [{lightGradient}border-color:{defaultBorder};] {id} a.cke_button_off:hover, {id} a.cke_button_off:focus, {id} a.cke_button_off:active [{mediumGradient}] {id} .cke_button_on [{ckeButtonOn}] {id} .cke_toolbar_separator [background-color: {ckeToolbarSeparator};] {id} .cke_combo_button [border-color:{defaultBorder};{lightGradient}] {id} a.cke_combo_button:hover, {id} a.cke_combo_button:focus, {id} .cke_combo_on a.cke_combo_button [border-color:{defaultBorder};{mediumGradient}] {id} .cke_path_item [color:{elementsPathColor};] {id} a.cke_path_item:hover, {id} a.cke_path_item:focus, {id} a.cke_path_item:active [background-color:{elementsPathBg};] {id}.cke_panel [border-color:{defaultBorder};] "),panel:new CKEDITOR.template(".cke_panel_grouptitle [{lightGradient}border-color:{defaultBorder};] .cke_menubutton_icon [background-color:{menubuttonIcon};] .cke_menubutton:hover .cke_menubutton_icon, .cke_menubutton:focus .cke_menubutton_icon, .cke_menubutton:active .cke_menubutton_icon [background-color:{menubuttonIconHover};] .cke_menuseparator [background-color:{menubuttonIcon};] a:hover.cke_colorbox, a:focus.cke_colorbox, a:active.cke_colorbox [border-color:{defaultBorder};] a:hover.cke_colorauto, a:hover.cke_colormore, a:focus.cke_colorauto, a:focus.cke_colormore, a:active.cke_colorauto, a:active.cke_colormore [background-color:{ckeColorauto};border-color:{defaultBorder};] ")};
return function(c,f){var b=c.uiColor,b={id:"."+c.id,defaultBorder:a(b,-0.1),defaultGradient:e(a(b,0.9),b),lightGradient:e(a(b,1),a(b,0.7)),mediumGradient:e(a(b,0.8),a(b,0.5)),ckeButtonOn:e(a(b,0.6),a(b,0.7)),ckeResizer:a(b,-0.4),ckeToolbarSeparator:a(b,0.5),ckeColorauto:a(b,0.8),dialogBody:a(b,0.7),dialogTabSelected:e("#FFFFFF","#FFFFFF"),dialogTabSelectedBorder:"#FFF",elementsPathColor:a(b,-0.6),elementsPathBg:b,menubuttonIcon:a(b,0.5),menubuttonIconHover:a(b,0.3)};
return d[f].output(b).replace(/\[/g,"{").replace(/\]/g,"}")
}
}();
CKEDITOR.plugins.add("dialogui",{onLoad:function(){var e=function(h){this._||(this._={});
this._["default"]=this._.initValue=h["default"]||"";
this._.required=h.required||!1;
for(var j=[this._],k=1;
k<arguments.length;
k++){j.push(arguments[k])
}j.push(!0);
CKEDITOR.tools.extend.apply(CKEDITOR.tools,j);
return this._
},c={build:function(h,j,k){return new CKEDITOR.ui.dialog.textInput(h,j,k)
}},i={build:function(h,j,k){return new CKEDITOR.ui.dialog[j.type](h,j,k)
}},g={isChanged:function(){return this.getValue()!=this.getInitValue()
},reset:function(h){this.setValue(this.getInitValue(),h)
},setInitValue:function(){this._.initValue=this.getValue()
},resetInitValue:function(){this._.initValue=this._["default"]
},getInitValue:function(){return this._.initValue
}},f=CKEDITOR.tools.extend({},CKEDITOR.ui.dialog.uiElement.prototype.eventProcessors,{onChange:function(h,j){this._.domOnChangeRegistered||(h.on("load",function(){this.getInputElement().on("change",function(){h.parts.dialog.isVisible()&&this.fire("change",{value:this.getValue()})
},this)
},this),this._.domOnChangeRegistered=!0);
this.on("change",j)
}},!0),a=/^on([A-Z]\w+)/,d=function(h){for(var j in h){(a.test(j)||"title"==j||"type"==j)&&delete h[j]
}return h
},b=function(h){h=h.data.getKeystroke();
h==CKEDITOR.SHIFT+CKEDITOR.ALT+36?this.setDirectionMarker("ltr"):h==CKEDITOR.SHIFT+CKEDITOR.ALT+35&&this.setDirectionMarker("rtl")
};
CKEDITOR.tools.extend(CKEDITOR.ui.dialog,{labeledElement:function(h,j,m,k){if(!(4>arguments.length)){var n=e.call(this,j);
n.labelId=CKEDITOR.tools.getNextId()+"_label";
this._.children=[];
var l={role:j.role||"presentation"};
j.includeLabel&&(l["aria-labelledby"]=n.labelId);
CKEDITOR.ui.dialog.uiElement.call(this,h,j,m,"div",null,l,function(){var p=[],o=j.required?" cke_required":"";
"horizontal"!=j.labelLayout?p.push('\x3clabel class\x3d"cke_dialog_ui_labeled_label'+o+'" ',' id\x3d"'+n.labelId+'"',n.inputId?' for\x3d"'+n.inputId+'"':"",(j.labelStyle?' style\x3d"'+j.labelStyle+'"':"")+"\x3e",j.label,"\x3c/label\x3e",'\x3cdiv class\x3d"cke_dialog_ui_labeled_content"',j.controlStyle?' style\x3d"'+j.controlStyle+'"':"",' role\x3d"presentation"\x3e',k.call(this,h,j),"\x3c/div\x3e"):(o={type:"hbox",widths:j.widths,padding:0,children:[{type:"html",html:'\x3clabel class\x3d"cke_dialog_ui_labeled_label'+o+'" id\x3d"'+n.labelId+'" for\x3d"'+n.inputId+'"'+(j.labelStyle?' style\x3d"'+j.labelStyle+'"':"")+"\x3e"+CKEDITOR.tools.htmlEncode(j.label)+"\x3c/label\x3e"},{type:"html",html:'\x3cspan class\x3d"cke_dialog_ui_labeled_content"'+(j.controlStyle?' style\x3d"'+j.controlStyle+'"':"")+"\x3e"+k.call(this,h,j)+"\x3c/span\x3e"}]},CKEDITOR.dialog._.uiElementBuilders.hbox.build(h,o,p));
return p.join("")
})
}},textInput:function(j,k,o){if(!(3>arguments.length)){e.call(this,k);
var l=this._.inputId=CKEDITOR.tools.getNextId()+"_textInput",p={"class":"cke_dialog_ui_input_"+k.type,id:l,type:k.type};
k.validate&&(this.validate=k.validate);
k.maxLength&&(p.maxlength=k.maxLength);
k.size&&(p.size=k.size);
k.inputStyle&&(p.style=k.inputStyle);
var n=this,h=!1;
j.on("load",function(){n.getInputElement().on("keydown",function(m){13==m.data.getKeystroke()&&(h=!0)
});
n.getInputElement().on("keyup",function(m){13==m.data.getKeystroke()&&h&&(j.getButton("ok")&&setTimeout(function(){j.getButton("ok").click()
},0),h=!1);
n.bidi&&b.call(n,m)
},null,null,1000)
});
CKEDITOR.ui.dialog.labeledElement.call(this,j,k,o,function(){var m=['\x3cdiv class\x3d"cke_dialog_ui_input_',k.type,'" role\x3d"presentation"'];
k.width&&m.push('style\x3d"width:'+k.width+'" ');
m.push("\x3e\x3cinput ");
p["aria-labelledby"]=this._.labelId;
this._.required&&(p["aria-required"]=this._.required);
for(var q in p){m.push(q+'\x3d"'+p[q]+'" ')
}m.push(" /\x3e\x3c/div\x3e");
return m.join("")
})
}},textarea:function(h,j,m){if(!(3>arguments.length)){e.call(this,j);
var k=this,n=this._.inputId=CKEDITOR.tools.getNextId()+"_textarea",l={};
j.validate&&(this.validate=j.validate);
l.rows=j.rows||5;
l.cols=j.cols||20;
l["class"]="cke_dialog_ui_input_textarea "+(j["class"]||"");
"undefined"!=typeof j.inputStyle&&(l.style=j.inputStyle);
j.dir&&(l.dir=j.dir);
if(k.bidi){h.on("load",function(){k.getInputElement().on("keyup",b)
},k)
}CKEDITOR.ui.dialog.labeledElement.call(this,h,j,m,function(){l["aria-labelledby"]=this._.labelId;
this._.required&&(l["aria-required"]=this._.required);
var p=['\x3cdiv class\x3d"cke_dialog_ui_input_textarea" role\x3d"presentation"\x3e\x3ctextarea id\x3d"',n,'" '],o;
for(o in l){p.push(o+'\x3d"'+CKEDITOR.tools.htmlEncode(l[o])+'" ')
}p.push("\x3e",CKEDITOR.tools.htmlEncode(k._["default"]),"\x3c/textarea\x3e\x3c/div\x3e");
return p.join("")
})
}},checkbox:function(h,j,l){if(!(3>arguments.length)){var k=e.call(this,j,{"default":!!j["default"]});
j.validate&&(this.validate=j.validate);
CKEDITOR.ui.dialog.uiElement.call(this,h,j,l,"span",null,null,function(){var p=CKEDITOR.tools.extend({},j,{id:j.id?j.id+"_checkbox":CKEDITOR.tools.getNextId()+"_checkbox"},!0),n=[],o=CKEDITOR.tools.getNextId()+"_label",m={"class":"cke_dialog_ui_checkbox_input",type:"checkbox","aria-labelledby":o};
d(p);
j["default"]&&(m.checked="checked");
"undefined"!=typeof p.inputStyle&&(p.style=p.inputStyle);
k.checkbox=new CKEDITOR.ui.dialog.uiElement(h,p,n,"input",null,m);
n.push(' \x3clabel id\x3d"',o,'" for\x3d"',m.id,'"'+(j.labelStyle?' style\x3d"'+j.labelStyle+'"':"")+"\x3e",CKEDITOR.tools.htmlEncode(j.label),"\x3c/label\x3e");
return n.join("")
})
}},radio:function(h,j,l){if(!(3>arguments.length)){e.call(this,j);
this._["default"]||(this._["default"]=this._.initValue=j.items[0][1]);
j.validate&&(this.validate=j.validate);
var k=[],m=this;
j.role="radiogroup";
j.includeLabel=!0;
CKEDITOR.ui.dialog.labeledElement.call(this,h,j,l,function(){for(var A=[],B=[],z=(j.id?j.id:CKEDITOR.tools.getNextId())+"_radio",x=0;
x<j.items.length;
x++){var w=j.items[x],y=void 0!==w[2]?w[2]:w[0],v=void 0!==w[1]?w[1]:w[0],t=CKEDITOR.tools.getNextId()+"_radio_input",s=t+"_label",t=CKEDITOR.tools.extend({},j,{id:t,title:null,type:null},!0),y=CKEDITOR.tools.extend({},t,{title:y},!0),o={type:"radio","class":"cke_dialog_ui_radio_input",name:z,value:v,"aria-labelledby":s},C=[];
m._["default"]==v&&(o.checked="checked");
d(t);
d(y);
"undefined"!=typeof t.inputStyle&&(t.style=t.inputStyle);
t.keyboardFocusable=!0;
k.push(new CKEDITOR.ui.dialog.uiElement(h,t,C,"input",null,o));
C.push(" ");
new CKEDITOR.ui.dialog.uiElement(h,y,C,"label",null,{id:s,"for":o.id},w[0]);
A.push(C.join(""))
}new CKEDITOR.ui.dialog.hbox(h,k,A,B);
return B.join("")
});
this._.children=k
}},button:function(h,j,m){if(arguments.length){"function"==typeof j&&(j=j(h.getParentEditor()));
e.call(this,j,{disabled:j.disabled||!1});
CKEDITOR.event.implementOn(this);
var k=this;
h.on("load",function(){var o=this.getElement();
(function(){o.on("click",function(p){k.click();
p.data.preventDefault()
});
o.on("keydown",function(p){p.data.getKeystroke() in {32:1}&&(k.click(),p.data.preventDefault())
})
})();
o.unselectable()
},this);
var n=CKEDITOR.tools.extend({},j);
delete n.style;
var l=CKEDITOR.tools.getNextId()+"_label";
CKEDITOR.ui.dialog.uiElement.call(this,h,n,m,"a",null,{style:j.style,href:"javascript:void(0)",title:j.label,hidefocus:"true","class":j["class"],role:"button","aria-labelledby":l},'\x3cspan id\x3d"'+l+'" class\x3d"cke_dialog_ui_button"\x3e'+CKEDITOR.tools.htmlEncode(j.label)+"\x3c/span\x3e")
}},select:function(h,j,l){if(!(3>arguments.length)){var k=e.call(this,j);
j.validate&&(this.validate=j.validate);
k.inputId=CKEDITOR.tools.getNextId()+"_select";
CKEDITOR.ui.dialog.labeledElement.call(this,h,j,l,function(){var r=CKEDITOR.tools.extend({},j,{id:j.id?j.id+"_select":CKEDITOR.tools.getNextId()+"_select"},!0),p=[],q=[],o={id:k.inputId,"class":"cke_dialog_ui_input_select","aria-labelledby":this._.labelId};
p.push('\x3cdiv class\x3d"cke_dialog_ui_input_',j.type,'" role\x3d"presentation"');
j.width&&p.push('style\x3d"width:'+j.width+'" ');
p.push("\x3e");
void 0!==j.size&&(o.size=j.size);
void 0!==j.multiple&&(o.multiple=j.multiple);
d(r);
for(var n=0,m;
n<j.items.length&&(m=j.items[n]);
n++){q.push('\x3coption value\x3d"',CKEDITOR.tools.htmlEncode(void 0!==m[1]?m[1]:m[0]).replace(/"/g,"\x26quot;"),'" /\x3e ',CKEDITOR.tools.htmlEncode(m[0]))
}"undefined"!=typeof r.inputStyle&&(r.style=r.inputStyle);
k.select=new CKEDITOR.ui.dialog.uiElement(h,r,p,"select",null,o,q.join(""));
p.push("\x3c/div\x3e");
return p.join("")
})
}},file:function(h,j,l){if(!(3>arguments.length)){void 0===j["default"]&&(j["default"]="");
var k=CKEDITOR.tools.extend(e.call(this,j),{definition:j,buttons:[]});
j.validate&&(this.validate=j.validate);
h.on("load",function(){CKEDITOR.document.getById(k.frameId).getParent().addClass("cke_dialog_ui_input_file")
});
CKEDITOR.ui.dialog.labeledElement.call(this,h,j,l,function(){k.frameId=CKEDITOR.tools.getNextId()+"_fileInput";
var m=['\x3ciframe frameborder\x3d"0" allowtransparency\x3d"0" class\x3d"cke_dialog_ui_input_file" role\x3d"presentation" id\x3d"',k.frameId,'" title\x3d"',j.label,'" src\x3d"javascript:void('];
m.push(CKEDITOR.env.ie?"(function(){"+encodeURIComponent("document.open();("+CKEDITOR.tools.fixDomain+")();document.close();")+"})()":"0");
m.push(')"\x3e\x3c/iframe\x3e');
return m.join("")
})
}},fileButton:function(h,j,m){var k=this;
if(!(3>arguments.length)){e.call(this,j);
j.validate&&(this.validate=j.validate);
var n=CKEDITOR.tools.extend({},j),l=n.onClick;
n.className=(n.className?n.className+" ":"")+"cke_dialog_ui_button";
n.onClick=function(p){var o=j["for"];
l&&!1===l.call(this,p)||(h.getContentElement(o[0],o[1]).submit(),this.disable())
};
h.on("load",function(){h.getContentElement(j["for"][0],j["for"][1])._.buttons.push(k)
});
CKEDITOR.ui.dialog.button.call(this,h,n,m)
}},html:function(){var h=/^\s*<[\w:]+\s+([^>]*)?>/,j=/^(\s*<[\w:]+(?:\s+[^>]*)?)((?:.|\r|\n)+)$/,k=/\/$/;
return function(r,t,s){if(!(3>arguments.length)){var n=[],q=t.html;
"\x3c"!=q.charAt(0)&&(q="\x3cspan\x3e"+q+"\x3c/span\x3e");
var p=t.focus;
if(p){var o=this.focus;
this.focus=function(){("function"==typeof p?p:o).call(this);
this.fire("focus")
};
t.isFocusable&&(this.isFocusable=this.isFocusable);
this.keyboardFocusable=!0
}CKEDITOR.ui.dialog.uiElement.call(this,r,t,n,"span",null,null,"");
n=n.join("").match(h);
q=q.match(j)||["","",""];
k.test(q[1])&&(q[1]=q[1].slice(0,-1),q[2]="/"+q[2]);
s.push([q[1]," ",n[1]||"",q[2]].join(""))
}}
}(),fieldset:function(h,j,m,k,n){var l=n.label;
this._={children:j};
CKEDITOR.ui.dialog.uiElement.call(this,h,n,k,"fieldset",null,null,function(){var p=[];
l&&p.push("\x3clegend"+(n.labelStyle?' style\x3d"'+n.labelStyle+'"':"")+"\x3e"+l+"\x3c/legend\x3e");
for(var o=0;
o<m.length;
o++){p.push(m[o])
}return p.join("")
})
}},!0);
CKEDITOR.ui.dialog.html.prototype=new CKEDITOR.ui.dialog.uiElement;
CKEDITOR.ui.dialog.labeledElement.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.uiElement,{setLabel:function(h){var j=CKEDITOR.document.getById(this._.labelId);
1>j.getChildCount()?(new CKEDITOR.dom.text(h,CKEDITOR.document)).appendTo(j):j.getChild(0).$.nodeValue=h;
return this
},getLabel:function(){var h=CKEDITOR.document.getById(this._.labelId);
return !h||1>h.getChildCount()?"":h.getChild(0).getText()
},eventProcessors:f},!0);
CKEDITOR.ui.dialog.button.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.uiElement,{click:function(){return this._.disabled?!1:this.fire("click",{dialog:this._.dialog})
},enable:function(){this._.disabled=!1;
var h=this.getElement();
h&&h.removeClass("cke_disabled")
},disable:function(){this._.disabled=!0;
this.getElement().addClass("cke_disabled")
},isVisible:function(){return this.getElement().getFirst().isVisible()
},isEnabled:function(){return !this._.disabled
},eventProcessors:CKEDITOR.tools.extend({},CKEDITOR.ui.dialog.uiElement.prototype.eventProcessors,{onClick:function(h,j){this.on("click",function(){j.apply(this,arguments)
})
}},!0),accessKeyUp:function(){this.click()
},accessKeyDown:function(){this.focus()
},keyboardFocusable:!0},!0);
CKEDITOR.ui.dialog.textInput.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.labeledElement,{getInputElement:function(){return CKEDITOR.document.getById(this._.inputId)
},focus:function(){var h=this.selectParentTab();
setTimeout(function(){var j=h.getInputElement();
j&&j.$.focus()
},0)
},select:function(){var h=this.selectParentTab();
setTimeout(function(){var j=h.getInputElement();
j&&(j.$.focus(),j.$.select())
},0)
},accessKeyUp:function(){this.select()
},setValue:function(h){if(this.bidi){var j=h&&h.charAt(0);
(j=""==j?"ltr":""==j?"rtl":null)&&(h=h.slice(1));
this.setDirectionMarker(j)
}h||(h="");
return CKEDITOR.ui.dialog.uiElement.prototype.setValue.apply(this,arguments)
},getValue:function(){var h=CKEDITOR.ui.dialog.uiElement.prototype.getValue.call(this);
if(this.bidi&&h){var j=this.getDirectionMarker();
j&&(h=("ltr"==j?"":"")+h)
}return h
},setDirectionMarker:function(h){var j=this.getInputElement();
h?j.setAttributes({dir:h,"data-cke-dir-marker":h}):this.getDirectionMarker()&&j.removeAttributes(["dir","data-cke-dir-marker"])
},getDirectionMarker:function(){return this.getInputElement().data("cke-dir-marker")
},keyboardFocusable:!0},g,!0);
CKEDITOR.ui.dialog.textarea.prototype=new CKEDITOR.ui.dialog.textInput;
CKEDITOR.ui.dialog.select.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.labeledElement,{getInputElement:function(){return this._.select.getElement()
},add:function(h,j,l){var k=new CKEDITOR.dom.element("option",this.getDialog().getParentEditor().document),m=this.getInputElement().$;
k.$.text=h;
k.$.value=void 0===j||null===j?h:j;
void 0===l||null===l?CKEDITOR.env.ie?m.add(k.$):m.add(k.$,null):m.add(k.$,l);
return this
},remove:function(h){this.getInputElement().$.remove(h);
return this
},clear:function(){for(var h=this.getInputElement().$;
0<h.length;
){h.remove(0)
}return this
},keyboardFocusable:!0},g,!0);
CKEDITOR.ui.dialog.checkbox.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.uiElement,{getInputElement:function(){return this._.checkbox.getElement()
},setValue:function(h,j){this.getInputElement().$.checked=h;
!j&&this.fire("change",{value:h})
},getValue:function(){return this.getInputElement().$.checked
},accessKeyUp:function(){this.setValue(!this.getValue())
},eventProcessors:{onChange:function(h,j){if(!CKEDITOR.env.ie||8<CKEDITOR.env.version){return f.onChange.apply(this,arguments)
}h.on("load",function(){var k=this._.checkbox.getElement();
k.on("propertychange",function(l){l=l.data.$;
"checked"==l.propertyName&&this.fire("change",{value:k.$.checked})
},this)
},this);
this.on("change",j);
return null
}},keyboardFocusable:!0},g,!0);
CKEDITOR.ui.dialog.radio.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.uiElement,{setValue:function(h,j){for(var l=this._.children,k,m=0;
m<l.length&&(k=l[m]);
m++){k.getElement().$.checked=k.getValue()==h
}!j&&this.fire("change",{value:h})
},getValue:function(){for(var h=this._.children,j=0;
j<h.length;
j++){if(h[j].getElement().$.checked){return h[j].getValue()
}}return null
},accessKeyUp:function(){var h=this._.children,j;
for(j=0;
j<h.length;
j++){if(h[j].getElement().$.checked){h[j].getElement().focus();
return
}}h[0].getElement().focus()
},eventProcessors:{onChange:function(h,j){if(!CKEDITOR.env.ie||8<CKEDITOR.env.version){return f.onChange.apply(this,arguments)
}h.on("load",function(){for(var l=this._.children,k=this,m=0;
m<l.length;
m++){l[m].getElement().on("propertychange",function(n){n=n.data.$;
"checked"==n.propertyName&&this.$.checked&&k.fire("change",{value:this.getAttribute("value")})
})
}},this);
this.on("change",j);
return null
}}},g,!0);
CKEDITOR.ui.dialog.file.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.labeledElement,g,{getInputElement:function(){var h=CKEDITOR.document.getById(this._.frameId).getFrameDocument();
return 0<h.$.forms.length?new CKEDITOR.dom.element(h.$.forms[0].elements[0]):this.getElement()
},submit:function(){this.getInputElement().getParent().$.submit();
return this
},getAction:function(){return this.getInputElement().getParent().$.action
},registerEvents:function(h){var j=/^on([A-Z]\w+)/,l,k=function(o,n,q,p){o.on("formLoaded",function(){o.getInputElement().on(q,p,o)
})
},m;
for(m in h){if(l=m.match(j)){this.eventProcessors[m]?this.eventProcessors[m].call(this,this._.dialog,h[m]):k(this,this._.dialog,l[1].toLowerCase(),h[m])
}}return this
},reset:function(){function r(){p.$.open();
var k="";
n.size&&(k=n.size-(CKEDITOR.env.ie?7:0));
var m=s.frameId+"_input";
p.$.write(['\x3chtml dir\x3d"'+l+'" lang\x3d"'+j+'"\x3e\x3chead\x3e\x3ctitle\x3e\x3c/title\x3e\x3c/head\x3e\x3cbody style\x3d"margin: 0; overflow: hidden; background: transparent;"\x3e','\x3cform enctype\x3d"multipart/form-data" method\x3d"POST" dir\x3d"'+l+'" lang\x3d"'+j+'" action\x3d"',CKEDITOR.tools.htmlEncode(n.action),'"\x3e\x3clabel id\x3d"',s.labelId,'" for\x3d"',m,'" style\x3d"display:none"\x3e',CKEDITOR.tools.htmlEncode(n.label),'\x3c/label\x3e\x3cinput style\x3d"width:100%" id\x3d"',m,'" aria-labelledby\x3d"',s.labelId,'" type\x3d"file" name\x3d"',CKEDITOR.tools.htmlEncode(n.id||"cke_upload"),'" size\x3d"',CKEDITOR.tools.htmlEncode(0<k?k:""),'" /\x3e\x3c/form\x3e\x3c/body\x3e\x3c/html\x3e\x3cscript\x3e',CKEDITOR.env.ie?"("+CKEDITOR.tools.fixDomain+")();":"","window.parent.CKEDITOR.tools.callFunction("+o+");","window.onbeforeunload \x3d function() {window.parent.CKEDITOR.tools.callFunction("+h+")}","\x3c/script\x3e"].join(""));
p.$.close();
for(k=0;
k<q.length;
k++){q[k].enable()
}}var s=this._,p=CKEDITOR.document.getById(s.frameId).getFrameDocument(),n=s.definition,q=s.buttons,o=this.formLoadedNumber,h=this.formUnloadNumber,l=s.dialog._.editor.lang.dir,j=s.dialog._.editor.langCode;
o||(o=this.formLoadedNumber=CKEDITOR.tools.addFunction(function(){this.fire("formLoaded")
},this),h=this.formUnloadNumber=CKEDITOR.tools.addFunction(function(){this.getInputElement().clearCustomData()
},this),this.getDialog()._.editor.on("destroy",function(){CKEDITOR.tools.removeFunction(o);
CKEDITOR.tools.removeFunction(h)
}));
CKEDITOR.env.gecko?setTimeout(r,500):r()
},getValue:function(){return this.getInputElement().$.value||""
},setInitValue:function(){this._.initValue=""
},eventProcessors:{onChange:function(h,j){this._.domOnChangeRegistered||(this.on("formLoaded",function(){this.getInputElement().on("change",function(){this.fire("change",{value:this.getValue()})
},this)
},this),this._.domOnChangeRegistered=!0);
this.on("change",j)
}},keyboardFocusable:!0},!0);
CKEDITOR.ui.dialog.fileButton.prototype=new CKEDITOR.ui.dialog.button;
CKEDITOR.ui.dialog.fieldset.prototype=CKEDITOR.tools.clone(CKEDITOR.ui.dialog.hbox.prototype);
CKEDITOR.dialog.addUIElement("text",c);
CKEDITOR.dialog.addUIElement("password",c);
CKEDITOR.dialog.addUIElement("textarea",i);
CKEDITOR.dialog.addUIElement("checkbox",i);
CKEDITOR.dialog.addUIElement("radio",i);
CKEDITOR.dialog.addUIElement("button",i);
CKEDITOR.dialog.addUIElement("select",i);
CKEDITOR.dialog.addUIElement("file",i);
CKEDITOR.dialog.addUIElement("fileButton",i);
CKEDITOR.dialog.addUIElement("html",i);
CKEDITOR.dialog.addUIElement("fieldset",{build:function(r,s,p){for(var n=s.children,q,o=[],l=[],m=0;
m<n.length&&(q=n[m]);
m++){var j=[];
o.push(j);
l.push(CKEDITOR.dialog._.uiElementBuilders[q.type].build(r,q,j))
}return new CKEDITOR.ui.dialog[s.type](r,l,o,p,s)
}})
}});
CKEDITOR.DIALOG_RESIZE_NONE=0;
CKEDITOR.DIALOG_RESIZE_WIDTH=1;
CKEDITOR.DIALOG_RESIZE_HEIGHT=2;
CKEDITOR.DIALOG_RESIZE_BOTH=3;
CKEDITOR.DIALOG_STATE_IDLE=1;
CKEDITOR.DIALOG_STATE_BUSY=2;
(function(){function y(){for(var v=this._.tabIdList.length,u=CKEDITOR.tools.indexOf(this._.tabIdList,this._.currentTabId)+v,w=u-1;
w>u-v;
w--){if(this._.tabs[this._.tabIdList[w%v]][0].$.offsetHeight){return this._.tabIdList[w%v]
}}return null
}function ad(){for(var v=this._.tabIdList.length,u=CKEDITOR.tools.indexOf(this._.tabIdList,this._.currentTabId),w=u+1;
w<u+v;
w++){if(this._.tabs[this._.tabIdList[w%v]][0].$.offsetHeight){return this._.tabIdList[w%v]
}}return null
}function r(v,u){for(var A=v.$.getElementsByTagName("input"),z=0,x=A.length;
z<x;
z++){var w=new CKEDITOR.dom.element(A[z]);
"text"==w.getAttribute("type").toLowerCase()&&(u?(w.setAttribute("value",w.getCustomData("fake_value")||""),w.removeCustomData("fake_value")):(w.setCustomData("fake_value",w.getAttribute("value")),w.setAttribute("value","")))
}}function g(v,u){var w=this.getInputElement();
w&&(v?w.removeAttribute("aria-invalid"):w.setAttribute("aria-invalid",!0));
v||(this.select?this.select():this.focus());
u&&alert(u);
this.fire("validated",{valid:v,msg:u})
}function f(){var u=this.getInputElement();
u&&u.removeAttribute("aria-invalid")
}function e(v){var u=CKEDITOR.dom.element.createFromHtml(CKEDITOR.addTemplate("dialog",d).output({id:CKEDITOR.tools.getNextNumber(),editorId:v.id,langDir:v.lang.dir,langCode:v.langCode,editorDialogClass:"cke_editor_"+v.name.replace(/\./g,"\\.")+"_dialog",closeTitle:v.lang.common.close,hidpi:CKEDITOR.env.hidpi?"cke_hidpi":""})),z=u.getChild([0,0,0,0,0]),x=z.getChild(0),w=z.getChild(1);
v.plugins.clipboard&&CKEDITOR.plugins.clipboard.preventDefaultDropOnElement(z);
!CKEDITOR.env.ie||CKEDITOR.env.quirks||CKEDITOR.env.edge||(v="javascript:void(function(){"+encodeURIComponent("document.open();("+CKEDITOR.tools.fixDomain+")();document.close();")+"}())",CKEDITOR.dom.element.createFromHtml('\x3ciframe frameBorder\x3d"0" class\x3d"cke_iframe_shim" src\x3d"'+v+'" tabIndex\x3d"-1"\x3e\x3c/iframe\x3e').appendTo(z.getParent()));
x.unselectable();
w.unselectable();
return{element:u,parts:{dialog:u.getChild(0),title:x,close:w,tabs:z.getChild(2),contents:z.getChild([3,0,0,0]),footer:z.getChild([3,0,1,0])}}
}function p(v,u,w){this.element=u;
this.focusIndex=w;
this.tabIndex=0;
this.isFocusable=function(){return !u.getAttribute("disabled")&&u.isVisible()
};
this.focus=function(){v._.currentFocusIndex=this.focusIndex;
this.element.focus()
};
u.on("keydown",function(x){x.data.getKeystroke() in {32:1,13:1}&&this.fire("click")
});
u.on("focus",function(){this.fire("mouseover")
});
u.on("blur",function(){this.fire("mouseout")
})
}function c(v){function u(){v.layout()
}var w=CKEDITOR.document.getWindow();
w.on("resize",u);
v.on("hide",function(){w.removeListener("resize",u)
})
}function o(v,u){this._={dialog:v};
CKEDITOR.tools.extend(this,u)
}function b(v){function u(F){var N=v.getSize(),J=CKEDITOR.document.getWindow().getViewPaneSize(),L=F.data.$.screenX,M=F.data.$.screenY,K=L-D.x,I=M-D.y;
D={x:L,y:M};
A.x+=K;
A.y+=I;
v.move(A.x+w[3]<x?-w[3]:A.x-w[1]>J.width-N.width-x?J.width-N.width+("rtl"==z.lang.dir?0:w[1]):A.x,A.y+w[0]<x?-w[0]:A.y-w[2]>J.height-N.height-x?J.height-N.height+w[2]:A.y,1);
F.data.preventDefault()
}function E(){CKEDITOR.document.removeListener("mousemove",u);
CKEDITOR.document.removeListener("mouseup",E);
if(CKEDITOR.env.ie6Compat){var F=G.getChild(0).getFrameDocument();
F.removeListener("mousemove",u);
F.removeListener("mouseup",E)
}}var D=null,A=null,z=v.getParentEditor(),x=z.config.dialog_magnetDistance,w=CKEDITOR.skin.margins||[0,0,0,0];
"undefined"==typeof x&&(x=20);
v.parts.title.on("mousedown",function(F){D={x:F.data.$.screenX,y:F.data.$.screenY};
CKEDITOR.document.on("mousemove",u);
CKEDITOR.document.on("mouseup",E);
A=v.getPosition();
if(CKEDITOR.env.ie6Compat){var I=G.getChild(0).getFrameDocument();
I.on("mousemove",u);
I.on("mouseup",E)
}F.data.preventDefault()
},v)
}function a(L){function K(M){var T="rtl"==E.lang.dir,S=z.width,P=z.height,O=S+(M.data.$.screenX-x.x)*(T?-1:1)*(L._.moved?1:2),N=P+(M.data.$.screenY-x.y)*(L._.moved?1:2),R=L._.element.getFirst(),R=T&&R.getComputedStyle("right"),Q=L.getPosition();
Q.y+N>v.height&&(N=v.height-Q.y);
(T?R:Q.x)+O>v.width&&(O=v.width-(T?R:Q.x));
if(F==CKEDITOR.DIALOG_RESIZE_WIDTH||F==CKEDITOR.DIALOG_RESIZE_BOTH){S=Math.max(I.minWidth||0,O-D)
}if(F==CKEDITOR.DIALOG_RESIZE_HEIGHT||F==CKEDITOR.DIALOG_RESIZE_BOTH){P=Math.max(I.minHeight||0,N-A)
}L.resize(S,P);
L._.moved||L.layout();
M.data.preventDefault()
}function J(){CKEDITOR.document.removeListener("mouseup",J);
CKEDITOR.document.removeListener("mousemove",K);
u&&(u.remove(),u=null);
if(CKEDITOR.env.ie6Compat){var M=G.getChild(0).getFrameDocument();
M.removeListener("mouseup",J);
M.removeListener("mousemove",K)
}}var I=L.definition,F=I.resizable;
if(F!=CKEDITOR.DIALOG_RESIZE_NONE){var E=L.getParentEditor(),D,A,v,x,z,u,w=CKEDITOR.tools.addFunction(function(M){z=L.getSize();
var N=L.parts.contents;
N.$.getElementsByTagName("iframe").length&&(u=CKEDITOR.dom.element.createFromHtml('\x3cdiv class\x3d"cke_dialog_resize_cover" style\x3d"height: 100%; position: absolute; width: 100%;"\x3e\x3c/div\x3e'),N.append(u));
A=z.height-L.parts.contents.getSize("height",!(CKEDITOR.env.gecko||CKEDITOR.env.ie&&CKEDITOR.env.quirks));
D=z.width-L.parts.contents.getSize("width",1);
x={x:M.screenX,y:M.screenY};
v=CKEDITOR.document.getWindow().getViewPaneSize();
CKEDITOR.document.on("mousemove",K);
CKEDITOR.document.on("mouseup",J);
CKEDITOR.env.ie6Compat&&(N=G.getChild(0).getFrameDocument(),N.on("mousemove",K),N.on("mouseup",J));
M.preventDefault&&M.preventDefault()
});
L.on("load",function(){var M="";
F==CKEDITOR.DIALOG_RESIZE_WIDTH?M=" cke_resizer_horizontal":F==CKEDITOR.DIALOG_RESIZE_HEIGHT&&(M=" cke_resizer_vertical");
M=CKEDITOR.dom.element.createFromHtml('\x3cdiv class\x3d"cke_resizer'+M+" cke_resizer_"+E.lang.dir+'" title\x3d"'+CKEDITOR.tools.htmlEncode(E.lang.common.resize)+'" onmousedown\x3d"CKEDITOR.tools.callFunction('+w+', event )"\x3e'+("ltr"==E.lang.dir?"◢":"◣")+"\x3c/div\x3e");
L.parts.footer.append(M,1)
});
E.on("destroy",function(){CKEDITOR.tools.removeFunction(w)
})
}}function t(u){u.data.preventDefault(1)
}function n(F){var E=CKEDITOR.document.getWindow(),D=F.config,A=D.dialog_backgroundCoverColor||"white",z=D.dialog_backgroundCoverOpacity,x=D.baseFloatZIndex,D=CKEDITOR.tools.genKey(A,z,x),w=q[D];
w?w.show():(x=['\x3cdiv tabIndex\x3d"-1" style\x3d"position: ',CKEDITOR.env.ie6Compat?"absolute":"fixed","; z-index: ",x,"; top: 0px; left: 0px; ",CKEDITOR.env.ie6Compat?"":"background-color: "+A,'" class\x3d"cke_dialog_background_cover"\x3e'],CKEDITOR.env.ie6Compat&&(A="\x3chtml\x3e\x3cbody style\x3d\\'background-color:"+A+";\\'\x3e\x3c/body\x3e\x3c/html\x3e",x.push('\x3ciframe hidefocus\x3d"true" frameborder\x3d"0" id\x3d"cke_dialog_background_iframe" src\x3d"javascript:'),x.push("void((function(){"+encodeURIComponent("document.open();("+CKEDITOR.tools.fixDomain+")();document.write( '"+A+"' );document.close();")+"})())"),x.push('" style\x3d"position:absolute;left:0;top:0;width:100%;height: 100%;filter: progid:DXImageTransform.Microsoft.Alpha(opacity\x3d0)"\x3e\x3c/iframe\x3e')),x.push("\x3c/div\x3e"),w=CKEDITOR.dom.element.createFromHtml(x.join("")),w.setOpacity(void 0!==z?z:0.5),w.on("keydown",t),w.on("keypress",t),w.on("keyup",t),w.appendTo(CKEDITOR.document.getBody()),q[D]=w);
F.focusManager.add(w);
G=w;
F=function(){var I=E.getViewPaneSize();
w.setStyles({width:I.width+"px",height:I.height+"px"})
};
var v=function(){var I=E.getScrollPosition(),J=CKEDITOR.dialog._.currentTop;
w.setStyles({left:I.x+"px",top:I.y+"px"});
if(J){do{I=J.getPosition(),J.move(I.x,I.y)
}while(J=J._.parentDialog)
}};
s=F;
E.on("resize",F);
F();
CKEDITOR.env.mac&&CKEDITOR.env.webkit||w.focus();
if(CKEDITOR.env.ie6Compat){var u=function(){v();
arguments.callee.prevScrollHandler.apply(this,arguments)
};
E.$.setTimeout(function(){u.prevScrollHandler=window.onscroll||function(){};
window.onscroll=u
},0);
v()
}}function m(u){G&&(u.focusManager.remove(G),u=CKEDITOR.document.getWindow(),G.hide(),u.removeListener("resize",s),CKEDITOR.env.ie6Compat&&u.$.setTimeout(function(){window.onscroll=window.onscroll&&window.onscroll.prevScrollHandler||null
},0),s=null)
}var C=CKEDITOR.tools.cssLength,d='\x3cdiv class\x3d"cke_reset_all {editorId} {editorDialogClass} {hidpi}" dir\x3d"{langDir}" lang\x3d"{langCode}" role\x3d"dialog" aria-labelledby\x3d"cke_dialog_title_{id}"\x3e\x3ctable class\x3d"cke_dialog '+CKEDITOR.env.cssClass+' cke_{langDir}" style\x3d"position:absolute" role\x3d"presentation"\x3e\x3ctr\x3e\x3ctd role\x3d"presentation"\x3e\x3cdiv class\x3d"cke_dialog_body" role\x3d"presentation"\x3e\x3cdiv id\x3d"cke_dialog_title_{id}" class\x3d"cke_dialog_title" role\x3d"presentation"\x3e\x3c/div\x3e\x3ca id\x3d"cke_dialog_close_button_{id}" class\x3d"cke_dialog_close_button" href\x3d"javascript:void(0)" title\x3d"{closeTitle}" role\x3d"button"\x3e\x3cspan class\x3d"cke_label"\x3eX\x3c/span\x3e\x3c/a\x3e\x3cdiv id\x3d"cke_dialog_tabs_{id}" class\x3d"cke_dialog_tabs" role\x3d"tablist"\x3e\x3c/div\x3e\x3ctable class\x3d"cke_dialog_contents" role\x3d"presentation"\x3e\x3ctr\x3e\x3ctd id\x3d"cke_dialog_contents_{id}" class\x3d"cke_dialog_contents_body" role\x3d"presentation"\x3e\x3c/td\x3e\x3c/tr\x3e\x3ctr\x3e\x3ctd id\x3d"cke_dialog_footer_{id}" class\x3d"cke_dialog_footer" role\x3d"presentation"\x3e\x3c/td\x3e\x3c/tr\x3e\x3c/table\x3e\x3c/div\x3e\x3c/td\x3e\x3c/tr\x3e\x3c/table\x3e\x3c/div\x3e';
CKEDITOR.dialog=function(O,N){function L(){var R=A._.focusList;
R.sort(function(U,T){return U.tabIndex!=T.tabIndex?T.tabIndex-U.tabIndex:U.focusIndex-T.focusIndex
});
for(var Q=R.length,S=0;
S<Q;
S++){R[S].focusIndex=S
}}function K(R){var Q=A._.focusList;
R=R||0;
if(!(1>Q.length)){var V=A._.currentFocusIndex;
A._.tabBarMode&&0>R&&(V=0);
try{Q[V].getInputElement().$.blur()
}catch(T){}var U=V,S=1<A._.pageCount;
do{U+=R;
if(S&&!A._.tabBarMode&&(U==Q.length||-1==U)){A._.tabBarMode=!0;
A._.tabs[A._.currentTabId][0].focus();
A._.currentFocusIndex=-1;
return
}U=(U+Q.length)%Q.length;
if(U==V){break
}}while(R&&!Q[U].isFocusable());
Q[U].focus();
"text"==Q[U].type&&Q[U].select()
}}function J(Q){if(A==CKEDITOR.dialog._.currentTop){var T=Q.data.getKeystroke(),S="rtl"==O.lang.dir,R=[37,38,39,40];
v=x=0;
if(9==T||T==CKEDITOR.SHIFT+9){K(T==CKEDITOR.SHIFT+9?-1:1),v=1
}else{if(T==CKEDITOR.ALT+121&&!A._.tabBarMode&&1<A.getPageCount()){A._.tabBarMode=!0,A._.tabs[A._.currentTabId][0].focus(),A._.currentFocusIndex=-1,v=1
}else{if(-1!=CKEDITOR.tools.indexOf(R,T)&&A._.tabBarMode){T=-1!=CKEDITOR.tools.indexOf([S?39:37,38],T)?y.call(A):ad.call(A),A.selectPage(T),A._.tabs[T][0].focus(),v=1
}else{if(13!=T&&32!=T||!A._.tabBarMode){if(13==T){T=Q.data.getTarget(),T.is("a","button","select","textarea")||T.is("input")&&"button"==T.$.type||((T=this.getButton("ok"))&&CKEDITOR.tools.setTimeout(T.click,0,T),v=1),x=1
}else{if(27==T){(T=this.getButton("cancel"))?CKEDITOR.tools.setTimeout(T.click,0,T):!1!==this.fire("cancel",{hide:!0}).hide&&this.hide(),x=1
}else{return
}}}else{this.selectPage(this._.currentTabId),this._.tabBarMode=!1,this._.currentFocusIndex=-1,K(1),v=1
}}}}I(Q)
}}function I(Q){v?Q.data.preventDefault(1):x&&Q.data.stopPropagation()
}var F=CKEDITOR.dialog._.dialogDefinitions[N],E=CKEDITOR.tools.clone(af),w=O.config.dialog_buttonsOrder||"OS",z=O.lang.dir,D={},v,x;
("OS"==w&&CKEDITOR.env.mac||"rtl"==w&&"ltr"==z||"ltr"==w&&"rtl"==z)&&E.buttons.reverse();
F=CKEDITOR.tools.extend(F(O),E);
F=CKEDITOR.tools.clone(F);
F=new l(this,F);
E=e(O);
this._={editor:O,element:E.element,name:N,contentSize:{width:0,height:0},size:{width:0,height:0},contents:{},buttons:{},accessKeyMap:{},tabs:{},tabIdList:[],currentTabId:null,currentTabIndex:null,pageCount:0,lastTab:null,tabBarMode:!1,focusList:[],currentFocusIndex:0,hasFocus:!1};
this.parts=E.parts;
CKEDITOR.tools.setTimeout(function(){O.fire("ariaWidget",this.parts.contents)
},0,this);
E={position:CKEDITOR.env.ie6Compat?"absolute":"fixed",top:0,visibility:"hidden"};
E["rtl"==z?"right":"left"]=0;
this.parts.dialog.setStyles(E);
CKEDITOR.event.call(this);
this.definition=F=CKEDITOR.fire("dialogDefinition",{name:N,definition:F},O).definition;
if(!("removeDialogTabs" in O._)&&O.config.removeDialogTabs){E=O.config.removeDialogTabs.split(";");
for(z=0;
z<E.length;
z++){if(w=E[z].split(":"),2==w.length){var u=w[0];
D[u]||(D[u]=[]);
D[u].push(w[1])
}}O._.removeDialogTabs=D
}if(O._.removeDialogTabs&&(D=O._.removeDialogTabs[N])){for(z=0;
z<D.length;
z++){F.removeContents(D[z])
}}if(F.onLoad){this.on("load",F.onLoad)
}if(F.onShow){this.on("show",F.onShow)
}if(F.onHide){this.on("hide",F.onHide)
}if(F.onOk){this.on("ok",function(Q){O.fire("saveSnapshot");
setTimeout(function(){O.fire("saveSnapshot")
},0);
!1===F.onOk.call(this,Q)&&(Q.data.hide=!1)
})
}this.state=CKEDITOR.DIALOG_STATE_IDLE;
if(F.onCancel){this.on("cancel",function(Q){!1===F.onCancel.call(this,Q)&&(Q.data.hide=!1)
})
}var A=this,P=function(R){var Q=A._.contents,U=!1,T;
for(T in Q){for(var S in Q[T]){if(U=R.call(this,Q[T][S])){return
}}}};
this.on("ok",function(Q){P(function(R){if(R.validate){var T=R.validate(this),S="string"==typeof T||!1===T;
S&&(Q.data.hide=!1,Q.stop());
g.call(R,!S,"string"==typeof T?T:void 0);
return S
}})
},this,null,0);
this.on("cancel",function(Q){P(function(R){if(R.isChanged()){return O.config.dialog_noConfirmCancel||confirm(O.lang.common.confirmCancel)||(Q.data.hide=!1),!0
}})
},this,null,0);
this.parts.close.on("click",function(Q){!1!==this.fire("cancel",{hide:!0}).hide&&this.hide();
Q.data.preventDefault()
},this);
this.changeFocus=K;
var M=this._.element;
O.focusManager.add(M,1);
this.on("show",function(){M.on("keydown",J,this);
if(CKEDITOR.env.gecko){M.on("keypress",I,this)
}});
this.on("hide",function(){M.removeListener("keydown",J);
CKEDITOR.env.gecko&&M.removeListener("keypress",I);
P(function(Q){f.apply(Q)
})
});
this.on("iframeAdded",function(Q){(new CKEDITOR.dom.document(Q.data.iframe.$.contentWindow.document)).on("keydown",J,this,null,0)
});
this.on("show",function(){L();
var Q=1<A._.pageCount;
O.config.dialog_startupFocusTab&&Q?(A._.tabBarMode=!0,A._.tabs[A._.currentTabId][0].focus(),A._.currentFocusIndex=-1):this._.hasFocus||(this._.currentFocusIndex=Q?-1:this._.focusList.length-1,F.onFocus?(Q=F.onFocus.call(this))&&Q.focus():K(1))
},this,null,4294967295);
if(CKEDITOR.env.ie6Compat){this.on("load",function(){var R=this.getElement(),Q=R.getFirst();
Q.remove();
Q.appendTo(R)
},this)
}b(this);
a(this);
(new CKEDITOR.dom.text(F.title,CKEDITOR.document)).appendTo(this.parts.title);
for(z=0;
z<F.contents.length;
z++){(D=F.contents[z])&&this.addPage(D)
}this.parts.tabs.on("click",function(R){var Q=R.data.getTarget();
Q.hasClass("cke_dialog_tab")&&(Q=Q.$.id,this.selectPage(Q.substring(4,Q.lastIndexOf("_"))),this._.tabBarMode&&(this._.tabBarMode=!1,this._.currentFocusIndex=-1,K(1)),R.data.preventDefault())
},this);
z=[];
D=CKEDITOR.dialog._.uiElementBuilders.hbox.build(this,{type:"hbox",className:"cke_dialog_footer_buttons",widths:[],children:F.buttons},z).getChild();
this.parts.footer.setHtml(z.join(""));
for(z=0;
z<D.length;
z++){this._.buttons[D[z].id]=D[z]
}};
CKEDITOR.dialog.prototype={destroy:function(){this.hide();
this._.element.remove()
},resize:function(){return function(v,u){this._.contentSize&&this._.contentSize.width==v&&this._.contentSize.height==u||(CKEDITOR.dialog.fire("resize",{dialog:this,width:v,height:u},this._.editor),this.fire("resize",{width:v,height:u},this._.editor),this.parts.contents.setStyles({width:v+"px",height:u+"px"}),"rtl"==this._.editor.lang.dir&&this._.position&&(this._.position.x=CKEDITOR.document.getWindow().getViewPaneSize().width-this._.contentSize.width-parseInt(this._.element.getFirst().getStyle("right"),10)),this._.contentSize={width:v,height:u})
}
}(),getSize:function(){var u=this._.element.getFirst();
return{width:u.$.offsetWidth||0,height:u.$.offsetHeight||0}
},move:function(v,u,A){var z=this._.element.getFirst(),x="rtl"==this._.editor.lang.dir,w="fixed"==z.getComputedStyle("position");
CKEDITOR.env.ie&&z.setStyle("zoom","100%");
w&&this._.position&&this._.position.x==v&&this._.position.y==u||(this._.position={x:v,y:u},w||(w=CKEDITOR.document.getWindow().getScrollPosition(),v+=w.x,u+=w.y),x&&(w=this.getSize(),v=CKEDITOR.document.getWindow().getViewPaneSize().width-w.width-v),u={top:(0<u?u:0)+"px"},u[x?"right":"left"]=(0<v?v:0)+"px",z.setStyles(u),A&&(this._.moved=1))
},getPosition:function(){return CKEDITOR.tools.extend({},this._.position)
},show:function(){var v=this._.element,u=this.definition;
v.getParent()&&v.getParent().equals(CKEDITOR.document.getBody())?v.setStyle("display","block"):v.appendTo(CKEDITOR.document.getBody());
this.resize(this._.contentSize&&this._.contentSize.width||u.width||u.minWidth,this._.contentSize&&this._.contentSize.height||u.height||u.minHeight);
this.reset();
this.selectPage(this.definition.contents[0].id);
null===CKEDITOR.dialog._.currentZIndex&&(CKEDITOR.dialog._.currentZIndex=this._.editor.config.baseFloatZIndex);
this._.element.getFirst().setStyle("z-index",CKEDITOR.dialog._.currentZIndex+=10);
null===CKEDITOR.dialog._.currentTop?(CKEDITOR.dialog._.currentTop=this,this._.parentDialog=null,n(this._.editor)):(this._.parentDialog=CKEDITOR.dialog._.currentTop,this._.parentDialog.getElement().getFirst().$.style.zIndex-=Math.floor(this._.editor.config.baseFloatZIndex/2),CKEDITOR.dialog._.currentTop=this);
v.on("keydown",j);
v.on("keyup",i);
this._.hasFocus=!1;
for(var E in u.contents){if(u.contents[E]){var v=u.contents[E],D=this._.tabs[v.id],A=v.requiredContent,z=0;
if(D){for(var x in this._.contents[v.id]){var w=this._.contents[v.id][x];
"hbox"!=w.type&&"vbox"!=w.type&&w.getInputElement()&&(w.requiredContent&&!this._.editor.activeFilter.check(w.requiredContent)?w.disable():(w.enable(),z++))
}!z||A&&!this._.editor.activeFilter.check(A)?D[0].addClass("cke_dialog_tab_disabled"):D[0].removeClass("cke_dialog_tab_disabled")
}}}CKEDITOR.tools.setTimeout(function(){this.layout();
c(this);
this.parts.dialog.setStyle("visibility","");
this.fireOnce("load",{});
CKEDITOR.ui.fire("ready",this);
this.fire("show",{});
this._.editor.fire("dialogShow",this);
this._.parentDialog||this._.editor.focusManager.lock();
this.foreach(function(F){F.setInitValue&&F.setInitValue()
})
},100,this)
},layout:function(){var v=this.parts.dialog,u=this.getSize(),z=CKEDITOR.document.getWindow().getViewPaneSize(),x=(z.width-u.width)/2,w=(z.height-u.height)/2;
CKEDITOR.env.ie6Compat||(u.height+(0<w?w:0)>z.height||u.width+(0<x?x:0)>z.width?v.setStyle("position","absolute"):v.setStyle("position","fixed"));
this.move(this._.moved?this._.position.x:x,this._.moved?this._.position.y:w)
},foreach:function(v){for(var u in this._.contents){for(var w in this._.contents[u]){v.call(this,this._.contents[u][w])
}}return this
},reset:function(){var u=function(v){v.reset&&v.reset(1)
};
return function(){this.foreach(u);
return this
}
}(),setupContent:function(){var u=arguments;
this.foreach(function(v){v.setup&&v.setup.apply(v,u)
})
},commitContent:function(){var u=arguments;
this.foreach(function(v){CKEDITOR.env.ie&&this._.currentFocusIndex==v.focusIndex&&v.getInputElement().$.blur();
v.commit&&v.commit.apply(v,u)
})
},hide:function(){if(this.parts.dialog.isVisible()){this.fire("hide",{});
this._.editor.fire("dialogHide",this);
this.selectPage(this._.tabIdList[0]);
var v=this._.element;
v.setStyle("display","none");
this.parts.dialog.setStyle("visibility","hidden");
for(k(this);
CKEDITOR.dialog._.currentTop!=this;
){CKEDITOR.dialog._.currentTop.hide()
}if(this._.parentDialog){var u=this._.parentDialog.getElement().getFirst();
u.setStyle("z-index",parseInt(u.$.style.zIndex,10)+Math.floor(this._.editor.config.baseFloatZIndex/2))
}else{m(this._.editor)
}if(CKEDITOR.dialog._.currentTop=this._.parentDialog){CKEDITOR.dialog._.currentZIndex-=10
}else{CKEDITOR.dialog._.currentZIndex=null;
v.removeListener("keydown",j);
v.removeListener("keyup",i);
var w=this._.editor;
w.focus();
setTimeout(function(){w.focusManager.unlock();
CKEDITOR.env.iOS&&w.window.focus()
},0)
}delete this._.parentDialog;
this.foreach(function(x){x.resetInitValue&&x.resetInitValue()
});
this.setState(CKEDITOR.DIALOG_STATE_IDLE)
}},addPage:function(v){if(!v.requiredContent||this._.editor.filter.check(v.requiredContent)){for(var u=[],D=v.label?' title\x3d"'+CKEDITOR.tools.htmlEncode(v.label)+'"':"",A=CKEDITOR.dialog._.uiElementBuilders.vbox.build(this,{type:"vbox",className:"cke_dialog_page_contents",children:v.elements,expand:!!v.expand,padding:v.padding,style:v.style||"width: 100%;"},u),z=this._.contents[v.id]={},x=A.getChild(),w=0;
A=x.shift();
){A.notAllowed||"hbox"==A.type||"vbox"==A.type||w++,z[A.id]=A,"function"==typeof A.getChild&&x.push.apply(x,A.getChild())
}w||(v.hidden=!0);
u=CKEDITOR.dom.element.createFromHtml(u.join(""));
u.setAttribute("role","tabpanel");
A=CKEDITOR.env;
z="cke_"+v.id+"_"+CKEDITOR.tools.getNextNumber();
D=CKEDITOR.dom.element.createFromHtml(['\x3ca class\x3d"cke_dialog_tab"',0<this._.pageCount?" cke_last":"cke_first",D,v.hidden?' style\x3d"display:none"':"",' id\x3d"',z,'"',A.gecko&&!A.hc?"":' href\x3d"javascript:void(0)"',' tabIndex\x3d"-1" hidefocus\x3d"true" role\x3d"tab"\x3e',v.label,"\x3c/a\x3e"].join(""));
u.setAttribute("aria-labelledby",z);
this._.tabs[v.id]=[D,u];
this._.tabIdList.push(v.id);
!v.hidden&&this._.pageCount++;
this._.lastTab=D;
this.updateStyle();
u.setAttribute("name",v.id);
u.appendTo(this.parts.contents);
D.unselectable();
this.parts.tabs.append(D);
v.accessKey&&(h(this,this,"CTRL+"+v.accessKey,ae,ag),this._.accessKeyMap["CTRL+"+v.accessKey]=v.id)
}},selectPage:function(v){if(this._.currentTabId!=v&&!this._.tabs[v][0].hasClass("cke_dialog_tab_disabled")&&!1!==this.fire("selectPage",{page:v,currentPage:this._.currentTabId})){for(var u in this._.tabs){var z=this._.tabs[u][0],x=this._.tabs[u][1];
u!=v&&(z.removeClass("cke_dialog_tab_selected"),x.hide());
x.setAttribute("aria-hidden",u!=v)
}var w=this._.tabs[v];
w[0].addClass("cke_dialog_tab_selected");
CKEDITOR.env.ie6Compat||CKEDITOR.env.ie7Compat?(r(w[1]),w[1].show(),setTimeout(function(){r(w[1],1)
},0)):w[1].show();
this._.currentTabId=v;
this._.currentTabIndex=CKEDITOR.tools.indexOf(this._.tabIdList,v)
}},updateStyle:function(){this.parts.dialog[(1===this._.pageCount?"add":"remove")+"Class"]("cke_single_page")
},hidePage:function(v){var u=this._.tabs[v]&&this._.tabs[v][0];
u&&1!=this._.pageCount&&u.isVisible()&&(v==this._.currentTabId&&this.selectPage(y.call(this)),u.hide(),this._.pageCount--,this.updateStyle())
},showPage:function(u){if(u=this._.tabs[u]&&this._.tabs[u][0]){u.show(),this._.pageCount++,this.updateStyle()
}},getElement:function(){return this._.element
},getName:function(){return this._.name
},getContentElement:function(v,u){var w=this._.contents[v];
return w&&w[u]
},getValueOf:function(v,u){return this.getContentElement(v,u).getValue()
},setValueOf:function(v,u,w){return this.getContentElement(v,u).setValue(w)
},getButton:function(u){return this._.buttons[u]
},click:function(u){return this._.buttons[u].click()
},disableButton:function(u){return this._.buttons[u].disable()
},enableButton:function(u){return this._.buttons[u].enable()
},getPageCount:function(){return this._.pageCount
},getParentEditor:function(){return this._.editor
},getSelectedElement:function(){return this.getParentEditor().getSelection().getSelectedElement()
},addFocusable:function(v,u){if("undefined"==typeof u){u=this._.focusList.length,this._.focusList.push(new p(this,v,u))
}else{this._.focusList.splice(u,0,new p(this,v,u));
for(var w=u+1;
w<this._.focusList.length;
w++){this._.focusList[w].focusIndex++
}}},setState:function(v){if(this.state!=v){this.state=v;
if(v==CKEDITOR.DIALOG_STATE_BUSY){if(!this.parts.spinner){var u=this.getParentEditor().lang.dir,w={attributes:{"class":"cke_dialog_spinner"},styles:{"float":"rtl"==u?"right":"left"}};
w.styles["margin-"+("rtl"==u?"left":"right")]="8px";
this.parts.spinner=CKEDITOR.document.createElement("div",w);
this.parts.spinner.setHtml("\x26#8987;");
this.parts.spinner.appendTo(this.parts.title,1)
}this.parts.spinner.show();
this.getButton("ok").disable()
}else{v==CKEDITOR.DIALOG_STATE_IDLE&&(this.parts.spinner&&this.parts.spinner.hide(),this.getButton("ok").enable())
}this.fire("state",v)
}}};
CKEDITOR.tools.extend(CKEDITOR.dialog,{add:function(v,u){this._.dialogDefinitions[v]&&"function"!=typeof u||(this._.dialogDefinitions[v]=u)
},exists:function(u){return !!this._.dialogDefinitions[u]
},getCurrent:function(){return CKEDITOR.dialog._.currentTop
},isTabEnabled:function(v,u,w){v=v.config.removeDialogTabs;
return !(v&&v.match(new RegExp("(?:^|;)"+u+":"+w+"(?:$|;)","i")))
},okButton:function(){var u=function(v,w){w=w||{};
return CKEDITOR.tools.extend({id:"ok",type:"button",label:v.lang.common.ok,"class":"cke_dialog_ui_button_ok",onClick:function(x){x=x.data.dialog;
!1!==x.fire("ok",{hide:!0}).hide&&x.hide()
}},w,!0)
};
u.type="button";
u.override=function(v){return CKEDITOR.tools.extend(function(w){return u(w,v)
},{type:"button"},!0)
};
return u
}(),cancelButton:function(){var u=function(v,w){w=w||{};
return CKEDITOR.tools.extend({id:"cancel",type:"button",label:v.lang.common.cancel,"class":"cke_dialog_ui_button_cancel",onClick:function(x){x=x.data.dialog;
!1!==x.fire("cancel",{hide:!0}).hide&&x.hide()
}},w,!0)
};
u.type="button";
u.override=function(v){return CKEDITOR.tools.extend(function(w){return u(w,v)
},{type:"button"},!0)
};
return u
}(),addUIElement:function(v,u){this._.uiElementBuilders[v]=u
}});
CKEDITOR.dialog._={uiElementBuilders:{},dialogDefinitions:{},currentTop:null,currentZIndex:null};
CKEDITOR.event.implementOn(CKEDITOR.dialog);
CKEDITOR.event.implementOn(CKEDITOR.dialog.prototype);
var af={resizable:CKEDITOR.DIALOG_RESIZE_BOTH,minWidth:600,minHeight:400,buttons:[CKEDITOR.dialog.okButton,CKEDITOR.dialog.cancelButton]},ac=function(v,u,z){for(var x=0,w;
w=v[x];
x++){if(w.id==u||z&&w[z]&&(w=ac(w[z],u,z))){return w
}}return null
},ab=function(v,u,D,A,z){if(D){for(var x=0,w;
w=v[x];
x++){if(w.id==D){return v.splice(x,0,u),u
}if(A&&w[A]&&(w=ab(w[A],u,D,A,!0))){return w
}}if(z){return null
}}v.push(u);
return u
},H=function(v,u,z){for(var x=0,w;
w=v[x];
x++){if(w.id==u){return v.splice(x,1)
}if(z&&w[z]&&(w=H(w[z],u,z))){return w
}}return null
},l=function(v,u){this.dialog=v;
for(var z=u.contents,x=0,w;
w=z[x];
x++){z[x]=w&&new o(v,w)
}CKEDITOR.tools.extend(this,u)
};
l.prototype={getContents:function(u){return ac(this.contents,u)
},getButton:function(u){return ac(this.buttons,u)
},addContents:function(v,u){return ab(this.contents,v,u)
},addButton:function(v,u){return ab(this.buttons,v,u)
},removeContents:function(u){H(this.contents,u)
},removeButton:function(u){H(this.buttons,u)
}};
o.prototype={get:function(u){return ac(this.elements,u,"children")
},add:function(v,u){return ab(this.elements,v,u,"children")
},remove:function(u){H(this.elements,u,"children")
}};
var s,q={},G,B={},j=function(v){var u=v.data.$.ctrlKey||v.data.$.metaKey,z=v.data.$.altKey,x=v.data.$.shiftKey,w=String.fromCharCode(v.data.$.keyCode);
(u=B[(u?"CTRL+":"")+(z?"ALT+":"")+(x?"SHIFT+":"")+w])&&u.length&&(u=u[u.length-1],u.keydown&&u.keydown.call(u.uiElement,u.dialog,u.key),v.data.preventDefault())
},i=function(v){var u=v.data.$.ctrlKey||v.data.$.metaKey,z=v.data.$.altKey,x=v.data.$.shiftKey,w=String.fromCharCode(v.data.$.keyCode);
(u=B[(u?"CTRL+":"")+(z?"ALT+":"")+(x?"SHIFT+":"")+w])&&u.length&&(u=u[u.length-1],u.keyup&&(u.keyup.call(u.uiElement,u.dialog,u.key),v.data.preventDefault()))
},h=function(v,u,z,x,w){(B[z]||(B[z]=[])).push({uiElement:v,dialog:u,key:z,keyup:w||v.accessKeyUp,keydown:x||v.accessKeyDown})
},k=function(v){for(var u in B){for(var x=B[u],w=x.length-1;
0<=w;
w--){x[w].dialog!=v&&x[w].uiElement!=v||x.splice(w,1)
}0===x.length&&delete B[u]
}},ag=function(v,u){v._.accessKeyMap[u]&&v.selectPage(v._.accessKeyMap[u])
},ae=function(){};
(function(){CKEDITOR.ui.dialog={uiElement:function(N,M,L,K,J,I,F){if(!(4>arguments.length)){var E=(K.call?K(M):K)||"div",w=["\x3c",E," "],z=(J&&J.call?J(M):J)||{},D=(I&&I.call?I(M):I)||{},v=(F&&F.call?F.call(this,N,M):F)||"",x=this.domId=D.id||CKEDITOR.tools.getNextId()+"_uiElement";
M.requiredContent&&!N.getParentEditor().filter.check(M.requiredContent)&&(z.display="none",this.notAllowed=!0);
D.id=x;
var u={};
M.type&&(u["cke_dialog_ui_"+M.type]=1);
M.className&&(u[M.className]=1);
M.disabled&&(u.cke_disabled=1);
for(var A=D["class"]&&D["class"].split?D["class"].split(" "):[],x=0;
x<A.length;
x++){A[x]&&(u[A[x]]=1)
}A=[];
for(x in u){A.push(x)
}D["class"]=A.join(" ");
M.title&&(D.title=M.title);
u=(M.style||"").split(";");
M.align&&(A=M.align,z["margin-left"]="left"==A?0:"auto",z["margin-right"]="right"==A?0:"auto");
for(x in z){u.push(x+":"+z[x])
}M.hidden&&u.push("display:none");
for(x=u.length-1;
0<=x;
x--){""===u[x]&&u.splice(x,1)
}0<u.length&&(D.style=(D.style?D.style+"; ":"")+u.join("; "));
for(x in D){w.push(x+'\x3d"'+CKEDITOR.tools.htmlEncode(D[x])+'" ')
}w.push("\x3e",v,"\x3c/",E,"\x3e");
L.push(w.join(""));
(this._||(this._={})).dialog=N;
"boolean"==typeof M.isChanged&&(this.isChanged=function(){return M.isChanged
});
"function"==typeof M.isChanged&&(this.isChanged=M.isChanged);
"function"==typeof M.setValue&&(this.setValue=CKEDITOR.tools.override(this.setValue,function(P){return function(Q){P.call(this,M.setValue.call(this,Q))
}
}));
"function"==typeof M.getValue&&(this.getValue=CKEDITOR.tools.override(this.getValue,function(P){return function(){return M.getValue.call(this,P.call(this))
}
}));
CKEDITOR.event.implementOn(this);
this.registerEvents(M);
this.accessKeyUp&&this.accessKeyDown&&M.accessKey&&h(this,N,"CTRL+"+M.accessKey);
var O=this;
N.on("load",function(){var P=O.getInputElement();
if(P){var Q=O.type in {checkbox:1,ratio:1}&&CKEDITOR.env.ie&&8>CKEDITOR.env.version?"cke_dialog_ui_focused":"";
P.on("focus",function(){N._.tabBarMode=!1;
N._.hasFocus=!0;
O.fire("focus");
Q&&this.addClass(Q)
});
P.on("blur",function(){O.fire("blur");
Q&&this.removeClass(Q)
})
}});
CKEDITOR.tools.extend(this,M);
this.keyboardFocusable&&(this.tabIndex=M.tabIndex||0,this.focusIndex=N._.focusList.push(this)-1,this.on("focus",function(){N._.currentFocusIndex=O.focusIndex
}))
}},hbox:function(I,F,E,D,A){if(!(4>arguments.length)){this._||(this._={});
var z=this._.children=F,x=A&&A.widths||null,w=A&&A.height||null,u,v={role:"presentation"};
A&&A.align&&(v.align=A.align);
CKEDITOR.ui.dialog.uiElement.call(this,I,A||{type:"hbox"},D,"table",{},v,function(){var K=['\x3ctbody\x3e\x3ctr class\x3d"cke_dialog_ui_hbox"\x3e'];
for(u=0;
u<E.length;
u++){var J="cke_dialog_ui_hbox_child",L=[];
0===u&&(J="cke_dialog_ui_hbox_first");
u==E.length-1&&(J="cke_dialog_ui_hbox_last");
K.push('\x3ctd class\x3d"',J,'" role\x3d"presentation" ');
x?x[u]&&L.push("width:"+C(x[u])):L.push("width:"+Math.floor(100/E.length)+"%");
w&&L.push("height:"+C(w));
A&&void 0!==A.padding&&L.push("padding:"+C(A.padding));
CKEDITOR.env.ie&&CKEDITOR.env.quirks&&z[u].align&&L.push("text-align:"+z[u].align);
0<L.length&&K.push('style\x3d"'+L.join("; ")+'" ');
K.push("\x3e",E[u],"\x3c/td\x3e")
}K.push("\x3c/tr\x3e\x3c/tbody\x3e");
return K.join("")
})
}},vbox:function(v,u,E,D,A){if(!(3>arguments.length)){this._||(this._={});
var z=this._.children=u,x=A&&A.width||null,w=A&&A.heights||null;
CKEDITOR.ui.dialog.uiElement.call(this,v,A||{type:"vbox"},D,"div",null,{role:"presentation"},function(){var F=['\x3ctable role\x3d"presentation" cellspacing\x3d"0" border\x3d"0" '];
F.push('style\x3d"');
A&&A.expand&&F.push("height:100%;");
F.push("width:"+C(x||"100%"),";");
CKEDITOR.env.webkit&&F.push("float:none;");
F.push('"');
F.push('align\x3d"',CKEDITOR.tools.htmlEncode(A&&A.align||("ltr"==v.getParentEditor().lang.dir?"left":"right")),'" ');
F.push("\x3e\x3ctbody\x3e");
for(var J=0;
J<E.length;
J++){var I=[];
F.push('\x3ctr\x3e\x3ctd role\x3d"presentation" ');
x&&I.push("width:"+C(x||"100%"));
w?I.push("height:"+C(w[J])):A&&A.expand&&I.push("height:"+Math.floor(100/E.length)+"%");
A&&void 0!==A.padding&&I.push("padding:"+C(A.padding));
CKEDITOR.env.ie&&CKEDITOR.env.quirks&&z[J].align&&I.push("text-align:"+z[J].align);
0<I.length&&F.push('style\x3d"',I.join("; "),'" ');
F.push(' class\x3d"cke_dialog_ui_vbox_child"\x3e',E[J],"\x3c/td\x3e\x3c/tr\x3e")
}F.push("\x3c/tbody\x3e\x3c/table\x3e");
return F.join("")
})
}}}
})();
CKEDITOR.ui.dialog.uiElement.prototype={getElement:function(){return CKEDITOR.document.getById(this.domId)
},getInputElement:function(){return this.getElement()
},getDialog:function(){return this._.dialog
},setValue:function(v,u){this.getInputElement().setValue(v);
!u&&this.fire("change",{value:v});
return this
},getValue:function(){return this.getInputElement().getValue()
},isChanged:function(){return !1
},selectParentTab:function(){for(var u=this.getInputElement();
(u=u.getParent())&&-1==u.$.className.search("cke_dialog_page_contents");
){}if(!u){return this
}u=u.getAttribute("name");
this._.dialog._.currentTabId!=u&&this._.dialog.selectPage(u);
return this
},focus:function(){this.selectParentTab().getInputElement().focus();
return this
},registerEvents:function(v){var u=/^on([A-Z]\w+)/,z,x=function(D,A,F,E){A.on("load",function(){D.getInputElement().on(F,E,D)
})
},w;
for(w in v){if(z=w.match(u)){this.eventProcessors[w]?this.eventProcessors[w].call(this,this._.dialog,v[w]):x(this,this._.dialog,z[1].toLowerCase(),v[w])
}}return this
},eventProcessors:{onLoad:function(v,u){v.on("load",u,this)
},onShow:function(v,u){v.on("show",u,this)
},onHide:function(v,u){v.on("hide",u,this)
}},accessKeyDown:function(){this.focus()
},accessKeyUp:function(){},disable:function(){var u=this.getElement();
this.getInputElement().setAttribute("disabled","true");
u.addClass("cke_disabled")
},enable:function(){var u=this.getElement();
this.getInputElement().removeAttribute("disabled");
u.removeClass("cke_disabled")
},isEnabled:function(){return !this.getElement().hasClass("cke_disabled")
},isVisible:function(){return this.getInputElement().isVisible()
},isFocusable:function(){return this.isEnabled()&&this.isVisible()?!0:!1
}};
CKEDITOR.ui.dialog.hbox.prototype=CKEDITOR.tools.extend(new CKEDITOR.ui.dialog.uiElement,{getChild:function(u){if(1>arguments.length){return this._.children.concat()
}u.splice||(u=[u]);
return 2>u.length?this._.children[u[0]]:this._.children[u[0]]&&this._.children[u[0]].getChild?this._.children[u[0]].getChild(u.slice(1,u.length)):null
}},!0);
CKEDITOR.ui.dialog.vbox.prototype=new CKEDITOR.ui.dialog.hbox;
(function(){var u={build:function(I,F,E){for(var D=F.children,A,z=[],x=[],v=0;
v<D.length&&(A=D[v]);
v++){var w=[];
z.push(w);
x.push(CKEDITOR.dialog._.uiElementBuilders[A.type].build(I,A,w))
}return new CKEDITOR.ui.dialog[F.type](I,x,z,E,F)
}};
CKEDITOR.dialog.addUIElement("hbox",u);
CKEDITOR.dialog.addUIElement("vbox",u)
})();
CKEDITOR.dialogCommand=function(v,u){this.dialogName=v;
CKEDITOR.tools.extend(this,u,!0)
};
CKEDITOR.dialogCommand.prototype={exec:function(u){u.openDialog(this.dialogName)
},canUndo:!1,editorFocus:1};
(function(){var v=/^([a]|[^a])+$/,u=/^\d*$/,A=/^\d*(?:\.\d+)?$/,z=/^(((\d*(\.\d+))|(\d*))(px|\%)?)?$/,x=/^(((\d*(\.\d+))|(\d*))(px|em|ex|in|cm|mm|pt|pc|\%)?)?$/i,w=/^(\s*[\w-]+\s*:\s*[^:;]+(?:;|$))*$/;
CKEDITOR.VALIDATE_OR=1;
CKEDITOR.VALIDATE_AND=2;
CKEDITOR.dialog.validate={functions:function(){var D=arguments;
return function(){var E=this&&this.getValue?this.getValue():D[0],L,J=CKEDITOR.VALIDATE_AND,I=[],F;
for(F=0;
F<D.length;
F++){if("function"==typeof D[F]){I.push(D[F])
}else{break
}}F<D.length&&"string"==typeof D[F]&&(L=D[F],F++);
F<D.length&&"number"==typeof D[F]&&(J=D[F]);
var K=J==CKEDITOR.VALIDATE_AND?!0:!1;
for(F=0;
F<I.length;
F++){K=J==CKEDITOR.VALIDATE_AND?K&&I[F](E):K||I[F](E)
}return K?!0:L
}
},regex:function(E,D){return function(F){F=this&&this.getValue?this.getValue():F;
return E.test(F)?!0:D
}
},notEmpty:function(D){return this.regex(v,D)
},integer:function(D){return this.regex(u,D)
},number:function(D){return this.regex(A,D)
},cssLength:function(D){return this.functions(function(E){return x.test(CKEDITOR.tools.trim(E))
},D)
},htmlLength:function(D){return this.functions(function(E){return z.test(CKEDITOR.tools.trim(E))
},D)
},inlineStyle:function(D){return this.functions(function(E){return w.test(CKEDITOR.tools.trim(E))
},D)
},equals:function(E,D){return this.functions(function(F){return F==E
},D)
},notEqual:function(E,D){return this.functions(function(F){return F!=E
},D)
}};
CKEDITOR.on("instanceDestroyed",function(E){if(CKEDITOR.tools.isEmpty(CKEDITOR.instances)){for(var D;
D=CKEDITOR.dialog._.currentTop;
){D.hide()
}for(var I in q){q[I].remove()
}q={}
}E=E.editor._.storedDialogs;
for(var F in E){E[F].destroy()
}})
})();
CKEDITOR.tools.extend(CKEDITOR.editor.prototype,{openDialog:function(v,u){var x=null,w=CKEDITOR.dialog._.dialogDefinitions[v];
null===CKEDITOR.dialog._.currentTop&&n(this);
if("function"==typeof w){x=this._.storedDialogs||(this._.storedDialogs={}),x=x[v]||(x[v]=new CKEDITOR.dialog(this,v)),u&&u.call(x,x),x.show()
}else{if("failed"==w){throw m(this),Error('[CKEDITOR.dialog.openDialog] Dialog "'+v+'" failed when loading definition.')
}"string"==typeof w&&CKEDITOR.scriptLoader.load(CKEDITOR.getUrl(w),function(){"function"!=typeof CKEDITOR.dialog._.dialogDefinitions[v]&&(CKEDITOR.dialog._.dialogDefinitions[v]="failed");
this.openDialog(v,u)
},this,0,1)
}CKEDITOR.skin.loadPart("dialog");
return x
}})
})();
CKEDITOR.plugins.add("dialog",{requires:"dialogui",init:function(a){a.on("doubleclick",function(b){b.data.dialog&&a.openDialog(b.data.dialog)
},null,null,999)
}});
CKEDITOR.plugins.add("about",{requires:"dialog",init:function(d){var c=d.addCommand("about",new CKEDITOR.dialogCommand("about"));
c.modes={wysiwyg:1,source:1};
c.canUndo=!1;
c.readOnly=1;
d.ui.addButton&&d.ui.addButton("About",{label:d.lang.about.title,command:"about",toolbar:"about"});
CKEDITOR.dialog.add("about",this.path+"dialogs/about.js")
}});
(function(){CKEDITOR.plugins.add("a11yhelp",{requires:"dialog",availableLangs:{af:1,ar:1,bg:1,ca:1,cs:1,cy:1,da:1,de:1,el:1,en:1,"en-gb":1,eo:1,es:1,et:1,fa:1,fi:1,fo:1,fr:1,"fr-ca":1,gl:1,gu:1,he:1,hi:1,hr:1,hu:1,id:1,it:1,ja:1,km:1,ko:1,ku:1,lt:1,lv:1,mk:1,mn:1,nb:1,nl:1,no:1,pl:1,pt:1,"pt-br":1,ro:1,ru:1,si:1,sk:1,sl:1,sq:1,sr:1,"sr-latn":1,sv:1,th:1,tr:1,tt:1,ug:1,uk:1,vi:1,zh:1,"zh-cn":1},init:function(a){var d=this;
a.addCommand("a11yHelp",{exec:function(){var b=a.langCode,b=d.availableLangs[b]?b:d.availableLangs[b.replace(/-.*/,"")]?b.replace(/-.*/,""):"en";
CKEDITOR.scriptLoader.load(CKEDITOR.getUrl(d.path+"dialogs/lang/"+b+".js"),function(){a.lang.a11yhelp=d.langEntries[b];
a.openDialog("a11yHelp")
})
},modes:{wysiwyg:1,source:1},readOnly:1,canUndo:!1});
a.setKeystroke(CKEDITOR.ALT+48,"a11yHelp");
CKEDITOR.dialog.add("a11yHelp",this.path+"dialogs/a11yhelp.js");
a.on("ariaEditorHelpLabel",function(b){b.data.label=a.lang.common.editorHelp
})
}})
})();
(function(){function c(e){var d=this.att;
e=e&&e.hasAttribute(d)&&e.getAttribute(d)||"";
void 0!==e&&this.setValue(e)
}function b(){for(var f,e=0;
e<arguments.length;
e++){if(arguments[e] instanceof CKEDITOR.dom.element){f=arguments[e];
break
}}if(f){var e=this.att,d=this.getValue();
d?f.setAttribute(e,d):f.removeAttribute(e,d)
}}var a={id:1,dir:1,classes:1,styles:1};
CKEDITOR.plugins.add("dialogadvtab",{requires:"dialog",allowedContent:function(f){f||(f=a);
var e=[];
f.id&&e.push("id");
f.dir&&e.push("dir");
var d="";
e.length&&(d+="["+e.join(",")+"]");
f.classes&&(d+="(*)");
f.styles&&(d+="{*}");
return d
},createAdvancedTab:function(l,g,f){g||(g=a);
var k=l.lang.common,i={id:"advanced",label:k.advancedTab,title:k.advancedTab,elements:[{type:"vbox",padding:1,children:[]}]},j=[];
if(g.id||g.dir){g.id&&j.push({id:"advId",att:"id",type:"text",requiredContent:f?f+"[id]":null,label:k.id,setup:c,commit:b}),g.dir&&j.push({id:"advLangDir",att:"dir",type:"select",requiredContent:f?f+"[dir]":null,label:k.langDir,"default":"",style:"width:100%",items:[[k.notSet,""],[k.langDirLTR,"ltr"],[k.langDirRTL,"rtl"]],setup:c,commit:b}),i.elements[0].children.push({type:"hbox",widths:["50%","50%"],children:[].concat(j)})
}if(g.styles||g.classes){j=[],g.styles&&j.push({id:"advStyles",att:"style",type:"text",requiredContent:f?f+"{cke-xyz}":null,label:k.styles,"default":"",validate:CKEDITOR.dialog.validate.inlineStyle(k.invalidInlineStyle),onChange:function(){},getStyle:function(e,h){var d=this.getValue().match(new RegExp("(?:^|;)\\s*"+e+"\\s*:\\s*([^;]*)","i"));
return d?d[1]:h
},updateStyle:function(m,h){var o=this.getValue(),n=l.document.createElement("span");
n.setAttribute("style",o);
n.setStyle(m,h);
o=CKEDITOR.tools.normalizeCssText(n.getAttribute("style"));
this.setValue(o,1)
},setup:c,commit:b}),g.classes&&j.push({type:"hbox",widths:["45%","55%"],children:[{id:"advCSSClasses",att:"class",type:"text",requiredContent:f?f+"(cke-xyz)":null,label:k.cssClasses,"default":"",setup:c,commit:b}]}),i.elements[0].children.push({type:"hbox",widths:["50%","50%"],children:[].concat(j)})
}return i
}})
})();
CKEDITOR.plugins.add("basicstyles",{init:function(l){var j=0,k=function(h,n,c,e){if(e){e=new CKEDITOR.style(e);
var m=i[c];
m.unshift(e);
l.attachStyleStateChange(e,function(b){!l.readOnly&&l.getCommand(c).setState(b)
});
l.addCommand(c,new CKEDITOR.styleCommand(e,{contentForms:m}));
l.ui.addButton&&l.ui.addButton(h,{label:n,command:c,toolbar:"basicstyles,"+(j+=10)})
}},i={bold:["strong","b",["span",function(b){b=b.styles["font-weight"];
return"bold"==b||700<=+b
}]],italic:["em","i",["span",function(b){return"italic"==b.styles["font-style"]
}]],underline:["u",["span",function(b){return"underline"==b.styles["text-decoration"]
}]],strike:["s","strike",["span",function(b){return"line-through"==b.styles["text-decoration"]
}]],subscript:["sub"],superscript:["sup"]},f=l.config,g=l.lang.basicstyles;
k("Bold",g.bold,"bold",f.coreStyles_bold);
k("Italic",g.italic,"italic",f.coreStyles_italic);
k("Underline",g.underline,"underline",f.coreStyles_underline);
k("Strike",g.strike,"strike",f.coreStyles_strike);
k("Subscript",g.subscript,"subscript",f.coreStyles_subscript);
k("Superscript",g.superscript,"superscript",f.coreStyles_superscript);
l.setKeystroke([[CKEDITOR.CTRL+66,"bold"],[CKEDITOR.CTRL+73,"italic"],[CKEDITOR.CTRL+85,"underline"]])
}});
CKEDITOR.config.coreStyles_bold={element:"strong",overrides:"b"};
CKEDITOR.config.coreStyles_italic={element:"em",overrides:"i"};
CKEDITOR.config.coreStyles_underline={element:"u"};
CKEDITOR.config.coreStyles_strike={element:"s",overrides:"strike"};
CKEDITOR.config.coreStyles_subscript={element:"sub"};
CKEDITOR.config.coreStyles_superscript={element:"sup"};
(function(){function b(l,m,n,k){if(!l.isReadOnly()&&!l.equals(n.editable())){CKEDITOR.dom.element.setMarker(k,l,"bidi_processed",1);
k=l;
for(var o=n.editable();
(k=k.getParent())&&!k.equals(o);
){if(k.getCustomData("bidi_processed")){l.removeStyle("direction");
l.removeAttribute("dir");
return
}}k="useComputedState" in n.config?n.config.useComputedState:1;
(k?l.getComputedStyle("direction"):l.getStyle("direction")||l.hasAttribute("dir"))!=m&&(l.removeStyle("direction"),k?(l.removeAttribute("dir"),m!=l.getComputedStyle("direction")&&l.setAttribute("dir",m)):l.setAttribute("dir",m),n.forceNextSelectionCheck())
}}function h(l,m,n){var k=l.getCommonAncestor(!1,!0);
l=l.clone();
l.enlarge(n==CKEDITOR.ENTER_BR?CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS:CKEDITOR.ENLARGE_BLOCK_CONTENTS);
if(l.checkBoundaryOfElement(k,CKEDITOR.START)&&l.checkBoundaryOfElement(k,CKEDITOR.END)){for(var o;
k&&k.type==CKEDITOR.NODE_ELEMENT&&(o=k.getParent())&&1==o.getChildCount()&&!(k.getName() in m);
){k=o
}return k.type==CKEDITOR.NODE_ELEMENT&&k.getName() in m&&k
}}function c(k){return{context:"p",allowedContent:{"h1 h2 h3 h4 h5 h6 table ul ol blockquote div tr p div li td":{propertiesOnly:!0,attributes:"dir"}},requiredContent:"p[dir]",refresh:function(m,p){var l=m.config.useComputedState,q,l=void 0===l||l;
if(!l){q=p.lastElement;
for(var n=m.editable();
q&&!(q.getName() in i||q.equals(n));
){var o=q.getParent();
if(!o){break
}q=o
}}q=q||p.block||p.blockLimit;
q.equals(m.editable())&&(n=m.getSelection().getRanges()[0].getEnclosedNode())&&n.type==CKEDITOR.NODE_ELEMENT&&(q=n);
q&&(l=l?q.getComputedStyle("direction"):q.getStyle("direction")||q.getAttribute("dir"),m.getCommand("bidirtl").setState("rtl"==l?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF),m.getCommand("bidiltr").setState("ltr"==l?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF));
l=(p.block||p.blockLimit||m.editable()).getDirection(1);
l!=(m._.selDir||m.lang.dir)&&(m._.selDir=l,m.fire("contentDirChanged",l))
},exec:function(w){var y=w.getSelection(),A=w.config.enterMode,z=y.getRanges();
if(z&&z.length){for(var u={},x=y.createBookmarks(),z=z.createIterator(),v,s=0;
v=z.getNextRange(1);
){var t=v.getEnclosedNode();
t&&(!t||t.type==CKEDITOR.NODE_ELEMENT&&t.getName() in a)||(t=h(v,j,A));
t&&b(t,k,w,u);
var r=new CKEDITOR.dom.walker(v),q=x[s].startNode,o=x[s++].endNode;
r.evaluator=function(l){var n=A==CKEDITOR.ENTER_P?"p":"div",m;
if(m=(l?l.type==CKEDITOR.NODE_ELEMENT:!1)&&l.getName() in j){if(n=l.is(n)){n=(n=l.getParent())?n.type==CKEDITOR.NODE_ELEMENT:!1
}m=!(n&&l.getParent().is("blockquote"))
}return !!(m&&l.getPosition(q)&CKEDITOR.POSITION_FOLLOWING&&(l.getPosition(o)&CKEDITOR.POSITION_PRECEDING+CKEDITOR.POSITION_CONTAINS)==CKEDITOR.POSITION_PRECEDING)
};
for(;
t=r.next();
){b(t,k,w,u)
}v=v.createIterator();
for(v.enlargeBr=A!=CKEDITOR.ENTER_BR;
t=v.getNextParagraph(A==CKEDITOR.ENTER_P?"p":"div");
){b(t,k,w,u)
}}CKEDITOR.dom.element.clearAllMarkers(u);
w.forceNextSelectionCheck();
y.selectBookmarks(x);
w.focus()
}}}
}function g(l){var m=l==f.setAttribute,n=l==f.removeAttribute,k=/\bdirection\s*:\s*(.*?)\s*(:?$|;)/;
return function(r,o){if(!this.isReadOnly()){var q;
if(q=r==(m||n?"dir":"direction")||"style"==r&&(n||k.test(o))){l:{q=this;
for(var p=q.getDocument().getBody().getParent();
q;
){if(q.equals(p)){q=!1;
break l
}q=q.getParent()
}q=!0
}q=!q
}if(q&&(q=this.getDirection(1),p=l.apply(this,arguments),q!=this.getDirection(1))){return this.getDocument().fire("dirChanged",this),p
}}return l.apply(this,arguments)
}
}var j={table:1,ul:1,ol:1,blockquote:1,div:1},a={},i={};
CKEDITOR.tools.extend(a,j,{tr:1,p:1,div:1,li:1});
CKEDITOR.tools.extend(i,a,{td:1});
CKEDITOR.plugins.add("bidi",{init:function(k){function l(n,r,q,p,o){k.addCommand(q,new CKEDITOR.command(k,p));
k.ui.addButton&&k.ui.addButton(n,{label:r,command:q,toolbar:"bidi,"+o})
}if(!k.blockless){var m=k.lang.bidi;
l("BidiLtr",m.ltr,"bidiltr",c("ltr"),10);
l("BidiRtl",m.rtl,"bidirtl",c("rtl"),20);
k.on("contentDom",function(){k.document.on("dirChanged",function(n){k.fire("dirChanged",{node:n.data,dir:n.data.getDirection(1)})
})
});
k.on("contentDirChanged",function(n){n=(k.lang.dir!=n.data?"add":"remove")+"Class";
var o=k.ui.space(k.config.toolbarLocation);
if(o){o[n]("cke_mixed_dir_content")
}})
}}});
for(var f=CKEDITOR.dom.element.prototype,d=["setStyle","removeStyle","setAttribute","removeAttribute"],e=0;
e<d.length;
e++){f[d[e]]=CKEDITOR.tools.override(f[d[e]],g)
}})();
(function(){var a={exec:function(p){var v=p.getCommand("blockquote").state,n=p.getSelection(),t=n&&n.getRanges()[0];
if(t){var o=n.createBookmarks();
if(CKEDITOR.env.ie){var r=o[0].startNode,u=o[0].endNode,s;
if(r&&"blockquote"==r.getParent().getName()){for(s=r;
s=s.getNext();
){if(s.type==CKEDITOR.NODE_ELEMENT&&s.isBlockBoundary()){r.move(s,!0);
break
}}}if(u&&"blockquote"==u.getParent().getName()){for(s=u;
s=s.getPrevious();
){if(s.type==CKEDITOR.NODE_ELEMENT&&s.isBlockBoundary()){u.move(s);
break
}}}}var q=t.createIterator();
q.enlargeBr=p.config.enterMode!=CKEDITOR.ENTER_BR;
if(v==CKEDITOR.TRISTATE_OFF){for(r=[];
v=q.getNextParagraph();
){r.push(v)
}1>r.length&&(v=p.document.createElement(p.config.enterMode==CKEDITOR.ENTER_P?"p":"div"),u=o.shift(),t.insertNode(v),v.append(new CKEDITOR.dom.text("",p.document)),t.moveToBookmark(u),t.selectNodeContents(v),t.collapse(!0),u=t.createBookmark(),r.push(v),o.unshift(u));
s=r[0].getParent();
t=[];
for(u=0;
u<r.length;
u++){v=r[u],s=s.getCommonAncestor(v.getParent())
}for(v={table:1,tbody:1,tr:1,ol:1,ul:1};
v[s.getName()];
){s=s.getParent()
}for(u=null;
0<r.length;
){for(v=r.shift();
!v.getParent().equals(s);
){v=v.getParent()
}v.equals(u)||t.push(v);
u=v
}for(;
0<t.length;
){if(v=t.shift(),"blockquote"==v.getName()){for(u=new CKEDITOR.dom.documentFragment(p.document);
v.getFirst();
){u.append(v.getFirst().remove()),r.push(u.getLast())
}u.replace(v)
}else{r.push(v)
}}t=p.document.createElement("blockquote");
for(t.insertBefore(r[0]);
0<r.length;
){v=r.shift(),t.append(v)
}}else{if(v==CKEDITOR.TRISTATE_ON){u=[];
for(s={};
v=q.getNextParagraph();
){for(r=t=null;
v.getParent();
){if("blockquote"==v.getParent().getName()){t=v.getParent();
r=v;
break
}v=v.getParent()
}t&&r&&!r.getCustomData("blockquote_moveout")&&(u.push(r),CKEDITOR.dom.element.setMarker(s,r,"blockquote_moveout",!0))
}CKEDITOR.dom.element.clearAllMarkers(s);
v=[];
r=[];
for(s={};
0<u.length;
){q=u.shift(),t=q.getParent(),q.getPrevious()?q.getNext()?(q.breakParent(q.getParent()),r.push(q.getNext())):q.remove().insertAfter(t):q.remove().insertBefore(t),t.getCustomData("blockquote_processed")||(r.push(t),CKEDITOR.dom.element.setMarker(s,t,"blockquote_processed",!0)),v.push(q)
}CKEDITOR.dom.element.clearAllMarkers(s);
for(u=r.length-1;
0<=u;
u--){t=r[u];
v:{s=t;
for(var q=0,i=s.getChildCount(),j=void 0;
q<i&&(j=s.getChild(q));
q++){if(j.type==CKEDITOR.NODE_ELEMENT&&j.isBlockBoundary()){s=!1;
break v
}}s=!0
}s&&t.remove()
}if(p.config.enterMode==CKEDITOR.ENTER_BR){for(t=!0;
v.length;
){if(q=v.shift(),"div"==q.getName()){u=new CKEDITOR.dom.documentFragment(p.document);
!t||!q.getPrevious()||q.getPrevious().type==CKEDITOR.NODE_ELEMENT&&q.getPrevious().isBlockBoundary()||u.append(p.document.createElement("br"));
for(t=q.getNext()&&!(q.getNext().type==CKEDITOR.NODE_ELEMENT&&q.getNext().isBlockBoundary());
q.getFirst();
){q.getFirst().remove().appendTo(u)
}t&&u.append(p.document.createElement("br"));
u.replace(q);
t=!1
}}}}}n.selectBookmarks(o);
p.focus()
}},refresh:function(c,b){this.setState(c.elementPath(b.block||b.blockLimit).contains("blockquote",1)?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF)
},context:"blockquote",allowedContent:"blockquote",requiredContent:"blockquote"};
CKEDITOR.plugins.add("blockquote",{init:function(b){b.blockless||(b.addCommand("blockquote",a),b.ui.addButton&&b.ui.addButton("Blockquote",{label:b.lang.blockquote.toolbar,command:"blockquote",toolbar:"blocks,10"}))
}})
})();
(function(){function b(k,l,m){l.type||(l.type="auto");
if(m&&!1===k.fire("beforePaste",l)||!l.dataValue&&l.dataTransfer.isEmpty()){return !1
}l.dataValue||(l.dataValue="");
if(CKEDITOR.env.gecko&&"drop"==l.method&&k.toolbox){k.once("afterPaste",function(){k.toolbox.focus()
})
}return k.fire("paste",l)
}function g(K){function L(){var k=K.editable();
if(CKEDITOR.plugins.clipboard.isCustomCopyCutSupported){var l=function(p){B.initPasteDataTransfer(p,K);
p.data.preventDefault()
};
k.on("copy",l);
k.on("cut",l);
k.on("cut",function(){K.extractSelectedHtml()
},null,null,999)
}k.on(B.mainPasteEvent,function(p){"beforepaste"==B.mainPasteEvent&&A||D(p)
});
"beforepaste"==B.mainPasteEvent&&(k.on("paste",function(p){r||(H(),p.data.preventDefault(),D(p),G("paste")||K.openDialog("paste"))
}),k.on("contextmenu",F,null,null,0),k.on("beforepaste",function(p){!p.data||p.data.$.ctrlKey||p.data.$.shiftKey||F()
},null,null,0));
k.on("beforecut",function(){!A&&E(K)
});
var n;
k.attachListener(CKEDITOR.env.ie?k:K.document.getDocumentElement(),"mouseup",function(){n=setTimeout(function(){x()
},0)
});
K.on("destroy",function(){clearTimeout(n)
});
k.on("keyup",x)
}function J(k){return{type:k,canUndo:"cut"==k,startDisabled:!0,exec:function(){"cut"==this.type&&E();
var l;
var n=this.type;
if(CKEDITOR.env.ie){l=G(n)
}else{try{l=K.document.$.execCommand(n,!1,null)
}catch(p){l=!1
}}l||K.showNotification(K.lang.clipboard[this.type+"Error"]);
return l
}}
}function I(){return{canUndo:!1,async:!0,exec:function(k,l){var n=function(q,t){q&&b(k,q,!!t);
k.fire("afterCommandExec",{name:"paste",command:p,returnValue:!!q})
},p=this;
"string"==typeof l?n({dataValue:l,method:"paste",dataTransfer:B.initPasteDataTransfer()},1):k.getClipboardData(n)
}}
}function H(){r=1;
setTimeout(function(){r=0
},100)
}function F(){A=1;
setTimeout(function(){A=0
},10)
}function G(k){var p=K.document,q=p.getBody(),n=!1,l=function(){n=!0
};
q.on(k,l);
7<CKEDITOR.env.version?p.$.execCommand(k):p.$.selection.createRange().execCommand(k);
q.removeListener(k,l);
return n
}function E(){if(CKEDITOR.env.ie&&!CKEDITOR.env.quirks){var k=K.getSelection(),n,p,l;
k.getType()==CKEDITOR.SELECTION_ELEMENT&&(n=k.getSelectedElement())&&(p=k.getRanges()[0],l=K.document.createText(""),l.insertBefore(n),p.setStartBefore(l),p.setEndAfter(n),k.selectRanges([p]),setTimeout(function(){n.getParent()&&(l.remove(),k.selectElement(n))
},0))
}}function C(Q,O){var P=K.document,N=K.editable(),M=function(k){k.cancel()
},n;
if(!P.getById("cke_pastebin")){var z=K.getSelection(),w=z.createBookmarks();
CKEDITOR.env.ie&&z.root.fire("selectionchange");
var p=new CKEDITOR.dom.element(!CKEDITOR.env.webkit&&!N.is("body")||CKEDITOR.env.ie?"div":"body",P);
p.setAttributes({id:"cke_pastebin","data-cke-temp":"1"});
var v=0,P=P.getWindow();
CKEDITOR.env.webkit?(N.append(p),p.addClass("cke_editable"),N.is("body")||(v="static"!=N.getComputedStyle("position")?N:CKEDITOR.dom.element.get(N.$.offsetParent),v=v.getDocumentPosition().y)):N.getAscendant(CKEDITOR.env.ie?"body":"html",1).append(p);
p.setStyles({position:"absolute",top:P.getScrollPosition().y-v+10+"px",width:"1px",height:Math.max(1,P.getViewPaneSize().height-20)+"px",overflow:"hidden",margin:0,padding:0});
CKEDITOR.env.safari&&p.setStyles(CKEDITOR.tools.cssVendorPrefix("user-select","text"));
(v=p.getParent().isReadOnly())?(p.setOpacity(0),p.setAttribute("contenteditable",!0)):p.setStyle("ltr"==K.config.contentsLangDirection?"left":"right","-1000px");
K.on("selectionChange",M,null,null,0);
if(CKEDITOR.env.webkit||CKEDITOR.env.gecko){n=N.once("blur",M,null,null,-100)
}v&&p.focus();
v=new CKEDITOR.dom.range(p);
v.selectNodeContents(p);
var R=v.select();
CKEDITOR.env.ie&&(n=N.once("blur",function(){K.lockSelection(R)
}));
var u=CKEDITOR.document.getWindow().getScrollPosition().y;
setTimeout(function(){CKEDITOR.env.webkit&&(CKEDITOR.document.getBody().$.scrollTop=u);
n&&n.removeListener();
CKEDITOR.env.ie&&N.focus();
z.selectBookmarks(w);
p.remove();
var k;
CKEDITOR.env.webkit&&(k=p.getFirst())&&k.is&&k.hasClass("Apple-style-span")&&(p=k);
K.removeListener("selectionChange",M);
O(p.getHtml())
},0)
}}function m(){if("paste"==B.mainPasteEvent){return K.fire("beforePaste",{type:"auto",method:"paste"}),!1
}K.focus();
H();
var k=K.focusManager;
k.lock();
if(K.editable().fire(B.mainPasteEvent)&&!G("paste")){return k.unlock(),!1
}k.unlock();
return !0
}function y(k){if("wysiwyg"==K.mode){switch(k.data.keyCode){case CKEDITOR.CTRL+86:case CKEDITOR.SHIFT+45:k=K.editable();
H();
"paste"==B.mainPasteEvent&&k.fire("beforepaste");
break;
case CKEDITOR.CTRL+88:case CKEDITOR.SHIFT+46:K.fire("saveSnapshot"),setTimeout(function(){K.fire("saveSnapshot")
},50)
}}}function D(k){var l={type:"auto",method:"paste",dataTransfer:B.initPasteDataTransfer(k)};
l.dataTransfer.cacheData();
var n=!1!==K.fire("beforePaste",l);
n&&B.canClipboardApiBeTrusted(l.dataTransfer,K)?(k.data.preventDefault(),setTimeout(function(){b(K,l)
},0)):C(k,function(p){l.dataValue=p.replace(/<span[^>]+data-cke-bookmark[^<]*?<\/span>/ig,"");
n&&b(K,l)
})
}function x(){if("wysiwyg"==K.mode){var k=s("paste");
K.getCommand("cut").setState(s("cut"));
K.getCommand("copy").setState(s("copy"));
K.getCommand("paste").setState(k);
K.fire("pasteState",k)
}}function s(k){if(o&&k in {paste:1,cut:1}){return CKEDITOR.TRISTATE_DISABLED
}if("paste"==k){return CKEDITOR.TRISTATE_OFF
}k=K.getSelection();
var l=k.getRanges();
return k.getType()==CKEDITOR.SELECTION_NONE||1==l.length&&l[0].collapsed?CKEDITOR.TRISTATE_DISABLED:CKEDITOR.TRISTATE_OFF
}var B=CKEDITOR.plugins.clipboard,A=0,r=0,o=0;
(function(){K.on("key",y);
K.on("contentDom",L);
K.on("selectionChange",function(k){o=k.data.selection.getRanges()[0].checkReadOnly();
x()
});
K.contextMenu&&K.contextMenu.addListener(function(k,l){o=l.getRanges()[0].checkReadOnly();
return{cut:s("cut"),copy:s("copy"),paste:s("paste")}
})
})();
(function(){function k(u,v,t,p,n){var l=K.lang.clipboard[v];
K.addCommand(v,t);
K.ui.addButton&&K.ui.addButton(u,{label:l,command:v,toolbar:"clipboard,"+p});
K.addMenuItems&&K.addMenuItem(v,{label:l,command:v,group:"clipboard",order:n})
}k("Cut","cut",J("cut"),10,1);
k("Copy","copy",J("copy"),20,4);
k("Paste","paste",I(),30,8)
})();
K.getClipboardData=function(l,w){function z(k){k.removeListener();
k.cancel();
w(k.data)
}function v(k){k.removeListener();
k.cancel();
n=!0;
w({type:p,dataValue:k.data,method:"paste"})
}function u(){this.customTitle=l&&l.title
}var t=!1,p="auto",n=!1;
w||(w=l,l=null);
K.on("paste",z,null,null,0);
K.on("beforePaste",function(k){k.removeListener();
t=!0;
p=k.data.type
},null,null,1000);
!1===m()&&(K.removeListener("paste",z),t&&K.fire("pasteDialog",u)?(K.on("pasteDialogCommit",v),K.on("dialogHide",function(k){k.removeListener();
k.data.removeListener("pasteDialogCommit",v);
setTimeout(function(){n||w(null)
},10)
})):w(null))
}
}function f(k){if(CKEDITOR.env.webkit){if(!k.match(/^[^<]*$/g)&&!k.match(/^(<div><br( ?\/)?><\/div>|<div>[^<]*<\/div>)*$/gi)){return"html"
}}else{if(CKEDITOR.env.ie){if(!k.match(/^([^<]|<br( ?\/)?>)*$/gi)&&!k.match(/^(<p>([^<]|<br( ?\/)?>)*<\/p>|(\r\n))*$/gi)){return"html"
}}else{if(CKEDITOR.env.gecko){if(!k.match(/^([^<]|<br( ?\/)?>)*$/gi)){return"html"
}}else{return"html"
}}}return"htmlifiedtext"
}function e(k,l){function m(n){return CKEDITOR.tools.repeat("\x3c/p\x3e\x3cp\x3e",~~(n/2))+(1==n%2?"\x3cbr\x3e":"")
}l=l.replace(/\s+/g," ").replace(/> +</g,"\x3e\x3c").replace(/<br ?\/>/gi,"\x3cbr\x3e");
l=l.replace(/<\/?[A-Z]+>/g,function(n){return n.toLowerCase()
});
if(l.match(/^[^<]$/)){return l
}CKEDITOR.env.webkit&&-1<l.indexOf("\x3cdiv\x3e")&&(l=l.replace(/^(<div>(<br>|)<\/div>)(?!$|(<div>(<br>|)<\/div>))/g,"\x3cbr\x3e").replace(/^(<div>(<br>|)<\/div>){2}(?!$)/g,"\x3cdiv\x3e\x3c/div\x3e"),l.match(/<div>(<br>|)<\/div>/)&&(l="\x3cp\x3e"+l.replace(/(<div>(<br>|)<\/div>)+/g,function(n){return m(n.split("\x3c/div\x3e\x3cdiv\x3e").length+1)
})+"\x3c/p\x3e"),l=l.replace(/<\/div><div>/g,"\x3cbr\x3e"),l=l.replace(/<\/?div>/g,""));
CKEDITOR.env.gecko&&k.enterMode!=CKEDITOR.ENTER_BR&&(CKEDITOR.env.gecko&&(l=l.replace(/^<br><br>$/,"\x3cbr\x3e")),-1<l.indexOf("\x3cbr\x3e\x3cbr\x3e")&&(l="\x3cp\x3e"+l.replace(/(<br>){2,}/g,function(n){return m(n.length/4)
})+"\x3c/p\x3e"));
return c(k,l)
}function a(){function k(){var n={},m;
for(m in CKEDITOR.dtd){"$"!=m.charAt(0)&&"div"!=m&&"span"!=m&&(n[m]=1)
}return n
}var l={};
return{get:function(m){return"plain-text"==m?l.plainText||(l.plainText=new CKEDITOR.filter("br")):"semantic-content"==m?((m=l.semanticContent)||(m=new CKEDITOR.filter,m.allow({$1:{elements:k(),attributes:!0,styles:!1,classes:!1}}),m=l.semanticContent=m),m):m?new CKEDITOR.filter(m):null
}}
}function h(k,l,n){l=CKEDITOR.htmlParser.fragment.fromHtml(l);
var m=new CKEDITOR.htmlParser.basicWriter;
n.applyTo(l,!0,!1,k.activeEnterMode);
l.writeHtml(m);
return m.getHtml()
}function c(k,l){k.enterMode==CKEDITOR.ENTER_BR?l=l.replace(/(<\/p><p>)+/g,function(m){return CKEDITOR.tools.repeat("\x3cbr\x3e",m.length/7*2)
}).replace(/<\/?p>/g,""):k.enterMode==CKEDITOR.ENTER_DIV&&(l=l.replace(/<(\/)?p>/g,"\x3c$1div\x3e"));
return l
}function j(k){k.data.preventDefault();
k.data.$.dataTransfer.dropEffect="none"
}function i(k){var l=CKEDITOR.plugins.clipboard;
k.on("contentDom",function(){function u(p,v,w){v.select();
b(k,{dataTransfer:w,method:"drop"},1);
w.sourceEditor.fire("saveSnapshot");
w.sourceEditor.editable().extractHtmlFromRange(p);
w.sourceEditor.getSelection().selectRanges([p]);
w.sourceEditor.fire("saveSnapshot")
}function t(p,v){p.select();
b(k,{dataTransfer:v,method:"drop"},1);
l.resetDragDataTransfer()
}function s(p,w,x){var v={$:p.data.$,target:p.data.getTarget()};
w&&(v.dragRange=w);
x&&(v.dropRange=x);
!1===k.fire(p.name,v)&&p.data.preventDefault()
}function o(p){p.type!=CKEDITOR.NODE_ELEMENT&&(p=p.getParent());
return p.getChildCount()
}var q=k.editable(),n=CKEDITOR.plugins.clipboard.getDropTarget(k),m=k.ui.space("top"),r=k.ui.space("bottom");
l.preventDefaultDropOnElement(m);
l.preventDefaultDropOnElement(r);
q.attachListener(n,"dragstart",s);
q.attachListener(k,"dragstart",l.resetDragDataTransfer,l,null,1);
q.attachListener(k,"dragstart",function(p){l.initDragDataTransfer(p,k)
},null,null,2);
q.attachListener(k,"dragstart",function(){var p=l.dragRange=k.getSelection().getRanges()[0];
CKEDITOR.env.ie&&10>CKEDITOR.env.version&&(l.dragStartContainerChildCount=p?o(p.startContainer):null,l.dragEndContainerChildCount=p?o(p.endContainer):null)
},null,null,100);
q.attachListener(n,"dragend",s);
q.attachListener(k,"dragend",l.initDragDataTransfer,l,null,1);
q.attachListener(k,"dragend",l.resetDragDataTransfer,l,null,100);
q.attachListener(n,"dragover",function(v){var p=v.data.getTarget();
p&&p.is&&p.is("html")?v.data.preventDefault():CKEDITOR.env.ie&&CKEDITOR.plugins.clipboard.isFileApiSupported&&v.data.$.dataTransfer.types.contains("Files")&&v.data.preventDefault()
});
q.attachListener(n,"drop",function(v){v.data.preventDefault();
var w=v.data.getTarget();
if(!w.isReadOnly()||w.type==CKEDITOR.NODE_ELEMENT&&w.is("html")){var w=l.getRangeAtDropPosition(v,k),p=l.dragRange;
w&&s(v,p,w)
}});
q.attachListener(k,"drop",l.initDragDataTransfer,l,null,1);
q.attachListener(k,"drop",function(x){if(x=x.data){var w=x.dropRange,v=x.dragRange,p=x.dataTransfer;
p.getTransferType(k)==CKEDITOR.DATA_TRANSFER_INTERNAL?setTimeout(function(){l.internalDrop(v,w,p,k)
},0):p.getTransferType(k)==CKEDITOR.DATA_TRANSFER_CROSS_EDITORS?u(v,w,p):t(w,p)
}},null,null,9999)
})
}CKEDITOR.plugins.add("clipboard",{requires:"dialog",init:function(k){var l,m=a();
k.config.forcePasteAsPlainText?l="plain-text":k.config.pasteFilter?l=k.config.pasteFilter:!CKEDITOR.env.webkit||"pasteFilter" in k.config||(l="semantic-content");
k.pasteFilter=m.get(l);
g(k);
i(k);
CKEDITOR.dialog.add("paste",CKEDITOR.getUrl(this.path+"dialogs/paste.js"));
k.on("paste",function(n){n.data.dataTransfer||(n.data.dataTransfer=new CKEDITOR.plugins.clipboard.dataTransfer);
if(!n.data.dataValue){var p=n.data.dataTransfer,o=p.getData("text/html");
if(o){n.data.dataValue=o,n.data.type="html"
}else{if(o=p.getData("text/plain")){n.data.dataValue=k.editable().transformPlainTextToHtml(o),n.data.type="text"
}}}},null,null,1);
k.on("paste",function(p){var n=p.data.dataValue,s=CKEDITOR.dtd.$block;
-1<n.indexOf("Apple-")&&(n=n.replace(/<span class="Apple-converted-space">&nbsp;<\/span>/gi," "),"html"!=p.data.type&&(n=n.replace(/<span class="Apple-tab-span"[^>]*>([^<]*)<\/span>/gi,function(u,t){return t.replace(/\t/g,"\x26nbsp;\x26nbsp; \x26nbsp;")
})),-1<n.indexOf('\x3cbr class\x3d"Apple-interchange-newline"\x3e')&&(p.data.startsWithEOL=1,p.data.preSniffing="html",n=n.replace(/<br class="Apple-interchange-newline">/,"")),n=n.replace(/(<[^>]+) class="Apple-[^"]*"/gi,"$1"));
if(n.match(/^<[^<]+cke_(editable|contents)/i)){var r,q,o=new CKEDITOR.dom.element("div");
for(o.setHtml(n);
1==o.getChildCount()&&(r=o.getFirst())&&r.type==CKEDITOR.NODE_ELEMENT&&(r.hasClass("cke_editable")||r.hasClass("cke_contents"));
){o=q=r
}q&&(n=q.getHtml().replace(/<br>$/i,""))
}CKEDITOR.env.ie?n=n.replace(/^&nbsp;(?: |\r\n)?<(\w+)/g,function(t,u){return u.toLowerCase() in s?(p.data.preSniffing="html","\x3c"+u):t
}):CKEDITOR.env.webkit?n=n.replace(/<\/(\w+)><div><br><\/div>$/,function(t,u){return u in s?(p.data.endsWithEOL=1,"\x3c/"+u+"\x3e"):t
}):CKEDITOR.env.gecko&&(n=n.replace(/(\s)<br>$/,"$1"));
p.data.dataValue=n
},null,null,3);
k.on("paste",function(o){o=o.data;
var s=o.type,q=o.dataValue,r,p=k.config.clipboard_defaultContentType||"html",n=o.dataTransfer.getTransferType(k);
r="html"==s||"html"==o.preSniffing?"html":f(q);
"htmlifiedtext"==r&&(q=e(k.config,q));
"text"==s&&"html"==r?q=h(k,q,m.get("plain-text")):n==CKEDITOR.DATA_TRANSFER_EXTERNAL&&k.pasteFilter&&!o.dontFilter&&(q=h(k,q,k.pasteFilter));
o.startsWithEOL&&(q='\x3cbr data-cke-eol\x3d"1"\x3e'+q);
o.endsWithEOL&&(q+='\x3cbr data-cke-eol\x3d"1"\x3e');
"auto"==s&&(s="html"==r||"html"==p?"html":"text");
o.type=s;
o.dataValue=q;
delete o.preSniffing;
delete o.startsWithEOL;
delete o.endsWithEOL
},null,null,6);
k.on("paste",function(n){n=n.data;
n.dataValue&&(k.insertHtml(n.dataValue,n.type,n.range),setTimeout(function(){k.fire("afterPaste")
},0))
},null,null,1000);
k.on("pasteDialog",function(n){setTimeout(function(){k.openDialog("paste",n.data)
},0)
})
}});
CKEDITOR.plugins.clipboard={isCustomCopyCutSupported:!CKEDITOR.env.ie&&!CKEDITOR.env.iOS,isCustomDataTypesSupported:!CKEDITOR.env.ie,isFileApiSupported:!CKEDITOR.env.ie||9<CKEDITOR.env.version,mainPasteEvent:CKEDITOR.env.ie&&!CKEDITOR.env.edge?"beforepaste":"paste",canClipboardApiBeTrusted:function(k,l){return k.getTransferType(l)!=CKEDITOR.DATA_TRANSFER_EXTERNAL||CKEDITOR.env.chrome&&!k.isEmpty()||CKEDITOR.env.gecko&&(k.getData("text/html")||k.getFilesCount())?!0:!1
},getDropTarget:function(k){var l=k.editable();
return CKEDITOR.env.ie&&9>CKEDITOR.env.version||l.isInline()?l:k.document
},fixSplitNodesAfterDrop:function(k,l,p,o){function n(q,t,s){var r=q;
r.type==CKEDITOR.NODE_TEXT&&(r=q.getParent());
if(r.equals(t)&&s!=t.getChildCount()){return q=l.startContainer.getChild(l.startOffset-1),t=l.startContainer.getChild(l.startOffset),q&&q.type==CKEDITOR.NODE_TEXT&&t&&t.type==CKEDITOR.NODE_TEXT&&(s=q.getLength(),q.setText(q.getText()+t.getText()),t.remove(),l.setStart(q,s),l.collapse(!0)),!0
}}var m=l.startContainer;
"number"==typeof o&&"number"==typeof p&&m.type==CKEDITOR.NODE_ELEMENT&&(n(k.startContainer,m,p)||n(k.endContainer,m,o))
},isDropRangeAffectedByDragRange:function(k,l){var n=l.startContainer,m=l.endOffset;
return k.endContainer.equals(n)&&k.endOffset<=m||k.startContainer.getParent().equals(n)&&k.startContainer.getIndex()<m||k.endContainer.getParent().equals(n)&&k.endContainer.getIndex()<m?!0:!1
},internalDrop:function(k,l,r,q){var p=CKEDITOR.plugins.clipboard,n=q.editable(),o,m;
q.fire("saveSnapshot");
q.fire("lockSnapshot",{dontUpdate:1});
CKEDITOR.env.ie&&10>CKEDITOR.env.version&&this.fixSplitNodesAfterDrop(k,l,p.dragStartContainerChildCount,p.dragEndContainerChildCount);
(m=this.isDropRangeAffectedByDragRange(k,l))||(o=k.createBookmark(!1));
p=l.clone().createBookmark(!1);
m&&(o=k.createBookmark(!1));
k=o.startNode;
l=o.endNode;
m=p.startNode;
l&&k.getPosition(m)&CKEDITOR.POSITION_PRECEDING&&l.getPosition(m)&CKEDITOR.POSITION_FOLLOWING&&m.insertBefore(k);
k=q.createRange();
k.moveToBookmark(o);
n.extractHtmlFromRange(k,1);
l=q.createRange();
l.moveToBookmark(p);
b(q,{dataTransfer:r,method:"drop",range:l},1);
q.fire("unlockSnapshot")
},getRangeAtDropPosition:function(F,G){var E=F.data.$,D=E.clientX,C=E.clientY,A=G.getSelection(!0).getRanges()[0],B=G.createRange();
if(F.data.testRange){return F.data.testRange
}if(document.caretRangeFromPoint){E=G.document.$.caretRangeFromPoint(D,C),B.setStart(CKEDITOR.dom.node(E.startContainer),E.startOffset),B.collapse(!0)
}else{if(E.rangeParent){B.setStart(CKEDITOR.dom.node(E.rangeParent),E.rangeOffset),B.collapse(!0)
}else{if(CKEDITOR.env.ie&&8<CKEDITOR.env.version&&A&&G.editable().hasFocus){return A
}if(document.body.createTextRange){G.focus();
E=G.document.getBody().$.createTextRange();
try{for(var z=!1,x=0;
20>x&&!z;
x++){if(!z){try{E.moveToPoint(D,C-x),z=!0
}catch(s){}}if(!z){try{E.moveToPoint(D,C+x),z=!0
}catch(o){}}}if(z){var y="cke-temp-"+(new Date).getTime();
E.pasteHTML('\x3cspan id\x3d"'+y+'"\x3e\x3c/span\x3e');
var J=G.document.getById(y);
B.moveToPosition(J,CKEDITOR.POSITION_BEFORE_START);
J.remove()
}else{var I=G.document.$.elementFromPoint(D,C),w=new CKEDITOR.dom.element(I),m;
if(w.equals(G.editable())||"html"==w.getName()){return A&&A.startContainer&&!A.startContainer.equals(G.editable())?A:null
}m=w.getClientRect();
D<m.left?B.setStartAt(w,CKEDITOR.POSITION_AFTER_START):B.setStartAt(w,CKEDITOR.POSITION_BEFORE_END);
B.collapse(!0)
}}catch(H){return null
}}else{return null
}}}return B
},initDragDataTransfer:function(k,l){var n=k.data.$?k.data.$.dataTransfer:null,m=new this.dataTransfer(n,l);
n?this.dragData&&m.id==this.dragData.id?m=this.dragData:this.dragData=m:this.dragData?m=this.dragData:this.dragData=m;
k.data.dataTransfer=m
},resetDragDataTransfer:function(){this.dragData=null
},initPasteDataTransfer:function(k,l){if(this.isCustomCopyCutSupported&&k&&k.data&&k.data.$){var m=new this.dataTransfer(k.data.$.clipboardData,l);
this.copyCutData&&m.id==this.copyCutData.id?(m=this.copyCutData,m.$=k.data.$.clipboardData):this.copyCutData=m;
return m
}return new this.dataTransfer(null,l)
},preventDefaultDropOnElement:function(k){k&&k.on("dragover",j)
}};
var d=CKEDITOR.plugins.clipboard.isCustomDataTypesSupported?"cke/id":"Text";
CKEDITOR.plugins.clipboard.dataTransfer=function(k,l){k&&(this.$=k);
this._={metaRegExp:/^<meta.*?>/i,bodyRegExp:/<body(?:[\s\S]*?)>([\s\S]*)<\/body>/i,fragmentRegExp:/\x3c!--(?:Start|End)Fragment--\x3e/g,data:{},files:[],normalizeType:function(n){n=n.toLowerCase();
return"text"==n||"text/plain"==n?"Text":"url"==n?"URL":n
}};
this.id=this.getData(d);
this.id||(this.id="Text"==d?"":"cke-"+CKEDITOR.tools.getUniqueId());
if("Text"!=d){try{this.$.setData(d,this.id)
}catch(m){}}l&&(this.sourceEditor=l,this.setData("text/html",l.getSelectedHtml(1)),"Text"==d||this.getData("text/plain")||this.setData("text/plain",l.getSelection().getSelectedText()))
};
CKEDITOR.DATA_TRANSFER_INTERNAL=1;
CKEDITOR.DATA_TRANSFER_CROSS_EDITORS=2;
CKEDITOR.DATA_TRANSFER_EXTERNAL=3;
CKEDITOR.plugins.clipboard.dataTransfer.prototype={getData:function(k){k=this._.normalizeType(k);
var l=this._.data[k];
if(void 0===l||null===l||""===l){try{l=this.$.getData(k)
}catch(m){}}if(void 0===l||null===l||""===l){l=""
}"text/html"==k?(l=l.replace(this._.metaRegExp,""),(k=this._.bodyRegExp.exec(l))&&k.length&&(l=k[1],l=l.replace(this._.fragmentRegExp,""))):"Text"==k&&CKEDITOR.env.gecko&&this.getFilesCount()&&"file://"==l.substring(0,7)&&(l="");
return l
},setData:function(k,l){k=this._.normalizeType(k);
this._.data[k]=l;
if(CKEDITOR.plugins.clipboard.isCustomDataTypesSupported||"URL"==k||"Text"==k){"Text"==d&&"Text"==k&&(this.id=l);
try{this.$.setData(k,l)
}catch(m){}}},getTransferType:function(k){return this.sourceEditor?this.sourceEditor==k?CKEDITOR.DATA_TRANSFER_INTERNAL:CKEDITOR.DATA_TRANSFER_CROSS_EDITORS:CKEDITOR.DATA_TRANSFER_EXTERNAL
},cacheData:function(){function k(o){o=l._.normalizeType(o);
var p=l.getData(o);
p&&(l._.data[o]=p)
}if(this.$){var l=this,n,m;
if(CKEDITOR.plugins.clipboard.isCustomDataTypesSupported){if(this.$.types){for(n=0;
n<this.$.types.length;
n++){k(this.$.types[n])
}}}else{k("Text"),k("URL")
}m=this._getImageFromClipboard();
if(this.$&&this.$.files||m){this._.files=[];
for(n=0;
n<this.$.files.length;
n++){this._.files.push(this.$.files[n])
}0===this._.files.length&&m&&this._.files.push(m)
}}},getFilesCount:function(){return this._.files.length?this._.files.length:this.$&&this.$.files&&this.$.files.length?this.$.files.length:this._getImageFromClipboard()?1:0
},getFile:function(k){return this._.files.length?this._.files[k]:this.$&&this.$.files&&this.$.files.length?this.$.files[k]:0===k?this._getImageFromClipboard():void 0
},isEmpty:function(){var k={},l;
if(this.getFilesCount()){return !1
}for(l in this._.data){k[l]=1
}if(this.$){if(CKEDITOR.plugins.clipboard.isCustomDataTypesSupported){if(this.$.types){for(var m=0;
m<this.$.types.length;
m++){k[this.$.types[m]]=1
}}}else{k.Text=1,k.URL=1
}}"Text"!=d&&(k[d]=0);
for(l in k){if(k[l]&&""!==this.getData(l)){return !1
}}return !0
},_getImageFromClipboard:function(){var k;
if(this.$&&this.$.items&&this.$.items[0]){try{if((k=this.$.items[0].getAsFile())&&k.type){return k
}}catch(l){}}}}
})();
(function(){var d='\x3ca id\x3d"{id}" class\x3d"cke_button cke_button__{name} cke_button_{state} {cls}"'+(CKEDITOR.env.gecko&&!CKEDITOR.env.hc?"":" href\x3d\"javascript:void('{titleJs}')\"")+' title\x3d"{title}" tabindex\x3d"-1" hidefocus\x3d"true" role\x3d"button" aria-labelledby\x3d"{id}_label" aria-haspopup\x3d"{hasArrow}" aria-disabled\x3d"{ariaDisabled}"';
CKEDITOR.env.gecko&&CKEDITOR.env.mac&&(d+=' onkeypress\x3d"return false;"');
CKEDITOR.env.gecko&&(d+=' onblur\x3d"this.style.cssText \x3d this.style.cssText;"');
var d=d+(' onkeydown\x3d"return CKEDITOR.tools.callFunction({keydownFn},event);" onfocus\x3d"return CKEDITOR.tools.callFunction({focusFn},event);" '+(CKEDITOR.env.ie?'onclick\x3d"return false;" onmouseup':"onclick")+'\x3d"CKEDITOR.tools.callFunction({clickFn},this);return false;"\x3e\x3cspan class\x3d"cke_button_icon cke_button__{iconName}_icon" style\x3d"{style}"'),d=d+'\x3e\x26nbsp;\x3c/span\x3e\x3cspan id\x3d"{id}_label" class\x3d"cke_button_label cke_button__{name}_label" aria-hidden\x3d"false"\x3e{label}\x3c/span\x3e{arrowHtml}\x3c/a\x3e',b=CKEDITOR.addTemplate("buttonArrow",'\x3cspan class\x3d"cke_button_arrow"\x3e'+(CKEDITOR.env.hc?"\x26#9660;":"")+"\x3c/span\x3e"),a=CKEDITOR.addTemplate("button",d);
CKEDITOR.plugins.add("button",{beforeInit:function(c){c.ui.addHandler(CKEDITOR.UI_BUTTON,CKEDITOR.ui.button.handler)
}});
CKEDITOR.UI_BUTTON="button";
CKEDITOR.ui.button=function(c){CKEDITOR.tools.extend(this,c,{title:c.label,click:c.click||function(e){e.execCommand(c.command)
}});
this._={}
};
CKEDITOR.ui.button.handler={create:function(c){return new CKEDITOR.ui.button(c)
}};
CKEDITOR.ui.button.prototype={render:function(B,A){function z(){var c=B.mode;
c&&(c=this.modes[c]?void 0!==s[c]?s[c]:CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,c=B.readOnly&&!this.readOnly?CKEDITOR.TRISTATE_DISABLED:c,this.setState(c),this.refresh&&this.refresh())
}var r=CKEDITOR.env,o=this._.id=CKEDITOR.tools.getNextId(),x="",w=this.command,j;
this._.editor=B;
var y={id:o,button:this,editor:B,focus:function(){CKEDITOR.document.getById(o).focus()
},execute:function(){this.button.click(B)
},attach:function(c){this.button.attach(c)
}},D=CKEDITOR.tools.addFunction(function(c){if(y.onkey){return c=new CKEDITOR.dom.event(c),!1!==y.onkey(y,c.getKeystroke())
}}),C=CKEDITOR.tools.addFunction(function(f){var c;
y.onfocus&&(c=!1!==y.onfocus(y,new CKEDITOR.dom.event(f)));
return c
}),i=0;
y.clickFn=j=CKEDITOR.tools.addFunction(function(){i&&(B.unlockSelection(1),i=0);
y.execute();
r.iOS&&B.focus()
});
if(this.modes){var s={};
B.on("beforeModeUnload",function(){B.mode&&this._.state!=CKEDITOR.TRISTATE_DISABLED&&(s[B.mode]=this._.state)
},this);
B.on("activeFilterChange",z,this);
B.on("mode",z,this);
!this.readOnly&&B.on("readOnly",z,this)
}else{w&&(w=B.getCommand(w))&&(w.on("state",function(){this.setState(w.state)
},this),x+=w.state==CKEDITOR.TRISTATE_ON?"on":w.state==CKEDITOR.TRISTATE_DISABLED?"disabled":"off")
}if(this.directional){B.on("contentDirChanged",function(f){var h=CKEDITOR.document.getById(this._.id),g=h.getFirst();
f=f.data;
f!=B.lang.dir?h.addClass("cke_"+f):h.removeClass("cke_ltr").removeClass("cke_rtl");
g.setAttribute("style",CKEDITOR.skin.getIconStyle(t,"rtl"==f,this.icon,this.iconOffset))
},this)
}w||(x+="off");
var e=this.name||this.command,t=e;
this.icon&&!/\./.test(this.icon)&&(t=this.icon,this.icon=null);
x={id:o,name:e,iconName:t,label:this.label,cls:this.className||"",state:x,ariaDisabled:"disabled"==x?"true":"false",title:this.title,titleJs:r.gecko&&!r.hc?"":(this.title||"").replace("'",""),hasArrow:this.hasArrow?"true":"false",keydownFn:D,focusFn:C,clickFn:j,style:CKEDITOR.skin.getIconStyle(t,"rtl"==B.lang.dir,this.icon,this.iconOffset),arrowHtml:this.hasArrow?b.output():""};
a.output(x,A);
if(this.onRender){this.onRender()
}return y
},setState:function(e){if(this._.state==e){return !1
}this._.state=e;
var c=CKEDITOR.document.getById(this._.id);
return c?(c.setState(e,"cke_button"),e==CKEDITOR.TRISTATE_DISABLED?c.setAttribute("aria-disabled",!0):c.removeAttribute("aria-disabled"),this.hasArrow?(e=e==CKEDITOR.TRISTATE_ON?this._.editor.lang.button.selectedLabel.replace(/%1/g,this.label):this.label,CKEDITOR.document.getById(this._.id+"_label").setText(e)):e==CKEDITOR.TRISTATE_ON?c.setAttribute("aria-pressed",!0):c.removeAttribute("aria-pressed"),!0):!1
},getState:function(){return this._.state
},toFeature:function(e){if(this._.feature){return this._.feature
}var c=this;
this.allowedContent||this.requiredContent||!this.command||(c=e.getCommand(this.command)||c);
return this._.feature=c
}};
CKEDITOR.ui.prototype.addButton=function(e,c){this.add(e,CKEDITOR.UI_BUTTON,c)
}
})();
CKEDITOR.plugins.add("panelbutton",{requires:"button",onLoad:function(){function a(d){var b=this._;
b.state!=CKEDITOR.TRISTATE_DISABLED&&(this.createPanel(d),b.on?b.panel.hide():b.panel.showBlock(this._.id,this.document.getById(this._.id),4))
}CKEDITOR.ui.panelButton=CKEDITOR.tools.createClass({base:CKEDITOR.ui.button,$:function(d){var b=d.panel||{};
delete d.panel;
this.base(d);
this.document=b.parent&&b.parent.getDocument()||CKEDITOR.document;
b.block={attributes:b.attributes};
this.hasArrow=b.toolbarRelated=!0;
this.click=a;
this._={panelDefinition:b}
},statics:{handler:{create:function(b){return new CKEDITOR.ui.panelButton(b)
}}},proto:{createPanel:function(n){var i=this._;
if(!i.panel){var k=this._.panelDefinition,l=this._.panelDefinition.block,j=k.parent||CKEDITOR.document.getBody(),m=this._.panel=new CKEDITOR.ui.floatPanel(n,j,k),k=m.addBlock(i.id,l),h=this;
m.onShow=function(){h.className&&this.element.addClass(h.className+"_panel");
h.setState(CKEDITOR.TRISTATE_ON);
i.on=1;
h.editorFocus&&n.focus();
if(h.onOpen){h.onOpen()
}};
m.onHide=function(b){h.className&&this.element.getFirst().removeClass(h.className+"_panel");
h.setState(h.modes&&h.modes[n.mode]?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED);
i.on=0;
if(!b&&h.onClose){h.onClose()
}};
m.onEscape=function(){m.hide(1);
h.document.getById(i.id).focus()
};
if(this.onBlock){this.onBlock(m,k)
}k.onHide=function(){i.on=0;
h.setState(CKEDITOR.TRISTATE_OFF)
}
}}}})
},beforeInit:function(a){a.ui.addHandler(CKEDITOR.UI_PANELBUTTON,CKEDITOR.ui.panelButton.handler)
}});
CKEDITOR.UI_PANELBUTTON="panelbutton";
(function(){CKEDITOR.plugins.add("panel",{beforeInit:function(d){d.ui.addHandler(CKEDITOR.UI_PANEL,CKEDITOR.ui.panel.handler)
}});
CKEDITOR.UI_PANEL="panel";
CKEDITOR.ui.panel=function(e,d){d&&CKEDITOR.tools.extend(this,d);
CKEDITOR.tools.extend(this,{className:"",css:[]});
this.id=CKEDITOR.tools.getNextId();
this.document=e;
this.isFramed=this.forceIFrame||this.css.length;
this._={blocks:{}}
};
CKEDITOR.ui.panel.handler={create:function(d){return new CKEDITOR.ui.panel(d)
}};
var c=CKEDITOR.addTemplate("panel",'\x3cdiv lang\x3d"{langCode}" id\x3d"{id}" dir\x3d{dir} class\x3d"cke cke_reset_all {editorId} cke_panel cke_panel {cls} cke_{dir}" style\x3d"z-index:{z-index}" role\x3d"presentation"\x3e{frame}\x3c/div\x3e'),b=CKEDITOR.addTemplate("panel-frame",'\x3ciframe id\x3d"{id}" class\x3d"cke_panel_frame" role\x3d"presentation" frameborder\x3d"0" src\x3d"{src}"\x3e\x3c/iframe\x3e'),a=CKEDITOR.addTemplate("panel-frame-inner",'\x3c!DOCTYPE html\x3e\x3chtml class\x3d"cke_panel_container {env}" dir\x3d"{dir}" lang\x3d"{langCode}"\x3e\x3chead\x3e{css}\x3c/head\x3e\x3cbody class\x3d"cke_{dir}" style\x3d"margin:0;padding:0" onload\x3d"{onload}"\x3e\x3c/body\x3e\x3c/html\x3e');
CKEDITOR.ui.panel.prototype={render:function(g,f){this.getHolderElement=function(){var e=this._.holder;
if(!e){if(this.isFramed){var e=this.document.getById(this.id+"_frame"),d=e.getParent(),e=e.getFrameDocument();
CKEDITOR.env.iOS&&d.setStyles({overflow:"scroll","-webkit-overflow-scrolling":"touch"});
d=CKEDITOR.tools.addFunction(CKEDITOR.tools.bind(function(){this.isLoaded=!0;
if(this.onLoad){this.onLoad()
}},this));
e.write(a.output(CKEDITOR.tools.extend({css:CKEDITOR.tools.buildStyleHtml(this.css),onload:"window.parent.CKEDITOR.tools.callFunction("+d+");"},i)));
e.getWindow().$.CKEDITOR=CKEDITOR;
e.on("keydown",function(k){var j=k.data.getKeystroke(),l=this.document.getById(this.id).getAttribute("dir");
this._.onKeyDown&&!1===this._.onKeyDown(j)?k.data.preventDefault():(27==j||j==("rtl"==l?39:37))&&this.onEscape&&!1===this.onEscape(j)&&k.data.preventDefault()
},this);
e=e.getBody();
e.unselectable();
CKEDITOR.env.air&&CKEDITOR.tools.callFunction(d)
}else{e=this.document.getById(this.id)
}this._.holder=e
}return e
};
var i={editorId:g.id,id:this.id,langCode:g.langCode,dir:g.lang.dir,cls:this.className,frame:"",env:CKEDITOR.env.cssClass,"z-index":g.config.baseFloatZIndex+1};
if(this.isFramed){var h=CKEDITOR.env.air?"javascript:void(0)":CKEDITOR.env.ie?"javascript:void(function(){"+encodeURIComponent("document.open();("+CKEDITOR.tools.fixDomain+")();document.close();")+"}())":"";
i.frame=b.output({id:this.id+"_frame",src:h})
}h=c.output(i);
f&&f.push(h);
return h
},addBlock:function(e,d){d=this._.blocks[e]=d instanceof CKEDITOR.ui.panel.block?d:new CKEDITOR.ui.panel.block(this.getHolderElement(),d);
this._.currentBlock||this.showBlock(e);
return d
},getBlock:function(d){return this._.blocks[d]
},showBlock:function(f){f=this._.blocks[f];
var e=this._.currentBlock,g=!this.forceIFrame||CKEDITOR.env.ie?this._.holder:this.document.getById(this.id+"_frame");
e&&e.hide();
this._.currentBlock=f;
CKEDITOR.fire("ariaWidget",g);
f._.focusIndex=-1;
this._.onKeyDown=f.onKeyDown&&CKEDITOR.tools.bind(f.onKeyDown,f);
f.show();
return f
},destroy:function(){this.element&&this.element.remove()
}};
CKEDITOR.ui.panel.block=CKEDITOR.tools.createClass({$:function(e,d){this.element=e.append(e.getDocument().createElement("div",{attributes:{tabindex:-1,"class":"cke_panel_block"},styles:{display:"none"}}));
d&&CKEDITOR.tools.extend(this,d);
this.element.setAttributes({role:this.attributes.role||"presentation","aria-label":this.attributes["aria-label"],title:this.attributes.title||this.attributes["aria-label"]});
this.keys={};
this._.focusIndex=-1;
this.element.disableContextMenu()
},_:{markItem:function(d){-1!=d&&(d=this.element.getElementsByTag("a").getItem(this._.focusIndex=d),CKEDITOR.env.webkit&&d.getDocument().getWindow().focus(),d.focus(),this.onMark&&this.onMark(d))
}},proto:{show:function(){this.element.setStyle("display","")
},hide:function(){this.onHide&&!0===this.onHide.call(this)||this.element.setStyle("display","none")
},onKeyDown:function(g,f){var i=this.keys[g];
switch(i){case"next":for(var h=this._.focusIndex,i=this.element.getElementsByTag("a"),j;
j=i.getItem(++h);
){if(j.getAttribute("_cke_focus")&&j.$.offsetWidth){this._.focusIndex=h;
j.focus();
break
}}return j||f?!1:(this._.focusIndex=-1,this.onKeyDown(g,1));
case"prev":h=this._.focusIndex;
for(i=this.element.getElementsByTag("a");
0<h&&(j=i.getItem(--h));
){if(j.getAttribute("_cke_focus")&&j.$.offsetWidth){this._.focusIndex=h;
j.focus();
break
}j=null
}return j||f?!1:(this._.focusIndex=i.count(),this.onKeyDown(g,1));
case"click":case"mouseup":return h=this._.focusIndex,(j=0<=h&&this.element.getElementsByTag("a").getItem(h))&&(j.$[i]?j.$[i]():j.$["on"+i]()),!1
}return !0
}}})
})();
CKEDITOR.plugins.add("floatpanel",{requires:"panel"});
(function(){function a(f,d,k,e,i){i=CKEDITOR.tools.genKey(d.getUniqueId(),k.getUniqueId(),f.lang.dir,f.uiColor||"",e.css||"",i||"");
var j=b[i];
j||(j=b[i]=new CKEDITOR.ui.panel(d,e),j.element=k.append(CKEDITOR.dom.element.createFromHtml(j.render(f),d)),j.element.setStyles({display:"none",position:"absolute"}));
return j
}var b={};
CKEDITOR.ui.floatPanel=CKEDITOR.tools.createClass({$:function(r,q,o,i){function j(){n.hide()
}o.forceIFrame=1;
o.toolbarRelated&&r.elementMode==CKEDITOR.ELEMENT_MODE_INLINE&&(q=CKEDITOR.document.getById("cke_"+r.name));
var k=q.getDocument();
i=a(r,k,q,o,i||0);
var f=i.element,d=f.getFirst(),n=this;
f.disableContextMenu();
this.element=f;
this._={editor:r,panel:i,parentElement:q,definition:o,document:k,iframe:d,children:[],dir:r.lang.dir,showBlockParams:null};
r.on("mode",j);
r.on("resize",j);
k.getWindow().on("resize",function(){this.reposition()
},this)
},proto:{addBlock:function(d,c){return this._.panel.addBlock(d,c)
},addListBlock:function(d,c){return this._.panel.addListBlock(d,c)
},getBlock:function(c){return this._.panel.getBlock(c)
},showBlock:function(E,D,C,v,x,y){var u=this._.panel,o=u.showBlock(E);
this._.showBlockParams=[].slice.call(arguments);
this.allowBlur(!1);
var A=this._.editor.editable();
this._.returnFocus=A.hasFocus?A:new CKEDITOR.dom.element(CKEDITOR.document.$.activeElement);
this._.hideTimeout=0;
var w=this.element,A=this._.iframe,A=CKEDITOR.env.ie&&!CKEDITOR.env.edge?A:new CKEDITOR.dom.window(A.$.contentWindow),z=w.getDocument(),i=this._.parentElement.getPositionedAncestor(),F=D.getDocumentPosition(z),z=i?i.getDocumentPosition(z):{x:0,y:0},j="rtl"==this._.dir,B=F.x+(v||0)-z.x,s=F.y+(x||0)-z.y;
!j||1!=C&&4!=C?j||2!=C&&3!=C||(B+=D.$.offsetWidth-1):B+=D.$.offsetWidth;
if(3==C||4==C){s+=D.$.offsetHeight-1
}this._.panel._.offsetParentId=D.getId();
w.setStyles({top:s+"px",left:0,display:""});
w.setOpacity(0);
w.getFirst().removeStyle("width");
this._.editor.focusManager.add(A);
this._.blurSet||(CKEDITOR.event.useCapture=!0,A.on("blur",function(c){function d(){delete this._.returnFocus;
this.hide()
}this.allowBlur()&&c.data.getPhase()==CKEDITOR.EVENT_PHASE_AT_TARGET&&this.visible&&!this._.activeChild&&(CKEDITOR.env.iOS?this._.hideTimeout||(this._.hideTimeout=CKEDITOR.tools.setTimeout(d,0,this)):d.call(this))
},this),A.on("focus",function(){this._.focused=!0;
this.hideChild();
this.allowBlur(!0)
},this),CKEDITOR.env.iOS&&(A.on("touchstart",function(){clearTimeout(this._.hideTimeout)
},this),A.on("touchend",function(){this._.hideTimeout=0;
this.focus()
},this)),CKEDITOR.event.useCapture=!1,this._.blurSet=1);
u.onEscape=CKEDITOR.tools.bind(function(c){if(this.onEscape&&!1===this.onEscape(c)){return !1
}},this);
CKEDITOR.tools.setTimeout(function(){var c=CKEDITOR.tools.bind(function(){var k=w;
k.removeStyle("width");
if(o.autoSize){var d=o.element.getDocument(),d=(CKEDITOR.env.webkit?o.element:d.getBody()).$.scrollWidth;
CKEDITOR.env.ie&&CKEDITOR.env.quirks&&0<d&&(d+=(k.$.offsetWidth||0)-(k.$.clientWidth||0)+3);
k.setStyle("width",d+10+"px");
d=o.element.$.scrollHeight;
CKEDITOR.env.ie&&CKEDITOR.env.quirks&&0<d&&(d+=(k.$.offsetHeight||0)-(k.$.clientHeight||0)+3);
k.setStyle("height",d+"px");
u._.currentBlock.element.setStyle("display","none").removeStyle("display")
}else{k.removeStyle("height")
}j&&(B-=w.$.offsetWidth);
w.setStyle("left",B+"px");
var d=u.element.getWindow(),k=w.$.getBoundingClientRect(),d=d.getViewPaneSize(),q=k.width||k.right-k.left,p=k.height||k.bottom-k.top,g=j?k.right:d.width-k.left,m=j?d.width-k.right:k.left;
j?g<q&&(B=m>q?B+q:d.width>q?B-k.left:B-k.right+d.width):g<q&&(B=m>q?B-q:d.width>q?B-k.right+d.width:B-k.left);
q=k.top;
d.height-k.top<p&&(s=q>p?s-p:d.height>p?s-k.bottom+d.height:s-k.top);
CKEDITOR.env.ie&&(d=k=new CKEDITOR.dom.element(w.$.offsetParent),"html"==d.getName()&&(d=d.getDocument().getBody()),"rtl"==d.getComputedStyle("direction")&&(B=CKEDITOR.env.ie8Compat?B-2*w.getDocument().getDocumentElement().$.scrollLeft:B-(k.$.scrollWidth-k.$.clientWidth)));
var k=w.getFirst(),n;
(n=k.getCustomData("activePanel"))&&n.onHide&&n.onHide.call(this,1);
k.setCustomData("activePanel",this);
w.setStyles({top:s+"px",left:B+"px"});
w.setOpacity(1);
y&&y()
},this);
u.isLoaded?c():u.onLoad=c;
CKEDITOR.tools.setTimeout(function(){var d=CKEDITOR.env.webkit&&CKEDITOR.document.getWindow().getScrollPosition().y;
this.focus();
o.element.focus();
CKEDITOR.env.webkit&&(CKEDITOR.document.getBody().$.scrollTop=d);
this.allowBlur(!0);
this._.editor.fire("panelShow",this)
},0,this)
},CKEDITOR.env.air?200:0,this);
this.visible=1;
this.onShow&&this.onShow.call(this)
},reposition:function(){var c=this._.showBlockParams;
this.visible&&this._.showBlockParams&&(this.hide(),this.showBlock.apply(this,c))
},focus:function(){if(CKEDITOR.env.webkit){var c=CKEDITOR.document.getActive();
c&&!c.equals(this._.iframe)&&c.$.blur()
}(this._.lastFocused||this._.iframe.getFrameDocument().getWindow()).focus()
},blur:function(){var c=this._.iframe.getFrameDocument().getActive();
c&&c.is("a")&&(this._.lastFocused=c)
},hide:function(c){if(this.visible&&(!this.onHide||!0!==this.onHide.call(this))){this.hideChild();
CKEDITOR.env.gecko&&this._.iframe.getFrameDocument().$.activeElement.blur();
this.element.setStyle("display","none");
this.visible=0;
this.element.getFirst().removeCustomData("activePanel");
if(c=c&&this._.returnFocus){CKEDITOR.env.webkit&&c.type&&c.getWindow().$.focus(),c.focus()
}delete this._.lastFocused;
this._.showBlockParams=null;
this._.editor.fire("panelHide",this)
}},allowBlur:function(d){var c=this._.panel;
void 0!==d&&(c.allowBlur=d);
return c.allowBlur
},showAsChild:function(e,d,l,k,i,j){if(this._.activeChild!=e||e._.panel._.offsetParentId!=l.getId()){this.hideChild(),e.onHide=CKEDITOR.tools.bind(function(){CKEDITOR.tools.setTimeout(function(){this._.focused||this.hide()
},0,this)
},this),this._.activeChild=e,this._.focused=!1,e.showBlock(d,l,k,i,j),this.blur(),(CKEDITOR.env.ie7Compat||CKEDITOR.env.ie6Compat)&&setTimeout(function(){e.element.getChild(0).$.style.cssText+=""
},100)
}},hideChild:function(d){var c=this._.activeChild;
c&&(delete c.onHide,delete this._.activeChild,c.hide(),d&&this.focus())
}}});
CKEDITOR.on("instanceDestroyed",function(){var e=CKEDITOR.tools.isEmpty(CKEDITOR.instances),d;
for(d in b){var f=b[d];
e?f.destroy():f.element.hide()
}e&&(b={})
})
})();
CKEDITOR.plugins.add("colorbutton",{requires:"panelbutton,floatpanel",init:function(h){function e(n,j,l,i){var f=new CKEDITOR.style(a["colorButton_"+j+"Style"]),c=CKEDITOR.tools.getNextId()+"_colorBox";
h.ui.add(n,CKEDITOR.UI_PANELBUTTON,{label:l,title:l,modes:{wysiwyg:1},editorFocus:0,toolbar:"colors,"+i,allowedContent:f,requiredContent:f,panel:{css:CKEDITOR.skin.getPath("editor"),attributes:{role:"listbox","aria-label":g.panelTitle}},onBlock:function(m,k){k.autoSize=!0;
k.element.addClass("cke_colorblock");
k.element.setHtml(b(m,j,c));
k.element.getDocument().getBody().setStyle("overflow","hidden");
CKEDITOR.ui.fire("ready",this);
var p=k.keys,o="rtl"==h.lang.dir;
p[o?37:39]="next";
p[40]="next";
p[9]="next";
p[o?39:37]="prev";
p[38]="prev";
p[CKEDITOR.SHIFT+9]="prev";
p[32]="click"
},refresh:function(){h.activeFilter.check(f)||this.setState(CKEDITOR.TRISTATE_DISABLED)
},onOpen:function(){var m=h.getSelection(),m=m&&m.getStartElement(),m=h.elementPath(m),k;
if(m){m=m.block||m.blockLimit||h.document.getBody();
do{k=m&&m.getComputedStyle("back"==j?"background-color":"color")||"transparent"
}while("back"==j&&"transparent"==k&&m&&(m=m.getParent()));
k&&"transparent"!=k||(k="#ffffff");
this._.panel._.iframe.getFrameDocument().getById(c).setStyle("background-color",k);
return k
}}})
}function b(f,r,s){var o=[],l=a.colorButton_colors.split(","),j=h.plugins.colordialog&&!1!==a.colorButton_enableMore,v=l.length+(j?2:1),u=CKEDITOR.tools.addFunction(function(m,k){function q(w){this.removeListener("ok",q);
this.removeListener("cancel",q);
"ok"==w.name&&p(this.getContentElement("picker","selectedColor").getValue(),k)
}var p=arguments.callee;
if("?"==m){h.openDialog("colordialog",function(){this.on("ok",q);
this.on("cancel",q)
})
}else{h.focus();
f.hide();
h.fire("saveSnapshot");
h.removeStyle(new CKEDITOR.style(a["colorButton_"+k+"Style"],{color:"inherit"}));
if(m){var n=a["colorButton_"+k+"Style"];
n.childRule="back"==k?function(w){return d(w)
}:function(w){return !(w.is("a")||w.getElementsByTag("a").count())||d(w)
};
h.applyStyle(new CKEDITOR.style(n,{color:m}))
}h.fire("saveSnapshot")
}});
o.push('\x3ca class\x3d"cke_colorauto" _cke_focus\x3d1 hidefocus\x3dtrue title\x3d"',g.auto,'" onclick\x3d"CKEDITOR.tools.callFunction(',u,",null,'",r,"');return false;\" href\x3d\"javascript:void('",g.auto,'\')" role\x3d"option" aria-posinset\x3d"1" aria-setsize\x3d"',v,'"\x3e\x3ctable role\x3d"presentation" cellspacing\x3d0 cellpadding\x3d0 width\x3d"100%"\x3e\x3ctr\x3e\x3ctd\x3e\x3cspan class\x3d"cke_colorbox" id\x3d"',s,'"\x3e\x3c/span\x3e\x3c/td\x3e\x3ctd colspan\x3d7 align\x3dcenter\x3e',g.auto,'\x3c/td\x3e\x3c/tr\x3e\x3c/table\x3e\x3c/a\x3e\x3ctable role\x3d"presentation" cellspacing\x3d0 cellpadding\x3d0 width\x3d"100%"\x3e');
for(s=0;
s<l.length;
s++){0===s%8&&o.push("\x3c/tr\x3e\x3ctr\x3e");
var t=l[s].split("/"),i=t[0],c=t[1]||i;
t[1]||(i="#"+i.replace(/^(.)(.)(.)$/,"$1$1$2$2$3$3"));
t=h.lang.colorbutton.colors[c]||c;
o.push('\x3ctd\x3e\x3ca class\x3d"cke_colorbox" _cke_focus\x3d1 hidefocus\x3dtrue title\x3d"',t,'" onclick\x3d"CKEDITOR.tools.callFunction(',u,",'",i,"','",r,"'); return false;\" href\x3d\"javascript:void('",t,'\')" role\x3d"option" aria-posinset\x3d"',s+2,'" aria-setsize\x3d"',v,'"\x3e\x3cspan class\x3d"cke_colorbox" style\x3d"background-color:#',c,'"\x3e\x3c/span\x3e\x3c/a\x3e\x3c/td\x3e')
}j&&o.push('\x3c/tr\x3e\x3ctr\x3e\x3ctd colspan\x3d8 align\x3dcenter\x3e\x3ca class\x3d"cke_colormore" _cke_focus\x3d1 hidefocus\x3dtrue title\x3d"',g.more,'" onclick\x3d"CKEDITOR.tools.callFunction(',u,",'?','",r,"');return false;\" href\x3d\"javascript:void('",g.more,"')\"",' role\x3d"option" aria-posinset\x3d"',v,'" aria-setsize\x3d"',v,'"\x3e',g.more,"\x3c/a\x3e\x3c/td\x3e");
o.push("\x3c/tr\x3e\x3c/table\x3e");
return o.join("")
}function d(f){return"false"==f.getAttribute("contentEditable")||f.getAttribute("data-nostyle")
}var a=h.config,g=h.lang.colorbutton;
CKEDITOR.env.hc||(e("TextColor","fore",g.textColorTitle,10),e("BGColor","back",g.bgColorTitle,20))
}});
CKEDITOR.config.colorButton_colors="000,800000,8B4513,2F4F4F,008080,000080,4B0082,696969,B22222,A52A2A,DAA520,006400,40E0D0,0000CD,800080,808080,F00,FF8C00,FFD700,008000,0FF,00F,EE82EE,A9A9A9,FFA07A,FFA500,FFFF00,00FF00,AFEEEE,ADD8E6,DDA0DD,D3D3D3,FFF0F5,FAEBD7,FFFFE0,F0FFF0,F0FFFF,F0F8FF,E6E6FA,FFF";
CKEDITOR.config.colorButton_foreStyle={element:"span",styles:{color:"#(color)"},overrides:[{element:"font",attributes:{color:null}}]};
CKEDITOR.config.colorButton_backStyle={element:"span",styles:{"background-color":"#(color)"}};
CKEDITOR.plugins.colordialog={requires:"dialog",init:function(a){var d=new CKEDITOR.dialogCommand("colordialog");
d.editorFocus=!1;
a.addCommand("colordialog",d);
CKEDITOR.dialog.add("colordialog",this.path+"dialogs/colordialog.js");
a.getColorFromDialog=function(i,b){var h=function(c){this.removeListener("ok",h);
this.removeListener("cancel",h);
c="ok"==c.name?this.getValueOf("picker","selectedColor"):null;
i.call(b,c)
},g=function(c){c.on("ok",h);
c.on("cancel",h)
};
a.execCommand("colordialog");
if(a._.storedDialogs&&a._.storedDialogs.colordialog){g(a._.storedDialogs.colordialog)
}else{CKEDITOR.on("dialogDefinition",function(e){if("colordialog"==e.data.name){var c=e.data.definition;
e.removeListener();
c.onLoad=CKEDITOR.tools.override(c.onLoad,function(f){return function(){g(this);
c.onLoad=f;
"function"==typeof f&&f.call(this)
}
})
}})
}}
}};
CKEDITOR.plugins.add("colordialog",CKEDITOR.plugins.colordialog);
(function(){CKEDITOR.plugins.add("templates",{requires:"dialog",init:function(c){CKEDITOR.dialog.add("templates",CKEDITOR.getUrl(this.path+"dialogs/templates.js"));
c.addCommand("templates",new CKEDITOR.dialogCommand("templates"));
c.ui.addButton&&c.ui.addButton("Templates",{label:c.lang.templates.button,command:"templates",toolbar:"doctools,10"})
}});
var b={},a={};
CKEDITOR.addTemplates=function(c,e){b[c]=e
};
CKEDITOR.getTemplates=function(c){return b[c]
};
CKEDITOR.loadTemplates=function(g,i){for(var h=[],f=0,j=g.length;
f<j;
f++){a[g[f]]||(h.push(g[f]),a[g[f]]=1)
}h.length?CKEDITOR.scriptLoader.load(h,i):setTimeout(i,0)
}
})();
CKEDITOR.config.templates_files=[CKEDITOR.getUrl("plugins/templates/templates/default.js")];
CKEDITOR.config.templates_replaceContent=!0;
CKEDITOR.plugins.add("menu",{requires:"floatpanel",beforeInit:function(d){for(var c=d.config.menu_groups.split(","),e=d._.menuGroups={},f=d._.menuItems={},b=0;
b<c.length;
b++){e[c[b]]=b+1
}d.addMenuGroup=function(g,h){e[g]=h||100
};
d.addMenuItem=function(g,h){e[h.group]&&(f[g]=new CKEDITOR.menuItem(this,g,h))
};
d.addMenuItems=function(g){for(var h in g){this.addMenuItem(h,g[h])
}};
d.getMenuItem=function(g){return f[g]
};
d.removeMenuItem=function(g){delete f[g]
}
}});
(function(){function b(e){e.sort(function(f,g){return f.group<g.group?-1:f.group>g.group?1:f.order<g.order?-1:f.order>g.order?1:0
})
}var a='\x3cspan class\x3d"cke_menuitem"\x3e\x3ca id\x3d"{id}" class\x3d"cke_menubutton cke_menubutton__{name} cke_menubutton_{state} {cls}" href\x3d"{href}" title\x3d"{title}" tabindex\x3d"-1"_cke_focus\x3d1 hidefocus\x3d"true" role\x3d"{role}" aria-haspopup\x3d"{hasPopup}" aria-disabled\x3d"{disabled}" {ariaChecked}';
CKEDITOR.env.gecko&&CKEDITOR.env.mac&&(a+=' onkeypress\x3d"return false;"');
CKEDITOR.env.gecko&&(a+=' onblur\x3d"this.style.cssText \x3d this.style.cssText;"');
var a=a+(' onmouseover\x3d"CKEDITOR.tools.callFunction({hoverFn},{index});" onmouseout\x3d"CKEDITOR.tools.callFunction({moveOutFn},{index});" '+(CKEDITOR.env.ie?'onclick\x3d"return false;" onmouseup':"onclick")+'\x3d"CKEDITOR.tools.callFunction({clickFn},{index}); return false;"\x3e'),c=CKEDITOR.addTemplate("menuItem",a+'\x3cspan class\x3d"cke_menubutton_inner"\x3e\x3cspan class\x3d"cke_menubutton_icon"\x3e\x3cspan class\x3d"cke_button_icon cke_button__{iconName}_icon" style\x3d"{iconStyle}"\x3e\x3c/span\x3e\x3c/span\x3e\x3cspan class\x3d"cke_menubutton_label"\x3e{label}\x3c/span\x3e{arrowHtml}\x3c/span\x3e\x3c/a\x3e\x3c/span\x3e'),d=CKEDITOR.addTemplate("menuArrow",'\x3cspan class\x3d"cke_menuarrow"\x3e\x3cspan\x3e{label}\x3c/span\x3e\x3c/span\x3e');
CKEDITOR.menu=CKEDITOR.tools.createClass({$:function(g,f){f=this._.definition=f||{};
this.id=CKEDITOR.tools.getNextId();
this.editor=g;
this.items=[];
this._.listeners=[];
this._.level=f.level||1;
var h=CKEDITOR.tools.extend({},f.panel,{css:[CKEDITOR.skin.getPath("editor")],level:this._.level-1,block:{}}),e=h.block.attributes=h.attributes||{};
!e.role&&(e.role="menu");
this._.panelDefinition=h
},_:{onShow:function(){var j=this.editor.getSelection(),h=j&&j.getStartElement(),q=this.editor.elementPath(),g=this._.listeners;
this.removeAll();
for(var p=0;
p<g.length;
p++){var i=g[p](h,j,q);
if(i){for(var n in i){var o=this.editor.getMenuItem(n);
!o||o.command&&!this.editor.getCommand(o.command).state||(o.state=i[n],this.add(o))
}}}},onClick:function(e){this.hide();
if(e.onClick){e.onClick()
}else{e.command&&this.editor.execCommand(e.command)
}},onEscape:function(f){var e=this.parent;
e?e._.panel.hideChild(1):27==f&&this.hide(1);
return !1
},onHide:function(){this.onHide&&this.onHide()
},showSubMenu:function(i){var g=this._.subMenu,k=this.items[i];
if(k=k.getItems&&k.getItems()){g?g.removeAll():(g=this._.subMenu=new CKEDITOR.menu(this.editor,CKEDITOR.tools.extend({},this._.definition,{level:this._.level+1},!0)),g.parent=this,g._.onClick=CKEDITOR.tools.bind(this._.onClick,this));
for(var f in k){var j=this.editor.getMenuItem(f);
j&&(j.state=k[f],g.add(j))
}var h=this._.panel.getBlock(this.id).element.getDocument().getById(this.id+String(i));
setTimeout(function(){g.show(h,2)
},0)
}else{this._.panel.hideChild(1)
}}},proto:{add:function(e){e.order||(e.order=this.items.length);
this.items.push(e)
},removeAll:function(){this.items=[]
},show:function(z,y,x,o){if(!this.parent&&(this._.onShow(),!this.items.length)){return
}y=y||("rtl"==this.editor.lang.dir?2:1);
var v=this.items,r=this.editor,s=this._.panel,u=this._.element;
if(!s){s=this._.panel=new CKEDITOR.ui.floatPanel(this.editor,CKEDITOR.document.getBody(),this._.panelDefinition,this._.level);
s.onEscape=CKEDITOR.tools.bind(function(e){if(!1===this._.onEscape(e)){return !1
}},this);
s.onShow=function(){s._.panel.getHolderElement().getParent().addClass("cke").addClass("cke_reset_all")
};
s.onHide=CKEDITOR.tools.bind(function(){this._.onHide&&this._.onHide()
},this);
u=s.addBlock(this.id,this._.panelDefinition.block);
u.autoSize=!0;
var w=u.keys;
w[40]="next";
w[9]="next";
w[38]="prev";
w[CKEDITOR.SHIFT+9]="prev";
w["rtl"==r.lang.dir?37:39]=CKEDITOR.env.ie?"mouseup":"click";
w[32]=CKEDITOR.env.ie?"mouseup":"click";
CKEDITOR.env.ie&&(w[13]="mouseup");
u=this._.element=u.element;
w=u.getDocument();
w.getBody().setStyle("overflow","hidden");
w.getElementsByTag("html").getItem(0).setStyle("overflow","hidden");
this._.itemOverFn=CKEDITOR.tools.addFunction(function(e){clearTimeout(this._.showSubTimeout);
this._.showSubTimeout=CKEDITOR.tools.setTimeout(this._.showSubMenu,r.config.menu_subMenuDelay||400,this,[e])
},this);
this._.itemOutFn=CKEDITOR.tools.addFunction(function(){clearTimeout(this._.showSubTimeout)
},this);
this._.itemClickFn=CKEDITOR.tools.addFunction(function(f){var e=this.items[f];
if(e.state==CKEDITOR.TRISTATE_DISABLED){this.hide(1)
}else{if(e.getItems){this._.showSubMenu(f)
}else{this._.onClick(e)
}}},this)
}b(v);
for(var w=r.elementPath(),w=['\x3cdiv class\x3d"cke_menu'+(w&&w.direction()!=r.lang.dir?" cke_mixed_dir_content":"")+'" role\x3d"presentation"\x3e'],t=v.length,i=t&&v[0].group,j=0;
j<t;
j++){var g=v[j];
i!=g.group&&(w.push('\x3cdiv class\x3d"cke_menuseparator" role\x3d"separator"\x3e\x3c/div\x3e'),i=g.group);
g.render(this,j,w)
}w.push("\x3c/div\x3e");
u.setHtml(w.join(""));
CKEDITOR.ui.fire("ready",this);
this.parent?this.parent._.panel.showAsChild(s,this.id,z,y,x,o):s.showBlock(this.id,z,y,x,o);
r.fire("menuShow",[s])
},addListener:function(e){this._.listeners.push(e)
},hide:function(e){this._.onHide&&this._.onHide();
this._.panel&&this._.panel.hide(e)
}}});
CKEDITOR.menuItem=CKEDITOR.tools.createClass({$:function(f,e,g){CKEDITOR.tools.extend(this,g,{order:0,className:"cke_menubutton__"+e});
this.group=f._.menuGroups[this.group];
this.editor=f;
this.name=e
},proto:{render:function(t,s,r){var m=t.id+String(s),p="undefined"==typeof this.state?CKEDITOR.TRISTATE_OFF:this.state,i="",j=p==CKEDITOR.TRISTATE_ON?"on":p==CKEDITOR.TRISTATE_DISABLED?"disabled":"off";
this.role in {menuitemcheckbox:1,menuitemradio:1}&&(i=' aria-checked\x3d"'+(p==CKEDITOR.TRISTATE_ON?"true":"false")+'"');
var o=this.getItems,q="\x26#"+("rtl"==this.editor.lang.dir?"9668":"9658")+";",n=this.name;
this.icon&&!/\./.test(this.icon)&&(n=this.icon);
t={id:m,name:this.name,iconName:n,label:this.label,cls:this.className||"",state:j,hasPopup:o?"true":"false",disabled:p==CKEDITOR.TRISTATE_DISABLED,title:this.label,href:"javascript:void('"+(this.label||"").replace("'")+"')",hoverFn:t._.itemOverFn,moveOutFn:t._.itemOutFn,clickFn:t._.itemClickFn,index:s,iconStyle:CKEDITOR.skin.getIconStyle(n,"rtl"==this.editor.lang.dir,n==this.icon?null:this.icon,this.iconOffset),arrowHtml:o?d.output({label:q}):"",role:this.role?this.role:"menuitem",ariaChecked:i};
c.output(t,r)
}}})
})();
CKEDITOR.config.menu_groups="clipboard,form,tablecell,tablecellproperties,tablerow,tablecolumn,table,anchor,link,image,flash,checkbox,radio,textfield,hiddenfield,imagebutton,button,select,textarea,div";
CKEDITOR.plugins.add("contextmenu",{requires:"menu",onLoad:function(){CKEDITOR.plugins.contextMenu=CKEDITOR.tools.createClass({base:CKEDITOR.menu,$:function(b){this.base.call(this,b,{panel:{className:"cke_menu_panel",attributes:{"aria-label":b.lang.contextmenu.options}}})
},proto:{addTarget:function(b,g){b.on("contextmenu",function(f){f=f.data;
var l=CKEDITOR.env.webkit?c:CKEDITOR.env.mac?f.$.metaKey:f.$.ctrlKey;
if(!g||!l){f.preventDefault();
if(CKEDITOR.env.mac&&CKEDITOR.env.webkit){var l=this.editor,e=(new CKEDITOR.dom.elementPath(f.getTarget(),l.editable())).contains(function(d){return d.hasAttribute("contenteditable")
},!0);
e&&"false"==e.getAttribute("contenteditable")&&l.getSelection().fake(e)
}var e=f.getTarget().getDocument(),k=f.getTarget().getDocument().getDocumentElement(),l=!e.equals(CKEDITOR.document),e=e.getWindow().getScrollPosition(),j=l?f.$.clientX:f.$.pageX||e.x+f.$.clientX,i=l?f.$.clientY:f.$.pageY||e.y+f.$.clientY;
CKEDITOR.tools.setTimeout(function(){this.open(k,null,j,i)
},CKEDITOR.env.ie?200:0,this)
}},this);
if(CKEDITOR.env.webkit){var c,h=function(){c=0
};
b.on("keydown",function(d){c=CKEDITOR.env.mac?d.data.$.metaKey:d.data.$.ctrlKey
});
b.on("keyup",h);
b.on("contextmenu",h)
}},open:function(b,g,c,h){this.editor.focus();
b=b||CKEDITOR.document.getDocumentElement();
this.editor.selectionChange(1);
this.show(b,g,c,h)
}}})
},beforeInit:function(b){var c=b.contextMenu=new CKEDITOR.plugins.contextMenu(b);
b.on("contentDom",function(){c.addTarget(b.editable(),!1!==b.config.browserContextMenuOnCtrl)
});
b.addCommand("contextMenu",{exec:function(){b.contextMenu.open(b.document.getBody())
}});
b.setKeystroke(CKEDITOR.SHIFT+121,"contextMenu");
b.setKeystroke(CKEDITOR.CTRL+CKEDITOR.SHIFT+121,"contextMenu")
}});
(function(){CKEDITOR.plugins.add("div",{requires:"dialog",init:function(e){if(!e.blockless){var f=e.lang.div,d="div(*)";
CKEDITOR.dialog.isTabEnabled(e,"editdiv","advanced")&&(d+=";div[dir,id,lang,title]{*}");
e.addCommand("creatediv",new CKEDITOR.dialogCommand("creatediv",{allowedContent:d,requiredContent:"div",contextSensitive:!0,refresh:function(b,g){this.setState("div" in (b.config.div_wrapTable?g.root:g.blockLimit).getDtd()?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED)
}}));
e.addCommand("editdiv",new CKEDITOR.dialogCommand("editdiv",{requiredContent:"div"}));
e.addCommand("removediv",{requiredContent:"div",exec:function(j){function p(a){(a=CKEDITOR.plugins.div.getSurroundDiv(j,a))&&!a.data("cke-div-added")&&(m.push(a),a.data("cke-div-added"))
}for(var i=j.getSelection(),l=i&&i.getRanges(),n,k=i.createBookmarks(),m=[],o=0;
o<l.length;
o++){n=l[o],n.collapsed?p(i.getStartElement()):(n=new CKEDITOR.dom.walker(n),n.evaluator=p,n.lastForward())
}for(o=0;
o<m.length;
o++){m[o].remove(!0)
}i.selectBookmarks(k)
}});
e.ui.addButton&&e.ui.addButton("CreateDiv",{label:f.toolbar,command:"creatediv",toolbar:"blocks,50"});
e.addMenuItems&&(e.addMenuItems({editdiv:{label:f.edit,command:"editdiv",group:"div",order:1},removediv:{label:f.remove,command:"removediv",group:"div",order:5}}),e.contextMenu&&e.contextMenu.addListener(function(a){return !a||a.isReadOnly()?null:CKEDITOR.plugins.div.getSurroundDiv(e)?{editdiv:CKEDITOR.TRISTATE_OFF,removediv:CKEDITOR.TRISTATE_OFF}:null
}));
CKEDITOR.dialog.add("creatediv",this.path+"dialogs/div.js");
CKEDITOR.dialog.add("editdiv",this.path+"dialogs/div.js")
}}});
CKEDITOR.plugins.div={getSurroundDiv:function(e,f){var d=e.elementPath(f);
return e.elementPath(d.blockLimit).contains(function(b){return b.is("div")&&!b.isReadOnly()
},1)
}}
})();
CKEDITOR.plugins.add("resize",{init:function(w){function u(g){var c=v.width,a=v.height,b=c+(g.data.$.screenX-j.x)*("rtl"==t?-1:1);
g=a+(g.data.$.screenY-j.y);
s&&(c=Math.max(x.resize_minWidth,Math.min(b,x.resize_maxWidth)));
i&&(a=Math.max(x.resize_minHeight,Math.min(g,x.resize_maxHeight)));
w.resize(s?c:null,a)
}function o(){CKEDITOR.document.removeListener("mousemove",u);
CKEDITOR.document.removeListener("mouseup",o);
w.document&&(w.document.removeListener("mousemove",u),w.document.removeListener("mouseup",o))
}var x=w.config,d=w.ui.spaceId("resizer"),t=w.element?w.element.getDirection(1):"ltr";
!x.resize_dir&&(x.resize_dir="vertical");
void 0===x.resize_maxWidth&&(x.resize_maxWidth=3000);
void 0===x.resize_maxHeight&&(x.resize_maxHeight=3000);
void 0===x.resize_minWidth&&(x.resize_minWidth=750);
void 0===x.resize_minHeight&&(x.resize_minHeight=250);
if(!1!==x.resize_enabled){var m=null,j,v,s=("both"==x.resize_dir||"horizontal"==x.resize_dir)&&x.resize_minWidth!=x.resize_maxWidth,i=("both"==x.resize_dir||"vertical"==x.resize_dir)&&x.resize_minHeight!=x.resize_maxHeight,e=CKEDITOR.tools.addFunction(function(a){m||(m=w.getResizable());
v={width:m.$.offsetWidth||0,height:m.$.offsetHeight||0};
j={x:a.screenX,y:a.screenY};
x.resize_minWidth>v.width&&(x.resize_minWidth=v.width);
x.resize_minHeight>v.height&&(x.resize_minHeight=v.height);
CKEDITOR.document.on("mousemove",u);
CKEDITOR.document.on("mouseup",o);
w.document&&(w.document.on("mousemove",u),w.document.on("mouseup",o));
a.preventDefault&&a.preventDefault()
});
w.on("destroy",function(){CKEDITOR.tools.removeFunction(e)
});
w.on("uiSpace",function(b){if("bottom"==b.data.space){var f="";
s&&!i&&(f=" cke_resizer_horizontal");
!s&&i&&(f=" cke_resizer_vertical");
var g='\x3cspan id\x3d"'+d+'" class\x3d"cke_resizer'+f+" cke_resizer_"+t+'" title\x3d"'+CKEDITOR.tools.htmlEncode(w.lang.common.resize)+'" onmousedown\x3d"CKEDITOR.tools.callFunction('+e+', event)"\x3e'+("ltr"==t?"◢":"◣")+"\x3c/span\x3e";
"ltr"==t&&"ltr"==f?b.data.html+=g:b.data.html=g+b.data.html
}},w,null,100);
w.on("maximize",function(b){w.ui.space("resizer")[b.data==CKEDITOR.TRISTATE_ON?"hide":"show"]()
})
}}});
(function(){function d(i){function m(){for(var o=k(),s=CKEDITOR.tools.clone(i.config.toolbarGroups)||a(i),r=0;
r<s.length;
r++){var p=s[r];
if("/"!=p){"string"==typeof p&&(p=s[r]={name:p});
var g,t=p.groups;
if(t){for(var q=0;
q<t.length;
q++){g=t[q],(g=o[g])&&n(p,g)
}}(g=o[p.name])&&n(p,g)
}}return s
}function k(){var g={},p,h,o;
for(p in i.ui.items){h=i.ui.items[p],o=h.toolbar||"others",o=o.split(","),h=o[0],o=parseInt(o[1]||-1,10),g[h]||(g[h]=[]),g[h].push({name:p,order:o})
}for(h in g){g[h]=g[h].sort(function(e,q){return e.order==q.order?0:0>q.order?-1:0>e.order?1:e.order<q.order?-1:1
})
}return g
}function n(o,h){if(h.length){o.items?o.items.push(i.ui.create("-")):o.items=[];
for(var g;
g=h.shift();
){g="string"==typeof g?g:g.name,f&&-1!=CKEDITOR.tools.indexOf(f,g)||(g=i.ui.create(g))&&i.addFeature(g)&&o.items.push(g)
}}}function j(g){var o=[],q,r,p;
for(q=0;
q<g.length;
++q){r=g[q],p={},"/"==r?o.push(r):CKEDITOR.tools.isArray(r)?(n(p,CKEDITOR.tools.clone(r)),o.push(p)):r.items&&(n(p,CKEDITOR.tools.clone(r.items)),p.name=r.name,o.push(p))
}return o
}var f=i.config.removeButtons,f=f&&f.split(","),l=i.config.toolbar;
"string"==typeof l&&(l=i.config["toolbar_"+l]);
return i.toolbar=l?j(l):m()
}function a(e){return e._.toolbarGroups||(e._.toolbarGroups=[{name:"document",groups:["mode","document","doctools"]},{name:"clipboard",groups:["clipboard","undo"]},{name:"editing",groups:["find","selection","spellchecker"]},{name:"forms"},"/",{name:"basicstyles",groups:["basicstyles","cleanup"]},{name:"paragraph",groups:["list","indent","blocks","align","bidi"]},{name:"links"},{name:"insert"},"/",{name:"styles"},{name:"colors"},{name:"tools"},{name:"others"},{name:"about"}])
}var c=function(){this.toolbars=[];
this.focusCommandExecuted=!1
};
c.prototype.focus=function(){for(var e=0,h;
h=this.toolbars[e++];
){for(var f=0,i;
i=h.items[f++];
){if(i.focus){i.focus();
return
}}}};
var b={modes:{wysiwyg:1,source:1},readOnly:1,exec:function(e){e.toolbox&&(e.toolbox.focusCommandExecuted=!0,CKEDITOR.env.ie||CKEDITOR.env.air?setTimeout(function(){e.toolbox.focus()
},100):e.toolbox.focus())
}};
CKEDITOR.plugins.add("toolbar",{requires:"button",init:function(e){var h,f=function(n,j){var g,m="rtl"==e.lang.dir,i=e.config.toolbarGroupCycling,l=m?37:39,m=m?39:37,i=void 0===i||i;
switch(j){case 9:case CKEDITOR.SHIFT+9:for(;
!g||!g.items.length;
){if(g=9==j?(g?g.next:n.toolbar.next)||e.toolbox.toolbars[0]:(g?g.previous:n.toolbar.previous)||e.toolbox.toolbars[e.toolbox.toolbars.length-1],g.items.length){for(n=g.items[h?g.items.length-1:0];
n&&!n.focus;
){(n=h?n.previous:n.next)||(g=0)
}}}n&&n.focus();
return !1;
case l:g=n;
do{g=g.next,!g&&i&&(g=n.toolbar.items[0])
}while(g&&!g.focus);
g?g.focus():f(n,9);
return !1;
case 40:return n.button&&n.button.hasArrow?(e.once("panelShow",function(k){k.data._.panel._.currentBlock.onKeyDown(40)
}),n.execute()):f(n,40==j?l:m),!1;
case m:case 38:g=n;
do{g=g.previous,!g&&i&&(g=n.toolbar.items[n.toolbar.items.length-1])
}while(g&&!g.focus);
g?g.focus():(h=1,f(n,CKEDITOR.SHIFT+9),h=0);
return !1;
case 27:return e.focus(),!1;
case 13:case 32:return n.execute(),!1
}return !0
};
e.on("uiSpace",function(M){if(M.data.space==e.config.toolbarLocation){M.removeListener();
e.toolbox=new c;
var L=CKEDITOR.tools.getNextId(),N=['\x3cspan id\x3d"',L,'" class\x3d"cke_voice_label"\x3e',e.lang.toolbar.toolbars,"\x3c/span\x3e",'\x3cspan id\x3d"'+e.ui.spaceId("toolbox")+'" class\x3d"cke_toolbox" role\x3d"group" aria-labelledby\x3d"',L,'" onmousedown\x3d"return false;"\x3e'],L=!1!==e.config.toolbarStartupExpanded,K,I;
e.config.toolbarCanCollapse&&e.elementMode!=CKEDITOR.ELEMENT_MODE_INLINE&&N.push('\x3cspan class\x3d"cke_toolbox_main"'+(L?"\x3e":' style\x3d"display:none"\x3e'));
for(var B=e.toolbox.toolbars,J=d(e),H=0;
H<J.length;
H++){var G,F=0,o,E=J[H],j;
if(E){if(K&&(N.push("\x3c/span\x3e"),I=K=0),"/"===E){N.push('\x3cspan class\x3d"cke_toolbar_break"\x3e\x3c/span\x3e')
}else{j=E.items||E;
for(var i=0;
i<j.length;
i++){var y=j[i],C;
if(y){var g=function(k){k=k.render(e,N);
s=F.items.push(k)-1;
0<s&&(k.previous=F.items[s-1],k.previous.next=k);
k.toolbar=F;
k.onkey=f;
k.onfocus=function(){e.toolbox.focusCommandExecuted||e.focus()
}
};
if(y.type==CKEDITOR.UI_SEPARATOR){I=K&&y
}else{C=!1!==y.canGroup;
if(!F){G=CKEDITOR.tools.getNextId();
F={id:G,items:[]};
o=E.name&&(e.lang.toolbar.toolbarGroups[E.name]||E.name);
N.push('\x3cspan id\x3d"',G,'" class\x3d"cke_toolbar"',o?' aria-labelledby\x3d"'+G+'_label"':"",' role\x3d"toolbar"\x3e');
o&&N.push('\x3cspan id\x3d"',G,'_label" class\x3d"cke_voice_label"\x3e',o,"\x3c/span\x3e");
N.push('\x3cspan class\x3d"cke_toolbar_start"\x3e\x3c/span\x3e');
var s=B.push(F)-1;
0<s&&(F.previous=B[s-1],F.previous.next=F)
}C?K||(N.push('\x3cspan class\x3d"cke_toolgroup" role\x3d"presentation"\x3e'),K=1):K&&(N.push("\x3c/span\x3e"),K=0);
I&&(g(I),I=0);
g(y)
}}}K&&(N.push("\x3c/span\x3e"),I=K=0);
F&&N.push('\x3cspan class\x3d"cke_toolbar_end"\x3e\x3c/span\x3e\x3c/span\x3e')
}}}e.config.toolbarCanCollapse&&N.push("\x3c/span\x3e");
if(e.config.toolbarCanCollapse&&e.elementMode!=CKEDITOR.ELEMENT_MODE_INLINE){var D=CKEDITOR.tools.addFunction(function(){e.execCommand("toolbarCollapse")
});
e.on("destroy",function(){CKEDITOR.tools.removeFunction(D)
});
e.addCommand("toolbarCollapse",{readOnly:1,exec:function(k){var l=k.ui.space("toolbar_collapser"),t=l.getPrevious(),q=k.ui.space("contents"),r=t.getParent(),p=parseInt(q.$.style.height,10),m=r.$.offsetHeight,n=l.hasClass("cke_toolbox_collapser_min");
n?(t.show(),l.removeClass("cke_toolbox_collapser_min"),l.setAttribute("title",k.lang.toolbar.toolbarCollapse)):(t.hide(),l.addClass("cke_toolbox_collapser_min"),l.setAttribute("title",k.lang.toolbar.toolbarExpand));
l.getFirst().setText(n?"▲":"◀");
q.setStyle("height",p-(r.$.offsetHeight-m)+"px");
k.fire("resize",{outerHeight:k.container.$.offsetHeight,contentsHeight:q.$.offsetHeight,outerWidth:k.container.$.offsetWidth})
},modes:{wysiwyg:1,source:1}});
e.setKeystroke(CKEDITOR.ALT+(CKEDITOR.env.ie||CKEDITOR.env.webkit?189:109),"toolbarCollapse");
N.push('\x3ca title\x3d"'+(L?e.lang.toolbar.toolbarCollapse:e.lang.toolbar.toolbarExpand)+'" id\x3d"'+e.ui.spaceId("toolbar_collapser")+'" tabIndex\x3d"-1" class\x3d"cke_toolbox_collapser');
L||N.push(" cke_toolbox_collapser_min");
N.push('" onclick\x3d"CKEDITOR.tools.callFunction('+D+')"\x3e','\x3cspan class\x3d"cke_arrow"\x3e\x26#9650;\x3c/span\x3e',"\x3c/a\x3e")
}N.push("\x3c/span\x3e");
M.data.html+=N.join("")
}});
e.on("destroy",function(){if(this.toolbox){var j,m=0,i,l,k;
for(j=this.toolbox.toolbars;
m<j.length;
m++){for(l=j[m].items,i=0;
i<l.length;
i++){k=l[i],k.clickFn&&CKEDITOR.tools.removeFunction(k.clickFn),k.keyDownFn&&CKEDITOR.tools.removeFunction(k.keyDownFn)
}}}});
e.on("uiReady",function(){var g=e.ui.space("toolbox");
g&&e.focusManager.add(g,1)
});
e.addCommand("toolbarFocus",b);
e.setKeystroke(CKEDITOR.ALT+121,"toolbarFocus");
e.ui.add("-",CKEDITOR.UI_SEPARATOR,{});
e.ui.addHandler(CKEDITOR.UI_SEPARATOR,{create:function(){return{render:function(g,i){i.push('\x3cspan class\x3d"cke_toolbar_separator" role\x3d"separator"\x3e\x3c/span\x3e');
return{}
}}
}})
}});
CKEDITOR.ui.prototype.addToolbarGroup=function(f,k,j){var l=a(this.editor),i=0===k,e={name:f};
if(j){if(j=CKEDITOR.tools.search(l,function(g){return g.name==j
})){!j.groups&&(j.groups=[]);
if(k&&(k=CKEDITOR.tools.indexOf(j.groups,k),0<=k)){j.groups.splice(k+1,0,f);
return
}i?j.groups.splice(0,0,f):j.groups.push(f);
return
}k=null
}k&&(k=CKEDITOR.tools.indexOf(l,function(g){return g.name==k
}));
i?l.splice(0,0,f):"number"==typeof k?l.splice(k+1,0,e):l.push(f)
}
})();
CKEDITOR.UI_SEPARATOR="separator";
CKEDITOR.config.toolbarLocation="top";
(function(){function b(s,r){function j(k){k=o.list[k];
if(k.equals(s.editable())||"true"==k.getAttribute("contenteditable")){var d=s.createRange();
d.selectNodeContents(k);
d.select()
}else{s.getSelection().selectElement(k)
}s.focus()
}function i(){h&&h.setHtml('\x3cspan class\x3d"cke_path_empty"\x3e\x26nbsp;\x3c/span\x3e');
delete o.list
}var g=s.ui.spaceId("path"),h,o=s._.elementsPath,f=o.idBase;
r.html+='\x3cspan id\x3d"'+g+'_label" class\x3d"cke_voice_label"\x3e'+s.lang.elementspath.eleLabel+'\x3c/span\x3e\x3cspan id\x3d"'+g+'" class\x3d"cke_path" role\x3d"group" aria-labelledby\x3d"'+g+'_label"\x3e\x3cspan class\x3d"cke_path_empty"\x3e\x26nbsp;\x3c/span\x3e\x3c/span\x3e';
s.on("uiReady",function(){var d=s.ui.space("path");
d&&s.focusManager.add(d,1)
});
o.onClick=j;
var u=CKEDITOR.tools.addFunction(j),t=CKEDITOR.tools.addFunction(function(m,l){var k=o.idBase,d;
l=new CKEDITOR.dom.event(l);
d="rtl"==s.lang.dir;
switch(l.getKeystroke()){case d?39:37:case 9:return(d=CKEDITOR.document.getById(k+(m+1)))||(d=CKEDITOR.document.getById(k+"0")),d.focus(),!1;
case d?37:39:case CKEDITOR.SHIFT+9:return(d=CKEDITOR.document.getById(k+(m-1)))||(d=CKEDITOR.document.getById(k+(o.list.length-1))),d.focus(),!1;
case 27:return s.focus(),!1;
case 13:case 32:return j(m),!1
}return !0
});
s.on("selectionChange",function(){for(var z=[],x=o.list=[],v=[],A=o.filters,y=!0,p=s.elementPath().elements,w,B=p.length;
B--;
){var q=p[B],k=0;
w=q.data("cke-display-name")?q.data("cke-display-name"):q.data("cke-real-element-type")?q.data("cke-real-element-type"):q.getName();
(y=q.hasAttribute("contenteditable")?"true"==q.getAttribute("contenteditable"):y)||q.hasAttribute("contenteditable")||(k=1);
for(var C=0;
C<A.length;
C++){var n=A[C](q,w);
if(!1===n){k=1;
break
}w=n||w
}k||(x.unshift(q),v.unshift(w))
}x=x.length;
for(A=0;
A<x;
A++){w=v[A],y=s.lang.elementspath.eleTitle.replace(/%1/,w),w=a.output({id:f+A,label:y,text:w,jsTitle:"javascript:void('"+w+"')",index:A,keyDownFn:t,clickFn:u}),z.unshift(w)
}h||(h=CKEDITOR.document.getById(g));
v=h;
v.setHtml(z.join("")+'\x3cspan class\x3d"cke_path_empty"\x3e\x26nbsp;\x3c/span\x3e');
s.fire("elementsPathUpdate",{space:v})
});
s.on("readOnly",i);
s.on("contentDomUnload",i);
s.addCommand("elementsPathFocus",e.toolbarFocus);
s.setKeystroke(CKEDITOR.ALT+122,"elementsPathFocus")
}var e={toolbarFocus:{editorFocus:!1,readOnly:1,exec:function(d){(d=CKEDITOR.document.getById(d._.elementsPath.idBase+"0"))&&d.focus(CKEDITOR.env.ie||CKEDITOR.env.air)
}}},c="";
CKEDITOR.env.gecko&&CKEDITOR.env.mac&&(c+=' onkeypress\x3d"return false;"');
CKEDITOR.env.gecko&&(c+=' onblur\x3d"this.style.cssText \x3d this.style.cssText;"');
var a=CKEDITOR.addTemplate("pathItem",'\x3ca id\x3d"{id}" href\x3d"{jsTitle}" tabindex\x3d"-1" class\x3d"cke_path_item" title\x3d"{label}"'+c+' hidefocus\x3d"true"  onkeydown\x3d"return CKEDITOR.tools.callFunction({keyDownFn},{index}, event );" onclick\x3d"CKEDITOR.tools.callFunction({clickFn},{index}); return false;" role\x3d"button" aria-label\x3d"{label}"\x3e{text}\x3c/a\x3e');
CKEDITOR.plugins.add("elementspath",{init:function(d){d._.elementsPath={idBase:"cke_elementspath_"+CKEDITOR.tools.getNextNumber()+"_",filters:[]};
d.on("uiSpace",function(f){"bottom"==f.data.space&&b(d,f.data)
})
}})
})();
(function(){function a(j,l,k){k=j.config.forceEnterMode||k;
"wysiwyg"==j.mode&&(l||(l=j.activeEnterMode),j.elementPath().isContextFor("p")||(l=CKEDITOR.ENTER_BR,k=1),j.fire("saveSnapshot"),l==CKEDITOR.ENTER_BR?i(j,l,null,k):h(j,l,null,k),j.fire("saveSnapshot"))
}function g(j){j=j.getSelection().getRanges(!0);
for(var k=j.length-1;
0<k;
k--){j[k].deleteContents()
}return j[0]
}function d(j){var k=j.startContainer.getAscendant(function(l){return l.type==CKEDITOR.NODE_ELEMENT&&"true"==l.getAttribute("contenteditable")
},!0);
if(j.root.equals(k)){return j
}k=new CKEDITOR.dom.range(k);
k.moveToRange(j);
return k
}CKEDITOR.plugins.add("enterkey",{init:function(j){j.addCommand("enter",{modes:{wysiwyg:1},editorFocus:!1,exec:function(k){a(k)
}});
j.addCommand("shiftEnter",{modes:{wysiwyg:1},editorFocus:!1,exec:function(k){a(k,k.activeShiftEnterMode,1)
}});
j.setKeystroke([[13,"enter"],[CKEDITOR.SHIFT+13,"shiftEnter"]])
}});
var c=CKEDITOR.dom.walker.whitespaces(),b=CKEDITOR.dom.walker.bookmark();
CKEDITOR.plugins.enterkey={enterBlock:function(D,B,E,x){if(E=E||g(D)){E=d(E);
var z=E.document,w=E.checkStartOfBlock(),u=E.checkEndOfBlock(),v=D.elementPath(E.startContainer),C=v.block,t=B==CKEDITOR.ENTER_DIV?"div":"p",A;
if(w&&u){if(C&&(C.is("li")||C.getParent().is("li"))){C.is("li")||(C=C.getParent());
E=C.getParent();
A=E.getParent();
x=!C.hasPrevious();
var s=!C.hasNext(),t=D.getSelection(),y=t.createBookmarks(),w=C.getDirection(1),u=C.getAttribute("class"),j=C.getAttribute("style"),o=A.getDirection(1)!=w;
D=D.enterMode!=CKEDITOR.ENTER_BR||o||j||u;
if(A.is("li")){x||s?(x&&s&&E.remove(),C[s?"insertAfter":"insertBefore"](A)):C.breakParent(A)
}else{if(D){if(v.block.is("li")?(A=z.createElement(B==CKEDITOR.ENTER_P?"p":"div"),o&&A.setAttribute("dir",w),j&&A.setAttribute("style",j),u&&A.setAttribute("class",u),C.moveChildren(A)):A=v.block,x||s){A[x?"insertBefore":"insertAfter"](E)
}else{C.breakParent(E),A.insertAfter(E)
}}else{if(C.appendBogus(!0),x||s){for(;
z=C[x?"getFirst":"getLast"]();
){z[x?"insertBefore":"insertAfter"](E)
}}else{for(C.breakParent(E);
z=C.getLast();
){z.insertAfter(E)
}}}C.remove()
}t.selectBookmarks(y);
return
}if(C&&C.getParent().is("blockquote")){C.breakParent(C.getParent());
C.getPrevious().getFirst(CKEDITOR.dom.walker.invisible(1))||C.getPrevious().remove();
C.getNext().getFirst(CKEDITOR.dom.walker.invisible(1))||C.getNext().remove();
E.moveToElementEditStart(C);
E.select();
return
}}else{if(C&&C.is("pre")&&!u){i(D,B,E,x);
return
}}if(w=E.splitBlock(t)){B=w.previousBlock;
C=w.nextBlock;
v=w.wasStartOfBlock;
D=w.wasEndOfBlock;
C?(y=C.getParent(),y.is("li")&&(C.breakParent(y),C.move(C.getNext(),1))):B&&(y=B.getParent())&&y.is("li")&&(B.breakParent(y),y=B.getNext(),E.moveToElementEditStart(y),B.move(B.getPrevious()));
if(v||D){if(B){if(B.is("li")||!f.test(B.getName())&&!B.is("pre")){A=B.clone()
}}else{C&&(A=C.clone())
}A?x&&!A.is("li")&&A.renameNode(t):y&&y.is("li")?A=y:(A=z.createElement(t),B&&(s=B.getDirection())&&A.setAttribute("dir",s));
if(z=w.elementPath){for(x=0,t=z.elements.length;
x<t;
x++){y=z.elements[x];
if(y.equals(z.block)||y.equals(z.blockLimit)){break
}CKEDITOR.dtd.$removeEmpty[y.getName()]&&(y=y.clone(),A.moveChildren(y),A.append(y))
}}A.appendBogus();
A.getParent()||E.insertNode(A);
A.is("li")&&A.removeAttribute("value");
!CKEDITOR.env.ie||!v||D&&B.getChildCount()||(E.moveToElementEditStart(D?B:A),E.select());
E.moveToElementEditStart(v&&!D?C:A)
}else{C.is("li")&&(A=E.clone(),A.selectNodeContents(C),A=new CKEDITOR.dom.walker(A),A.evaluator=function(k){return !(b(k)||c(k)||k.type==CKEDITOR.NODE_ELEMENT&&k.getName() in CKEDITOR.dtd.$inline&&!(k.getName() in CKEDITOR.dtd.$empty))
},(y=A.next())&&y.type==CKEDITOR.NODE_ELEMENT&&y.is("ul","ol")&&(CKEDITOR.env.needsBrFiller?z.createElement("br"):z.createText(" ")).insertBefore(y)),C&&E.moveToElementEditStart(C)
}E.select();
E.scrollIntoView()
}}},enterBr:function(t,r,u,p){if(u=u||g(t)){var q=u.document,o=u.checkEndOfBlock(),j=new CKEDITOR.dom.elementPath(t.getSelection().getStartElement()),n=j.block,s=n&&j.block.getName();
p||"li"!=s?(!p&&o&&f.test(s)?(o=n.getDirection())?(q=q.createElement("div"),q.setAttribute("dir",o),q.insertAfter(n),u.setStart(q,0)):(q.createElement("br").insertAfter(n),CKEDITOR.env.gecko&&q.createText("").insertAfter(n),u.setStartAt(n.getNext(),CKEDITOR.env.ie?CKEDITOR.POSITION_BEFORE_START:CKEDITOR.POSITION_AFTER_START)):(t="pre"==s&&CKEDITOR.env.ie&&8>CKEDITOR.env.version?q.createText("\r"):q.createElement("br"),u.deleteContents(),u.insertNode(t),CKEDITOR.env.needsBrFiller?(q.createText("").insertAfter(t),o&&(n||j.blockLimit).appendBogus(),t.getNext().$.nodeValue="",u.setStartAt(t.getNext(),CKEDITOR.POSITION_AFTER_START)):u.setStartAt(t,CKEDITOR.POSITION_AFTER_END)),u.collapse(!0),u.select(),u.scrollIntoView()):h(t,r,u,p)
}}};
var e=CKEDITOR.plugins.enterkey,i=e.enterBr,h=e.enterBlock,f=/^h[1-6]$/
})();
(function(){function a(i,m){var l={},p=[],n={nbsp:" ",shy:"",gt:"\x3e",lt:"\x3c",amp:"\x26",apos:"'",quot:'"'};
i=i.replace(/\b(nbsp|shy|gt|lt|amp|apos|quot)(?:,|$)/g,function(c,e){var f=m?"\x26"+e+";":n[e];
l[f]=m?n[e]:"\x26"+e+";";
p.push(f);
return""
});
if(!m&&i){i=i.split(",");
var j=document.createElement("div"),o;
j.innerHTML="\x26"+i.join(";\x26")+";";
o=j.innerHTML;
j=null;
for(j=0;
j<o.length;
j++){var k=o.charAt(j);
l[k]="\x26"+i[j]+";";
p.push(k)
}}l.regex=p.join(m?"|":"");
return l
}CKEDITOR.plugins.add("entities",{afterInit:function(q){function m(b){return j[b]
}function k(c){return"force"!=p.entities_processNumerical&&r[c]?r[c]:"\x26#"+c.charCodeAt(0)+";"
}var p=q.config;
if(q=(q=q.dataProcessor)&&q.htmlFilter){var n=[];
!1!==p.basicEntities&&n.push("nbsp,gt,lt,amp");
p.entities&&(n.length&&n.push("quot,iexcl,cent,pound,curren,yen,brvbar,sect,uml,copy,ordf,laquo,not,shy,reg,macr,deg,plusmn,sup2,sup3,acute,micro,para,middot,cedil,sup1,ordm,raquo,frac14,frac12,frac34,iquest,times,divide,fnof,bull,hellip,prime,Prime,oline,frasl,weierp,image,real,trade,alefsym,larr,uarr,rarr,darr,harr,crarr,lArr,uArr,rArr,dArr,hArr,forall,part,exist,empty,nabla,isin,notin,ni,prod,sum,minus,lowast,radic,prop,infin,ang,and,or,cap,cup,int,there4,sim,cong,asymp,ne,equiv,le,ge,sub,sup,nsub,sube,supe,oplus,otimes,perp,sdot,lceil,rceil,lfloor,rfloor,lang,rang,loz,spades,clubs,hearts,diams,circ,tilde,ensp,emsp,thinsp,zwnj,zwj,lrm,rlm,ndash,mdash,lsquo,rsquo,sbquo,ldquo,rdquo,bdquo,dagger,Dagger,permil,lsaquo,rsaquo,euro"),p.entities_latin&&n.push("Agrave,Aacute,Acirc,Atilde,Auml,Aring,AElig,Ccedil,Egrave,Eacute,Ecirc,Euml,Igrave,Iacute,Icirc,Iuml,ETH,Ntilde,Ograve,Oacute,Ocirc,Otilde,Ouml,Oslash,Ugrave,Uacute,Ucirc,Uuml,Yacute,THORN,szlig,agrave,aacute,acirc,atilde,auml,aring,aelig,ccedil,egrave,eacute,ecirc,euml,igrave,iacute,icirc,iuml,eth,ntilde,ograve,oacute,ocirc,otilde,ouml,oslash,ugrave,uacute,ucirc,uuml,yacute,thorn,yuml,OElig,oelig,Scaron,scaron,Yuml"),p.entities_greek&&n.push("Alpha,Beta,Gamma,Delta,Epsilon,Zeta,Eta,Theta,Iota,Kappa,Lambda,Mu,Nu,Xi,Omicron,Pi,Rho,Sigma,Tau,Upsilon,Phi,Chi,Psi,Omega,alpha,beta,gamma,delta,epsilon,zeta,eta,theta,iota,kappa,lambda,mu,nu,xi,omicron,pi,rho,sigmaf,sigma,tau,upsilon,phi,chi,psi,omega,thetasym,upsih,piv"),p.entities_additional&&n.push(p.entities_additional));
var r=a(n.join(",")),o=r.regex?"["+r.regex+"]":"a^";
delete r.regex;
p.entities&&p.entities_processNumerical&&(o="[^ -~]|"+o);
var o=new RegExp(o,"g"),j=a("nbsp,gt,lt,amp,shy",!0),i=new RegExp(j.regex,"g");
q.addRules({text:function(b){return b.replace(i,m).replace(o,k)
}},{applyToAll:!0,excludeNestedEditable:!0})
}}})
})();
CKEDITOR.config.basicEntities=!0;
CKEDITOR.config.entities=!0;
CKEDITOR.config.entities_latin=!0;
CKEDITOR.config.entities_greek=!0;
CKEDITOR.config.entities_additional="#39";
CKEDITOR.plugins.add("popup");
CKEDITOR.tools.extend(CKEDITOR.editor.prototype,{popup:function(n,j,i,o){j=j||"80%";
i=i||"70%";
"string"==typeof j&&1<j.length&&"%"==j.substr(j.length-1,1)&&(j=parseInt(window.screen.width*parseInt(j,10)/100,10));
"string"==typeof i&&1<i.length&&"%"==i.substr(i.length-1,1)&&(i=parseInt(window.screen.height*parseInt(i,10)/100,10));
640>j&&(j=640);
420>i&&(i=420);
var m=parseInt((window.screen.height-i)/2,10),l=parseInt((window.screen.width-j)/2,10);
o=(o||"location\x3dno,menubar\x3dno,toolbar\x3dno,dependent\x3dyes,minimizable\x3dno,modal\x3dyes,alwaysRaised\x3dyes,resizable\x3dyes,scrollbars\x3dyes")+",width\x3d"+j+",height\x3d"+i+",top\x3d"+m+",left\x3d"+l;
var p=window.open("",null,o,!0);
if(!p){return !1
}try{-1==navigator.userAgent.toLowerCase().indexOf(" chrome/")&&(p.moveTo(l,m),p.resizeTo(j,i)),p.focus(),p.location.href=n
}catch(k){window.open(n,null,o,!0)
}return !0
}});
(function(){function e(h,l){var k=[];
if(l){for(var g in l){k.push(g+"\x3d"+encodeURIComponent(l[g]))
}}else{return h
}return h+(-1!=h.indexOf("?")?"\x26":"?")+k.join("\x26")
}function c(g){g+="";
return g.charAt(0).toUpperCase()+g.substr(1)
}function a(){var h=this.getDialog(),l=h.getParentEditor();
l._.filebrowserSe=this;
var k=l.config["filebrowser"+c(h.getName())+"WindowWidth"]||l.config.filebrowserWindowWidth||"80%",h=l.config["filebrowser"+c(h.getName())+"WindowHeight"]||l.config.filebrowserWindowHeight||"70%",g=this.filebrowser.params||{};
g.CKEditor=l.name;
g.CKEditorFuncNum=l._.filebrowserFn;
g.langCode||(g.langCode=l.langCode);
g=e(this.filebrowser.url,g);
l.popup(g,k,h,l.config.filebrowserWindowFeatures||l.config.fileBrowserWindowFeatures)
}function j(){var g=this.getDialog();
g.getParentEditor()._.filebrowserSe=this;
return g.getContentElement(this["for"][0],this["for"][1]).getInputElement().$.value&&g.getContentElement(this["for"][0],this["for"][1]).getAction()?!0:!1
}function i(h,l,k){var g=k.params||{};
g.CKEditor=h.name;
g.CKEditorFuncNum=h._.filebrowserFn;
g.langCode||(g.langCode=h.langCode);
l.action=e(k.url,g);
l.filebrowser=k
}function b(l,r,q,k){if(k&&k.length){for(var p,n=k.length;
n--;
){if(p=k[n],"hbox"!=p.type&&"vbox"!=p.type&&"fieldset"!=p.type||b(l,r,q,p.children),p.filebrowser){if("string"==typeof p.filebrowser&&(p.filebrowser={action:"fileButton"==p.type?"QuickUpload":"Browse",target:p.filebrowser}),"Browse"==p.filebrowser.action){var o=p.filebrowser.url;
void 0===o&&(o=l.config["filebrowser"+c(r)+"BrowseUrl"],void 0===o&&(o=l.config.filebrowserBrowseUrl));
o&&(p.onClick=a,p.filebrowser.url=o,p.hidden=!1)
}else{if("QuickUpload"==p.filebrowser.action&&p["for"]&&(o=p.filebrowser.url,void 0===o&&(o=l.config["filebrowser"+c(r)+"UploadUrl"],void 0===o&&(o=l.config.filebrowserUploadUrl)),o)){var m=p.onClick;
p.onClick=function(h){var g=h.sender;
return m&&!1===m.call(g,h)?!1:j.call(g,h)
};
p.filebrowser.url=o;
p.hidden=!1;
i(l,q.getContents(p["for"][0]).get(p["for"][1]),p.filebrowser)
}}}}}}function d(h,l,k){if(-1!==k.indexOf(";")){k=k.split(";");
for(var g=0;
g<k.length;
g++){if(d(h,l,k[g])){return !0
}}return !1
}return(h=h.getContents(l).get(k).filebrowser)&&h.url
}function f(h,m){var l=this._.filebrowserSe.getDialog(),g=this._.filebrowserSe["for"],k=this._.filebrowserSe.filebrowser.onSelect;
g&&l.getContentElement(g[0],g[1]).reset();
if("function"!=typeof m||!1!==m.call(this._.filebrowserSe)){if(!k||!1!==k.call(this._.filebrowserSe,h,m)){if("string"==typeof m&&m&&alert(m),h&&(g=this._.filebrowserSe,l=g.getDialog(),g=g.filebrowser.target||null)){if(g=g.split(":"),k=l.getContentElement(g[0],g[1])){k.setValue(h),l.selectPage(g[0])
}}}}}CKEDITOR.plugins.add("filebrowser",{requires:"popup",init:function(g){g._.filebrowserFn=CKEDITOR.tools.addFunction(f,g);
g.on("destroy",function(){CKEDITOR.tools.removeFunction(this._.filebrowserFn)
})
}});
CKEDITOR.on("dialogDefinition",function(h){if(h.editor.plugins.filebrowser){for(var l=h.data.definition,k,g=0;
g<l.contents.length;
++g){if(k=l.contents[g]){b(h.editor,h.data.name,l,k.elements),k.hidden&&k.filebrowser&&(k.hidden=!d(l,k.id,k.filebrowser))
}}}})
})();
CKEDITOR.plugins.add("find",{requires:"dialog",init:function(d){var c=d.addCommand("find",new CKEDITOR.dialogCommand("find"));
c.canUndo=!1;
c.readOnly=1;
d.addCommand("replace",new CKEDITOR.dialogCommand("replace")).canUndo=!1;
d.ui.addButton&&(d.ui.addButton("Find",{label:d.lang.find.find,command:"find",toolbar:"find,10"}),d.ui.addButton("Replace",{label:d.lang.find.replace,command:"replace",toolbar:"find,20"}));
CKEDITOR.dialog.add("find",this.path+"dialogs/find.js");
CKEDITOR.dialog.add("replace",this.path+"dialogs/find.js")
}});
CKEDITOR.config.find_highlight={element:"span",styles:{"background-color":"#004",color:"#fff"}};
(function(){function e(g,f){var i=b.exec(g),h=b.exec(f);
if(i){if(!i[2]&&"px"==h[2]){return h[1]
}if("px"==i[2]&&!h[2]){return h[1]+"px"
}}return f
}var c=CKEDITOR.htmlParser.cssStyle,d=CKEDITOR.tools.cssLength,b=/^((?:\d*(?:\.\d+))|(?:\d+))(.*)?$/i,a={elements:{$:function(g){var f=g.attributes;
if((f=(f=(f=f&&f["data-cke-realelement"])&&new CKEDITOR.htmlParser.fragment.fromHtml(decodeURIComponent(f)))&&f.children[0])&&g.attributes["data-cke-resizable"]){var i=(new c(g)).rules;
g=f.attributes;
var h=i.width,i=i.height;
h&&(g.width=e(g.width,h));
i&&(g.height=e(g.height,i))
}return f
}}};
CKEDITOR.plugins.add("fakeobjects",{init:function(f){f.filter.allow("img[!data-cke-realelement,src,alt,title](*){*}","fakeobjects")
},afterInit:function(f){(f=(f=f.dataProcessor)&&f.htmlFilter)&&f.addRules(a,{applyToAll:!0})
}});
CKEDITOR.editor.prototype.createFakeElement=function(g,f,j,i){var h=this.lang.fakeobjects,h=h[j]||h.unknown;
f={"class":f,"data-cke-realelement":encodeURIComponent(g.getOuterHtml()),"data-cke-real-node-type":g.type,alt:h,title:h,align:g.getAttribute("align")||""};
CKEDITOR.env.hc||(f.src=CKEDITOR.tools.transparentImageData);
j&&(f["data-cke-real-element-type"]=j);
i&&(f["data-cke-resizable"]=i,j=new c,i=g.getAttribute("width"),g=g.getAttribute("height"),i&&(j.rules.width=d(i)),g&&(j.rules.height=d(g)),j.populate(f));
return this.document.createElement("img",{attributes:f})
};
CKEDITOR.editor.prototype.createFakeParserElement=function(h,g,l,k){var j=this.lang.fakeobjects,j=j[l]||j.unknown,i;
i=new CKEDITOR.htmlParser.basicWriter;
h.writeHtml(i);
i=i.getHtml();
g={"class":g,"data-cke-realelement":encodeURIComponent(i),"data-cke-real-node-type":h.type,alt:j,title:j,align:h.attributes.align||""};
CKEDITOR.env.hc||(g.src=CKEDITOR.tools.transparentImageData);
l&&(g["data-cke-real-element-type"]=l);
k&&(g["data-cke-resizable"]=k,k=h.attributes,h=new c,l=k.width,k=k.height,void 0!==l&&(h.rules.width=d(l)),void 0!==k&&(h.rules.height=d(k)),h.populate(g));
return new CKEDITOR.htmlParser.element("img",g)
};
CKEDITOR.editor.prototype.restoreRealElement=function(g){if(g.data("cke-real-node-type")!=CKEDITOR.NODE_ELEMENT){return null
}var f=CKEDITOR.dom.element.createFromHtml(decodeURIComponent(g.data("cke-realelement")),this.document);
if(g.data("cke-resizable")){var h=g.getStyle("width");
g=g.getStyle("height");
h&&f.setAttribute("width",e(f.getAttribute("width"),h));
g&&f.setAttribute("height",e(f.getAttribute("height"),g))
}return f
}
})();
(function(){function c(d){d=d.attributes;
return"application/x-shockwave-flash"==d.type||a.test(d.src||"")
}function b(e,d){return e.createFakeParserElement(d,"cke_flash","flash",!0)
}var a=/\.swf(?:$|\?)/i;
CKEDITOR.plugins.add("flash",{requires:"dialog,fakeobjects",onLoad:function(){CKEDITOR.addCss("img.cke_flash{background-image: url("+CKEDITOR.getUrl(this.path+"images/placeholder.png")+");background-position: center center;background-repeat: no-repeat;border: 1px solid #a9a9a9;width: 80px;height: 80px;}")
},init:function(e){var d="object[classid,codebase,height,hspace,vspace,width];param[name,value];embed[height,hspace,pluginspage,src,type,vspace,width]";
CKEDITOR.dialog.isTabEnabled(e,"flash","properties")&&(d+=";object[align]; embed[allowscriptaccess,quality,scale,wmode]");
CKEDITOR.dialog.isTabEnabled(e,"flash","advanced")&&(d+=";object[id]{*}; embed[bgcolor]{*}(*)");
e.addCommand("flash",new CKEDITOR.dialogCommand("flash",{allowedContent:d,requiredContent:"embed"}));
e.ui.addButton&&e.ui.addButton("Flash",{label:e.lang.common.flash,command:"flash",toolbar:"insert,20"});
CKEDITOR.dialog.add("flash",this.path+"dialogs/flash.js");
e.addMenuItems&&e.addMenuItems({flash:{label:e.lang.flash.properties,command:"flash",group:"flash"}});
e.on("doubleclick",function(g){var f=g.data.element;
f.is("img")&&"flash"==f.data("cke-real-element-type")&&(g.data.dialog="flash")
});
e.contextMenu&&e.contextMenu.addListener(function(f){if(f&&f.is("img")&&!f.isReadOnly()&&"flash"==f.data("cke-real-element-type")){return{flash:CKEDITOR.TRISTATE_OFF}
}})
},afterInit:function(e){var d=e.dataProcessor;
(d=d&&d.dataFilter)&&d.addRules({elements:{"cke:object":function(f){var g=f.attributes;
if(!(g.classid&&String(g.classid).toLowerCase()||c(f))){for(g=0;
g<f.children.length;
g++){if("cke:embed"==f.children[g].name){if(!c(f.children[g])){break
}return b(e,f)
}}return null
}return b(e,f)
},"cke:embed":function(f){return c(f)?b(e,f):null
}}},5)
}})
})();
CKEDITOR.tools.extend(CKEDITOR.config,{flashEmbedTagOnly:!1,flashAddEmbedTag:!0,flashConvertOnEdit:!1});
(function(){function b(g){var f=g.config,n=g.fire("uiSpace",{space:"top",html:""}).html,j=function(){function A(k,q,p){d.setStyle(q,a(p));
d.setStyle("position",k)
}function B(k){var e=y.getDocumentPosition();
switch(k){case"top":A("absolute","top",e.y-o-l);
break;
case"pin":A("fixed","top",D);
break;
case"bottom":A("absolute","top",e.y+(C.height||C.bottom-C.top)+l)
}w=k
}var w,y,t,C,z,o,E,s=f.floatSpaceDockedOffsetX||0,l=f.floatSpaceDockedOffsetY||0,F=f.floatSpacePinnedOffsetX||0,D=f.floatSpacePinnedOffsetY||0;
return function(k){if(y=g.editable()){var e=k&&"focus"==k.name;
e&&d.show();
g.fire("floatingSpaceLayout",{show:e});
d.removeStyle("left");
d.removeStyle("right");
t=d.getClientRect();
C=y.getClientRect();
z=c.getViewPaneSize();
o=t.height;
E="pageXOffset" in c.$?c.$.pageXOffset:CKEDITOR.document.$.documentElement.scrollLeft;
w?(o+l<=C.top?B("top"):o+l>z.height-C.bottom?B("pin"):B("bottom"),k=z.width/2,k=f.floatSpacePreferRight?"right":0<C.left&&C.right<z.width&&C.width>t.width?"rtl"==f.contentsLangDirection?"right":"left":k-C.left>C.right-k?"left":"right",t.width>z.width?(k="left",e=0):(e="left"==k?0<C.left?C.left:0:C.right<z.width?z.width-C.right:0,e+t.width>z.width&&(k="left"==k?"right":"left",e=0)),d.setStyle(k,a(("pin"==w?F:s)+e+("pin"==w?0:"left"==k?E:-E)))):(w="pin",B("pin"),j(k))
}}
}();
if(n){var h=new CKEDITOR.template('\x3cdiv id\x3d"cke_{name}" class\x3d"cke {id} cke_reset_all cke_chrome cke_editor_{name} cke_float cke_{langDir} '+CKEDITOR.env.cssClass+'" dir\x3d"{langDir}" title\x3d"'+(CKEDITOR.env.gecko?" ":"")+'" lang\x3d"{langCode}" role\x3d"application" style\x3d"{style}"'+(g.title?' aria-labelledby\x3d"cke_{name}_arialbl"':" ")+"\x3e"+(g.title?'\x3cspan id\x3d"cke_{name}_arialbl" class\x3d"cke_voice_label"\x3e{voiceLabel}\x3c/span\x3e':" ")+'\x3cdiv class\x3d"cke_inner"\x3e\x3cdiv id\x3d"{topId}" class\x3d"cke_top" role\x3d"presentation"\x3e{content}\x3c/div\x3e\x3c/div\x3e\x3c/div\x3e'),d=CKEDITOR.document.getBody().append(CKEDITOR.dom.element.createFromHtml(h.output({content:n,id:g.id,langDir:g.lang.dir,langCode:g.langCode,name:g.name,style:"display:none;z-index:"+(f.baseFloatZIndex-1),topId:g.ui.spaceId("top"),voiceLabel:g.title}))),i=CKEDITOR.tools.eventsBuffer(500,j),m=CKEDITOR.tools.eventsBuffer(100,j);
d.unselectable();
d.on("mousedown",function(e){e=e.data;
e.getTarget().hasAscendant("a",1)||e.preventDefault()
});
g.on("focus",function(e){j(e);
g.on("change",i.input);
c.on("scroll",m.input);
c.on("resize",m.input)
});
g.on("blur",function(){d.hide();
g.removeListener("change",i.input);
c.removeListener("scroll",m.input);
c.removeListener("resize",m.input)
});
g.on("destroy",function(){c.removeListener("scroll",m.input);
c.removeListener("resize",m.input);
d.clearCustomData();
d.remove()
});
g.focusManager.hasFocus&&d.show();
g.focusManager.add(d,1)
}}var c=CKEDITOR.document.getWindow(),a=CKEDITOR.tools.cssLength;
CKEDITOR.plugins.add("floatingspace",{init:function(d){d.on("loaded",function(){b(this)
},null,null,20)
}})
})();
CKEDITOR.plugins.add("listblock",{requires:"panel",onLoad:function(){var d=CKEDITOR.addTemplate("panel-list",'\x3cul role\x3d"presentation" class\x3d"cke_panel_list"\x3e{items}\x3c/ul\x3e'),c=CKEDITOR.addTemplate("panel-list-item",'\x3cli id\x3d"{id}" class\x3d"cke_panel_listItem" role\x3dpresentation\x3e\x3ca id\x3d"{id}_option" _cke_focus\x3d1 hidefocus\x3dtrue title\x3d"{title}" href\x3d"javascript:void(\'{val}\')"  {onclick}\x3d"CKEDITOR.tools.callFunction({clickFn},\'{val}\'); return false;" role\x3d"option"\x3e{text}\x3c/a\x3e\x3c/li\x3e'),b=CKEDITOR.addTemplate("panel-list-group",'\x3ch1 id\x3d"{id}" class\x3d"cke_panel_grouptitle" role\x3d"presentation" \x3e{label}\x3c/h1\x3e'),a=/\'/g;
CKEDITOR.ui.panel.prototype.addListBlock=function(f,e){return this.addBlock(f,new CKEDITOR.ui.listBlock(this.getHolderElement(),e))
};
CKEDITOR.ui.listBlock=CKEDITOR.tools.createClass({base:CKEDITOR.ui.panel.block,$:function(f,e){e=e||{};
var g=e.attributes||(e.attributes={});
(this.multiSelect=!!e.multiSelect)&&(g["aria-multiselectable"]=!0);
!g.role&&(g.role="listbox");
this.base.apply(this,arguments);
this.element.setAttribute("role",g.role);
g=this.keys;
g[40]="next";
g[9]="next";
g[38]="prev";
g[CKEDITOR.SHIFT+9]="prev";
g[32]=CKEDITOR.env.ie?"mouseup":"click";
CKEDITOR.env.ie&&(g[13]="mouseup");
this._.pendingHtml=[];
this._.pendingList=[];
this._.items={};
this._.groups={}
},_:{close:function(){if(this._.started){var e=d.output({items:this._.pendingList.join("")});
this._.pendingList=[];
this._.pendingHtml.push(e);
delete this._.started
}},getClick:function(){this._.click||(this._.click=CKEDITOR.tools.addFunction(function(f){var e=this.toggle(f);
if(this.onClick){this.onClick(f,e)
}},this));
return this._.click
}},proto:{add:function(g,f,j){var i=CKEDITOR.tools.getNextId();
this._.started||(this._.started=1,this._.size=this._.size||0);
this._.items[g]=i;
var h;
h=CKEDITOR.tools.htmlEncodeAttr(g).replace(a,"\\'");
g={id:i,val:h,onclick:CKEDITOR.env.ie?'onclick\x3d"return false;" onmouseup':"onclick",clickFn:this._.getClick(),title:CKEDITOR.tools.htmlEncodeAttr(j||g),text:f||g};
this._.pendingList.push(c.output(g))
},startGroup:function(f){this._.close();
var e=CKEDITOR.tools.getNextId();
this._.groups[f]=e;
this._.pendingHtml.push(b.output({id:e,label:f}))
},commit:function(){this._.close();
this.element.appendHtml(this._.pendingHtml.join(""));
delete this._.size;
this._.pendingHtml=[]
},toggle:function(f){var e=this.isMarked(f);
e?this.unmark(f):this.mark(f);
return !e
},hideGroup:function(f){var e=(f=this.element.getDocument().getById(this._.groups[f]))&&f.getNext();
f&&(f.setStyle("display","none"),e&&"ul"==e.getName()&&e.setStyle("display","none"))
},hideItem:function(e){this.element.getDocument().getById(this._.items[e]).setStyle("display","none")
},showAll:function(){var g=this._.items,f=this._.groups,j=this.element.getDocument(),i;
for(i in g){j.getById(g[i]).setStyle("display","")
}for(var h in f){g=j.getById(f[h]),i=g.getNext(),g.setStyle("display",""),i&&"ul"==i.getName()&&i.setStyle("display","")
}},mark:function(f){this.multiSelect||this.unmarkAll();
f=this._.items[f];
var e=this.element.getDocument().getById(f);
e.addClass("cke_selected");
this.element.getDocument().getById(f+"_option").setAttribute("aria-selected",!0);
this.onMark&&this.onMark(e)
},unmark:function(f){var e=this.element.getDocument();
f=this._.items[f];
var g=e.getById(f);
g.removeClass("cke_selected");
e.getById(f+"_option").removeAttribute("aria-selected");
this.onUnmark&&this.onUnmark(g)
},unmarkAll:function(){var f=this._.items,e=this.element.getDocument(),h;
for(h in f){var g=f[h];
e.getById(g).removeClass("cke_selected");
e.getById(g+"_option").removeAttribute("aria-selected")
}this.onUnmark&&this.onUnmark()
},isMarked:function(e){return this.element.getDocument().getById(this._.items[e]).hasClass("cke_selected")
},focus:function(f){this._.focusIndex=-1;
var e=this.element.getElementsByTag("a"),h,g=-1;
if(f){for(h=this.element.getDocument().getById(this._.items[f]).getFirst();
f=e.getItem(++g);
){if(f.equals(h)){this._.focusIndex=g;
break
}}}else{this.element.focus()
}h&&setTimeout(function(){h.focus()
},0)
}}})
}});
CKEDITOR.plugins.add("richcombo",{requires:"floatpanel,listblock,button",beforeInit:function(a){a.ui.addHandler(CKEDITOR.UI_RICHCOMBO,CKEDITOR.ui.richCombo.handler)
}});
(function(){var b='\x3cspan id\x3d"{id}" class\x3d"cke_combo cke_combo__{name} {cls}" role\x3d"presentation"\x3e\x3cspan id\x3d"{id}_label" class\x3d"cke_combo_label"\x3e{label}\x3c/span\x3e\x3ca class\x3d"cke_combo_button" title\x3d"{title}" tabindex\x3d"-1"'+(CKEDITOR.env.gecko&&!CKEDITOR.env.hc?"":" href\x3d\"javascript:void('{titleJs}')\"")+' hidefocus\x3d"true" role\x3d"button" aria-labelledby\x3d"{id}_label" aria-haspopup\x3d"true"';
CKEDITOR.env.gecko&&CKEDITOR.env.mac&&(b+=' onkeypress\x3d"return false;"');
CKEDITOR.env.gecko&&(b+=' onblur\x3d"this.style.cssText \x3d this.style.cssText;"');
var b=b+(' onkeydown\x3d"return CKEDITOR.tools.callFunction({keydownFn},event,this);" onfocus\x3d"return CKEDITOR.tools.callFunction({focusFn},event);" '+(CKEDITOR.env.ie?'onclick\x3d"return false;" onmouseup':"onclick")+'\x3d"CKEDITOR.tools.callFunction({clickFn},this);return false;"\x3e\x3cspan id\x3d"{id}_text" class\x3d"cke_combo_text cke_combo_inlinelabel"\x3e{label}\x3c/span\x3e\x3cspan class\x3d"cke_combo_open"\x3e\x3cspan class\x3d"cke_combo_arrow"\x3e'+(CKEDITOR.env.hc?"\x26#9660;":CKEDITOR.env.air?"\x26nbsp;":"")+"\x3c/span\x3e\x3c/span\x3e\x3c/a\x3e\x3c/span\x3e"),a=CKEDITOR.addTemplate("combo",b);
CKEDITOR.UI_RICHCOMBO="richcombo";
CKEDITOR.ui.richCombo=CKEDITOR.tools.createClass({$:function(c){CKEDITOR.tools.extend(this,c,{canGroup:!1,title:c.label,modes:{wysiwyg:1},editorFocus:1});
c=this.panel||{};
delete this.panel;
this.id=CKEDITOR.tools.getNextNumber();
this.document=c.parent&&c.parent.getDocument()||CKEDITOR.document;
c.className="cke_combopanel";
c.block={multiSelect:c.multiSelect,attributes:c.attributes};
c.toolbarRelated=!0;
this._={panelDefinition:c,items:{}}
},proto:{renderHtml:function(d){var c=[];
this.render(d,c);
return c.join("")
},render:function(v,u){function p(){if(this.getState()!=CKEDITOR.TRISTATE_ON){var d=this.modes[v.mode]?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED;
v.readOnly&&!this.readOnly&&(d=CKEDITOR.TRISTATE_DISABLED);
this.setState(d);
this.setValue("");
d!=CKEDITOR.TRISTATE_DISABLED&&this.refresh&&this.refresh()
}}var s=CKEDITOR.env,o="cke_"+this.id,r=CKEDITOR.tools.addFunction(function(c){k&&(v.unlockSelection(1),k=0);
t.execute(c)
},this),q=this,t={id:o,combo:this,focus:function(){CKEDITOR.document.getById(o).getChild(1).focus()
},execute:function(g){var e=q._;
if(e.state!=CKEDITOR.TRISTATE_DISABLED){if(q.createPanel(v),e.on){e.panel.hide()
}else{q.commit();
var f=q.getValue();
f?e.list.mark(f):e.list.unmarkAll();
e.panel.showBlock(q.id,new CKEDITOR.dom.element(g),4)
}}},clickFn:r};
v.on("activeFilterChange",p,this);
v.on("mode",p,this);
v.on("selectionChange",p,this);
!this.readOnly&&v.on("readOnly",p,this);
var j=CKEDITOR.tools.addFunction(function(c,f){c=new CKEDITOR.dom.event(c);
var e=c.getKeystroke();
if(40==e){v.once("panelShow",function(d){d.data._.panel._.currentBlock.onKeyDown(40)
})
}switch(e){case 13:case 32:case 40:CKEDITOR.tools.callFunction(r,f);
break;
default:t.onkey(t,e)
}c.preventDefault()
}),i=CKEDITOR.tools.addFunction(function(){t.onfocus&&t.onfocus()
}),k=0;
t.keyDownFn=j;
s={id:o,name:this.name||this.command,label:this.label,title:this.title,cls:this.className||"",titleJs:s.gecko&&!s.hc?"":(this.title||"").replace("'",""),keydownFn:j,focusFn:i,clickFn:r};
a.output(s,u);
if(this.onRender){this.onRender()
}return t
},createPanel:function(i){if(!this._.panel){var g=this._.panelDefinition,o=this._.panelDefinition.block,j=g.parent||CKEDITOR.document.getBody(),l="cke_combopanel__"+this.name,n=new CKEDITOR.ui.floatPanel(i,j,g),m=n.addListBlock(this.id,o),p=this;
n.onShow=function(){this.element.addClass(l);
p.setState(CKEDITOR.TRISTATE_ON);
p._.on=1;
p.editorFocus&&!i.focusManager.hasFocus&&i.focus();
if(p.onOpen){p.onOpen()
}i.once("panelShow",function(){m.focus(!m.multiSelect&&p.getValue())
})
};
n.onHide=function(c){this.element.removeClass(l);
p.setState(p.modes&&p.modes[i.mode]?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED);
p._.on=0;
if(!c&&p.onClose){p.onClose()
}};
n.onEscape=function(){n.hide(1)
};
m.onClick=function(d,c){p.onClick&&p.onClick.call(p,d,c);
n.hide()
};
this._.panel=n;
this._.list=m;
n.getBlock(this.id).onHide=function(){p._.on=0;
p.setState(CKEDITOR.TRISTATE_OFF)
};
this.init&&this.init()
}},setValue:function(e,c){this._.value=e;
var f=this.document.getById("cke_"+this.id+"_text");
f&&(e||c?f.removeClass("cke_combo_inlinelabel"):(c=this.label,f.addClass("cke_combo_inlinelabel")),f.setText("undefined"!=typeof c?c:e))
},getValue:function(){return this._.value||""
},unmarkAll:function(){this._.list.unmarkAll()
},mark:function(c){this._.list.mark(c)
},hideItem:function(c){this._.list.hideItem(c)
},hideGroup:function(c){this._.list.hideGroup(c)
},showAll:function(){this._.list.showAll()
},add:function(e,c,f){this._.items[e]=f||e;
this._.list.add(e,c,f)
},startGroup:function(c){this._.list.startGroup(c)
},commit:function(){this._.committed||(this._.list.commit(),this._.committed=1,CKEDITOR.ui.fire("ready",this));
this._.committed=1
},setState:function(d){if(this._.state!=d){var c=this.document.getById("cke_"+this.id);
c.setState(d,"cke_combo");
d==CKEDITOR.TRISTATE_DISABLED?c.setAttribute("aria-disabled",!0):c.removeAttribute("aria-disabled");
this._.state=d
}},getState:function(){return this._.state
},enable:function(){this._.state==CKEDITOR.TRISTATE_DISABLED&&this.setState(this._.lastState)
},disable:function(){this._.state!=CKEDITOR.TRISTATE_DISABLED&&(this._.lastState=this._.state,this.setState(CKEDITOR.TRISTATE_DISABLED))
}},statics:{handler:{create:function(c){return new CKEDITOR.ui.richCombo(c)
}}}});
CKEDITOR.ui.prototype.addRichCombo=function(d,c){this.add(d,CKEDITOR.UI_RICHCOMBO,c)
}
})();
(function(){function a(B,A,z,x,g,o,F,D){var C=B.config,e=new CKEDITOR.style(F),r=g.split(";");
g=[];
for(var j={},y=0;
y<r.length;
y++){var s=r[y];
if(s){var s=s.split("/"),E={},i=r[y]=s[0];
E[z]=g[y]=s[1]||i;
j[i]=new CKEDITOR.style(F,E);
j[i]._.definition.name=i
}else{r.splice(y--,1)
}}B.ui.addRichCombo(A,{label:x.label,title:x.panelTitle,toolbar:"styles,"+D,allowedContent:e,requiredContent:e,panel:{css:[CKEDITOR.skin.getPath("editor")].concat(C.contentsCss),multiSelect:!1,attributes:{"aria-label":x.panelTitle}},init:function(){this.startGroup(x.panelTitle);
for(var d=0;
d<r.length;
d++){var c=r[d];
this.add(c,j[c].buildPreview(),c)
}},onClick:function(G){B.focus();
B.fire("saveSnapshot");
var w=this.getValue(),t=j[G];
if(w&&G!=w){var n=j[w],u=B.getSelection().getRanges()[0];
if(u.collapsed){var v=B.elementPath(),q=v.contains(function(c){return n.checkElementRemovable(c)
});
if(q){var p=u.checkBoundaryOfElement(q,CKEDITOR.START),m=u.checkBoundaryOfElement(q,CKEDITOR.END);
if(p&&m){for(p=u.createBookmark();
v=q.getFirst();
){v.insertBefore(q)
}q.remove();
u.moveToBookmark(p)
}else{p?u.moveToPosition(q,CKEDITOR.POSITION_BEFORE_START):m?u.moveToPosition(q,CKEDITOR.POSITION_AFTER_END):(u.splitElement(q),u.moveToPosition(q,CKEDITOR.POSITION_AFTER_END),b(u,v.elements.slice(),q))
}B.getSelection().selectRanges([u])
}}else{B.removeStyle(n)
}}B[w==G?"removeStyle":"applyStyle"](t);
B.fire("saveSnapshot")
},onRender:function(){B.on("selectionChange",function(h){var n=this.getValue();
h=h.data.path.elements;
for(var m=0,k;
m<h.length;
m++){k=h[m];
for(var l in j){if(j[l].checkElementMatch(k,!0,B)){l!=n&&this.setValue(l);
return
}}}this.setValue("",o)
},this)
},refresh:function(){B.activeFilter.check(e)||this.setState(CKEDITOR.TRISTATE_DISABLED)
}})
}function b(e,d,h){var g=d.pop();
if(g){if(h){return b(e,d,g.equals(h)?null:h)
}h=g.clone();
e.insertNode(h);
e.moveToPosition(h,CKEDITOR.POSITION_AFTER_START);
b(e,d)
}}CKEDITOR.plugins.add("font",{requires:"richcombo",init:function(d){var c=d.config;
a(d,"Font","family",d.lang.font,c.font_names,c.font_defaultLabel,c.font_style,30);
a(d,"FontSize","size",d.lang.font.fontSize,c.fontSize_sizes,c.fontSize_defaultLabel,c.fontSize_style,40)
}})
})();
CKEDITOR.config.font_names="Arial/Arial, Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif";
CKEDITOR.config.font_defaultLabel="";
CKEDITOR.config.font_style={element:"span",styles:{"font-family":"#(family)"},overrides:[{element:"font",attributes:{face:null}}]};
CKEDITOR.config.fontSize_sizes="8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;72/72px";
CKEDITOR.config.fontSize_defaultLabel="";
CKEDITOR.config.fontSize_style={element:"span",styles:{"font-size":"#(size)"},overrides:[{element:"font",attributes:{size:null}}]};
CKEDITOR.plugins.add("forms",{requires:"dialog,fakeobjects",onLoad:function(){CKEDITOR.addCss(".cke_editable form{border: 1px dotted #FF0000;padding: 2px;}\n");
CKEDITOR.addCss("img.cke_hidden{background-image: url("+CKEDITOR.getUrl(this.path+"images/hiddenfield.gif")+");background-position: center center;background-repeat: no-repeat;border: 1px solid #a9a9a9;width: 16px !important;height: 16px !important;}")
},init:function(r){var q=r.lang,n=0,j={email:1,password:1,search:1,tel:1,text:1,url:1},d={checkbox:"input[type,name,checked,required]",radio:"input[type,name,checked,required]",textfield:"input[type,name,value,size,maxlength,required]",textarea:"textarea[cols,rows,name,required]",select:"select[name,size,multiple,required]; option[value,selected]",button:"input[type,name,value]",form:"form[action,name,id,enctype,target,method]",hiddenfield:"input[type,name,value]",imagebutton:"input[type,alt,src]{width,height,border,border-width,border-style,margin,float}"},c={checkbox:"input",radio:"input",textfield:"input",textarea:"textarea",select:"select",button:"input",form:"form",hiddenfield:"input",imagebutton:"input"},p=function(f,g,b){var a={allowedContent:d[g],requiredContent:c[g]};
"form"==g&&(a.context="form");
r.addCommand(g,new CKEDITOR.dialogCommand(g,a));
r.ui.addButton&&r.ui.addButton(f,{label:q.common[f.charAt(0).toLowerCase()+f.slice(1)],command:g,toolbar:"forms,"+(n+=10)});
CKEDITOR.dialog.add(g,b)
},o=this.path+"dialogs/";
!r.blockless&&p("Form","form",o+"form.js");
p("Checkbox","checkbox",o+"checkbox.js");
p("Radio","radio",o+"radio.js");
p("TextField","textfield",o+"textfield.js");
p("Textarea","textarea",o+"textarea.js");
p("Select","select",o+"select.js");
p("Button","button",o+"button.js");
var i=r.plugins.image;
i&&!r.plugins.image2&&p("ImageButton","imagebutton",CKEDITOR.plugins.getPath("image")+"dialogs/image.js");
p("HiddenField","hiddenfield",o+"hiddenfield.js");
r.addMenuItems&&(p={checkbox:{label:q.forms.checkboxAndRadio.checkboxTitle,command:"checkbox",group:"checkbox"},radio:{label:q.forms.checkboxAndRadio.radioTitle,command:"radio",group:"radio"},textfield:{label:q.forms.textfield.title,command:"textfield",group:"textfield"},hiddenfield:{label:q.forms.hidden.title,command:"hiddenfield",group:"hiddenfield"},button:{label:q.forms.button.title,command:"button",group:"button"},select:{label:q.forms.select.title,command:"select",group:"select"},textarea:{label:q.forms.textarea.title,command:"textarea",group:"textarea"}},i&&(p.imagebutton={label:q.image.titleButton,command:"imagebutton",group:"imagebutton"}),!r.blockless&&(p.form={label:q.forms.form.menu,command:"form",group:"form"}),r.addMenuItems(p));
r.contextMenu&&(!r.blockless&&r.contextMenu.addListener(function(e,f,b){if((e=b.contains("form",1))&&!e.isReadOnly()){return{form:CKEDITOR.TRISTATE_OFF}
}}),r.contextMenu.addListener(function(e){if(e&&!e.isReadOnly()){var f=e.getName();
if("select"==f){return{select:CKEDITOR.TRISTATE_OFF}
}if("textarea"==f){return{textarea:CKEDITOR.TRISTATE_OFF}
}if("input"==f){var b=e.getAttribute("type")||"text";
switch(b){case"button":case"submit":case"reset":return{button:CKEDITOR.TRISTATE_OFF};
case"checkbox":return{checkbox:CKEDITOR.TRISTATE_OFF};
case"radio":return{radio:CKEDITOR.TRISTATE_OFF};
case"image":return i?{imagebutton:CKEDITOR.TRISTATE_OFF}:null
}if(j[b]){return{textfield:CKEDITOR.TRISTATE_OFF}
}}if("img"==f&&"hiddenfield"==e.data("cke-real-element-type")){return{hiddenfield:CKEDITOR.TRISTATE_OFF}
}}}));
r.on("doubleclick",function(a){var b=a.data.element;
if(!r.blockless&&b.is("form")){a.data.dialog="form"
}else{if(b.is("select")){a.data.dialog="select"
}else{if(b.is("textarea")){a.data.dialog="textarea"
}else{if(b.is("img")&&"hiddenfield"==b.data("cke-real-element-type")){a.data.dialog="hiddenfield"
}else{if(b.is("input")){b=b.getAttribute("type")||"text";
switch(b){case"button":case"submit":case"reset":a.data.dialog="button";
break;
case"checkbox":a.data.dialog="checkbox";
break;
case"radio":a.data.dialog="radio";
break;
case"image":a.data.dialog="imagebutton"
}j[b]&&(a.data.dialog="textfield")
}}}}}})
},afterInit:function(d){var c=d.dataProcessor,e=c&&c.htmlFilter,c=c&&c.dataFilter;
CKEDITOR.env.ie&&e&&e.addRules({elements:{input:function(g){g=g.attributes;
var f=g.type;
f||(g.type="text");
"checkbox"!=f&&"radio"!=f||"on"!=g.value||delete g.value
}}},{applyToAll:!0});
c&&c.addRules({elements:{input:function(a){if("hidden"==a.attributes.type){return d.createFakeParserElement(a,"cke_hidden","hiddenfield")
}}}},{applyToAll:!0})
}});
CKEDITOR.plugins.add("format",{requires:"richcombo",init:function(t){if(!t.blockless){for(var q=t.config,s=t.lang.format,i=q.format_tags.split(";"),r={},e=0,b=[],p=0;
p<i.length;
p++){var o=i[p],j=new CKEDITOR.style(q["format_"+o]);
if(!t.filter.customConfig||t.filter.check(j)){e++,r[o]=j,r[o]._.enterMode=t.config.enterMode,b.push(j)
}}0!==e&&t.ui.addRichCombo("Format",{label:s.label,title:s.panelTitle,toolbar:"styles,20",allowedContent:b,panel:{css:[CKEDITOR.skin.getPath("editor")].concat(q.contentsCss),multiSelect:!1,attributes:{"aria-label":s.panelTitle}},init:function(){this.startGroup(s.panelTitle);
for(var c in r){var d=s["tag_"+c];
this.add(c,r[c].buildPreview(d),d)
}},onClick:function(a){t.focus();
t.fire("saveSnapshot");
a=r[a];
var c=t.elementPath();
t[a.checkActive(c,t)?"removeStyle":"applyStyle"](a);
setTimeout(function(){t.fire("saveSnapshot")
},0)
},onRender:function(){t.on("selectionChange",function(a){var d=this.getValue();
a=a.data.path;
this.refresh();
for(var f in r){if(r[f].checkActive(a,t)){f!=d&&this.setValue(f,t.lang.format["tag_"+f]);
return
}}this.setValue("")
},this)
},onOpen:function(){this.showAll();
for(var a in r){t.activeFilter.check(r[a])||this.hideItem(a)
}},refresh:function(){var a=t.elementPath();
if(a){if(a.isContextFor("p")){for(var d in r){if(t.activeFilter.check(r[d])){return
}}}this.setState(CKEDITOR.TRISTATE_DISABLED)
}}})
}}});
CKEDITOR.config.format_tags="p;h1;h2;h3;h4;h5;h6;pre;address;div";
CKEDITOR.config.format_p={element:"p"};
CKEDITOR.config.format_div={element:"div"};
CKEDITOR.config.format_pre={element:"pre"};
CKEDITOR.config.format_address={element:"address"};
CKEDITOR.config.format_h1={element:"h1"};
CKEDITOR.config.format_h2={element:"h2"};
CKEDITOR.config.format_h3={element:"h3"};
CKEDITOR.config.format_h4={element:"h4"};
CKEDITOR.config.format_h5={element:"h5"};
CKEDITOR.config.format_h6={element:"h6"};
(function(){var a={canUndo:!1,exec:function(d){var c=d.document.createElement("hr");
d.insertElement(c)
},allowedContent:"hr",requiredContent:"hr"};
CKEDITOR.plugins.add("horizontalrule",{init:function(b){b.blockless||(b.addCommand("horizontalrule",a),b.ui.addButton&&b.ui.addButton("HorizontalRule",{label:b.lang.horizontalrule.toolbar,command:"horizontalrule",toolbar:"insert,40"}))
}})
})();
CKEDITOR.plugins.add("htmlwriter",{init:function(c){var d=new CKEDITOR.htmlWriter;
d.forceSimpleAmpersand=c.config.forceSimpleAmpersand;
d.indentationChars=c.config.dataIndentationChars||"\t";
c.dataProcessor.writer=d
}});
CKEDITOR.htmlWriter=CKEDITOR.tools.createClass({base:CKEDITOR.htmlParser.basicWriter,$:function(){this.base();
this.indentationChars="\t";
this.selfClosingEnd=" /\x3e";
this.lineBreakChars="\n";
this.sortAttributes=1;
this._.indent=0;
this._.indentation="";
this._.inPre=0;
this._.rules={};
var c=CKEDITOR.dtd,d;
for(d in CKEDITOR.tools.extend({},c.$nonBodyContent,c.$block,c.$listItem,c.$tableContent)){this.setRules(d,{indent:!c[d]["#"],breakBeforeOpen:1,breakBeforeClose:!c[d]["#"],breakAfterClose:1,needsSpace:d in c.$block&&!(d in {li:1,dt:1,dd:1})})
}this.setRules("br",{breakAfterOpen:1});
this.setRules("title",{indent:0,breakAfterOpen:0});
this.setRules("style",{indent:0,breakBeforeClose:1});
this.setRules("pre",{breakAfterOpen:1,indent:0})
},proto:{openTag:function(c){var d=this._.rules[c];
this._.afterCloser&&d&&d.needsSpace&&this._.needsSpace&&this._.output.push("\n");
this._.indent?this.indentation():d&&d.breakBeforeOpen&&(this.lineBreak(),this.indentation());
this._.output.push("\x3c",c);
this._.afterCloser=0
},openTagClose:function(d,e){var f=this._.rules[d];
e?(this._.output.push(this.selfClosingEnd),f&&f.breakAfterClose&&(this._.needsSpace=f.needsSpace)):(this._.output.push("\x3e"),f&&f.indent&&(this._.indentation+=this.indentationChars));
f&&f.breakAfterOpen&&this.lineBreak();
"pre"==d&&(this._.inPre=1)
},attribute:function(c,d){"string"==typeof d&&(this.forceSimpleAmpersand&&(d=d.replace(/&amp;/g,"\x26")),d=CKEDITOR.tools.htmlEncodeAttr(d));
this._.output.push(" ",c,'\x3d"',d,'"')
},closeTag:function(c){var d=this._.rules[c];
d&&d.indent&&(this._.indentation=this._.indentation.substr(this.indentationChars.length));
this._.indent?this.indentation():d&&d.breakBeforeClose&&(this.lineBreak(),this.indentation());
this._.output.push("\x3c/",c,"\x3e");
"pre"==c&&(this._.inPre=0);
d&&d.breakAfterClose&&(this.lineBreak(),this._.needsSpace=d.needsSpace);
this._.afterCloser=1
},text:function(a){this._.indent&&(this.indentation(),!this._.inPre&&(a=CKEDITOR.tools.ltrim(a)));
this._.output.push(a)
},comment:function(a){this._.indent&&this.indentation();
this._.output.push("\x3c!--",a,"--\x3e")
},lineBreak:function(){!this._.inPre&&0<this._.output.length&&this._.output.push(this.lineBreakChars);
this._.indent=1
},indentation:function(){!this._.inPre&&this._.indentation&&this._.output.push(this._.indentation);
this._.indent=0
},reset:function(){this._.output=[];
this._.indent=0;
this._.indentation="";
this._.afterCloser=0;
this._.inPre=0
},setRules:function(d,e){var f=this._.rules[d];
f?CKEDITOR.tools.extend(f,e,!0):this._.rules[d]=e
}}});
(function(){CKEDITOR.plugins.add("iframe",{requires:"dialog,fakeobjects",onLoad:function(){CKEDITOR.addCss("img.cke_iframe{background-image: url("+CKEDITOR.getUrl(this.path+"images/placeholder.png")+");background-position: center center;background-repeat: no-repeat;border: 1px solid #a9a9a9;width: 80px;height: 80px;}")
},init:function(e){var d=e.lang.iframe,f="iframe[align,longdesc,frameborder,height,name,scrolling,src,title,width]";
e.plugins.dialogadvtab&&(f+=";iframe"+e.plugins.dialogadvtab.allowedContent({id:1,classes:1,styles:1}));
CKEDITOR.dialog.add("iframe",this.path+"dialogs/iframe.js");
e.addCommand("iframe",new CKEDITOR.dialogCommand("iframe",{allowedContent:f,requiredContent:"iframe"}));
e.ui.addButton&&e.ui.addButton("Iframe",{label:d.toolbar,command:"iframe",toolbar:"insert,80"});
e.on("doubleclick",function(g){var c=g.data.element;
c.is("img")&&"iframe"==c.data("cke-real-element-type")&&(g.data.dialog="iframe")
});
e.addMenuItems&&e.addMenuItems({iframe:{label:d.title,command:"iframe",group:"image"}});
e.contextMenu&&e.contextMenu.addListener(function(b){if(b&&b.is("img")&&"iframe"==b.data("cke-real-element-type")){return{iframe:CKEDITOR.TRISTATE_OFF}
}})
},afterInit:function(d){var c=d.dataProcessor;
(c=c&&c.dataFilter)&&c.addRules({elements:{iframe:function(a){return d.createFakeParserElement(a,"cke_iframe","iframe",!0)
}}})
}})
})();
(function(){function a(j){function n(f){var e=!1;
l.attachListener(l,"keydown",function(){var h=p.getBody().getElementsByTag(f);
if(!e){for(var g=0;
g<h.count();
g++){h.getItem(g).setCustomData("retain",!0)
}e=!0
}},null,null,1);
l.attachListener(l,"keyup",function(){var g=p.getElementsByTag(f);
e&&(1!=g.count()||g.getItem(0).getCustomData("retain")||g.getItem(0).remove(1),e=!1)
})
}var i=this.editor,p=j.document,o=p.body,m=p.getElementById("cke_actscrpt");
m&&m.parentNode.removeChild(m);
(m=p.getElementById("cke_shimscrpt"))&&m.parentNode.removeChild(m);
(m=p.getElementById("cke_basetagscrpt"))&&m.parentNode.removeChild(m);
o.contentEditable=!0;
CKEDITOR.env.ie&&(o.hideFocus=!0,o.disabled=!0,o.removeAttribute("disabled"));
delete this._.isLoadingData;
this.$=o;
p=new CKEDITOR.dom.document(p);
this.setup();
this.fixInitialSelection();
var l=this;
CKEDITOR.env.ie&&!CKEDITOR.env.edge&&i.enterMode!=CKEDITOR.ENTER_P?n("p"):CKEDITOR.env.edge&&i.enterMode!=CKEDITOR.ENTER_DIV&&n("div");
if(CKEDITOR.env.webkit||CKEDITOR.env.ie&&10<CKEDITOR.env.version){p.getDocumentElement().on("mousedown",function(e){e.data.getTarget().is("html")&&setTimeout(function(){i.editable().focus()
})
})
}d(i);
try{i.document.$.execCommand("2D-position",!1,!0)
}catch(k){}(CKEDITOR.env.gecko||CKEDITOR.env.ie&&"CSS1Compat"==i.document.$.compatMode)&&this.attachListener(this,"keydown",function(e){var g=e.data.getKeystroke();
if(33==g||34==g){if(CKEDITOR.env.ie){setTimeout(function(){i.getSelection().scrollIntoView()
},0)
}else{if(i.window.$.innerHeight>this.$.offsetHeight){var f=i.createRange();
f[33==g?"moveToElementEditStart":"moveToElementEditEnd"](this);
f.select();
e.data.preventDefault()
}}}});
CKEDITOR.env.ie&&this.attachListener(p,"blur",function(){try{p.$.selection.empty()
}catch(e){}});
CKEDITOR.env.iOS&&this.attachListener(p,"touchend",function(){j.focus()
});
o=i.document.getElementsByTag("title").getItem(0);
o.data("cke-title",o.getText());
CKEDITOR.env.ie&&(i.document.$.title=this._.docTitle);
CKEDITOR.tools.setTimeout(function(){"unloaded"==this.status&&(this.status="ready");
i.fire("contentDom");
this._.isPendingFocus&&(i.focus(),this._.isPendingFocus=!1);
setTimeout(function(){i.fire("dataReady")
},0)
},0,this)
}function d(g){function h(){var e;
g.editable().attachListener(g,"selectionChange",function(){var k=g.getSelection().getSelectedElement();
k&&(e&&(e.detachEvent("onresizestart",f),e=null),k.$.attachEvent("onresizestart",f),e=k.$)
})
}function f(e){e.returnValue=!1
}if(CKEDITOR.env.gecko){try{var j=g.document.$;
j.execCommand("enableObjectResizing",!1,!g.config.disableObjectResizing);
j.execCommand("enableInlineTableEditing",!1,!g.config.disableNativeTableHandles)
}catch(i){}}else{CKEDITOR.env.ie&&11>CKEDITOR.env.version&&g.config.disableObjectResizing&&h(g)
}}function c(){var g=[];
if(8<=CKEDITOR.document.$.documentMode){g.push("html.CSS1Compat [contenteditable\x3dfalse]{min-height:0 !important}");
var h=[],f;
for(f in CKEDITOR.dtd.$removeEmpty){h.push("html.CSS1Compat "+f+"[contenteditable\x3dfalse]")
}g.push(h.join(",")+"{display:inline-block}")
}else{CKEDITOR.env.gecko&&(g.push("html{height:100% !important}"),g.push("img:-moz-broken{-moz-force-broken-image-icon:1;min-width:24px;min-height:24px}"))
}g.push("html{cursor:text;*cursor:auto}");
g.push("img,input,textarea{cursor:default}");
return g.join("\n")
}CKEDITOR.plugins.add("wysiwygarea",{init:function(e){e.config.fullPage&&e.addFeature({allowedContent:"html head title; style [media,type]; body (*)[id]; meta link [*]",requiredContent:"body"});
e.addMode("wysiwyg",function(o){function i(f){f&&f.removeListener();
e.editable(new b(e,p.$.contentWindow.document.body));
e.setData(e.getData(1),o)
}var q="document.open();"+(CKEDITOR.env.ie?"("+CKEDITOR.tools.fixDomain+")();":"")+"document.close();",q=CKEDITOR.env.air?"javascript:void(0)":CKEDITOR.env.ie&&!CKEDITOR.env.edge?"javascript:void(function(){"+encodeURIComponent(q)+"}())":"",p=CKEDITOR.dom.element.createFromHtml('\x3ciframe src\x3d"'+q+'" frameBorder\x3d"0"\x3e\x3c/iframe\x3e');
p.setStyles({width:"100%",height:"100%"});
p.addClass("cke_wysiwyg_frame").addClass("cke_reset");
q=e.ui.space("contents");
q.append(p);
var n=CKEDITOR.env.ie&&!CKEDITOR.env.edge||CKEDITOR.env.gecko;
if(n){p.on("load",i)
}var m=e.title,l=e.fire("ariaEditorHelpLabel",{}).label;
m&&(CKEDITOR.env.ie&&l&&(m+=", "+l),p.setAttribute("title",m));
if(l){var m=CKEDITOR.tools.getNextId(),j=CKEDITOR.dom.element.createFromHtml('\x3cspan id\x3d"'+m+'" class\x3d"cke_voice_label"\x3e'+l+"\x3c/span\x3e");
q.append(j,1);
p.setAttribute("aria-describedby",m)
}e.on("beforeModeUnload",function(f){f.removeListener();
j&&j.remove()
});
p.setAttributes({tabIndex:e.tabIndex,allowTransparency:"true"});
!n&&i();
e.fire("ariaWidget",p)
})
}});
CKEDITOR.editor.prototype.addContentsCss=function(g){var h=this.config,f=h.contentsCss;
CKEDITOR.tools.isArray(f)||(h.contentsCss=f?[f]:[]);
h.contentsCss.push(g)
};
var b=CKEDITOR.tools.createClass({$:function(){this.base.apply(this,arguments);
this._.frameLoadedHandler=CKEDITOR.tools.addFunction(function(e){CKEDITOR.tools.setTimeout(a,0,this,e)
},this);
this._.docTitle=this.getWindow().getFrame().getAttribute("title")
},base:CKEDITOR.editable,proto:{setData:function(t,p){var s=this.editor;
if(p){this.setHtml(t),this.fixInitialSelection(),s.fire("dataReady")
}else{this._.isLoadingData=!0;
s._.dataStore={id:1};
var r=s.config,q=r.fullPage,o=r.docType,n=CKEDITOR.tools.buildStyleHtml(c()).replace(/<style>/,'\x3cstyle data-cke-temp\x3d"1"\x3e');
q||(n+=CKEDITOR.tools.buildStyleHtml(s.config.contentsCss));
var m=r.baseHref?'\x3cbase href\x3d"'+r.baseHref+'" data-cke-temp\x3d"1" /\x3e':"";
q&&(t=t.replace(/<!DOCTYPE[^>]*>/i,function(e){s.docType=o=e;
return""
}).replace(/<\?xml\s[^\?]*\?>/i,function(e){s.xmlDeclaration=e;
return""
}));
t=s.dataProcessor.toHtml(t);
q?(/<body[\s|>]/.test(t)||(t="\x3cbody\x3e"+t),/<html[\s|>]/.test(t)||(t="\x3chtml\x3e"+t+"\x3c/html\x3e"),/<head[\s|>]/.test(t)?/<title[\s|>]/.test(t)||(t=t.replace(/<head[^>]*>/,"$\x26\x3ctitle\x3e\x3c/title\x3e")):t=t.replace(/<html[^>]*>/,"$\x26\x3chead\x3e\x3ctitle\x3e\x3c/title\x3e\x3c/head\x3e"),m&&(t=t.replace(/<head[^>]*?>/,"$\x26"+m)),t=t.replace(/<\/head\s*>/,n+"$\x26"),t=o+t):t=r.docType+'\x3chtml dir\x3d"'+r.contentsLangDirection+'" lang\x3d"'+(r.contentsLanguage||s.langCode)+'"\x3e\x3chead\x3e\x3ctitle\x3e'+this._.docTitle+"\x3c/title\x3e"+m+n+"\x3c/head\x3e\x3cbody"+(r.bodyId?' id\x3d"'+r.bodyId+'"':"")+(r.bodyClass?' class\x3d"'+r.bodyClass+'"':"")+"\x3e"+t+"\x3c/body\x3e\x3c/html\x3e";
CKEDITOR.env.gecko&&(t=t.replace(/<body/,'\x3cbody contenteditable\x3d"true" '),20000>CKEDITOR.env.version&&(t=t.replace(/<body[^>]*>/,"$\x26\x3c!-- cke-content-start --\x3e")));
r='\x3cscript id\x3d"cke_actscrpt" type\x3d"text/javascript"'+(CKEDITOR.env.ie?' defer\x3d"defer" ':"")+"\x3evar wasLoaded\x3d0;function onload(){if(!wasLoaded)window.parent.CKEDITOR.tools.callFunction("+this._.frameLoadedHandler+",window);wasLoaded\x3d1;}"+(CKEDITOR.env.ie?"onload();":'document.addEventListener("DOMContentLoaded", onload, false );')+"\x3c/script\x3e";
CKEDITOR.env.ie&&9>CKEDITOR.env.version&&(r+='\x3cscript id\x3d"cke_shimscrpt"\x3ewindow.parent.CKEDITOR.tools.enableHtml5Elements(document)\x3c/script\x3e');
m&&CKEDITOR.env.ie&&10>CKEDITOR.env.version&&(r+='\x3cscript id\x3d"cke_basetagscrpt"\x3evar baseTag \x3d document.querySelector( "base" );baseTag.href \x3d baseTag.href;\x3c/script\x3e');
t=t.replace(/(?=\s*<\/(:?head)>)/,r);
this.clearCustomData();
this.clearListeners();
s.fire("contentDomUnload");
var j=this.getDocument();
try{j.write(t)
}catch(i){setTimeout(function(){j.write(t)
},0)
}}},getData:function(h){if(h){return this.getHtml()
}h=this.editor;
var j=h.config,g=j.fullPage,l=g&&h.docType,k=g&&h.xmlDeclaration,i=this.getDocument(),g=g?i.getDocumentElement().getOuterHtml():i.getBody().getHtml();
CKEDITOR.env.gecko&&j.enterMode!=CKEDITOR.ENTER_BR&&(g=g.replace(/<br>(?=\s*(:?$|<\/body>))/,""));
g=h.dataProcessor.toDataFormat(g);
k&&(g=k+"\n"+g);
l&&(g=l+"\n"+g);
return g
},focus:function(){this._.isLoadingData?this._.isPendingFocus=!0:b.baseProto.focus.call(this)
},detach:function(){var f=this.editor,g=f.document,f=f.window.getFrame();
b.baseProto.detach.call(this);
this.clearCustomData();
g.getDocumentElement().clearCustomData();
f.clearCustomData();
CKEDITOR.tools.removeFunction(this._.frameLoadedHandler);
(g=f.removeCustomData("onResize"))&&g.removeListener();
f.remove()
}}})
})();
CKEDITOR.config.disableObjectResizing=!1;
CKEDITOR.config.disableNativeTableHandles=!0;
CKEDITOR.config.disableNativeSpellChecker=!0;
CKEDITOR.config.contentsCss=CKEDITOR.getUrl("contents.css");
(function(){function b(c,d){d||(d=c.getSelection().getSelectedElement());
if(d&&d.is("img")&&!d.data("cke-realelement")&&!d.isReadOnly()){return d
}}function a(c){var d=c.getStyle("float");
if("inherit"==d||"none"==d){d=0
}d||(d=c.getAttribute("align"));
return d
}CKEDITOR.plugins.add("image",{requires:"dialog",init:function(c){if(!c.plugins.image2){CKEDITOR.dialog.add("image",this.path+"dialogs/image.js");
var d="img[alt,!src]{border-style,border-width,float,height,margin,margin-bottom,margin-left,margin-right,margin-top,width}";
CKEDITOR.dialog.isTabEnabled(c,"image","advanced")&&(d="img[alt,dir,id,lang,longdesc,!src,title]{*}(*)");
c.addCommand("image",new CKEDITOR.dialogCommand("image",{allowedContent:d,requiredContent:"img[alt,src]",contentTransformations:[["img{width}: sizeToStyle","img[width]: sizeToAttribute"],["img{float}: alignmentToStyle","img[align]: alignmentToAttribute"]]}));
c.ui.addButton&&c.ui.addButton("Image",{label:c.lang.common.image,command:"image",toolbar:"insert,10"});
c.on("doubleclick",function(e){var f=e.data.element;
!f.is("img")||f.data("cke-realelement")||f.isReadOnly()||(e.data.dialog="image")
});
c.addMenuItems&&c.addMenuItems({image:{label:c.lang.image.menu,command:"image",group:"image"}});
c.contextMenu&&c.contextMenu.addListener(function(e){if(b(c,e)){return{image:CKEDITOR.TRISTATE_OFF}
}})
}},afterInit:function(c){function d(e){var f=c.getCommand("justify"+e);
if(f){if("left"==e||"right"==e){f.on("exec",function(i){var j=b(c),h;
j&&(h=a(j),h==e?(j.removeStyle("float"),e==a(j)&&j.removeAttribute("align")):j.setStyle("float",e),i.cancel())
})
}f.on("refresh",function(g){var h=b(c);
h&&(h=a(h),this.setState(h==e?CKEDITOR.TRISTATE_ON:"right"==e||"left"==e?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED),g.cancel())
})
}}c.plugins.image2||(d("left"),d("right"),d("center"),d("block"))
}})
})();
CKEDITOR.config.image_removeLinkByEmptyURL=!0;
(function(){function a(g,d){var i,h;
d.on("refresh",function(f){var e=[b],j;
for(j in f.data.states){e.push(f.data.states[j])
}this.setState(CKEDITOR.tools.search(e,c)?c:b)
},d,null,100);
d.on("exec",function(e){i=g.getSelection();
h=i.createBookmarks(1);
e.data||(e.data={});
e.data.done=!1
},d,null,0);
d.on("exec",function(){g.forceNextSelectionCheck();
i.selectBookmarks(h)
},d,null,100)
}var b=CKEDITOR.TRISTATE_DISABLED,c=CKEDITOR.TRISTATE_OFF;
CKEDITOR.plugins.add("indent",{init:function(e){var d=CKEDITOR.plugins.indent.genericDefinition;
a(e,e.addCommand("indent",new d(!0)));
a(e,e.addCommand("outdent",new d));
e.ui.addButton&&(e.ui.addButton("Indent",{label:e.lang.indent.indent,command:"indent",directional:!0,toolbar:"indent,20"}),e.ui.addButton("Outdent",{label:e.lang.indent.outdent,command:"outdent",directional:!0,toolbar:"indent,10"}));
e.on("dirChanged",function(i){var o=e.createRange(),j=i.data.node;
o.setStartBefore(j);
o.setEndAfter(j);
for(var r=new CKEDITOR.dom.walker(o),q;
q=r.next();
){if(q.type==CKEDITOR.NODE_ELEMENT){if(!q.equals(j)&&q.getDirection()){o.setStartAfter(q),r=new CKEDITOR.dom.walker(o)
}else{var p=e.config.indentClasses;
if(p){for(var m="ltr"==i.data.dir?["_rtl",""]:["","_rtl"],k=0;
k<p.length;
k++){q.hasClass(p[k]+m[0])&&(q.removeClass(p[k]+m[0]),q.addClass(p[k]+m[1]))
}}p=q.getStyle("margin-right");
m=q.getStyle("margin-left");
p?q.setStyle("margin-left",p):q.removeStyle("margin-left");
m?q.setStyle("margin-right",m):q.removeStyle("margin-right")
}}}})
}});
CKEDITOR.plugins.indent={genericDefinition:function(d){this.isIndent=!!d;
this.startDisabled=!this.isIndent
},specificDefinition:function(f,d,g){this.name=d;
this.editor=f;
this.jobs={};
this.enterBr=f.config.enterMode==CKEDITOR.ENTER_BR;
this.isIndent=!!g;
this.relatedGlobal=g?"indent":"outdent";
this.indentKey=g?9:CKEDITOR.SHIFT+9;
this.database={}
},registerCommands:function(e,d){e.on("pluginsLoaded",function(){for(var f in d){(function(h,g){var i=h.getCommand(g.relatedGlobal),j;
for(j in g.jobs){i.on("exec",function(k){k.data.done||(h.fire("lockSnapshot"),g.execJob(h,j)&&(k.data.done=!0),h.fire("unlockSnapshot"),CKEDITOR.dom.element.clearAllMarkers(g.database))
},this,null,j),i.on("refresh",function(k){k.data.states||(k.data.states={});
k.data.states[g.name+"@"+j]=g.refreshJob(h,j,k.data.path)
},this,null,j)
}h.addFeature(g)
})(this,d[f])
}})
}};
CKEDITOR.plugins.indent.genericDefinition.prototype={context:"p",exec:function(){}};
CKEDITOR.plugins.indent.specificDefinition.prototype={execJob:function(f,d){var g=this.jobs[d];
if(g.state!=b){return g.exec.call(this,f)
}},refreshJob:function(f,d,g){d=this.jobs[d];
f.activeFilter.checkFeature(this)?d.state=d.refresh.call(this,f,g):d.state=b;
return d.state
},getContext:function(d){return d.contains(this.context)
}}
})();
(function(){function d(f,m,i){if(!f.getCustomData("indent_processed")){var k=this.editor,h=this.isIndent;
if(m){k=f.$.className.match(this.classNameRegex);
i=0;
k&&(k=k[1],i=CKEDITOR.tools.indexOf(m,k)+1);
if(0>(i+=h?1:-1)){return
}i=Math.min(i,m.length);
i=Math.max(i,0);
f.$.className=CKEDITOR.tools.ltrim(f.$.className.replace(this.classNameRegex,""));
0<i&&f.addClass(m[i-1])
}else{m=a(f,i);
i=parseInt(f.getStyle(m),10);
var j=k.config.indentOffset||40;
isNaN(i)&&(i=0);
i+=(h?1:-1)*j;
if(0>i){return
}i=Math.max(i,0);
i=Math.ceil(i/j)*j;
f.setStyle(m,i?i+(k.config.indentUnit||"px"):"");
""===f.getAttribute("style")&&f.removeAttribute("style")
}CKEDITOR.dom.element.setMarker(this.database,f,"indent_processed",1)
}}function a(f,h){return"ltr"==(h||f.getComputedStyle("direction"))?"margin-left":"margin-right"
}var c=CKEDITOR.dtd.$listItem,e=CKEDITOR.dtd.$list,b=CKEDITOR.TRISTATE_DISABLED,g=CKEDITOR.TRISTATE_OFF;
CKEDITOR.plugins.add("indentblock",{requires:"indent",init:function(f){function j(){h.specificDefinition.apply(this,arguments);
this.allowedContent={"div h1 h2 h3 h4 h5 h6 ol p pre ul":{propertiesOnly:!0,styles:i?null:"margin-left,margin-right",classes:i||null}};
this.enterBr&&(this.allowedContent.div=!0);
this.requiredContent=(this.enterBr?"div":"p")+(i?"("+i.join(",")+")":"{margin-left}");
this.jobs={20:{refresh:function(l,k){var n=k.block||k.blockLimit;
if(!n.is(c)){var o=n.getAscendant(c),n=o&&k.contains(o)||n
}n.is(c)&&(n=n.getParent());
if(this.enterBr||this.getContext(k)){if(i){var o=i,n=n.$.className.match(this.classNameRegex),m=this.isIndent,o=n?m?n[1]!=o.slice(-1):!0:m;
return o?g:b
}return this.isIndent?g:n?CKEDITOR[0>=(parseInt(n.getStyle(a(n)),10)||0)?"TRISTATE_DISABLED":"TRISTATE_OFF"]:b
}return b
},exec:function(l){var k=l.getSelection(),k=k&&k.getRanges()[0],m;
if(m=l.elementPath().contains(e)){d.call(this,m,i)
}else{for(k=k.createIterator(),l=l.config.enterMode,k.enforceRealBlocks=!0,k.enlargeBr=l!=CKEDITOR.ENTER_BR;
m=k.getNextParagraph(l==CKEDITOR.ENTER_P?"p":"div");
){m.isReadOnly()||d.call(this,m,i)
}}return !0
}}}
}var h=CKEDITOR.plugins.indent,i=f.config.indentClasses;
h.registerCommands(f,{indentblock:new j(f,"indentblock",!0),outdentblock:new j(f,"outdentblock")});
CKEDITOR.tools.extend(j.prototype,h.specificDefinition.prototype,{context:{div:1,dl:1,h1:1,h2:1,h3:1,h4:1,h5:1,h6:1,ul:1,ol:1,p:1,pre:1,table:1},classNameRegex:i?new RegExp("(?:^|\\s+)("+i.join("|")+")(?\x3d$|\\s)"):null})
}})
})();
(function(){function b(t){function r(A){for(var z=s.startContainer,B=s.endContainer;
z&&!z.getParent().equals(A);
){z=z.getParent()
}for(;
B&&!B.getParent().equals(A);
){B=B.getParent()
}if(!z||!B){return !1
}for(var x=z,z=[],v=!1;
!v;
){x.equals(B)&&(v=!0),z.push(x),x=x.getNext()
}if(1>z.length){return !1
}x=A.getParents(!0);
for(B=0;
B<x.length;
B++){if(x[B].getName&&j[x[B].getName()]){A=x[B];
break
}}for(var x=m.isIndent?1:-1,B=z[0],z=z[z.length-1],v=CKEDITOR.plugins.list.listToArray(A,i),p=v[z.getCustomData("listarray_index")].indent,B=B.getCustomData("listarray_index");
B<=z.getCustomData("listarray_index");
B++){if(v[B].indent+=x,0<x){var w=v[B].parent;
v[B].parent=new CKEDITOR.dom.element(w.getName(),w.getDocument())
}}for(B=z.getCustomData("listarray_index")+1;
B<v.length&&v[B].indent>p;
B++){v[B].indent+=x
}z=CKEDITOR.plugins.list.arrayToList(v,i,null,t.config.enterMode,A.getDirection());
if(!m.isIndent){var y;
if((y=A.getParent())&&y.is("li")){for(var x=z.listNode.getChildren(),l=[],q,B=x.count()-1;
0<=B;
B--){(q=x.getItem(B))&&q.is&&q.is("li")&&l.push(q)
}}}z&&z.listNode.replace(A);
if(l&&l.length){for(B=0;
B<l.length;
B++){for(q=A=l[B];
(q=q.getNext())&&q.is&&q.getName() in j;
){CKEDITOR.env.needsNbspFiller&&!A.getFirst(a)&&A.append(s.document.createText(" ")),A.append(q)
}A.insertAfter(y)
}}z&&t.fire("contentDomInvalidated");
return !0
}for(var m=this,i=this.database,j=this.context,k=t.getSelection(),k=(k&&k.getRanges()).createIterator(),s;
s=k.getNextRange();
){for(var u=s.getCommonAncestor();
u&&(u.type!=CKEDITOR.NODE_ELEMENT||!j[u.getName()]);
){if(t.editable().equals(u)){u=!1;
break
}u=u.getParent()
}u||(u=s.startPath().contains(j))&&s.setEndAt(u,CKEDITOR.POSITION_BEFORE_END);
if(!u){var o=s.getEnclosedNode();
o&&o.type==CKEDITOR.NODE_ELEMENT&&o.getName() in j&&(s.setStartAt(o,CKEDITOR.POSITION_AFTER_START),s.setEndAt(o,CKEDITOR.POSITION_BEFORE_END),u=o)
}u&&s.startContainer.type==CKEDITOR.NODE_ELEMENT&&s.startContainer.getName() in j&&(o=new CKEDITOR.dom.walker(s),o.evaluator=e,s.startContainer=o.next());
u&&s.endContainer.type==CKEDITOR.NODE_ELEMENT&&s.endContainer.getName() in j&&(o=new CKEDITOR.dom.walker(s),o.evaluator=e,s.endContainer=o.previous());
if(u){return r(u)
}}return 0
}function e(h){return h.type==CKEDITOR.NODE_ELEMENT&&h.is("li")
}function a(h){return g(h)&&f(h)
}var g=CKEDITOR.dom.walker.whitespaces(!0),f=CKEDITOR.dom.walker.bookmark(!1,!0),d=CKEDITOR.TRISTATE_DISABLED,c=CKEDITOR.TRISTATE_OFF;
CKEDITOR.plugins.add("indentlist",{requires:"indent",init:function(j){function i(k){h.specificDefinition.apply(this,arguments);
this.requiredContent=["ul","ol"];
k.on("key",function(l){if("wysiwyg"==k.mode&&l.data.keyCode==this.indentKey){var m=this.getContext(k.elementPath());
!m||this.isIndent&&CKEDITOR.plugins.indentList.firstItemInPath(this.context,k.elementPath(),m)||(k.execCommand(this.relatedGlobal),l.cancel())
}},this);
this.jobs[this.isIndent?10:30]={refresh:this.isIndent?function(o,m){var n=this.getContext(m),l=CKEDITOR.plugins.indentList.firstItemInPath(this.context,m,n);
return n&&this.isIndent&&!l?c:d
}:function(m,l){return !this.getContext(l)||this.isIndent?d:c
},exec:CKEDITOR.tools.bind(b,this)}
}var h=CKEDITOR.plugins.indent;
h.registerCommands(j,{indentlist:new i(j,"indentlist",!0),outdentlist:new i(j,"outdentlist")});
CKEDITOR.tools.extend(i.prototype,h.specificDefinition.prototype,{context:{ol:1,ul:1}})
}});
CKEDITOR.plugins.indentList={};
CKEDITOR.plugins.indentList.firstItemInPath=function(k,j,h){var i=j.contains(e);
h||(h=j.contains(k));
return h&&i&&i.equals(h.getFirst(e))
}
})();
CKEDITOR.plugins.add("smiley",{requires:"dialog",init:function(b){b.config.smiley_path=b.config.smiley_path||this.path+"images/";
b.addCommand("smiley",new CKEDITOR.dialogCommand("smiley",{allowedContent:"img[alt,height,!src,title,width]",requiredContent:"img"}));
b.ui.addButton&&b.ui.addButton("Smiley",{label:b.lang.smiley.toolbar,command:"smiley",toolbar:"insert,50"});
CKEDITOR.dialog.add("smiley",this.path+"dialogs/smiley.js")
}});
CKEDITOR.config.smiley_images="regular_smile.png sad_smile.png wink_smile.png teeth_smile.png confused_smile.png tongue_smile.png embarrassed_smile.png omg_smile.png whatchutalkingabout_smile.png angry_smile.png angel_smile.png shades_smile.png devil_smile.png cry_smile.png lightbulb.png thumbs_down.png thumbs_up.png heart.png broken_heart.png kiss.png envelope.png".split(" ");
CKEDITOR.config.smiley_descriptions="smiley;sad;wink;laugh;frown;cheeky;blush;surprise;indecision;angry;angel;cool;devil;crying;enlightened;no;yes;heart;broken heart;kiss;mail".split(";");
(function(){function c(e,f){f=void 0===f||f;
var d;
if(f){d=e.getComputedStyle("text-align")
}else{for(;
!e.hasAttribute||!e.hasAttribute("align")&&!e.getStyle("text-align");
){d=e.getParent();
if(!d){break
}e=d
}d=e.getStyle("text-align")||e.getAttribute("align")||""
}d&&(d=d.replace(/(?:-(?:moz|webkit)-)?(?:start|auto)/i,""));
!d&&f&&(d="rtl"==e.getComputedStyle("direction")?"right":"left");
return d
}function b(e,g,d){this.editor=e;
this.name=g;
this.value=d;
this.context="p";
g=e.config.justifyClasses;
var f=e.config.enterMode==CKEDITOR.ENTER_P?"p":"div";
if(g){switch(d){case"left":this.cssClassName=g[0];
break;
case"center":this.cssClassName=g[1];
break;
case"right":this.cssClassName=g[2];
break;
case"justify":this.cssClassName=g[3]
}this.cssClassRegex=new RegExp("(?:^|\\s+)(?:"+g.join("|")+")(?\x3d$|\\s)");
this.requiredContent=f+"("+this.cssClassName+")"
}else{this.requiredContent=f+"{text-align}"
}this.allowedContent={"caption div h1 h2 h3 h4 h5 h6 p pre td th li":{propertiesOnly:!0,styles:this.cssClassName?null:"text-align",classes:this.cssClassName||null}};
e.config.enterMode==CKEDITOR.ENTER_BR&&(this.allowedContent.div=!0)
}function a(g){var l=g.editor,f=l.createRange();
f.setStartBefore(g.data.node);
f.setEndAfter(g.data.node);
for(var i=new CKEDITOR.dom.walker(f),k;
k=i.next();
){if(k.type==CKEDITOR.NODE_ELEMENT){if(!k.equals(g.data.node)&&k.getDirection()){f.setStartAfter(k),i=new CKEDITOR.dom.walker(f)
}else{var j=l.config.justifyClasses;
j&&(k.hasClass(j[0])?(k.removeClass(j[0]),k.addClass(j[2])):k.hasClass(j[2])&&(k.removeClass(j[2]),k.addClass(j[0])));
j=k.getStyle("text-align");
"left"==j?k.setStyle("text-align","right"):"right"==j&&k.setStyle("text-align","left")
}}}}b.prototype={exec:function(x){var v=x.getSelection(),w=x.config.enterMode;
if(v){for(var q=v.createBookmarks(),u=v.getRanges(),t=this.cssClassName,r,s,o=x.config.useComputedState,o=void 0===o||o,j=u.length-1;
0<=j;
j--){for(r=u[j].createIterator(),r.enlargeBr=w!=CKEDITOR.ENTER_BR;
s=r.getNextParagraph(w==CKEDITOR.ENTER_P?"p":"div");
){if(!s.isReadOnly()){s.removeAttribute("align");
s.removeStyle("text-align");
var n=t&&(s.$.className=CKEDITOR.tools.ltrim(s.$.className.replace(this.cssClassRegex,""))),i=this.state==CKEDITOR.TRISTATE_OFF&&(!o||c(s,!0)!=this.value);
t?i?s.addClass(t):n||s.removeAttribute("class"):i&&s.setStyle("text-align",this.value)
}}}x.focus();
x.forceNextSelectionCheck();
v.selectBookmarks(q)
}},refresh:function(e,f){var d=f.block||f.blockLimit;
this.setState("body"!=d.getName()&&c(d,this.editor.config.useComputedState)==this.value?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF)
}};
CKEDITOR.plugins.add("justify",{init:function(f){if(!f.blockless){var j=new b(f,"justifyleft","left"),e=new b(f,"justifycenter","center"),g=new b(f,"justifyright","right"),i=new b(f,"justifyblock","justify");
f.addCommand("justifyleft",j);
f.addCommand("justifycenter",e);
f.addCommand("justifyright",g);
f.addCommand("justifyblock",i);
f.ui.addButton&&(f.ui.addButton("JustifyLeft",{label:f.lang.justify.left,command:"justifyleft",toolbar:"align,10"}),f.ui.addButton("JustifyCenter",{label:f.lang.justify.center,command:"justifycenter",toolbar:"align,20"}),f.ui.addButton("JustifyRight",{label:f.lang.justify.right,command:"justifyright",toolbar:"align,30"}),f.ui.addButton("JustifyBlock",{label:f.lang.justify.block,command:"justifyblock",toolbar:"align,40"}));
f.on("dirChanged",a)
}}})
})();
CKEDITOR.plugins.add("menubutton",{requires:"button,menu",onLoad:function(){var a=function(f){var e=this._,d=e.menu;
e.state!==CKEDITOR.TRISTATE_DISABLED&&(e.on&&d?d.hide():(e.previousState=e.state,d||(d=e.menu=new CKEDITOR.menu(f,{panel:{className:"cke_menu_panel",attributes:{"aria-label":f.lang.common.options}}}),d.onHide=CKEDITOR.tools.bind(function(){var c=this.command?f.getCommand(this.command).modes:this.modes;
this.setState(!c||c[f.mode]?e.previousState:CKEDITOR.TRISTATE_DISABLED);
e.on=0
},this),this.onMenu&&d.addListener(this.onMenu)),this.setState(CKEDITOR.TRISTATE_ON),e.on=1,setTimeout(function(){d.show(CKEDITOR.document.getById(e.id),4)
},0)))
};
CKEDITOR.ui.menuButton=CKEDITOR.tools.createClass({base:CKEDITOR.ui.button,$:function(b){delete b.panel;
this.base(b);
this.hasArrow=!0;
this.click=a
},statics:{handler:{create:function(b){return new CKEDITOR.ui.menuButton(b)
}}}})
},beforeInit:function(a){a.ui.addHandler(CKEDITOR.UI_MENUBUTTON,CKEDITOR.ui.menuButton.handler)
}});
CKEDITOR.UI_MENUBUTTON="menubutton";
(function(){CKEDITOR.plugins.add("language",{requires:"menubutton",init:function(r){var q=r.config.language_list||["ar:Arabic:rtl","fr:French","es:Spanish"],p=this,o=r.lang.language,n={},l,j,i,m;
r.addCommand("language",{allowedContent:"span[!lang,!dir]",requiredContent:"span[lang,dir]",contextSensitive:!0,exec:function(e,d){var f=n["language_"+d];
if(f){e[f.style.checkActive(e.elementPath(),e)?"removeStyle":"applyStyle"](f.style)
}},refresh:function(b){this.setState(p.getCurrentLangElement(b)?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF)
}});
for(m=0;
m<q.length;
m++){l=q[m].split(":"),j=l[0],i="language_"+j,n[i]={label:l[1],langId:j,group:"language",order:m,ltr:"rtl"!=(""+l[2]).toLowerCase(),onClick:function(){r.execCommand("language",this.langId)
},role:"menuitemcheckbox"},n[i].style=new CKEDITOR.style({element:"span",attributes:{lang:j,dir:n[i].ltr?"ltr":"rtl"}})
}n.language_remove={label:o.remove,group:"language_remove",state:CKEDITOR.TRISTATE_DISABLED,order:n.length,onClick:function(){var a=p.getCurrentLangElement(r);
a&&r.execCommand("language",a.getAttribute("lang"))
}};
r.addMenuGroup("language",1);
r.addMenuGroup("language_remove");
r.addMenuItems(n);
r.ui.add("Language",CKEDITOR.UI_MENUBUTTON,{label:o.button,allowedContent:"span[!lang,!dir]",requiredContent:"span[lang,dir]",toolbar:"bidi,30",command:"language",onMenu:function(){var a={},e=p.getCurrentLangElement(r),c;
for(c in n){a[c]=CKEDITOR.TRISTATE_OFF
}a.language_remove=e?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED;
e&&(a["language_"+e.getAttribute("lang")]=CKEDITOR.TRISTATE_ON);
return a
}})
},getCurrentLangElement:function(f){var e=f.elementPath();
f=e&&e.elements;
var h;
if(e){for(var g=0;
g<f.length;
g++){e=f[g],!h&&"span"==e.getName()&&e.hasAttribute("dir")&&e.hasAttribute("lang")&&(h=e)
}}return h
}})
})();
(function(){function f(m){return m.replace(/'/g,"\\$\x26")
}function d(r){for(var m,n=r.length,p=[],q=0;
q<n;
q++){m=r.charCodeAt(q),p.push(m)
}return"String.fromCharCode("+p.join(",")+")"
}function b(u,m){var n=u.plugins.link,q=n.compiledProtectionFunction.params,r,t;
t=[n.compiledProtectionFunction.name,"("];
for(var p=0;
p<q.length;
p++){n=q[p].toLowerCase(),r=m[n],0<p&&t.push(","),t.push("'",r?f(encodeURIComponent(m[n])):"","'")
}t.push(")");
return t.join("")
}function h(n){n=n.config.emailProtection||"";
var m;
n&&"encode"!=n&&(m={},n.replace(/^([^(]+)\(([^)]+)\)$/,function(p,r,q){m.name=r;
m.params=[];
q.replace(/[^,\s]+/g,function(t){m.params.push(t)
})
}));
return m
}CKEDITOR.plugins.add("link",{requires:"dialog,fakeobjects",onLoad:function(){function p(q){return n.replace(/%1/g,"rtl"==q?"right":"left").replace(/%2/g,"cke_contents_"+q)
}var m="background:url("+CKEDITOR.getUrl(this.path+"images"+(CKEDITOR.env.hidpi?"/hidpi":"")+"/anchor.png")+") no-repeat %1 center;border:1px dotted #00f;background-size:16px;",n=".%2 a.cke_anchor,.%2 a.cke_anchor_empty,.cke_editable.%2 a[name],.cke_editable.%2 a[data-cke-saved-name]{"+m+"padding-%1:18px;cursor:auto;}.%2 img.cke_anchor{"+m+"width:16px;min-height:15px;height:1.15em;vertical-align:text-bottom;}";
CKEDITOR.addCss(p("ltr")+p("rtl"))
},init:function(n){var m="a[!href]";
CKEDITOR.dialog.isTabEnabled(n,"link","advanced")&&(m=m.replace("]",",accesskey,charset,dir,id,lang,name,rel,tabindex,title,type]{*}(*)"));
CKEDITOR.dialog.isTabEnabled(n,"link","target")&&(m=m.replace("]",",target,onclick]"));
n.addCommand("link",new CKEDITOR.dialogCommand("link",{allowedContent:m,requiredContent:"a[href]"}));
n.addCommand("anchor",new CKEDITOR.dialogCommand("anchor",{allowedContent:"a[!name,id]",requiredContent:"a[name]"}));
n.addCommand("unlink",new CKEDITOR.unlinkCommand);
n.addCommand("removeAnchor",new CKEDITOR.removeAnchorCommand);
n.setKeystroke(CKEDITOR.CTRL+76,"link");
n.ui.addButton&&(n.ui.addButton("Link",{label:n.lang.link.toolbar,command:"link",toolbar:"links,10"}),n.ui.addButton("Unlink",{label:n.lang.link.unlink,command:"unlink",toolbar:"links,20"}),n.ui.addButton("Anchor",{label:n.lang.link.anchor.toolbar,command:"anchor",toolbar:"links,30"}));
CKEDITOR.dialog.add("link",this.path+"dialogs/link.js");
CKEDITOR.dialog.add("anchor",this.path+"dialogs/anchor.js");
n.on("doubleclick",function(q){var p=CKEDITOR.plugins.link.getSelectedLink(n)||q.data.element;
p.isReadOnly()||(p.is("a")?(q.data.dialog=!p.getAttribute("name")||p.getAttribute("href")&&p.getChildCount()?"link":"anchor",q.data.link=p):CKEDITOR.plugins.link.tryRestoreFakeAnchor(n,p)&&(q.data.dialog="anchor"))
},null,null,0);
n.on("doubleclick",function(p){p.data.dialog in {link:1,anchor:1}&&p.data.link&&n.getSelection().selectElement(p.data.link)
},null,null,20);
n.addMenuItems&&n.addMenuItems({anchor:{label:n.lang.link.anchor.menu,command:"anchor",group:"anchor",order:1},removeAnchor:{label:n.lang.link.anchor.remove,command:"removeAnchor",group:"anchor",order:5},link:{label:n.lang.link.menu,command:"link",group:"link",order:1},unlink:{label:n.lang.link.unlink,command:"unlink",group:"link",order:5}});
n.contextMenu&&n.contextMenu.addListener(function(q){if(!q||q.isReadOnly()){return null
}q=CKEDITOR.plugins.link.tryRestoreFakeAnchor(n,q);
if(!q&&!(q=CKEDITOR.plugins.link.getSelectedLink(n))){return null
}var p={};
q.getAttribute("href")&&q.getChildCount()&&(p={link:CKEDITOR.TRISTATE_OFF,unlink:CKEDITOR.TRISTATE_OFF});
q&&q.hasAttribute("name")&&(p.anchor=p.removeAnchor=CKEDITOR.TRISTATE_OFF);
return p
});
this.compiledProtectionFunction=h(n)
},afterInit:function(n){n.dataProcessor.dataFilter.addRules({elements:{a:function(p){return p.attributes.name?p.children.length?null:n.createFakeParserElement(p,"cke_anchor","anchor"):null
}}});
var m=n._.elementsPath&&n._.elementsPath.filters;
m&&m.push(function(q,p){if("a"==p&&(CKEDITOR.plugins.link.tryRestoreFakeAnchor(n,q)||q.getAttribute("name")&&(!q.getAttribute("href")||!q.getChildCount()))){return"anchor"
}})
}});
var F=/^javascript:/,E=/^mailto:([^?]+)(?:\?(.+))?$/,s=/subject=([^;?:@&=$,\/]*)/,o=/body=([^;?:@&=$,\/]*)/,l=/^#(.*)$/,k=/^((?:http|https|ftp|news):\/\/)?(.*)$/,j=/^(_(?:self|top|parent|blank))$/,g=/^javascript:void\(location\.href='mailto:'\+String\.fromCharCode\(([^)]+)\)(?:\+'(.*)')?\)$/,e=/^javascript:([^(]+)\(([^)]+)\)$/,c=/\s*window.open\(\s*this\.href\s*,\s*(?:'([^']*)'|null)\s*,\s*'([^']*)'\s*\)\s*;\s*return\s*false;*\s*/,a=/(?:^|,)([^=]+)=(\d+|yes|no)/gi,i={id:"advId",dir:"advLangDir",accessKey:"advAccessKey",name:"advName",lang:"advLangCode",tabindex:"advTabIndex",title:"advTitle",type:"advContentType","class":"advCSSClasses",charset:"advCharset",style:"advStyles",rel:"advRel"};
CKEDITOR.plugins.link={getSelectedLink:function(p){var m=p.getSelection(),n=m.getSelectedElement();
return n&&n.is("a")?n:(m=m.getRanges()[0])?(m.shrink(CKEDITOR.SHRINK_TEXT),p.elementPath(m.getCommonAncestor()).contains("a",1)):null
},getEditorAnchors:function(t){for(var m=t.editable(),n=m.isInline()&&!t.plugins.divarea?t.document:m,m=n.getElementsByTag("a"),n=n.getElementsByTag("img"),p=[],q=0,r;
r=m.getItem(q++);
){(r.data("cke-saved-name")||r.hasAttribute("name"))&&p.push({name:r.data("cke-saved-name")||r.getAttribute("name"),id:r.getAttribute("id")})
}for(q=0;
r=n.getItem(q++);
){(r=this.tryRestoreFakeAnchor(t,r))&&p.push({name:r.getAttribute("name"),id:r.getAttribute("id")})
}return p
},fakeAnchor:!0,tryRestoreFakeAnchor:function(p,m){if(m&&m.data("cke-real-element-type")&&"anchor"==m.data("cke-real-element-type")){var n=p.restoreRealElement(m);
if(n.data("cke-saved-name")){return n
}}},parseLinkAttributes:function(u,v){var w=v&&(v.data("cke-saved-href")||v.getAttribute("href"))||"",q=u.plugins.link.compiledProtectionFunction,r=u.config.emailProtection,t,p={};
w.match(F)&&("encode"==r?w=w.replace(g,function(y,x,z){return"mailto:"+String.fromCharCode.apply(String,x.split(","))+(z&&z.replace(/\\'/g,"'"))
}):r&&w.replace(e,function(y,x,D){if(x==q.name){p.type="email";
y=p.email={};
x=/(^')|('$)/g;
D=D.match(/[^,\s]+/g);
for(var C=D.length,B,A,z=0;
z<C;
z++){B=decodeURIComponent,A=D[z].replace(x,"").replace(/\\'/g,"'"),A=B(A),B=q.params[z].toLowerCase(),y[B]=A
}y.address=[y.name,y.domain].join("@")
}}));
if(!p.type){if(r=w.match(l)){p.type="anchor",p.anchor={},p.anchor.name=p.anchor.id=r[1]
}else{if(r=w.match(E)){t=w.match(s);
w=w.match(o);
p.type="email";
var m=p.email={};
m.address=r[1];
t&&(m.subject=decodeURIComponent(t[1]));
w&&(m.body=decodeURIComponent(w[1]))
}else{w&&(t=w.match(k))&&(p.type="url",p.url={},p.url.protocol=t[1],p.url.url=t[2])
}}}if(v){if(w=v.getAttribute("target")){p.target={type:w.match(j)?w:"frame",name:w}
}else{if(w=(w=v.data("cke-pa-onclick")||v.getAttribute("onclick"))&&w.match(c)){for(p.target={type:"popup",name:w[1]};
r=a.exec(w[2]);
){"yes"!=r[2]&&"1"!=r[2]||r[1] in {height:1,width:1,top:1,left:1}?isFinite(r[2])&&(p.target[r[1]]=r[2]):p.target[r[1]]=!0
}}}var w={},n;
for(n in i){(r=v.getAttribute(n))&&(w[i[n]]=r)
}if(n=v.data("cke-saved-name")||w.advName){w.advName=n
}CKEDITOR.tools.isEmpty(w)||(p.advanced=w)
}return p
},getLinkAttributes:function(x,y){var z=x.config.emailProtection||"",u={};
switch(y.type){case"url":var z=y.url&&void 0!==y.url.protocol?y.url.protocol:"http://",v=y.url&&CKEDITOR.tools.trim(y.url.url)||"";
u["data-cke-saved-href"]=0===v.indexOf("/")?v:z+v;
break;
case"anchor":z=y.anchor&&y.anchor.id;
u["data-cke-saved-href"]="#"+(y.anchor&&y.anchor.name||z||"");
break;
case"email":var w=y.email,v=w.address;
switch(z){case"":case"encode":var t=encodeURIComponent(w.subject||""),q=encodeURIComponent(w.body||""),w=[];
t&&w.push("subject\x3d"+t);
q&&w.push("body\x3d"+q);
w=w.length?"?"+w.join("\x26"):"";
"encode"==z?(z=["javascript:void(location.href\x3d'mailto:'+",d(v)],w&&z.push("+'",f(w),"'"),z.push(")")):z=["mailto:",v,w];
break;
default:z=v.split("@",2),w.name=z[0],w.domain=z[1],z=["javascript:",b(x,w)]
}u["data-cke-saved-href"]=z.join("")
}if(y.target){if("popup"==y.target.type){for(var z=["window.open(this.href, '",y.target.name||"","', '"],r="resizable status location toolbar menubar fullscreen scrollbars dependent".split(" "),v=r.length,t=function(n){y.target[n]&&r.push(n+"\x3d"+y.target[n])
},w=0;
w<v;
w++){r[w]+=y.target[r[w]]?"\x3dyes":"\x3dno"
}t("width");
t("left");
t("height");
t("top");
z.push(r.join(","),"'); return false;");
u["data-cke-pa-onclick"]=z.join("")
}else{"notSet"!=y.target.type&&y.target.name&&(u.target=y.target.name)
}}if(y.advanced){for(var p in i){(z=y.advanced[i[p]])&&(u[p]=z)
}u.name&&(u["data-cke-saved-name"]=u.name)
}u["data-cke-saved-href"]&&(u.href=u["data-cke-saved-href"]);
p={target:1,onclick:1,"data-cke-pa-onclick":1,"data-cke-saved-name":1};
y.advanced&&CKEDITOR.tools.extend(p,i);
for(var m in u){delete p[m]
}return{set:u,removed:CKEDITOR.tools.objectKeys(p)}
}};
CKEDITOR.unlinkCommand=function(){};
CKEDITOR.unlinkCommand.prototype={exec:function(n){var m=new CKEDITOR.style({element:"a",type:CKEDITOR.STYLE_INLINE,alwaysRemoveElement:1});
n.removeStyle(m)
},refresh:function(p,m){var n=m.lastElement&&m.lastElement.getAscendant("a",!0);
n&&"a"==n.getName()&&n.getAttribute("href")&&n.getChildCount()?this.setState(CKEDITOR.TRISTATE_OFF):this.setState(CKEDITOR.TRISTATE_DISABLED)
},contextSensitive:1,startDisabled:1,requiredContent:"a[href]"};
CKEDITOR.removeAnchorCommand=function(){};
CKEDITOR.removeAnchorCommand.prototype={exec:function(q){var m=q.getSelection(),n=m.createBookmarks(),p;
if(m&&(p=m.getSelectedElement())&&(p.getChildCount()?p.is("a"):CKEDITOR.plugins.link.tryRestoreFakeAnchor(q,p))){p.remove(1)
}else{if(p=CKEDITOR.plugins.link.getSelectedLink(q)){p.hasAttribute("href")?(p.removeAttributes({name:1,"data-cke-saved-name":1}),p.removeClass("cke_anchor")):p.remove(1)
}}m.selectBookmarks(n)
},requiredContent:"a[name]"};
CKEDITOR.tools.extend(CKEDITOR.config,{linkShowAdvancedTab:!0,linkShowTargetTab:!0})
})();
(function(){function h(y,q,v){function x(p){if(!(!(z=w[p?"getFirst":"getLast"]())||z.is&&z.isBlockBoundary()||!(o=q.root[p?"getPrevious":"getNext"](CKEDITOR.dom.walker.invisible(!0)))||o.is&&o.isBlockBoundary({br:1}))){y.document.createElement("br")[p?"insertBefore":"insertAfter"](z)
}}for(var u=CKEDITOR.plugins.list.listToArray(q.root,v),t=[],r=0;
r<q.contents.length;
r++){var s=q.contents[r];
(s=s.getAscendant("li",!0))&&!s.getCustomData("list_item_processed")&&(t.push(s),CKEDITOR.dom.element.setMarker(v,s,"list_item_processed",!0))
}s=null;
for(r=0;
r<t.length;
r++){s=t[r].getCustomData("listarray_index"),u[s].indent=-1
}for(r=s+1;
r<u.length;
r++){if(u[r].indent>u[r-1].indent+1){t=u[r-1].indent+1-u[r].indent;
for(s=u[r].indent;
u[r]&&u[r].indent>=s;
){u[r].indent+=t,r++
}r--
}}var w=CKEDITOR.plugins.list.arrayToList(u,v,null,y.config.enterMode,q.root.getAttribute("dir")).listNode,z,o;
x(!0);
x();
w.replace(q.root);
y.fire("contentDomInvalidated")
}function b(p,o){this.name=p;
this.context=this.type=o;
this.allowedContent=o+" li";
this.requiredContent=o
}function n(p,o,s,t){for(var r,q;
r=p[t?"getLast":"getFirst"](g);
){(q=r.getDirection(1))!==o.getDirection(1)&&r.setAttribute("dir",q),r.remove(),s?r[t?"insertBefore":"insertAfter"](s):o.append(r,t)
}}function m(p){function o(q){var r=p[q?"getPrevious":"getNext"](k);
r&&r.type==CKEDITOR.NODE_ELEMENT&&r.is(p.getName())&&(n(p,r,null,!q),p.remove(),p=r)
}o();
o(1)
}function l(o){return o.type==CKEDITOR.NODE_ELEMENT&&(o.getName() in CKEDITOR.dtd.$block||o.getName() in CKEDITOR.dtd.$listItem)&&CKEDITOR.dtd[o.getName()]["#"]
}function a(w,o,t){w.fire("saveSnapshot");
t.enlarge(CKEDITOR.ENLARGE_LIST_ITEM_CONTENTS);
var v=t.extractContents();
o.trim(!1,!0);
var s=o.createBookmark(),r=new CKEDITOR.dom.elementPath(o.startContainer),p=r.block,r=r.lastElement.getAscendant("li",1)||p,q=new CKEDITOR.dom.elementPath(t.startContainer),u=q.contains(CKEDITOR.dtd.$listItem),q=q.contains(CKEDITOR.dtd.$list);
p?(p=p.getBogus())&&p.remove():q&&(p=q.getPrevious(k))&&e(p)&&p.remove();
(p=v.getLast())&&p.type==CKEDITOR.NODE_ELEMENT&&p.is("br")&&p.remove();
(p=o.startContainer.getChild(o.startOffset))?v.insertBefore(p):o.startContainer.append(v);
u&&(v=c(u))&&(r.contains(u)?(n(v,u.getParent(),u),v.remove()):r.append(v));
for(;
t.checkStartOfBlock()&&t.checkEndOfBlock();
){q=t.startPath();
v=q.block;
if(!v){break
}v.is("li")&&(r=v.getParent(),v.equals(r.getLast(k))&&v.equals(r.getFirst(k))&&(v=r));
t.moveToPosition(v,CKEDITOR.POSITION_BEFORE_START);
v.remove()
}t=t.clone();
v=w.editable();
t.setEndAt(v,CKEDITOR.POSITION_BEFORE_END);
t=new CKEDITOR.dom.walker(t);
t.evaluator=function(x){return k(x)&&!e(x)
};
(t=t.next())&&t.type==CKEDITOR.NODE_ELEMENT&&t.getName() in CKEDITOR.dtd.$list&&m(t);
o.moveToBookmark(s);
o.select();
w.fire("saveSnapshot")
}function c(o){return(o=o.getLast(k))&&o.type==CKEDITOR.NODE_ELEMENT&&o.getName() in j?o:null
}var j={ol:1,ul:1},f=CKEDITOR.dom.walker.whitespaces(),i=CKEDITOR.dom.walker.bookmark(),k=function(o){return !(f(o)||i(o))
},e=CKEDITOR.dom.walker.bogus();
CKEDITOR.plugins.list={listToArray:function(z,q,w,y,v){if(!j[z.getName()]){return[]
}y||(y=0);
w||(w=[]);
for(var u=0,s=z.getChildCount();
u<s;
u++){var t=z.getChild(u);
t.type==CKEDITOR.NODE_ELEMENT&&t.getName() in CKEDITOR.dtd.$list&&CKEDITOR.plugins.list.listToArray(t,q,w,y+1);
if("li"==t.$.nodeName.toLowerCase()){var x={parent:z,indent:y,element:t,contents:[]};
v?x.grandparent=v:(x.grandparent=z.getParent(),x.grandparent&&"li"==x.grandparent.$.nodeName.toLowerCase()&&(x.grandparent=x.grandparent.getParent()));
q&&CKEDITOR.dom.element.setMarker(q,t,"listarray_index",w.length);
w.push(x);
for(var A=0,o=t.getChildCount(),r;
A<o;
A++){r=t.getChild(A),r.type==CKEDITOR.NODE_ELEMENT&&j[r.getName()]?CKEDITOR.plugins.list.listToArray(r,q,w,y+1,x.grandparent):x.contents.push(r)
}}}return w
},arrayToList:function(W,N,T,V,S){T||(T=0);
if(!W||W.length<T+1){return null
}for(var R,P=W[T].parent.getDocument(),Q=new CKEDITOR.dom.documentFragment(P),U=null,X=T,L=Math.max(W[T].indent,0),O=null,J,M,E=V==CKEDITOR.ENTER_P?"p":"div";
;
){var H=W[X];
R=H.grandparent;
J=H.element.getDirection(1);
if(H.indent==L){U&&W[X].parent.getName()==U.getName()||(U=W[X].parent.clone(!1,1),S&&U.setAttribute("dir",S),Q.append(U));
O=U.append(H.element.clone(0,1));
J!=U.getDirection(1)&&O.setAttribute("dir",J);
for(R=0;
R<H.contents.length;
R++){O.append(H.contents[R].clone(1,1))
}X++
}else{if(H.indent==Math.max(L,0)+1){H=W[X-1].element.getDirection(1),X=CKEDITOR.plugins.list.arrayToList(W,null,X,V,H!=J?J:null),!O.getChildCount()&&CKEDITOR.env.needsNbspFiller&&7>=P.$.documentMode&&O.append(P.createText(" ")),O.append(X.listNode),X=X.nextIndex
}else{if(-1==H.indent&&!T&&R){j[R.getName()]?(O=H.element.clone(!1,!0),J!=R.getDirection(1)&&O.setAttribute("dir",J)):O=new CKEDITOR.dom.documentFragment(P);
var U=R.getDirection(1)!=J,s=H.element,F=s.getAttribute("class"),o=s.getAttribute("style"),K=O.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT&&(V!=CKEDITOR.ENTER_BR||U||o||F),v,I=H.contents.length,u;
for(R=0;
R<I;
R++){if(v=H.contents[R],i(v)&&1<I){K?u=v.clone(1,1):O.append(v.clone(1,1))
}else{if(v.type==CKEDITOR.NODE_ELEMENT&&v.isBlockBoundary()){U&&!v.getDirection()&&v.setAttribute("dir",J);
M=v;
var G=s.getAttribute("style");
G&&M.setAttribute("style",G.replace(/([^;])$/,"$1;")+(M.getAttribute("style")||""));
F&&v.addClass(F);
M=null;
u&&(O.append(u),u=null);
O.append(v.clone(1,1))
}else{K?(M||(M=P.createElement(E),O.append(M),U&&M.setAttribute("dir",J)),o&&M.setAttribute("style",o),F&&M.setAttribute("class",F),u&&(M.append(u),u=null),M.append(v.clone(1,1))):O.append(v.clone(1,1))
}}}u&&((M||O).append(u),u=null);
O.type==CKEDITOR.NODE_DOCUMENT_FRAGMENT&&X!=W.length-1&&(CKEDITOR.env.needsBrFiller&&(J=O.getLast())&&J.type==CKEDITOR.NODE_ELEMENT&&J.is("br")&&J.remove(),(J=O.getLast(k))&&J.type==CKEDITOR.NODE_ELEMENT&&J.is(CKEDITOR.dtd.$block)||O.append(P.createElement("br")));
J=O.$.nodeName.toLowerCase();
"div"!=J&&"p"!=J||O.appendBogus();
Q.append(O);
U=null;
X++
}else{return null
}}}M=null;
if(W.length<=X||Math.max(W[X].indent,0)<L){break
}}if(N){for(W=Q.getFirst();
W;
){if(W.type==CKEDITOR.NODE_ELEMENT&&(CKEDITOR.dom.element.clearMarkers(N,W),W.getName() in CKEDITOR.dtd.$listItem&&(T=W,P=S=V=void 0,V=T.getDirection()))){for(S=T.getParent();
S&&!(P=S.getDirection());
){S=S.getParent()
}V==P&&T.removeAttribute("dir")
}W=W.getNextSourceNode()
}}return{listNode:Q,nextIndex:X}
}};
var d=/^h[1-6]$/,g=CKEDITOR.dom.walker.nodeType(CKEDITOR.NODE_ELEMENT);
b.prototype={exec:function(H){this.refresh(H,H.elementPath());
var w=H.config,D=H.getSelection(),F=D&&D.getRanges();
if(this.state==CKEDITOR.TRISTATE_OFF){var C=H.editable();
if(C.getFirst(k)){var B=1==F.length&&F[0];
(w=B&&B.getEnclosedNode())&&w.is&&this.type==w.getName()&&this.setState(CKEDITOR.TRISTATE_ON)
}else{w.enterMode==CKEDITOR.ENTER_BR?C.appendBogus():F[0].fixBlock(1,w.enterMode==CKEDITOR.ENTER_P?"p":"div"),D.selectRanges(F)
}}for(var w=D.createBookmarks(!0),C=[],z={},F=F.createIterator(),A=0;
(B=F.getNextRange())&&++A;
){var E=B.getBoundaryNodes(),I=E.startNode,u=E.endNode;
I.type==CKEDITOR.NODE_ELEMENT&&"td"==I.getName()&&B.setStartAt(E.startNode,CKEDITOR.POSITION_AFTER_START);
u.type==CKEDITOR.NODE_ELEMENT&&"td"==u.getName()&&B.setEndAt(E.endNode,CKEDITOR.POSITION_BEFORE_END);
B=B.createIterator();
for(B.forceBrBreak=this.state==CKEDITOR.TRISTATE_OFF;
E=B.getNextParagraph();
){if(!E.getCustomData("list_block")){CKEDITOR.dom.element.setMarker(z,E,"list_block",1);
for(var x=H.elementPath(E),I=x.elements,u=0,x=x.blockLimit,s,v=I.length-1;
0<=v&&(s=I[v]);
v--){if(j[s.getName()]&&x.contains(s)){x.removeCustomData("list_group_object_"+A);
(I=s.getCustomData("list_group_object"))?I.contents.push(E):(I={root:s,contents:[E]},C.push(I),CKEDITOR.dom.element.setMarker(z,s,"list_group_object",I));
u=1;
break
}}u||(u=x,u.getCustomData("list_group_object_"+A)?u.getCustomData("list_group_object_"+A).contents.push(E):(I={root:u,contents:[E]},CKEDITOR.dom.element.setMarker(z,u,"list_group_object_"+A,I),C.push(I)))
}}}for(s=[];
0<C.length;
){if(I=C.shift(),this.state==CKEDITOR.TRISTATE_OFF){if(j[I.root.getName()]){F=H;
A=I;
I=z;
B=s;
u=CKEDITOR.plugins.list.listToArray(A.root,I);
x=[];
for(E=0;
E<A.contents.length;
E++){v=A.contents[E],(v=v.getAscendant("li",!0))&&!v.getCustomData("list_item_processed")&&(x.push(v),CKEDITOR.dom.element.setMarker(I,v,"list_item_processed",!0))
}for(var v=A.root.getDocument(),J=void 0,o=void 0,E=0;
E<x.length;
E++){var G=x[E].getCustomData("listarray_index"),J=u[G].parent;
J.is(this.type)||(o=v.createElement(this.type),J.copyAttributes(o,{start:1,type:1}),o.removeStyle("list-style-type"),u[G].parent=o)
}I=CKEDITOR.plugins.list.arrayToList(u,I,null,F.config.enterMode);
u=void 0;
x=I.listNode.getChildCount();
for(E=0;
E<x&&(u=I.listNode.getChild(E));
E++){u.getName()==this.type&&B.push(u)
}I.listNode.replace(A.root);
F.fire("contentDomInvalidated")
}else{u=H;
B=I;
E=s;
x=B.contents;
F=B.root.getDocument();
A=[];
1==x.length&&x[0].equals(B.root)&&(I=F.createElement("div"),x[0].moveChildren&&x[0].moveChildren(I),x[0].append(I),x[0]=I);
B=B.contents[0].getParent();
for(v=0;
v<x.length;
v++){B=B.getCommonAncestor(x[v].getParent())
}J=u.config.useComputedState;
u=I=void 0;
J=void 0===J||J;
for(v=0;
v<x.length;
v++){for(o=x[v];
G=o.getParent();
){if(G.equals(B)){A.push(o);
!u&&o.getDirection()&&(u=1);
o=o.getDirection(J);
null!==I&&(I=I&&I!=o?null:o);
break
}o=G
}}if(!(1>A.length)){x=A[A.length-1].getNext();
v=F.createElement(this.type);
E.push(v);
for(J=E=void 0;
A.length;
){E=A.shift(),J=F.createElement("li"),o=E,o.is("pre")||d.test(o.getName())||"false"==o.getAttribute("contenteditable")?E.appendTo(J):(E.copyAttributes(J),I&&E.getDirection()&&(J.removeStyle("direction"),J.removeAttribute("dir")),E.moveChildren(J),E.remove()),J.appendTo(v)
}I&&u&&v.setAttribute("dir",I);
x?v.insertBefore(x):v.appendTo(B)
}}}else{this.state==CKEDITOR.TRISTATE_ON&&j[I.root.getName()]&&h.call(this,H,I,z)
}}for(v=0;
v<s.length;
v++){m(s[v])
}CKEDITOR.dom.element.clearAllMarkers(z);
D.selectBookmarks(w);
H.focus()
},refresh:function(p,o){var q=o.contains(j,1),r=o.blockLimit||o.root;
q&&r.contains(q)?this.setState(q.is(this.type)?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF):this.setState(CKEDITOR.TRISTATE_OFF)
}};
CKEDITOR.plugins.add("list",{requires:"indentlist",init:function(o){o.blockless||(o.addCommand("numberedlist",new b("numberedlist","ol")),o.addCommand("bulletedlist",new b("bulletedlist","ul")),o.ui.addButton&&(o.ui.addButton("NumberedList",{label:o.lang.list.numberedlist,command:"numberedlist",directional:!0,toolbar:"list,10"}),o.ui.addButton("BulletedList",{label:o.lang.list.bulletedlist,command:"bulletedlist",directional:!0,toolbar:"list,20"})),o.on("key",function(r){var w=r.data.domEvent.getKey(),y;
if("wysiwyg"==o.mode&&w in {8:1,46:1}){var v=o.getSelection().getRanges()[0],u=v&&v.startPath();
if(v&&v.collapsed){var s=8==w,t=o.editable(),x=new CKEDITOR.dom.walker(v.clone());
x.evaluator=function(p){return k(p)&&!e(p)
};
x.guard=function(A,p){return !(p&&A.type==CKEDITOR.NODE_ELEMENT&&A.is("table"))
};
w=v.clone();
if(s){var z;
(z=u.contains(j))&&v.checkBoundaryOfElement(z,CKEDITOR.START)&&(z=z.getParent())&&z.is("li")&&(z=c(z))?(y=z,z=z.getPrevious(k),w.moveToPosition(z&&e(z)?z:y,CKEDITOR.POSITION_BEFORE_START)):(x.range.setStartAt(t,CKEDITOR.POSITION_AFTER_START),x.range.setEnd(v.startContainer,v.startOffset),(z=x.previous())&&z.type==CKEDITOR.NODE_ELEMENT&&(z.getName() in j||z.is("li"))&&(z.is("li")||(x.range.selectNodeContents(z),x.reset(),x.evaluator=l,z=x.previous()),y=z,w.moveToElementEditEnd(y),w.moveToPosition(w.endPath().block,CKEDITOR.POSITION_BEFORE_END)));
if(y){a(o,w,v),r.cancel()
}else{var q=u.contains(j);
q&&v.checkBoundaryOfElement(q,CKEDITOR.START)&&(y=q.getFirst(k),v.checkBoundaryOfElement(y,CKEDITOR.START)&&(z=q.getPrevious(k),c(y)?z&&(v.moveToElementEditEnd(z),v.select()):o.execCommand("outdent"),r.cancel()))
}}else{if(y=u.contains("li")){if(x.range.setEndAt(t,CKEDITOR.POSITION_BEFORE_END),s=(t=y.getLast(k))&&l(t)?t:y,u=0,(z=x.next())&&z.type==CKEDITOR.NODE_ELEMENT&&z.getName() in j&&z.equals(t)?(u=1,z=x.next()):v.checkBoundaryOfElement(s,CKEDITOR.END)&&(u=2),u&&z){v=v.clone();
v.moveToElementEditStart(z);
if(1==u&&(w.optimize(),!w.startContainer.equals(y))){for(y=w.startContainer;
y.is(CKEDITOR.dtd.$inline);
){q=y,y=y.getParent()
}q&&w.moveToPosition(q,CKEDITOR.POSITION_AFTER_END)
}2==u&&(w.moveToPosition(w.endPath().block,CKEDITOR.POSITION_BEFORE_END),v.endPath().block&&v.moveToPosition(v.endPath().block,CKEDITOR.POSITION_AFTER_START));
a(o,w,v);
r.cancel()
}}else{x.range.setEndAt(t,CKEDITOR.POSITION_BEFORE_END),(z=x.next())&&z.type==CKEDITOR.NODE_ELEMENT&&z.is(j)&&(z=z.getFirst(k),u.block&&v.checkStartOfBlock()&&v.checkEndOfBlock()?(u.block.remove(),v.moveToElementEditStart(z),v.select()):c(z)?(v.moveToElementEditStart(z),v.select()):(v=v.clone(),v.moveToElementEditStart(z),a(o,w,v)),r.cancel())
}}setTimeout(function(){o.selectionChange(1)
})
}}}))
}})
})();
(function(){CKEDITOR.plugins.liststyle={requires:"dialog,contextmenu",init:function(d){if(!d.blockless){var c;
c=new CKEDITOR.dialogCommand("numberedListStyle",{requiredContent:"ol",allowedContent:"ol{list-style-type}[start]"});
c=d.addCommand("numberedListStyle",c);
d.addFeature(c);
CKEDITOR.dialog.add("numberedListStyle",this.path+"dialogs/liststyle.js");
c=new CKEDITOR.dialogCommand("bulletedListStyle",{requiredContent:"ul",allowedContent:"ul{list-style-type}"});
c=d.addCommand("bulletedListStyle",c);
d.addFeature(c);
CKEDITOR.dialog.add("bulletedListStyle",this.path+"dialogs/liststyle.js");
d.addMenuGroup("list",108);
d.addMenuItems({numberedlist:{label:d.lang.liststyle.numberedTitle,group:"list",command:"numberedListStyle"},bulletedlist:{label:d.lang.liststyle.bulletedTitle,group:"list",command:"bulletedListStyle"}});
d.contextMenu.addListener(function(f){if(!f||f.isReadOnly()){return null
}for(;
f;
){var e=f.getName();
if("ol"==e){return{numberedlist:CKEDITOR.TRISTATE_OFF}
}if("ul"==e){return{bulletedlist:CKEDITOR.TRISTATE_OFF}
}f=f.getParent()
}return null
})
}}};
CKEDITOR.plugins.add("liststyle",CKEDITOR.plugins.liststyle)
})();
(function(){function i(n,r,q){return aK(r)&&aK(q)&&q.equals(r.getNext(function(t){return !(ak(t)||aj(t)||aG(t))
}))
}function az(n){this.upper=n[0];
this.lower=n[1];
this.set.apply(this,n.slice(2))
}function s(n){var r=n.element;
if(r&&aK(r)&&(r=r.getAscendant(n.triggers,!0))&&n.editable.contains(r)){var q=p(r);
if("true"==q.getAttribute("contenteditable")){return r
}if(q.is(n.triggers)){return q
}}return null
}function d(n,r,q){aI(n,r);
aI(n,q);
n=r.size.bottom;
q=q.size.top;
return n&&q?0|(n+q)/2:n||q
}function aD(n,r,q){return r=r[q?"getPrevious":"getNext"](function(t){return t&&t.type==CKEDITOR.NODE_TEXT&&!ak(t)||aK(t)&&!aG(t)&&!ao(n,t)
})
}function aJ(n,r,q){return n>r&&n<q
}function p(n,q){if(n.data("cke-editable")){return null
}for(q||(n=n.getParent());
n&&!n.data("cke-editable");
){if(n.hasAttribute("contenteditable")){return n
}n=n.getParent()
}return null
}function aC(q){var t=q.doc,r=ai('\x3cspan contenteditable\x3d"false" style\x3d"'+o+"position:absolute;border-top:1px dashed "+q.boxColor+'"\x3e\x3c/span\x3e',t),n=CKEDITOR.getUrl(this.path+"images/"+(aH.hidpi?"hidpi/":"")+"icon"+(q.rtl?"-rtl":"")+".png");
aF(r,{attach:function(){this.wrap.getParent()||this.wrap.appendTo(q.editable,!0);
return this
},lineChildren:[aF(ai('\x3cspan title\x3d"'+q.editor.lang.magicline.title+'" contenteditable\x3d"false"\x3e\x26#8629;\x3c/span\x3e',t),{base:o+"height:17px;width:17px;"+(q.rtl?"left":"right")+":17px;background:url("+n+") center no-repeat "+q.boxColor+";cursor:pointer;"+(aH.hc?"font-size: 15px;line-height:14px;border:1px solid #fff;text-align:center;":"")+(aH.hidpi?"background-size: 9px 10px;":""),looks:["top:-8px; border-radius: 2px;","top:-17px; border-radius: 2px 2px 0px 0px;","top:-1px; border-radius: 0px 0px 2px 2px;"]}),aF(ai(g,t),{base:f+"left:0px;border-left-color:"+q.boxColor+";",looks:["border-width:8px 0 8px 8px;top:-8px","border-width:8px 0 0 8px;top:-8px","border-width:0 0 8px 8px;top:0px"]}),aF(ai(g,t),{base:f+"right:0px;border-right-color:"+q.boxColor+";",looks:["border-width:8px 8px 8px 0;top:-8px","border-width:8px 8px 0 0;top:-8px","border-width:0 8px 8px 0;top:0px"]})],detach:function(){this.wrap.getParent()&&this.wrap.remove();
return this
},mouseNear:function(){aI(q,this);
var u=q.holdDistance,v=this.size;
return v&&aJ(q.mouse.y,v.top-u,v.bottom+u)&&aJ(q.mouse.x,v.left-u,v.right+u)?!0:!1
},place:function(){var C=q.view,B=q.editable,A=q.trigger,y=A.upper,z=A.lower,x=y||z,u=x.getParent(),w={};
this.trigger=A;
y&&aI(q,y,!0);
z&&aI(q,z,!0);
aI(q,u,!0);
q.inInlineMode&&ah(q,!0);
u.equals(B)?(w.left=C.scroll.x,w.right=-C.scroll.x,w.width=""):(w.left=x.size.left-x.size.margin.left+C.scroll.x-(q.inInlineMode?C.editable.left+C.editable.border.left:0),w.width=x.size.outerWidth+x.size.margin.left+x.size.margin.right+C.scroll.x,w.right="");
y&&z?w.top=y.size.margin.bottom===z.size.margin.top?0|y.size.bottom+y.size.margin.bottom/2:y.size.margin.bottom<z.size.margin.top?y.size.bottom+y.size.margin.bottom:y.size.bottom+y.size.margin.bottom-z.size.margin.top:y?z||(w.top=y.size.bottom+y.size.margin.bottom):w.top=z.size.top-z.size.margin.top;
A.is(am)||aJ(w.top,C.scroll.y-15,C.scroll.y+5)?(w.top=q.inInlineMode?0:C.scroll.y,this.look(am)):A.is(al)||aJ(w.top,C.pane.bottom-5,C.pane.bottom+15)?(w.top=q.inInlineMode?C.editable.height+C.editable.padding.top+C.editable.padding.bottom:C.pane.bottom-1,this.look(al)):(q.inInlineMode&&(w.top-=C.editable.top+C.editable.border.top),this.look(aB));
q.inInlineMode&&(w.top--,w.top+=C.editable.scroll.top,w.left+=C.editable.scroll.left);
for(var v in w){w[v]=CKEDITOR.tools.cssLength(w[v])
}this.setStyles(w)
},look:function(v){if(this.oldLook!=v){for(var u=this.lineChildren.length,w;
u--;
){(w=this.lineChildren[u]).setAttribute("style",w.base+w.looks[0|v/2])
}this.oldLook=v
}},wrap:new m("span",q.doc)});
for(t=r.lineChildren.length;
t--;
){r.lineChildren[t].appendTo(r)
}r.look(aB);
r.appendTo(r.wrap);
r.unselectable();
r.lineChildren[0].on("mouseup",function(u){r.detach();
l(q,function(v){var w=q.line.trigger;
v[w.is(ag)?"insertBefore":"insertAfter"](w.is(ag)?w.lower:w.upper)
},!0);
q.editor.focus();
aH.ie||q.enterMode==CKEDITOR.ENTER_BR||q.hotNode.scrollIntoView();
u.data.preventDefault(!0)
});
r.on("mousedown",function(u){u.data.preventDefault(!0)
});
q.line=r
}function l(q,v,u){var n=new CKEDITOR.dom.range(q.doc),t=q.editor,r;
aH.ie&&q.enterMode==CKEDITOR.ENTER_BR?r=q.doc.createText(af):(r=(r=p(q.element,!0))&&r.data("cke-enter-mode")||q.enterMode,r=new m(ae[r],q.doc),r.is("br")||q.doc.createText(af).appendTo(r));
u&&t.fire("saveSnapshot");
v(r);
n.moveToPosition(r,CKEDITOR.POSITION_AFTER_START);
t.getSelection().selectRanges([n]);
q.hotNode=r;
u&&t.fire("saveSnapshot")
}function e(n,q){return{canUndo:!0,modes:{wysiwyg:1},exec:function(){function r(t){var v=aH.ie&&9>aH.version?" ":af,u=n.hotNode&&n.hotNode.getText()==v&&n.element.equals(n.hotNode)&&n.lastCmdDirection===!!q;
l(n,function(w){u&&n.hotNode&&n.hotNode.remove();
w[q?"insertAfter":"insertBefore"](t);
w.setAttributes({"data-cke-magicline-hot":1,"data-cke-magicline-dir":!!q});
n.lastCmdDirection=!!q
});
aH.ie||n.enterMode==CKEDITOR.ENTER_BR||n.hotNode.scrollIntoView();
n.line.detach()
}return function(t){t=t.getSelection().getStartElement();
var v;
t=t.getAscendant(c,1);
if(!ap(n,t)&&t&&!t.equals(n.editable)&&!t.contains(n.editable)){(v=p(t))&&"false"==v.getAttribute("contenteditable")&&(t=v);
n.element=t;
v=aD(n,t,!q);
var u;
aK(v)&&v.is(n.triggers)&&v.is(au)&&(!aD(n,v,!q)||(u=aD(n,v,!q))&&aK(u)&&u.is(n.triggers))?r(v):(u=s(n,t),aK(u)&&(aD(n,u,!q)?(t=aD(n,u,!q))&&aK(t)&&t.is(n.triggers)&&r(u):r(u)))
}}
}()}
}function ao(n,r){if(!r||r.type!=CKEDITOR.NODE_ELEMENT||!r.$){return !1
}var q=n.line;
return q.wrap.equals(r)||q.wrap.contains(r)
}function aK(n){return n&&n.type==CKEDITOR.NODE_ELEMENT&&n.$
}function aG(n){if(!aK(n)){return !1
}var q;
(q=aL(n))||(aK(n)?(q={left:1,right:1,center:1},q=!(!q[n.getComputedStyle("float")]&&!q[n.getAttribute("align")])):q=!1);
return q
}function aL(n){return !!{absolute:1,fixed:1}[n.getComputedStyle("position")]
}function ad(n,q){return aK(q)?q.is(n.triggers):null
}function ap(q,u){if(!u){return !1
}for(var t=u.getParents(1),n=t.length;
n--;
){for(var r=q.tabuList.length;
r--;
){if(t[n].hasAttribute(q.tabuList[r])){return !0
}}}return !1
}function h(n,r,q){r=r[q?"getLast":"getFirst"](function(t){return n.isRelevant(t)&&!t.is(aE)
});
if(!r){return !1
}aI(n,r);
return q?r.size.top>n.mouse.y:r.size.bottom<n.mouse.y
}function aw(q){var v=q.editable,u=q.mouse,n=q.view,t=q.triggerOffset;
ah(q);
var r=u.y>(q.inInlineMode?n.editable.top+n.editable.height/2:Math.min(n.editable.height,n.pane.height)/2),v=v[r?"getLast":"getFirst"](function(w){return !(ak(w)||aj(w))
});
if(!v){return null
}ao(q,v)&&(v=q.line.wrap[r?"getPrevious":"getNext"](function(w){return !(ak(w)||aj(w))
}));
if(!aK(v)||aG(v)||!ad(q,v)){return null
}aI(q,v);
return !r&&0<=v.size.top&&aJ(u.y,0,v.size.top+t)?(q=q.inInlineMode||0===n.scroll.y?am:aB,new az([null,v,ag,ac,q])):r&&v.size.bottom<=n.pane.height&&aJ(u.y,v.size.bottom-t,n.pane.height)?(q=q.inInlineMode||aJ(v.size.bottom,n.pane.height-t,n.pane.height)?al:aB,new az([v,null,aq,ac,q])):null
}function a(y){var w=y.mouse,v=y.view,x=y.triggerOffset,u=s(y);
if(!u){return null
}aI(y,u);
var x=Math.min(x,0|u.size.outerHeight/2),t=[],n,q;
if(aJ(w.y,u.size.top-1,u.size.top+x)){q=!1
}else{if(aJ(w.y,u.size.bottom-x,u.size.bottom+1)){q=!0
}else{return null
}}if(aG(u)||h(y,u,q)||u.getParent().is(ax)){return null
}var r=aD(y,u,!q);
if(r){if(r&&r.type==CKEDITOR.NODE_TEXT){return null
}if(aK(r)){if(aG(r)||!ad(y,r)||r.getParent().is(ax)){return null
}t=[r,u][q?"reverse":"concat"]().concat([k,ac])
}}else{u.equals(y.editable[q?"getLast":"getFirst"](y.isRelevant))?(ah(y),q&&aJ(w.y,u.size.bottom-x,v.pane.height)&&aJ(u.size.bottom,v.pane.height-x,v.pane.height)?n=al:aJ(w.y,0,u.size.top+x)&&(n=am)):n=aB,t=[null,u][q?"reverse":"concat"]().concat([q?aq:ag,ac,n,u.equals(y.editable[q?"getLast":"getFirst"](y.isRelevant))?q?al:am:aB])
}return 0 in t?new az(t):null
}function j(z,x,w,y){for(var v=x.getDocumentPosition(),u={},q={},r={},t={},n=aA.length;
n--;
){u[aA[n]]=parseInt(x.getComputedStyle.call(x,"border-"+aA[n]+"-width"),10)||0,r[aA[n]]=parseInt(x.getComputedStyle.call(x,"padding-"+aA[n]),10)||0,q[aA[n]]=parseInt(x.getComputedStyle.call(x,"margin-"+aA[n]),10)||0
}w&&!y||ab(z,y);
t.top=v.y-(w?0:z.view.scroll.y);
t.left=v.x-(w?0:z.view.scroll.x);
t.outerWidth=x.$.offsetWidth;
t.outerHeight=x.$.offsetHeight;
t.height=t.outerHeight-(r.top+r.bottom+u.top+u.bottom);
t.width=t.outerWidth-(r.left+r.right+u.left+u.right);
t.bottom=t.top+t.outerHeight;
t.right=t.left+t.outerWidth;
z.inInlineMode&&(t.scroll={top:x.$.scrollTop,left:x.$.scrollLeft});
return aF({border:u,padding:r,margin:q,ignoreScroll:w},t,!0)
}function aI(n,r,q){if(!aK(r)){return r.size=null
}if(!r.size){r.size={}
}else{if(r.size.ignoreScroll==q&&r.size.date>new Date-ar){return null
}}return aF(r.size,j(n,r,q),{date:+new Date},!0)
}function ah(n,q){n.view.editable=j(n,n.editable,q,!0)
}function ab(q,t){q.view||(q.view={});
var r=q.view;
if(!(!t&&r&&r.date>new Date-ar)){var n=q.win,r=n.getScrollPosition(),n=n.getViewPaneSize();
aF(q.view,{scroll:{x:r.x,y:r.y,width:q.doc.$.documentElement.scrollWidth-n.width,height:q.doc.$.documentElement.scrollHeight-n.height},pane:{width:n.width,height:n.height,bottom:n.height+r.y},date:+new Date},!0)
}}function av(A,y,x,z){for(var w=z,v=z,r=0,t=!1,u=!1,q=A.view.pane.height,n=A.mouse;
n.y+r<q&&0<n.y-r;
){t||(t=y(w,z));
u||(u=y(v,z));
!t&&0<n.y-r&&(w=x(A,{x:n.x,y:n.y-r}));
!u&&n.y+r<q&&(v=x(A,{x:n.x,y:n.y+r}));
if(t&&u){break
}r+=2
}return new az([w,v,null,null])
}CKEDITOR.plugins.add("magicline",{init:function(q){var w=q.config,v=w.magicline_triggerOffset||30,n={editor:q,enterMode:w.enterMode,triggerOffset:v,holdDistance:0|v*(w.magicline_holdDistance||0.5),boxColor:w.magicline_color||"#ff0000",rtl:"rtl"==w.contentsLangDirection,tabuList:["data-cke-hidden-sel"].concat(w.magicline_tabuList||[]),triggers:w.magicline_everywhere?c:{table:1,hr:1,div:1,ul:1,ol:1,dl:1,form:1,blockquote:1}},u,t,r;
n.isRelevant=function(x){return aK(x)&&!ao(n,x)&&!aG(x)
};
q.on("contentDom",function(){var z=q.editable(),y=q.document,x=q.window;
aF(n,{editable:z,inInlineMode:z.isInline(),doc:y,win:x,hotNode:null},!0);
n.boundary=n.inInlineMode?n.editable:n.doc.getDocumentElement();
z.is(an.$inline)||(n.inInlineMode&&!aL(z)&&z.setStyles({position:"relative",top:null,left:null}),aC.call(this,n),ab(n),z.attachListener(q,"beforeUndoImage",function(){n.line.detach()
}),z.attachListener(q,"beforeGetData",function(){n.line.wrap.getParent()&&(n.line.detach(),q.once("getData",function(){n.line.attach()
},null,null,1000))
},null,null,0),z.attachListener(n.inInlineMode?y:y.getWindow().getFrame(),"mouseout",function(D){if("wysiwyg"==q.mode){if(n.inInlineMode){var C=D.data.$.clientX;
D=D.data.$.clientY;
ab(n);
ah(n,!0);
var B=n.view.editable,A=n.view.scroll;
C>B.left-A.x&&C<B.right-A.x&&D>B.top-A.y&&D<B.bottom-A.y||(clearTimeout(r),r=null,n.line.detach())
}else{clearTimeout(r),r=null,n.line.detach()
}}}),z.attachListener(z,"keyup",function(){n.hiddenMode=0
}),z.attachListener(z,"keydown",function(A){if("wysiwyg"==q.mode){switch(A.data.getKeystroke()){case 2228240:case 16:n.hiddenMode=1,n.line.detach()
}}}),z.attachListener(n.inInlineMode?z:y,"mousemove",function(B){t=!0;
if("wysiwyg"==q.mode&&!q.readOnly&&!r){var A={x:B.data.$.clientX,y:B.data.$.clientY};
r=setTimeout(function(){n.mouse=A;
r=n.trigger=null;
ab(n);
t&&!n.hiddenMode&&q.focusManager.hasFocus&&!n.line.mouseNear()&&(n.element=b(n,!0))&&((n.trigger=aw(n)||a(n)||ay(n))&&!ap(n,n.trigger.upper||n.trigger.lower)?n.line.attach().place():(n.trigger=null,n.line.detach()),t=!1)
},30)
}}),z.attachListener(x,"scroll",function(){"wysiwyg"==q.mode&&(n.line.detach(),aH.webkit&&(n.hiddenMode=1,clearTimeout(u),u=setTimeout(function(){n.mouseDown||(n.hiddenMode=0)
},50)))
}),z.attachListener(at?y:x,"mousedown",function(){"wysiwyg"==q.mode&&(n.line.detach(),n.hiddenMode=1,n.mouseDown=1)
}),z.attachListener(at?y:x,"mouseup",function(){n.hiddenMode=0;
n.mouseDown=0
}),q.addCommand("accessPreviousSpace",e(n)),q.addCommand("accessNextSpace",e(n,!0)),q.setKeystroke([[w.magicline_keystrokePrevious,"accessPreviousSpace"],[w.magicline_keystrokeNext,"accessNextSpace"]]),q.on("loadSnapshot",function(){var D,C,B,A;
for(A in {p:1,br:1,div:1}){for(D=q.document.getElementsByTag(A),B=D.count();
B--;
){if((C=D.getItem(B)).data("cke-magicline-hot")){n.hotNode=C;
n.lastCmdDirection="true"===C.data("cke-magicline-dir")?!0:!1;
return
}}}}),this.backdoor={accessFocusSpace:l,boxTrigger:az,isLine:ao,getAscendantTrigger:s,getNonEmptyNeighbour:aD,getSize:j,that:n,triggerEdge:a,triggerEditable:aw,triggerExpand:ay})
},this)
}});
var aF=CKEDITOR.tools.extend,m=CKEDITOR.dom.element,ai=m.createFromHtml,aH=CKEDITOR.env,at=CKEDITOR.env.ie&&9>CKEDITOR.env.version,an=CKEDITOR.dtd,ae={},ag=128,aq=64,k=32,ac=16,am=4,al=2,aB=1,af=" ",ax=an.$listItem,aE=an.$tableContent,au=aF({},an.$nonEditable,an.$empty),c=an.$block,ar=100,o="width:0px;height:0px;padding:0px;margin:0px;display:block;z-index:9999;color:#fff;position:absolute;font-size: 0px;line-height:0px;",f=o+"border-color:transparent;display:block;border-style:solid;",g="\x3cspan\x3e"+af+"\x3c/span\x3e";
ae[CKEDITOR.ENTER_BR]="br";
ae[CKEDITOR.ENTER_P]="p";
ae[CKEDITOR.ENTER_DIV]="div";
az.prototype={set:function(n,r,q){this.properties=n+r+(q||aB);
return this
},is:function(n){return(this.properties&n)==n
}};
var b=function(){function n(r,t){var q=r.$.elementFromPoint(t.x,t.y);
return q&&q.nodeType?new CKEDITOR.dom.element(q):null
}return function(w,v,q){if(!w.mouse){return null
}var u=w.doc,t=w.line.wrap;
q=q||w.mouse;
var r=n(u,q);
v&&ao(w,r)&&(t.hide(),r=n(u,q),t.show());
return !r||r.type!=CKEDITOR.NODE_ELEMENT||!r.$||aH.ie&&9>aH.version&&!w.boundary.equals(r)&&!w.boundary.contains(r)?null:r
}
}(),ak=CKEDITOR.dom.walker.whitespaces(),aj=CKEDITOR.dom.walker.nodeType(CKEDITOR.NODE_COMMENT),ay=function(){function n(A){var z=A.element,y,x,u;
if(!aK(z)||z.contains(A.editable)||z.isReadOnly()){return null
}u=av(A,function(C,B){return !B.equals(C)
},function(C,B){return b(C,!0,B)
},z);
y=u.upper;
x=u.lower;
if(i(A,y,x)){return u.set(k,8)
}if(y&&z.contains(y)){for(;
!y.getParent().equals(z);
){y=y.getParent()
}}else{y=z.getFirst(function(B){return q(A,B)
})
}if(x&&z.contains(x)){for(;
!x.getParent().equals(z);
){x=x.getParent()
}}else{x=z.getLast(function(B){return q(A,B)
})
}if(!y||!x){return null
}aI(A,y);
aI(A,x);
if(!aJ(A.mouse.y,y.size.top,x.size.bottom)){return null
}for(var z=Number.MAX_VALUE,v,w,t,r;
x&&!x.equals(y)&&(w=y.getNext(A.isRelevant));
){v=Math.abs(d(A,y,w)-A.mouse.y),v<z&&(z=v,t=y,r=w),y=w,aI(A,y)
}if(!t||!r||!aJ(A.mouse.y,t.size.top,r.size.bottom)){return null
}u.upper=t;
u.lower=r;
return u.set(k,8)
}function q(t,r){return !(r&&r.type==CKEDITOR.NODE_TEXT||aj(r)||aG(r)||ao(t,r)||r.type==CKEDITOR.NODE_ELEMENT&&r.$&&r.is("br"))
}return function(v){var r=n(v),u;
if(u=r){u=r.upper;
var t=r.lower;
u=!u||!t||aG(t)||aG(u)||t.equals(u)||u.equals(t)||t.contains(u)||u.contains(t)?!1:ad(v,u)&&ad(v,t)&&i(v,u,t)?!0:!1
}return u?r:null
}
}(),aA=["top","left","right","bottom"]
})();
CKEDITOR.config.magicline_keystrokePrevious=CKEDITOR.CTRL+CKEDITOR.SHIFT+51;
CKEDITOR.config.magicline_keystrokeNext=CKEDITOR.CTRL+CKEDITOR.SHIFT+52;
(function(){function e(h){if(!h||h.type!=CKEDITOR.NODE_ELEMENT||"form"!=h.getName()){return[]
}for(var j=[],i=["style","className"],g=0;
g<i.length;
g++){var k=h.$.elements.namedItem(i[g]);
k&&(k=new CKEDITOR.dom.element(k),j.push([k,k.nextSibling]),k.remove())
}return j
}function c(h,j){if(h&&h.type==CKEDITOR.NODE_ELEMENT&&"form"==h.getName()&&0<j.length){for(var i=j.length-1;
0<=i;
i--){var g=j[i][0],k=j[i][1];
k?g.insertBefore(k):g.appendTo(h)
}}}function d(h,j){var i=e(h),g={},k=h.$;
j||(g["class"]=k.className||"",k.className="");
g.inline=k.style.cssText||"";
j||(k.style.cssText="position: static; overflow: visible");
c(i);
return g
}function b(h,j){var i=e(h),g=h.$;
"class" in j&&(g.className=j["class"]);
"inline" in j&&(g.style.cssText=j.inline);
c(i)
}function a(h){if(!h.editable().isInline()){var j=CKEDITOR.instances,i;
for(i in j){var g=j[i];
"wysiwyg"!=g.mode||g.readOnly||(g=g.document.getBody(),g.setAttribute("contentEditable",!1),g.setAttribute("contentEditable",!0))
}h.editable().hasFocus&&(h.toolbox.focus(),h.focus())
}}CKEDITOR.plugins.add("maximize",{init:function(s){function o(){var f=q.getViewPaneSize();
s.resize(f.width,f.height,null,!0)
}if(s.elementMode!=CKEDITOR.ELEMENT_MODE_INLINE){var k=s.lang,r=CKEDITOR.document,q=r.getWindow(),j,i,g,h=CKEDITOR.TRISTATE_OFF;
s.addCommand("maximize",{modes:{wysiwyg:!CKEDITOR.env.iOS,source:!CKEDITOR.env.iOS},readOnly:1,editorFocus:!1,exec:function(){var l=s.container.getFirst(function(t){return t.type==CKEDITOR.NODE_ELEMENT&&t.hasClass("cke_inner")
}),m=s.ui.space("contents");
if("wysiwyg"==s.mode){var p=s.getSelection();
j=p&&p.getRanges();
i=q.getScrollPosition()
}else{var f=s.editable().$;
j=!CKEDITOR.env.ie&&[f.selectionStart,f.selectionEnd];
i=[f.scrollLeft,f.scrollTop]
}if(this.state==CKEDITOR.TRISTATE_OFF){q.on("resize",o);
g=q.getScrollPosition();
for(p=s.container;
p=p.getParent();
){p.setCustomData("maximize_saved_styles",d(p)),p.setStyle("z-index",s.config.baseFloatZIndex-5)
}m.setCustomData("maximize_saved_styles",d(m,!0));
l.setCustomData("maximize_saved_styles",d(l,!0));
m={overflow:CKEDITOR.env.webkit?"":"hidden",width:0,height:0};
r.getDocumentElement().setStyles(m);
!CKEDITOR.env.gecko&&r.getDocumentElement().setStyle("position","fixed");
CKEDITOR.env.gecko&&CKEDITOR.env.quirks||r.getBody().setStyles(m);
CKEDITOR.env.ie?setTimeout(function(){q.$.scrollTo(0,0)
},0):q.$.scrollTo(0,0);
l.setStyle("position",CKEDITOR.env.gecko&&CKEDITOR.env.quirks?"fixed":"absolute");
l.$.offsetLeft;
l.setStyles({"z-index":s.config.baseFloatZIndex-5,left:"0px",top:"0px"});
l.addClass("cke_maximized");
o();
m=l.getDocumentPosition();
l.setStyles({left:-1*m.x+"px",top:-1*m.y+"px"});
CKEDITOR.env.gecko&&a(s)
}else{if(this.state==CKEDITOR.TRISTATE_ON){q.removeListener("resize",o);
for(var p=[m,l],n=0;
n<p.length;
n++){b(p[n],p[n].getCustomData("maximize_saved_styles")),p[n].removeCustomData("maximize_saved_styles")
}for(p=s.container;
p=p.getParent();
){b(p,p.getCustomData("maximize_saved_styles")),p.removeCustomData("maximize_saved_styles")
}CKEDITOR.env.ie?setTimeout(function(){q.$.scrollTo(g.x,g.y)
},0):q.$.scrollTo(g.x,g.y);
l.removeClass("cke_maximized");
CKEDITOR.env.webkit&&(l.setStyle("display","inline"),setTimeout(function(){l.setStyle("display","block")
},0));
s.fire("resize",{outerHeight:s.container.$.offsetHeight,contentsHeight:m.$.offsetHeight,outerWidth:s.container.$.offsetWidth})
}}this.toggleState();
if(p=this.uiItems[0]){m=this.state==CKEDITOR.TRISTATE_OFF?k.maximize.maximize:k.maximize.minimize,p=CKEDITOR.document.getById(p._.id),p.getChild(1).setHtml(m),p.setAttribute("title",m),p.setAttribute("href",'javascript:void("'+m+'");')
}"wysiwyg"==s.mode?j?(CKEDITOR.env.gecko&&a(s),s.getSelection().selectRanges(j),(f=s.getSelection().getStartElement())&&f.scrollIntoView(!0)):q.$.scrollTo(i.x,i.y):(j&&(f.selectionStart=j[0],f.selectionEnd=j[1]),f.scrollLeft=i[0],f.scrollTop=i[1]);
j=i=null;
h=this.state;
s.fire("maximize",this.state)
},canUndo:!1});
s.ui.addButton&&s.ui.addButton("Maximize",{label:k.maximize.maximize,command:"maximize",toolbar:"tools,10"});
s.on("mode",function(){var f=s.getCommand("maximize");
f.setState(f.state==CKEDITOR.TRISTATE_DISABLED?CKEDITOR.TRISTATE_DISABLED:h)
},null,null,100)
}}})
})();
CKEDITOR.plugins.add("newpage",{init:function(b){b.addCommand("newpage",{modes:{wysiwyg:1,source:1},exec:function(c){var d=this;
c.setData(c.config.newpage_html||"",function(){c.focus();
setTimeout(function(){c.fire("afterCommandExec",{name:"newpage",command:d});
c.selectionChange()
},200)
})
},async:!0});
b.ui.addButton&&b.ui.addButton("NewPage",{label:b.lang.newpage.toolbar,command:"newpage",toolbar:"document,20"})
}});
(function(){function a(b){return{"aria-label":b,"class":"cke_pagebreak",contenteditable:"false","data-cke-display-name":"pagebreak","data-cke-pagebreak":1,style:"page-break-after: always",title:b}
}CKEDITOR.plugins.add("pagebreak",{requires:"fakeobjects",onLoad:function(){var b=("background:url("+CKEDITOR.getUrl(this.path+"images/pagebreak.gif")+") no-repeat center center;clear:both;width:100%;border-top:#999 1px dotted;border-bottom:#999 1px dotted;padding:0;height:7px;cursor:default;").replace(/;/g," !important;");
CKEDITOR.addCss("div.cke_pagebreak{"+b+"}")
},init:function(b){b.blockless||(b.addCommand("pagebreak",CKEDITOR.plugins.pagebreakCmd),b.ui.addButton&&b.ui.addButton("PageBreak",{label:b.lang.pagebreak.toolbar,command:"pagebreak",toolbar:"insert,70"}),CKEDITOR.env.webkit&&b.on("contentDom",function(){b.document.on("click",function(c){c=c.data.getTarget();
c.is("div")&&c.hasClass("cke_pagebreak")&&b.getSelection().selectElement(c)
})
}))
},afterInit:function(e){function d(b){CKEDITOR.tools.extend(b.attributes,a(e.lang.pagebreak.alt),!0);
b.children.length=0
}var l=e.dataProcessor,j=l&&l.dataFilter,l=l&&l.htmlFilter,i=/page-break-after\s*:\s*always/i,f=/display\s*:\s*none/i;
l&&l.addRules({attributes:{"class":function(h,g){var m=h.replace("cke_pagebreak","");
if(m!=h){var k=CKEDITOR.htmlParser.fragment.fromHtml('\x3cspan style\x3d"display: none;"\x3e\x26nbsp;\x3c/span\x3e').children[0];
g.children.length=0;
g.add(k);
k=g.attributes;
delete k["aria-label"];
delete k.contenteditable;
delete k.title
}return m
}}},{applyToAll:!0,priority:5});
j&&j.addRules({elements:{div:function(b){if(b.attributes["data-cke-pagebreak"]){d(b)
}else{if(i.test(b.attributes.style)){var g=b.children[0];
g&&"span"==g.name&&f.test(g.attributes.style)&&d(b)
}}}}})
}});
CKEDITOR.plugins.pagebreakCmd={exec:function(d){var c=d.document.createElement("div",{attributes:a(d.lang.pagebreak.alt)});
d.insertElement(c)
},context:"div",allowedContent:{div:{styles:"!page-break-after"},span:{match:function(b){return(b=b.parent)&&"div"==b.name&&b.styles&&b.styles["page-break-after"]
},styles:"display"}},requiredContent:"div{page-break-after}"}
})();
(function(){var a={canUndo:!1,async:!0,exec:function(b){b.getClipboardData({title:b.lang.pastetext.title},function(c){c&&b.fire("paste",{type:"text",dataValue:c.dataValue,method:"paste",dataTransfer:CKEDITOR.plugins.clipboard.initPasteDataTransfer()});
b.fire("afterCommandExec",{name:"pastetext",command:a,returnValue:!!c})
})
}};
CKEDITOR.plugins.add("pastetext",{requires:"clipboard",init:function(b){b.addCommand("pastetext",a);
b.ui.addButton&&b.ui.addButton("PasteText",{label:b.lang.pastetext.button,command:"pastetext",toolbar:"clipboard,40"});
if(b.config.forcePasteAsPlainText){b.on("beforePaste",function(c){"html"!=c.data.type&&(c.data.type="text")
})
}b.on("pasteState",function(c){b.getCommand("pastetext").setState(c.data)
})
}})
})();
(function(){function b(e,h,g){var c=CKEDITOR.cleanWord;
c?g():(e=CKEDITOR.getUrl(e.config.pasteFromWordCleanupFile||h+"filter/default.js"),CKEDITOR.scriptLoader.load(e,g,null,!0));
return !c
}function a(c){c.data.type="html"
}CKEDITOR.plugins.add("pastefromword",{requires:"clipboard",init:function(c){var g=0,e=this.path;
c.addCommand("pastefromword",{canUndo:!1,async:!0,exec:function(d){var f=this;
g=1;
d.once("beforePaste",a);
d.getClipboardData({title:d.lang.pastefromword.title},function(h){h&&d.fire("paste",{type:"html",dataValue:h.dataValue,method:"paste",dataTransfer:CKEDITOR.plugins.clipboard.initPasteDataTransfer()});
d.fire("afterCommandExec",{name:"pastefromword",command:f,returnValue:!!h})
})
}});
c.ui.addButton&&c.ui.addButton("PasteFromWord",{label:c.lang.pastefromword.toolbar,command:"pastefromword",toolbar:"clipboard,50"});
c.on("pasteState",function(d){c.getCommand("pastefromword").setState(d.data)
});
c.on("paste",function(d){var h=d.data,i=h.dataValue;
if(i&&(g||/(class=\"?Mso|style=\"[^\"]*\bmso\-|w:WordDocument)/.test(i))){h.dontFilter=!0;
var f=b(c,e,function(){if(f){c.fire("paste",h)
}else{if(!c.config.pasteFromWordPromptCleanup||g||confirm(c.lang.pastefromword.confirmCleanup)){h.dataValue=CKEDITOR.cleanWord(i,c)
}}g=0
});
f&&d.cancel()
}},null,null,3)
}})
})();
(function(){var b,a={modes:{wysiwyg:1,source:1},canUndo:!1,readOnly:1,exec:function(i){var l,h=i.config,m=h.baseHref?'\x3cbase href\x3d"'+h.baseHref+'"/\x3e':"";
if(h.fullPage){l=i.getData().replace(/<head>/,"$\x26"+m).replace(/[^>]*(?=<\/title>)/,"$\x26 \x26mdash; "+i.lang.preview.preview)
}else{var h="\x3cbody ",o=i.document&&i.document.getBody();
o&&(o.getAttribute("id")&&(h+='id\x3d"'+o.getAttribute("id")+'" '),o.getAttribute("class")&&(h+='class\x3d"'+o.getAttribute("class")+'" '));
h+="\x3e";
l=i.config.docType+'\x3chtml dir\x3d"'+i.config.contentsLangDirection+'"\x3e\x3chead\x3e'+m+"\x3ctitle\x3e"+i.lang.preview.preview+"\x3c/title\x3e"+CKEDITOR.tools.buildStyleHtml(i.config.contentsCss)+"\x3c/head\x3e"+h+i.getData()+"\x3c/body\x3e\x3c/html\x3e"
}m=640;
h=420;
o=80;
try{var p=window.screen,m=Math.round(0.8*p.width),h=Math.round(0.7*p.height),o=Math.round(0.1*p.width)
}catch(j){}if(!1===i.fire("contentPreview",i={dataValue:l})){return !1
}var p="",n;
CKEDITOR.env.ie&&(window._cke_htmlToLoad=i.dataValue,n="javascript:void( (function(){document.open();"+("("+CKEDITOR.tools.fixDomain+")();").replace(/\/\/.*?\n/g,"").replace(/parent\./g,"window.opener.")+"document.write( window.opener._cke_htmlToLoad );document.close();window.opener._cke_htmlToLoad \x3d null;})() )",p="");
CKEDITOR.env.gecko&&(window._cke_htmlToLoad=i.dataValue,p=CKEDITOR.getUrl(b+"preview.html"));
p=window.open(p,null,"toolbar\x3dyes,location\x3dno,status\x3dyes,menubar\x3dyes,scrollbars\x3dyes,resizable\x3dyes,width\x3d"+m+",height\x3d"+h+",left\x3d"+o);
CKEDITOR.env.ie&&p&&(p.location=n);
CKEDITOR.env.ie||CKEDITOR.env.gecko||(n=p.document,n.open(),n.write(i.dataValue),n.close());
return !0
}};
CKEDITOR.plugins.add("preview",{init:function(c){c.elementMode!=CKEDITOR.ELEMENT_MODE_INLINE&&(b=this.path,c.addCommand("preview",a),c.ui.addButton&&c.ui.addButton("Preview",{label:c.lang.preview.preview,command:"preview",toolbar:"document,40"}))
}})
})();
CKEDITOR.plugins.add("print",{init:function(b){b.elementMode!=CKEDITOR.ELEMENT_MODE_INLINE&&(b.addCommand("print",CKEDITOR.plugins.print),b.ui.addButton&&b.ui.addButton("Print",{label:b.lang.print.toolbar,command:"print",toolbar:"document,50"}))
}});
CKEDITOR.plugins.print={exec:function(b){CKEDITOR.env.gecko?b.window.$.print():b.document.$.execCommand("Print")
},canUndo:!1,readOnly:1,modes:{wysiwyg:1}};
CKEDITOR.plugins.add("removeformat",{init:function(b){b.addCommand("removeFormat",CKEDITOR.plugins.removeformat.commands.removeformat);
b.ui.addButton&&b.ui.addButton("RemoveFormat",{label:b.lang.removeformat.toolbar,command:"removeFormat",toolbar:"cleanup,10"})
}});
CKEDITOR.plugins.removeformat={commands:{removeformat:{exec:function(x){for(var r=x._.removeFormatRegex||(x._.removeFormatRegex=new RegExp("^(?:"+x.config.removeFormatTags.replace(/,/g,"|")+")$","i")),t=x._.removeAttributes||(x._.removeAttributes=x.config.removeFormatAttributes.split(",")),s=CKEDITOR.plugins.removeformat.filter,j=x.getSelection().getRanges(),i=j.createIterator(),g=function(b){return b.type==CKEDITOR.NODE_ELEMENT
},v;
v=i.getNextRange();
){v.collapsed||v.enlarge(CKEDITOR.ENLARGE_ELEMENT);
var o=v.createBookmark(),w=o.startNode,u=o.endNode,q=function(a){for(var l=x.elementPath(a),h=l.elements,k=1,f;
(f=h[k])&&!f.equals(l.block)&&!f.equals(l.blockLimit);
k++){r.test(f.getName())&&s(x,f)&&a.breakParent(f)
}};
q(w);
if(u){for(q(u),w=w.getNextSourceNode(!0,CKEDITOR.NODE_ELEMENT);
w&&!w.equals(u);
){if(w.isReadOnly()){if(w.getPosition(u)&CKEDITOR.POSITION_CONTAINS){break
}w=w.getNext(g)
}else{q=w.getNextSourceNode(!1,CKEDITOR.NODE_ELEMENT),"img"==w.getName()&&w.data("cke-realelement")||!s(x,w)||(r.test(w.getName())?w.remove(1):(w.removeAttributes(t),x.fire("removeFormatCleanup",w))),w=q
}}}v.moveToBookmark(o)
}x.forceNextSelectionCheck();
x.getSelection().selectRanges(j)
}}},filter:function(b,c){for(var g=b._.removeFormatFilters||[],d=0;
d<g.length;
d++){if(!1===g[d](c)){return !1
}}return !0
}};
CKEDITOR.editor.prototype.addRemoveFormatFilter=function(b){this._.removeFormatFilters||(this._.removeFormatFilters=[]);
this._.removeFormatFilters.push(b)
};
CKEDITOR.config.removeFormatTags="b,big,cite,code,del,dfn,em,font,i,ins,kbd,q,s,samp,small,span,strike,strong,sub,sup,tt,u,var";
CKEDITOR.config.removeFormatAttributes="class,style,lang,width,height,align,hspace,valign";
(function(){var a={readOnly:1,exec:function(d){if(d.fire("save")&&(d=d.element.$.form)){try{d.submit()
}catch(c){d.submit.click&&d.submit.click()
}}}};
CKEDITOR.plugins.add("save",{init:function(b){b.elementMode==CKEDITOR.ELEMENT_MODE_REPLACE&&(b.addCommand("save",a).modes={wysiwyg:!!b.element.$.form},b.ui.addButton&&b.ui.addButton("Save",{label:b.lang.save.toolbar,command:"save",toolbar:"document,10"}))
}})
})();
(function(){CKEDITOR.plugins.add("selectall",{init:function(a){a.addCommand("selectAll",{modes:{wysiwyg:1,source:1},exec:function(e){var d=e.editable();
if(d.is("textarea")){e=d.$,CKEDITOR.env.ie?e.createTextRange().execCommand("SelectAll"):(e.selectionStart=0,e.selectionEnd=e.value.length),e.focus()
}else{if(d.is("body")){e.document.$.execCommand("SelectAll",!1,null)
}else{var f=e.createRange();
f.selectNodeContents(d);
f.select()
}e.forceNextSelectionCheck();
e.selectionChange()
}},canUndo:!1});
a.ui.addButton&&a.ui.addButton("SelectAll",{label:a.lang.selectall.toolbar,command:"selectAll",toolbar:"selection,10"})
}})
})();
(function(){var a={readOnly:1,preserveState:!0,editorFocus:!1,exec:function(b){this.toggleState();
this.refresh(b)
},refresh:function(b){if(b.document){var d=this.state!=CKEDITOR.TRISTATE_ON||b.elementMode==CKEDITOR.ELEMENT_MODE_INLINE&&!b.focusManager.hasFocus?"removeClass":"attachClass";
b.editable()[d]("cke_show_blocks")
}}};
CKEDITOR.plugins.add("showblocks",{onLoad:function(){var t="p div pre address blockquote h1 h2 h3 h4 h5 h6".split(" "),r,s,p,o,j=CKEDITOR.getUrl(this.path),i=!(CKEDITOR.env.ie&&9>CKEDITOR.env.version),n=i?":not([contenteditable\x3dfalse]):not(.cke_show_blocks_off)":"",q,m;
for(r=s=p=o="";
q=t.pop();
){m=t.length?",":"",r+=".cke_show_blocks "+q+n+m,p+=".cke_show_blocks.cke_contents_ltr "+q+n+m,o+=".cke_show_blocks.cke_contents_rtl "+q+n+m,s+=".cke_show_blocks "+q+n+"{background-image:url("+CKEDITOR.getUrl(j+"images/block_"+q+".png")+")}"
}CKEDITOR.addCss((r+"{background-repeat:no-repeat;border:1px dotted gray;padding-top:8px}").concat(s,p+"{background-position:top left;padding-left:8px}",o+"{background-position:top right;padding-right:8px}"));
i||CKEDITOR.addCss(".cke_show_blocks [contenteditable\x3dfalse],.cke_show_blocks .cke_show_blocks_off{border:none;padding-top:0;background-image:none}.cke_show_blocks.cke_contents_rtl [contenteditable\x3dfalse],.cke_show_blocks.cke_contents_rtl .cke_show_blocks_off{padding-right:0}.cke_show_blocks.cke_contents_ltr [contenteditable\x3dfalse],.cke_show_blocks.cke_contents_ltr .cke_show_blocks_off{padding-left:0}")
},init:function(e){function f(){d.refresh(e)
}if(!e.blockless){var d=e.addCommand("showblocks",a);
d.canUndo=!1;
e.config.startupOutlineBlocks&&d.setState(CKEDITOR.TRISTATE_ON);
e.ui.addButton&&e.ui.addButton("ShowBlocks",{label:e.lang.showblocks.toolbar,command:"showblocks",toolbar:"tools,20"});
e.on("mode",function(){d.state!=CKEDITOR.TRISTATE_DISABLED&&d.refresh(e)
});
e.elementMode==CKEDITOR.ELEMENT_MODE_INLINE&&(e.on("focus",f),e.on("blur",f));
e.on("contentDom",function(){d.state!=CKEDITOR.TRISTATE_DISABLED&&d.refresh(e)
})
}}})
})();
(function(){var a={preserveState:!0,editorFocus:!1,readOnly:1,exec:function(b){this.toggleState();
this.refresh(b)
},refresh:function(d){if(d.document){var c=this.state==CKEDITOR.TRISTATE_ON?"attachClass":"removeClass";
d.editable()[c]("cke_show_borders")
}}};
CKEDITOR.plugins.add("showborders",{modes:{wysiwyg:1},onLoad:function(){var b;
b=(CKEDITOR.env.ie6Compat?[".%1 table.%2,",".%1 table.%2 td, .%1 table.%2 th","{","border : #d3d3d3 1px dotted","}"]:".%1 table.%2,;.%1 table.%2 \x3e tr \x3e td, .%1 table.%2 \x3e tr \x3e th,;.%1 table.%2 \x3e tbody \x3e tr \x3e td, .%1 table.%2 \x3e tbody \x3e tr \x3e th,;.%1 table.%2 \x3e thead \x3e tr \x3e td, .%1 table.%2 \x3e thead \x3e tr \x3e th,;.%1 table.%2 \x3e tfoot \x3e tr \x3e td, .%1 table.%2 \x3e tfoot \x3e tr \x3e th;{;border : #d3d3d3 1px dotted;}".split(";")).join("").replace(/%2/g,"cke_show_border").replace(/%1/g,"cke_show_borders ");
CKEDITOR.addCss(b)
},init:function(d){var c=d.addCommand("showborders",a);
c.canUndo=!1;
!1!==d.config.startupShowBorders&&c.setState(CKEDITOR.TRISTATE_ON);
d.on("mode",function(){c.state!=CKEDITOR.TRISTATE_DISABLED&&c.refresh(d)
},null,null,100);
d.on("contentDom",function(){c.state!=CKEDITOR.TRISTATE_DISABLED&&c.refresh(d)
});
d.on("removeFormatCleanup",function(b){b=b.data;
d.getCommand("showborders").state==CKEDITOR.TRISTATE_ON&&b.is("table")&&(!b.hasAttribute("border")||0>=parseInt(b.getAttribute("border"),10))&&b.addClass("cke_show_border")
})
},afterInit:function(d){var c=d.dataProcessor;
d=c&&c.dataFilter;
c=c&&c.htmlFilter;
d&&d.addRules({elements:{table:function(f){f=f.attributes;
var e=f["class"],g=parseInt(f.border,10);
g&&!(0>=g)||e&&-1!=e.indexOf("cke_show_border")||(f["class"]=(e||"")+" cke_show_border")
}}});
c&&c.addRules({elements:{table:function(f){f=f.attributes;
var e=f["class"];
e&&(f["class"]=e.replace("cke_show_border","").replace(/\s{2}/," ").replace(/^\s+|\s+$/,""))
}}})
}});
CKEDITOR.on("dialogDefinition",function(d){var c=d.data.name;
if("table"==c||"tableProperties"==c){if(d=d.data.definition,c=d.getContents("info").get("txtBorder"),c.commit=CKEDITOR.tools.override(c.commit,function(b){return function(f,h){b.apply(this,arguments);
var g=parseInt(this.getValue(),10);
h[!g||0>=g?"addClass":"removeClass"]("cke_show_border")
}
}),d=(d=d.getContents("advanced"))&&d.get("advCSSClasses")){d.setup=CKEDITOR.tools.override(d.setup,function(b){return function(){b.apply(this,arguments);
this.setValue(this.getValue().replace(/cke_show_border/,""))
}
}),d.commit=CKEDITOR.tools.override(d.commit,function(b){return function(e,f){b.apply(this,arguments);
parseInt(f.getAttribute("border"),10)||f.addClass("cke_show_border")
}
})
}}})
})();
(function(){CKEDITOR.plugins.add("sourcearea",{init:function(b){function h(){var d=g&&this.equals(CKEDITOR.document.getActive());
this.hide();
this.setStyle("height",this.getParent().$.clientHeight+"px");
this.setStyle("width",this.getParent().$.clientWidth+"px");
this.show();
d&&this.focus()
}if(b.elementMode!=CKEDITOR.ELEMENT_MODE_INLINE){var c=CKEDITOR.plugins.sourcearea;
b.addMode("source",function(f){var d=b.ui.space("contents").getDocument().createElement("textarea");
d.setStyles(CKEDITOR.tools.extend({width:CKEDITOR.env.ie7Compat?"99%":"100%",height:"100%",resize:"none",outline:"none","text-align":"left"},CKEDITOR.tools.cssVendorPrefix("tab-size",b.config.sourceAreaTabSize||4)));
d.setAttribute("dir","ltr");
d.addClass("cke_source").addClass("cke_reset").addClass("cke_enable_context_menu");
b.ui.space("contents").append(d);
d=b.editable(new a(b,d));
d.setData(b.getData(1));
CKEDITOR.env.ie&&(d.attachListener(b,"resize",h,d),d.attachListener(CKEDITOR.document.getWindow(),"resize",h,d),CKEDITOR.tools.setTimeout(h,0,d));
b.fire("ariaWidget",this);
f()
});
b.addCommand("source",c.commands.source);
b.ui.addButton&&b.ui.addButton("Source",{label:b.lang.sourcearea.toolbar,command:"source",toolbar:"mode,10"});
b.on("mode",function(){b.getCommand("source").setState("source"==b.mode?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF)
});
var g=CKEDITOR.env.ie&&9==CKEDITOR.env.version
}}});
var a=CKEDITOR.tools.createClass({base:CKEDITOR.editable,proto:{setData:function(b){this.setValue(b);
this.status="ready";
this.editor.fire("dataReady")
},getData:function(){return this.getValue()
},insertHtml:function(){},insertElement:function(){},insertText:function(){},setReadOnly:function(b){this[(b?"set":"remove")+"Attribute"]("readOnly","readonly")
},detach:function(){a.baseProto.detach.call(this);
this.clearCustomData();
this.remove()
}}})
})();
CKEDITOR.plugins.sourcearea={commands:{source:{modes:{wysiwyg:1,source:1},editorFocus:!1,readOnly:1,exec:function(a){"wysiwyg"==a.mode&&a.fire("saveSnapshot");
a.getCommand("source").setState(CKEDITOR.TRISTATE_DISABLED);
a.setMode("source"==a.mode?"wysiwyg":"source")
},canUndo:!1}}};
CKEDITOR.plugins.add("specialchar",{availableLangs:{af:1,ar:1,bg:1,ca:1,cs:1,cy:1,da:1,de:1,el:1,en:1,"en-gb":1,eo:1,es:1,et:1,fa:1,fi:1,fr:1,"fr-ca":1,gl:1,he:1,hr:1,hu:1,id:1,it:1,ja:1,km:1,ko:1,ku:1,lt:1,lv:1,nb:1,nl:1,no:1,pl:1,pt:1,"pt-br":1,ru:1,si:1,sk:1,sl:1,sq:1,sv:1,th:1,tr:1,tt:1,ug:1,uk:1,vi:1,zh:1,"zh-cn":1},requires:"dialog",init:function(b){var d=this;
CKEDITOR.dialog.add("specialchar",this.path+"dialogs/specialchar.js");
b.addCommand("specialchar",{exec:function(){var a=b.langCode,a=d.availableLangs[a]?a:d.availableLangs[a.replace(/-.*/,"")]?a.replace(/-.*/,""):"en";
CKEDITOR.scriptLoader.load(CKEDITOR.getUrl(d.path+"dialogs/lang/"+a+".js"),function(){CKEDITOR.tools.extend(b.lang.specialchar,d.langEntries[a]);
b.openDialog("specialchar")
})
},modes:{wysiwyg:1},canUndo:!1});
b.ui.addButton&&b.ui.addButton("SpecialChar",{label:b.lang.specialchar.toolbar,command:"specialchar",toolbar:"insert,50"})
}});
CKEDITOR.config.specialChars="! \x26quot; # $ % \x26amp; ' ( ) * + - . / 0 1 2 3 4 5 6 7 8 9 : ; \x26lt; \x3d \x26gt; ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ \x26euro; \x26lsquo; \x26rsquo; \x26ldquo; \x26rdquo; \x26ndash; \x26mdash; \x26iexcl; \x26cent; \x26pound; \x26curren; \x26yen; \x26brvbar; \x26sect; \x26uml; \x26copy; \x26ordf; \x26laquo; \x26not; \x26reg; \x26macr; \x26deg; \x26sup2; \x26sup3; \x26acute; \x26micro; \x26para; \x26middot; \x26cedil; \x26sup1; \x26ordm; \x26raquo; \x26frac14; \x26frac12; \x26frac34; \x26iquest; \x26Agrave; \x26Aacute; \x26Acirc; \x26Atilde; \x26Auml; \x26Aring; \x26AElig; \x26Ccedil; \x26Egrave; \x26Eacute; \x26Ecirc; \x26Euml; \x26Igrave; \x26Iacute; \x26Icirc; \x26Iuml; \x26ETH; \x26Ntilde; \x26Ograve; \x26Oacute; \x26Ocirc; \x26Otilde; \x26Ouml; \x26times; \x26Oslash; \x26Ugrave; \x26Uacute; \x26Ucirc; \x26Uuml; \x26Yacute; \x26THORN; \x26szlig; \x26agrave; \x26aacute; \x26acirc; \x26atilde; \x26auml; \x26aring; \x26aelig; \x26ccedil; \x26egrave; \x26eacute; \x26ecirc; \x26euml; \x26igrave; \x26iacute; \x26icirc; \x26iuml; \x26eth; \x26ntilde; \x26ograve; \x26oacute; \x26ocirc; \x26otilde; \x26ouml; \x26divide; \x26oslash; \x26ugrave; \x26uacute; \x26ucirc; \x26uuml; \x26yacute; \x26thorn; \x26yuml; \x26OElig; \x26oelig; \x26#372; \x26#374 \x26#373 \x26#375; \x26sbquo; \x26#8219; \x26bdquo; \x26hellip; \x26trade; \x26#9658; \x26bull; \x26rarr; \x26rArr; \x26hArr; \x26diams; \x26asymp;".split(" ");
CKEDITOR.plugins.add("scayt",{requires:"menubutton,dialog",tabToOpen:null,dialogName:"scaytDialog",init:function(g){var j=this,i=CKEDITOR.plugins.scayt;
this.bindEvents(g);
this.parseConfig(g);
this.addRule(g);
CKEDITOR.dialog.add(this.dialogName,CKEDITOR.getUrl(this.path+"dialogs/options.js"));
this.addMenuItems(g);
var e=g.lang.scayt,h=CKEDITOR.env;
g.ui.add("Scayt",CKEDITOR.UI_MENUBUTTON,{label:e.text_title,title:g.plugins.wsc?g.lang.wsc.title:e.text_title,modes:{wysiwyg:!(h.ie&&(8>h.version||h.quirks))},toolbar:"spellchecker,20",refresh:function(){var a=g.ui.instances.Scayt.getState();
g.scayt&&(a=i.state[g.name]?CKEDITOR.TRISTATE_ON:CKEDITOR.TRISTATE_OFF);
g.fire("scaytButtonState",a)
},onRender:function(){var a=this;
g.on("scaytButtonState",function(b){void 0!==typeof b.data&&a.setState(b.data)
})
},onMenu:function(){var a=g.scayt;
g.getMenuItem("scaytToggle").label=g.lang.scayt[a&&i.state[g.name]?"btn_disable":"btn_enable"];
a={scaytToggle:CKEDITOR.TRISTATE_OFF,scaytOptions:a?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,scaytLangs:a?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,scaytDict:a?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,scaytAbout:a?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,WSC:g.plugins.wsc?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED};
g.config.scayt_uiTabs[0]||delete a.scaytOptions;
g.config.scayt_uiTabs[1]||delete a.scaytLangs;
g.config.scayt_uiTabs[2]||delete a.scaytDict;
return a
}});
g.contextMenu&&g.addMenuItems&&(g.contextMenu.addListener(function(n,a){var m=g.scayt,k;
if(m){var c=m.getSelectionNode();
if(c=c?c.getAttribute(m.getNodeAttribute()):c){k=j.menuGenerator(g,c,j),m.showBanner("."+g.contextMenu._.definition.panel.className.split(" ").join(" ."))
}}return k
}),g.contextMenu._.onHide=CKEDITOR.tools.override(g.contextMenu._.onHide,function(a){return function(){var c=g.scayt;
c&&c.hideBanner();
return a.apply(this)
}
}))
},addMenuItems:function(g){var j=this,i=CKEDITOR.plugins.scayt;
g.addMenuGroup("scaytButton");
var e=g.config.scayt_contextMenuItemsOrder.split("|");
if(e&&e.length){for(var h=0;
h<e.length;
h++){g.addMenuGroup("scayt_"+e[h],h-10)
}}g.addCommand("scaytToggle",{exec:function(d){var c=d.scayt;
i.state[d.name]=!i.state[d.name];
!0===i.state[d.name]?c||i.createScayt(d):c&&i.destroy(d)
}});
g.addCommand("scaytAbout",{exec:function(b){b.scayt.tabToOpen="about";
b.lockSelection();
b.openDialog(j.dialogName)
}});
g.addCommand("scaytOptions",{exec:function(b){b.scayt.tabToOpen="options";
b.lockSelection();
b.openDialog(j.dialogName)
}});
g.addCommand("scaytLangs",{exec:function(b){b.scayt.tabToOpen="langs";
b.lockSelection();
b.openDialog(j.dialogName)
}});
g.addCommand("scaytDict",{exec:function(b){b.scayt.tabToOpen="dictionaries";
b.lockSelection();
b.openDialog(j.dialogName)
}});
e={scaytToggle:{label:g.lang.scayt.btn_enable,group:"scaytButton",command:"scaytToggle"},scaytAbout:{label:g.lang.scayt.btn_about,group:"scaytButton",command:"scaytAbout"},scaytOptions:{label:g.lang.scayt.btn_options,group:"scaytButton",command:"scaytOptions"},scaytLangs:{label:g.lang.scayt.btn_langs,group:"scaytButton",command:"scaytLangs"},scaytDict:{label:g.lang.scayt.btn_dictionaries,group:"scaytButton",command:"scaytDict"}};
g.plugins.wsc&&(e.WSC={label:g.lang.wsc.toolbar,group:"scaytButton",onClick:function(){var f=CKEDITOR.plugins.scayt,a=g.scayt,k=g.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?g.container.getText():g.document.getBody().getText();
(k=k.replace(/\s/g,""))?(a&&f.state[g.name]&&a.setMarkupPaused&&a.setMarkupPaused(!0),g.lockSelection(),g.execCommand("checkspell")):alert("Nothing to check!")
}});
g.addMenuItems(e)
},bindEvents:function(i){function p(){var a=i.scayt;
a&&(a.removeMarkupInSelectionNode(),a.fire("startSpellCheck"))
}var o=CKEDITOR.plugins.scayt,h=i.elementMode==CKEDITOR.ELEMENT_MODE_INLINE,m=function(){o.destroy(i)
},n=function(){!o.state[i.name]||i.readOnly||i.scayt||o.createScayt(i)
},l=function(){var a=i.editable();
a.attachListener(a,"focus",function(g){CKEDITOR.plugins.scayt&&!i.scayt&&setTimeout(n,0);
g=CKEDITOR.plugins.scayt&&CKEDITOR.plugins.scayt.state[i.name]&&i.scayt;
var k,b;
if((h||g)&&i._.savedSelection){g=i._.savedSelection.getSelectedElement();
g=!g&&i._.savedSelection.getRanges();
for(var e=0;
e<g.length;
e++){b=g[e],"string"===typeof b.startContainer.$.nodeValue&&(k=b.startContainer.getText().length,(k<b.startOffset||k<b.endOffset)&&i.unlockSelection(!1))
}}},this,null,-10)
},j=function(){h?(i.on("blur",m),i.on("focus",n),i.focusManager.hasFocus&&n()):n();
l()
};
i.on("contentDom",j);
i.on("beforeCommandExec",function(a){var d;
if(a.data.name in o.options.disablingCommandExec&&"wysiwyg"==i.mode){if(d=i.scayt){o.destroy(i),i.fire("scaytButtonState",CKEDITOR.TRISTATE_DISABLED)
}}else{if("bold"===a.data.name||"italic"===a.data.name||"underline"===a.data.name||"strike"===a.data.name||"subscript"===a.data.name||"superscript"===a.data.name||"enter"===a.data.name){if(d=i.scayt){d.removeMarkupInSelectionNode(),setTimeout(function(){d.fire("startSpellCheck")
},0)
}}}});
i.on("beforeSetMode",function(a){if("source"==a.data){if(a=i.scayt){o.destroy(i),i.fire("scaytButtonState",CKEDITOR.TRISTATE_DISABLED)
}i.document&&i.document.getBody().removeAttribute("_jquid")
}});
i.on("afterCommandExec",function(c){var a;
"wysiwyg"!=i.mode||"undo"!=c.data.name&&"redo"!=c.data.name||(a=i.scayt)&&setTimeout(function(){a.fire("startSpellCheck")
},250)
});
i.on("readOnly",function(a){var d;
a&&(d=i.scayt,!0===a.editor.readOnly?d&&d.fire("removeMarkupInDocument",{}):d?d.fire("startSpellCheck"):"wysiwyg"==a.editor.mode&&!0===o.state[a.editor.name]&&(o.createScayt(i),a.editor.fire("scaytButtonState",CKEDITOR.TRISTATE_ON)))
});
i.on("beforeDestroy",m);
i.on("setData",function(){m();
(i.elementMode==CKEDITOR.ELEMENT_MODE_INLINE||i.plugins.divarea)&&j()
},this,null,50);
i.on("insertElement",function(){CKEDITOR.env.ie?setTimeout(function(){p()
},50):p()
},this,null,50);
i.on("insertHtml",function(){p()
},this,null,50);
i.on("insertText",function(){p()
},this,null,50);
i.on("scaytDialogShown",function(a){a.data.selectPage(i.scayt.tabToOpen)
});
i.on("paste",function(a){(a=i.scayt)&&a.removeMarkupInSelectionNode()
},null,null,0)
},parseConfig:function(i){var p=CKEDITOR.plugins.scayt;
p.replaceOldOptionsNames(i.config);
"boolean"!==typeof i.config.scayt_autoStartup&&(i.config.scayt_autoStartup=!1);
p.state[i.name]=i.config.scayt_autoStartup;
i.config.scayt_contextCommands||(i.config.scayt_contextCommands="ignore|ignoreall|add");
i.config.scayt_contextMenuItemsOrder||(i.config.scayt_contextMenuItemsOrder="suggest|moresuggest|control");
i.config.scayt_sLang||(i.config.scayt_sLang="en_US");
if(void 0===i.config.scayt_maxSuggestions||"number"!=typeof i.config.scayt_maxSuggestions||0>i.config.scayt_maxSuggestions){i.config.scayt_maxSuggestions=5
}if(void 0===i.config.scayt_minWordLength||"number"!=typeof i.config.scayt_minWordLength||1>i.config.scayt_minWordLength){i.config.scayt_minWordLength=4
}if(void 0===i.config.scayt_customDictionaryIds||"string"!==typeof i.config.scayt_customDictionaryIds){i.config.scayt_customDictionaryIds=""
}if(void 0===i.config.scayt_userDictionaryName||"string"!==typeof i.config.scayt_userDictionaryName){i.config.scayt_userDictionaryName=null
}if("string"===typeof i.config.scayt_uiTabs&&3===i.config.scayt_uiTabs.split(",").length){var o=[],h=[];
i.config.scayt_uiTabs=i.config.scayt_uiTabs.split(",");
CKEDITOR.tools.search(i.config.scayt_uiTabs,function(b){1===Number(b)||0===Number(b)?(h.push(!0),o.push(Number(b))):h.push(!1)
});
null===CKEDITOR.tools.search(h,!1)?i.config.scayt_uiTabs=o:i.config.scayt_uiTabs=[1,1,1]
}else{i.config.scayt_uiTabs=[1,1,1]
}"string"!=typeof i.config.scayt_serviceProtocol&&(i.config.scayt_serviceProtocol=null);
"string"!=typeof i.config.scayt_serviceHost&&(i.config.scayt_serviceHost=null);
"string"!=typeof i.config.scayt_servicePort&&(i.config.scayt_servicePort=null);
"string"!=typeof i.config.scayt_servicePath&&(i.config.scayt_servicePath=null);
i.config.scayt_moreSuggestions||(i.config.scayt_moreSuggestions="on");
"string"!==typeof i.config.scayt_customerId&&(i.config.scayt_customerId="1:WvF0D4-UtPqN1-43nkD4-NKvUm2-daQqk3-LmNiI-z7Ysb4-mwry24-T8YrS3-Q2tpq2");
"string"!==typeof i.config.scayt_srcUrl&&(p=document.location.protocol,p=-1!=p.search(/https?:/)?p:"http:",i.config.scayt_srcUrl=p+"//svc.webspellchecker.net/spellcheck31/lf/scayt3/ckscayt/ckscayt.js");
"boolean"!==typeof CKEDITOR.config.scayt_handleCheckDirty&&(CKEDITOR.config.scayt_handleCheckDirty=!0);
"boolean"!==typeof CKEDITOR.config.scayt_handleUndoRedo&&(CKEDITOR.config.scayt_handleUndoRedo=!0);
if(i.config.scayt_disableOptionsStorage){var p=CKEDITOR.tools.isArray(i.config.scayt_disableOptionsStorage)?i.config.scayt_disableOptionsStorage:"string"===typeof i.config.scayt_disableOptionsStorage?[i.config.scayt_disableOptionsStorage]:void 0,m="all options lang ignore-all-caps-words ignore-domain-names ignore-words-with-mixed-cases ignore-words-with-numbers".split(" "),n=["lang","ignore-all-caps-words","ignore-domain-names","ignore-words-with-mixed-cases","ignore-words-with-numbers"],l=CKEDITOR.tools.search,j=CKEDITOR.tools.indexOf;
i.config.scayt_disableOptionsStorage=function(g){for(var k=[],f=0;
f<g.length;
f++){var q=g[f],e=!!l(g,"options");
if(!l(m,q)||e&&l(n,function(b){if("lang"===b){return !1
}})){return
}l(n,q)&&n.splice(j(n,q),1);
if("all"===q||e&&l(g,"lang")){return[]
}"options"===q&&(n=["lang"])
}return k=k.concat(n)
}(p)
}},addRule:function(i){var n=i.dataProcessor,m=n&&n.htmlFilter,h=i._.elementsPath&&i._.elementsPath.filters,n=n&&n.dataFilter,k=i.addRemoveFormatFilter,l=function(c){var a=CKEDITOR.plugins.scayt;
if(i.scayt&&c.hasAttribute(a.options.data_attribute_name)){return !1
}},j=function(e){var a=CKEDITOR.plugins.scayt,f=!0;
i.scayt&&e.hasAttribute(a.options.data_attribute_name)&&(f=!1);
return f
};
h&&h.push(l);
n&&n.addRules({elements:{span:function(c){var a=CKEDITOR.plugins.scayt;
a&&a.state[i.name]&&c.classes&&CKEDITOR.tools.search(c.classes,a.options.misspelled_word_class)&&(c.classes&&c.parent.type===CKEDITOR.NODE_DOCUMENT_FRAGMENT?(delete c.attributes.style,delete c.name):delete c.classes[CKEDITOR.tools.indexOf(c.classes,a.options.misspelled_word_class)]);
return c
}}});
m&&m.addRules({elements:{span:function(c){var a=CKEDITOR.plugins.scayt;
a&&a.state[i.name]&&c.hasClass(a.options.misspelled_word_class)&&c.attributes[a.options.data_attribute_name]&&(c.removeClass(a.options.misspelled_word_class),delete c.attributes[a.options.data_attribute_name],delete c.name);
return c
}}});
k&&k.call(i,j)
},scaytMenuDefinition:function(b){var d=this;
b=b.scayt;
return{scayt_ignore:{label:b.getLocal("btn_ignore"),group:"scayt_control",order:1,exec:function(c){c.scayt.ignoreWord()
}},scayt_ignoreall:{label:b.getLocal("btn_ignoreAll"),group:"scayt_control",order:2,exec:function(c){c.scayt.ignoreAllWords()
}},scayt_add:{label:b.getLocal("btn_addWord"),group:"scayt_control",order:3,exec:function(e){var c=e.scayt;
setTimeout(function(){c.addWordToUserDictionary()
},10)
}},option:{label:b.getLocal("btn_options"),group:"scayt_control",order:4,exec:function(c){c.scayt.tabToOpen="options";
c.lockSelection();
c.openDialog(d.dialogName)
},verification:function(c){return 1==c.config.scayt_uiTabs[0]?!0:!1
}},language:{label:b.getLocal("btn_langs"),group:"scayt_control",order:5,exec:function(c){c.scayt.tabToOpen="langs";
c.lockSelection();
c.openDialog(d.dialogName)
},verification:function(c){return 1==c.config.scayt_uiTabs[1]?!0:!1
}},dictionary:{label:b.getLocal("btn_dictionaries"),group:"scayt_control",order:6,exec:function(c){c.scayt.tabToOpen="dictionaries";
c.lockSelection();
c.openDialog(d.dialogName)
},verification:function(c){return 1==c.config.scayt_uiTabs[2]?!0:!1
}},about:{label:b.getLocal("btn_about"),group:"scayt_control",order:7,exec:function(c){c.scayt.tabToOpen="about";
c.lockSelection();
c.openDialog(d.dialogName)
}}}
},buildSuggestionMenuItems:function(i,n){var m={},h={},k=i.scayt;
if(0<n.length&&"no_any_suggestions"!==n[0]){for(var l=0;
l<n.length;
l++){var j="scayt_suggest_"+CKEDITOR.plugins.scayt.suggestions[l].replace(" ","_");
i.addCommand(j,this.createCommand(CKEDITOR.plugins.scayt.suggestions[l]));
l<i.config.scayt_maxSuggestions?(i.addMenuItem(j,{label:n[l],command:j,group:"scayt_suggest",order:l+1}),m[j]=CKEDITOR.TRISTATE_OFF):(i.addMenuItem(j,{label:n[l],command:j,group:"scayt_moresuggest",order:l+1}),h[j]=CKEDITOR.TRISTATE_OFF,"on"===i.config.scayt_moreSuggestions&&(i.addMenuItem("scayt_moresuggest",{label:k.getLocal("btn_moreSuggestions"),group:"scayt_moresuggest",order:10,getItems:function(){return h
}}),m.scayt_moresuggest=CKEDITOR.TRISTATE_OFF))
}}else{m.no_scayt_suggest=CKEDITOR.TRISTATE_DISABLED,i.addCommand("no_scayt_suggest",{exec:function(){}}),i.addMenuItem("no_scayt_suggest",{label:k.getLocal("btn_noSuggestions")||"no_scayt_suggest",command:"no_scayt_suggest",group:"scayt_suggest",order:0})
}return m
},menuGenerator:function(i,n){var m=i.scayt,h=this.scaytMenuDefinition(i),k={},l=i.config.scayt_contextCommands.split("|");
m.fire("getSuggestionsList",{lang:m.getLang(),word:n});
k=this.buildSuggestionMenuItems(i,CKEDITOR.plugins.scayt.suggestions);
if("off"==i.config.scayt_contextCommands){return k
}for(var j in h){if(-1!=CKEDITOR.tools.indexOf(l,j.replace("scayt_",""))||"all"==i.config.scayt_contextCommands){k[j]=CKEDITOR.TRISTATE_OFF,"function"!==typeof h[j].verification||h[j].verification(i)||delete k[j],i.addCommand(j,{exec:h[j].exec}),i.addMenuItem(j,{label:i.lang.scayt[h[j].label]||h[j].label,command:j,group:h[j].group,order:h[j].order})
}}return k
},createCommand:function(b){return{exec:function(a){a.scayt.replaceSelectionNode({word:b})
}}
}});
CKEDITOR.plugins.scayt={state:{},suggestions:[],loadingHelper:{loadOrder:[]},isLoading:!1,options:{disablingCommandExec:{source:!0,newpage:!0,templates:!0},data_attribute_name:"data-scayt-word",misspelled_word_class:"scayt-misspell-word"},backCompatibilityMap:{scayt_service_protocol:"scayt_serviceProtocol",scayt_service_host:"scayt_serviceHost",scayt_service_port:"scayt_servicePort",scayt_service_path:"scayt_servicePath",scayt_customerid:"scayt_customerId"},replaceOldOptionsNames:function(b){for(var d in b){d in this.backCompatibilityMap&&(b[this.backCompatibilityMap[d]]=b[d],delete b[d])
}},createScayt:function(b){var d=this;
this.loadScaytLibrary(b,function(e){var c={lang:e.config.scayt_sLang,container:"BODY"==e.editable().$.nodeName?e.document.getWindow().$.frameElement:e.editable().$,customDictionary:e.config.scayt_customDictionaryIds,userDictionaryName:e.config.scayt_userDictionaryName,localization:e.langCode,customer_id:e.config.scayt_customerId,debug:e.config.scayt_debug,data_attribute_name:d.options.data_attribute_name,misspelled_word_class:d.options.misspelled_word_class,"options-to-restore":e.config.scayt_disableOptionsStorage,focused:e.editable().hasFocus,ignoreElementsRegex:e.config.scayt_elementsToIgnore,minWordLength:e.config.scayt_minWordLength};
e.config.scayt_serviceProtocol&&(c.service_protocol=e.config.scayt_serviceProtocol);
e.config.scayt_serviceHost&&(c.service_host=e.config.scayt_serviceHost);
e.config.scayt_servicePort&&(c.service_port=e.config.scayt_servicePort);
e.config.scayt_servicePath&&(c.service_path=e.config.scayt_servicePath);
c=new SCAYT.CKSCAYT(c,function(){},function(){});
c.subscribe("suggestionListSend",function(g){for(var f={},h=[],i=0;
i<g.suggestionList.length;
i++){f["word_"+g.suggestionList[i]]||(f["word_"+g.suggestionList[i]]=g.suggestionList[i],h.push(g.suggestionList[i]))
}CKEDITOR.plugins.scayt.suggestions=h
});
c.subscribe("selectionIsChanged",function(a){e.getSelection().isLocked&&e.lockSelection()
});
e.scayt=c;
e.fire("scaytButtonState",e.readOnly?CKEDITOR.TRISTATE_DISABLED:CKEDITOR.TRISTATE_ON)
})
},destroy:function(b){b.scayt&&b.scayt.destroy();
delete b.scayt;
b.fire("scaytButtonState",CKEDITOR.TRISTATE_OFF)
},loadScaytLibrary:function(f,h){var g=this,e;
"undefined"===typeof window.SCAYT||"function"!==typeof window.SCAYT.CKSCAYT?this.loadingHelper[f.name]||(this.loadingHelper[f.name]=h,this.loadingHelper.loadOrder.push(f.name),e=new Date,e=e.getTime(),e=f.config.scayt_srcUrl+"?"+e,CKEDITOR.scriptLoader.load(e,function(d){CKEDITOR.fireOnce("scaytReady");
for(var c=0;
c<g.loadingHelper.loadOrder.length;
c++){d=g.loadingHelper.loadOrder[c];
if("function"===typeof g.loadingHelper[d]){g.loadingHelper[d](CKEDITOR.instances[d])
}delete g.loadingHelper[d]
}g.loadingHelper.loadOrder=[]
})):window.SCAYT&&"function"===typeof window.SCAYT.CKSCAYT&&(CKEDITOR.fireOnce("scaytReady"),f.scayt||"function"===typeof h&&h(f))
}};
CKEDITOR.on("dialogDefinition",function(b){if("scaytDialog"===b.data.name){b.data.definition.dialog.on("cancel",function(c){return !1
},this,null,-1)
}});
CKEDITOR.on("scaytReady",function(){if(!0===CKEDITOR.config.scayt_handleCheckDirty){var b=CKEDITOR.editor.prototype;
b.checkDirty=CKEDITOR.tools.override(b.checkDirty,function(c){return function(){var a=null,g=this.scayt;
if(CKEDITOR.plugins.scayt&&CKEDITOR.plugins.scayt.state[this.name]&&this.scayt){if(a="ready"==this.status){var f=g.removeMarkupFromString(this.getSnapshot()),g=g.removeMarkupFromString(this._.previousValue),a=a&&g!==f
}}else{a=c.call(this)
}return a
}
});
b.resetDirty=CKEDITOR.tools.override(b.resetDirty,function(c){return function(){var a=this.scayt;
CKEDITOR.plugins.scayt&&CKEDITOR.plugins.scayt.state[this.name]&&this.scayt?this._.previousValue=a.removeMarkupFromString(this.getSnapshot()):c.call(this)
}
})
}if(!0===CKEDITOR.config.scayt_handleUndoRedo){var b=CKEDITOR.plugins.undo.Image.prototype,d="function"==typeof b.equalsContent?"equalsContent":"equals";
b[d]=CKEDITOR.tools.override(b[d],function(c){return function(a){var j=a.editor.scayt,i=this.contents,h=a.contents,f=null;
CKEDITOR.plugins.scayt&&CKEDITOR.plugins.scayt.state[a.editor.name]&&a.editor.scayt&&(this.contents=j.removeMarkupFromString(i)||"",a.contents=j.removeMarkupFromString(h)||"");
f=c.apply(this,arguments);
this.contents=i;
a.contents=h;
return f
}
})
}});
(function(){CKEDITOR.plugins.add("stylescombo",{requires:"richcombo",init:function(i){var b=i.config,e=i.lang.stylescombo,h={},d=[],a=[];
i.on("stylesSet",function(c){if(c=c.data.styles){for(var f,g,k,j=0,l=c.length;
j<l;
j++){(f=c[j],i.blockless&&f.element in CKEDITOR.dtd.$block||(g=f.name,f=new CKEDITOR.style(f),i.filter.customConfig&&!i.filter.check(f)))||(f._name=g,f._.enterMode=b.enterMode,f._.type=k=f.assignedTo||f.type,f._.weight=j+1000*(k==CKEDITOR.STYLE_OBJECT?1:k==CKEDITOR.STYLE_BLOCK?2:3),h[g]=f,d.push(f),a.push(f))
}d.sort(function(n,m){return n._.weight-m._.weight
})
}});
i.ui.addRichCombo("Styles",{label:e.label,title:e.panelTitle,toolbar:"styles,10",allowedContent:a,panel:{css:[CKEDITOR.skin.getPath("editor")].concat(b.contentsCss),multiSelect:!0,attributes:{"aria-label":e.panelTitle}},init:function(){var g,j,n,m,l,k;
l=0;
for(k=d.length;
l<k;
l++){g=d[l],j=g._name,m=g._.type,m!=n&&(this.startGroup(e["panelTitle"+String(m)]),n=m),this.add(j,g.type==CKEDITOR.STYLE_OBJECT?j:g.buildPreview(),j)
}this.commit()
},onClick:function(c){i.focus();
i.fire("saveSnapshot");
c=h[c];
var f=i.elementPath();
i[c.checkActive(f,i)?"removeStyle":"applyStyle"](c);
i.fire("saveSnapshot")
},onRender:function(){i.on("selectionChange",function(c){var f=this.getValue();
c=c.data.path.elements;
for(var j=0,m=c.length,l;
j<m;
j++){l=c[j];
for(var k in h){if(h[k].checkElementRemovable(l,!0,i)){k!=f&&this.setValue(k);
return
}}}this.setValue("")
},this)
},onOpen:function(){var c=i.getSelection().getSelectedElement(),c=i.elementPath(c),f=[0,0,0,0];
this.showAll();
this.unmarkAll();
for(var g in h){var k=h[g],j=k._.type;
k.checkApplicable(c,i,i.activeFilter)?f[j]++:this.hideItem(g);
k.checkActive(c,i)&&this.mark(g)
}f[CKEDITOR.STYLE_BLOCK]||this.hideGroup(e["panelTitle"+String(CKEDITOR.STYLE_BLOCK)]);
f[CKEDITOR.STYLE_INLINE]||this.hideGroup(e["panelTitle"+String(CKEDITOR.STYLE_INLINE)]);
f[CKEDITOR.STYLE_OBJECT]||this.hideGroup(e["panelTitle"+String(CKEDITOR.STYLE_OBJECT)])
},refresh:function(){var c=i.elementPath();
if(c){for(var f in h){if(h[f].checkApplicable(c,i,i.activeFilter)){return
}}this.setState(CKEDITOR.TRISTATE_DISABLED)
}},reset:function(){h={};
d=[]
}})
}})
})();
(function(){function a(e){return{editorFocus:!1,canUndo:!1,modes:{wysiwyg:1},exec:function(o){if(o.editable().hasFocus){var n=o.getSelection(),i;
if(i=(new CKEDITOR.dom.elementPath(n.getCommonAncestor(),n.root)).contains({td:1,th:1},1)){var n=o.createRange(),j=CKEDITOR.tools.tryThese(function(){var f=i.getParent().$.cells[i.$.cellIndex+(e?-1:1)];
f.parentNode.parentNode;
return f
},function(){var f=i.getParent(),f=f.getAscendant("table").$.rows[f.$.rowIndex+(e?-1:1)];
return f.cells[e?f.cells.length-1:0]
});
if(j||e){if(j){j=new CKEDITOR.dom.element(j),n.moveToElementEditStart(j),n.checkStartOfBlock()&&n.checkEndOfBlock()||n.selectNodeContents(j)
}else{return !0
}}else{for(var m=i.getAscendant("table").$,j=i.getParent().$.cells,m=new CKEDITOR.dom.element(m.insertRow(-1),o.document),l=0,k=j.length;
l<k;
l++){m.append((new CKEDITOR.dom.element(j[l],o.document)).clone(!1,!1)).appendBogus()
}n.moveToElementEditStart(m)
}n.select(!0);
return !0
}}return !1
}}
}var b={editorFocus:!1,modes:{wysiwyg:1,source:1}},c={exec:function(e){e.container.focusNext(!0,e.tabIndex)
}},d={exec:function(e){e.container.focusPrevious(!0,e.tabIndex)
}};
CKEDITOR.plugins.add("tab",{init:function(i){for(var h=!1!==i.config.enableTabKeyTools,g=i.config.tabSpaces||0,f="";
g--;
){f+=" "
}if(f){i.on("key",function(e){9==e.data.keyCode&&(i.insertText(f),e.cancel())
})
}if(h){i.on("key",function(e){(9==e.data.keyCode&&i.execCommand("selectNextCell")||e.data.keyCode==CKEDITOR.SHIFT+9&&i.execCommand("selectPreviousCell"))&&e.cancel()
})
}i.addCommand("blur",CKEDITOR.tools.extend(c,b));
i.addCommand("blurBack",CKEDITOR.tools.extend(d,b));
i.addCommand("selectNextCell",a());
i.addCommand("selectPreviousCell",a(!0))
}})
})();
CKEDITOR.dom.element.prototype.focusNext=function(i,j){var l=void 0===j?this.getTabIndex():j,m,p,o,n,q,r;
if(0>=l){for(q=this.getNextSourceNode(i,CKEDITOR.NODE_ELEMENT);
q;
){if(q.isVisible()&&0===q.getTabIndex()){o=q;
break
}q=q.getNextSourceNode(!1,CKEDITOR.NODE_ELEMENT)
}}else{for(q=this.getDocument().getBody().getFirst();
q=q.getNextSourceNode(!1,CKEDITOR.NODE_ELEMENT);
){if(!m){if(!p&&q.equals(this)){if(p=!0,i){if(!(q=q.getNextSourceNode(!0,CKEDITOR.NODE_ELEMENT))){break
}m=1
}}else{p&&!this.contains(q)&&(m=1)
}}if(q.isVisible()&&!(0>(r=q.getTabIndex()))){if(m&&r==l){o=q;
break
}r>l&&(!o||!n||r<n)?(o=q,n=r):o||0!==r||(o=q,n=r)
}}}o&&o.focus()
};
CKEDITOR.dom.element.prototype.focusPrevious=function(i,j){for(var l=void 0===j?this.getTabIndex():j,m,p,o,n=0,q,r=this.getDocument().getBody().getLast();
r=r.getPreviousSourceNode(!1,CKEDITOR.NODE_ELEMENT);
){if(!m){if(!p&&r.equals(this)){if(p=!0,i){if(!(r=r.getPreviousSourceNode(!0,CKEDITOR.NODE_ELEMENT))){break
}m=1
}}else{p&&!this.contains(r)&&(m=1)
}}if(r.isVisible()&&!(0>(q=r.getTabIndex()))){if(0>=l){if(m&&0===q){o=r;
break
}q>n&&(o=r,n=q)
}else{if(m&&q==l){o=r;
break
}q<l&&(!o||q>n)&&(o=r,n=q)
}}}o&&o.focus()
};
CKEDITOR.plugins.add("table",{requires:"dialog",init:function(b){function d(c){return CKEDITOR.tools.extend(c||{},{contextSensitive:1,refresh:function(e,g){this.setState(g.contains("table",1)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED)
}})
}if(!b.blockless){var f=b.lang.table;
b.addCommand("table",new CKEDITOR.dialogCommand("table",{context:"table",allowedContent:"table{width,height}[align,border,cellpadding,cellspacing,summary];caption tbody thead tfoot;th td tr[scope];"+(b.plugins.dialogadvtab?"table"+b.plugins.dialogadvtab.allowedContent():""),requiredContent:"table",contentTransformations:[["table{width}: sizeToStyle","table[width]: sizeToAttribute"]]}));
b.addCommand("tableProperties",new CKEDITOR.dialogCommand("tableProperties",d()));
b.addCommand("tableDelete",d({exec:function(g){var e=g.elementPath().contains("table",1);
if(e){var h=e.getParent(),i=g.editable();
1!=h.getChildCount()||h.is("td","th")||h.equals(i)||(e=h);
g=g.createRange();
g.moveToPosition(e,CKEDITOR.POSITION_BEFORE_START);
e.remove();
g.select()
}}}));
b.ui.addButton&&b.ui.addButton("Table",{label:f.toolbar,command:"table",toolbar:"insert,30"});
CKEDITOR.dialog.add("table",this.path+"dialogs/table.js");
CKEDITOR.dialog.add("tableProperties",this.path+"dialogs/table.js");
b.addMenuItems&&b.addMenuItems({table:{label:f.menu,command:"tableProperties",group:"table",order:5},tabledelete:{label:f.deleteTable,command:"tableDelete",group:"table",order:1}});
b.on("doubleclick",function(c){c.data.element.is("table")&&(c.data.dialog="tableProperties")
});
b.contextMenu&&b.contextMenu.addListener(function(){return{tabledelete:CKEDITOR.TRISTATE_OFF,table:CKEDITOR.TRISTATE_OFF}
})
}}});
(function(){function n(r){function s(u){0<m.length||u.type!=CKEDITOR.NODE_ELEMENT||!a.test(u.getName())||u.getCustomData("selected_cell")||(CKEDITOR.dom.element.setMarker(t,u,"selected_cell",!0),m.push(u))
}r=r.getRanges();
for(var m=[],t={},o=0;
o<r.length;
o++){var q=r[o];
if(q.collapsed){q=q.getCommonAncestor(),(q=q.getAscendant("td",!0)||q.getAscendant("th",!0))&&m.push(q)
}else{var q=new CKEDITOR.dom.walker(q),p;
for(q.guard=s;
p=q.next();
){p.type==CKEDITOR.NODE_ELEMENT&&p.is(CKEDITOR.dtd.table)||(p=p.getAscendant("td",!0)||p.getAscendant("th",!0))&&!p.getCustomData("selected_cell")&&(CKEDITOR.dom.element.setMarker(t,p,"selected_cell",!0),m.push(p))
}}}CKEDITOR.dom.element.clearAllMarkers(t);
return m
}function d(r,s){for(var u=n(r),t=u[0],v=t.getAscendant("table"),t=t.getDocument(),q=u[0].getParent(),p=q.$.rowIndex,u=u[u.length-1],o=u.getParent().$.rowIndex+u.$.rowSpan-1,u=new CKEDITOR.dom.element(v.$.rows[o]),p=s?p:o,q=s?q:u,u=CKEDITOR.tools.buildTableMap(v),v=u[p],p=s?u[p-1]:u[p+1],u=u[0].length,t=t.createElement("tr"),o=0;
v[o]&&o<u;
o++){var m;
1<v[o].rowSpan&&p&&v[o]==p[o]?(m=v[o],m.rowSpan+=1):(m=(new CKEDITOR.dom.element(v[o])).clone(),m.removeAttribute("rowSpan"),m.appendBogus(),t.append(m),m=m.$);
o+=m.colSpan-1
}s?t.insertBefore(q):t.insertAfter(q)
}function l(t){if(t instanceof CKEDITOR.dom.selection){var u=n(t),w=u[0].getAscendant("table"),v=CKEDITOR.tools.buildTableMap(w);
t=u[0].getParent().$.rowIndex;
for(var u=u[u.length-1],x=u.getParent().$.rowIndex+u.$.rowSpan-1,u=[],s=t;
s<=x;
s++){for(var r=v[s],q=new CKEDITOR.dom.element(w.$.rows[s]),p=0;
p<r.length;
p++){var o=new CKEDITOR.dom.element(r[p]),m=o.getParent().$.rowIndex;
1==o.$.rowSpan?o.remove():(--o.$.rowSpan,m==s&&(m=v[s+1],m[p-1]?o.insertAfter(new CKEDITOR.dom.element(m[p-1])):(new CKEDITOR.dom.element(w.$.rows[s+1])).append(o,1)));
p+=o.$.colSpan-1
}u.push(q)
}v=w.$.rows;
w=new CKEDITOR.dom.element(v[x+1]||(0<t?v[t-1]:null)||w.$.parentNode);
for(s=u.length;
0<=s;
s--){l(u[s])
}return w
}t instanceof CKEDITOR.dom.element&&(w=t.getAscendant("table"),1==w.$.rows.length?w.remove():t.remove());
return null
}function k(s,t){for(var v=t?Infinity:0,u=0;
u<s.length;
u++){var w;
w=s[u];
for(var r=t,q=w.getParent().$.cells,p=0,o=0;
o<q.length;
o++){var m=q[o],p=p+(r?1:m.colSpan);
if(m==w.$){break
}}w=p-1;
if(t?w<v:w>v){v=w
}}return v
}function f(s,t){for(var m=n(s),u=m[0].getAscendant("table"),o=k(m,1),m=k(m),o=t?o:m,r=CKEDITOR.tools.buildTableMap(u),u=[],m=[],q=r.length,p=0;
p<q;
p++){u.push(r[p][o]),m.push(t?r[p][o-1]:r[p][o+1])
}for(p=0;
p<q;
p++){u[p]&&(1<u[p].colSpan&&m[p]==u[p]?(o=u[p],o.colSpan+=1):(o=(new CKEDITOR.dom.element(u[p])).clone(),o.removeAttribute("colSpan"),o.appendBogus(),o[t?"insertBefore":"insertAfter"].call(o,new CKEDITOR.dom.element(u[p])),o=o.$),p+=o.rowSpan-1)
}}function h(o,p){var m=o.getStartElement();
if(m=m.getAscendant("td",1)||m.getAscendant("th",1)){var q=m.clone();
q.appendBogus();
p?q.insertBefore(m):q.insertAfter(m)
}}function i(r){if(r instanceof CKEDITOR.dom.selection){r=n(r);
var s=r[0]&&r[0].getAscendant("table"),m;
o:{var t=0;
m=r.length-1;
for(var o={},q,p;
q=r[t++];
){CKEDITOR.dom.element.setMarker(o,q,"delete_cell",!0)
}for(t=0;
q=r[t++];
){if((p=q.getPrevious())&&!p.getCustomData("delete_cell")||(p=q.getNext())&&!p.getCustomData("delete_cell")){CKEDITOR.dom.element.clearAllMarkers(o);
m=p;
break o
}}CKEDITOR.dom.element.clearAllMarkers(o);
p=r[0].getParent();
(p=p.getPrevious())?m=p.getLast():(p=r[m].getParent(),m=(p=p.getNext())?p.getChild(0):null)
}for(p=r.length-1;
0<=p;
p--){i(r[p])
}m?b(m,!0):s&&s.remove()
}else{r instanceof CKEDITOR.dom.element&&(s=r.getParent(),1==s.getChildCount()?s.remove():r.remove())
}}function b(o,p){var m=o.getDocument(),q=CKEDITOR.document;
CKEDITOR.env.ie&&10==CKEDITOR.env.version&&(q.focus(),m.focus());
m=new CKEDITOR.dom.range(m);
m["moveToElementEdit"+(p?"End":"Start")](o)||(m.selectNodeContents(o),m.collapse(p?!1:!0));
m.select(!0)
}function g(o,p,m){o=o[p];
if("undefined"==typeof m){return o
}for(p=0;
o&&p<o.length;
p++){if(m.is&&o[p]==m.$){return p
}if(p==m){return new CKEDITOR.dom.element(o[p])
}}return m.is?-1:null
}function j(P,Q,S){var R=n(P),T;
if((Q?1!=R.length:2>R.length)||(T=P.getCommonAncestor())&&T.type==CKEDITOR.NODE_ELEMENT&&T.is("table")){return !1
}var O;
P=R[0];
T=P.getAscendant("table");
var N=CKEDITOR.tools.buildTableMap(T),M=N.length,L=N[0].length,K=P.getParent().$.rowIndex,I=g(N,K,P);
if(Q){var E;
try{var F=parseInt(P.getAttribute("rowspan"),10)||1;
O=parseInt(P.getAttribute("colspan"),10)||1;
E=N["up"==Q?K-F:"down"==Q?K+F:K]["left"==Q?I-O:"right"==Q?I+O:I]
}catch(C){return !1
}if(!E||P.$==E){return !1
}R["up"==Q||"left"==Q?"unshift":"push"](new CKEDITOR.dom.element(E))
}Q=P.getDocument();
var H=K,F=E=0,B=!S&&new CKEDITOR.dom.documentFragment(Q),t=0;
for(Q=0;
Q<R.length;
Q++){O=R[Q];
var J=O.getParent(),s=O.getFirst(),z=O.$.colSpan,o=O.$.rowSpan,J=J.$.rowIndex,G=g(N,J,O),t=t+z*o,F=Math.max(F,G-I+z);
E=Math.max(E,J-K+o);
S||(z=O,(o=z.getBogus())&&o.remove(),z.trim(),O.getChildren().count()&&(J==H||!s||s.isBlockBoundary&&s.isBlockBoundary({br:1})||(H=B.getLast(CKEDITOR.dom.walker.whitespaces(!0)),!H||H.is&&H.is("br")||B.append("br")),O.moveChildren(B)),Q?O.remove():O.setHtml(""));
H=J
}if(S){return E*F==t
}B.moveChildren(P);
P.appendBogus();
F>=L?P.removeAttribute("rowSpan"):P.$.rowSpan=E;
E>=M?P.removeAttribute("colSpan"):P.$.colSpan=F;
S=new CKEDITOR.dom.nodeList(T.$.rows);
R=S.count();
for(Q=R-1;
0<=Q;
Q--){T=S.getItem(Q),T.$.cells.length||(T.remove(),R++)
}return P
}function e(t,u){var w=n(t);
if(1<w.length){return !1
}if(u){return !0
}var w=w[0],v=w.getParent(),x=v.getAscendant("table"),s=CKEDITOR.tools.buildTableMap(x),r=v.$.rowIndex,q=g(s,r,w),p=w.$.rowSpan,o;
if(1<p){o=Math.ceil(p/2);
for(var p=Math.floor(p/2),v=r+o,x=new CKEDITOR.dom.element(x.$.rows[v]),s=g(s,v),m,v=w.clone(),r=0;
r<s.length;
r++){if(m=s[r],m.parentNode==x.$&&r>q){v.insertBefore(new CKEDITOR.dom.element(m));
break
}else{m=null
}}m||x.append(v)
}else{for(p=o=1,x=v.clone(),x.insertAfter(v),x.append(v=w.clone()),m=g(s,r),q=0;
q<m.length;
q++){m[q].rowSpan++
}}v.appendBogus();
w.$.rowSpan=o;
v.$.rowSpan=p;
1==o&&w.removeAttribute("rowSpan");
1==p&&v.removeAttribute("rowSpan");
return v
}function c(s,t){var v=n(s);
if(1<v.length){return !1
}if(t){return !0
}var v=v[0],u=v.getParent(),w=u.getAscendant("table"),w=CKEDITOR.tools.buildTableMap(w),r=g(w,u.$.rowIndex,v),q=v.$.colSpan;
if(1<q){u=Math.ceil(q/2),q=Math.floor(q/2)
}else{for(var q=u=1,p=[],o=0;
o<w.length;
o++){var m=w[o];
p.push(m[r]);
1<m[r].rowSpan&&(o+=m[r].rowSpan-1)
}for(w=0;
w<p.length;
w++){p[w].colSpan++
}}w=v.clone();
w.insertAfter(v);
w.appendBogus();
v.$.colSpan=u;
w.$.colSpan=q;
1==u&&v.removeAttribute("colSpan");
1==q&&w.removeAttribute("colSpan");
return w
}var a=/^(?:td|th)$/;
CKEDITOR.plugins.tabletools={requires:"table,dialog,contextmenu",init:function(o){function p(r){return CKEDITOR.tools.extend(r||{},{contextSensitive:1,refresh:function(t,s){this.setState(s.contains({td:1,th:1},1)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED)
}})
}function m(s,r){var t=o.addCommand(s,r);
o.addFeature(t)
}var q=o.lang.table;
m("cellProperties",new CKEDITOR.dialogCommand("cellProperties",p({allowedContent:"td th{width,height,border-color,background-color,white-space,vertical-align,text-align}[colspan,rowspan]",requiredContent:"table"})));
CKEDITOR.dialog.add("cellProperties",this.path+"dialogs/tableCell.js");
m("rowDelete",p({requiredContent:"table",exec:function(r){r=r.getSelection();
b(l(r))
}}));
m("rowInsertBefore",p({requiredContent:"table",exec:function(r){r=r.getSelection();
d(r,!0)
}}));
m("rowInsertAfter",p({requiredContent:"table",exec:function(r){r=r.getSelection();
d(r)
}}));
m("columnDelete",p({requiredContent:"table",exec:function(C){C=C.getSelection();
C=n(C);
var B=C[0],A=C[C.length-1];
C=B.getAscendant("table");
for(var z=CKEDITOR.tools.buildTableMap(C),y,x,v=[],s=0,t=z.length;
s<t;
s++){for(var w=0,D=z[s].length;
w<D;
w++){z[s][w]==B.$&&(y=w),z[s][w]==A.$&&(x=w)
}}for(s=y;
s<=x;
s++){for(w=0;
w<z.length;
w++){A=z[w],B=new CKEDITOR.dom.element(C.$.rows[w]),A=new CKEDITOR.dom.element(A[s]),A.$&&(1==A.$.colSpan?A.remove():--A.$.colSpan,w+=A.$.rowSpan-1,B.$.cells.length||v.push(B))
}}x=C.$.rows[0]&&C.$.rows[0].cells;
y=new CKEDITOR.dom.element(x[y]||(y?x[y-1]:C.$.parentNode));
v.length==t&&C.remove();
y&&b(y,!0)
}}));
m("columnInsertBefore",p({requiredContent:"table",exec:function(r){r=r.getSelection();
f(r,!0)
}}));
m("columnInsertAfter",p({requiredContent:"table",exec:function(r){r=r.getSelection();
f(r)
}}));
m("cellDelete",p({requiredContent:"table",exec:function(r){r=r.getSelection();
i(r)
}}));
m("cellMerge",p({allowedContent:"td[colspan,rowspan]",requiredContent:"td[colspan,rowspan]",exec:function(r){b(j(r.getSelection()),!0)
}}));
m("cellMergeRight",p({allowedContent:"td[colspan]",requiredContent:"td[colspan]",exec:function(r){b(j(r.getSelection(),"right"),!0)
}}));
m("cellMergeDown",p({allowedContent:"td[rowspan]",requiredContent:"td[rowspan]",exec:function(r){b(j(r.getSelection(),"down"),!0)
}}));
m("cellVerticalSplit",p({allowedContent:"td[rowspan]",requiredContent:"td[rowspan]",exec:function(r){b(c(r.getSelection()))
}}));
m("cellHorizontalSplit",p({allowedContent:"td[colspan]",requiredContent:"td[colspan]",exec:function(r){b(e(r.getSelection()))
}}));
m("cellInsertBefore",p({requiredContent:"table",exec:function(r){r=r.getSelection();
h(r,!0)
}}));
m("cellInsertAfter",p({requiredContent:"table",exec:function(r){r=r.getSelection();
h(r)
}}));
o.addMenuItems&&o.addMenuItems({tablecell:{label:q.cell.menu,group:"tablecell",order:1,getItems:function(){var s=o.getSelection(),r=n(s);
return{tablecell_insertBefore:CKEDITOR.TRISTATE_OFF,tablecell_insertAfter:CKEDITOR.TRISTATE_OFF,tablecell_delete:CKEDITOR.TRISTATE_OFF,tablecell_merge:j(s,null,!0)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,tablecell_merge_right:j(s,"right",!0)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,tablecell_merge_down:j(s,"down",!0)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,tablecell_split_vertical:c(s,!0)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,tablecell_split_horizontal:e(s,!0)?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED,tablecell_properties:0<r.length?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED}
}},tablecell_insertBefore:{label:q.cell.insertBefore,group:"tablecell",command:"cellInsertBefore",order:5},tablecell_insertAfter:{label:q.cell.insertAfter,group:"tablecell",command:"cellInsertAfter",order:10},tablecell_delete:{label:q.cell.deleteCell,group:"tablecell",command:"cellDelete",order:15},tablecell_merge:{label:q.cell.merge,group:"tablecell",command:"cellMerge",order:16},tablecell_merge_right:{label:q.cell.mergeRight,group:"tablecell",command:"cellMergeRight",order:17},tablecell_merge_down:{label:q.cell.mergeDown,group:"tablecell",command:"cellMergeDown",order:18},tablecell_split_horizontal:{label:q.cell.splitHorizontal,group:"tablecell",command:"cellHorizontalSplit",order:19},tablecell_split_vertical:{label:q.cell.splitVertical,group:"tablecell",command:"cellVerticalSplit",order:20},tablecell_properties:{label:q.cell.title,group:"tablecellproperties",command:"cellProperties",order:21},tablerow:{label:q.row.menu,group:"tablerow",order:1,getItems:function(){return{tablerow_insertBefore:CKEDITOR.TRISTATE_OFF,tablerow_insertAfter:CKEDITOR.TRISTATE_OFF,tablerow_delete:CKEDITOR.TRISTATE_OFF}
}},tablerow_insertBefore:{label:q.row.insertBefore,group:"tablerow",command:"rowInsertBefore",order:5},tablerow_insertAfter:{label:q.row.insertAfter,group:"tablerow",command:"rowInsertAfter",order:10},tablerow_delete:{label:q.row.deleteRow,group:"tablerow",command:"rowDelete",order:15},tablecolumn:{label:q.column.menu,group:"tablecolumn",order:1,getItems:function(){return{tablecolumn_insertBefore:CKEDITOR.TRISTATE_OFF,tablecolumn_insertAfter:CKEDITOR.TRISTATE_OFF,tablecolumn_delete:CKEDITOR.TRISTATE_OFF}
}},tablecolumn_insertBefore:{label:q.column.insertBefore,group:"tablecolumn",command:"columnInsertBefore",order:5},tablecolumn_insertAfter:{label:q.column.insertAfter,group:"tablecolumn",command:"columnInsertAfter",order:10},tablecolumn_delete:{label:q.column.deleteColumn,group:"tablecolumn",command:"columnDelete",order:15}});
o.contextMenu&&o.contextMenu.addListener(function(s,r,t){return(s=t.contains({td:1,th:1},1))&&!s.isReadOnly()?{tablecell:CKEDITOR.TRISTATE_OFF,tablerow:CKEDITOR.TRISTATE_OFF,tablecolumn:CKEDITOR.TRISTATE_OFF}:null
})
},getSelectedCells:n};
CKEDITOR.plugins.add("tabletools",CKEDITOR.plugins.tabletools)
})();
CKEDITOR.tools.buildTableMap=function(j){j=j.$.rows;
for(var b=-1,i=[],h=0;
h<j.length;
h++){b++;
!i[b]&&(i[b]=[]);
for(var c=-1,e=0;
e<j[h].cells.length;
e++){var f=j[h].cells[e];
for(c++;
i[b][c];
){c++
}for(var a=isNaN(f.colSpan)?1:f.colSpan,f=isNaN(f.rowSpan)?1:f.rowSpan,d=0;
d<f;
d++){i[b+d]||(i[b+d]=[]);
for(var g=0;
g<a;
g++){i[b+d][c+g]=j[h].cells[e]
}}c+=a-1
}}return i
};
(function(){var d=[CKEDITOR.CTRL+90,CKEDITOR.CTRL+89,CKEDITOR.CTRL+CKEDITOR.SHIFT+90],l={8:1,46:1};
CKEDITOR.plugins.add("undo",{init:function(k){function e(f){o.enabled&&!1!==f.data.command.canUndo&&o.save()
}function p(){o.enabled=k.readOnly?!1:"wysiwyg"==k.mode;
o.onChange()
}var o=k.undoManager=new j(k),g=o.editingHandler=new b(o),n=k.addCommand("undo",{exec:function(){o.undo()&&(k.selectionChange(),this.fire("afterUndo"))
},startDisabled:!0,canUndo:!1}),m=k.addCommand("redo",{exec:function(){o.redo()&&(k.selectionChange(),this.fire("afterRedo"))
},startDisabled:!0,canUndo:!1});
k.setKeystroke([[d[0],"undo"],[d[1],"redo"],[d[2],"redo"]]);
o.onChange=function(){n.setState(o.undoable()?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED);
m.setState(o.redoable()?CKEDITOR.TRISTATE_OFF:CKEDITOR.TRISTATE_DISABLED)
};
k.on("beforeCommandExec",e);
k.on("afterCommandExec",e);
k.on("saveSnapshot",function(f){o.save(f.data&&f.data.contentOnly)
});
k.on("contentDom",g.attachListeners,g);
k.on("instanceReady",function(){k.fire("saveSnapshot")
});
k.on("beforeModeUnload",function(){"wysiwyg"==k.mode&&o.save(!0)
});
k.on("mode",p);
k.on("readOnly",p);
k.ui.addButton&&(k.ui.addButton("Undo",{label:k.lang.undo.undo,command:"undo",toolbar:"undo,10"}),k.ui.addButton("Redo",{label:k.lang.undo.redo,command:"redo",toolbar:"undo,20"}));
k.resetUndo=function(){o.reset();
k.fire("saveSnapshot")
};
k.on("updateSnapshot",function(){o.currentImage&&o.update()
});
k.on("lockSnapshot",function(f){f=f.data;
o.lock(f&&f.dontUpdate,f&&f.forceUpdate)
});
k.on("unlockSnapshot",o.unlock,o)
}});
CKEDITOR.plugins.undo={};
var j=CKEDITOR.plugins.undo.UndoManager=function(e){this.strokesRecorded=[0,0];
this.locked=null;
this.previousKeyGroup=-1;
this.limit=e.config.undoStackSize||20;
this.strokesLimit=25;
this.editor=e;
this.reset()
};
j.prototype={type:function(f,e){var h=j.getKeyGroup(f),g=this.strokesRecorded[h]+1;
e=e||g>=this.strokesLimit;
this.typing||(this.hasUndo=this.typing=!0,this.hasRedo=!1,this.onChange());
e?(g=0,this.editor.fire("saveSnapshot")):this.editor.fire("change");
this.strokesRecorded[h]=g;
this.previousKeyGroup=h
},keyGroupChanged:function(e){return j.getKeyGroup(e)!=this.previousKeyGroup
},reset:function(){this.snapshots=[];
this.index=-1;
this.currentImage=null;
this.hasRedo=this.hasUndo=!1;
this.locked=null;
this.resetType()
},resetType:function(){this.strokesRecorded=[0,0];
this.typing=!1;
this.previousKeyGroup=-1
},refreshState:function(){this.hasUndo=!!this.getNextImage(!0);
this.hasRedo=!!this.getNextImage(!1);
this.resetType();
this.onChange()
},save:function(g,f,m){var k=this.editor;
if(this.locked||"ready"!=k.status||"wysiwyg"!=k.mode){return !1
}var h=k.editable();
if(!h||"ready"!=h.status){return !1
}h=this.snapshots;
f||(f=new i(k));
if(!1===f.contents){return !1
}if(this.currentImage){if(f.equalsContent(this.currentImage)){if(g||f.equalsSelection(this.currentImage)){return !1
}}else{!1!==m&&k.fire("change")
}}h.splice(this.index+1,h.length-this.index-1);
h.length==this.limit&&h.shift();
this.index=h.push(f)-1;
this.currentImage=f;
!1!==m&&this.refreshState();
return !0
},restoreImage:function(f){var e=this.editor,g;
f.bookmarks&&(e.focus(),g=e.getSelection());
this.locked={level:999};
this.editor.loadSnapshot(f.contents);
f.bookmarks?g.selectBookmarks(f.bookmarks):CKEDITOR.env.ie&&(g=this.editor.document.getBody().$.createTextRange(),g.collapse(!0),g.select());
this.locked=null;
this.index=f.index;
this.currentImage=this.snapshots[this.index];
this.update();
this.refreshState();
e.fire("change")
},getNextImage:function(f){var e=this.snapshots,h=this.currentImage,g;
if(h){if(f){for(g=this.index-1;
0<=g;
g--){if(f=e[g],!h.equalsContent(f)){return f.index=g,f
}}}else{for(g=this.index+1;
g<e.length;
g++){if(f=e[g],!h.equalsContent(f)){return f.index=g,f
}}}}return null
},redoable:function(){return this.enabled&&this.hasRedo
},undoable:function(){return this.enabled&&this.hasUndo
},undo:function(){if(this.undoable()){this.save(!0);
var e=this.getNextImage(!0);
if(e){return this.restoreImage(e),!0
}}return !1
},redo:function(){if(this.redoable()&&(this.save(!0),this.redoable())){var e=this.getNextImage(!1);
if(e){return this.restoreImage(e),!0
}}return !1
},update:function(f){if(!this.locked){f||(f=new i(this.editor));
for(var e=this.index,g=this.snapshots;
0<e&&this.currentImage.equalsContent(g[e-1]);
){--e
}g.splice(e,this.index-e+1,f);
this.index=e;
this.currentImage=f
}},updateSelection:function(f){if(!this.snapshots.length){return !1
}var e=this.snapshots,g=e[e.length-1];
return g.equalsContent(f)&&!g.equalsSelection(f)?(this.currentImage=e[e.length-1]=f,!0):!1
},lock:function(f,e){if(this.locked){this.locked.level++
}else{if(f){this.locked={level:1}
}else{var h=null;
if(e){h=!0
}else{var g=new i(this.editor,!0);
this.currentImage&&this.currentImage.equalsContent(g)&&(h=g)
}this.locked={update:h,level:1}
}}},unlock:function(){if(this.locked&&!--this.locked.level){var f=this.locked.update;
this.locked=null;
if(!0===f){this.update()
}else{if(f){var e=new i(this.editor,!0);
f.equalsContent(e)||this.update()
}}}}};
j.navigationKeyCodes={37:1,38:1,39:1,40:1,36:1,35:1,33:1,34:1};
j.keyGroups={PRINTABLE:0,FUNCTIONAL:1};
j.isNavigationKey=function(e){return !!j.navigationKeyCodes[e]
};
j.getKeyGroup=function(f){var e=j.keyGroups;
return l[f]?e.FUNCTIONAL:e.PRINTABLE
};
j.getOppositeKeyGroup=function(f){var e=j.keyGroups;
return f==e.FUNCTIONAL?e.PRINTABLE:e.FUNCTIONAL
};
j.ieFunctionalKeysBug=function(e){return CKEDITOR.env.ie&&j.getKeyGroup(e)==j.keyGroups.FUNCTIONAL
};
var i=CKEDITOR.plugins.undo.Image=function(f,e){this.editor=f;
f.fire("beforeUndoImage");
var g=f.getSnapshot();
CKEDITOR.env.ie&&g&&(g=g.replace(/\s+data-cke-expando=".*?"/g,""));
this.contents=g;
e||(this.bookmarks=(g=g&&f.getSelection())&&g.createBookmarks2(!0));
f.fire("afterUndoImage")
},c=/\b(?:href|src|name)="[^"]*?"/gi;
i.prototype={equalsContent:function(f){var e=this.contents;
f=f.contents;
CKEDITOR.env.ie&&(CKEDITOR.env.ie7Compat||CKEDITOR.env.quirks)&&(e=e.replace(c,""),f=f.replace(c,""));
return e!=f?!1:!0
},equalsSelection:function(g){var f=this.bookmarks;
g=g.bookmarks;
if(f||g){if(!f||!g||f.length!=g.length){return !1
}for(var m=0;
m<f.length;
m++){var k=f[m],h=g[m];
if(k.startOffset!=h.startOffset||k.endOffset!=h.endOffset||!CKEDITOR.tools.arrayCompare(k.start,h.start)||!CKEDITOR.tools.arrayCompare(k.end,h.end)){return !1
}}}return !0
}};
var b=CKEDITOR.plugins.undo.NativeEditingHandler=function(e){this.undoManager=e;
this.ignoreInputEvent=!1;
this.keyEventsStack=new a;
this.lastKeydownImage=null
};
b.prototype={onKeydown:function(f){var e=f.data.getKey();
if(229!==e){if(-1<CKEDITOR.tools.indexOf(d,f.data.getKeystroke())){f.data.preventDefault()
}else{if(this.keyEventsStack.cleanUp(f),f=this.undoManager,this.keyEventsStack.getLast(e)||this.keyEventsStack.push(e),this.lastKeydownImage=new i(f.editor),j.isNavigationKey(e)||this.undoManager.keyGroupChanged(e)){if(f.strokesRecorded[0]||f.strokesRecorded[1]){f.save(!1,this.lastKeydownImage,!1),f.resetType()
}}}}},onInput:function(){if(this.ignoreInputEvent){this.ignoreInputEvent=!1
}else{var e=this.keyEventsStack.getLast();
e||(e=this.keyEventsStack.push(0));
this.keyEventsStack.increment(e.keyCode);
this.keyEventsStack.getTotalInputs()>=this.undoManager.strokesLimit&&(this.undoManager.type(e.keyCode,!0),this.keyEventsStack.resetInputs())
}},onKeyup:function(f){var e=this.undoManager;
f=f.data.getKey();
var g=this.keyEventsStack.getTotalInputs();
this.keyEventsStack.remove(f);
if(!(j.ieFunctionalKeysBug(f)&&this.lastKeydownImage&&this.lastKeydownImage.equalsContent(new i(e.editor,!0)))){if(0<g){e.type(f)
}else{if(j.isNavigationKey(f)){this.onNavigationKey(!0)
}}}},onNavigationKey:function(f){var e=this.undoManager;
!f&&e.save(!0,null,!1)||e.updateSelection(new i(e.editor));
e.resetType()
},ignoreInputEventListener:function(){this.ignoreInputEvent=!0
},attachListeners:function(){var f=this.undoManager.editor,e=f.editable(),g=this;
e.attachListener(e,"keydown",function(h){g.onKeydown(h);
if(j.ieFunctionalKeysBug(h.data.getKey())){g.onInput()
}},null,null,999);
e.attachListener(e,CKEDITOR.env.ie?"keypress":"input",g.onInput,g,null,999);
e.attachListener(e,"keyup",g.onKeyup,g,null,999);
e.attachListener(e,"paste",g.ignoreInputEventListener,g,null,999);
e.attachListener(e,"drop",g.ignoreInputEventListener,g,null,999);
e.attachListener(e.isInline()?e:f.document.getDocumentElement(),"click",function(){g.onNavigationKey()
},null,null,999);
e.attachListener(this.undoManager.editor,"blur",function(){g.keyEventsStack.remove(9)
},null,null,999)
}};
var a=CKEDITOR.plugins.undo.KeyEventsStack=function(){this.stack=[]
};
a.prototype={push:function(e){e=this.stack.push({keyCode:e,inputs:0});
return this.stack[e-1]
},getLastIndex:function(f){if("number"!=typeof f){return this.stack.length-1
}for(var e=this.stack.length;
e--;
){if(this.stack[e].keyCode==f){return e
}}return -1
},getLast:function(e){e=this.getLastIndex(e);
return -1!=e?this.stack[e]:null
},increment:function(e){this.getLast(e).inputs++
},remove:function(e){e=this.getLastIndex(e);
-1!=e&&this.stack.splice(e,1)
},resetInputs:function(e){if("number"==typeof e){this.getLast(e).inputs=0
}else{for(e=this.stack.length;
e--;
){this.stack[e].inputs=0
}}},getTotalInputs:function(){for(var f=this.stack.length,e=0;
f--;
){e+=this.stack[f].inputs
}return e
},cleanUp:function(e){e=e.data.$;
e.ctrlKey||e.metaKey||this.remove(17);
e.shiftKey||this.remove(16);
e.altKey||this.remove(18)
}}
})();
CKEDITOR.plugins.add("wsc",{requires:"dialog",parseApi:function(b){b.config.wsc_onFinish="function"===typeof b.config.wsc_onFinish?b.config.wsc_onFinish:function(){};
b.config.wsc_onClose="function"===typeof b.config.wsc_onClose?b.config.wsc_onClose:function(){}
},parseConfig:function(b){b.config.wsc_customerId=b.config.wsc_customerId||CKEDITOR.config.wsc_customerId||"1:ua3xw1-2XyGJ3-GWruD3-6OFNT1-oXcuB1-nR6Bp4-hgQHc-EcYng3-sdRXG3-NOfFk";
b.config.wsc_customDictionaryIds=b.config.wsc_customDictionaryIds||CKEDITOR.config.wsc_customDictionaryIds||"";
b.config.wsc_userDictionaryName=b.config.wsc_userDictionaryName||CKEDITOR.config.wsc_userDictionaryName||"";
b.config.wsc_customLoaderScript=b.config.wsc_customLoaderScript||CKEDITOR.config.wsc_customLoaderScript;
CKEDITOR.config.wsc_cmd=b.config.wsc_cmd||CKEDITOR.config.wsc_cmd||"spell";
CKEDITOR.config.wsc_version="v4.3.0-master-d769233";
CKEDITOR.config.wsc_removeGlobalVariable=!0
},init:function(d){var c=CKEDITOR.env;
this.parseConfig(d);
this.parseApi(d);
d.addCommand("checkspell",new CKEDITOR.dialogCommand("checkspell")).modes={wysiwyg:!CKEDITOR.env.opera&&!CKEDITOR.env.air&&document.domain==window.location.hostname&&!(c.ie&&(8>c.version||c.quirks))};
"undefined"==typeof d.plugins.scayt&&d.ui.addButton&&d.ui.addButton("SpellChecker",{label:d.lang.wsc.toolbar,click:function(f){var e=f.elementMode==CKEDITOR.ELEMENT_MODE_INLINE?f.container.getText():f.document.getBody().getText();
(e=e.replace(/\s/g,""))?f.execCommand("checkspell"):alert("Nothing to check!")
},toolbar:"spellchecker,10"});
CKEDITOR.dialog.add("checkspell",this.path+(CKEDITOR.env.ie&&7>=CKEDITOR.env.version?"dialogs/wsc_ie.js":window.postMessage?"dialogs/wsc.js":"dialogs/wsc_ie.js"))
}});
CKEDITOR.config.plugins="dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,undo,wsc";
CKEDITOR.config.skin="moono";
(function(){var a=function(c,d){var e=CKEDITOR.getUrl("plugins/"+d);
c=c.split(",");
for(var b=0;
b<c.length;
b++){CKEDITOR.skin.icons[c[b]]={path:e,offset:-c[++b],bgsize:c[++b]}
}};
if(CKEDITOR.env.hidpi){a("about,0,,bold,24,,italic,48,,strike,72,,subscript,96,,superscript,120,,underline,144,,bidiltr,168,,bidirtl,192,,blockquote,216,,copy-rtl,240,,copy,264,,cut-rtl,288,,cut,312,,paste-rtl,336,,paste,360,,bgcolor,384,,textcolor,408,,templates-rtl,432,,templates,456,,creatediv,480,,find-rtl,504,,find,528,,replace,552,,flash,576,,button,600,,checkbox,624,,form,648,,hiddenfield,672,,imagebutton,696,,radio,720,,select-rtl,744,,select,768,,textarea-rtl,792,,textarea,816,,textfield-rtl,840,,textfield,864,,horizontalrule,888,,iframe,912,,image,936,,indent-rtl,960,,indent,984,,outdent-rtl,1008,,outdent,1032,,smiley,1056,,justifyblock,1080,,justifycenter,1104,,justifyleft,1128,,justifyright,1152,,language,1176,,anchor-rtl,1200,,anchor,1224,,link,1248,,unlink,1272,,bulletedlist-rtl,1296,,bulletedlist,1320,,numberedlist-rtl,1344,,numberedlist,1368,,maximize,1392,,newpage-rtl,1416,,newpage,1440,,pagebreak-rtl,1464,,pagebreak,1488,,pastetext-rtl,1512,,pastetext,1536,,pastefromword-rtl,1560,,pastefromword,1584,,preview-rtl,1608,,preview,1632,,print,1656,,removeformat,1680,,save,1704,,selectall,1728,,showblocks-rtl,1752,,showblocks,1776,,source-rtl,1800,,source,1824,,specialchar,1848,,scayt,1872,,table,1896,,redo-rtl,1920,,redo,1944,,undo-rtl,1968,,undo,1992,,spellchecker,2016,","icons_hidpi.png")
}else{a("about,0,auto,bold,24,auto,italic,48,auto,strike,72,auto,subscript,96,auto,superscript,120,auto,underline,144,auto,bidiltr,168,auto,bidirtl,192,auto,blockquote,216,auto,copy-rtl,240,auto,copy,264,auto,cut-rtl,288,auto,cut,312,auto,paste-rtl,336,auto,paste,360,auto,bgcolor,384,auto,textcolor,408,auto,templates-rtl,432,auto,templates,456,auto,creatediv,480,auto,find-rtl,504,auto,find,528,auto,replace,552,auto,flash,576,auto,button,600,auto,checkbox,624,auto,form,648,auto,hiddenfield,672,auto,imagebutton,696,auto,radio,720,auto,select-rtl,744,auto,select,768,auto,textarea-rtl,792,auto,textarea,816,auto,textfield-rtl,840,auto,textfield,864,auto,horizontalrule,888,auto,iframe,912,auto,image,936,auto,indent-rtl,960,auto,indent,984,auto,outdent-rtl,1008,auto,outdent,1032,auto,smiley,1056,auto,justifyblock,1080,auto,justifycenter,1104,auto,justifyleft,1128,auto,justifyright,1152,auto,language,1176,auto,anchor-rtl,1200,auto,anchor,1224,auto,link,1248,auto,unlink,1272,auto,bulletedlist-rtl,1296,auto,bulletedlist,1320,auto,numberedlist-rtl,1344,auto,numberedlist,1368,auto,maximize,1392,auto,newpage-rtl,1416,auto,newpage,1440,auto,pagebreak-rtl,1464,auto,pagebreak,1488,auto,pastetext-rtl,1512,auto,pastetext,1536,auto,pastefromword-rtl,1560,auto,pastefromword,1584,auto,preview-rtl,1608,auto,preview,1632,auto,print,1656,auto,removeformat,1680,auto,save,1704,auto,selectall,1728,auto,showblocks-rtl,1752,auto,showblocks,1776,auto,source-rtl,1800,auto,source,1824,auto,specialchar,1848,auto,scayt,1872,auto,table,1896,auto,redo-rtl,1920,auto,redo,1944,auto,undo-rtl,1968,auto,undo,1992,auto,spellchecker,2016,auto","icons.png")
}})();
CKEDITOR.lang.languages={af:1,sq:1,ar:1,eu:1,bn:1,bs:1,bg:1,ca:1,"zh-cn":1,zh:1,hr:1,cs:1,da:1,nl:1,en:1,"en-au":1,"en-ca":1,"en-gb":1,eo:1,et:1,fo:1,fi:1,fr:1,"fr-ca":1,gl:1,ka:1,de:1,el:1,gu:1,he:1,hi:1,hu:1,is:1,id:1,it:1,ja:1,km:1,ko:1,ku:1,lv:1,lt:1,mk:1,ms:1,mn:1,no:1,nb:1,fa:1,pl:1,"pt-br":1,pt:1,ro:1,ru:1,sr:1,"sr-latn":1,si:1,sk:1,sl:1,es:1,sv:1,tt:1,th:1,tr:1,ug:1,uk:1,vi:1,cy:1}
}());
(function(){a("codesnippet","CodeSnippet");
a("blockquote","Blockquote");
a("oembed","oembed");
a("autolink","autolink");
a("table","Table");
a("tableresize","tableresize");
function a(c,b){if(!CKEDITOR.config.extraPlugins||CKEDITOR.config.extraPlugins.length==0){CKEDITOR.config.extraPlugins=c
}else{if(CKEDITOR.config.extraPlugins.indexOf(c)===-1){CKEDITOR.config.extraPlugins=CKEDITOR.config.extraPlugins.concat(","+c)
}}$(function(){SCF.fieldManagers.ckeditor.prototype.config.toolbar[0].items.push(b)
})
}})();