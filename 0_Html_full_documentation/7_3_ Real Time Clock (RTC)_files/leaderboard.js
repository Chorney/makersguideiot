(function(b){var a=b.View.extend({viewName:"DataTableView",setSortIcon:function(){},initializePagination:function(){},refreshView:function(l,m,k){this.render();
var g=this;
var d=0;
var n=$(this.el).find("table").DataTable({dom:"rAtiJ",ordering:false,serverSide:true,deferLoading:l,displayStart:m,ajax:function(o,p,i){if(d===1){k(g,o.start)
}d++
},language:{search:"_INPUT_",searchPlaceholder:CQ.I18n.get("Filter")}});
var h=new $.fn.dataTable.ColVis(n,{fnStateChange:function(){var i=$.Event("dataTablesColumnUpdate");
$(this.dom.wrapper).trigger(i);
g.columnVisibility=[];
n.columns().eq(0).each(function(o){var p=n.column(o);
g.columnVisibility.push(p.visible())
})
}});
this.initializeSort();
var e=0;
var f=0;
_.each($(this.el).find(".dataTable thead th"),function(o){var i=true;
if(!_.isUndefined(this.columnVisibility)){i=this.columnVisibility[e]
}if($(o).hasClass("dt-hide")||!i){$($(this.el).find("table")[0]).dataTable().fnSetColumnVis(e,false,false);
if(f===e){f++
}}e++
},this);
$.fn.dataTable.ColVis.fnRebuild($($(this.el).find("table")[0]).dataTable());
n.order([[f,"asc"]]).draw(false);
$(this.el).find(".dataTables_wrapper").prepend(h.button());
var j=$("<div class='dataTables_toolbar'></div>");
$(j).append($(this.el).find(".ColVis"));
$(j).append($(this.el).find(".dataTables_filter"));
$(this.el).find(".dataTables_wrapper").prepend(j);
var c=n.settings()[0];
$(c.nTable).trigger("init.dt",[c])
}});
b.DataTableView=a
})(SCF);
(function(b){b.LOG_LEVEL=1;
var c=b.Model.extend({reportGenerated:false,fetchNewReport:function(d){this.paginationUrl=d;
this.reportGenerated=false;
this.clear();
this.firstPage()
},changePage:function(e){this.currentOffset=e;
this.url=this.paginationUrl.replace("${startIndex}",e);
var d=this;
this.fetch({success:function(g,f){if(f.items!==undefined&&f.items.length>0){d.set("model",f)
}}})
},firstPage:function(){var d=0;
this.changePage(d)
},nextPage:function(){var d=this.currentOffset+=(this.pageSize);
this.changePage(d)
},previousPage:function(){var d=this.currentOffset-=(this.pageSize);
this.changePage(d)
},lastPage:function(){var d=this.lastPageOffset;
this.changePage(d)
}});
b.ReportPaginatedTableModel=c;
var a=b.View.extend({init:function(){b.Util.listenTo("reportValidatePaginationButtons",$.proxy(this.validate,this))
},firstPage:function(){this.model.firstPage()
},nextPage:function(){this.model.nextPage()
},previousPage:function(){this.model.previousPage()
},lastPage:function(){this.model.lastPage()
},validate:function(e){if(e.model===this.model){var f=false;
var d=false;
if(this.model.currentOffset===0){f=true
}if(this.model.currentOffset===this.model.lastPageOffset){d=true
}this.$el.find(".scf-pagination-first-btn").prop("disabled",f);
this.$el.find(".scf-pagination-previous-btn").prop("disabled",f);
this.$el.find(".scf-pagination-next-btn").prop("disabled",d);
this.$el.find(".scf-pagination-last-btn").prop("disabled",d)
}}});
b.ReportPaginationControlView=a
})(SCF);
(function(a){a.LOG_LEVEL=1;
var b=a.DataTableView.extend({top:-1,viewName:"BootstrapReportPaginatedTableView",init:function(){this.listenTo(this.model,"sync",this.onSync);
a.Util.listenTo("reportRefreshTable",$.proxy(this.refreshTable,this))
},refreshTable:function(){var c=this.collection.size();
if(c>0){this.refreshView(c,this.model.currentOffset,this.pagingCallback);
this.model.currentOffset=this.model.get("pageInfo").currentIndex;
this.model.pageSize=this.model.get("pageInfo").pageSize;
this.model.lastPageOffset=this.model.pageSize*(this.model.get("pageInfo").totalPages-1);
this.model.reportGenerated=true;
this.renderDueStatusLabels();
this.validatePaginationButtons()
}else{this.render()
}},pagingCallback:function(c){c.model.firstPage()
},renderDueStatusLabels:function(){},validatePaginationButtons:function(){a.Util.announce("reportValidatePaginationButtons",{model:this.model,currentOffset:this.model.currentOffset,lastPageOffset:this.model.lastPageOffset})
},initializeSort:function(){}});
a.BootstrapReportPaginatedTableView=b;
a.registerComponent("social/reporting/components/hbs/reporting/datatable/bootstrap/pagination",a.Model,a.ReportPaginationControlView)
})(SCF);
(function(a,b){b(a).ready(function(){var d=b(".scf-js-leaderboarditem-progression");
var e=d.data("thresholds");
if(e){var c=e[e.length-1];
b(".scf-js-progress").attr("aria-valuemax",c)
}})
})(document,$);
(function(b){var a=b.Model.extend({modelName:"LeaderboardModel",loadDataAsync:function(){this.url=this.get("pageInfo").urlpattern.replace(".html",".json").replace(".10","."+this.get("displayLimit")).replace("${startIndex}","0");
var d;
var h=CQ.shared.HTTP.getPath();
var e=CQ.shared.HTTP.getExtension(window.location.pathname);
var g=h.split(e);
if(e&&e!==undefined&&g&&g!==undefined){if(g.length>1){d=g[1]
}else{d=g[0]
}}else{d=h
}this.url=this.url+d;
var f=this;
this.fetch({success:function(j,i){if(!_.isUndefined(i.items)&&i.items.length>0){f.set("model",i)
}}})
}});
var c=b.BootstrapReportPaginatedTableView.extend({viewName:"LeaderboardView",className:"scf-leaderboard",init:function(){this.collection=this.model.get("items");
b.BootstrapReportPaginatedTableView.prototype.init.apply(this);
this.listenTo(this.model,"change:pageInfo",this.onPageInfoChange);
this.model.loadDataAsync()
},paginate:function(){var g=b.config.urlRoot+this.model.get("id")+b.constants.SOCIAL_SELECTOR+".";
var e=arguments[1];
var f=arguments[2];
var h=(arguments.length<=3)?null:arguments[3];
var d=null;
if(arguments.length<=3){d=g+e+"."+f+b.constants.JSON_EXT
}else{d=g+"index."+e+"."+f+"."+(h!==undefined?h+".":"")+b.constants.JSON_EXT
}this.model.url=d;
this.model.reload()
},navigate:function(h){var d=this.model.get("pageInfo");
var i=$(h.currentTarget).data("pageSuffix");
var g=i.toString();
var f=g.split(".");
if(d.selectedIndex!==null){this.paginate(d.basePageURL,f[0],f[1],d.selectedIndex)
}else{this.paginate(d.basePageURL,f[0],f[1])
}}});
b.LeaderboardModel=a;
b.LeaderboardView=c;
b.registerComponent("social/gamification/components/hbs/leaderboard",b.LeaderboardModel,b.LeaderboardView)
})(SCF);