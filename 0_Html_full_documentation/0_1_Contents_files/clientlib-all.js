/*! jQuery v2.2.2 | (c) jQuery Foundation | jquery.org/license */
;
!function(d,c){"object"==typeof module&&"object"==typeof module.exports?module.exports=d.document?c(d,!0):function(b){if(!b.document){throw new Error("jQuery requires a window with a document")
}return c(b)
}:c(d)
}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="2.2.2",n=function(a,b){return new n.fn.init(a,b)
},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()
};
n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)
},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)
},pushStack:function(a){var b=n.merge(this.constructor(),a);
return b.prevObject=this,b.context=this.context,b
},each:function(a){return n.each(this,a)
},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)
}))
},slice:function(){return this.pushStack(e.apply(this,arguments))
},first:function(){return this.eq(0)
},last:function(){return this.eq(-1)
},eq:function(a){var b=this.length,c=+a+(0>a?b:0);
return this.pushStack(c>=0&&b>c?[this[c]]:[])
},end:function(){return this.prevObject||this.constructor()
},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;
for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);
i>h;
h++){if(null!=(a=arguments[h])){for(b in a){c=g[b],d=a[b],g!==d&&(j&&d&&(n.isPlainObject(d)||(e=n.isArray(d)))?(e?(e=!1,f=c&&n.isArray(c)?c:[]):f=c&&n.isPlainObject(c)?c:{},g[b]=n.extend(j,f,d)):void 0!==d&&(g[b]=d))
}}}return g
},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)
},noop:function(){},isFunction:function(a){return"function"===n.type(a)
},isArray:Array.isArray,isWindow:function(a){return null!=a&&a===a.window
},isNumeric:function(a){var b=a&&a.toString();
return !n.isArray(a)&&b-parseFloat(b)+1>=0
},isPlainObject:function(a){var b;
if("object"!==n.type(a)||a.nodeType||n.isWindow(a)){return !1
}if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype||{},"isPrototypeOf")){return !1
}for(b in a){}return void 0===b||k.call(a,b)
},isEmptyObject:function(a){var b;
for(b in a){return !1
}return !0
},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a
},globalEval:function(a){var b,c=eval;
a=n.trim(a),a&&(1===a.indexOf("use strict")?(b=d.createElement("script"),b.text=a,d.head.appendChild(b).parentNode.removeChild(b)):c(a))
},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)
},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()
},each:function(a,b){var c,d=0;
if(s(a)){for(c=a.length;
c>d;
d++){if(b.call(a[d],d,a[d])===!1){break
}}}else{for(d in a){if(b.call(a[d],d,a[d])===!1){break
}}}return a
},trim:function(a){return null==a?"":(a+"").replace(o,"")
},makeArray:function(a,b){var c=b||[];
return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c
},inArray:function(a,b,c){return null==b?-1:h.call(b,a,c)
},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;
c>d;
d++){a[e++]=b[d]
}return a.length=e,a
},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;
g>f;
f++){d=!b(a[f],f),d!==h&&e.push(a[f])
}return e
},map:function(a,b,c){var d,e,g=0,h=[];
if(s(a)){for(d=a.length;
d>g;
g++){e=b(a[g],g,c),null!=e&&h.push(e)
}}else{for(g in a){e=b(a[g],g,c),null!=e&&h.push(e)
}}return f.apply([],h)
},guid:1,proxy:function(a,b){var c,d,f;
return"string"==typeof b&&(c=a[b],b=a,a=c),n.isFunction(a)?(d=e.call(arguments,2),f=function(){return a.apply(b||this,d.concat(e.call(arguments)))
},f.guid=a.guid=a.guid||n.guid++,f):void 0
},now:Date.now,support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()
});
function s(a){var b=!!a&&"length" in a&&a.length,c=n.type(a);
return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a
}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0
},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;
d>c;
c++){if(a[c]===b){return c
}}return -1
},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;
return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)
},da=function(){m()
};
try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType
}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))
}:function(a,b){var c=a.length,d=0;
while(a[c++]=b[d++]){}a.length=c-1
}}
}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;
if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x){return d
}if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a))){if(f=o[1]){if(9===x){if(!(j=b.getElementById(f))){return d
}if(j.id===f){return d.push(j),d
}}else{if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f){return d.push(j),d
}}}else{if(o[2]){return H.apply(d,b.getElementsByTagName(a)),d
}if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName){return H.apply(d,b.getElementsByClassName(f)),d
}}}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x){w=b,s=a
}else{if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";
while(h--){r[h]=l+" "+qa(r[h])
}s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b
}}if(s){try{return H.apply(d,w.querySelectorAll(s)),d
}catch(y){}finally{k===u&&b.removeAttribute("id")
}}}}return i(a.replace(Q,"$1"),b,d,e)
}function ga(){var a=[];
function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e
}return b
}function ha(a){return a[u]=!0,a
}function ia(a){var b=n.createElement("div");
try{return !!a(b)
}catch(c){return !1
}finally{b.parentNode&&b.parentNode.removeChild(b),b=null
}}function ja(a,b){var c=a.split("|"),e=c.length;
while(e--){d.attrHandle[c[e]]=b
}}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);
if(d){return d
}if(c){while(c=c.nextSibling){if(c===b){return -1
}}}return a?1:-1
}function la(a){return function(b){var c=b.nodeName.toLowerCase();
return"input"===c&&b.type===a
}
}function ma(a){return function(b){var c=b.nodeName.toLowerCase();
return("input"===c||"button"===c)&&b.type===a
}
}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;
while(g--){c[e=f[g]]&&(c[e]=!(d[e]=c[e]))
}})
})
}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a
}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;
return b?"HTML"!==b.nodeName:!1
},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;
return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")
}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length
}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length
}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);
return c?[c]:[]
}},d.filter.ID=function(a){var b=a.replace(ba,ca);
return function(a){return a.getAttribute("id")===b
}
}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);
return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");
return c&&c.value===b
}
}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0
}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);
if("*"===a){while(c=f[e++]){1===c.nodeType&&d.push(c)
}return d
}return f
},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0
},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")
}),ia(function(a){var b=n.createElement("input");
b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")
})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)
}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;
return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))
}:function(a,b){if(b){while(b=b.parentNode){if(b===a){return !0
}}}return !1
},B=b?function(a,b){if(a===b){return l=!0,0
}var d=!a.compareDocumentPosition-!b.compareDocumentPosition;
return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)
}:function(a,b){if(a===b){return l=!0,0
}var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];
if(!e||!f){return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0
}if(e===f){return ka(a,b)
}c=a;
while(c=c.parentNode){g.unshift(c)
}c=b;
while(c=c.parentNode){h.unshift(c)
}while(g[d]===h[d]){d++
}return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0
},n):n
},fa.matches=function(a,b){return fa(a,null,null,b)
},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b))){try{var d=s.call(a,b);
if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType){return d
}}catch(e){}}return fa(b,n,null,[a]).length>0
},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)
},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);
var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;
return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null
},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)
},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;
if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++]){b===a[f]&&(e=d.push(f))
}while(e--){a.splice(d[e],1)
}}return k=null,a
},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;
if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent){return a.textContent
}for(a=a.firstChild;
a;
a=a.nextSibling){c+=e(a)
}}else{if(3===f||4===f){return a.nodeValue
}}}else{while(b=a[d++]){c+=e(b)
}}return c
},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)
},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a
},PSEUDO:function(a){var b,c=!a[6]&&a[2];
return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))
}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();
return"*"===a?function(){return !0
}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b
}
},CLASS:function(a){var b=y[a+" "];
return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")
})
},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);
return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0
}
},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;
return 1===d&&0===e?function(a){return !!a.parentNode
}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;
if(q){if(f){while(p){m=b;
while(m=m[p]){if(h?m.nodeName.toLowerCase()===r:1===m.nodeType){return !1
}}o=p="only"===a&&!o&&"nextSibling"
}return !0
}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];
while(m=++n&&m&&m[p]||(t=n=0)||o.pop()){if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];
break
}}}else{if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1){while(m=++n&&m&&m[p]||(t=n=0)||o.pop()){if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b)){break
}}}}return t-=e,t===d||t%d===0&&t/d>=0
}}
},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);
return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;
while(g--){d=J(a,f[g]),a[d]=!(c[d]=f[g])
}}):function(a){return e(a,0,c)
}):e
}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));
return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;
while(h--){(f=g[h])&&(a[h]=!(b[h]=f))
}}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()
}
}),has:ha(function(a){return function(b){return fa(a,b).length>0
}
}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1
}
}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;
do{if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang")){return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-")
}}while((b=b.parentNode)&&1===b.nodeType);
return !1
}
}),target:function(b){var c=a.location&&a.location.hash;
return c&&c.slice(1)===b.id
},root:function(a){return a===o
},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)
},enabled:function(a){return a.disabled===!1
},disabled:function(a){return a.disabled===!0
},checked:function(a){var b=a.nodeName.toLowerCase();
return"input"===b&&!!a.checked||"option"===b&&!!a.selected
},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0
},empty:function(a){for(a=a.firstChild;
a;
a=a.nextSibling){if(a.nodeType<6){return !1
}}return !0
},parent:function(a){return !d.pseudos.empty(a)
},header:function(a){return Y.test(a.nodeName)
},input:function(a){return X.test(a.nodeName)
},button:function(a){var b=a.nodeName.toLowerCase();
return"input"===b&&"button"===a.type||"button"===b
},text:function(a){var b;
return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())
},first:na(function(){return[0]
}),last:na(function(a,b){return[b-1]
}),eq:na(function(a,b,c){return[0>c?c+b:c]
}),even:na(function(a,b){for(var c=0;
b>c;
c+=2){a.push(c)
}return a
}),odd:na(function(a,b){for(var c=1;
b>c;
c+=2){a.push(c)
}return a
}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;
--d>=0;
){a.push(d)
}return a
}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;
++d<b;
){a.push(d)
}return a
})}},d.pseudos.nth=d.pseudos.eq;
for(b in {radio:!0,checkbox:!0,file:!0,password:!0,image:!0}){d.pseudos[b]=la(b)
}for(b in {submit:!0,reset:!0}){d.pseudos[b]=ma(b)
}function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];
if(k){return b?0:k.slice(0)
}h=a,i=[],j=d.preFilter;
while(h){c&&!(e=R.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));
for(g in d.filter){!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length))
}if(!c){break
}}return b?h.length:h?fa.error(a):z(a,i).slice(0)
};
function qa(a){for(var b=0,c=a.length,d="";
c>b;
b++){d+=a[b].value
}return d
}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;
return b.first?function(b,c,f){while(b=b[d]){if(1===b.nodeType||e){return a(b,c,f)
}}}:function(b,c,g){var h,i,j,k=[w,f];
if(g){while(b=b[d]){if((1===b.nodeType||e)&&a(b,c,g)){return !0
}}}else{while(b=b[d]){if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f){return k[2]=h[2]
}if(i[d]=k,k[2]=a(b,c,g)){return !0
}}}}}
}function sa(a){return a.length>1?function(b,c,d){var e=a.length;
while(e--){if(!a[e](b,c,d)){return !1
}}return !0
}:a[0]
}function ta(a,b,c){for(var d=0,e=b.length;
e>d;
d++){fa(a,b[d],c)
}return c
}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;
i>h;
h++){(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)))
}return g
}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;
if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;
while(k--){(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))
}}if(f){if(e||a){if(e){j=[],k=r.length;
while(k--){(l=r[k])&&j.push(q[k]=l)
}e(null,r=[],j,i)
}k=r.length;
while(k--){(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))
}}}else{r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)
}})
}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b
},h,!0),l=ra(function(a){return J(b,a)>-1
},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));
return b=null,e
}];
f>i;
i++){if(c=d.relative[a[i].type]){m=[ra(sa(m),c)]
}else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;
f>e;
e++){if(d.relative[a[e].type]){break
}}return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))
}m.push(c)
}}return sa(m)
}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||0.1,z=x.length;
for(k&&(j=g===n||g||k);
s!==z&&null!=(l=x[s]);
s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);
while(q=a[o++]){if(q(l,g||n,h)){i.push(l);
break
}}k&&(w=y)
}c&&((l=!q&&l)&&r--,f&&t.push(l))
}if(r+=s,c&&s!==r){o=0;
while(q=b[o++]){q(t,u,g,h)
}if(f){if(r>0){while(s--){t[s]||u[s]||(u[s]=F.call(i))
}}u=ua(u)
}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)
}return k&&(w=y,j=v),t
};
return c?ha(f):f
}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];
if(!f){b||(b=g(a)),c=b.length;
while(c--){f=wa(b[c]),f[u]?d.push(f):e.push(f)
}f=A(a,xa(e,d)),f.selector=a
}return f
},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);
if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b){return e
}n&&(b=b.parentNode),a=a.slice(j.shift().value.length)
}i=W.needsContext.test(a)?0:j.length;
while(i--){if(k=j[i],d.relative[l=k.type]){break
}if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a){return H.apply(e,f),e
}break
}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e
},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))
}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")
})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)
}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")
})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue
}),ia(function(a){return null==a.getAttribute("disabled")
})||ja(K,function(a,b,c){var d;
return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null
}),fa
}(a);
n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;
var u=function(a,b,c){var d=[],e=void 0!==c;
while((a=a[b])&&9!==a.nodeType){if(1===a.nodeType){if(e&&n(a).is(c)){break
}d.push(a)
}}return d
},v=function(a,b){for(var c=[];
a;
a=a.nextSibling){1===a.nodeType&&a!==b&&c.push(a)
}return c
},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;
function z(a,b,c){if(n.isFunction(b)){return n.grep(a,function(a,d){return !!b.call(a,d,a)!==c
})
}if(b.nodeType){return n.grep(a,function(a){return a===b!==c
})
}if("string"==typeof b){if(y.test(b)){return n.filter(b,a,c)
}b=n.filter(b,a)
}return n.grep(a,function(a){return h.call(b,a)>-1!==c
})
}n.filter=function(a,b,c){var d=b[0];
return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType
}))
},n.fn.extend({find:function(a){var b,c=this.length,d=[],e=this;
if("string"!=typeof a){return this.pushStack(n(a).filter(function(){for(b=0;
c>b;
b++){if(n.contains(e[b],this)){return !0
}}}))
}for(b=0;
c>b;
b++){n.find(a,e[b],d)
}return d=this.pushStack(c>1?n.unique(d):d),d.selector=this.selector?this.selector+" "+a:a,d
},filter:function(a){return this.pushStack(z(this,a||[],!1))
},not:function(a){return this.pushStack(z(this,a||[],!0))
},is:function(a){return !!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length
}});
var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;
if(!a){return this
}if(c=c||A,"string"==typeof a){if(e="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b){return !b||b.jquery?(b||c).find(a):this.constructor(b).find(a)
}if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b)){for(e in b){n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e])
}}return this
}return f=d.getElementById(e[2]),f&&f.parentNode&&(this.length=1,this[0]=f),this.context=d,this.selector=a,this
}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?void 0!==c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))
};
C.prototype=n.fn,A=n(d);
var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};
n.fn.extend({has:function(a){var b=n(a,this),c=b.length;
return this.filter(function(){for(var a=0;
c>a;
a++){if(n.contains(this,b[a])){return !0
}}})
},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;
e>d;
d++){for(c=this[d];
c&&c!==b;
c=c.parentNode){if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);
break
}}}return this.pushStack(f.length>1?n.uniqueSort(f):f)
},index:function(a){return a?"string"==typeof a?h.call(n(a),this[0]):h.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1
},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))
},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))
}});
function F(a,b){while((a=a[b])&&1!==a.nodeType){}return a
}n.each({parent:function(a){var b=a.parentNode;
return b&&11!==b.nodeType?b:null
},parents:function(a){return u(a,"parentNode")
},parentsUntil:function(a,b,c){return u(a,"parentNode",c)
},next:function(a){return F(a,"nextSibling")
},prev:function(a){return F(a,"previousSibling")
},nextAll:function(a){return u(a,"nextSibling")
},prevAll:function(a){return u(a,"previousSibling")
},nextUntil:function(a,b,c){return u(a,"nextSibling",c)
},prevUntil:function(a,b,c){return u(a,"previousSibling",c)
},siblings:function(a){return v((a.parentNode||{}).firstChild,a)
},children:function(a){return v(a.firstChild)
},contents:function(a){return a.contentDocument||n.merge([],a.childNodes)
}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);
return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||n.uniqueSort(e),D.test(a)&&e.reverse()),this.pushStack(e)
}
});
var G=/\S+/g;
function H(a){var b={};
return n.each(a.match(G)||[],function(a,c){b[c]=!0
}),b
}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);
var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;
g.length;
h=-1){c=g.shift();
while(++h<f.length){f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)
}}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")
},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)
})
}(arguments),c&&!b&&i()),this
},remove:function(){return n.each(arguments,function(a,b){var c;
while((c=n.inArray(b,f,c))>-1){f.splice(c,1),h>=c&&h--
}}),this
},has:function(a){return a?n.inArray(a,f)>-1:f.length>0
},empty:function(){return f&&(f=[]),this
},disable:function(){return e=g=[],f=c="",this
},disabled:function(){return !f
},lock:function(){return e=g=[],c||(f=c=""),this
},locked:function(){return !!e
},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this
},fire:function(){return j.fireWith(this,arguments),this
},fired:function(){return !!d
}};
return j
},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c
},always:function(){return e.done(arguments).fail(arguments),this
},then:function(){var a=arguments;
return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];
e[f[1]](function(){var a=g&&g.apply(this,arguments);
a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)
})
}),a=null
}).promise()
},promise:function(a){return null!=a?n.extend(a,d):d
}},e={};
return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];
d[f[1]]=g.add,h&&g.add(function(){c=h
},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this
},e[f[0]+"With"]=g.fireWith
}),d.promise(e),a&&a.call(e,e),e
},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)
}
},i,j,k;
if(d>1){for(i=new Array(d),j=new Array(d),k=new Array(d);
d>b;
b++){c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f
}}return f||g.resolveWith(k,c),g.promise()
}});
var I;
n.fn.ready=function(a){return n.ready.promise().done(a),this
},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)
},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))
}});
function J(){d.removeEventListener("DOMContentLoaded",J),a.removeEventListener("load",J),n.ready()
}n.ready.promise=function(b){return I||(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll?a.setTimeout(n.ready):(d.addEventListener("DOMContentLoaded",J),a.addEventListener("load",J))),I.promise(b)
},n.ready.promise();
var K=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;
if("object"===n.type(c)){e=!0;
for(h in c){K(a,b,h,c[h],!0,f,g)
}}else{if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)
})),b)){for(;
i>h;
h++){b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)))
}}}return e?a:j?b.call(a):i?b(a[0],c):f
},L=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType
};
function M(){this.expando=n.expando+M.uid++
}M.uid=1,M.prototype={register:function(a,b){var c=b||{};
return a.nodeType?a[this.expando]=c:Object.defineProperty(a,this.expando,{value:c,writable:!0,configurable:!0}),a[this.expando]
},cache:function(a){if(!L(a)){return{}
}var b=a[this.expando];
return b||(b={},L(a)&&(a.nodeType?a[this.expando]=b:Object.defineProperty(a,this.expando,{value:b,configurable:!0}))),b
},set:function(a,b,c){var d,e=this.cache(a);
if("string"==typeof b){e[b]=c
}else{for(d in b){e[d]=b[d]
}}return e
},get:function(a,b){return void 0===b?this.cache(a):a[this.expando]&&a[this.expando][b]
},access:function(a,b,c){var d;
return void 0===b||b&&"string"==typeof b&&void 0===c?(d=this.get(a,b),void 0!==d?d:this.get(a,n.camelCase(b))):(this.set(a,b,c),void 0!==c?c:b)
},remove:function(a,b){var c,d,e,f=a[this.expando];
if(void 0!==f){if(void 0===b){this.register(a)
}else{n.isArray(b)?d=b.concat(b.map(n.camelCase)):(e=n.camelCase(b),b in f?d=[b,e]:(d=e,d=d in f?[d]:d.match(G)||[])),c=d.length;
while(c--){delete f[d[c]]
}}(void 0===b||n.isEmptyObject(f))&&(a.nodeType?a[this.expando]=void 0:delete a[this.expando])
}},hasData:function(a){var b=a[this.expando];
return void 0!==b&&!n.isEmptyObject(b)
}};
var N=new M,O=new M,P=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Q=/[A-Z]/g;
function R(a,b,c){var d;
if(void 0===c&&1===a.nodeType){if(d="data-"+b.replace(Q,"-$&").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:P.test(c)?n.parseJSON(c):c
}catch(e){}O.set(a,b,c)
}else{c=void 0
}}return c
}n.extend({hasData:function(a){return O.hasData(a)||N.hasData(a)
},data:function(a,b,c){return O.access(a,b,c)
},removeData:function(a,b){O.remove(a,b)
},_data:function(a,b,c){return N.access(a,b,c)
},_removeData:function(a,b){N.remove(a,b)
}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;
if(void 0===a){if(this.length&&(e=O.get(f),1===f.nodeType&&!N.get(f,"hasDataAttrs"))){c=g.length;
while(c--){g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),R(f,d,e[d])))
}N.set(f,"hasDataAttrs",!0)
}return e
}return"object"==typeof a?this.each(function(){O.set(this,a)
}):K(this,function(b){var c,d;
if(f&&void 0===b){if(c=O.get(f,a)||O.get(f,a.replace(Q,"-$&").toLowerCase()),void 0!==c){return c
}if(d=n.camelCase(a),c=O.get(f,d),void 0!==c){return c
}if(c=R(f,d,void 0),void 0!==c){return c
}}else{d=n.camelCase(a),this.each(function(){var c=O.get(this,d);
O.set(this,d,b),a.indexOf("-")>-1&&void 0!==c&&O.set(this,a,b)
})
}},null,b,arguments.length>1,null,!0)
},removeData:function(a){return this.each(function(){O.remove(this,a)
})
}}),n.extend({queue:function(a,b,c){var d;
return a?(b=(b||"fx")+"queue",d=N.get(a,b),c&&(!d||n.isArray(c)?d=N.access(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0
},dequeue:function(a,b){b=b||"fx";
var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)
};
"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()
},_queueHooks:function(a,b){var c=b+"queueHooks";
return N.get(a,c)||N.access(a,c,{empty:n.Callbacks("once memory").add(function(){N.remove(a,[b+"queue",c])
})})
}}),n.fn.extend({queue:function(a,b){var c=2;
return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);
n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)
})
},dequeue:function(a){return this.each(function(){n.dequeue(this,a)
})
},clearQueue:function(a){return this.queue(a||"fx",[])
},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])
};
"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";
while(g--){c=N.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h))
}return h(),e.promise(b)
}});
var S=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,T=new RegExp("^(?:([+-])=|)("+S+")([a-z%]*)$","i"),U=["Top","Right","Bottom","Left"],V=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)
};
function W(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()
}:function(){return n.css(a,b,"")
},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&T.exec(n.css(a,b));
if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;
do{f=f||".5",k/=f,n.style(a,b,k+j)
}while(f!==(f=h()/i)&&1!==f&&--g)
}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e
}var X=/^(?:checkbox|radio)$/i,Y=/<([\w:-]+)/,Z=/^$|\/(?:java|ecma)script/i,$={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};
$.optgroup=$.option,$.tbody=$.tfoot=$.colgroup=$.caption=$.thead,$.th=$.td;
function _(a,b){var c="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):[];
return void 0===b||b&&n.nodeName(a,b)?n.merge([a],c):c
}function aa(a,b){for(var c=0,d=a.length;
d>c;
c++){N.set(a[c],"globalEval",!b||N.get(b[c],"globalEval"))
}}var ba=/<|&#?\w+;/;
function ca(a,b,c,d,e){for(var f,g,h,i,j,k,l=b.createDocumentFragment(),m=[],o=0,p=a.length;
p>o;
o++){if(f=a[o],f||0===f){if("object"===n.type(f)){n.merge(m,f.nodeType?[f]:f)
}else{if(ba.test(f)){g=g||l.appendChild(b.createElement("div")),h=(Y.exec(f)||["",""])[1].toLowerCase(),i=$[h]||$._default,g.innerHTML=i[1]+n.htmlPrefilter(f)+i[2],k=i[0];
while(k--){g=g.lastChild
}n.merge(m,g.childNodes),g=l.firstChild,g.textContent=""
}else{m.push(b.createTextNode(f))
}}}}l.textContent="",o=0;
while(f=m[o++]){if(d&&n.inArray(f,d)>-1){e&&e.push(f)
}else{if(j=n.contains(f.ownerDocument,f),g=_(l.appendChild(f),"script"),j&&aa(g),c){k=0;
while(f=g[k++]){Z.test(f.type||"")&&c.push(f)
}}}}return l
}!function(){var a=d.createDocumentFragment(),b=a.appendChild(d.createElement("div")),c=d.createElement("input");
c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),l.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue
}();
var da=/^key/,ea=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,fa=/^([^.]*)(?:\.(.+)|)/;
function ga(){return !0
}function ha(){return !1
}function ia(){try{return d.activeElement
}catch(a){}}function ja(a,b,c,d,e,f){var g,h;
if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);
for(h in b){ja(a,h,c,d,b[h],f)
}return a
}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1){e=ha
}else{if(!e){return a
}}return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)
},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)
})
}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=N.get(a);
if(r){c.handler&&(f=c,c=f.handler,e=f.selector),c.guid||(c.guid=n.guid++),(i=r.events)||(i=r.events={}),(g=r.handle)||(g=r.handle=function(b){return"undefined"!=typeof n&&n.event.triggered!==b.type?n.event.dispatch.apply(a,arguments):void 0
}),b=(b||"").match(G)||[""],j=b.length;
while(j--){h=fa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o&&(l=n.event.special[o]||{},o=(e?l.delegateType:l.bindType)||o,l=n.event.special[o]||{},k=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},f),(m=i[o])||(m=i[o]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,p,g)!==!1||a.addEventListener&&a.addEventListener(o,g)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),n.event.global[o]=!0)
}}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=N.hasData(a)&&N.get(a);
if(r&&(i=r.events)){b=(b||"").match(G)||[""],j=b.length;
while(j--){if(h=fa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=i[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;
while(f--){k=m[f],!e&&q!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k))
}g&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete i[o])
}else{for(o in i){n.event.remove(a,o+b[j],c,d,!0)
}}}n.isEmptyObject(i)&&N.remove(a,"handle events")
}},dispatch:function(a){a=n.event.fix(a);
var b,c,d,f,g,h=[],i=e.call(arguments),j=(N.get(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};
if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;
while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;
while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped()){a.rnamespace&&!a.rnamespace.test(g.namespace)||(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))
}}return k.postDispatch&&k.postDispatch.call(this,a),a.result
}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;
if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1)){for(;
i!==this;
i=i.parentNode||this){if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;
h>c;
c++){f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f)
}d.length&&g.push({elem:i,handlers:d})
}}}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g
},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a
}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button;
return null==a.pageX&&null!=b.clientX&&(c=a.target.ownerDocument||d,e=c.documentElement,f=c.body,a.pageX=b.clientX+(e&&e.scrollLeft||f&&f.scrollLeft||0)-(e&&e.clientLeft||f&&f.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||f&&f.scrollTop||0)-(e&&e.clientTop||f&&f.clientTop||0)),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a
}},fix:function(a){if(a[n.expando]){return a
}var b,c,e,f=a.type,g=a,h=this.fixHooks[f];
h||(this.fixHooks[f]=h=ea.test(f)?this.mouseHooks:da.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;
while(b--){c=e[b],a[c]=g[c]
}return a.target||(a.target=d),3===a.target.nodeType&&(a.target=a.target.parentNode),h.filter?h.filter(a,g):a
},special:{load:{noBubble:!0},focus:{trigger:function(){return this!==ia()&&this.focus?(this.focus(),!1):void 0
},delegateType:"focusin"},blur:{trigger:function(){return this===ia()&&this.blur?(this.blur(),!1):void 0
},delegateType:"focusout"},click:{trigger:function(){return"checkbox"===this.type&&this.click&&n.nodeName(this,"input")?(this.click(),!1):void 0
},_default:function(a){return n.nodeName(a.target,"a")
}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)
}}}},n.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)
},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?ga:ha):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void (this[n.expando]=!0)):new n.Event(a,b)
},n.Event.prototype={constructor:n.Event,isDefaultPrevented:ha,isPropagationStopped:ha,isImmediatePropagationStopped:ha,preventDefault:function(){var a=this.originalEvent;
this.isDefaultPrevented=ga,a&&a.preventDefault()
},stopPropagation:function(){var a=this.originalEvent;
this.isPropagationStopped=ga,a&&a.stopPropagation()
},stopImmediatePropagation:function(){var a=this.originalEvent;
this.isImmediatePropagationStopped=ga,a&&a.stopImmediatePropagation(),this.stopPropagation()
}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;
return e&&(e===d||n.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c
}}
}),n.fn.extend({on:function(a,b,c,d){return ja(this,a,b,c,d)
},one:function(a,b,c,d){return ja(this,a,b,c,d,1)
},off:function(a,b,c){var d,e;
if(a&&a.preventDefault&&a.handleObj){return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this
}if("object"==typeof a){for(e in a){this.off(e,b,a[e])
}return this
}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=ha),this.each(function(){n.event.remove(this,a,c,b)
})
}});
var ka=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,la=/<script|<style|<link/i,ma=/checked\s*(?:[^=]|=\s*.checked.)/i,na=/^true\/(.*)/,oa=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
function pa(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a
}function qa(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a
}function ra(a){var b=na.exec(a.type);
return b?a.type=b[1]:a.removeAttribute("type"),a
}function sa(a,b){var c,d,e,f,g,h,i,j;
if(1===b.nodeType){if(N.hasData(a)&&(f=N.access(a),g=N.set(b,f),j=f.events)){delete g.handle,g.events={};
for(e in j){for(c=0,d=j[e].length;
d>c;
c++){n.event.add(b,e,j[e][c])
}}}O.hasData(a)&&(h=O.access(a),i=n.extend({},h),O.set(b,i))
}}function ta(a,b){var c=b.nodeName.toLowerCase();
"input"===c&&X.test(a.type)?b.checked=a.checked:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)
}function ua(a,b,c,d){b=f.apply([],b);
var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);
if(r||o>1&&"string"==typeof q&&!l.checkClone&&ma.test(q)){return a.each(function(e){var f=a.eq(e);
r&&(b[0]=q.call(this,e,f.html())),ua(f,b,c,d)
})
}if(o&&(e=ca(b,a[0].ownerDocument,!1,a,d),g=e.firstChild,1===e.childNodes.length&&(e=g),g||d)){for(h=n.map(_(e,"script"),qa),i=h.length;
o>m;
m++){j=e,m!==p&&(j=n.clone(j,!0,!0),i&&n.merge(h,_(j,"script"))),c.call(a[m],j,m)
}if(i){for(k=h[h.length-1].ownerDocument,n.map(h,ra),m=0;
i>m;
m++){j=h[m],Z.test(j.type||"")&&!N.access(j,"globalEval")&&n.contains(k,j)&&(j.src?n._evalUrl&&n._evalUrl(j.src):n.globalEval(j.textContent.replace(oa,"")))
}}}return a
}function va(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;
null!=(d=e[f]);
f++){c||1!==d.nodeType||n.cleanData(_(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&aa(_(d,"script")),d.parentNode.removeChild(d))
}return a
}n.extend({htmlPrefilter:function(a){return a.replace(ka,"<$1></$2>")
},clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=n.contains(a.ownerDocument,a);
if(!(l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a))){for(g=_(h),f=_(a),d=0,e=f.length;
e>d;
d++){ta(f[d],g[d])
}}if(b){if(c){for(f=f||_(a),g=g||_(h),d=0,e=f.length;
e>d;
d++){sa(f[d],g[d])
}}else{sa(a,h)
}}return g=_(h,"script"),g.length>0&&aa(g,!i&&_(a,"script")),h
},cleanData:function(a){for(var b,c,d,e=n.event.special,f=0;
void 0!==(c=a[f]);
f++){if(L(c)){if(b=c[N.expando]){if(b.events){for(d in b.events){e[d]?n.event.remove(c,d):n.removeEvent(c,d,b.handle)
}}c[N.expando]=void 0
}c[O.expando]&&(c[O.expando]=void 0)
}}}}),n.fn.extend({domManip:ua,detach:function(a){return va(this,a,!0)
},remove:function(a){return va(this,a)
},text:function(a){return K(this,function(a){return void 0===a?n.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=a)
})
},null,a,arguments.length)
},append:function(){return ua(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=pa(this,a);
b.appendChild(a)
}})
},prepend:function(){return ua(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=pa(this,a);
b.insertBefore(a,b.firstChild)
}})
},before:function(){return ua(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)
})
},after:function(){return ua(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)
})
},empty:function(){for(var a,b=0;
null!=(a=this[b]);
b++){1===a.nodeType&&(n.cleanData(_(a,!1)),a.textContent="")
}return this
},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)
})
},html:function(a){return K(this,function(a){var b=this[0]||{},c=0,d=this.length;
if(void 0===a&&1===b.nodeType){return b.innerHTML
}if("string"==typeof a&&!la.test(a)&&!$[(Y.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);
try{for(;
d>c;
c++){b=this[c]||{},1===b.nodeType&&(n.cleanData(_(b,!1)),b.innerHTML=a)
}b=0
}catch(e){}}b&&this.empty().append(a)
},null,a,arguments.length)
},replaceWith:function(){var a=[];
return ua(this,arguments,function(b){var c=this.parentNode;
n.inArray(this,a)<0&&(n.cleanData(_(this)),c&&c.replaceChild(b,this))
},a)
}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=[],e=n(a),f=e.length-1,h=0;
f>=h;
h++){c=h===f?this:this.clone(!0),n(e[h])[b](c),g.apply(d,c.get())
}return this.pushStack(d)
}
});
var wa,xa={HTML:"block",BODY:"block"};
function ya(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");
return c.detach(),d
}function za(a){var b=d,c=xa[a];
return c||(c=ya(a,b),"none"!==c&&c||(wa=(wa||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=wa[0].contentDocument,b.write(),b.close(),c=ya(a,b),wa.detach()),xa[a]=c),c
}var Aa=/^margin/,Ba=new RegExp("^("+S+")(?!px)[a-z%]+$","i"),Ca=function(b){var c=b.ownerDocument.defaultView;
return c&&c.opener||(c=a),c.getComputedStyle(b)
},Da=function(a,b,c,d){var e,f,g={};
for(f in b){g[f]=a.style[f],a.style[f]=b[f]
}e=c.apply(a,d||[]);
for(f in b){a.style[f]=g[f]
}return e
},Ea=d.documentElement;
!function(){var b,c,e,f,g=d.createElement("div"),h=d.createElement("div");
if(h.style){h.style.backgroundClip="content-box",h.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===h.style.backgroundClip,g.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",g.appendChild(h);
function i(){h.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",h.innerHTML="",Ea.appendChild(g);
var d=a.getComputedStyle(h);
b="1%"!==d.top,f="2px"===d.marginLeft,c="4px"===d.width,h.style.marginRight="50%",e="4px"===d.marginRight,Ea.removeChild(g)
}n.extend(l,{pixelPosition:function(){return i(),b
},boxSizingReliable:function(){return null==c&&i(),c
},pixelMarginRight:function(){return null==c&&i(),e
},reliableMarginLeft:function(){return null==c&&i(),f
},reliableMarginRight:function(){var b,c=h.appendChild(d.createElement("div"));
return c.style.cssText=h.style.cssText="-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",c.style.marginRight=c.style.width="0",h.style.width="1px",Ea.appendChild(g),b=!parseFloat(a.getComputedStyle(c).marginRight),Ea.removeChild(g),h.removeChild(c),b
}})
}}();
function Fa(a,b,c){var d,e,f,g,h=a.style;
return c=c||Ca(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Ba.test(g)&&Aa.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0!==g?g+"":g
}function Ga(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)
}}
}var Ha=/^(none|table(?!-c[ea]).+)/,Ia={position:"absolute",visibility:"hidden",display:"block"},Ja={letterSpacing:"0",fontWeight:"400"},Ka=["Webkit","O","Moz","ms"],La=d.createElement("div").style;
function Ma(a){if(a in La){return a
}var b=a[0].toUpperCase()+a.slice(1),c=Ka.length;
while(c--){if(a=Ka[c]+b,a in La){return a
}}}function Na(a,b,c){var d=T.exec(b);
return d?Math.max(0,d[2]-(c||0))+(d[3]||"px"):b
}function Oa(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;
4>f;
f+=2){"margin"===c&&(g+=n.css(a,c+U[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+U[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+U[f]+"Width",!0,e))):(g+=n.css(a,"padding"+U[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+U[f]+"Width",!0,e)))
}return g
}function Pa(b,c,e){var f=!0,g="width"===c?b.offsetWidth:b.offsetHeight,h=Ca(b),i="border-box"===n.css(b,"boxSizing",!1,h);
if(d.msFullscreenElement&&a.top!==a&&b.getClientRects().length&&(g=Math.round(100*b.getBoundingClientRect()[c])),0>=g||null==g){if(g=Fa(b,c,h),(0>g||null==g)&&(g=b.style[c]),Ba.test(g)){return g
}f=i&&(l.boxSizingReliable()||g===b.style[c]),g=parseFloat(g)||0
}return g+Oa(b,c,e||(i?"border":"content"),f,h)+"px"
}function Qa(a,b){for(var c,d,e,f=[],g=0,h=a.length;
h>g;
g++){d=a[g],d.style&&(f[g]=N.get(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&V(d)&&(f[g]=N.access(d,"olddisplay",za(d.nodeName)))):(e=V(d),"none"===c&&e||N.set(d,"olddisplay",e?c:n.css(d,"display"))))
}for(g=0;
h>g;
g++){d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"))
}return a
}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Fa(a,"opacity");
return""===c?"1":c
}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;
return b=n.cssProps[h]||(n.cssProps[h]=Ma(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c?g&&"get" in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]:(f=typeof c,"string"===f&&(e=T.exec(c))&&e[1]&&(c=W(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),g&&"set" in g&&void 0===(c=g.set(a,c,d))||(i[b]=c)),void 0)
}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);
return b=n.cssProps[h]||(n.cssProps[h]=Ma(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get" in g&&(e=g.get(a,!0,c)),void 0===e&&(e=Fa(a,b,d)),"normal"===e&&b in Ja&&(e=Ja[b]),""===c||c?(f=parseFloat(e),c===!0||isFinite(f)?f||0:e):e
}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Ha.test(n.css(a,"display"))&&0===a.offsetWidth?Da(a,Ia,function(){return Pa(a,b,d)
}):Pa(a,b,d):void 0
},set:function(a,c,d){var e,f=d&&Ca(a),g=d&&Oa(a,b,d,"border-box"===n.css(a,"boxSizing",!1,f),f);
return g&&(e=T.exec(c))&&"px"!==(e[3]||"px")&&(a.style[b]=c,c=n.css(a,b)),Na(a,c,g)
}}
}),n.cssHooks.marginLeft=Ga(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Fa(a,"marginLeft"))||a.getBoundingClientRect().left-Da(a,{marginLeft:0},function(){return a.getBoundingClientRect().left
}))+"px":void 0
}),n.cssHooks.marginRight=Ga(l.reliableMarginRight,function(a,b){return b?Da(a,{display:"inline-block"},Fa,[a,"marginRight"]):void 0
}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];
4>d;
d++){e[a+U[d]+b]=f[d]||f[d-2]||f[0]
}return e
}},Aa.test(a)||(n.cssHooks[a+b].set=Na)
}),n.fn.extend({css:function(a,b){return K(this,function(a,b,c){var d,e,f={},g=0;
if(n.isArray(b)){for(d=Ca(a),e=b.length;
e>g;
g++){f[b[g]]=n.css(a,b[g],!1,d)
}return f
}return void 0!==c?n.style(a,b,c):n.css(a,b)
},a,b,arguments.length>1)
},show:function(){return Qa(this,!0)
},hide:function(){return Qa(this)
},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){V(this)?n(this).show():n(this).hide()
})
}});
function Ra(a,b,c,d,e){return new Ra.prototype.init(a,b,c,d,e)
}n.Tween=Ra,Ra.prototype={constructor:Ra,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")
},cur:function(){var a=Ra.propHooks[this.prop];
return a&&a.get?a.get(this):Ra.propHooks._default.get(this)
},run:function(a){var b,c=Ra.propHooks[this.prop];
return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Ra.propHooks._default.set(this),this
}},Ra.prototype.init.prototype=Ra.prototype,Ra.propHooks={_default:{get:function(a){var b;
return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)
},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)
}}},Ra.propHooks.scrollTop=Ra.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)
}},n.easing={linear:function(a){return a
},swing:function(a){return 0.5-Math.cos(a*Math.PI)/2
},_default:"swing"},n.fx=Ra.prototype.init,n.fx.step={};
var Sa,Ta,Ua=/^(?:toggle|show|hide)$/,Va=/queueHooks$/;
function Wa(){return a.setTimeout(function(){Sa=void 0
}),Sa=n.now()
}function Xa(a,b){var c,d=0,e={height:a};
for(b=b?1:0;
4>d;
d+=2-b){c=U[d],e["margin"+c]=e["padding"+c]=a
}return b&&(e.opacity=e.width=a),e
}function Ya(a,b,c){for(var d,e=(_a.tweeners[b]||[]).concat(_a.tweeners["*"]),f=0,g=e.length;
g>f;
f++){if(d=e[f].call(c,b,a)){return d
}}}function Za(a,b,c){var d,e,f,g,h,i,j,k,l=this,m={},o=a.style,p=a.nodeType&&V(a),q=N.get(a,"fxshow");
c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()
}),h.unqueued++,l.always(function(){l.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()
})
})),1===a.nodeType&&("height" in b||"width" in b)&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=n.css(a,"display"),k="none"===j?N.get(a,"olddisplay")||za(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(o.display="inline-block")),c.overflow&&(o.overflow="hidden",l.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]
}));
for(d in b){if(e=b[d],Ua.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d]){continue
}p=!0
}m[d]=q&&q[d]||n.style(a,d)
}else{j=void 0
}}if(n.isEmptyObject(m)){"inline"===("none"===j?za(a.nodeName):j)&&(o.display=j)
}else{q?"hidden" in q&&(p=q.hidden):q=N.access(a,"fxshow",{}),f&&(q.hidden=!p),p?n(a).show():l.done(function(){n(a).hide()
}),l.done(function(){var b;
N.remove(a,"fxshow");
for(b in m){n.style(a,b,m[b])
}});
for(d in m){g=Ya(p?q[d]:0,d,l),d in q||(q[d]=g.start,p&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))
}}}function $a(a,b){var c,d,e,f,g;
for(c in a){if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand" in g){f=g.expand(f),delete a[d];
for(c in f){c in a||(a[c]=f[c],b[c]=e)
}}else{b[d]=e
}}}function _a(a,b,c){var d,e,f=0,g=_a.prefilters.length,h=n.Deferred().always(function(){delete i.elem
}),i=function(){if(e){return !1
}for(var b=Sa||Wa(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;
i>g;
g++){j.tweens[g].run(f)
}return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)
},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:Sa||Wa(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);
return j.tweens.push(d),d
},stop:function(b){var c=0,d=b?j.tweens.length:0;
if(e){return this
}for(e=!0;
d>c;
c++){j.tweens[c].run(1)
}return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this
}}),k=j.props;
for($a(k,j.opts.specialEasing);
g>f;
f++){if(d=_a.prefilters[f].call(j,a,k,j.opts)){return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d
}}return n.map(k,Ya,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)
}n.Animation=n.extend(_a,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);
return W(c.elem,a,T.exec(b),c),c
}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);
for(var c,d=0,e=a.length;
e>d;
d++){c=a[d],_a.tweeners[c]=_a.tweeners[c]||[],_a.tweeners[c].unshift(b)
}},prefilters:[Za],prefilter:function(a,b){b?_a.prefilters.unshift(a):_a.prefilters.push(a)
}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};
return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)
},d
},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(V).css("opacity",0).show().end().animate({opacity:b},a,c,d)
},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=_a(this,n.extend({},a),f);
(e||N.get(this,"finish"))&&b.stop(!0)
};
return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)
},stop:function(a,b,c){var d=function(a){var b=a.stop;
delete a.stop,b(c)
};
return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=N.get(this);
if(e){g[e]&&g[e].stop&&d(g[e])
}else{for(e in g){g[e]&&g[e].stop&&Va.test(e)&&d(g[e])
}}for(e=f.length;
e--;
){f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1))
}!b&&c||n.dequeue(this,a)
})
},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=N.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;
for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;
b--;
){f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1))
}for(b=0;
g>b;
b++){d[b]&&d[b].finish&&d[b].finish.call(this)
}delete c.finish
})
}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];
n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(Xa(b,!0),a,d,e)
}
}),n.each({slideDown:Xa("show"),slideUp:Xa("hide"),slideToggle:Xa("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)
}
}),n.timers=[],n.fx.tick=function(){var a,b=0,c=n.timers;
for(Sa=n.now();
b<c.length;
b++){a=c[b],a()||c[b]!==a||c.splice(b--,1)
}c.length||n.fx.stop(),Sa=void 0
},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()
},n.fx.interval=13,n.fx.start=function(){Ta||(Ta=a.setInterval(n.fx.tick,n.fx.interval))
},n.fx.stop=function(){a.clearInterval(Ta),Ta=null
},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);
d.stop=function(){a.clearTimeout(e)
}
})
},function(){var a=d.createElement("input"),b=d.createElement("select"),c=b.appendChild(d.createElement("option"));
a.type="checkbox",l.checkOn=""!==a.value,l.optSelected=c.selected,b.disabled=!0,l.optDisabled=!c.disabled,a=d.createElement("input"),a.value="t",a.type="radio",l.radioValue="t"===a.value
}();
var ab,bb=n.expr.attrHandle;
n.fn.extend({attr:function(a,b){return K(this,n.attr,a,b,arguments.length>1)
},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)
})
}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;
if(3!==f&&8!==f&&2!==f){return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ab:void 0)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set" in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get" in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))
}},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;
return a.setAttribute("type",b),c&&(a.value=c),b
}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);
if(f&&1===a.nodeType){while(c=f[e++]){d=n.propFix[c]||c,n.expr.match.bool.test(c)&&(a[d]=!1),a.removeAttribute(c)
}}}}),ab={set:function(a,b,c){return b===!1?n.removeAttr(a,c):a.setAttribute(c,c),c
}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=bb[b]||n.find.attr;
bb[b]=function(a,b,d){var e,f;
return d||(f=bb[b],bb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,bb[b]=f),e
}
});
var cb=/^(?:input|select|textarea|button)$/i,db=/^(?:a|area)$/i;
n.fn.extend({prop:function(a,b){return K(this,n.prop,a,b,arguments.length>1)
},removeProp:function(a){return this.each(function(){delete this[n.propFix[a]||a]
})
}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;
if(3!==f&&8!==f&&2!==f){return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set" in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get" in e&&null!==(d=e.get(a,b))?d:a[b]
}},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");
return b?parseInt(b,10):cb.test(a.nodeName)||db.test(a.nodeName)&&a.href?0:-1
}}},propFix:{"for":"htmlFor","class":"className"}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;
return b&&b.parentNode&&b.parentNode.selectedIndex,null
},set:function(a){var b=a.parentNode;
b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)
}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this
});
var eb=/[\t\r\n\f]/g;
function fb(a){return a.getAttribute&&a.getAttribute("class")||""
}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;
if(n.isFunction(a)){return this.each(function(b){n(this).addClass(a.call(this,b,fb(this)))
})
}if("string"==typeof a&&a){b=a.match(G)||[];
while(c=this[i++]){if(e=fb(c),d=1===c.nodeType&&(" "+e+" ").replace(eb," ")){g=0;
while(f=b[g++]){d.indexOf(" "+f+" ")<0&&(d+=f+" ")
}h=n.trim(d),e!==h&&c.setAttribute("class",h)
}}}return this
},removeClass:function(a){var b,c,d,e,f,g,h,i=0;
if(n.isFunction(a)){return this.each(function(b){n(this).removeClass(a.call(this,b,fb(this)))
})
}if(!arguments.length){return this.attr("class","")
}if("string"==typeof a&&a){b=a.match(G)||[];
while(c=this[i++]){if(e=fb(c),d=1===c.nodeType&&(" "+e+" ").replace(eb," ")){g=0;
while(f=b[g++]){while(d.indexOf(" "+f+" ")>-1){d=d.replace(" "+f+" "," ")
}}h=n.trim(d),e!==h&&c.setAttribute("class",h)
}}}return this
},toggleClass:function(a,b){var c=typeof a;
return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,fb(this),b),b)
}):this.each(function(){var b,d,e,f;
if("string"===c){d=0,e=n(this),f=a.match(G)||[];
while(b=f[d++]){e.hasClass(b)?e.removeClass(b):e.addClass(b)
}}else{void 0!==a&&"boolean"!==c||(b=fb(this),b&&N.set(this,"__className__",b),this.setAttribute&&this.setAttribute("class",b||a===!1?"":N.get(this,"__className__")||""))
}})
},hasClass:function(a){var b,c,d=0;
b=" "+a+" ";
while(c=this[d++]){if(1===c.nodeType&&(" "+fb(c)+" ").replace(eb," ").indexOf(b)>-1){return !0
}}return !1
}});
var gb=/\r/g,hb=/[\x20\t\r\n\f]+/g;
n.fn.extend({val:function(a){var b,c,d,e=this[0];
if(arguments.length){return d=n.isFunction(a),this.each(function(c){var e;
1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""
})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set" in b&&void 0!==b.set(this,e,"value")||(this.value=e))
})
}if(e){return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get" in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(gb,""):null==c?"":c)
}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");
return null!=b?b:n.trim(n.text(a)).replace(hb," ")
}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;
h>i;
i++){if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f){return b
}g.push(b)
}}return g
},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;
while(g--){d=e[g],(d.selected=n.inArray(n.valHooks.option.get(d),f)>-1)&&(c=!0)
}return c||(a.selectedIndex=-1),f
}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0
}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value
})
});
var ib=/^(?:focusinfocus|focusoutblur)$/;
n.extend(n.event,{trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];
if(h=i=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!ib.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),l=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),o=n.event.special[q]||{},f||!o.trigger||o.trigger.apply(e,c)!==!1)){if(!f&&!o.noBubble&&!n.isWindow(e)){for(j=o.delegateType||q,ib.test(j+q)||(h=h.parentNode);
h;
h=h.parentNode){p.push(h),i=h
}i===(e.ownerDocument||d)&&p.push(i.defaultView||i.parentWindow||a)
}g=0;
while((h=p[g++])&&!b.isPropagationStopped()){b.type=g>1?j:o.bindType||q,m=(N.get(h,"events")||{})[b.type]&&N.get(h,"handle"),m&&m.apply(h,c),m=l&&h[l],m&&m.apply&&L(h)&&(b.result=m.apply(h,c),b.result===!1&&b.preventDefault())
}return b.type=q,f||b.isDefaultPrevented()||o._default&&o._default.apply(p.pop(),c)!==!1||!L(e)||l&&n.isFunction(e[q])&&!n.isWindow(e)&&(i=e[l],i&&(e[l]=null),n.event.triggered=q,e[q](),n.event.triggered=void 0,i&&(e[l]=i)),b.result
}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});
n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()
}}),n.fn.extend({trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)
})
},triggerHandler:function(a,b){var c=this[0];
return c?n.event.trigger(a,b,c,!0):void 0
}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)
}
}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)
}}),l.focusin="onfocusin" in a,l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))
};
n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=N.access(d,b);
e||d.addEventListener(a,c,!0),N.access(d,b,(e||0)+1)
},teardown:function(){var d=this.ownerDocument||this,e=N.access(d,b)-1;
e?N.access(d,b,e):(d.removeEventListener(a,c,!0),N.remove(d,b))
}}
});
var jb=a.location,kb=n.now(),lb=/\?/;
n.parseJSON=function(a){return JSON.parse(a+"")
},n.parseXML=function(b){var c;
if(!b||"string"!=typeof b){return null
}try{c=(new a.DOMParser).parseFromString(b,"text/xml")
}catch(d){c=void 0
}return c&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c
};
var mb=/#.*$/,nb=/([?&])_=[^&]*/,ob=/^(.*?):[ \t]*([^\r\n]*)$/gm,pb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,qb=/^(?:GET|HEAD)$/,rb=/^\/\//,sb={},tb={},ub="*/".concat("*"),vb=d.createElement("a");
vb.href=jb.href;
function wb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");
var d,e=0,f=b.toLowerCase().match(G)||[];
if(n.isFunction(c)){while(d=f[e++]){"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)
}}}
}function xb(a,b,c,d){var e={},f=a===tb;
function g(h){var i;
return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);
return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)
}),i
}return g(b.dataTypes[0])||!e["*"]&&g("*")
}function yb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};
for(c in b){void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c])
}return d&&n.extend(!0,a,d),a
}function zb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;
while("*"===i[0]){i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"))
}if(d){for(e in h){if(h[e]&&h[e].test(d)){i.unshift(e);
break
}}}if(i[0] in c){f=i[0]
}else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;
break
}g||(g=e)
}f=f||g
}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0
}function Ab(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();
if(k[1]){for(g in a.converters){j[g.toLowerCase()]=a.converters[g]
}}f=k.shift();
while(f){if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift()){if("*"===f){f=i
}else{if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g){for(e in j){if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));
break
}}}if(g!==!0){if(g&&a["throws"]){b=g(b)
}else{try{b=g(b)
}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}
}}}}}}}return{state:"success",data:b}
}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:jb.href,type:"GET",isLocal:pb.test(jb.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":ub,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?yb(yb(a,n.ajaxSettings),b):yb(n.ajaxSettings,a)
},ajaxPrefilter:wb(sb),ajaxTransport:wb(tb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};
var e,f,g,h,i,j,k,l,m=n.ajaxSetup({},c),o=m.context||m,p=m.context&&(o.nodeType||o.jquery)?n(o):n.event,q=n.Deferred(),r=n.Callbacks("once memory"),s=m.statusCode||{},t={},u={},v=0,w="canceled",x={readyState:0,getResponseHeader:function(a){var b;
if(2===v){if(!h){h={};
while(b=ob.exec(g)){h[b[1].toLowerCase()]=b[2]
}}b=h[a.toLowerCase()]
}return null==b?null:b
},getAllResponseHeaders:function(){return 2===v?g:null
},setRequestHeader:function(a,b){var c=a.toLowerCase();
return v||(a=u[c]=u[c]||a,t[a]=b),this
},overrideMimeType:function(a){return v||(m.mimeType=a),this
},statusCode:function(a){var b;
if(a){if(2>v){for(b in a){s[b]=[s[b],a[b]]
}}else{x.always(a[x.status])
}}return this
},abort:function(a){var b=a||w;
return e&&e.abort(b),z(0,b),this
}};
if(q.promise(x).complete=r.add,x.success=x.done,x.error=x.fail,m.url=((b||m.url||jb.href)+"").replace(mb,"").replace(rb,jb.protocol+"//"),m.type=c.method||c.type||m.method||m.type,m.dataTypes=n.trim(m.dataType||"*").toLowerCase().match(G)||[""],null==m.crossDomain){j=d.createElement("a");
try{j.href=m.url,j.href=j.href,m.crossDomain=vb.protocol+"//"+vb.host!=j.protocol+"//"+j.host
}catch(y){m.crossDomain=!0
}}if(m.data&&m.processData&&"string"!=typeof m.data&&(m.data=n.param(m.data,m.traditional)),xb(sb,m,c,x),2===v){return x
}k=n.event&&m.global,k&&0===n.active++&&n.event.trigger("ajaxStart"),m.type=m.type.toUpperCase(),m.hasContent=!qb.test(m.type),f=m.url,m.hasContent||(m.data&&(f=m.url+=(lb.test(f)?"&":"?")+m.data,delete m.data),m.cache===!1&&(m.url=nb.test(f)?f.replace(nb,"$1_="+kb++):f+(lb.test(f)?"&":"?")+"_="+kb++)),m.ifModified&&(n.lastModified[f]&&x.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&x.setRequestHeader("If-None-Match",n.etag[f])),(m.data&&m.hasContent&&m.contentType!==!1||c.contentType)&&x.setRequestHeader("Content-Type",m.contentType),x.setRequestHeader("Accept",m.dataTypes[0]&&m.accepts[m.dataTypes[0]]?m.accepts[m.dataTypes[0]]+("*"!==m.dataTypes[0]?", "+ub+"; q=0.01":""):m.accepts["*"]);
for(l in m.headers){x.setRequestHeader(l,m.headers[l])
}if(m.beforeSend&&(m.beforeSend.call(o,x,m)===!1||2===v)){return x.abort()
}w="abort";
for(l in {success:1,error:1,complete:1}){x[l](m[l])
}if(e=xb(tb,m,c,x)){if(x.readyState=1,k&&p.trigger("ajaxSend",[x,m]),2===v){return x
}m.async&&m.timeout>0&&(i=a.setTimeout(function(){x.abort("timeout")
},m.timeout));
try{v=1,e.send(t,z)
}catch(y){if(!(2>v)){throw y
}z(-1,y)
}}else{z(-1,"No Transport")
}function z(b,c,d,h){var j,l,t,u,w,y=c;
2!==v&&(v=2,i&&a.clearTimeout(i),e=void 0,g=h||"",x.readyState=b>0?4:0,j=b>=200&&300>b||304===b,d&&(u=zb(m,x,d)),u=Ab(m,u,x,j),j?(m.ifModified&&(w=x.getResponseHeader("Last-Modified"),w&&(n.lastModified[f]=w),w=x.getResponseHeader("etag"),w&&(n.etag[f]=w)),204===b||"HEAD"===m.type?y="nocontent":304===b?y="notmodified":(y=u.state,l=u.data,t=u.error,j=!t)):(t=y,!b&&y||(y="error",0>b&&(b=0))),x.status=b,x.statusText=(c||y)+"",j?q.resolveWith(o,[l,y,x]):q.rejectWith(o,[x,y,t]),x.statusCode(s),s=void 0,k&&p.trigger(j?"ajaxSuccess":"ajaxError",[x,m,j?l:t]),r.fireWith(o,[x,y]),k&&(p.trigger("ajaxComplete",[x,m]),--n.active||n.event.trigger("ajaxStop")))
}return x
},getJSON:function(a,b,c){return n.get(a,b,c,"json")
},getScript:function(a,b){return n.get(a,void 0,b,"script")
}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))
}
}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})
},n.fn.extend({wrapAll:function(a){var b;
return n.isFunction(a)?this.each(function(b){n(this).wrapAll(a.call(this,b))
}):(this[0]&&(b=n(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;
while(a.firstElementChild){a=a.firstElementChild
}return a
}).append(this)),this)
},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))
}):this.each(function(){var b=n(this),c=b.contents();
c.length?c.wrapAll(a):b.append(a)
})
},wrap:function(a){var b=n.isFunction(a);
return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)
})
},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)
}).end()
}}),n.expr.filters.hidden=function(a){return !n.expr.filters.visible(a)
},n.expr.filters.visible=function(a){return a.offsetWidth>0||a.offsetHeight>0||a.getClientRects().length>0
};
var Bb=/%20/g,Cb=/\[\]$/,Db=/\r?\n/g,Eb=/^(?:submit|button|image|reset|file)$/i,Fb=/^(?:input|select|textarea|keygen)/i;
function Gb(a,b,c,d){var e;
if(n.isArray(b)){n.each(b,function(b,e){c||Cb.test(a)?d(a,e):Gb(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)
})
}else{if(c||"object"!==n.type(b)){d(a,b)
}else{for(e in b){Gb(a+"["+e+"]",b[e],c,d)
}}}}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)
};
if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a)){n.each(a,function(){e(this.name,this.value)
})
}else{for(c in a){Gb(c,a[c],b,e)
}}return d.join("&").replace(Bb,"+")
},n.fn.extend({serialize:function(){return n.param(this.serializeArray())
},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");
return a?n.makeArray(a):this
}).filter(function(){var a=this.type;
return this.name&&!n(this).is(":disabled")&&Fb.test(this.nodeName)&&!Eb.test(a)&&(this.checked||!X.test(a))
}).map(function(a,b){var c=n(this).val();
return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(Db,"\r\n")}
}):{name:b.name,value:c.replace(Db,"\r\n")}
}).get()
}}),n.ajaxSettings.xhr=function(){try{return new a.XMLHttpRequest
}catch(b){}};
var Hb={0:200,1223:204},Ib=n.ajaxSettings.xhr();
l.cors=!!Ib&&"withCredentials" in Ib,l.ajax=Ib=!!Ib,n.ajaxTransport(function(b){var c,d;
return l.cors||Ib&&!b.crossDomain?{send:function(e,f){var g,h=b.xhr();
if(h.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields){for(g in b.xhrFields){h[g]=b.xhrFields[g]
}}b.mimeType&&h.overrideMimeType&&h.overrideMimeType(b.mimeType),b.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest");
for(g in e){h.setRequestHeader(g,e[g])
}c=function(a){return function(){c&&(c=d=h.onload=h.onerror=h.onabort=h.onreadystatechange=null,"abort"===a?h.abort():"error"===a?"number"!=typeof h.status?f(0,"error"):f(h.status,h.statusText):f(Hb[h.status]||h.status,h.statusText,"text"!==(h.responseType||"text")||"string"!=typeof h.responseText?{binary:h.response}:{text:h.responseText},h.getAllResponseHeaders()))
}
},h.onload=c(),d=h.onerror=c("error"),void 0!==h.onabort?h.onabort=d:h.onreadystatechange=function(){4===h.readyState&&a.setTimeout(function(){c&&d()
})
},c=c("abort");
try{h.send(b.hasContent&&b.data||null)
}catch(i){if(c){throw i
}}},abort:function(){c&&c()
}}:void 0
}),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a
}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")
}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;
return{send:function(e,f){b=n("<script>").prop({charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&f("error"===a.type?404:200,a.type)
}),d.head.appendChild(b[0])
},abort:function(){c&&c()
}}
}});
var Jb=[],Kb=/(=)\?(?=&|$)|\?\?/;
n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Jb.pop()||n.expando+"_"+kb++;
return this[a]=!0,a
}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Kb.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Kb.test(b.data)&&"data");
return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Kb,"$1"+e):b.jsonp!==!1&&(b.url+=(lb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]
},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments
},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Jb.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0
}),"script"):void 0
}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a){return null
}"boolean"==typeof b&&(c=b,b=!1),b=b||d;
var e=x.exec(a),f=!c&&[];
return e?[b.createElement(e[1])]:(e=ca([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))
};
var Lb=n.fn.load;
n.fn.load=function(a,b,c){if("string"!=typeof a&&Lb){return Lb.apply(this,arguments)
}var d,e,f,g=this,h=a.indexOf(" ");
return h>-1&&(d=n.trim(a.slice(h)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)
}).always(c&&function(a,b){g.each(function(){c.apply(g,f||[a.responseText,b,a])
})
}),this
},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)
}
}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem
}).length
};
function Mb(a){return n.isWindow(a)?a:9===a.nodeType&&a.defaultView
}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};
"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using" in b?b.using.call(a,m):l.css(m)
}},n.fn.extend({offset:function(a){if(arguments.length){return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)
})
}var b,c,d=this[0],e={top:0,left:0},f=d&&d.ownerDocument;
if(f){return b=f.documentElement,n.contains(b,d)?(e=d.getBoundingClientRect(),c=Mb(f),{top:e.top+c.pageYOffset-b.clientTop,left:e.left+c.pageXOffset-b.clientLeft}):e
}},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};
return"fixed"===n.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(d=a.offset()),d.top+=n.css(a[0],"borderTopWidth",!0),d.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-d.top-n.css(c,"marginTop",!0),left:b.left-d.left-n.css(c,"marginLeft",!0)}
}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;
while(a&&"static"===n.css(a,"position")){a=a.offsetParent
}return a||Ea
})
}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c="pageYOffset"===b;
n.fn[a]=function(d){return K(this,function(a,d,e){var f=Mb(a);
return void 0===e?f?f[b]:a[d]:void (f?f.scrollTo(c?f.pageXOffset:e,c?e:f.pageYOffset):a[d]=e)
},a,d,arguments.length)
}
}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ga(l.pixelPosition,function(a,c){return c?(c=Fa(a,b),Ba.test(c)?n(a).position()[b]+"px":c):void 0
})
}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");
return K(this,function(b,c,d){var e;
return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)
},b,f?d:void 0,f,null)
}
})
}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)
},unbind:function(a,b){return this.off(a,null,b)
},delegate:function(a,b,c,d){return this.on(b,a,c,d)
},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)
},size:function(){return this.length
}}),n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n
});
var Nb=a.jQuery,Ob=a.$;
return n.noConflict=function(b){return a.$===n&&(a.$=Ob),b&&a.jQuery===n&&(a.jQuery=Nb),n
},b||(a.jQuery=a.$=n),n
});
!function(f){var b="6.3.1";
var g={version:b,_plugins:{},_uuids:[],rtl:function(){return f("html").attr("dir")==="rtl"
},plugin:function(m,h){var l=h||e(m);
var k=d(l);
this._plugins[k]=this[l]=m
},registerPlugin:function(k,h){var l=h?d(h):e(k.constructor).toLowerCase();
k.uuid=this.GetYoDigits(6,l);
if(!k.$element.attr("data-"+l)){k.$element.attr("data-"+l,k.uuid)
}if(!k.$element.data("zfPlugin")){k.$element.data("zfPlugin",k)
}k.$element.trigger("init.zf."+l);
this._uuids.push(k.uuid);
return
},unregisterPlugin:function(h){var k=d(e(h.$element.data("zfPlugin").constructor));
this._uuids.splice(this._uuids.indexOf(h.uuid),1);
h.$element.removeAttr("data-"+k).removeData("zfPlugin").trigger("destroyed.zf."+k);
for(var l in h){h[l]=null
}return
},reInit:function(h){var n=h instanceof f;
try{if(n){h.each(function(){f(this).data("zfPlugin")._init()
})
}else{var l=typeof h,o=this,k={object:function(p){p.forEach(function(q){q=d(q);
f("[data-"+q+"]").foundation("_init")
})
},string:function(){h=d(h);
f("[data-"+h+"]").foundation("_init")
},"undefined":function(){this["object"](Object.keys(o._plugins))
}};
k[l](h)
}}catch(m){console.error(m)
}finally{return h
}},GetYoDigits:function(k,h){k=k||6;
return Math.round(Math.pow(36,k+1)-Math.random()*Math.pow(36,k)).toString(36).slice(1)+(h?"-"+h:"")
},reflow:function(k,h){if(typeof h==="undefined"){h=Object.keys(this._plugins)
}else{if(typeof h==="string"){h=[h]
}}var l=this;
f.each(h,function(o,n){var p=l._plugins[n];
var m=f(k).find("[data-"+n+"]").addBack("[data-"+n+"]");
m.each(function(){var r=f(this),s={};
if(r.data("zfPlugin")){console.warn("Tried to initialize "+n+" on an element that already has a Foundation plugin.");
return
}if(r.attr("data-options")){var q=r.attr("data-options").split(";").forEach(function(w,v){var u=w.split(":").map(function(x){return x.trim()
});
if(u[0]){s[u[0]]=a(u[1])
}})
}try{r.data("zfPlugin",new p(f(this),s))
}catch(t){console.error(t)
}finally{return
}})
})
},getFnName:e,transitionend:function(k){var n={transition:"transitionend",WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"otransitionend"};
var m=document.createElement("div"),h;
for(var l in n){if(typeof m.style[l]!=="undefined"){h=n[l]
}}if(h){return h
}else{h=setTimeout(function(){k.triggerHandler("transitionend",[k])
},1);
return"transitionend"
}}};
g.util={throttle:function(k,h){var l=null;
return function(){var n=this,m=arguments;
if(l===null){l=setTimeout(function(){k.apply(n,m);
l=null
},h)
}}
}};
var c=function(o){var n=typeof o,l=f("meta.foundation-mq"),h=f(".no-js");
if(!l.length){f('<meta class="foundation-mq">').appendTo(document.head)
}if(h.length){h.removeClass("no-js")
}if(n==="undefined"){g.MediaQuery._init();
g.reflow(this)
}else{if(n==="string"){var k=Array.prototype.slice.call(arguments,1);
var m=this.data("zfPlugin");
if(m!==undefined&&m[o]!==undefined){if(this.length===1){m[o].apply(m,k)
}else{this.each(function(p,q){m[o].apply(f(q).data("zfPlugin"),k)
})
}}else{throw new ReferenceError("We're sorry, '"+o+"' is not an available method for "+(m?e(m):"this element")+".")
}}else{throw new TypeError("We're sorry, "+n+" is not a valid parameter. You must use a string representing the method you wish to invoke.")
}}return this
};
window.Foundation=g;
f.fn.foundation=c;
(function(){if(!Date.now||!window.Date.now){window.Date.now=Date.now=function(){return new Date().getTime()
}
}var m=["webkit","moz"];
for(var k=0;
k<m.length&&!window.requestAnimationFrame;
++k){var h=m[k];
window.requestAnimationFrame=window[h+"RequestAnimationFrame"];
window.cancelAnimationFrame=window[h+"CancelAnimationFrame"]||window[h+"CancelRequestAnimationFrame"]
}if(/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent)||!window.requestAnimationFrame||!window.cancelAnimationFrame){var l=0;
window.requestAnimationFrame=function(p){var o=Date.now();
var n=Math.max(l+16,o);
return setTimeout(function(){p(l=n)
},n-o)
};
window.cancelAnimationFrame=clearTimeout
}if(!window.performance||!window.performance.now){window.performance={start:Date.now(),now:function(){return Date.now()-this.start
}}
}})();
if(!Function.prototype.bind){Function.prototype.bind=function(h){if(typeof this!=="function"){throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable")
}var n=Array.prototype.slice.call(arguments,1),m=this,k=function(){},l=function(){return m.apply(this instanceof k?this:h,n.concat(Array.prototype.slice.call(arguments)))
};
if(this.prototype){k.prototype=this.prototype
}l.prototype=new k();
return l
}
}function e(k){if(Function.prototype.name===undefined){var l=/function\s([^(]{1,})\(/;
var h=l.exec(k.toString());
return h&&h.length>1?h[1].trim():""
}else{if(k.prototype===undefined){return k.constructor.name
}else{return k.prototype.constructor.name
}}}function a(h){if("true"===h){return true
}else{if("false"===h){return false
}else{if(!isNaN(h*1)){return parseFloat(h)
}}}return h
}function d(h){return h.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase()
}}(jQuery);
"use strict";
!function(c){Foundation.Box={ImNotTouchingYou:d,GetDimensions:b,GetOffsets:a};
function d(h,p,l,k){var g=b(h),n,e,f,q;
if(p){var m=b(p);
e=g.offset.top+g.height<=m.height+m.offset.top;
n=g.offset.top>=m.offset.top;
f=g.offset.left>=m.offset.left;
q=g.offset.left+g.width<=m.width+m.offset.left
}else{e=g.offset.top+g.height<=g.windowDims.height+g.windowDims.offset.top;
n=g.offset.top>=g.windowDims.offset.top;
f=g.offset.left>=g.windowDims.offset.left;
q=g.offset.left+g.width<=g.windowDims.width
}var o=[e,n,f,q];
if(l){return f===q===true
}if(k){return n===e===true
}return o.indexOf(false)===-1
}function b(l,m){l=l.length?l[0]:l;
if(l===window||l===document){throw new Error("I'm sorry, Dave. I'm afraid I can't do that.")
}var k=l.getBoundingClientRect(),h=l.parentNode.getBoundingClientRect(),g=document.body.getBoundingClientRect(),e=window.pageYOffset,f=window.pageXOffset;
return{width:k.width,height:k.height,offset:{top:k.top+e,left:k.left+f},parentDims:{width:h.width,height:h.height,offset:{top:h.top+e,left:h.left+f}},windowDims:{width:g.width,height:g.height,offset:{top:e,left:f}}}
}function a(g,f,e,n,k,l){var m=b(g),h=f?b(f):null;
switch(e){case"top":return{left:Foundation.rtl()?h.offset.left-m.width+h.width:h.offset.left,top:h.offset.top-(m.height+n)};
break;
case"left":return{left:h.offset.left-(m.width+k),top:h.offset.top};
break;
case"right":return{left:h.offset.left+h.width+k,top:h.offset.top};
break;
case"center top":return{left:h.offset.left+h.width/2-m.width/2,top:h.offset.top-(m.height+n)};
break;
case"center bottom":return{left:l?k:h.offset.left+h.width/2-m.width/2,top:h.offset.top+h.height+n};
break;
case"center left":return{left:h.offset.left-(m.width+k),top:h.offset.top+h.height/2-m.height/2};
break;
case"center right":return{left:h.offset.left+h.width+k+1,top:h.offset.top+h.height/2-m.height/2};
break;
case"center":return{left:m.windowDims.offset.left+m.windowDims.width/2-m.width/2,top:m.windowDims.offset.top+m.windowDims.height/2-m.height/2};
break;
case"reveal":return{left:(m.windowDims.width-m.width)/2,top:m.windowDims.offset.top+n};
case"reveal full":return{left:m.windowDims.offset.left,top:m.windowDims.offset.top};
break;
case"left bottom":return{left:h.offset.left,top:h.offset.top+h.height+n};
break;
case"right bottom":return{left:h.offset.left+h.width+k-m.width,top:h.offset.top+h.height+n};
break;
default:return{left:Foundation.rtl()?h.offset.left-m.width+h.width:h.offset.left+k,top:h.offset.top+h.height+n}
}}}(jQuery);
"use strict";
!function(d){var c={9:"TAB",13:"ENTER",27:"ESCAPE",32:"SPACE",37:"ARROW_LEFT",38:"ARROW_UP",39:"ARROW_RIGHT",40:"ARROW_DOWN"};
var a={};
var b={keys:e(c),parseKey:function(g){var f=c[g.which||g.keyCode]||String.fromCharCode(g.which).toUpperCase();
f=f.replace(/\W+/,"");
if(g.shiftKey){f="SHIFT_"+f
}if(g.ctrlKey){f="CTRL_"+f
}if(g.altKey){f="ALT_"+f
}f=f.replace(/_$/,"");
return f
},handleKey:function(g,n,l){var o=a[n],p=this.parseKey(g),h,k,m;
if(!o){return console.warn("Component not defined!")
}if(typeof o.ltr==="undefined"){h=o
}else{if(Foundation.rtl()){h=d.extend({},o.ltr,o.rtl)
}else{h=d.extend({},o.rtl,o.ltr)
}}k=h[p];
m=l[k];
if(m&&typeof m==="function"){var f=m.apply();
if(l.handled||typeof l.handled==="function"){l.handled(f)
}}else{if(l.unhandled||typeof l.unhandled==="function"){l.unhandled()
}}},findFocusable:function(f){if(!f){return false
}return f.find("a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]").filter(function(){if(!d(this).is(":visible")||d(this).attr("tabindex")<0){return false
}return true
})
},register:function(g,f){a[g]=f
},trapFocus:function(h){var k=Foundation.Keyboard.findFocusable(h),f=k.eq(0),g=k.eq(-1);
h.on("keydown.zf.trapfocus",function(l){if(l.target===g[0]&&Foundation.Keyboard.parseKey(l)==="TAB"){l.preventDefault();
f.focus()
}else{if(l.target===f[0]&&Foundation.Keyboard.parseKey(l)==="SHIFT_TAB"){l.preventDefault();
g.focus()
}}})
},releaseFocus:function(f){f.off("keydown.zf.trapfocus")
}};
function e(h){var f={};
for(var g in h){f[h[g]]=h[g]
}return f
}Foundation.Keyboard=b
}(jQuery);
"use strict";
!function(d){var a={"default":"only screen",landscape:"only screen and (orientation: landscape)",portrait:"only screen and (orientation: portrait)",retina:"only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 2/1),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx)"};
var b={queries:[],current:"",_init:function(){var e=this;
var g=d(".foundation-mq").css("font-family");
var h;
h=c(g);
for(var f in h){if(h.hasOwnProperty(f)){e.queries.push({name:f,value:"only screen and (min-width: "+h[f]+")"})
}}this.current=this._getCurrentSize();
this._watcher()
},atLeast:function(e){var f=this.get(e);
if(f){return window.matchMedia(f).matches
}return false
},is:function(e){e=e.trim().split(" ");
if(e.length>1&&e[1]==="only"){if(e[0]===this._getCurrentSize()){return true
}}else{return this.atLeast(e[0])
}return false
},get:function(f){for(var e in this.queries){if(this.queries.hasOwnProperty(e)){var g=this.queries[e];
if(f===g.name){return g.value
}}}return null
},_getCurrentSize:function(){var e;
for(var f=0;
f<this.queries.length;
f++){var g=this.queries[f];
if(window.matchMedia(g.value).matches){e=g
}}if(typeof e==="object"){return e.name
}else{return e
}},_watcher:function(){var e=this;
d(window).on("resize.zf.mediaquery",function(){var g=e._getCurrentSize(),f=e.current;
if(g!==f){e.current=g;
d(window).trigger("changed.zf.mediaquery",[g,f])
}})
}};
Foundation.MediaQuery=b;
window.matchMedia||(window.matchMedia=function(){var f=window.styleMedia||window.media;
if(!f){var g=document.createElement("style"),e=document.getElementsByTagName("script")[0],h=null;
g.type="text/css";
g.id="matchmediajs-test";
e&&e.parentNode&&e.parentNode.insertBefore(g,e);
h="getComputedStyle" in window&&window.getComputedStyle(g,null)||g.currentStyle;
f={matchMedium:function(k){var l="@media "+k+"{ #matchmediajs-test { width: 1px; } }";
if(g.styleSheet){g.styleSheet.cssText=l
}else{g.textContent=l
}return h.width==="1px"
}}
}return function(k){return{matches:f.matchMedium(k||"all"),media:k||"all"}
}
}());
function c(f){var e={};
if(typeof f!=="string"){return e
}f=f.trim().slice(1,-1);
if(!f){return e
}e=f.split("&").reduce(function(g,m){var k=m.replace(/\+/g," ").split("=");
var h=k[0];
var l=k[1];
h=decodeURIComponent(h);
l=l===undefined?null:decodeURIComponent(l);
if(!g.hasOwnProperty(h)){g[h]=l
}else{if(Array.isArray(g[h])){g[h].push(l)
}else{g[h]=[g[h],l]
}}return g
},{});
return e
}Foundation.MediaQuery=b
}(jQuery);
"use strict";
!function(e){var d=["mui-enter","mui-leave"];
var c=["mui-enter-active","mui-leave-active"];
var a={animateIn:function(h,k,g){b(true,h,k,g)
},animateOut:function(h,k,g){b(false,h,k,g)
}};
function f(n,k,h){var l,m,o=null;
if(n===0){h.apply(k);
k.trigger("finished.zf.animate",[k]).triggerHandler("finished.zf.animate",[k]);
return
}function g(p){if(!o){o=p
}m=p-o;
h.apply(k);
if(m<n){l=window.requestAnimationFrame(g,k)
}else{window.cancelAnimationFrame(l);
k.trigger("finished.zf.animate",[k]).triggerHandler("finished.zf.animate",[k])
}}l=window.requestAnimationFrame(g)
}function b(p,l,o,g){l=e(l).eq(0);
if(!l.length){return
}var m=p?d[0]:d[1];
var k=p?c[0]:c[1];
n();
l.addClass(o).css("transition","none");
requestAnimationFrame(function(){l.addClass(m);
if(p){l.show()
}});
requestAnimationFrame(function(){l[0].offsetWidth;
l.css("transition","").addClass(k)
});
l.one(Foundation.transitionend(l),h);
function h(){if(!p){l.hide()
}n();
if(g){g.apply(l)
}}function n(){l[0].style.transitionDuration=0;
l.removeClass(m+" "+k+" "+o)
}}Foundation.Move=f;
Foundation.Motion=a
}(jQuery);
"use strict";
!function(b){var a={Feather:function(h){var e=arguments.length>1&&arguments[1]!==undefined?arguments[1]:"zf";
h.attr("role","menubar");
var d=h.find("li").attr({role:"menuitem"}),g="is-"+e+"-submenu",f=g+"-item",c="is-"+e+"-submenu-parent";
d.each(function(){var k=b(this),l=k.children("ul");
if(l.length){k.addClass(c).attr({"aria-haspopup":true,"aria-label":k.children("a:first").text()});
if(e==="drilldown"){k.attr({"aria-expanded":false})
}l.addClass("submenu "+g).attr({"data-submenu":"",role:"menu"});
if(e==="drilldown"){l.attr({"aria-hidden":true})
}}if(k.parent("[data-submenu]").length){k.addClass("is-submenu-item "+f)
}});
return
},Burn:function(g,d){var f="is-"+d+"-submenu",e=f+"-item",c="is-"+d+"-submenu-parent";
g.find(">li, .menu, .menu > li").removeClass(f+" "+e+" "+c+" is-submenu-item submenu is-active").removeAttr("data-submenu").css("display","")
}};
Foundation.Nest=a
}(jQuery);
"use strict";
!function(b){function a(g,n,h){var m=this,k=n.duration,d=Object.keys(g.data())[0]||"timer",l=-1,f,e;
this.isPaused=false;
this.restart=function(){l=-1;
clearTimeout(e);
this.start()
};
this.start=function(){this.isPaused=false;
clearTimeout(e);
l=l<=0?k:l;
g.data("paused",false);
f=Date.now();
e=setTimeout(function(){if(n.infinite){m.restart()
}if(h&&typeof h==="function"){h()
}},l);
g.trigger("timerstart.zf."+d)
};
this.pause=function(){this.isPaused=true;
clearTimeout(e);
g.data("paused",true);
var o=Date.now();
l=l-(o-f);
g.trigger("timerpaused.zf."+d)
}
}function c(d,h){var e=this,f=d.length;
if(f===0){h()
}d.each(function(){if(this.complete||this.readyState===4||this.readyState==="complete"){g()
}else{var k=b(this).attr("src");
b(this).attr("src",k+(k.indexOf("?")>=0?"&":"?")+new Date().getTime());
b(this).one("load",function(){g()
})
}});
function g(){f--;
if(f===0){h()
}}}Foundation.Timer=a;
Foundation.onImagesLoaded=c
}(jQuery);
(function(d){d.spotSwipe={version:"1.0.0",enabled:"ontouchstart" in document.documentElement,preventDefault:false,moveThreshold:75,timeThreshold:200};
var l,k,b,m,e=false;
function g(){this.removeEventListener("touchmove",a);
this.removeEventListener("touchend",g);
e=false
}function a(r){if(d.spotSwipe.preventDefault){r.preventDefault()
}if(e){var n=r.touches[0].pageX;
var s=r.touches[0].pageY;
var p=l-n;
var o=k-s;
var q;
m=new Date().getTime()-b;
if(Math.abs(p)>=d.spotSwipe.moveThreshold&&m<=d.spotSwipe.timeThreshold){q=p>0?"left":"right"
}if(q){r.preventDefault();
g.call(this);
d(this).trigger("swipe",q).trigger("swipe"+q)
}}}function c(n){if(n.touches.length==1){l=n.touches[0].pageX;
k=n.touches[0].pageY;
e=true;
b=new Date().getTime();
this.addEventListener("touchmove",a,false);
this.addEventListener("touchend",g,false)
}}function h(){this.addEventListener&&this.addEventListener("touchstart",c,false)
}function f(){this.removeEventListener("touchstart",c)
}d.event.special.swipe={setup:h};
d.each(["left","up","down","right"],function(){d.event.special["swipe"+this]={setup:function(){d(this).on("swipe",d.noop)
}}
})
})(jQuery);
!function(a){a.fn.addTouch=function(){this.each(function(c,d){a(d).bind("touchstart touchmove touchend touchcancel",function(){b(event)
})
});
var b=function(f){var g=f.changedTouches,h=g[0],e={touchstart:"mousedown",touchmove:"mousemove",touchend:"mouseup"},c=e[f.type],d;
if("MouseEvent" in window&&typeof window.MouseEvent==="function"){d=new window.MouseEvent(c,{bubbles:true,cancelable:true,screenX:h.screenX,screenY:h.screenY,clientX:h.clientX,clientY:h.clientY})
}else{d=document.createEvent("MouseEvent");
d.initMouseEvent(c,true,true,window,1,h.screenX,h.screenY,h.clientX,h.clientY,false,false,false,false,0,null)
}h.target.dispatchEvent(d)
}
}
}(jQuery);
"use strict";
!function(e){var d=function(){var m=["WebKit","Moz","O","Ms",""];
for(var l=0;
l<m.length;
l++){if(m[l]+"MutationObserver" in window){return window[m[l]+"MutationObserver"]
}}return false
}();
var f=function(m,l){m.data(l).split(" ").forEach(function(n){e("#"+n)[l==="close"?"trigger":"triggerHandler"](l+".zf.trigger",[m])
})
};
e(document).on("click.zf.trigger","[data-open]",function(){f(e(this),"open")
});
e(document).on("click.zf.trigger","[data-close]",function(){var l=e(this).data("close");
if(l){f(e(this),"close")
}else{e(this).trigger("close.zf.trigger")
}});
e(document).on("click.zf.trigger","[data-toggle]",function(){var l=e(this).data("toggle");
if(l){f(e(this),"toggle")
}else{e(this).trigger("toggle.zf.trigger")
}});
e(document).on("close.zf.trigger","[data-closable]",function(m){m.stopPropagation();
var l=e(this).data("closable");
if(l!==""){Foundation.Motion.animateOut(e(this),l,function(){e(this).trigger("closed.zf")
})
}else{e(this).fadeOut().trigger("closed.zf")
}});
e(document).on("focus.zf.trigger blur.zf.trigger","[data-toggle-focus]",function(){var l=e(this).data("toggle-focus");
e("#"+l).triggerHandler("toggle.zf.trigger",[e(this)])
});
e(window).on("load",function(){g()
});
function g(){b();
k();
h();
c();
a()
}function a(o){var l=e("[data-yeti-box]"),n=["dropdown","tooltip","reveal"];
if(o){if(typeof o==="string"){n.push(o)
}else{if(typeof o==="object"&&typeof o[0]==="string"){n.concat(o)
}else{console.error("Plugin names must be strings")
}}}if(l.length){var m=n.map(function(p){return"closeme.zf."+p
}).join(" ");
e(window).off(m).on(m,function(s,q){var r=s.namespace.split(".")[0];
var p=e("[data-"+r+"]").not('[data-yeti-box="'+q+'"]');
p.each(function(){var t=e(this);
t.triggerHandler("close.zf.trigger",[t])
})
})
}}function k(l){var n=void 0,m=e("[data-resize]");
if(m.length){e(window).off("resize.zf.trigger").on("resize.zf.trigger",function(o){if(n){clearTimeout(n)
}n=setTimeout(function(){if(!d){m.each(function(){e(this).triggerHandler("resizeme.zf.trigger")
})
}m.attr("data-events","resize")
},l||10)
})
}}function h(l){var n=void 0,m=e("[data-scroll]");
if(m.length){e(window).off("scroll.zf.trigger").on("scroll.zf.trigger",function(o){if(n){clearTimeout(n)
}n=setTimeout(function(){if(!d){m.each(function(){e(this).triggerHandler("scrollme.zf.trigger")
})
}m.attr("data-events","scroll")
},l||10)
})
}}function c(l){var m=e("[data-mutate]");
if(m.length&&d){m.each(function(){e(this).triggerHandler("mutateme.zf.trigger")
})
}}function b(){if(!d){return false
}var l=document.querySelectorAll("[data-resize], [data-scroll], [data-mutate]");
var o=function(q){var p=e(q[0].target);
switch(q[0].type){case"attributes":if(p.attr("data-events")==="scroll"&&q[0].attributeName==="data-events"){p.triggerHandler("scrollme.zf.trigger",[p,window.pageYOffset])
}if(p.attr("data-events")==="resize"&&q[0].attributeName==="data-events"){p.triggerHandler("resizeme.zf.trigger",[p])
}if(q[0].attributeName==="style"){p.closest("[data-mutate]").attr("data-events","mutate");
p.closest("[data-mutate]").triggerHandler("mutateme.zf.trigger",[p.closest("[data-mutate]")])
}break;
case"childList":p.closest("[data-mutate]").attr("data-events","mutate");
p.closest("[data-mutate]").triggerHandler("mutateme.zf.trigger",[p.closest("[data-mutate]")]);
break;
default:return false
}};
if(l.length){for(var m=0;
m<=l.length-1;
m++){var n=new d(o);
n.observe(l[m],{attributes:true,childList:true,characterData:false,subtree:true,attributeFilter:["data-events","style"]})
}}}Foundation.IHearYou=g
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function q(x){var w=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{};
_classCallCheck(this,q);
this.$element=x;
this.options=b.extend({},q.defaults,this.$element.data(),w);
this._init();
Foundation.registerPlugin(this,"Abide")
}_createClass(q,[{key:"_init",value:function l(){this.$inputs=this.$element.find("input, textarea, select");
this._events()
}},{key:"_events",value:function m(){var w=this;
this.$element.off(".abide").on("reset.zf.abide",function(){w.resetForm()
}).on("submit.zf.abide",function(){return w.validateForm()
});
if(this.options.validateOn==="fieldChange"){this.$inputs.off("change.zf.abide").on("change.zf.abide",function(x){w.validateInput(b(x.target))
})
}if(this.options.liveValidate){this.$inputs.off("input.zf.abide").on("input.zf.abide",function(x){w.validateInput(b(x.target))
})
}if(this.options.validateOnBlur){this.$inputs.off("blur.zf.abide").on("blur.zf.abide",function(x){w.validateInput(b(x.target))
})
}}},{key:"_reflow",value:function t(){this._init()
}},{key:"requiredCheck",value:function k(x){if(!x.attr("required")){return true
}var y=true;
switch(x[0].type){case"checkbox":y=x[0].checked;
break;
case"select":case"select-one":case"select-multiple":var w=x.find("option:selected");
if(!w.length||!w.val()){y=false
}break;
default:if(!x.val()||!x.val().length){y=false
}}return y
}},{key:"findFormError",value:function s(x){var w=x.siblings(this.options.formErrorSelector);
if(!w.length){w=x.parent().find(this.options.formErrorSelector)
}return w
}},{key:"findLabel",value:function g(x){var y=x[0].id;
var w=this.$element.find('label[for="'+y+'"]');
if(!w.length){return x.closest("label")
}return w
}},{key:"findRadioLabels",value:function r(x){var w=this;
var y=x.map(function(A,B){var C=B.id;
var z=w.$element.find('label[for="'+C+'"]');
if(!z.length){z=b(B).closest("label")
}return z[0]
});
return b(y)
}},{key:"addErrorClasses",value:function h(x){var w=this.findLabel(x);
var y=this.findFormError(x);
if(w.length){w.addClass(this.options.labelErrorClass)
}if(y.length){y.addClass(this.options.formErrorClass)
}x.addClass(this.options.inputErrorClass).attr("data-invalid","")
}},{key:"removeRadioErrorClasses",value:function e(z){var y=this.$element.find(':radio[name="'+z+'"]');
var w=this.findRadioLabels(y);
var x=this.findFormError(y);
if(w.length){w.removeClass(this.options.labelErrorClass)
}if(x.length){x.removeClass(this.options.formErrorClass)
}y.removeClass(this.options.inputErrorClass).removeAttr("data-invalid")
}},{key:"removeErrorClasses",value:function v(x){if(x[0].type=="radio"){return this.removeRadioErrorClasses(x.attr("name"))
}var w=this.findLabel(x);
var y=this.findFormError(x);
if(w.length){w.removeClass(this.options.labelErrorClass)
}if(y.length){y.removeClass(this.options.formErrorClass)
}x.removeClass(this.options.inputErrorClass).removeAttr("data-invalid")
}},{key:"validateInput",value:function o(F){var B=this;
var z=this.requiredCheck(F),x=false,y=true,w=F.attr("data-validator"),A=true;
if(F.is("[data-abide-ignore]")||F.is('[type="hidden"]')||F.is("[disabled]")){return true
}switch(F[0].type){case"radio":x=this.validateRadio(F.attr("name"));
break;
case"checkbox":x=z;
break;
case"select":case"select-one":case"select-multiple":x=z;
break;
default:x=this.validateText(F)
}if(w){y=this.matchValidation(F,w,F.attr("required"))
}if(F.attr("data-equalto")){A=this.options.validators.equalTo(F)
}var C=[z,x,y,A].indexOf(false)===-1;
var E=(C?"valid":"invalid")+".zf.abide";
if(C){var D=this.$element.find('[data-equalto="'+F.attr("id")+'"]');
if(D.length){(function(){var G=B;
D.each(function(){if(b(this).val()){G.validateInput(b(this))
}})
})()
}}this[C?"removeErrorClasses":"addErrorClasses"](F);
F.trigger(E,[F]);
return C
}},{key:"validateForm",value:function f(){var x=[];
var y=this;
this.$inputs.each(function(){x.push(y.validateInput(b(this)))
});
var w=x.indexOf(false)===-1;
this.$element.find("[data-abide-error]").css("display",w?"none":"block");
this.$element.trigger((w?"formvalid":"forminvalid")+".zf.abide",[this.$element]);
return w
}},{key:"validateText",value:function p(w,y){y=y||w.attr("pattern")||w.attr("type");
var z=w.val();
var x=false;
if(z.length){if(this.options.patterns.hasOwnProperty(y)){x=this.options.patterns[y].test(z)
}else{if(y!==w.attr("type")){x=new RegExp(y).test(z)
}else{x=true
}}}else{if(!w.prop("required")){x=true
}}return x
}},{key:"validateRadio",value:function n(z){var w=this.$element.find(':radio[name="'+z+'"]');
var x=false,y=false;
w.each(function(A,B){if(b(B).attr("required")){y=true
}});
if(!y){x=true
}if(!x){w.each(function(A,B){if(b(B).prop("checked")){x=true
}})
}return x
}},{key:"matchValidation",value:function c(z,x,A){var y=this;
A=A?true:false;
var w=x.split(" ").map(function(B){return y.options.validators[B](z,A,z.parent())
});
return w.indexOf(false)===-1
}},{key:"resetForm",value:function d(){var w=this.$element,x=this.options;
b("."+x.labelErrorClass,w).not("small").removeClass(x.labelErrorClass);
b("."+x.inputErrorClass,w).not("small").removeClass(x.inputErrorClass);
b(x.formErrorSelector+"."+x.formErrorClass).removeClass(x.formErrorClass);
w.find("[data-abide-error]").css("display","none");
b(":input",w).not(":button, :submit, :reset, :hidden, :radio, :checkbox, [data-abide-ignore]").val("").removeAttr("data-invalid");
b(":input:radio",w).not("[data-abide-ignore]").prop("checked",false).removeAttr("data-invalid");
b(":input:checkbox",w).not("[data-abide-ignore]").prop("checked",false).removeAttr("data-invalid");
w.trigger("formreset.zf.abide",[w])
}},{key:"destroy",value:function u(){var w=this;
this.$element.off(".abide").find("[data-abide-error]").css("display","none");
this.$inputs.off(".abide").each(function(){w.removeErrorClasses(b(this))
});
Foundation.unregisterPlugin(this)
}}]);
return q
}();
a.defaults={validateOn:"fieldChange",labelErrorClass:"is-invalid-label",inputErrorClass:"is-invalid-input",formErrorSelector:".form-error",formErrorClass:"is-visible",liveValidate:false,validateOnBlur:false,patterns:{alpha:/^[a-zA-Z]+$/,alpha_numeric:/^[a-zA-Z0-9]+$/,integer:/^[-+]?\d+$/,number:/^[-+]?\d*(?:[\.\,]\d+)?$/,card:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,cvv:/^([0-9]){3,4}$/,email:/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/,url:/^(https?|ftp|file|ssh):\/\/(((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/,domain:/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,8}$/,datetime:/^([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9])T([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])(Z|([\-\+]([0-1][0-9])\:00))$/,date:/(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))$/,time:/^(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}$/,dateISO:/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/,month_day_year:/^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.]\d{4}$/,day_month_year:/^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.]\d{4}$/,color:/^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/},validators:{equalTo:function(d,e,c){return b("#"+d.attr("data-equalto")).val()===d.val()
}}};
Foundation.plugin(a,"Abide")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function e(m,l){_classCallCheck(this,e);
this.$element=m;
this.options=b.extend({},e.defaults,this.$element.data(),l);
this._init();
Foundation.registerPlugin(this,"Accordion");
Foundation.Keyboard.register("Accordion",{ENTER:"toggle",SPACE:"toggle",ARROW_DOWN:"next",ARROW_UP:"previous"})
}_createClass(e,[{key:"_init",value:function g(){this.$element.attr("role","tablist");
this.$tabs=this.$element.children("[data-accordion-item]");
this.$tabs.each(function(m,p){var o=b(p),n=o.children("[data-tab-content]"),r=n[0].id||Foundation.GetYoDigits(6,"accordion"),q=p.id||r+"-label";
o.find("a:first").attr({"aria-controls":r,role:"tab",id:q,"aria-expanded":false,"aria-selected":false});
n.attr({role:"tabpanel","aria-labelledby":q,"aria-hidden":true,id:r})
});
var l=this.$element.find(".is-active").children("[data-tab-content]");
if(l.length){this.down(l,true)
}this._events()
}},{key:"_events",value:function h(){var l=this;
this.$tabs.each(function(){var m=b(this);
var n=m.children("[data-tab-content]");
if(n.length){m.children("a").off("click.zf.accordion keydown.zf.accordion").on("click.zf.accordion",function(o){o.preventDefault();
l.toggle(n)
}).on("keydown.zf.accordion",function(o){Foundation.Keyboard.handleKey(o,"Accordion",{toggle:function(){l.toggle(n)
},next:function(){var p=m.next().find("a").focus();
if(!l.options.multiExpand){p.trigger("click.zf.accordion")
}},previous:function(){var p=m.prev().find("a").focus();
if(!l.options.multiExpand){p.trigger("click.zf.accordion")
}},handled:function(){o.preventDefault();
o.stopPropagation()
}})
})
}})
}},{key:"toggle",value:function d(l){if(l.parent().hasClass("is-active")){this.up(l)
}else{this.down(l)
}}},{key:"down",value:function k(l,o){var n=this;
l.attr("aria-hidden",false).parent("[data-tab-content]").addBack().parent().addClass("is-active");
if(!this.options.multiExpand&&!o){var m=this.$element.children(".is-active").children("[data-tab-content]");
if(m.length){this.up(m.not(l))
}}l.slideDown(this.options.slideSpeed,function(){n.$element.trigger("down.zf.accordion",[l])
});
b("#"+l.attr("aria-labelledby")).attr({"aria-expanded":true,"aria-selected":true})
}},{key:"up",value:function c(l){var m=l.parent().siblings(),n=this;
if(!this.options.allowAllClosed&&!m.hasClass("is-active")||!l.parent().hasClass("is-active")){return
}l.slideUp(n.options.slideSpeed,function(){n.$element.trigger("up.zf.accordion",[l])
});
l.attr("aria-hidden",true).parent().removeClass("is-active");
b("#"+l.attr("aria-labelledby")).attr({"aria-expanded":false,"aria-selected":false})
}},{key:"destroy",value:function f(){this.$element.find("[data-tab-content]").stop(true).slideUp(0).css("display","");
this.$element.find("a").off(".zf.accordion");
Foundation.unregisterPlugin(this)
}}]);
return e
}();
a.defaults={slideSpeed:250,multiExpand:false,allowAllClosed:false};
Foundation.plugin(a,"Accordion")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function h(o,n){_classCallCheck(this,h);
this.$element=o;
this.options=b.extend({},h.defaults,this.$element.data(),n);
Foundation.Nest.Feather(this.$element,"accordion");
this._init();
Foundation.registerPlugin(this,"AccordionMenu");
Foundation.Keyboard.register("AccordionMenu",{ENTER:"toggle",SPACE:"toggle",ARROW_RIGHT:"open",ARROW_UP:"up",ARROW_DOWN:"down",ARROW_LEFT:"close",ESCAPE:"closeAll"})
}_createClass(h,[{key:"_init",value:function k(){this.$element.find("[data-submenu]").not(".is-active").slideUp(0);
this.$element.attr({role:"menu","aria-multiselectable":this.options.multiOpen});
this.$menuLinks=this.$element.find(".is-accordion-submenu-parent");
this.$menuLinks.each(function(){var t=this.id||Foundation.GetYoDigits(6,"acc-menu-link"),p=b(this),s=p.children("[data-submenu]"),q=s[0].id||Foundation.GetYoDigits(6,"acc-menu"),r=s.hasClass("is-active");
p.attr({"aria-controls":q,"aria-expanded":r,role:"menuitem",id:t});
s.attr({"aria-labelledby":t,"aria-hidden":!r,role:"menu",id:q})
});
var n=this.$element.find(".is-active");
if(n.length){var o=this;
n.each(function(){o.down(b(this))
})
}this._events()
}},{key:"_events",value:function d(){var n=this;
this.$element.find("li").each(function(){var o=b(this).children("[data-submenu]");
if(o.length){b(this).children("a").off("click.zf.accordionMenu").on("click.zf.accordionMenu",function(p){p.preventDefault();
n.toggle(o)
})
}}).on("keydown.zf.accordionmenu",function(t){var q=b(this),r=q.parent("ul").children("li"),s,p,o=q.children("[data-submenu]");
r.each(function(u){if(b(this).is(q)){s=r.eq(Math.max(0,u-1)).find("a").first();
p=r.eq(Math.min(u+1,r.length-1)).find("a").first();
if(b(this).children("[data-submenu]:visible").length){p=q.find("li:first-child").find("a").first()
}if(b(this).is(":first-child")){s=q.parents("li").first().find("a").first()
}else{if(s.parents("li").first().children("[data-submenu]:visible").length){s=s.parents("li").find("li:last-child").find("a").first()
}}if(b(this).is(":last-child")){p=q.parents("li").first().next("li").find("a").first()
}return
}});
Foundation.Keyboard.handleKey(t,"AccordionMenu",{open:function(){if(o.is(":hidden")){n.down(o);
o.find("li").first().find("a").first().focus()
}},close:function(){if(o.length&&!o.is(":hidden")){n.up(o)
}else{if(q.parent("[data-submenu]").length){n.up(q.parent("[data-submenu]"));
q.parents("li").first().find("a").first().focus()
}}},up:function(){s.focus();
return true
},down:function(){p.focus();
return true
},toggle:function(){if(q.children("[data-submenu]").length){n.toggle(q.children("[data-submenu]"))
}},closeAll:function(){n.hideAll()
},handled:function(u){if(u){t.preventDefault()
}t.stopImmediatePropagation()
}})
})
}},{key:"hideAll",value:function f(){this.up(this.$element.find("[data-submenu]"))
}},{key:"showAll",value:function c(){this.down(this.$element.find("[data-submenu]"))
}},{key:"toggle",value:function g(n){if(!n.is(":animated")){if(!n.is(":hidden")){this.up(n)
}else{this.down(n)
}}}},{key:"down",value:function m(n){var o=this;
if(!this.options.multiOpen){this.up(this.$element.find(".is-active").not(n.parentsUntil(this.$element).add(n)))
}n.addClass("is-active").attr({"aria-hidden":false}).parent(".is-accordion-submenu-parent").attr({"aria-expanded":true});
n.slideDown(o.options.slideSpeed,function(){o.$element.trigger("down.zf.accordionMenu",[n])
})
}},{key:"up",value:function e(n){var p=this;
n.slideUp(p.options.slideSpeed,function(){p.$element.trigger("up.zf.accordionMenu",[n])
});
var o=n.find("[data-submenu]").slideUp(0).addBack().attr("aria-hidden",true);
o.parent(".is-accordion-submenu-parent").attr("aria-expanded",false)
}},{key:"destroy",value:function l(){this.$element.find("[data-submenu]").slideDown(0).css("display","");
this.$element.find("a").off("click.zf.accordionMenu");
Foundation.Nest.Burn(this.$element,"accordion");
Foundation.unregisterPlugin(this)
}}]);
return h
}();
a.defaults={slideSpeed:250,multiOpen:true};
Foundation.plugin(a,"AccordionMenu")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function n(u,t){_classCallCheck(this,n);
this.$element=u;
this.options=b.extend({},n.defaults,this.$element.data(),t);
Foundation.Nest.Feather(this.$element,"drilldown");
this._init();
Foundation.registerPlugin(this,"Drilldown");
Foundation.Keyboard.register("Drilldown",{ENTER:"open",SPACE:"open",ARROW_RIGHT:"next",ARROW_UP:"up",ARROW_DOWN:"down",ARROW_LEFT:"previous",ESCAPE:"close",TAB:"down",SHIFT_TAB:"up"})
}_createClass(n,[{key:"_init",value:function q(){this.$submenuAnchors=this.$element.find("li.is-drilldown-submenu-parent").children("a");
this.$submenus=this.$submenuAnchors.parent("li").children("[data-submenu]");
this.$menuItems=this.$element.find("li").not(".js-drilldown-back").attr("role","menuitem").find("a");
this.$element.attr("data-mutate",this.$element.attr("data-drilldown")||Foundation.GetYoDigits(6,"drilldown"));
this._prepareMenu();
this._registerEvents();
this._keyboardEvents()
}},{key:"_prepareMenu",value:function p(){var t=this;
this.$submenuAnchors.each(function(){var u=b(this);
var v=u.parent();
if(t.options.parentLink){u.clone().prependTo(v.children("[data-submenu]")).wrap('<li class="is-submenu-parent-item is-submenu-item is-drilldown-submenu-item" role="menu-item"></li>')
}u.data("savedHref",u.attr("href")).removeAttr("href").attr("tabindex",0);
u.children("[data-submenu]").attr({"aria-hidden":true,tabindex:0,role:"menu"});
t._events(u)
});
this.$submenus.each(function(){var u=b(this),v=u.find(".js-drilldown-back");
if(!v.length){switch(t.options.backButtonPosition){case"bottom":u.append(t.options.backButton);
break;
case"top":u.prepend(t.options.backButton);
break;
default:console.error("Unsupported backButtonPosition value '"+t.options.backButtonPosition+"'")
}}t._back(u)
});
this.$submenus.addClass("invisible");
if(!this.options.autoHeight){this.$submenus.addClass("drilldown-submenu-cover-previous")
}if(!this.$element.parent().hasClass("is-drilldown")){this.$wrapper=b(this.options.wrapper).addClass("is-drilldown");
if(this.options.animateHeight){this.$wrapper.addClass("animate-height")
}this.$element.wrap(this.$wrapper)
}this.$wrapper=this.$element.parent();
this.$wrapper.css(this._getMaxDims())
}},{key:"_resize",value:function o(){this.$wrapper.css({"max-width":"none","min-height":"none"});
this.$wrapper.css(this._getMaxDims())
}},{key:"_events",value:function h(t){var u=this;
t.off("click.zf.drilldown").on("click.zf.drilldown",function(w){if(b(w.target).parentsUntil("ul","li").hasClass("is-drilldown-submenu-parent")){w.stopImmediatePropagation();
w.preventDefault()
}u._show(t.parent("li"));
if(u.options.closeOnClick){var v=b("body");
v.off(".zf.drilldown").on("click.zf.drilldown",function(x){if(x.target===u.$element[0]||b.contains(u.$element[0],x.target)){return
}x.preventDefault();
u._hideAll();
v.off(".zf.drilldown")
})
}});
this.$element.on("mutateme.zf.trigger",this._resize.bind(this))
}},{key:"_registerEvents",value:function s(){if(this.options.scrollTop){this._bindHandler=this._scrollTop.bind(this);
this.$element.on("open.zf.drilldown hide.zf.drilldown closed.zf.drilldown",this._bindHandler)
}}},{key:"_scrollTop",value:function d(){var v=this;
var t=v.options.scrollTopElement!=""?b(v.options.scrollTopElement):v.$element,u=parseInt(t.offset().top+v.options.scrollTopOffset);
b("html, body").stop(true).animate({scrollTop:u},v.options.animationDuration,v.options.animationEasing,function(){if(this===b("html")[0]){v.$element.trigger("scrollme.zf.drilldown")
}})
}},{key:"_keyboardEvents",value:function l(){var t=this;
this.$menuItems.add(this.$element.find(".js-drilldown-back > a, .is-submenu-parent-item > a")).on("keydown.zf.drilldown",function(y){var v=b(this),w=v.parent("li").parent("ul").children("li").children("a"),x,u;
w.each(function(z){if(b(this).is(v)){x=w.eq(Math.max(0,z-1));
u=w.eq(Math.min(z+1,w.length-1));
return
}});
Foundation.Keyboard.handleKey(y,"Drilldown",{next:function(){if(v.is(t.$submenuAnchors)){t._show(v.parent("li"));
v.parent("li").one(Foundation.transitionend(v),function(){v.parent("li").find("ul li a").filter(t.$menuItems).first().focus()
});
return true
}},previous:function(){t._hide(v.parent("li").parent("ul"));
v.parent("li").parent("ul").one(Foundation.transitionend(v),function(){setTimeout(function(){v.parent("li").parent("ul").parent("li").children("a").first().focus()
},1)
});
return true
},up:function(){x.focus();
return !v.is(t.$element.find("> li:first-child > a"))
},down:function(){u.focus();
return !v.is(t.$element.find("> li:last-child > a"))
},close:function(){if(!v.is(t.$element.find("> li > a"))){t._hide(v.parent().parent());
v.parent().parent().siblings("a").focus()
}},open:function(){if(!v.is(t.$menuItems)){t._hide(v.parent("li").parent("ul"));
v.parent("li").parent("ul").one(Foundation.transitionend(v),function(){setTimeout(function(){v.parent("li").parent("ul").parent("li").children("a").first().focus()
},1)
});
return true
}else{if(v.is(t.$submenuAnchors)){t._show(v.parent("li"));
v.parent("li").one(Foundation.transitionend(v),function(){v.parent("li").find("ul li a").filter(t.$menuItems).first().focus()
});
return true
}}},handled:function(z){if(z){y.preventDefault()
}y.stopImmediatePropagation()
}})
})
}},{key:"_hideAll",value:function g(){var t=this.$element.find(".is-drilldown-submenu.is-active").addClass("is-closing");
if(this.options.autoHeight){this.$wrapper.css({height:t.parent().closest("ul").data("calcHeight")})
}t.one(Foundation.transitionend(t),function(u){t.removeClass("is-active is-closing")
});
this.$element.trigger("closed.zf.drilldown")
}},{key:"_back",value:function e(t){var u=this;
t.off("click.zf.drilldown");
t.children(".js-drilldown-back").on("click.zf.drilldown",function(v){v.stopImmediatePropagation();
u._hide(t);
var w=t.parent("li").parent("ul").parent("li");
if(w.length){u._show(w)
}})
}},{key:"_menuLinkEvents",value:function c(){var t=this;
this.$menuItems.not(".is-drilldown-submenu-parent").off("click.zf.drilldown").on("click.zf.drilldown",function(u){setTimeout(function(){t._hideAll()
},0)
})
}},{key:"_show",value:function k(t){if(this.options.autoHeight){this.$wrapper.css({height:t.children("[data-submenu]").data("calcHeight")})
}t.attr("aria-expanded",true);
t.children("[data-submenu]").addClass("is-active").removeClass("invisible").attr("aria-hidden",false);
this.$element.trigger("open.zf.drilldown",[t])
}},{key:"_hide",value:function f(t){if(this.options.autoHeight){this.$wrapper.css({height:t.parent().closest("ul").data("calcHeight")})
}var u=this;
t.parent("li").attr("aria-expanded",false);
t.attr("aria-hidden",true).addClass("is-closing");
t.addClass("is-closing").one(Foundation.transitionend(t),function(){t.removeClass("is-active is-closing");
t.blur().addClass("invisible")
});
t.trigger("hide.zf.drilldown",[t])
}},{key:"_getMaxDims",value:function m(){var u=0,t={},v=this;
this.$submenus.add(this.$element).each(function(){var x=b(this).children("li").length;
var w=Foundation.Box.GetDimensions(this).height;
u=w>u?w:u;
if(v.options.autoHeight){b(this).data("calcHeight",w);
if(!b(this).hasClass("is-drilldown-submenu")){t.height=w
}}});
if(!this.options.autoHeight){t["min-height"]=u+"px"
}t["max-width"]=this.$element[0].getBoundingClientRect().width+"px";
return t
}},{key:"destroy",value:function r(){if(this.options.scrollTop){this.$element.off(".zf.drilldown",this._bindHandler)
}this._hideAll();
this.$element.off("mutateme.zf.trigger");
Foundation.Nest.Burn(this.$element,"drilldown");
this.$element.unwrap().find(".js-drilldown-back, .is-submenu-parent-item").remove().end().find(".is-active, .is-closing, .is-drilldown-submenu").removeClass("is-active is-closing is-drilldown-submenu").end().find("[data-submenu]").removeAttr("aria-hidden tabindex role");
this.$submenuAnchors.each(function(){b(this).off(".zf.drilldown")
});
this.$submenus.removeClass("drilldown-submenu-cover-previous");
this.$element.find("a").each(function(){var t=b(this);
t.removeAttr("tabindex");
if(t.data("savedHref")){t.attr("href",t.data("savedHref")).removeData("savedHref")
}else{return
}});
Foundation.unregisterPlugin(this)
}}]);
return n
}();
a.defaults={backButton:'<li class="js-drilldown-back"><a tabindex="0">Back</a></li>',backButtonPosition:"top",wrapper:"<div></div>",parentLink:false,closeOnClick:false,autoHeight:false,animateHeight:false,scrollTop:false,scrollTopElement:"",scrollTopOffset:0,animationDuration:500,animationEasing:"swing"};
Foundation.plugin(a,"Drilldown")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function l(q,p){_classCallCheck(this,l);
this.$element=q;
this.options=b.extend({},l.defaults,this.$element.data(),p);
this._init();
Foundation.registerPlugin(this,"Dropdown");
Foundation.Keyboard.register("Dropdown",{ENTER:"open",SPACE:"open",ESCAPE:"close"})
}_createClass(l,[{key:"_init",value:function g(){var p=this.$element.attr("id");
this.$anchor=b('[data-toggle="'+p+'"]').length?b('[data-toggle="'+p+'"]'):b('[data-open="'+p+'"]');
this.$anchor.attr({"aria-controls":p,"data-is-focus":false,"data-yeti-box":p,"aria-haspopup":true,"aria-expanded":false});
if(this.options.parentClass){this.$parent=this.$element.parents("."+this.options.parentClass)
}else{this.$parent=null
}this.options.positionClass=this.getPositionClass();
this.counter=4;
this.usedPositions=[];
this.$element.attr({"aria-hidden":"true","data-yeti-box":p,"data-resize":p,"aria-labelledby":this.$anchor[0].id||Foundation.GetYoDigits(6,"dd-anchor")});
this._events()
}},{key:"getPositionClass",value:function k(){var q=this.$element[0].className.match(/(top|left|right|bottom)/g);
q=q?q[0]:"";
var r=/float-(\S+)/.exec(this.$anchor[0].className);
r=r?r[1]:"";
var p=r?r+" "+q:q;
return p
}},{key:"_reposition",value:function c(p){this.usedPositions.push(p?p:"bottom");
if(!p&&this.usedPositions.indexOf("top")<0){this.$element.addClass("top")
}else{if(p==="top"&&this.usedPositions.indexOf("bottom")<0){this.$element.removeClass(p)
}else{if(p==="left"&&this.usedPositions.indexOf("right")<0){this.$element.removeClass(p).addClass("right")
}else{if(p==="right"&&this.usedPositions.indexOf("left")<0){this.$element.removeClass(p).addClass("left")
}else{if(!p&&this.usedPositions.indexOf("top")>-1&&this.usedPositions.indexOf("left")<0){this.$element.addClass("left")
}else{if(p==="top"&&this.usedPositions.indexOf("bottom")>-1&&this.usedPositions.indexOf("left")<0){this.$element.removeClass(p).addClass("left")
}else{if(p==="left"&&this.usedPositions.indexOf("right")>-1&&this.usedPositions.indexOf("bottom")<0){this.$element.removeClass(p)
}else{if(p==="right"&&this.usedPositions.indexOf("left")>-1&&this.usedPositions.indexOf("bottom")<0){this.$element.removeClass(p)
}else{this.$element.removeClass(p)
}}}}}}}}this.classChanged=true;
this.counter--
}},{key:"_setPosition",value:function h(){if(this.$anchor.attr("aria-expanded")==="false"){return false
}var t=this.getPositionClass(),y=Foundation.Box.GetDimensions(this.$element),p=Foundation.Box.GetDimensions(this.$anchor),v=this,x=t==="left"?"left":t==="right"?"left":"top",q=x==="top"?"height":"width",r=q==="height"?this.options.vOffset:this.options.hOffset;
if(y.width>=y.windowDims.width||!this.counter&&!Foundation.Box.ImNotTouchingYou(this.$element,this.$parent)){var s=y.windowDims.width,u=0;
if(this.$parent){var w=Foundation.Box.GetDimensions(this.$parent),u=w.offset.left;
if(w.width<s){s=w.width
}}this.$element.offset(Foundation.Box.GetOffsets(this.$element,this.$anchor,"center bottom",this.options.vOffset,this.options.hOffset+u,true)).css({width:s-this.options.hOffset*2,height:"auto"});
this.classChanged=true;
return false
}this.$element.offset(Foundation.Box.GetOffsets(this.$element,this.$anchor,t,this.options.vOffset,this.options.hOffset));
while(!Foundation.Box.ImNotTouchingYou(this.$element,this.$parent,true)&&this.counter){this._reposition(t);
this._setPosition()
}}},{key:"_events",value:function d(){var p=this;
this.$element.on({"open.zf.trigger":this.open.bind(this),"close.zf.trigger":this.close.bind(this),"toggle.zf.trigger":this.toggle.bind(this),"resizeme.zf.trigger":this._setPosition.bind(this)});
if(this.options.hover){this.$anchor.off("mouseenter.zf.dropdown mouseleave.zf.dropdown").on("mouseenter.zf.dropdown",function(){var q=b("body").data();
if(typeof q.whatinput==="undefined"||q.whatinput==="mouse"){clearTimeout(p.timeout);
p.timeout=setTimeout(function(){p.open();
p.$anchor.data("hover",true)
},p.options.hoverDelay)
}}).on("mouseleave.zf.dropdown",function(){clearTimeout(p.timeout);
p.timeout=setTimeout(function(){p.close();
p.$anchor.data("hover",false)
},p.options.hoverDelay)
});
if(this.options.hoverPane){this.$element.off("mouseenter.zf.dropdown mouseleave.zf.dropdown").on("mouseenter.zf.dropdown",function(){clearTimeout(p.timeout)
}).on("mouseleave.zf.dropdown",function(){clearTimeout(p.timeout);
p.timeout=setTimeout(function(){p.close();
p.$anchor.data("hover",false)
},p.options.hoverDelay)
})
}}this.$anchor.add(this.$element).on("keydown.zf.dropdown",function(r){var q=b(this),s=Foundation.Keyboard.findFocusable(p.$element);
Foundation.Keyboard.handleKey(r,"Dropdown",{open:function(){if(q.is(p.$anchor)){p.open();
p.$element.attr("tabindex",-1).focus();
r.preventDefault()
}},close:function(){p.close();
p.$anchor.focus()
}})
})
}},{key:"_addBodyHandler",value:function o(){var p=b(document.body).not(this.$element),q=this;
p.off("click.zf.dropdown").on("click.zf.dropdown",function(r){if(q.$anchor.is(r.target)||q.$anchor.find(r.target).length){return
}if(q.$element.find(r.target).length){return
}q.close();
p.off("click.zf.dropdown")
})
}},{key:"open",value:function f(){this.$element.trigger("closeme.zf.dropdown",this.$element.attr("id"));
this.$anchor.addClass("hover").attr({"aria-expanded":true});
this._setPosition();
this.$element.addClass("is-open").attr({"aria-hidden":false});
if(this.options.autoFocus){var p=Foundation.Keyboard.findFocusable(this.$element);
if(p.length){p.eq(0).focus()
}}if(this.options.closeOnClick){this._addBodyHandler()
}if(this.options.trapFocus){Foundation.Keyboard.trapFocus(this.$element)
}this.$element.trigger("show.zf.dropdown",[this.$element])
}},{key:"close",value:function n(){if(!this.$element.hasClass("is-open")){return false
}this.$element.removeClass("is-open").attr({"aria-hidden":true});
this.$anchor.removeClass("hover").attr("aria-expanded",false);
if(this.classChanged){var p=this.getPositionClass();
if(p){this.$element.removeClass(p)
}this.$element.addClass(this.options.positionClass).css({height:"",width:""});
this.classChanged=false;
this.counter=4;
this.usedPositions.length=0
}this.$element.trigger("hide.zf.dropdown",[this.$element]);
if(this.options.trapFocus){Foundation.Keyboard.releaseFocus(this.$element)
}}},{key:"toggle",value:function e(){if(this.$element.hasClass("is-open")){if(this.$anchor.data("hover")){return
}this.close()
}else{this.open()
}}},{key:"destroy",value:function m(){this.$element.off(".zf.trigger").hide();
this.$anchor.off(".zf.dropdown");
Foundation.unregisterPlugin(this)
}}]);
return l
}();
a.defaults={parentClass:null,hoverDelay:250,hover:false,hoverPane:false,vOffset:1,hOffset:1,positionClass:"",trapFocus:false,autoFocus:false,closeOnClick:false};
Foundation.plugin(a,"Dropdown")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function d(n,m){_classCallCheck(this,d);
this.$element=n;
this.options=b.extend({},d.defaults,this.$element.data(),m);
Foundation.Nest.Feather(this.$element,"dropdown");
this._init();
Foundation.registerPlugin(this,"DropdownMenu");
Foundation.Keyboard.register("DropdownMenu",{ENTER:"open",SPACE:"open",ARROW_RIGHT:"next",ARROW_UP:"up",ARROW_DOWN:"down",ARROW_LEFT:"previous",ESCAPE:"close"})
}_createClass(d,[{key:"_init",value:function h(){var m=this.$element.find("li.is-dropdown-submenu-parent");
this.$element.children(".is-dropdown-submenu-parent").children(".is-dropdown-submenu").addClass("first-sub");
this.$menuItems=this.$element.find('[role="menuitem"]');
this.$tabs=this.$element.children('[role="menuitem"]');
this.$tabs.find("ul.is-dropdown-submenu").addClass(this.options.verticalClass);
if(this.$element.hasClass(this.options.rightClass)||this.options.alignment==="right"||Foundation.rtl()||this.$element.parents(".top-bar-right").is("*")){this.options.alignment="right";
m.addClass("opens-left")
}else{m.addClass("opens-right")
}this.changed=false;
this._events()
}},{key:"_isVertical",value:function g(){return this.$tabs.css("display")==="block"
}},{key:"_events",value:function k(){var p=this,o="ontouchstart" in window||typeof window.ontouchstart!=="undefined",n="is-dropdown-submenu-parent";
var m=function(t){var q=b(t.target).parentsUntil("ul","."+n),r=q.hasClass(n),u=q.attr("data-is-click")==="true",s=q.children(".is-dropdown-submenu");
if(r){if(u){if(!p.options.closeOnClick||!p.options.clickOpen&&!o||p.options.forceFollow&&o){return
}else{t.stopImmediatePropagation();
t.preventDefault();
p._hide(q)
}}else{t.preventDefault();
t.stopImmediatePropagation();
p._show(s);
q.add(q.parentsUntil(p.$element,"."+n)).attr("data-is-click",true)
}}};
if(this.options.clickOpen||o){this.$menuItems.on("click.zf.dropdownmenu touchstart.zf.dropdownmenu",m)
}if(p.options.closeOnClickInside){this.$menuItems.on("click.zf.dropdownmenu",function(s){var q=b(this),r=q.hasClass(n);
if(!r){p._hide()
}})
}if(!this.options.disableHover){this.$menuItems.on("mouseenter.zf.dropdownmenu",function(s){var q=b(this),r=q.hasClass(n);
if(r){clearTimeout(q.data("_delay"));
q.data("_delay",setTimeout(function(){p._show(q.children(".is-dropdown-submenu"))
},p.options.hoverDelay))
}}).on("mouseleave.zf.dropdownmenu",function(s){var q=b(this),r=q.hasClass(n);
if(r&&p.options.autoclose){if(q.attr("data-is-click")==="true"&&p.options.clickOpen){return false
}clearTimeout(q.data("_delay"));
q.data("_delay",setTimeout(function(){p._hide(q)
},p.options.closingTime))
}})
}this.$menuItems.on("keydown.zf.dropdownmenu",function(v){var A=b(v.target).parentsUntil("ul",'[role="menuitem"]'),q=p.$tabs.index(A)>-1,w=q?p.$tabs:A.siblings("li").add(A),z,y;
w.each(function(B){if(b(this).is(A)){z=w.eq(B-1);
y=w.eq(B+1);
return
}});
var s=function(){if(!A.is(":last-child")){y.children("a:first").focus();
v.preventDefault()
}},x=function(){z.children("a:first").focus();
v.preventDefault()
},r=function(){var B=A.children("ul.is-dropdown-submenu");
if(B.length){p._show(B);
A.find("li > a:first").focus();
v.preventDefault()
}else{return
}},u=function(){var B=A.parent("ul").parent("li");
B.children("a:first").focus();
p._hide(B);
v.preventDefault()
};
var t={open:r,close:function(){p._hide(p.$element);
p.$menuItems.find("a:first").focus();
v.preventDefault()
},handled:function(){v.stopImmediatePropagation()
}};
if(q){if(p._isVertical()){if(Foundation.rtl()){b.extend(t,{down:s,up:x,next:u,previous:r})
}else{b.extend(t,{down:s,up:x,next:r,previous:u})
}}else{if(Foundation.rtl()){b.extend(t,{next:x,previous:s,down:r,up:u})
}else{b.extend(t,{next:s,previous:x,down:r,up:u})
}}}else{if(Foundation.rtl()){b.extend(t,{next:u,previous:r,down:s,up:x})
}else{b.extend(t,{next:r,previous:u,down:s,up:x})
}}Foundation.Keyboard.handleKey(v,"DropdownMenu",t)
})
}},{key:"_addBodyHandler",value:function f(){var m=b(document.body),n=this;
m.off("mouseup.zf.dropdownmenu touchend.zf.dropdownmenu").on("mouseup.zf.dropdownmenu touchend.zf.dropdownmenu",function(p){var o=n.$element.find(p.target);
if(o.length){return
}n._hide();
m.off("mouseup.zf.dropdownmenu touchend.zf.dropdownmenu")
})
}},{key:"_show",value:function l(q){var n=this.$tabs.index(this.$tabs.filter(function(s,t){return b(t).find(q).length>0
}));
var p=q.parent("li.is-dropdown-submenu-parent").siblings("li.is-dropdown-submenu-parent");
this._hide(p,n);
q.css("visibility","hidden").addClass("js-dropdown-active").parent("li.is-dropdown-submenu-parent").addClass("is-active");
var m=Foundation.Box.ImNotTouchingYou(q,null,true);
if(!m){var o=this.options.alignment==="left"?"-right":"-left",r=q.parent(".is-dropdown-submenu-parent");
r.removeClass("opens"+o).addClass("opens-"+this.options.alignment);
m=Foundation.Box.ImNotTouchingYou(q,null,true);
if(!m){r.removeClass("opens-"+this.options.alignment).addClass("opens-inner")
}this.changed=true
}q.css("visibility","");
if(this.options.closeOnClick){this._addBodyHandler()
}this.$element.trigger("show.zf.dropdownmenu",[q])
}},{key:"_hide",value:function c(o,n){var m;
if(o&&o.length){m=o
}else{if(n!==undefined){m=this.$tabs.not(function(r,s){return r===n
})
}else{m=this.$element
}}var q=m.hasClass("is-active")||m.find(".is-active").length>0;
if(q){m.find("li.is-active").add(m).attr({"data-is-click":false}).removeClass("is-active");
m.find("ul.js-dropdown-active").removeClass("js-dropdown-active");
if(this.changed||m.find("opens-inner").length){var p=this.options.alignment==="left"?"right":"left";
m.find("li.is-dropdown-submenu-parent").add(m).removeClass("opens-inner opens-"+this.options.alignment).addClass("opens-"+p);
this.changed=false
}this.$element.trigger("hide.zf.dropdownmenu",[m])
}}},{key:"destroy",value:function e(){this.$menuItems.off(".zf.dropdownmenu").removeAttr("data-is-click").removeClass("is-right-arrow is-left-arrow is-down-arrow opens-right opens-left opens-inner");
b(document.body).off(".zf.dropdownmenu");
Foundation.Nest.Burn(this.$element,"dropdown");
Foundation.unregisterPlugin(this)
}}]);
return d
}();
a.defaults={disableHover:false,autoclose:true,hoverDelay:50,clickOpen:false,closingTime:500,alignment:"left",closeOnClick:true,closeOnClickInside:true,verticalClass:"vertical",rightClass:"align-right",forceFollow:true};
Foundation.plugin(a,"DropdownMenu")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function k(u,t){_classCallCheck(this,k);
this.$element=u;
this.options=b.extend({},k.defaults,this.$element.data(),t);
this._init();
Foundation.registerPlugin(this,"Equalizer")
}_createClass(k,[{key:"_init",value:function o(){var u=this.$element.attr("data-equalizer")||"";
var t=this.$element.find('[data-equalizer-watch="'+u+'"]');
this.$watched=t.length?t:this.$element.find("[data-equalizer-watch]");
this.$element.attr("data-resize",u||Foundation.GetYoDigits(6,"eq"));
this.$element.attr("data-mutate",u||Foundation.GetYoDigits(6,"eq"));
this.hasNested=this.$element.find("[data-equalizer]").length>0;
this.isNested=this.$element.parentsUntil(document.body,"[data-equalizer]").length>0;
this.isOn=false;
this._bindHandler={onResizeMeBound:this._onResizeMe.bind(this),onPostEqualizedBound:this._onPostEqualized.bind(this)};
var w=this.$element.find("img");
var v;
if(this.options.equalizeOn){v=this._checkMQ();
b(window).on("changed.zf.mediaquery",this._checkMQ.bind(this))
}else{this._events()
}if(v!==undefined&&v===false||v===undefined){if(w.length){Foundation.onImagesLoaded(w,this._reflow.bind(this))
}else{this._reflow()
}}}},{key:"_pauseEvents",value:function h(){this.isOn=false;
this.$element.off({".zf.equalizer":this._bindHandler.onPostEqualizedBound,"resizeme.zf.trigger":this._bindHandler.onResizeMeBound,"mutateme.zf.trigger":this._bindHandler.onResizeMeBound})
}},{key:"_onResizeMe",value:function l(t){this._reflow()
}},{key:"_onPostEqualized",value:function p(t){if(t.target!==this.$element[0]){this._reflow()
}}},{key:"_events",value:function g(){var t=this;
this._pauseEvents();
if(this.hasNested){this.$element.on("postequalized.zf.equalizer",this._bindHandler.onPostEqualizedBound)
}else{this.$element.on("resizeme.zf.trigger",this._bindHandler.onResizeMeBound);
this.$element.on("mutateme.zf.trigger",this._bindHandler.onResizeMeBound)
}this.isOn=true
}},{key:"_checkMQ",value:function s(){var t=!Foundation.MediaQuery.is(this.options.equalizeOn);
if(t){if(this.isOn){this._pauseEvents();
this.$watched.css("height","auto")
}}else{if(!this.isOn){this._events()
}}return t
}},{key:"_killswitch",value:function n(){return
}},{key:"_reflow",value:function q(){if(!this.options.equalizeOnStack){if(this._isStacked()){this.$watched.css("height","auto");
return false
}}if(this.options.equalizeByRow){this.getHeightsByRow(this.applyHeightByRow.bind(this))
}else{this.getHeights(this.applyHeight.bind(this))
}}},{key:"_isStacked",value:function c(){if(!this.$watched[0]||!this.$watched[1]){return true
}return this.$watched[0].getBoundingClientRect().top!==this.$watched[1].getBoundingClientRect().top
}},{key:"getHeights",value:function f(u){var w=[];
for(var v=0,t=this.$watched.length;
v<t;
v++){this.$watched[v].style.height="auto";
w.push(this.$watched[v].offsetHeight)
}u(w)
}},{key:"getHeightsByRow",value:function e(w){var u=this.$watched.length?this.$watched.first().offset().top:0,v=[],D=0;
v[D]=[];
for(var y=0,z=this.$watched.length;
y<z;
y++){this.$watched[y].style.height="auto";
var t=b(this.$watched[y]).offset().top;
if(t!=u){D++;
v[D]=[];
u=t
}v[D].push([this.$watched[y],this.$watched[y].offsetHeight])
}for(var x=0,A=v.length;
x<A;
x++){var B=b(v[x]).map(function(){return this[1]
}).get();
var C=Math.max.apply(null,B);
v[x].push(C)
}w(v)
}},{key:"applyHeight",value:function d(u){var t=Math.max.apply(null,u);
this.$element.trigger("preequalized.zf.equalizer");
this.$watched.css("height",t);
this.$element.trigger("postequalized.zf.equalizer")
}},{key:"applyHeightByRow",value:function m(w){this.$element.trigger("preequalized.zf.equalizer");
for(var z=0,v=w.length;
z<v;
z++){var x=w[z].length,u=w[z][x-1];
if(x<=2){b(w[z][0][0]).css({height:"auto"});
continue
}this.$element.trigger("preequalizedrow.zf.equalizer");
for(var y=0,t=x-1;
y<t;
y++){b(w[z][y][0]).css({height:u})
}this.$element.trigger("postequalizedrow.zf.equalizer")
}this.$element.trigger("postequalized.zf.equalizer")
}},{key:"destroy",value:function r(){this._pauseEvents();
this.$watched.css("height","auto");
Foundation.unregisterPlugin(this)
}}]);
return k
}();
a.defaults={equalizeOnStack:false,equalizeByRow:false,equalizeOn:""};
Foundation.plugin(a,"Equalizer")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(a){var b=function(){function l(n,m){_classCallCheck(this,l);
this.$element=n;
this.options=a.extend({},l.defaults,m);
this.rules=[];
this.currentPath="";
this._init();
this._events();
Foundation.registerPlugin(this,"Interchange")
}_createClass(l,[{key:"_init",value:function h(){this._addBreakpoints();
this._generateRules();
this._reflow()
}},{key:"_events",value:function k(){var m=this;
a(window).on("resize.zf.interchange",Foundation.util.throttle(function(){m._reflow()
},50))
}},{key:"_reflow",value:function g(){var m;
for(var n in this.rules){if(this.rules.hasOwnProperty(n)){var o=this.rules[n];
if(window.matchMedia(o.query).matches){m=o
}}}if(m){this.replace(m.path)
}}},{key:"_addBreakpoints",value:function d(){for(var m in Foundation.MediaQuery.queries){if(Foundation.MediaQuery.queries.hasOwnProperty(m)){var n=Foundation.MediaQuery.queries[m];
l.SPECIAL_QUERIES[n.name]=n.value
}}}},{key:"_generateRules",value:function c(n){var s=[];
var r;
if(this.options.rules){r=this.options.rules
}else{r=this.$element.data("interchange")
}r=typeof r==="string"?r.match(/\[.*?\]/g):r;
for(var m in r){if(r.hasOwnProperty(m)){var q=r[m].slice(1,-1).split(", ");
var p=q.slice(0,-1).join("");
var o=q[q.length-1];
if(l.SPECIAL_QUERIES[o]){o=l.SPECIAL_QUERIES[o]
}s.push({path:p,query:o})
}}this.rules=s
}},{key:"replace",value:function f(n){if(this.currentPath===n){return
}var o=this,m="replaced.zf.interchange";
if(this.$element[0].nodeName==="IMG"){this.$element.attr("src",n).on("load",function(){o.currentPath=n
}).trigger(m)
}else{if(n.match(/\.(gif|jpg|jpeg|png|svg|tiff)([?#].*)?/i)){this.$element.css({"background-image":"url("+n+")"}).trigger(m)
}else{a.get(n,function(p){o.$element.html(p).trigger(m);
a(p).foundation();
o.currentPath=n
})
}}}},{key:"destroy",value:function e(){}}]);
return l
}();
b.defaults={rules:null};
b.SPECIAL_QUERIES={landscape:"screen and (orientation: landscape)",portrait:"screen and (orientation: portrait)",retina:"only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx)"};
Foundation.plugin(b,"Interchange")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(a){var b=function(){function l(n,m){_classCallCheck(this,l);
this.$element=n;
this.options=a.extend({},l.defaults,this.$element.data(),m);
this._init();
this.calcPoints();
Foundation.registerPlugin(this,"Magellan")
}_createClass(l,[{key:"_init",value:function g(){var n=this.$element[0].id||Foundation.GetYoDigits(6,"magellan");
var m=this;
this.$targets=a("[data-magellan-target]");
this.$links=this.$element.find("a");
this.$element.attr({"data-resize":n,"data-scroll":n,id:n});
this.$active=a();
this.scrollPos=parseInt(window.pageYOffset,10);
this._events()
}},{key:"calcPoints",value:function e(){var o=this,m=document.body,n=document.documentElement;
this.points=[];
this.winHeight=Math.round(Math.max(window.innerHeight,n.clientHeight));
this.docHeight=Math.round(Math.max(m.scrollHeight,m.offsetHeight,n.clientHeight,n.scrollHeight,n.offsetHeight));
this.$targets.each(function(){var p=a(this),q=Math.round(p.offset().top-o.options.threshold);
p.targetPoint=q;
o.points.push(q)
})
}},{key:"_events",value:function k(){var o=this,n=a("html, body"),m={duration:o.options.animationDuration,easing:o.options.animationEasing};
a(window).one("load",function(){if(o.options.deepLinking){if(location.hash){o.scrollToLoc(location.hash)
}}o.calcPoints();
o._updateActive()
});
this.$element.on({"resizeme.zf.trigger":this.reflow.bind(this),"scrollme.zf.trigger":this._updateActive.bind(this)}).on("click.zf.magellan",'a[href^="#"]',function(q){q.preventDefault();
var p=this.getAttribute("href");
o.scrollToLoc(p)
});
a(window).on("popstate",function(p){if(o.options.deepLinking){o.scrollToLoc(window.location.hash)
}})
}},{key:"scrollToLoc",value:function d(m){if(!a(m).length){return false
}this._inTransition=true;
var o=this,n=Math.round(a(m).offset().top-this.options.threshold/2-this.options.barOffset);
a("html, body").stop(true).animate({scrollTop:n},this.options.animationDuration,this.options.animationEasing,function(){o._inTransition=false;
o._updateActive()
})
}},{key:"reflow",value:function c(){this.calcPoints();
this._updateActive()
}},{key:"_updateActive",value:function h(){if(this._inTransition){return
}var n=parseInt(window.pageYOffset,10),o;
if(n+this.winHeight===this.docHeight){o=this.points.length-1
}else{if(n<this.points[0]){o=undefined
}else{var m=this.scrollPos<n,r=this,p=this.points.filter(function(t,s){return m?t-r.options.barOffset<=n:t-r.options.barOffset-r.options.threshold<=n
});
o=p.length?p.length-1:0
}}this.$active.removeClass(this.options.activeClass);
this.$active=this.$links.filter('[href="#'+this.$targets.eq(o).data("magellan-target")+'"]').addClass(this.options.activeClass);
if(this.options.deepLinking){var q="";
if(o!=undefined){q=this.$active[0].getAttribute("href")
}if(q!==window.location.hash){if(window.history.pushState){window.history.pushState(null,null,q)
}else{window.location.hash=q
}}}this.scrollPos=n;
this.$element.trigger("update.zf.magellan",[this.$active])
}},{key:"destroy",value:function f(){this.$element.off(".zf.trigger .zf.magellan").find("."+this.options.activeClass).removeClass(this.options.activeClass);
if(this.options.deepLinking){var m=this.$active[0].getAttribute("href");
window.location.hash.replace(m,"")
}Foundation.unregisterPlugin(this)
}}]);
return l
}();
b.defaults={animationDuration:500,animationEasing:"linear",threshold:50,activeClass:"active",deepLinking:false,barOffset:0};
Foundation.plugin(b,"Magellan")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(a){var b=function(){function h(s,r){_classCallCheck(this,h);
this.$element=s;
this.options=a.extend({},h.defaults,this.$element.data(),r);
this.$lastTrigger=a();
this.$triggers=a();
this._init();
this._events();
Foundation.registerPlugin(this,"OffCanvas");
Foundation.Keyboard.register("OffCanvas",{ESCAPE:"close"})
}_createClass(h,[{key:"_init",value:function m(){var t=this.$element.attr("id");
this.$element.attr("aria-hidden","true");
this.$element.addClass("is-transition-"+this.options.transition);
this.$triggers=a(document).find('[data-open="'+t+'"], [data-close="'+t+'"], [data-toggle="'+t+'"]').attr("aria-expanded","false").attr("aria-controls",t);
if(this.options.contentOverlay===true){var r=document.createElement("div");
var s=a(this.$element).css("position")==="fixed"?"is-overlay-fixed":"is-overlay-absolute";
r.setAttribute("class","js-off-canvas-overlay "+s);
this.$overlay=a(r);
if(s==="is-overlay-fixed"){a("body").append(this.$overlay)
}else{this.$element.siblings("[data-off-canvas-content]").append(this.$overlay)
}}this.options.isRevealed=this.options.isRevealed||new RegExp(this.options.revealClass,"g").test(this.$element[0].className);
if(this.options.isRevealed===true){this.options.revealOn=this.options.revealOn||this.$element[0].className.match(/(reveal-for-medium|reveal-for-large)/g)[0].split("-")[2];
this._setMQChecker()
}if(!this.options.transitionTime===true){this.options.transitionTime=parseFloat(window.getComputedStyle(a("[data-off-canvas]")[0]).transitionDuration)*1000
}}},{key:"_events",value:function e(){this.$element.off(".zf.trigger .zf.offcanvas").on({"open.zf.trigger":this.open.bind(this),"close.zf.trigger":this.close.bind(this),"toggle.zf.trigger":this.toggle.bind(this),"keydown.zf.offcanvas":this._handleKeyboard.bind(this)});
if(this.options.closeOnClick===true){var r=this.options.contentOverlay?this.$overlay:a("[data-off-canvas-content]");
r.on({"click.zf.offcanvas":this.close.bind(this)})
}}},{key:"_setMQChecker",value:function l(){var r=this;
a(window).on("changed.zf.mediaquery",function(){if(Foundation.MediaQuery.atLeast(r.options.revealOn)){r.reveal(true)
}else{r.reveal(false)
}}).one("load.zf.offcanvas",function(){if(Foundation.MediaQuery.atLeast(r.options.revealOn)){r.reveal(true)
}})
}},{key:"reveal",value:function o(s){var r=this.$element.find("[data-close]");
if(s){this.close();
this.isRevealed=true;
this.$element.attr("aria-hidden","false");
this.$element.off("open.zf.trigger toggle.zf.trigger");
if(r.length){r.hide()
}}else{this.isRevealed=false;
this.$element.attr("aria-hidden","true");
this.$element.on({"open.zf.trigger":this.open.bind(this),"toggle.zf.trigger":this.toggle.bind(this)});
if(r.length){r.show()
}}}},{key:"_stopScrolling",value:function k(r){return false
}},{key:"_recordScrollable",value:function c(s){var r=this;
if(r.scrollHeight!==r.clientHeight){if(r.scrollTop===0){r.scrollTop=1
}if(r.scrollTop===r.scrollHeight-r.clientHeight){r.scrollTop=r.scrollHeight-r.clientHeight-1
}}r.allowUp=r.scrollTop>0;
r.allowDown=r.scrollTop<r.scrollHeight-r.clientHeight;
r.lastY=s.originalEvent.pageY
}},{key:"_stopScrollPropagation",value:function q(t){var s=this;
var r=t.pageY<s.lastY;
var u=!r;
s.lastY=t.pageY;
if(r&&s.allowUp||u&&s.allowDown){t.stopPropagation()
}else{t.preventDefault()
}}},{key:"open",value:function g(s,r){if(this.$element.hasClass("is-open")||this.isRevealed){return
}var t=this;
if(r){this.$lastTrigger=r
}if(this.options.forceTo==="top"){window.scrollTo(0,0)
}else{if(this.options.forceTo==="bottom"){window.scrollTo(0,document.body.scrollHeight)
}}t.$element.addClass("is-open");
this.$triggers.attr("aria-expanded","true");
this.$element.attr("aria-hidden","false").trigger("opened.zf.offcanvas");
if(this.options.contentScroll===false){a("body").addClass("is-off-canvas-open").on("touchmove",this._stopScrolling);
this.$element.on("touchstart",this._recordScrollable);
this.$element.on("touchmove",this._stopScrollPropagation)
}if(this.options.contentOverlay===true){this.$overlay.addClass("is-visible")
}if(this.options.closeOnClick===true&&this.options.contentOverlay===true){this.$overlay.addClass("is-closable")
}if(this.options.autoFocus===true){this.$element.one(Foundation.transitionend(this.$element),function(){t.$element.find("a, button").eq(0).focus()
})
}if(this.options.trapFocus===true){this.$element.siblings("[data-off-canvas-content]").attr("tabindex","-1");
Foundation.Keyboard.trapFocus(this.$element)
}}},{key:"close",value:function p(r){if(!this.$element.hasClass("is-open")||this.isRevealed){return
}var s=this;
s.$element.removeClass("is-open");
this.$element.attr("aria-hidden","true").trigger("closed.zf.offcanvas");
if(this.options.contentScroll===false){a("body").removeClass("is-off-canvas-open").off("touchmove",this._stopScrolling);
this.$element.off("touchstart",this._recordScrollable);
this.$element.off("touchmove",this._stopScrollPropagation)
}if(this.options.contentOverlay===true){this.$overlay.removeClass("is-visible")
}if(this.options.closeOnClick===true&&this.options.contentOverlay===true){this.$overlay.removeClass("is-closable")
}this.$triggers.attr("aria-expanded","false");
if(this.options.trapFocus===true){this.$element.siblings("[data-off-canvas-content]").removeAttr("tabindex");
Foundation.Keyboard.releaseFocus(this.$element)
}}},{key:"toggle",value:function f(s,r){if(this.$element.hasClass("is-open")){this.close(s,r)
}else{this.open(s,r)
}}},{key:"_handleKeyboard",value:function d(s){var r=this;
Foundation.Keyboard.handleKey(s,"OffCanvas",{close:function(){r.close();
r.$lastTrigger.focus();
return true
},handled:function(){s.stopPropagation();
s.preventDefault()
}})
}},{key:"destroy",value:function n(){this.close();
this.$element.off(".zf.trigger .zf.offcanvas");
this.$overlay.off(".zf.offcanvas");
Foundation.unregisterPlugin(this)
}}]);
return h
}();
b.defaults={closeOnClick:true,contentOverlay:true,contentScroll:true,transitionTime:0,transition:"push",forceTo:null,isRevealed:false,revealOn:null,autoFocus:true,revealClass:"reveal-for-",trapFocus:false};
Foundation.plugin(b,"OffCanvas")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(a){var b=function(){function e(r,q){_classCallCheck(this,e);
this.$element=r;
this.options=a.extend({},e.defaults,this.$element.data(),q);
this._init();
Foundation.registerPlugin(this,"Orbit");
Foundation.Keyboard.register("Orbit",{ltr:{ARROW_RIGHT:"next",ARROW_LEFT:"previous"},rtl:{ARROW_LEFT:"next",ARROW_RIGHT:"previous"}})
}_createClass(e,[{key:"_init",value:function n(){this._reset();
this.$wrapper=this.$element.find("."+this.options.containerClass);
this.$slides=this.$element.find("."+this.options.slideClass);
var q=this.$element.find("img"),r=this.$slides.filter(".is-active"),s=this.$element[0].id||Foundation.GetYoDigits(6,"orbit");
this.$element.attr({"data-resize":s,id:s});
if(!r.length){this.$slides.eq(0).addClass("is-active")
}if(!this.options.useMUI){this.$slides.addClass("no-motionui")
}if(q.length){Foundation.onImagesLoaded(q,this._prepareForOrbit.bind(this))
}else{this._prepareForOrbit()
}if(this.options.bullets){this._loadBullets()
}this._events();
if(this.options.autoPlay&&this.$slides.length>1){this.geoSync()
}if(this.options.accessible){this.$wrapper.attr("tabindex",0)
}}},{key:"_loadBullets",value:function f(){this.$bullets=this.$element.find("."+this.options.boxOfBullets).find("button")
}},{key:"geoSync",value:function g(){var q=this;
this.timer=new Foundation.Timer(this.$element,{duration:this.options.timerDelay,infinite:false},function(){q.changeSlide(true)
});
this.timer.start()
}},{key:"_prepareForOrbit",value:function l(){var q=this;
this._setWrapperHeight()
}},{key:"_setWrapperHeight",value:function k(r){var q=0,t,s=0,u=this;
this.$slides.each(function(){t=this.getBoundingClientRect().height;
a(this).attr("data-slide",s);
if(u.$slides.filter(".is-active")[0]!==u.$slides.eq(s)[0]){a(this).css({position:"relative",display:"none"})
}q=t>q?t:q;
s++
});
if(s===this.$slides.length){this.$wrapper.css({height:q});
if(r){r(q)
}}}},{key:"_setSlideHeight",value:function d(q){this.$slides.each(function(){a(this).css("max-height",q)
})
}},{key:"_events",value:function m(){var r=this;
this.$element.off(".resizeme.zf.trigger").on({"resizeme.zf.trigger":this._prepareForOrbit.bind(this)});
if(this.$slides.length>1){if(this.options.swipe){this.$slides.off("swipeleft.zf.orbit swiperight.zf.orbit").on("swipeleft.zf.orbit",function(s){s.preventDefault();
r.changeSlide(true)
}).on("swiperight.zf.orbit",function(s){s.preventDefault();
r.changeSlide(false)
})
}if(this.options.autoPlay){this.$slides.on("click.zf.orbit",function(){r.$element.data("clickedOn",r.$element.data("clickedOn")?false:true);
r.timer[r.$element.data("clickedOn")?"pause":"start"]()
});
if(this.options.pauseOnHover){this.$element.on("mouseenter.zf.orbit",function(){r.timer.pause()
}).on("mouseleave.zf.orbit",function(){if(!r.$element.data("clickedOn")){r.timer.start()
}})
}}if(this.options.navButtons){var q=this.$element.find("."+this.options.nextClass+", ."+this.options.prevClass);
q.attr("tabindex",0).on("click.zf.orbit touchend.zf.orbit",function(s){s.preventDefault();
r.changeSlide(a(this).hasClass(r.options.nextClass))
})
}if(this.options.bullets){this.$bullets.on("click.zf.orbit touchend.zf.orbit",function(){if(/is-active/g.test(this.className)){return false
}var s=a(this).data("slide"),t=s>r.$slides.filter(".is-active").data("slide"),u=r.$slides.eq(s);
r.changeSlide(t,u,s)
})
}if(this.options.accessible){this.$wrapper.add(this.$bullets).on("keydown.zf.orbit",function(s){Foundation.Keyboard.handleKey(s,"Orbit",{next:function(){r.changeSlide(true)
},previous:function(){r.changeSlide(false)
},handled:function(){if(a(s.target).is(r.$bullets)){r.$bullets.filter(".is-active").focus()
}}})
})
}}}},{key:"_reset",value:function o(){if(typeof this.$slides=="undefined"){return
}if(this.$slides.length>1){this.$element.off(".zf.orbit").find("*").off(".zf.orbit");
if(this.options.autoPlay){this.timer.restart()
}this.$slides.each(function(q){a(q).removeClass("is-active is-active is-in").removeAttr("aria-live").hide()
});
this.$slides.first().addClass("is-active").show();
this.$element.trigger("slidechange.zf.orbit",[this.$slides.first()]);
if(this.options.bullets){this._updateBullets(0)
}}}},{key:"changeSlide",value:function h(x,z,y){if(!this.$slides){return
}var s=this.$slides.filter(".is-active").eq(0);
if(/mui/g.test(s[0].className)){return false
}var w=this.$slides.first(),r=this.$slides.last(),u=x?"Right":"Left",q=x?"Left":"Right",v=this,t;
if(!z){t=x?this.options.infiniteWrap?s.next("."+this.options.slideClass).length?s.next("."+this.options.slideClass):w:s.next("."+this.options.slideClass):this.options.infiniteWrap?s.prev("."+this.options.slideClass).length?s.prev("."+this.options.slideClass):r:s.prev("."+this.options.slideClass)
}else{t=z
}if(t.length){this.$element.trigger("beforeslidechange.zf.orbit",[s,t]);
if(this.options.bullets){y=y||this.$slides.index(t);
this._updateBullets(y)
}if(this.options.useMUI&&!this.$element.is(":hidden")){Foundation.Motion.animateIn(t.addClass("is-active").css({position:"absolute",top:0}),this.options["animInFrom"+u],function(){t.css({position:"relative",display:"block"}).attr("aria-live","polite")
});
Foundation.Motion.animateOut(s.removeClass("is-active"),this.options["animOutTo"+q],function(){s.removeAttr("aria-live");
if(v.options.autoPlay&&!v.timer.isPaused){v.timer.restart()
}})
}else{s.removeClass("is-active is-in").removeAttr("aria-live").hide();
t.addClass("is-active is-in").attr("aria-live","polite").show();
if(this.options.autoPlay&&!this.timer.isPaused){this.timer.restart()
}}this.$element.trigger("slidechange.zf.orbit",[t])
}}},{key:"_updateBullets",value:function c(q){var s=this.$element.find("."+this.options.boxOfBullets).find(".is-active").removeClass("is-active").blur(),r=s.find("span:last").detach(),t=this.$bullets.eq(q).addClass("is-active").append(r)
}},{key:"destroy",value:function p(){this.$element.off(".zf.orbit").find("*").off(".zf.orbit").end().hide();
Foundation.unregisterPlugin(this)
}}]);
return e
}();
b.defaults={bullets:true,navButtons:true,animInFromRight:"slide-in-right",animOutToRight:"slide-out-right",animInFromLeft:"slide-in-left",animOutToLeft:"slide-out-left",autoPlay:true,timerDelay:5000,infiniteWrap:true,swipe:true,pauseOnHover:true,accessible:true,containerClass:"orbit-container",slideClass:"orbit-slide",boxOfBullets:"orbit-bullets",nextClass:"orbit-next",prevClass:"orbit-previous",useMUI:true};
Foundation.plugin(b,"Orbit")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(a){var b=function(){function h(l,k){_classCallCheck(this,h);
this.$element=a(l);
this.rules=this.$element.data("responsive-menu");
this.currentMq=null;
this.currentPlugin=null;
this._init();
this._events();
Foundation.registerPlugin(this,"ResponsiveMenu")
}_createClass(h,[{key:"_init",value:function e(){if(typeof this.rules==="string"){var k={};
var p=this.rules.split(" ");
for(var l=0;
l<p.length;
l++){var n=p[l].split("-");
var m=n.length>1?n[0]:"small";
var o=n.length>1?n[1]:n[0];
if(c[o]!==null){k[m]=c[o]
}}this.rules=k
}if(!a.isEmptyObject(this.rules)){this._checkMediaQueries()
}this.$element.attr("data-mutate",this.$element.attr("data-mutate")||Foundation.GetYoDigits(6,"responsive-menu"))
}},{key:"_events",value:function g(){var k=this;
a(window).on("changed.zf.mediaquery",function(){k._checkMediaQueries()
})
}},{key:"_checkMediaQueries",value:function f(){var k,l=this;
a.each(this.rules,function(m){if(Foundation.MediaQuery.atLeast(m)){k=m
}});
if(!k){return
}if(this.currentPlugin instanceof this.rules[k].plugin){return
}a.each(c,function(m,n){l.$element.removeClass(n.cssClass)
});
this.$element.addClass(this.rules[k].cssClass);
if(this.currentPlugin){this.currentPlugin.destroy()
}this.currentPlugin=new this.rules[k].plugin(this.$element,{})
}},{key:"destroy",value:function d(){this.currentPlugin.destroy();
a(window).off(".zf.ResponsiveMenu");
Foundation.unregisterPlugin(this)
}}]);
return h
}();
b.defaults={};
var c={dropdown:{cssClass:"dropdown",plugin:Foundation._plugins["dropdown-menu"]||null},drilldown:{cssClass:"drilldown",plugin:Foundation._plugins.drilldown||null},accordion:{cssClass:"accordion-menu",plugin:Foundation._plugins["accordion-menu"]||null}};
Foundation.plugin(b,"ResponsiveMenu")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function c(l,k){_classCallCheck(this,c);
this.$element=b(l);
this.options=b.extend({},c.defaults,this.$element.data(),k);
this._init();
this._events();
Foundation.registerPlugin(this,"ResponsiveToggle")
}_createClass(c,[{key:"_init",value:function g(){var l=this.$element.data("responsive-toggle");
if(!l){console.error("Your tab bar needs an ID of a Menu as the value of data-tab-bar.")
}this.$targetMenu=b("#"+l);
this.$toggler=this.$element.find("[data-toggle]").filter(function(){var m=b(this).data("toggle");
return m===l||m===""
});
this.options=b.extend({},this.options,this.$targetMenu.data());
if(this.options.animate){var k=this.options.animate.split(" ");
this.animationIn=k[0];
this.animationOut=k[1]||null
}this._update()
}},{key:"_events",value:function h(){var k=this;
this._updateMqHandler=this._update.bind(this);
b(window).on("changed.zf.mediaquery",this._updateMqHandler);
this.$toggler.on("click.zf.responsiveToggle",this.toggleMenu.bind(this))
}},{key:"_update",value:function d(){if(!Foundation.MediaQuery.atLeast(this.options.hideFor)){this.$element.show();
this.$targetMenu.hide()
}else{this.$element.hide();
this.$targetMenu.show()
}}},{key:"toggleMenu",value:function f(){var k=this;
if(!Foundation.MediaQuery.atLeast(this.options.hideFor)){if(this.options.animate){if(this.$targetMenu.is(":hidden")){Foundation.Motion.animateIn(this.$targetMenu,this.animationIn,function(){k.$element.trigger("toggled.zf.responsiveToggle");
k.$targetMenu.find("[data-mutate]").triggerHandler("mutateme.zf.trigger")
})
}else{Foundation.Motion.animateOut(this.$targetMenu,this.animationOut,function(){k.$element.trigger("toggled.zf.responsiveToggle")
})
}}else{this.$targetMenu.toggle(0);
this.$targetMenu.find("[data-mutate]").trigger("mutateme.zf.trigger");
this.$element.trigger("toggled.zf.responsiveToggle")
}}}},{key:"destroy",value:function e(){this.$element.off(".zf.responsiveToggle");
this.$toggler.off(".zf.responsiveToggle");
b(window).off("changed.zf.mediaquery",this._updateMqHandler);
Foundation.unregisterPlugin(this)
}}]);
return c
}();
a.defaults={hideFor:"medium",animate:false};
Foundation.plugin(a,"ResponsiveToggle")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(e){var d=function(){function f(t,s){_classCallCheck(this,f);
this.$element=t;
this.options=e.extend({},f.defaults,this.$element.data(),s);
this._init();
Foundation.registerPlugin(this,"Reveal");
Foundation.Keyboard.register("Reveal",{ENTER:"open",SPACE:"open",ESCAPE:"close"})
}_createClass(f,[{key:"_init",value:function o(){this.id=this.$element.attr("id");
this.isActive=false;
this.cached={mq:Foundation.MediaQuery.current};
this.isMobile=c();
this.$anchor=e('[data-open="'+this.id+'"]').length?e('[data-open="'+this.id+'"]'):e('[data-toggle="'+this.id+'"]');
this.$anchor.attr({"aria-controls":this.id,"aria-haspopup":true,tabindex:0});
if(this.options.fullScreen||this.$element.hasClass("full")){this.options.fullScreen=true;
this.options.overlay=false
}if(this.options.overlay&&!this.$overlay){this.$overlay=this._makeOverlay(this.id)
}this.$element.attr({role:"dialog","aria-hidden":true,"data-yeti-box":this.id,"data-resize":this.id});
if(this.$overlay){this.$element.detach().appendTo(this.$overlay)
}else{this.$element.detach().appendTo(e(this.options.appendTo));
this.$element.addClass("without-overlay")
}this._events();
if(this.options.deepLink&&window.location.hash==="#"+this.id){e(window).one("load.zf.reveal",this.open.bind(this))
}}},{key:"_makeOverlay",value:function l(){return e("<div></div>").addClass("reveal-overlay").appendTo(this.options.appendTo)
}},{key:"_updatePosition",value:function m(){var t=this.$element.outerWidth();
var x=e(window).width();
var s=this.$element.outerHeight();
var u=e(window).height();
var w,v;
if(this.options.hOffset==="auto"){w=parseInt((x-t)/2,10)
}else{w=parseInt(this.options.hOffset,10)
}if(this.options.vOffset==="auto"){if(s>u){v=parseInt(Math.min(100,u/10),10)
}else{v=parseInt((u-s)/4,10)
}}else{v=parseInt(this.options.vOffset,10)
}this.$element.css({top:v+"px"});
if(!this.$overlay||this.options.hOffset!=="auto"){this.$element.css({left:w+"px"});
this.$element.css({margin:"0px"})
}}},{key:"_events",value:function g(){var s=this;
var t=this;
this.$element.on({"open.zf.trigger":this.open.bind(this),"close.zf.trigger":function(v,u){if(v.target===t.$element[0]||e(v.target).parents("[data-closable]")[0]===u){return s.close.apply(s)
}},"toggle.zf.trigger":this.toggle.bind(this),"resizeme.zf.trigger":function(){t._updatePosition()
}});
if(this.$anchor.length){this.$anchor.on("keydown.zf.reveal",function(u){if(u.which===13||u.which===32){u.stopPropagation();
u.preventDefault();
t.open()
}})
}if(this.options.closeOnClick&&this.options.overlay){this.$overlay.off(".zf.reveal").on("click.zf.reveal",function(u){if(u.target===t.$element[0]||e.contains(t.$element[0],u.target)||!e.contains(document,u.target)){return
}t.close()
})
}if(this.options.deepLink){e(window).on("popstate.zf.reveal:"+this.id,this._handleState.bind(this))
}}},{key:"_handleState",value:function q(s){if(window.location.hash==="#"+this.id&&!this.isActive){this.open()
}else{this.close()
}}},{key:"open",value:function k(){var s=this;
if(this.options.deepLink){var u="#"+this.id;
if(window.history.pushState){window.history.pushState(null,null,u)
}else{window.location.hash=u
}}this.isActive=true;
this.$element.css({visibility:"hidden"}).show().scrollTop(0);
if(this.options.overlay){this.$overlay.css({visibility:"hidden"}).show()
}this._updatePosition();
this.$element.hide().css({visibility:""});
if(this.$overlay){this.$overlay.css({visibility:""}).hide();
if(this.$element.hasClass("fast")){this.$overlay.addClass("fast")
}else{if(this.$element.hasClass("slow")){this.$overlay.addClass("slow")
}}}if(!this.options.multipleOpened){this.$element.trigger("closeme.zf.reveal",this.id)
}var v=this;
function t(){if(v.isMobile){if(!v.originalScrollPos){v.originalScrollPos=window.pageYOffset
}e("html, body").addClass("is-reveal-open")
}else{e("body").addClass("is-reveal-open")
}}if(this.options.animationIn){(function(){var w=function(){v.$element.attr({"aria-hidden":false,tabindex:-1}).focus();
t();
Foundation.Keyboard.trapFocus(v.$element)
};
if(s.options.overlay){Foundation.Motion.animateIn(s.$overlay,"fade-in")
}Foundation.Motion.animateIn(s.$element,s.options.animationIn,function(){if(s.$element){s.focusableElements=Foundation.Keyboard.findFocusable(s.$element);
w()
}})
})()
}else{if(this.options.overlay){this.$overlay.show(0)
}this.$element.show(this.options.showDelay)
}this.$element.attr({"aria-hidden":false,tabindex:-1}).focus();
Foundation.Keyboard.trapFocus(this.$element);
this.$element.trigger("open.zf.reveal");
t();
setTimeout(function(){s._extraHandlers()
},0)
}},{key:"_extraHandlers",value:function n(){var s=this;
if(!this.$element){return
}this.focusableElements=Foundation.Keyboard.findFocusable(this.$element);
if(!this.options.overlay&&this.options.closeOnClick&&!this.options.fullScreen){e("body").on("click.zf.reveal",function(t){if(t.target===s.$element[0]||e.contains(s.$element[0],t.target)||!e.contains(document,t.target)){return
}s.close()
})
}if(this.options.closeOnEsc){e(window).on("keydown.zf.reveal",function(t){Foundation.Keyboard.handleKey(t,"Reveal",{close:function(){if(s.options.closeOnEsc){s.close();
s.$anchor.focus()
}}})
})
}this.$element.on("keydown.zf.reveal",function(u){var t=e(this);
Foundation.Keyboard.handleKey(u,"Reveal",{open:function(){if(s.$element.find(":focus").is(s.$element.find("[data-close]"))){setTimeout(function(){s.$anchor.focus()
},1)
}else{if(t.is(s.focusableElements)){s.open()
}}},close:function(){if(s.options.closeOnEsc){s.close();
s.$anchor.focus()
}},handled:function(v){if(v){u.preventDefault()
}}})
})
}},{key:"close",value:function r(){if(!this.isActive||!this.$element.is(":visible")){return false
}var t=this;
if(this.options.animationOut){if(this.options.overlay){Foundation.Motion.animateOut(this.$overlay,"fade-out",s)
}else{s()
}Foundation.Motion.animateOut(this.$element,this.options.animationOut)
}else{if(this.options.overlay){this.$overlay.hide(0,s)
}else{s()
}this.$element.hide(this.options.hideDelay)
}if(this.options.closeOnEsc){e(window).off("keydown.zf.reveal")
}if(!this.options.overlay&&this.options.closeOnClick){e("body").off("click.zf.reveal")
}this.$element.off("keydown.zf.reveal");
function s(){if(t.isMobile){e("html, body").removeClass("is-reveal-open");
if(t.originalScrollPos){e("body").scrollTop(t.originalScrollPos);
t.originalScrollPos=null
}}else{e("body").removeClass("is-reveal-open")
}Foundation.Keyboard.releaseFocus(t.$element);
t.$element.attr("aria-hidden",true);
t.$element.trigger("closed.zf.reveal")
}if(this.options.resetOnClose){this.$element.html(this.$element.html())
}this.isActive=false;
if(t.options.deepLink){if(window.history.replaceState){window.history.replaceState("",document.title,window.location.href.replace("#"+this.id,""))
}else{window.location.hash=""
}}}},{key:"toggle",value:function h(){if(this.isActive){this.close()
}else{this.open()
}}},{key:"destroy",value:function p(){if(this.options.overlay){this.$element.appendTo(e(this.options.appendTo));
this.$overlay.hide().off().remove()
}this.$element.hide().off();
this.$anchor.off(".zf");
e(window).off(".zf.reveal:"+this.id);
Foundation.unregisterPlugin(this)
}}]);
return f
}();
d.defaults={animationIn:"",animationOut:"",showDelay:0,hideDelay:0,closeOnClick:true,closeOnEsc:true,multipleOpened:false,vOffset:"auto",hOffset:"auto",fullScreen:false,btmOffsetPct:10,overlay:true,resetOnClose:false,deepLink:false,appendTo:"body"};
Foundation.plugin(d,"Reveal");
function a(){return(/iP(ad|hone|od).*OS/.test(window.navigator.userAgent))
}function b(){return(/Android/.test(window.navigator.userAgent))
}function c(){return a()||b()
}}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(d){var a=function(){function l(y,x){_classCallCheck(this,l);
this.$element=y;
this.options=d.extend({},l.defaults,this.$element.data(),x);
this._init();
Foundation.registerPlugin(this,"Slider");
Foundation.Keyboard.register("Slider",{ltr:{ARROW_RIGHT:"increase",ARROW_UP:"increase",ARROW_DOWN:"decrease",ARROW_LEFT:"decrease",SHIFT_ARROW_RIGHT:"increase_fast",SHIFT_ARROW_UP:"increase_fast",SHIFT_ARROW_DOWN:"decrease_fast",SHIFT_ARROW_LEFT:"decrease_fast"},rtl:{ARROW_LEFT:"increase",ARROW_RIGHT:"decrease",SHIFT_ARROW_LEFT:"increase_fast",SHIFT_ARROW_RIGHT:"decrease_fast"}})
}_createClass(l,[{key:"_init",value:function s(){this.inputs=this.$element.find("input");
this.handles=this.$element.find("[data-slider-handle]");
this.$handle=this.handles.eq(0);
this.$input=this.inputs.length?this.inputs.eq(0):d("#"+this.$handle.attr("aria-controls"));
this.$fill=this.$element.find("[data-slider-fill]").css(this.options.vertical?"height":"width",0);
var x=false,y=this;
if(this.options.disabled||this.$element.hasClass(this.options.disabledClass)){this.options.disabled=true;
this.$element.addClass(this.options.disabledClass)
}if(!this.inputs.length){this.inputs=d().add(this.$input);
this.options.binding=true
}this._setInitAttr(0);
if(this.handles[1]){this.options.doubleSided=true;
this.$handle2=this.handles.eq(1);
this.$input2=this.inputs.length>1?this.inputs.eq(1):d("#"+this.$handle2.attr("aria-controls"));
if(!this.inputs[1]){this.inputs=this.inputs.add(this.$input2)
}x=true;
this._setInitAttr(1)
}this.setHandles();
this._events()
}},{key:"setHandles",value:function p(){var x=this;
if(this.handles[1]){this._setHandlePos(this.$handle,this.inputs.eq(0).val(),true,function(){x._setHandlePos(x.$handle2,x.inputs.eq(1).val(),true)
})
}else{this._setHandlePos(this.$handle,this.inputs.eq(0).val(),true)
}}},{key:"_reflow",value:function t(){this.setHandles()
}},{key:"_pctOfBar",value:function o(x){var y=c(x-this.options.start,this.options.end-this.options.start);
switch(this.options.positionValueFunction){case"pow":y=this._logTransform(y);
break;
case"log":y=this._powTransform(y);
break
}return y.toFixed(2)
}},{key:"_value",value:function w(y){switch(this.options.positionValueFunction){case"pow":y=this._powTransform(y);
break;
case"log":y=this._logTransform(y);
break
}var x=(this.options.end-this.options.start)*y+this.options.start;
return x
}},{key:"_logTransform",value:function h(x){return e(this.options.nonLinearBase,x*(this.options.nonLinearBase-1)+1)
}},{key:"_powTransform",value:function n(x){return(Math.pow(this.options.nonLinearBase,x)-1)/(this.options.nonLinearBase-1)
}},{key:"_setHandlePos",value:function f(D,x,H,G){if(this.$element.hasClass(this.options.disabledClass)){return
}x=parseFloat(x);
if(x<this.options.start){x=this.options.start
}else{if(x>this.options.end){x=this.options.end
}}var F=this.options.doubleSided;
if(F){if(this.handles.index(D)===0){var S=parseFloat(this.$handle2.attr("aria-valuenow"));
x=x>=S?S-this.options.step:x
}else{var N=parseFloat(this.$handle.attr("aria-valuenow"));
x=x<=N?N+this.options.step:x
}}if(this.options.vertical&&!H){x=this.options.end-x
}var K=this,O=this.options.vertical,z=O?"height":"width",P=O?"top":"left",B=D[0].getBoundingClientRect()[z],C=this.$element[0].getBoundingClientRect()[z],R=this._pctOfBar(x),M=(C-B)*R,I=(c(M,C)*100).toFixed(this.options.decimal);
x=parseFloat(x.toFixed(this.options.decimal));
var E={};
this._setValues(D,x);
if(F){var y=this.handles.index(D)===0,L,J=~~(c(B,C)*100);
if(y){E[P]=I+"%";
L=parseFloat(this.$handle2[0].style[P])-I+J;
if(G&&typeof G==="function"){G()
}}else{var Q=parseFloat(this.$handle[0].style[P]);
L=I-(isNaN(Q)?(this.options.initialStart-this.options.start)/((this.options.end-this.options.start)/100):Q)+J
}E["min-"+z]=L+"%"
}this.$element.one("finished.zf.animate",function(){K.$element.trigger("moved.zf.slider",[D])
});
var A=this.$element.data("dragging")?1000/60:this.options.moveTime;
Foundation.Move(A,D,function(){if(isNaN(I)){D.css(P,R*100+"%")
}else{D.css(P,I+"%")
}if(!K.options.doubleSided){K.$fill.css(z,R*100+"%")
}else{K.$fill.css(E)
}});
clearTimeout(K.timeout);
K.timeout=setTimeout(function(){K.$element.trigger("changed.zf.slider",[D])
},K.options.changedDelay)
}},{key:"_setInitAttr",value:function g(x){var y=x===0?this.options.initialStart:this.options.initialEnd;
var z=this.inputs.eq(x).attr("id")||Foundation.GetYoDigits(6,"slider");
this.inputs.eq(x).attr({id:z,max:this.options.end,min:this.options.start,step:this.options.step});
this.inputs.eq(x).val(y);
this.handles.eq(x).attr({role:"slider","aria-controls":z,"aria-valuemax":this.options.end,"aria-valuemin":this.options.start,"aria-valuenow":y,"aria-orientation":this.options.vertical?"vertical":"horizontal",tabindex:0})
}},{key:"_setValues",value:function q(z,y){var x=this.options.doubleSided?this.handles.index(z):0;
this.inputs.eq(x).val(y);
z.attr("aria-valuenow",y)
}},{key:"_handleEvent",value:function v(K,y,P){var F,N;
if(!P){K.preventDefault();
var H=this,z=this.options.vertical,A=z?"height":"width",M=z?"top":"left",O=z?K.pageY:K.pageX,E=this.$handle[0].getBoundingClientRect()[A]/2,J=this.$element[0].getBoundingClientRect()[A],L=z?d(window).scrollTop():d(window).scrollLeft();
var C=this.$element.offset()[M];
if(K.clientY===K.pageY){O=O+L
}var B=O-C;
var x;
if(B<0){x=0
}else{if(B>J){x=J
}else{x=B
}}var D=c(x,J);
F=this._value(D);
if(Foundation.rtl()&&!this.options.vertical){F=this.options.end-F
}F=H._adjustValue(null,F);
N=false;
if(!y){var I=b(this.$handle,M,x,A),G=b(this.$handle2,M,x,A);
y=I<=G?this.$handle:this.$handle2
}}else{F=this._adjustValue(null,P);
N=true
}this._setHandlePos(y,F,N)
}},{key:"_adjustValue",value:function k(E,z){var C,x=this.options.step,D=parseFloat(x/2),B,A,y;
if(!!E){C=parseFloat(E.attr("aria-valuenow"))
}else{C=z
}B=C%x;
A=C-B;
y=A+x;
if(B===0){return C
}C=C>=A+D?y:A;
return C
}},{key:"_events",value:function m(){this._eventsForHandle(this.$handle);
if(this.handles[1]){this._eventsForHandle(this.$handle2)
}}},{key:"_eventsForHandle",value:function r(B){var A=this,x,z;
this.inputs.off("change.zf.slider").on("change.zf.slider",function(D){var C=A.inputs.index(d(this));
A._handleEvent(D,A.handles.eq(C),d(this).val())
});
if(this.options.clickSelect){this.$element.off("click.zf.slider").on("click.zf.slider",function(C){if(A.$element.data("dragging")){return false
}if(!d(C.target).is("[data-slider-handle]")){if(A.options.doubleSided){A._handleEvent(C)
}else{A._handleEvent(C,A.$handle)
}}})
}if(this.options.draggable){this.handles.addTouch();
var y=d("body");
B.off("mousedown.zf.slider").on("mousedown.zf.slider",function(C){B.addClass("is-dragging");
A.$fill.addClass("is-dragging");
A.$element.data("dragging",true);
x=d(C.currentTarget);
y.on("mousemove.zf.slider",function(D){D.preventDefault();
A._handleEvent(D,x)
}).on("mouseup.zf.slider",function(D){A._handleEvent(D,x);
B.removeClass("is-dragging");
A.$fill.removeClass("is-dragging");
A.$element.data("dragging",false);
y.off("mousemove.zf.slider mouseup.zf.slider")
})
}).on("selectstart.zf.slider touchmove.zf.slider",function(C){C.preventDefault()
})
}B.off("keydown.zf.slider").on("keydown.zf.slider",function(G){var E=d(this),C=A.options.doubleSided?A.handles.index(E):0,D=parseFloat(A.inputs.eq(C).val()),F;
Foundation.Keyboard.handleKey(G,"Slider",{decrease:function(){F=D-A.options.step
},increase:function(){F=D+A.options.step
},decrease_fast:function(){F=D-A.options.step*10
},increase_fast:function(){F=D+A.options.step*10
},handled:function(){G.preventDefault();
A._setHandlePos(E,F,true)
}})
})
}},{key:"destroy",value:function u(){this.handles.off(".zf.slider");
this.inputs.off(".zf.slider");
this.$element.off(".zf.slider");
clearTimeout(this.timeout);
Foundation.unregisterPlugin(this)
}}]);
return l
}();
a.defaults={start:0,end:100,step:1,initialStart:0,initialEnd:100,binding:false,clickSelect:true,vertical:false,draggable:true,disabled:false,doubleSided:false,decimal:2,moveTime:200,disabledClass:"disabled",invertVertical:false,changedDelay:500,nonLinearBase:5,positionValueFunction:"linear"};
function c(g,f){return g/f
}function b(k,f,g,h){return Math.abs(k.position()[f]+k[h]()/2-g)
}function e(g,f){return Math.log(f)/Math.log(g)
}Foundation.plugin(a,"Slider")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(c){var b=function(){function d(r,q){_classCallCheck(this,d);
this.$element=r;
this.options=c.extend({},d.defaults,this.$element.data(),q);
this._init();
Foundation.registerPlugin(this,"Sticky")
}_createClass(d,[{key:"_init",value:function n(){var q=this.$element.parent("[data-sticky-container]"),s=this.$element[0].id||Foundation.GetYoDigits(6,"sticky"),r=this;
if(!q.length){this.wasWrapped=true
}this.$container=q.length?q:c(this.options.container).wrapInner(this.$element);
this.$container.addClass(this.options.containerClass);
this.$element.addClass(this.options.stickyClass).attr({"data-resize":s});
this.scrollCount=this.options.checkEvery;
this.isStuck=false;
c(window).one("load.zf.sticky",function(){r.containerHeight=r.$element.css("display")=="none"?0:r.$element[0].getBoundingClientRect().height;
r.$container.css("height",r.containerHeight);
r.elemHeight=r.containerHeight;
if(r.options.anchor!==""){r.$anchor=c("#"+r.options.anchor)
}else{r._parsePoints()
}r._setSizes(function(){var t=window.pageYOffset;
r._calc(false,t);
if(!r.isStuck){r._removeSticky(t>=r.topPoint?false:true)
}});
r._events(s.split("-").reverse().join("-"))
})
}},{key:"_parsePoints",value:function f(){var w=this.options.topAnchor==""?1:this.options.topAnchor,q=this.options.btmAnchor==""?document.documentElement.scrollHeight:this.options.btmAnchor,x=[w,q],v={};
for(var t=0,u=x.length;
t<u&&x[t];
t++){var y;
if(typeof x[t]==="number"){y=x[t]
}else{var r=x[t].split(":"),s=c("#"+r[0]);
y=s.offset().top;
if(r[1]&&r[1].toLowerCase()==="bottom"){y+=s[0].getBoundingClientRect().height
}}v[t]=y
}this.points=v;
return
}},{key:"_events",value:function k(s){var r=this,q=this.scrollListener="scroll.zf."+s;
if(this.isOn){return
}if(this.canStick){this.isOn=true;
c(window).off(q).on(q,function(t){if(r.scrollCount===0){r.scrollCount=r.options.checkEvery;
r._setSizes(function(){r._calc(false,window.pageYOffset)
})
}else{r.scrollCount--;
r._calc(false,window.pageYOffset)
}})
}this.$element.off("resizeme.zf.trigger").on("resizeme.zf.trigger",function(u,t){r._setSizes(function(){r._calc(false);
if(r.canStick){if(!r.isOn){r._events(s)
}}else{if(r.isOn){r._pauseListeners(q)
}}})
})
}},{key:"_pauseListeners",value:function h(q){this.isOn=false;
c(window).off(q);
this.$element.trigger("pause.zf.sticky")
}},{key:"_calc",value:function e(r,q){if(r){this._setSizes()
}if(!this.canStick){if(this.isStuck){this._removeSticky(true)
}return false
}if(!q){q=window.pageYOffset
}if(q>=this.topPoint){if(q<=this.bottomPoint){if(!this.isStuck){this._setSticky()
}}else{if(this.isStuck){this._removeSticky(false)
}}}else{if(this.isStuck){this._removeSticky(true)
}}}},{key:"_setSticky",value:function m(){var u=this,t=this.options.stickTo,s=t==="top"?"marginTop":"marginBottom",q=t==="top"?"bottom":"top",r={};
r[s]=this.options[s]+"em";
r[t]=0;
r[q]="auto";
this.isStuck=true;
this.$element.removeClass("is-anchored is-at-"+q).addClass("is-stuck is-at-"+t).css(r).trigger("sticky.zf.stuckto:"+t);
this.$element.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",function(){u._setSizes()
})
}},{key:"_removeSticky",value:function p(s){var w=this.options.stickTo,v=w==="top",t={},q=(this.points?this.points[1]-this.points[0]:this.anchorHeight)-this.elemHeight,u=v?"marginTop":"marginBottom",r=v?"bottom":"top",x=s?"top":"bottom";
t[u]=0;
t.bottom="auto";
if(s){t.top=0
}else{t.top=q
}this.isStuck=false;
this.$element.removeClass("is-stuck is-at-"+w).addClass("is-anchored is-at-"+x).css(t).trigger("sticky.zf.unstuckfrom:"+x)
}},{key:"_setSizes",value:function l(r){this.canStick=Foundation.MediaQuery.is(this.options.stickyOn);
if(!this.canStick){if(r&&typeof r==="function"){r()
}}var x=this,w=this.$container[0].getBoundingClientRect().width,t=window.getComputedStyle(this.$container[0]),u=parseInt(t["padding-left"],10),s=parseInt(t["padding-right"],10);
if(this.$anchor&&this.$anchor.length){this.anchorHeight=this.$anchor[0].getBoundingClientRect().height
}else{this._parsePoints()
}this.$element.css({"max-width":w-u-s+"px"});
var v=this.$element[0].getBoundingClientRect().height||this.containerHeight;
if(this.$element.css("display")=="none"){v=0
}this.containerHeight=v;
this.$container.css({height:v});
this.elemHeight=v;
if(!this.isStuck){if(this.$element.hasClass("is-at-bottom")){var q=(this.points?this.points[1]-this.$container.offset().top:this.anchorHeight)-this.elemHeight;
this.$element.css("top",q)
}}this._setBreakPoints(v,function(){if(r&&typeof r==="function"){r()
}})
}},{key:"_setBreakPoints",value:function g(u,r){if(!this.canStick){if(r&&typeof r==="function"){r()
}else{return false
}}var w=a(this.options.marginTop),v=a(this.options.marginBottom),t=this.points?this.points[0]:this.$anchor.offset().top,q=this.points?this.points[1]:t+this.anchorHeight,s=window.innerHeight;
if(this.options.stickTo==="top"){t-=w;
q-=u+w
}else{if(this.options.stickTo==="bottom"){t-=s-(u+v);
q-=s-v
}else{}}this.topPoint=t;
this.bottomPoint=q;
if(r&&typeof r==="function"){r()
}}},{key:"destroy",value:function o(){this._removeSticky(true);
this.$element.removeClass(this.options.stickyClass+" is-anchored is-at-top").css({height:"",top:"",bottom:"","max-width":""}).off("resizeme.zf.trigger");
if(this.$anchor&&this.$anchor.length){this.$anchor.off("change.zf.sticky")
}c(window).off(this.scrollListener);
if(this.wasWrapped){this.$element.unwrap()
}else{this.$container.removeClass(this.options.containerClass).css({height:""})
}Foundation.unregisterPlugin(this)
}}]);
return d
}();
b.defaults={container:"<div data-sticky-container></div>",stickTo:"top",anchor:"",topAnchor:"",btmAnchor:"",marginTop:1,marginBottom:1,stickyOn:"medium",stickyClass:"sticky",containerClass:"sticky-container",checkEvery:-1};
function a(d){return parseInt(window.getComputedStyle(document.body,null).fontSize,10)*d
}Foundation.plugin(b,"Sticky")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function c(q,p){_classCallCheck(this,c);
this.$element=q;
this.options=b.extend({},c.defaults,this.$element.data(),p);
this._init();
Foundation.registerPlugin(this,"Tabs");
Foundation.Keyboard.register("Tabs",{ENTER:"open",SPACE:"open",ARROW_RIGHT:"next",ARROW_UP:"previous",ARROW_DOWN:"next",ARROW_LEFT:"previous"})
}_createClass(c,[{key:"_init",value:function l(){var q=this;
var r=this;
this.$element.attr({role:"tablist"});
this.$tabTitles=this.$element.find("."+this.options.linkClass);
this.$tabContent=b('[data-tabs-content="'+this.$element[0].id+'"]');
this.$tabTitles.each(function(){var t=b(this),s=t.find("a"),v=t.hasClass(""+r.options.linkActiveClass),w=s[0].hash.slice(1),x=s[0].id?s[0].id:w+"-label",u=b("#"+w);
t.attr({role:"presentation"});
s.attr({role:"tab","aria-controls":w,"aria-selected":v,id:x});
u.attr({role:"tabpanel","aria-hidden":!v,"aria-labelledby":x});
if(v&&r.options.autoFocus){b(window).load(function(){b("html, body").animate({scrollTop:t.offset().top},r.options.deepLinkSmudgeDelay,function(){s.focus()
})
})
}});
if(this.options.matchHeight){var p=this.$tabContent.find("img");
if(p.length){Foundation.onImagesLoaded(p,this._setHeight.bind(this))
}else{this._setHeight()
}}this._checkDeepLink=function(){var t=window.location.hash;
if(t.length){var s=q.$element.find('[href="'+t+'"]');
if(s.length){q.selectTab(b(t),true);
if(q.options.deepLinkSmudge){var u=q.$element.offset();
b("html, body").animate({scrollTop:u.top},q.options.deepLinkSmudgeDelay)
}q.$element.trigger("deeplink.zf.tabs",[s,b(t)])
}}};
if(this.options.deepLink){this._checkDeepLink()
}this._events()
}},{key:"_events",value:function g(){this._addKeyHandler();
this._addClickHandler();
this._setHeightMqHandler=null;
if(this.options.matchHeight){this._setHeightMqHandler=this._setHeight.bind(this);
b(window).on("changed.zf.mediaquery",this._setHeightMqHandler)
}if(this.options.deepLink){b(window).on("popstate",this._checkDeepLink)
}}},{key:"_addClickHandler",value:function m(){var p=this;
this.$element.off("click.zf.tabs").on("click.zf.tabs","."+this.options.linkClass,function(q){q.preventDefault();
q.stopPropagation();
p._handleTabChange(b(this))
})
}},{key:"_addKeyHandler",value:function e(){var p=this;
this.$tabTitles.off("keydown.zf.tabs").on("keydown.zf.tabs",function(u){if(u.which===9){return
}var r=b(this),s=r.parent("ul").children("li"),t,q;
s.each(function(v){if(b(this).is(r)){if(p.options.wrapOnKeys){t=v===0?s.last():s.eq(v-1);
q=v===s.length-1?s.first():s.eq(v+1)
}else{t=s.eq(Math.max(0,v-1));
q=s.eq(Math.min(v+1,s.length-1))
}return
}});
Foundation.Keyboard.handleKey(u,"Tabs",{open:function(){r.find('[role="tab"]').focus();
p._handleTabChange(r)
},previous:function(){t.find('[role="tab"]').focus();
p._handleTabChange(t)
},next:function(){q.find('[role="tab"]').focus();
p._handleTabChange(q)
},handled:function(){u.stopPropagation();
u.preventDefault()
}})
})
}},{key:"_handleTabChange",value:function o(q,t){if(q.hasClass(""+this.options.linkActiveClass)){if(this.options.activeCollapse){this._collapseTab(q);
this.$element.trigger("collapse.zf.tabs",[q])
}return
}var r=this.$element.find("."+this.options.linkClass+"."+this.options.linkActiveClass),u=q.find('[role="tab"]'),v=u[0].hash,p=this.$tabContent.find(v);
this._collapseTab(r);
this._openTab(q);
if(this.options.deepLink&&!t){var s=q.find("a").attr("href");
if(this.options.updateHistory){history.pushState({},"",s)
}else{history.replaceState({},"",s)
}}this.$element.trigger("change.zf.tabs",[q,p]);
p.find("[data-mutate]").trigger("mutateme.zf.trigger")
}},{key:"_openTab",value:function f(q){var r=q.find('[role="tab"]'),s=r[0].hash,p=this.$tabContent.find(s);
q.addClass(""+this.options.linkActiveClass);
r.attr({"aria-selected":"true"});
p.addClass(""+this.options.panelActiveClass).attr({"aria-hidden":"false"})
}},{key:"_collapseTab",value:function d(p){var q=p.removeClass(""+this.options.linkActiveClass).find('[role="tab"]').attr({"aria-selected":"false"});
b("#"+q.attr("aria-controls")).removeClass(""+this.options.panelActiveClass).attr({"aria-hidden":"true"})
}},{key:"selectTab",value:function k(s,r){var q;
if(typeof s==="object"){q=s[0].id
}else{q=s
}if(q.indexOf("#")<0){q="#"+q
}var p=this.$tabTitles.find('[href="'+q+'"]').parent("."+this.options.linkClass);
this._handleTabChange(p,r)
}},{key:"_setHeight",value:function h(){var p=0,q=this;
this.$tabContent.find("."+this.options.panelClass).css("height","").each(function(){var r=b(this),t=r.hasClass(""+q.options.panelActiveClass);
if(!t){r.css({visibility:"hidden",display:"block"})
}var s=this.getBoundingClientRect().height;
if(!t){r.css({visibility:"",display:""})
}p=s>p?s:p
}).css("height",p+"px")
}},{key:"destroy",value:function n(){this.$element.find("."+this.options.linkClass).off(".zf.tabs").hide().end().find("."+this.options.panelClass).hide();
if(this.options.matchHeight){if(this._setHeightMqHandler!=null){b(window).off("changed.zf.mediaquery",this._setHeightMqHandler)
}}if(this.options.deepLink){b(window).off("popstate",this._checkDeepLink)
}Foundation.unregisterPlugin(this)
}}]);
return c
}();
a.defaults={deepLink:false,deepLinkSmudge:false,deepLinkSmudgeDelay:300,updateHistory:false,autoFocus:false,wrapOnKeys:true,matchHeight:false,activeCollapse:false,linkClass:"tabs-title",linkActiveClass:"is-active",panelClass:"tabs-panel",panelActiveClass:"is-active"};
Foundation.plugin(a,"Tabs")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function g(n,m){_classCallCheck(this,g);
this.$element=n;
this.options=b.extend({},g.defaults,n.data(),m);
this.className="";
this._init();
this._events();
Foundation.registerPlugin(this,"Toggler")
}_createClass(g,[{key:"_init",value:function h(){var m;
if(this.options.animate){m=this.options.animate.split(" ");
this.animationIn=m[0];
this.animationOut=m[1]||null
}else{m=this.$element.data("toggler");
this.className=m[0]==="."?m.slice(1):m
}var n=this.$element[0].id;
b('[data-open="'+n+'"], [data-close="'+n+'"], [data-toggle="'+n+'"]').attr("aria-controls",n);
this.$element.attr("aria-expanded",this.$element.is(":hidden")?false:true)
}},{key:"_events",value:function k(){this.$element.off("toggle.zf.trigger").on("toggle.zf.trigger",this.toggle.bind(this))
}},{key:"toggle",value:function c(){this[this.options.animate?"_toggleAnimate":"_toggleClass"]()
}},{key:"_toggleClass",value:function l(){this.$element.toggleClass(this.className);
var m=this.$element.hasClass(this.className);
if(m){this.$element.trigger("on.zf.toggler")
}else{this.$element.trigger("off.zf.toggler")
}this._updateARIA(m);
this.$element.find("[data-mutate]").trigger("mutateme.zf.trigger")
}},{key:"_toggleAnimate",value:function d(){var m=this;
if(this.$element.is(":hidden")){Foundation.Motion.animateIn(this.$element,this.animationIn,function(){m._updateARIA(true);
this.trigger("on.zf.toggler");
this.find("[data-mutate]").trigger("mutateme.zf.trigger")
})
}else{Foundation.Motion.animateOut(this.$element,this.animationOut,function(){m._updateARIA(false);
this.trigger("off.zf.toggler");
this.find("[data-mutate]").trigger("mutateme.zf.trigger")
})
}}},{key:"_updateARIA",value:function f(m){this.$element.attr("aria-expanded",m?true:false)
}},{key:"destroy",value:function e(){this.$element.off(".zf.toggler");
Foundation.unregisterPlugin(this)
}}]);
return g
}();
a.defaults={animate:false};
Foundation.plugin(a,"Toggler")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function l(q,p){_classCallCheck(this,l);
this.$element=q;
this.options=b.extend({},l.defaults,this.$element.data(),p);
this.isActive=false;
this.isClick=false;
this._init();
Foundation.registerPlugin(this,"Tooltip")
}_createClass(l,[{key:"_init",value:function k(){var p=this.$element.attr("aria-describedby")||Foundation.GetYoDigits(6,"tooltip");
this.options.positionClass=this.options.positionClass||this._getPositionClass(this.$element);
this.options.tipText=this.options.tipText||this.$element.attr("title");
this.template=this.options.template?b(this.options.template):this._buildTemplate(p);
if(this.options.allowHtml){this.template.appendTo(document.body).html(this.options.tipText).hide()
}else{this.template.appendTo(document.body).text(this.options.tipText).hide()
}this.$element.attr({title:"","aria-describedby":p,"data-yeti-box":p,"data-toggle":p,"data-resize":p}).addClass(this.options.triggerClass);
this.usedPositions=[];
this.counter=4;
this.classChanged=false;
this._events()
}},{key:"_getPositionClass",value:function e(q){if(!q){return""
}var p=q[0].className.match(/\b(top|left|right)\b/g);
p=p?p[0]:"";
return p
}},{key:"_buildTemplate",value:function d(r){var q=(this.options.tooltipClass+" "+this.options.positionClass+" "+this.options.templateClasses).trim();
var p=b("<div></div>").addClass(q).attr({role:"tooltip","aria-hidden":true,"data-is-active":false,"data-is-focus":false,id:r});
return p
}},{key:"_reposition",value:function c(p){this.usedPositions.push(p?p:"bottom");
if(!p&&this.usedPositions.indexOf("top")<0){this.template.addClass("top")
}else{if(p==="top"&&this.usedPositions.indexOf("bottom")<0){this.template.removeClass(p)
}else{if(p==="left"&&this.usedPositions.indexOf("right")<0){this.template.removeClass(p).addClass("right")
}else{if(p==="right"&&this.usedPositions.indexOf("left")<0){this.template.removeClass(p).addClass("left")
}else{if(!p&&this.usedPositions.indexOf("top")>-1&&this.usedPositions.indexOf("left")<0){this.template.addClass("left")
}else{if(p==="top"&&this.usedPositions.indexOf("bottom")>-1&&this.usedPositions.indexOf("left")<0){this.template.removeClass(p).addClass("left")
}else{if(p==="left"&&this.usedPositions.indexOf("right")>-1&&this.usedPositions.indexOf("bottom")<0){this.template.removeClass(p)
}else{if(p==="right"&&this.usedPositions.indexOf("left")>-1&&this.usedPositions.indexOf("bottom")<0){this.template.removeClass(p)
}else{this.template.removeClass(p)
}}}}}}}}this.classChanged=true;
this.counter--
}},{key:"_setPosition",value:function m(){var p=this._getPositionClass(this.template),u=Foundation.Box.GetDimensions(this.template),q=Foundation.Box.GetDimensions(this.$element),r=p==="left"?"left":p==="right"?"left":"top",t=r==="top"?"height":"width",s=t==="height"?this.options.vOffset:this.options.hOffset,v=this;
if(u.width>=u.windowDims.width||!this.counter&&!Foundation.Box.ImNotTouchingYou(this.template)){this.template.offset(Foundation.Box.GetOffsets(this.template,this.$element,"center bottom",this.options.vOffset,this.options.hOffset,true)).css({width:q.windowDims.width-this.options.hOffset*2,height:"auto"});
return false
}this.template.offset(Foundation.Box.GetOffsets(this.template,this.$element,"center "+(p||"bottom"),this.options.vOffset,this.options.hOffset));
while(!Foundation.Box.ImNotTouchingYou(this.template)&&this.counter){this._reposition(p);
this._setPosition()
}}},{key:"show",value:function o(){if(this.options.showOn!=="all"&&!Foundation.MediaQuery.is(this.options.showOn)){return false
}var p=this;
this.template.css("visibility","hidden").show();
this._setPosition();
this.$element.trigger("closeme.zf.tooltip",this.template.attr("id"));
this.template.attr({"data-is-active":true,"aria-hidden":false});
p.isActive=true;
this.template.stop().hide().css("visibility","").fadeIn(this.options.fadeInDuration,function(){});
this.$element.trigger("show.zf.tooltip")
}},{key:"hide",value:function h(){var p=this;
this.template.stop().attr({"aria-hidden":true,"data-is-active":false}).fadeOut(this.options.fadeOutDuration,function(){p.isActive=false;
p.isClick=false;
if(p.classChanged){p.template.removeClass(p._getPositionClass(p.template)).addClass(p.options.positionClass);
p.usedPositions=[];
p.counter=4;
p.classChanged=false
}});
this.$element.trigger("hide.zf.tooltip")
}},{key:"_events",value:function f(){var r=this;
var q=this.template;
var p=false;
if(!this.options.disableHover){this.$element.on("mouseenter.zf.tooltip",function(s){if(!r.isActive){r.timeout=setTimeout(function(){r.show()
},r.options.hoverDelay)
}}).on("mouseleave.zf.tooltip",function(s){clearTimeout(r.timeout);
if(!p||r.isClick&&!r.options.clickOpen){r.hide()
}})
}if(this.options.clickOpen){this.$element.on("mousedown.zf.tooltip",function(s){s.stopImmediatePropagation();
if(r.isClick){}else{r.isClick=true;
if((r.options.disableHover||!r.$element.attr("tabindex"))&&!r.isActive){r.show()
}}})
}else{this.$element.on("mousedown.zf.tooltip",function(s){s.stopImmediatePropagation();
r.isClick=true
})
}if(!this.options.disableForTouch){this.$element.on("tap.zf.tooltip touchend.zf.tooltip",function(s){r.isActive?r.hide():r.show()
})
}this.$element.on({"close.zf.trigger":this.hide.bind(this)});
this.$element.on("focus.zf.tooltip",function(s){p=true;
if(r.isClick){if(!r.options.clickOpen){p=false
}return false
}else{r.show()
}}).on("focusout.zf.tooltip",function(s){p=false;
r.isClick=false;
r.hide()
}).on("resizeme.zf.trigger",function(){if(r.isActive){r._setPosition()
}})
}},{key:"toggle",value:function g(){if(this.isActive){this.hide()
}else{this.show()
}}},{key:"destroy",value:function n(){this.$element.attr("title",this.template.text()).off(".zf.trigger .zf.tooltip").removeClass("has-tip top right left").removeAttr("aria-describedby aria-haspopup data-disable-hover data-resize data-toggle data-tooltip data-yeti-box");
this.template.remove();
Foundation.unregisterPlugin(this)
}}]);
return l
}();
a.defaults={disableForTouch:false,hoverDelay:200,fadeInDuration:150,fadeOutDuration:150,disableHover:false,templateClasses:"",tooltipClass:"tooltip",triggerClass:"has-tip",showOn:"small",template:"",tipText:"",touchCloseText:"Tap to close.",clickOpen:true,positionClass:"",vOffset:10,hOffset:12,allowHtml:false};
Foundation.plugin(a,"Tooltip")
}(jQuery);
"use strict";
var _createClass=function(){function a(e,c){for(var b=0;
b<c.length;
b++){var d=c[b];
d.enumerable=d.enumerable||false;
d.configurable=true;
if("value" in d){d.writable=true
}Object.defineProperty(e,d.key,d)
}}return function(d,b,c){if(b){a(d.prototype,b)
}if(c){a(d,c)
}return d
}
}();
function _classCallCheck(a,b){if(!(a instanceof b)){throw new TypeError("Cannot call a class as a function")
}}!function(b){var a=function(){function e(n,m){_classCallCheck(this,e);
this.$element=b(n);
this.options=b.extend({},this.$element.data(),m);
this.rules=this.$element.data("responsive-accordion-tabs");
this.currentMq=null;
this.currentPlugin=null;
if(!this.$element.attr("id")){this.$element.attr("id",Foundation.GetYoDigits(6,"responsiveaccordiontabs"))
}this._init();
this._events();
Foundation.registerPlugin(this,"ResponsiveAccordionTabs")
}_createClass(e,[{key:"_init",value:function h(){if(typeof this.rules==="string"){var m={};
var r=this.rules.split(" ");
for(var n=0;
n<r.length;
n++){var p=r[n].split("-");
var o=p.length>1?p[0]:"small";
var q=p.length>1?p[1]:p[0];
if(c[q]!==null){m[o]=c[q]
}}this.rules=m
}this._getAllOptions();
if(!b.isEmptyObject(this.rules)){this._checkMediaQueries()
}}},{key:"_getAllOptions",value:function d(){var t=this;
t.allOptions={};
for(var o in c){if(c.hasOwnProperty(o)){var r=c[o];
try{var s=b("<ul></ul>");
var n=new r.plugin(s,t.options);
for(var p in n.options){if(n.options.hasOwnProperty(p)&&p!=="zfPlugin"){var m=n.options[p];
t.allOptions[p]=m
}}n.destroy()
}catch(q){}}}}},{key:"_events",value:function l(){var m=this;
b(window).on("changed.zf.mediaquery",function(){m._checkMediaQueries()
})
}},{key:"_checkMediaQueries",value:function k(){var m,n=this;
b.each(this.rules,function(o){if(Foundation.MediaQuery.atLeast(o)){m=o
}});
if(!m){return
}if(this.currentPlugin instanceof this.rules[m].plugin){return
}b.each(c,function(o,p){n.$element.removeClass(p.cssClass)
});
this.$element.addClass(this.rules[m].cssClass);
if(this.currentPlugin){if(!this.currentPlugin.$element.data("zfPlugin")&&this.storezfData){this.currentPlugin.$element.data("zfPlugin",this.storezfData)
}this.currentPlugin.destroy()
}this._handleMarkup(this.rules[m].cssClass);
this.currentPlugin=new this.rules[m].plugin(this.$element,{});
this.storezfData=this.currentPlugin.$element.data("zfPlugin")
}},{key:"_handleMarkup",value:function g(t){var r=this,p="accordion";
var o=b("[data-tabs-content="+this.$element.attr("id")+"]");
if(o.length){p="tabs"
}if(p===t){return
}var u=r.allOptions.linkClass?r.allOptions.linkClass:"tabs-title";
var n=r.allOptions.panelClass?r.allOptions.panelClass:"tabs-panel";
this.$element.removeAttr("role");
var m=this.$element.children("."+u+",[data-accordion-item]").removeClass(u).removeClass("accordion-item").removeAttr("data-accordion-item");
var v=m.children("a").removeClass("accordion-title");
if(p==="tabs"){o=o.children("."+n).removeClass(n).removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby");
o.children("a").removeAttr("role").removeAttr("aria-controls").removeAttr("aria-selected")
}else{o=m.children("[data-tab-content]").removeClass("accordion-content")
}o.css({display:"",visibility:""});
m.css({display:"",visibility:""});
if(t==="accordion"){o.each(function(w,x){b(x).appendTo(m.get(w)).addClass("accordion-content").attr("data-tab-content","").removeClass("is-active").css({height:""});
b("[data-tabs-content="+r.$element.attr("id")+"]").after('<div id="tabs-placeholder-'+r.$element.attr("id")+'"></div>').remove();
m.addClass("accordion-item").attr("data-accordion-item","");
v.addClass("accordion-title")
})
}else{if(t==="tabs"){var q=b("[data-tabs-content="+r.$element.attr("id")+"]");
var s=b("#tabs-placeholder-"+r.$element.attr("id"));
if(s.length){q=b('<div class="tabs-content"></div>').insertAfter(s).attr("data-tabs-content",r.$element.attr("id"));
s.remove()
}else{q=b('<div class="tabs-content"></div>').insertAfter(r.$element).attr("data-tabs-content",r.$element.attr("id"))
}o.each(function(w,z){var y=b(z).appendTo(q).addClass(n);
var A=v.get(w).hash.slice(1);
var B=b(z).attr("id")||Foundation.GetYoDigits(6,"accordion");
if(A!==B){if(A!==""){b(z).attr("id",A)
}else{A=B;
b(z).attr("id",A);
b(v.get(w)).attr("href",b(v.get(w)).attr("href").replace("#","")+"#"+A)
}}var x=b(m.get(w)).hasClass("is-active");
if(x){y.addClass("is-active")
}});
m.addClass(u)
}}}},{key:"destroy",value:function f(){if(this.currentPlugin){this.currentPlugin.destroy()
}b(window).off(".zf.ResponsiveAccordionTabs");
Foundation.unregisterPlugin(this)
}}]);
return e
}();
a.defaults={};
var c={tabs:{cssClass:"tabs",plugin:Foundation._plugins.tabs||null},accordion:{cssClass:"accordion",plugin:Foundation._plugins.accordion||null}};
Foundation.plugin(a,"ResponsiveAccordionTabs")
}(jQuery);
(function webpackUniversalModuleDefinition(a,b){if(typeof exports==="object"&&typeof module==="object"){module.exports=b()
}else{if(typeof define==="function"&&define.amd){define([],b)
}else{if(typeof exports==="object"){exports.Handlebars=b()
}else{a.Handlebars=b()
}}}})(this,function(){return(function(a){var b={};
function c(e){if(b[e]){return b[e].exports
}var d=b[e]={exports:{},id:e,loaded:false};
a[e].call(d.exports,d,d.exports,c);
d.loaded=true;
return d.exports
}c.m=a;
c.c=b;
c.p="";
return c(0)
})([(function(c,u,e){var a=e(1)["default"];
u.__esModule=true;
var g=e(2);
var d=a(g);
var b=e(35);
var p=a(b);
var h=e(36);
var q=e(41);
var k=e(42);
var l=a(k);
var r=e(39);
var t=a(r);
var n=e(34);
var m=a(n);
var s=d["default"].create;
function o(){var v=s();
v.compile=function(w,x){return q.compile(w,x,v)
};
v.precompile=function(w,x){return q.precompile(w,x,v)
};
v.AST=p["default"];
v.Compiler=q.Compiler;
v.JavaScriptCompiler=l["default"];
v.Parser=h.parser;
v.parse=h.parse;
return v
}var f=o();
f.create=o;
m["default"](f);
f.Visitor=t["default"];
f["default"]=f;
u["default"]=f;
c.exports=u["default"]
}),(function(b,a){a["default"]=function(c){return c&&c.__esModule?c:{"default":c}
};
a.__esModule=true
}),(function(b,u,d){var r=d(3)["default"];
var a=d(1)["default"];
u.__esModule=true;
var t=d(4);
var g=r(t);
var s=d(21);
var h=a(s);
var n=d(6);
var p=a(n);
var o=d(5);
var c=r(o);
var f=d(22);
var q=r(f);
var l=d(34);
var k=a(l);
function m(){var v=new g.HandlebarsEnvironment();
c.extend(v,g);
v.SafeString=h["default"];
v.Exception=p["default"];
v.Utils=c;
v.escapeExpression=c.escapeExpression;
v.VM=q;
v.template=function(w){return q.template(w,v)
};
return v
}var e=m();
e.create=m;
k["default"](e);
e["default"]=e;
u["default"]=e;
b.exports=u["default"]
}),(function(b,a){a["default"]=function(e){if(e&&e.__esModule){return e
}else{var c={};
if(e!=null){for(var d in e){if(Object.prototype.hasOwnProperty.call(e,d)){c[d]=e[d]
}}}c["default"]=e;
return c
}};
a.__esModule=true
}),(function(e,x,g){var c=g(1)["default"];
x.__esModule=true;
x.HandlebarsEnvironment=m;
var p=g(5);
var v=g(6);
var f=c(v);
var h=g(10);
var y=g(18);
var o=g(20);
var d=c(o);
var w="4.0.11";
x.VERSION=w;
var s=7;
x.COMPILER_REVISION=s;
var u={1:"<= 1.0.rc.2",2:"== 1.0.0-rc.3",3:"== 1.0.0-rc.4",4:"== 1.x.x",5:"== 2.0.0-alpha.x",6:">= 2.0.0-beta.1",7:">= 4.0.0"};
x.REVISION_CHANGES=u;
var r="[object Object]";
function m(B,A,z){this.helpers=B||{};
this.partials=A||{};
this.decorators=z||{};
h.registerDefaultHelpers(this);
y.registerDefaultDecorators(this)
}m.prototype={constructor:m,logger:d["default"],log:d["default"].log,registerHelper:function q(z,A){if(p.toString.call(z)===r){if(A){throw new f["default"]("Arg not supported with multiple helpers")
}p.extend(this.helpers,z)
}else{this.helpers[z]=A
}},unregisterHelper:function k(z){delete this.helpers[z]
},registerPartial:function l(A,z){if(p.toString.call(A)===r){p.extend(this.partials,A)
}else{if(typeof z==="undefined"){throw new f["default"]('Attempting to register a partial called "'+A+'" as undefined')
}this.partials[A]=z
}},unregisterPartial:function t(z){delete this.partials[z]
},registerDecorator:function b(z,A){if(p.toString.call(z)===r){if(A){throw new f["default"]("Arg not supported with multiple decorators")
}p.extend(this.decorators,z)
}else{this.decorators[z]=A
}},unregisterDecorator:function a(z){delete this.decorators[z]
}};
var n=d["default"].log;
x.log=n;
x.createFrame=p.createFrame;
x.logger=d["default"]
}),(function(d,g){g.__esModule=true;
g.extend=n;
g.indexOf=o;
g.escapeExpression=k;
g.isEmpty=h;
g.createFrame=q;
g.blockParams=m;
g.appendContextPath=e;
var p={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;","=":"&#x3D;"};
var a=/[&<>"'`=]/g,f=/[&<>"'`=]/;
function r(s){return p[s]
}function n(u){for(var t=1;
t<arguments.length;
t++){for(var s in arguments[t]){if(Object.prototype.hasOwnProperty.call(arguments[t],s)){u[s]=arguments[t][s]
}}}return u
}var c=Object.prototype.toString;
g.toString=c;
var b=function b(s){return typeof s==="function"
};
if(b(/x/)){g.isFunction=b=function(s){return typeof s==="function"&&c.call(s)==="[object Function]"
}
}g.isFunction=b;
var l=Array.isArray||function(s){return s&&typeof s==="object"?c.call(s)==="[object Array]":false
};
g.isArray=l;
function o(v,u){for(var t=0,s=v.length;
t<s;
t++){if(v[t]===u){return t
}}return -1
}function k(s){if(typeof s!=="string"){if(s&&s.toHTML){return s.toHTML()
}else{if(s==null){return""
}else{if(!s){return s+""
}}}s=""+s
}if(!f.test(s)){return s
}return s.replace(a,r)
}function h(s){if(!s&&s!==0){return true
}else{if(l(s)&&s.length===0){return true
}else{return false
}}}function q(s){var t=n({},s);
t._parent=s;
return t
}function m(t,s){t.path=s;
return t
}function e(s,t){return(s?s+".":"")+t
}}),(function(d,a,e){var c=e(7)["default"];
a.__esModule=true;
var f=["description","fileName","lineNumber","message","name","number","stack"];
function b(n,m){var o=m&&m.loc,h=undefined,l=undefined;
if(o){h=o.start.line;
l=o.start.column;
n+=" - "+h+":"+l
}var k=Error.prototype.constructor.call(this,n);
for(var g=0;
g<f.length;
g++){this[f[g]]=k[f[g]]
}if(Error.captureStackTrace){Error.captureStackTrace(this,b)
}try{if(o){this.lineNumber=h;
if(c){Object.defineProperty(this,"column",{value:l,enumerable:true})
}else{this.column=l
}}}catch(p){}}b.prototype=new Error();
a["default"]=b;
d.exports=a["default"]
}),(function(b,a,c){b.exports={"default":c(8),__esModule:true}
}),(function(c,b,e){var d=e(9);
c.exports=function a(g,f,h){return d.setDesc(g,f,h)
}
}),(function(c,a){var b=Object;
c.exports={create:b.create,getProto:b.getPrototypeOf,isEnum:{}.propertyIsEnumerable,getDesc:b.getOwnPropertyDescriptor,setDesc:b.defineProperty,setDescs:b.defineProperties,getKeys:b.keys,getNames:b.getOwnPropertyNames,getSymbols:b.getOwnPropertySymbols,each:[].forEach}
}),(function(e,u,f){var c=f(1)["default"];
u.__esModule=true;
u.registerDefaultHelpers=q;
var a=f(11);
var o=c(a);
var m=f(12);
var d=c(m);
var g=f(13);
var t=c(g);
var k=f(14);
var b=c(k);
var p=f(15);
var r=c(p);
var n=f(16);
var h=c(n);
var l=f(17);
var s=c(l);
function q(v){o["default"](v);
d["default"](v);
t["default"](v);
b["default"](v);
r["default"](v);
h["default"](v);
s["default"](v)
}}),(function(b,a,d){a.__esModule=true;
var c=d(5);
a["default"]=function(e){e.registerHelper("blockHelperMissing",function(h,g){var f=g.inverse,k=g.fn;
if(h===true){return k(this)
}else{if(h===false||h==null){return f(this)
}else{if(c.isArray(h)){if(h.length>0){if(g.ids){g.ids=[g.name]
}return e.helpers.each(h,g)
}else{return f(this)
}}else{if(g.data&&g.ids){var l=c.createFrame(g.data);
l.contextPath=c.appendContextPath(g.data.contextPath,g.name);
g={data:l}
}return k(h,g)
}}}})
};
b.exports=a["default"]
}),(function(c,a,f){var d=f(1)["default"];
a.__esModule=true;
var e=f(5);
var b=f(6);
var g=d(b);
a["default"]=function(h){h.registerHelper("each",function(k,v){if(!v){throw new g["default"]("Must pass iterator to #each")
}var t=v.fn,o=v.inverse,q=0,s="",p=undefined,l=undefined;
if(v.data&&v.ids){l=e.appendContextPath(v.data.contextPath,v.ids[0])+"."
}if(e.isFunction(k)){k=k.call(this)
}if(v.data){p=e.createFrame(v.data)
}function m(y,w,x){if(p){p.key=y;
p.index=w;
p.first=w===0;
p.last=!!x;
if(l){p.contextPath=l+y
}}s=s+t(k[y],{data:p,blockParams:e.blockParams([k[y],y],[l+y,null])})
}if(k&&typeof k==="object"){if(e.isArray(k)){for(var n=k.length;
q<n;
q++){if(q in k){m(q,q,q===k.length-1)
}}}else{var r=undefined;
for(var u in k){if(k.hasOwnProperty(u)){if(r!==undefined){m(r,q-1)
}r=u;
q++
}}if(r!==undefined){m(r,q-1,true)
}}}if(q===0){s=o(this)
}return s
})
};
c.exports=a["default"]
}),(function(c,a,e){var d=e(1)["default"];
a.__esModule=true;
var b=e(6);
var f=d(b);
a["default"]=function(g){g.registerHelper("helperMissing",function(){if(arguments.length===1){return undefined
}else{throw new f["default"]('Missing helper: "'+arguments[arguments.length-1].name+'"')
}})
};
c.exports=a["default"]
}),(function(b,a,d){a.__esModule=true;
var c=d(5);
a["default"]=function(e){e.registerHelper("if",function(g,f){if(c.isFunction(g)){g=g.call(this)
}if(!f.hash.includeZero&&!g||c.isEmpty(g)){return f.inverse(this)
}else{return f.fn(this)
}});
e.registerHelper("unless",function(g,f){return e.helpers["if"].call(this,g,{fn:f.inverse,inverse:f.fn,hash:f.hash})
})
};
b.exports=a["default"]
}),(function(b,a){a.__esModule=true;
a["default"]=function(c){c.registerHelper("log",function(){var e=[undefined],d=arguments[arguments.length-1];
for(var f=0;
f<arguments.length-1;
f++){e.push(arguments[f])
}var g=1;
if(d.hash.level!=null){g=d.hash.level
}else{if(d.data&&d.data.level!=null){g=d.data.level
}}e[0]=g;
c.log.apply(c,e)
})
};
b.exports=a["default"]
}),(function(b,a){a.__esModule=true;
a["default"]=function(c){c.registerHelper("lookup",function(e,d){return e&&e[d]
})
};
b.exports=a["default"]
}),(function(b,a,d){a.__esModule=true;
var c=d(5);
a["default"]=function(e){e.registerHelper("with",function(g,f){if(c.isFunction(g)){g=g.call(this)
}var h=f.fn;
if(!c.isEmpty(g)){var k=f.data;
if(f.data&&f.ids){k=c.createFrame(f.data);
k.contextPath=c.appendContextPath(f.data.contextPath,f.ids[0])
}return h(g,{data:k,blockParams:c.blockParams([g],[k&&k.contextPath])})
}else{return f.inverse(this)
}})
};
b.exports=a["default"]
}),(function(d,c,g){var f=g(1)["default"];
c.__esModule=true;
c.registerDefaultDecorators=e;
var b=g(19);
var a=f(b);
function e(h){a["default"](h)
}}),(function(b,a,d){a.__esModule=true;
var c=d(5);
a["default"]=function(e){e.registerDecorator("inline",function(l,k,f,h){var g=l;
if(!k.partials){k.partials={};
g=function(p,n){var o=f.partials;
f.partials=c.extend({},o,k.partials);
var m=l(p,n);
f.partials=o;
return m
}
}k.partials[h.args[0]]=h.fn;
return g
})
};
b.exports=a["default"]
}),(function(c,a,f){a.__esModule=true;
var e=f(5);
var b={methodMap:["debug","info","warn","error"],level:"info",lookupLevel:function g(k){if(typeof k==="string"){var h=e.indexOf(b.methodMap,k.toLowerCase());
if(h>=0){k=h
}else{k=parseInt(k,10)
}}return k
},log:function d(n){n=b.lookupLevel(n);
if(typeof console!=="undefined"&&b.lookupLevel(b.level)<=n){var m=b.methodMap[n];
if(!console[m]){m="log"
}for(var h=arguments.length,l=Array(h>1?h-1:0),k=1;
k<h;
k++){l[k-1]=arguments[k]
}console[m].apply(console,l)
}}};
a["default"]=b;
c.exports=a["default"]
}),(function(b,a){a.__esModule=true;
function c(d){this.string=d
}c.prototype.toString=c.prototype.toHTML=function(){return""+this.string
};
a["default"]=c;
b.exports=a["default"]
}),(function(b,t,e){var m=e(23)["default"];
var s=e(3)["default"];
var a=e(1)["default"];
t.__esModule=true;
t.checkRevision=n;
t.template=r;
t.wrapProgram=h;
t.resolvePartial=l;
t.invokePartial=u;
t.noop=f;
var k=e(5);
var d=s(k);
var o=e(6);
var c=a(o);
var p=e(4);
function n(x){var w=x&&x[0]||1,z=p.COMPILER_REVISION;
if(w!==z){if(w<z){var v=p.REVISION_CHANGES[z],y=p.REVISION_CHANGES[w];
throw new c["default"]("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version ("+v+") or downgrade your runtime to an older version ("+y+").")
}else{throw new c["default"]("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version ("+x[1]+").")
}}}function r(F,z){if(!z){throw new c["default"]("No environment passed to template")
}if(!F||!F.main){throw new c["default"]("Unknown template object: "+typeof F)
}F.main.decorator=F.main_d;
z.VM.checkRevision(F.compiler);
function G(K,N,L){if(L.hash){N=d.extend({},N,L.hash);
if(L.ids){L.ids[0]=true
}}K=z.VM.resolvePartial.call(this,K,N,L);
var H=z.VM.invokePartial.call(this,K,N,L);
if(H==null&&z.compile){L.partials[L.name]=z.compile(K,F.compilerOptions,z);
H=L.partials[L.name](N,L)
}if(H!=null){if(L.indent){var J=H.split("\n");
for(var M=0,I=J.length;
M<I;
M++){if(!J[M]&&M+1===I){break
}J[M]=L.indent+J[M]
}H=J.join("\n")
}return H
}else{throw new c["default"]("The partial "+L.name+" could not be compiled when running in runtime-only mode")
}}var v={strict:function E(I,H){if(!(H in I)){throw new c["default"]('"'+H+'" not defined in '+I)
}return I[H]
},lookup:function w(K,I){var H=K.length;
for(var J=0;
J<H;
J++){if(K[J]&&K[J][I]!=null){return K[J][I]
}}},lambda:function A(I,H){return typeof I==="function"?I.call(H):I
},escapeExpression:d.escapeExpression,invokePartial:G,fn:function D(I){var H=F[I];
H.decorator=F[I+"_d"];
return H
},programs:[],program:function y(J,M,I,L,N){var H=this.programs[J],K=this.fn(J);
if(M||N||L||I){H=h(this,J,K,M,I,L,N)
}else{if(!H){H=this.programs[J]=h(this,J,K)
}}return H
},data:function x(H,I){while(H&&I--){H=H._parent
}return H
},merge:function C(J,H){var I=J||H;
if(J&&H&&J!==H){I=d.extend({},H,J)
}return I
},nullContext:m({}),noop:z.VM.noop,compilerInfo:F.compiler};
function B(J){var I=arguments.length<=1||arguments[1]===undefined?{}:arguments[1];
var L=I.data;
B._setup(I);
if(!I.partial&&F.useData){L=q(J,L)
}var M=undefined,K=F.useBlockParams?[]:undefined;
if(F.useDepths){if(I.depths){M=J!=I.depths[0]?[J].concat(I.depths):I.depths
}else{M=[J]
}}function H(N){return""+F.main(v,N,v.helpers,v.partials,L,K,M)
}H=g(F.main,H,v,I.depths||[],L,K);
return H(J,I)
}B.isTop=true;
B._setup=function(H){if(!H.partial){v.helpers=v.merge(H.helpers,z.helpers);
if(F.usePartial){v.partials=v.merge(H.partials,z.partials)
}if(F.usePartial||F.useDecorators){v.decorators=v.merge(H.decorators,z.decorators)
}}else{v.helpers=H.helpers;
v.partials=H.partials;
v.decorators=H.decorators
}};
B._child=function(H,J,I,K){if(F.useBlockParams&&!I){throw new c["default"]("must pass block params")
}if(F.useDepths&&!K){throw new c["default"]("must pass parent depths")
}return h(v,H,F[H],J,0,I,K)
};
return B
}function h(v,x,y,A,w,z,C){function B(E){var D=arguments.length<=1||arguments[1]===undefined?{}:arguments[1];
var F=C;
if(C&&E!=C[0]&&!(E===v.nullContext&&C[0]===null)){F=[E].concat(C)
}return y(v,E,v.helpers,v.partials,D.data||A,z&&[D.blockParams].concat(z),F)
}B=g(y,B,v,C,A,z);
B.program=x;
B.depth=C?C.length:0;
B.blockParams=w||0;
return B
}function l(v,x,w){if(!v){if(w.name==="@partial-block"){v=w.data["partial-block"]
}else{v=w.partials[w.name]
}}else{if(!v.call&&!w.name){w.name=v;
v=w.partials[v]
}}return v
}function u(v,x,w){var z=w.data&&w.data["partial-block"];
w.partial=true;
if(w.ids){w.data.contextPath=w.ids[0]||w.data.contextPath
}var y=undefined;
if(w.fn&&w.fn!==f){(function(){w.data=p.createFrame(w.data);
var B=w.fn;
y=w.data["partial-block"]=function A(D){var C=arguments.length<=1||arguments[1]===undefined?{}:arguments[1];
C.data=p.createFrame(C.data);
C.data["partial-block"]=z;
return B(D,C)
};
if(B.partials){w.partials=d.extend({},w.partials,B.partials)
}})()
}if(v===undefined&&y){v=y
}if(v===undefined){throw new c["default"]("The partial "+w.name+" could not be found")
}else{if(v instanceof Function){return v(x,w)
}}}function f(){return""
}function q(v,w){if(!w||!("root" in w)){w=w?p.createFrame(w):{};
w.root=v
}return w
}function g(x,A,v,B,z,y){if(x.decorator){var w={};
A=x.decorator(A,w,v,B&&B[0],z,y,B);
d.extend(A,w)
}return A
}}),(function(b,a,c){b.exports={"default":c(24),__esModule:true}
}),(function(b,a,c){c(25);
b.exports=c(30).Object.seal
}),(function(c,b,d){var a=d(26);
d(27)("seal",function(e){return function f(g){return e&&a(g)?e(g):g
}
})
}),(function(b,a){b.exports=function(c){return typeof c==="object"?c!==null:typeof c==="function"
}
}),(function(d,c,e){var f=e(28),b=e(30),a=e(33);
d.exports=function(h,g){var k=(b.Object||{})[h]||Object[h],l={};
l[h]=g(k);
f(f.S+f.F*a(function(){k(1)
}),"Object",l)
}
}),(function(e,d,g){var f=g(29),c=g(30),b=g(31),a="prototype";
var h=function(u,m,k){var l=u&h.F,o=u&h.G,s=u&h.S,n=u&h.P,r=u&h.B,w=u&h.W,q=o?c:c[m]||(c[m]={}),t=o?f:s?f[m]:(f[m]||{})[a],v,x,p;
if(o){k=m
}for(v in k){x=!l&&t&&v in t;
if(x&&v in q){continue
}p=x?t[v]:k[v];
q[v]=o&&typeof t[v]!="function"?k[v]:r&&x?b(p,f):w&&t[v]==p?(function(z){var y=function(A){return this instanceof z?new z(A):z(A)
};
y[a]=z[a];
return y
})(p):n&&typeof p=="function"?b(Function.call,p):p;
if(n){(q[a]||(q[a]={}))[v]=p
}}};
h.F=1;
h.G=2;
h.S=4;
h.P=8;
h.B=16;
h.W=32;
e.exports=h
}),(function(b,a){var c=b.exports=typeof window!="undefined"&&window.Math==Math?window:typeof self!="undefined"&&self.Math==Math?self:Function("return this")();
if(typeof __g=="number"){__g=c
}}),(function(c,b){var a=c.exports={version:"1.2.6"};
if(typeof __e=="number"){__e=a
}}),(function(c,b,d){var a=d(32);
c.exports=function(e,g,f){a(e);
if(g===undefined){return e
}switch(f){case 1:return function(h){return e.call(g,h)
};
case 2:return function(k,h){return e.call(g,k,h)
};
case 3:return function(k,h,l){return e.call(g,k,h,l)
}
}return function(){return e.apply(g,arguments)
}
}
}),(function(b,a){b.exports=function(c){if(typeof c!="function"){throw TypeError(c+" is not a function!")
}return c
}
}),(function(b,a){b.exports=function(c){try{return !!c()
}catch(d){return true
}}
}),(function(b,a){(function(c){a.__esModule=true;
a["default"]=function(f){var d=typeof c!=="undefined"?c:window,e=d.Handlebars;
f.noConflict=function(){if(d.Handlebars===f){d.Handlebars=e
}return f
}
};
b.exports=a["default"]
}.call(a,(function(){return this
}())))
}),(function(c,a){a.__esModule=true;
var e={helpers:{helperExpression:function d(g){return g.type==="SubExpression"||(g.type==="MustacheStatement"||g.type==="BlockStatement")&&!!(g.params&&g.params.length||g.hash)
},scopedId:function f(g){return(/^\.|this\b/.test(g.original))
},simpleId:function b(g){return g.parts.length===1&&!e.helpers.scopedId(g)&&!g.depth
}}};
a["default"]=e;
c.exports=a["default"]
}),(function(c,e,b){var o=b(1)["default"];
var g=b(3)["default"];
e.__esModule=true;
e.parse=d;
var h=b(37);
var k=o(h);
var f=b(38);
var n=o(f);
var a=b(40);
var m=g(a);
var p=b(5);
e.parser=k["default"];
var l={};
p.extend(l,m);
function d(q,r){if(q.type==="Program"){return q
}k["default"].yy=l;
l.locInfo=function(t){return new l.SourceLocation(r&&r.srcName,t)
};
var s=new n["default"](r);
return s.accept(k["default"].parse(q))
}}),(function(c,b){b.__esModule=true;
var a=(function(){var l={trace:function f(){},yy:{},symbols_:{error:2,root:3,program:4,EOF:5,program_repetition0:6,statement:7,mustache:8,block:9,rawBlock:10,partial:11,partialBlock:12,content:13,COMMENT:14,CONTENT:15,openRawBlock:16,rawBlock_repetition_plus0:17,END_RAW_BLOCK:18,OPEN_RAW_BLOCK:19,helperName:20,openRawBlock_repetition0:21,openRawBlock_option0:22,CLOSE_RAW_BLOCK:23,openBlock:24,block_option0:25,closeBlock:26,openInverse:27,block_option1:28,OPEN_BLOCK:29,openBlock_repetition0:30,openBlock_option0:31,openBlock_option1:32,CLOSE:33,OPEN_INVERSE:34,openInverse_repetition0:35,openInverse_option0:36,openInverse_option1:37,openInverseChain:38,OPEN_INVERSE_CHAIN:39,openInverseChain_repetition0:40,openInverseChain_option0:41,openInverseChain_option1:42,inverseAndProgram:43,INVERSE:44,inverseChain:45,inverseChain_option0:46,OPEN_ENDBLOCK:47,OPEN:48,mustache_repetition0:49,mustache_option0:50,OPEN_UNESCAPED:51,mustache_repetition1:52,mustache_option1:53,CLOSE_UNESCAPED:54,OPEN_PARTIAL:55,partialName:56,partial_repetition0:57,partial_option0:58,openPartialBlock:59,OPEN_PARTIAL_BLOCK:60,openPartialBlock_repetition0:61,openPartialBlock_option0:62,param:63,sexpr:64,OPEN_SEXPR:65,sexpr_repetition0:66,sexpr_option0:67,CLOSE_SEXPR:68,hash:69,hash_repetition_plus0:70,hashSegment:71,ID:72,EQUALS:73,blockParams:74,OPEN_BLOCK_PARAMS:75,blockParams_repetition_plus0:76,CLOSE_BLOCK_PARAMS:77,path:78,dataName:79,STRING:80,NUMBER:81,BOOLEAN:82,UNDEFINED:83,NULL:84,DATA:85,pathSegments:86,SEP:87,"$accept":0,"$end":1},terminals_:{2:"error",5:"EOF",14:"COMMENT",15:"CONTENT",18:"END_RAW_BLOCK",19:"OPEN_RAW_BLOCK",23:"CLOSE_RAW_BLOCK",29:"OPEN_BLOCK",33:"CLOSE",34:"OPEN_INVERSE",39:"OPEN_INVERSE_CHAIN",44:"INVERSE",47:"OPEN_ENDBLOCK",48:"OPEN",51:"OPEN_UNESCAPED",54:"CLOSE_UNESCAPED",55:"OPEN_PARTIAL",60:"OPEN_PARTIAL_BLOCK",65:"OPEN_SEXPR",68:"CLOSE_SEXPR",72:"ID",73:"EQUALS",75:"OPEN_BLOCK_PARAMS",77:"CLOSE_BLOCK_PARAMS",80:"STRING",81:"NUMBER",82:"BOOLEAN",83:"UNDEFINED",84:"NULL",85:"DATA",87:"SEP"},productions_:[0,[3,2],[4,1],[7,1],[7,1],[7,1],[7,1],[7,1],[7,1],[7,1],[13,1],[10,3],[16,5],[9,4],[9,4],[24,6],[27,6],[38,6],[43,2],[45,3],[45,1],[26,3],[8,5],[8,5],[11,5],[12,3],[59,5],[63,1],[63,1],[64,5],[69,1],[71,3],[74,3],[20,1],[20,1],[20,1],[20,1],[20,1],[20,1],[20,1],[56,1],[56,1],[79,2],[78,1],[86,3],[86,1],[6,0],[6,2],[17,1],[17,2],[21,0],[21,2],[22,0],[22,1],[25,0],[25,1],[28,0],[28,1],[30,0],[30,2],[31,0],[31,1],[32,0],[32,1],[35,0],[35,2],[36,0],[36,1],[37,0],[37,1],[40,0],[40,2],[41,0],[41,1],[42,0],[42,1],[46,0],[46,1],[49,0],[49,2],[50,0],[50,1],[52,0],[52,2],[53,0],[53,1],[57,0],[57,2],[58,0],[58,1],[61,0],[61,2],[62,0],[62,1],[66,0],[66,2],[67,0],[67,1],[70,1],[70,2],[76,1],[76,2]],performAction:function e(r,s,m,t,u,v,o){var p=v.length-1;
switch(u){case 1:return v[p-1];
break;
case 2:this.$=t.prepareProgram(v[p]);
break;
case 3:this.$=v[p];
break;
case 4:this.$=v[p];
break;
case 5:this.$=v[p];
break;
case 6:this.$=v[p];
break;
case 7:this.$=v[p];
break;
case 8:this.$=v[p];
break;
case 9:this.$={type:"CommentStatement",value:t.stripComment(v[p]),strip:t.stripFlags(v[p],v[p]),loc:t.locInfo(this._$)};
break;
case 10:this.$={type:"ContentStatement",original:v[p],value:v[p],loc:t.locInfo(this._$)};
break;
case 11:this.$=t.prepareRawBlock(v[p-2],v[p-1],v[p],this._$);
break;
case 12:this.$={path:v[p-3],params:v[p-2],hash:v[p-1]};
break;
case 13:this.$=t.prepareBlock(v[p-3],v[p-2],v[p-1],v[p],false,this._$);
break;
case 14:this.$=t.prepareBlock(v[p-3],v[p-2],v[p-1],v[p],true,this._$);
break;
case 15:this.$={open:v[p-5],path:v[p-4],params:v[p-3],hash:v[p-2],blockParams:v[p-1],strip:t.stripFlags(v[p-5],v[p])};
break;
case 16:this.$={path:v[p-4],params:v[p-3],hash:v[p-2],blockParams:v[p-1],strip:t.stripFlags(v[p-5],v[p])};
break;
case 17:this.$={path:v[p-4],params:v[p-3],hash:v[p-2],blockParams:v[p-1],strip:t.stripFlags(v[p-5],v[p])};
break;
case 18:this.$={strip:t.stripFlags(v[p-1],v[p-1]),program:v[p]};
break;
case 19:var n=t.prepareBlock(v[p-2],v[p-1],v[p],v[p],false,this._$),q=t.prepareProgram([n],v[p-1].loc);
q.chained=true;
this.$={strip:v[p-2].strip,program:q,chain:true};
break;
case 20:this.$=v[p];
break;
case 21:this.$={path:v[p-1],strip:t.stripFlags(v[p-2],v[p])};
break;
case 22:this.$=t.prepareMustache(v[p-3],v[p-2],v[p-1],v[p-4],t.stripFlags(v[p-4],v[p]),this._$);
break;
case 23:this.$=t.prepareMustache(v[p-3],v[p-2],v[p-1],v[p-4],t.stripFlags(v[p-4],v[p]),this._$);
break;
case 24:this.$={type:"PartialStatement",name:v[p-3],params:v[p-2],hash:v[p-1],indent:"",strip:t.stripFlags(v[p-4],v[p]),loc:t.locInfo(this._$)};
break;
case 25:this.$=t.preparePartialBlock(v[p-2],v[p-1],v[p],this._$);
break;
case 26:this.$={path:v[p-3],params:v[p-2],hash:v[p-1],strip:t.stripFlags(v[p-4],v[p])};
break;
case 27:this.$=v[p];
break;
case 28:this.$=v[p];
break;
case 29:this.$={type:"SubExpression",path:v[p-3],params:v[p-2],hash:v[p-1],loc:t.locInfo(this._$)};
break;
case 30:this.$={type:"Hash",pairs:v[p],loc:t.locInfo(this._$)};
break;
case 31:this.$={type:"HashPair",key:t.id(v[p-2]),value:v[p],loc:t.locInfo(this._$)};
break;
case 32:this.$=t.id(v[p-1]);
break;
case 33:this.$=v[p];
break;
case 34:this.$=v[p];
break;
case 35:this.$={type:"StringLiteral",value:v[p],original:v[p],loc:t.locInfo(this._$)};
break;
case 36:this.$={type:"NumberLiteral",value:Number(v[p]),original:Number(v[p]),loc:t.locInfo(this._$)};
break;
case 37:this.$={type:"BooleanLiteral",value:v[p]==="true",original:v[p]==="true",loc:t.locInfo(this._$)};
break;
case 38:this.$={type:"UndefinedLiteral",original:undefined,value:undefined,loc:t.locInfo(this._$)};
break;
case 39:this.$={type:"NullLiteral",original:null,value:null,loc:t.locInfo(this._$)};
break;
case 40:this.$=v[p];
break;
case 41:this.$=v[p];
break;
case 42:this.$=t.preparePath(true,v[p],this._$);
break;
case 43:this.$=t.preparePath(false,v[p],this._$);
break;
case 44:v[p-2].push({part:t.id(v[p]),original:v[p],separator:v[p-1]});
this.$=v[p-2];
break;
case 45:this.$=[{part:t.id(v[p]),original:v[p]}];
break;
case 46:this.$=[];
break;
case 47:v[p-1].push(v[p]);
break;
case 48:this.$=[v[p]];
break;
case 49:v[p-1].push(v[p]);
break;
case 50:this.$=[];
break;
case 51:v[p-1].push(v[p]);
break;
case 58:this.$=[];
break;
case 59:v[p-1].push(v[p]);
break;
case 64:this.$=[];
break;
case 65:v[p-1].push(v[p]);
break;
case 70:this.$=[];
break;
case 71:v[p-1].push(v[p]);
break;
case 78:this.$=[];
break;
case 79:v[p-1].push(v[p]);
break;
case 82:this.$=[];
break;
case 83:v[p-1].push(v[p]);
break;
case 86:this.$=[];
break;
case 87:v[p-1].push(v[p]);
break;
case 90:this.$=[];
break;
case 91:v[p-1].push(v[p]);
break;
case 94:this.$=[];
break;
case 95:v[p-1].push(v[p]);
break;
case 98:this.$=[v[p]];
break;
case 99:v[p-1].push(v[p]);
break;
case 100:this.$=[v[p]];
break;
case 101:v[p-1].push(v[p]);
break
}},table:[{3:1,4:2,5:[2,46],6:3,14:[2,46],15:[2,46],19:[2,46],29:[2,46],34:[2,46],48:[2,46],51:[2,46],55:[2,46],60:[2,46]},{1:[3]},{5:[1,4]},{5:[2,2],7:5,8:6,9:7,10:8,11:9,12:10,13:11,14:[1,12],15:[1,20],16:17,19:[1,23],24:15,27:16,29:[1,21],34:[1,22],39:[2,2],44:[2,2],47:[2,2],48:[1,13],51:[1,14],55:[1,18],59:19,60:[1,24]},{1:[2,1]},{5:[2,47],14:[2,47],15:[2,47],19:[2,47],29:[2,47],34:[2,47],39:[2,47],44:[2,47],47:[2,47],48:[2,47],51:[2,47],55:[2,47],60:[2,47]},{5:[2,3],14:[2,3],15:[2,3],19:[2,3],29:[2,3],34:[2,3],39:[2,3],44:[2,3],47:[2,3],48:[2,3],51:[2,3],55:[2,3],60:[2,3]},{5:[2,4],14:[2,4],15:[2,4],19:[2,4],29:[2,4],34:[2,4],39:[2,4],44:[2,4],47:[2,4],48:[2,4],51:[2,4],55:[2,4],60:[2,4]},{5:[2,5],14:[2,5],15:[2,5],19:[2,5],29:[2,5],34:[2,5],39:[2,5],44:[2,5],47:[2,5],48:[2,5],51:[2,5],55:[2,5],60:[2,5]},{5:[2,6],14:[2,6],15:[2,6],19:[2,6],29:[2,6],34:[2,6],39:[2,6],44:[2,6],47:[2,6],48:[2,6],51:[2,6],55:[2,6],60:[2,6]},{5:[2,7],14:[2,7],15:[2,7],19:[2,7],29:[2,7],34:[2,7],39:[2,7],44:[2,7],47:[2,7],48:[2,7],51:[2,7],55:[2,7],60:[2,7]},{5:[2,8],14:[2,8],15:[2,8],19:[2,8],29:[2,8],34:[2,8],39:[2,8],44:[2,8],47:[2,8],48:[2,8],51:[2,8],55:[2,8],60:[2,8]},{5:[2,9],14:[2,9],15:[2,9],19:[2,9],29:[2,9],34:[2,9],39:[2,9],44:[2,9],47:[2,9],48:[2,9],51:[2,9],55:[2,9],60:[2,9]},{20:25,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:36,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{4:37,6:3,14:[2,46],15:[2,46],19:[2,46],29:[2,46],34:[2,46],39:[2,46],44:[2,46],47:[2,46],48:[2,46],51:[2,46],55:[2,46],60:[2,46]},{4:38,6:3,14:[2,46],15:[2,46],19:[2,46],29:[2,46],34:[2,46],44:[2,46],47:[2,46],48:[2,46],51:[2,46],55:[2,46],60:[2,46]},{13:40,15:[1,20],17:39},{20:42,56:41,64:43,65:[1,44],72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{4:45,6:3,14:[2,46],15:[2,46],19:[2,46],29:[2,46],34:[2,46],47:[2,46],48:[2,46],51:[2,46],55:[2,46],60:[2,46]},{5:[2,10],14:[2,10],15:[2,10],18:[2,10],19:[2,10],29:[2,10],34:[2,10],39:[2,10],44:[2,10],47:[2,10],48:[2,10],51:[2,10],55:[2,10],60:[2,10]},{20:46,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:47,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:48,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:42,56:49,64:43,65:[1,44],72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{33:[2,78],49:50,65:[2,78],72:[2,78],80:[2,78],81:[2,78],82:[2,78],83:[2,78],84:[2,78],85:[2,78]},{23:[2,33],33:[2,33],54:[2,33],65:[2,33],68:[2,33],72:[2,33],75:[2,33],80:[2,33],81:[2,33],82:[2,33],83:[2,33],84:[2,33],85:[2,33]},{23:[2,34],33:[2,34],54:[2,34],65:[2,34],68:[2,34],72:[2,34],75:[2,34],80:[2,34],81:[2,34],82:[2,34],83:[2,34],84:[2,34],85:[2,34]},{23:[2,35],33:[2,35],54:[2,35],65:[2,35],68:[2,35],72:[2,35],75:[2,35],80:[2,35],81:[2,35],82:[2,35],83:[2,35],84:[2,35],85:[2,35]},{23:[2,36],33:[2,36],54:[2,36],65:[2,36],68:[2,36],72:[2,36],75:[2,36],80:[2,36],81:[2,36],82:[2,36],83:[2,36],84:[2,36],85:[2,36]},{23:[2,37],33:[2,37],54:[2,37],65:[2,37],68:[2,37],72:[2,37],75:[2,37],80:[2,37],81:[2,37],82:[2,37],83:[2,37],84:[2,37],85:[2,37]},{23:[2,38],33:[2,38],54:[2,38],65:[2,38],68:[2,38],72:[2,38],75:[2,38],80:[2,38],81:[2,38],82:[2,38],83:[2,38],84:[2,38],85:[2,38]},{23:[2,39],33:[2,39],54:[2,39],65:[2,39],68:[2,39],72:[2,39],75:[2,39],80:[2,39],81:[2,39],82:[2,39],83:[2,39],84:[2,39],85:[2,39]},{23:[2,43],33:[2,43],54:[2,43],65:[2,43],68:[2,43],72:[2,43],75:[2,43],80:[2,43],81:[2,43],82:[2,43],83:[2,43],84:[2,43],85:[2,43],87:[1,51]},{72:[1,35],86:52},{23:[2,45],33:[2,45],54:[2,45],65:[2,45],68:[2,45],72:[2,45],75:[2,45],80:[2,45],81:[2,45],82:[2,45],83:[2,45],84:[2,45],85:[2,45],87:[2,45]},{52:53,54:[2,82],65:[2,82],72:[2,82],80:[2,82],81:[2,82],82:[2,82],83:[2,82],84:[2,82],85:[2,82]},{25:54,38:56,39:[1,58],43:57,44:[1,59],45:55,47:[2,54]},{28:60,43:61,44:[1,59],47:[2,56]},{13:63,15:[1,20],18:[1,62]},{15:[2,48],18:[2,48]},{33:[2,86],57:64,65:[2,86],72:[2,86],80:[2,86],81:[2,86],82:[2,86],83:[2,86],84:[2,86],85:[2,86]},{33:[2,40],65:[2,40],72:[2,40],80:[2,40],81:[2,40],82:[2,40],83:[2,40],84:[2,40],85:[2,40]},{33:[2,41],65:[2,41],72:[2,41],80:[2,41],81:[2,41],82:[2,41],83:[2,41],84:[2,41],85:[2,41]},{20:65,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{26:66,47:[1,67]},{30:68,33:[2,58],65:[2,58],72:[2,58],75:[2,58],80:[2,58],81:[2,58],82:[2,58],83:[2,58],84:[2,58],85:[2,58]},{33:[2,64],35:69,65:[2,64],72:[2,64],75:[2,64],80:[2,64],81:[2,64],82:[2,64],83:[2,64],84:[2,64],85:[2,64]},{21:70,23:[2,50],65:[2,50],72:[2,50],80:[2,50],81:[2,50],82:[2,50],83:[2,50],84:[2,50],85:[2,50]},{33:[2,90],61:71,65:[2,90],72:[2,90],80:[2,90],81:[2,90],82:[2,90],83:[2,90],84:[2,90],85:[2,90]},{20:75,33:[2,80],50:72,63:73,64:76,65:[1,44],69:74,70:77,71:78,72:[1,79],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{72:[1,80]},{23:[2,42],33:[2,42],54:[2,42],65:[2,42],68:[2,42],72:[2,42],75:[2,42],80:[2,42],81:[2,42],82:[2,42],83:[2,42],84:[2,42],85:[2,42],87:[1,51]},{20:75,53:81,54:[2,84],63:82,64:76,65:[1,44],69:83,70:77,71:78,72:[1,79],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{26:84,47:[1,67]},{47:[2,55]},{4:85,6:3,14:[2,46],15:[2,46],19:[2,46],29:[2,46],34:[2,46],39:[2,46],44:[2,46],47:[2,46],48:[2,46],51:[2,46],55:[2,46],60:[2,46]},{47:[2,20]},{20:86,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{4:87,6:3,14:[2,46],15:[2,46],19:[2,46],29:[2,46],34:[2,46],47:[2,46],48:[2,46],51:[2,46],55:[2,46],60:[2,46]},{26:88,47:[1,67]},{47:[2,57]},{5:[2,11],14:[2,11],15:[2,11],19:[2,11],29:[2,11],34:[2,11],39:[2,11],44:[2,11],47:[2,11],48:[2,11],51:[2,11],55:[2,11],60:[2,11]},{15:[2,49],18:[2,49]},{20:75,33:[2,88],58:89,63:90,64:76,65:[1,44],69:91,70:77,71:78,72:[1,79],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{65:[2,94],66:92,68:[2,94],72:[2,94],80:[2,94],81:[2,94],82:[2,94],83:[2,94],84:[2,94],85:[2,94]},{5:[2,25],14:[2,25],15:[2,25],19:[2,25],29:[2,25],34:[2,25],39:[2,25],44:[2,25],47:[2,25],48:[2,25],51:[2,25],55:[2,25],60:[2,25]},{20:93,72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:75,31:94,33:[2,60],63:95,64:76,65:[1,44],69:96,70:77,71:78,72:[1,79],75:[2,60],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:75,33:[2,66],36:97,63:98,64:76,65:[1,44],69:99,70:77,71:78,72:[1,79],75:[2,66],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:75,22:100,23:[2,52],63:101,64:76,65:[1,44],69:102,70:77,71:78,72:[1,79],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{20:75,33:[2,92],62:103,63:104,64:76,65:[1,44],69:105,70:77,71:78,72:[1,79],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{33:[1,106]},{33:[2,79],65:[2,79],72:[2,79],80:[2,79],81:[2,79],82:[2,79],83:[2,79],84:[2,79],85:[2,79]},{33:[2,81]},{23:[2,27],33:[2,27],54:[2,27],65:[2,27],68:[2,27],72:[2,27],75:[2,27],80:[2,27],81:[2,27],82:[2,27],83:[2,27],84:[2,27],85:[2,27]},{23:[2,28],33:[2,28],54:[2,28],65:[2,28],68:[2,28],72:[2,28],75:[2,28],80:[2,28],81:[2,28],82:[2,28],83:[2,28],84:[2,28],85:[2,28]},{23:[2,30],33:[2,30],54:[2,30],68:[2,30],71:107,72:[1,108],75:[2,30]},{23:[2,98],33:[2,98],54:[2,98],68:[2,98],72:[2,98],75:[2,98]},{23:[2,45],33:[2,45],54:[2,45],65:[2,45],68:[2,45],72:[2,45],73:[1,109],75:[2,45],80:[2,45],81:[2,45],82:[2,45],83:[2,45],84:[2,45],85:[2,45],87:[2,45]},{23:[2,44],33:[2,44],54:[2,44],65:[2,44],68:[2,44],72:[2,44],75:[2,44],80:[2,44],81:[2,44],82:[2,44],83:[2,44],84:[2,44],85:[2,44],87:[2,44]},{54:[1,110]},{54:[2,83],65:[2,83],72:[2,83],80:[2,83],81:[2,83],82:[2,83],83:[2,83],84:[2,83],85:[2,83]},{54:[2,85]},{5:[2,13],14:[2,13],15:[2,13],19:[2,13],29:[2,13],34:[2,13],39:[2,13],44:[2,13],47:[2,13],48:[2,13],51:[2,13],55:[2,13],60:[2,13]},{38:56,39:[1,58],43:57,44:[1,59],45:112,46:111,47:[2,76]},{33:[2,70],40:113,65:[2,70],72:[2,70],75:[2,70],80:[2,70],81:[2,70],82:[2,70],83:[2,70],84:[2,70],85:[2,70]},{47:[2,18]},{5:[2,14],14:[2,14],15:[2,14],19:[2,14],29:[2,14],34:[2,14],39:[2,14],44:[2,14],47:[2,14],48:[2,14],51:[2,14],55:[2,14],60:[2,14]},{33:[1,114]},{33:[2,87],65:[2,87],72:[2,87],80:[2,87],81:[2,87],82:[2,87],83:[2,87],84:[2,87],85:[2,87]},{33:[2,89]},{20:75,63:116,64:76,65:[1,44],67:115,68:[2,96],69:117,70:77,71:78,72:[1,79],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{33:[1,118]},{32:119,33:[2,62],74:120,75:[1,121]},{33:[2,59],65:[2,59],72:[2,59],75:[2,59],80:[2,59],81:[2,59],82:[2,59],83:[2,59],84:[2,59],85:[2,59]},{33:[2,61],75:[2,61]},{33:[2,68],37:122,74:123,75:[1,121]},{33:[2,65],65:[2,65],72:[2,65],75:[2,65],80:[2,65],81:[2,65],82:[2,65],83:[2,65],84:[2,65],85:[2,65]},{33:[2,67],75:[2,67]},{23:[1,124]},{23:[2,51],65:[2,51],72:[2,51],80:[2,51],81:[2,51],82:[2,51],83:[2,51],84:[2,51],85:[2,51]},{23:[2,53]},{33:[1,125]},{33:[2,91],65:[2,91],72:[2,91],80:[2,91],81:[2,91],82:[2,91],83:[2,91],84:[2,91],85:[2,91]},{33:[2,93]},{5:[2,22],14:[2,22],15:[2,22],19:[2,22],29:[2,22],34:[2,22],39:[2,22],44:[2,22],47:[2,22],48:[2,22],51:[2,22],55:[2,22],60:[2,22]},{23:[2,99],33:[2,99],54:[2,99],68:[2,99],72:[2,99],75:[2,99]},{73:[1,109]},{20:75,63:126,64:76,65:[1,44],72:[1,35],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{5:[2,23],14:[2,23],15:[2,23],19:[2,23],29:[2,23],34:[2,23],39:[2,23],44:[2,23],47:[2,23],48:[2,23],51:[2,23],55:[2,23],60:[2,23]},{47:[2,19]},{47:[2,77]},{20:75,33:[2,72],41:127,63:128,64:76,65:[1,44],69:129,70:77,71:78,72:[1,79],75:[2,72],78:26,79:27,80:[1,28],81:[1,29],82:[1,30],83:[1,31],84:[1,32],85:[1,34],86:33},{5:[2,24],14:[2,24],15:[2,24],19:[2,24],29:[2,24],34:[2,24],39:[2,24],44:[2,24],47:[2,24],48:[2,24],51:[2,24],55:[2,24],60:[2,24]},{68:[1,130]},{65:[2,95],68:[2,95],72:[2,95],80:[2,95],81:[2,95],82:[2,95],83:[2,95],84:[2,95],85:[2,95]},{68:[2,97]},{5:[2,21],14:[2,21],15:[2,21],19:[2,21],29:[2,21],34:[2,21],39:[2,21],44:[2,21],47:[2,21],48:[2,21],51:[2,21],55:[2,21],60:[2,21]},{33:[1,131]},{33:[2,63]},{72:[1,133],76:132},{33:[1,134]},{33:[2,69]},{15:[2,12]},{14:[2,26],15:[2,26],19:[2,26],29:[2,26],34:[2,26],47:[2,26],48:[2,26],51:[2,26],55:[2,26],60:[2,26]},{23:[2,31],33:[2,31],54:[2,31],68:[2,31],72:[2,31],75:[2,31]},{33:[2,74],42:135,74:136,75:[1,121]},{33:[2,71],65:[2,71],72:[2,71],75:[2,71],80:[2,71],81:[2,71],82:[2,71],83:[2,71],84:[2,71],85:[2,71]},{33:[2,73],75:[2,73]},{23:[2,29],33:[2,29],54:[2,29],65:[2,29],68:[2,29],72:[2,29],75:[2,29],80:[2,29],81:[2,29],82:[2,29],83:[2,29],84:[2,29],85:[2,29]},{14:[2,15],15:[2,15],19:[2,15],29:[2,15],34:[2,15],39:[2,15],44:[2,15],47:[2,15],48:[2,15],51:[2,15],55:[2,15],60:[2,15]},{72:[1,138],77:[1,137]},{72:[2,100],77:[2,100]},{14:[2,16],15:[2,16],19:[2,16],29:[2,16],34:[2,16],44:[2,16],47:[2,16],48:[2,16],51:[2,16],55:[2,16],60:[2,16]},{33:[1,139]},{33:[2,75]},{33:[2,32]},{72:[2,101],77:[2,101]},{14:[2,17],15:[2,17],19:[2,17],29:[2,17],34:[2,17],39:[2,17],44:[2,17],47:[2,17],48:[2,17],51:[2,17],55:[2,17],60:[2,17]}],defaultActions:{4:[2,1],55:[2,55],57:[2,20],61:[2,57],74:[2,81],83:[2,85],87:[2,18],91:[2,89],102:[2,53],105:[2,93],111:[2,19],112:[2,77],117:[2,97],120:[2,63],123:[2,69],124:[2,12],136:[2,75],137:[2,32]},parseError:function g(n,m){throw new Error(n)
},parse:function k(x){var E=this,u=[0],N=[null],z=[],O=this.table,n="",y=0,L=0,q=0,w=2,B=1;
this.lexer.setInput(x);
this.lexer.yy=this.yy;
this.yy.lexer=this.lexer;
this.yy.parser=this;
if(typeof this.lexer.yylloc=="undefined"){this.lexer.yylloc={}
}var o=this.lexer.yylloc;
z.push(o);
var s=this.lexer.options&&this.lexer.options.ranges;
if(typeof this.yy.parseError==="function"){this.parseError=this.yy.parseError
}function D(p){u.length=u.length-2*p;
N.length=N.length-p;
z.length=z.length-p
}function C(){var p;
p=E.lexer.lex()||1;
if(typeof p!=="number"){p=E.symbols_[p]||p
}return p
}var K,G,t,J,P,A,I={},F,M,m,v;
while(true){t=u[u.length-1];
if(this.defaultActions[t]){J=this.defaultActions[t]
}else{if(K===null||typeof K=="undefined"){K=C()
}J=O[t]&&O[t][K]
}if(typeof J==="undefined"||!J.length||!J[0]){var H="";
if(!q){v=[];
for(F in O[t]){if(this.terminals_[F]&&F>2){v.push("'"+this.terminals_[F]+"'")
}}if(this.lexer.showPosition){H="Parse error on line "+(y+1)+":\n"+this.lexer.showPosition()+"\nExpecting "+v.join(", ")+", got '"+(this.terminals_[K]||K)+"'"
}else{H="Parse error on line "+(y+1)+": Unexpected "+(K==1?"end of input":"'"+(this.terminals_[K]||K)+"'")
}this.parseError(H,{text:this.lexer.match,token:this.terminals_[K]||K,line:this.lexer.yylineno,loc:o,expected:v})
}}if(J[0] instanceof Array&&J.length>1){throw new Error("Parse Error: multiple actions possible at state: "+t+", token: "+K)
}switch(J[0]){case 1:u.push(K);
N.push(this.lexer.yytext);
z.push(this.lexer.yylloc);
u.push(J[1]);
K=null;
if(!G){L=this.lexer.yyleng;
n=this.lexer.yytext;
y=this.lexer.yylineno;
o=this.lexer.yylloc;
if(q>0){q--
}}else{K=G;
G=null
}break;
case 2:M=this.productions_[J[1]][1];
I.$=N[N.length-M];
I._$={first_line:z[z.length-(M||1)].first_line,last_line:z[z.length-1].last_line,first_column:z[z.length-(M||1)].first_column,last_column:z[z.length-1].last_column};
if(s){I._$.range=[z[z.length-(M||1)].range[0],z[z.length-1].range[1]]
}A=this.performAction.call(I,n,L,y,this.yy,J[1],N,z);
if(typeof A!=="undefined"){return A
}if(M){u=u.slice(0,-1*M*2);
N=N.slice(0,-1*M);
z=z.slice(0,-1*M)
}u.push(this.productions_[J[1]][0]);
N.push(I.$);
z.push(I._$);
m=O[u[u.length-2]][u[u.length-1]];
u.push(m);
break;
case 3:return true
}}return true
}};
var d=(function(){var o={EOF:1,parseError:function q(E,D){if(this.yy.parser){this.yy.parser.parseError(E,D)
}else{throw new Error(E)
}},setInput:function p(D){this._input=D;
this._more=this._less=this.done=false;
this.yylineno=this.yyleng=0;
this.yytext=this.matched=this.match="";
this.conditionStack=["INITIAL"];
this.yylloc={first_line:1,first_column:0,last_line:1,last_column:0};
if(this.options.ranges){this.yylloc.range=[0,0]
}this.offset=0;
return this
},input:function z(){var E=this._input[0];
this.yytext+=E;
this.yyleng++;
this.offset++;
this.match+=E;
this.matched+=E;
var D=E.match(/(?:\r\n?|\n).*/g);
if(D){this.yylineno++;
this.yylloc.last_line++
}else{this.yylloc.last_column++
}if(this.options.ranges){this.yylloc.range[1]++
}this._input=this._input.slice(1);
return E
},unput:function x(F){var D=F.length;
var E=F.split(/(?:\r\n?|\n)/g);
this._input=F+this._input;
this.yytext=this.yytext.substr(0,this.yytext.length-D-1);
this.offset-=D;
var H=this.match.split(/(?:\r\n?|\n)/g);
this.match=this.match.substr(0,this.match.length-1);
this.matched=this.matched.substr(0,this.matched.length-1);
if(E.length-1){this.yylineno-=E.length-1
}var G=this.yylloc.range;
this.yylloc={first_line:this.yylloc.first_line,last_line:this.yylineno+1,first_column:this.yylloc.first_column,last_column:E?(E.length===H.length?this.yylloc.first_column:0)+H[H.length-E.length].length-E[0].length:this.yylloc.first_column-D};
if(this.options.ranges){this.yylloc.range=[G[0],G[0]+this.yyleng-D]
}return this
},more:function v(){this._more=true;
return this
},less:function A(D){this.unput(this.match.slice(D))
},pastInput:function t(){var D=this.matched.substr(0,this.matched.length-this.match.length);
return(D.length>20?"...":"")+D.substr(-20).replace(/\n/g,"")
},upcomingInput:function B(){var D=this.match;
if(D.length<20){D+=this._input.substr(0,20-D.length)
}return(D.substr(0,20)+(D.length>20?"...":"")).replace(/\n/g,"")
},showPosition:function w(){var D=this.pastInput();
var E=new Array(D.length+1).join("-");
return D+this.upcomingInput()+"\n"+E+"^"
},next:function s(){if(this.done){return this.EOF
}if(!this._input){this.done=true
}var J,H,E,G,F,D;
if(!this._more){this.yytext="";
this.match=""
}var K=this._currentRules();
for(var I=0;
I<K.length;
I++){E=this._input.match(this.rules[K[I]]);
if(E&&(!H||E[0].length>H[0].length)){H=E;
G=I;
if(!this.options.flex){break
}}}if(H){D=H[0].match(/(?:\r\n?|\n).*/g);
if(D){this.yylineno+=D.length
}this.yylloc={first_line:this.yylloc.last_line,last_line:this.yylineno+1,first_column:this.yylloc.last_column,last_column:D?D[D.length-1].length-D[D.length-1].match(/\r?\n?/)[0].length:this.yylloc.last_column+H[0].length};
this.yytext+=H[0];
this.match+=H[0];
this.matches=H;
this.yyleng=this.yytext.length;
if(this.options.ranges){this.yylloc.range=[this.offset,this.offset+=this.yyleng]
}this._more=false;
this._input=this._input.slice(H[0].length);
this.matched+=H[0];
J=this.performAction.call(this,this.yy,this,K[G],this.conditionStack[this.conditionStack.length-1]);
if(this.done&&this._input){this.done=false
}if(J){return J
}else{return
}}if(this._input===""){return this.EOF
}else{return this.parseError("Lexical error on line "+(this.yylineno+1)+". Unrecognized text.\n"+this.showPosition(),{text:"",token:null,line:this.yylineno})
}},lex:function m(){var D=this.next();
if(typeof D!=="undefined"){return D
}else{return this.lex()
}},begin:function n(D){this.conditionStack.push(D)
},popState:function r(){return this.conditionStack.pop()
},_currentRules:function C(){return this.conditions[this.conditionStack[this.conditionStack.length-1]].rules
},topState:function u(){return this.conditionStack[this.conditionStack.length-2]
},pushState:function n(D){this.begin(D)
}};
o.options={};
o.performAction=function y(I,E,H,D){function F(K,J){return E.yytext=E.yytext.substr(K,E.yyleng-J)
}var G=D;
switch(H){case 0:if(E.yytext.slice(-2)==="\\\\"){F(0,1);
this.begin("mu")
}else{if(E.yytext.slice(-1)==="\\"){F(0,1);
this.begin("emu")
}else{this.begin("mu")
}}if(E.yytext){return 15
}break;
case 1:return 15;
break;
case 2:this.popState();
return 15;
break;
case 3:this.begin("raw");
return 15;
break;
case 4:this.popState();
if(this.conditionStack[this.conditionStack.length-1]==="raw"){return 15
}else{E.yytext=E.yytext.substr(5,E.yyleng-9);
return"END_RAW_BLOCK"
}break;
case 5:return 15;
break;
case 6:this.popState();
return 14;
break;
case 7:return 65;
break;
case 8:return 68;
break;
case 9:return 19;
break;
case 10:this.popState();
this.begin("raw");
return 23;
break;
case 11:return 55;
break;
case 12:return 60;
break;
case 13:return 29;
break;
case 14:return 47;
break;
case 15:this.popState();
return 44;
break;
case 16:this.popState();
return 44;
break;
case 17:return 34;
break;
case 18:return 39;
break;
case 19:return 51;
break;
case 20:return 48;
break;
case 21:this.unput(E.yytext);
this.popState();
this.begin("com");
break;
case 22:this.popState();
return 14;
break;
case 23:return 48;
break;
case 24:return 73;
break;
case 25:return 72;
break;
case 26:return 72;
break;
case 27:return 87;
break;
case 28:break;
case 29:this.popState();
return 54;
break;
case 30:this.popState();
return 33;
break;
case 31:E.yytext=F(1,2).replace(/\\"/g,'"');
return 80;
break;
case 32:E.yytext=F(1,2).replace(/\\'/g,"'");
return 80;
break;
case 33:return 85;
break;
case 34:return 82;
break;
case 35:return 82;
break;
case 36:return 83;
break;
case 37:return 84;
break;
case 38:return 81;
break;
case 39:return 75;
break;
case 40:return 77;
break;
case 41:return 72;
break;
case 42:E.yytext=E.yytext.replace(/\\([\\\]])/g,"$1");
return 72;
break;
case 43:return"INVALID";
break;
case 44:return 5;
break
}};
o.rules=[/^(?:[^\x00]*?(?=(\{\{)))/,/^(?:[^\x00]+)/,/^(?:[^\x00]{2,}?(?=(\{\{|\\\{\{|\\\\\{\{|$)))/,/^(?:\{\{\{\{(?=[^\/]))/,/^(?:\{\{\{\{\/[^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=[=}\s\/.])\}\}\}\})/,/^(?:[^\x00]*?(?=(\{\{\{\{)))/,/^(?:[\s\S]*?--(~)?\}\})/,/^(?:\()/,/^(?:\))/,/^(?:\{\{\{\{)/,/^(?:\}\}\}\})/,/^(?:\{\{(~)?>)/,/^(?:\{\{(~)?#>)/,/^(?:\{\{(~)?#\*?)/,/^(?:\{\{(~)?\/)/,/^(?:\{\{(~)?\^\s*(~)?\}\})/,/^(?:\{\{(~)?\s*else\s*(~)?\}\})/,/^(?:\{\{(~)?\^)/,/^(?:\{\{(~)?\s*else\b)/,/^(?:\{\{(~)?\{)/,/^(?:\{\{(~)?&)/,/^(?:\{\{(~)?!--)/,/^(?:\{\{(~)?![\s\S]*?\}\})/,/^(?:\{\{(~)?\*?)/,/^(?:=)/,/^(?:\.\.)/,/^(?:\.(?=([=~}\s\/.)|])))/,/^(?:[\/.])/,/^(?:\s+)/,/^(?:\}(~)?\}\})/,/^(?:(~)?\}\})/,/^(?:"(\\["]|[^"])*")/,/^(?:'(\\[']|[^'])*')/,/^(?:@)/,/^(?:true(?=([~}\s)])))/,/^(?:false(?=([~}\s)])))/,/^(?:undefined(?=([~}\s)])))/,/^(?:null(?=([~}\s)])))/,/^(?:-?[0-9]+(?:\.[0-9]+)?(?=([~}\s)])))/,/^(?:as\s+\|)/,/^(?:\|)/,/^(?:([^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=([=~}\s\/.)|]))))/,/^(?:\[(\\\]|[^\]])*\])/,/^(?:.)/,/^(?:$)/];
o.conditions={mu:{rules:[7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44],inclusive:false},emu:{rules:[2],inclusive:false},com:{rules:[6],inclusive:false},raw:{rules:[3,4,5],inclusive:false},INITIAL:{rules:[0,1,44],inclusive:true}};
return o
})();
l.lexer=d;
function h(){this.yy={}
}h.prototype=l;
l.Parser=h;
return new h()
})();
b["default"]=a;
c.exports=b["default"]
}),(function(b,c,a){var l=a(1)["default"];
c.__esModule=true;
var e=a(39);
var m=l(e);
function d(){var n=arguments.length<=0||arguments[0]===undefined?{}:arguments[0];
this.options=n
}d.prototype=new m["default"]();
d.prototype.Program=function(u){var w=!this.options.ignoreStandalone;
var q=!this.isRootSeen;
this.isRootSeen=true;
var v=u.body;
for(var t=0,r=v.length;
t<r;
t++){var x=v[t],n=this.accept(x);
if(!n){continue
}var p=f(v,t,q),s=h(v,t,q),o=n.openStandalone&&p,z=n.closeStandalone&&s,y=n.inlineStandalone&&p&&s;
if(n.close){g(v,t,true)
}if(n.open){k(v,t,true)
}if(w&&y){g(v,t);
if(k(v,t)){if(x.type==="PartialStatement"){x.indent=/([ \t]+$)/.exec(v[t-1].original)[1]
}}}if(w&&o){g((x.program||x.inverse).body);
k(v,t)
}if(w&&z){g(v,t);
k((x.inverse||x.program).body)
}}return u
};
d.prototype.BlockStatement=d.prototype.DecoratorBlock=d.prototype.PartialBlockStatement=function(t){this.accept(t.program);
this.accept(t.inverse);
var p=t.program||t.inverse,n=t.program&&t.inverse,o=n,s=n;
if(n&&n.chained){o=n.body[0].program;
while(s.chained){s=s.body[s.body.length-1].program
}}var q={open:t.openStrip.open,close:t.closeStrip.close,openStandalone:h(p.body),closeStandalone:f((o||p).body)};
if(t.openStrip.close){g(p.body,null,true)
}if(n){var r=t.inverseStrip;
if(r.open){k(p.body,null,true)
}if(r.close){g(o.body,null,true)
}if(t.closeStrip.open){k(s.body,null,true)
}if(!this.options.ignoreStandalone&&f(p.body)&&h(o.body)){k(p.body);
g(o.body)
}}else{if(t.closeStrip.open){k(p.body,null,true)
}}return q
};
d.prototype.Decorator=d.prototype.MustacheStatement=function(n){return n.strip
};
d.prototype.PartialStatement=d.prototype.CommentStatement=function(o){var n=o.strip||{};
return{inlineStandalone:true,open:n.open,close:n.close}
};
function f(n,p,o){if(p===undefined){p=n.length
}var r=n[p-1],q=n[p-2];
if(!r){return o
}if(r.type==="ContentStatement"){return(q||!o?/\r?\n\s*?$/:/(^|\r?\n)\s*?$/).test(r.original)
}}function h(n,p,o){if(p===undefined){p=-1
}var r=n[p+1],q=n[p+2];
if(!r){return o
}if(r.type==="ContentStatement"){return(q||!o?/^\s*?\r?\n/:/^\s*?(\r?\n|$)/).test(r.original)
}}function g(o,q,n){var r=o[q==null?0:q+1];
if(!r||r.type!=="ContentStatement"||!n&&r.rightStripped){return
}var p=r.value;
r.value=r.value.replace(n?/^\s+/:/^[ \t]*\r?\n?/,"");
r.rightStripped=r.value!==p
}function k(o,q,n){var r=o[q==null?o.length-1:q-1];
if(!r||r.type!=="ContentStatement"||!n&&r.leftStripped){return
}var p=r.value;
r.value=r.value.replace(n?/\s+$/:/[ \t]+$/,"");
r.leftStripped=r.value!==p;
return r.leftStripped
}c["default"]=d;
b.exports=c["default"]
}),(function(c,A,e){var a=e(1)["default"];
A.__esModule=true;
var v=e(6);
var d=a(v);
function z(){this.parents=[]
}z.prototype={constructor:z,mutating:false,acceptKey:function s(D,C){var E=this.accept(D[C]);
if(this.mutating){if(E&&!z.prototype[E.type]){throw new d["default"]('Unexpected node type "'+E.type+'" found when accepting '+C+" on "+D.type)
}D[C]=E
}},acceptRequired:function w(D,C){this.acceptKey(D,C);
if(!D[C]){throw new d["default"](D.type+" requires "+C)
}},acceptArray:function x(E){for(var D=0,C=E.length;
D<C;
D++){this.acceptKey(E,D);
if(!E[D]){E.splice(D,1);
D--;
C--
}}},accept:function f(D){if(!D){return
}if(!this[D.type]){throw new d["default"]("Unknown type: "+D.type,D)
}if(this.current){this.parents.unshift(this.current)
}this.current=D;
var C=this[D.type](D);
this.current=this.parents.shift();
if(!this.mutating||C){return C
}else{if(C!==false){return D
}}},Program:function y(C){this.acceptArray(C.body)
},MustacheStatement:p,Decorator:p,BlockStatement:h,DecoratorBlock:h,PartialStatement:B,PartialBlockStatement:function b(C){B.call(this,C);
this.acceptKey(C,"program")
},ContentStatement:function u(){},CommentStatement:function o(){},SubExpression:p,PathExpression:function g(){},StringLiteral:function r(){},NumberLiteral:function t(){},BooleanLiteral:function n(){},UndefinedLiteral:function q(){},NullLiteral:function m(){},Hash:function k(C){this.acceptArray(C.pairs)
},HashPair:function l(C){this.acceptRequired(C,"value")
}};
function p(C){this.acceptRequired(C,"path");
this.acceptArray(C.params);
this.acceptKey(C,"hash")
}function h(C){p.call(this,C);
this.acceptKey(C,"program");
this.acceptKey(C,"inverse")
}function B(C){this.acceptRequired(C,"name");
this.acceptArray(C.params);
this.acceptKey(C,"hash")
}A["default"]=z;
c.exports=A["default"]
}),(function(d,h,c){var s=c(1)["default"];
h.__esModule=true;
h.SourceLocation=n;
h.id=b;
h.stripFlags=g;
h.stripComment=q;
h.preparePath=f;
h.prepareMustache=m;
h.prepareRawBlock=p;
h.prepareBlock=r;
h.prepareProgram=l;
h.preparePartialBlock=o;
var e=c(6);
var a=s(e);
function k(t,v){v=v.path?v.path.original:v;
if(t.path.original!==v){var u={loc:t.path.loc};
throw new a["default"](t.path.original+" doesn't match "+v,u)
}}function n(u,t){this.source=u;
this.start={line:t.first_line,column:t.first_column};
this.end={line:t.last_line,column:t.last_column}
}function b(t){if(/^\[.*\]$/.test(t)){return t.substr(1,t.length-2)
}else{return t
}}function g(t,u){return{open:t.charAt(2)==="~",close:u.charAt(u.length-3)==="~"}
}function q(t){return t.replace(/^\{\{~?\!-?-?/,"").replace(/-?-?~?\}\}$/,"")
}function f(A,y,C){C=this.locInfo(C);
var w=A?"@":"",D=[],z=0,v="";
for(var B=0,x=y.length;
B<x;
B++){var u=y[B].part,t=y[B].original!==u;
w+=(y[B].separator||"")+u;
if(!t&&(u===".."||u==="."||u==="this")){if(D.length>0){throw new a["default"]("Invalid path: "+w,{loc:C})
}else{if(u===".."){z++;
v+="../"
}}}else{D.push(u)
}}return{type:"PathExpression",data:A,depth:z,parts:D,original:w,loc:C}
}function m(B,v,x,y,u,A){var z=y.charAt(3)||y.charAt(2),t=z!=="{"&&z!=="&";
var w=/\*/.test(y);
return{type:w?"Decorator":"MustacheStatement",path:B,params:v,hash:x,escaped:t,strip:u,loc:this.locInfo(A)}
}function p(t,w,x,v){k(t,x);
v=this.locInfo(v);
var u={type:"Program",body:w,strip:{},loc:v};
return{type:"BlockStatement",path:t.path,params:t.params,hash:t.hash,program:u,openStrip:{},inverseStrip:{},closeStrip:{},loc:v}
}function r(y,x,z,B,u,A){if(B&&B.path){k(y,B)
}var v=/\*/.test(y.open);
x.blockParams=y.blockParams;
var w=undefined,t=undefined;
if(z){if(v){throw new a["default"]("Unexpected inverse block on decorator",z)
}if(z.chain){z.program.body[0].closeStrip=B.strip
}t=z.strip;
w=z.program
}if(u){u=w;
w=x;
x=u
}return{type:v?"DecoratorBlock":"BlockStatement",path:y.path,params:y.params,hash:y.hash,program:x,inverse:w,openStrip:y.strip,inverseStrip:t,closeStrip:B&&B.strip,loc:this.locInfo(A)}
}function l(t,w){if(!w&&t.length){var v=t[0].loc,u=t[t.length-1].loc;
if(v&&u){w={source:v.source,start:{line:v.start.line,column:v.start.column},end:{line:u.end.line,column:u.end.column}}
}}return{type:"Program",body:t,strip:{},loc:w}
}function o(u,t,w,v){k(u,w);
return{type:"PartialBlockStatement",name:u.path,params:u.params,hash:u.hash,program:t,openStrip:u.strip,closeStrip:w&&w.strip,loc:this.locInfo(v)}
}}),(function(d,L,l){var U=l(1)["default"];
L.__esModule=true;
L.Compiler=z;
L.precompile=T;
L.compile=m;
var S=l(6);
var v=U(S);
var b=l(5);
var a=l(35);
var e=U(a);
var r=[].slice;
function z(){}z.prototype={compiler:z,equals:function y(W){var V=this.opcodes.length;
if(W.opcodes.length!==V){return false
}for(var Y=0;
Y<V;
Y++){var Z=this.opcodes[Y],X=W.opcodes[Y];
if(Z.opcode!==X.opcode||!x(Z.args,X.args)){return false
}}V=this.children.length;
for(var Y=0;
Y<V;
Y++){if(!this.children[Y].equals(W.children[Y])){return false
}}return true
},guid:0,compile:function m(W,X){this.sourceNode=[];
this.opcodes=[];
this.children=[];
this.options=X;
this.stringParams=X.stringParams;
this.trackIds=X.trackIds;
X.blockParams=X.blockParams||[];
var Y=X.knownHelpers;
X.knownHelpers={helperMissing:true,blockHelperMissing:true,each:true,"if":true,unless:true,"with":true,log:true,lookup:true};
if(Y){for(var V in Y){if(V in Y){this.options.knownHelpers[V]=Y[V]
}}}return this.accept(W)
},compileProgram:function q(W){var Y=new this.compiler(),V=Y.compile(W,this.options),X=this.guid++;
this.usePartial=this.usePartial||V.usePartial;
this.children[X]=V;
this.useDepths=this.useDepths||V.useDepths;
return X
},accept:function f(W){if(!this[W.type]){throw new v["default"]("Unknown type: "+W.type,W)
}this.sourceNode.unshift(W);
var V=this[W.type](W);
this.sourceNode.shift();
return V
},Program:function R(W){this.options.blockParams.unshift(W.blockParams);
var V=W.body,Y=V.length;
for(var X=0;
X<Y;
X++){this.accept(V[X])
}this.options.blockParams.shift();
this.isSimple=Y===1;
this.blockParams=W.blockParams?W.blockParams.length:0;
return this
},BlockStatement:function Q(Y){g(Y);
var W=Y.program,V=Y.inverse;
W=W&&this.compileProgram(W);
V=V&&this.compileProgram(V);
var X=this.classifySexpr(Y);
if(X==="helper"){this.helperSexpr(Y,W,V)
}else{if(X==="simple"){this.simpleSexpr(Y);
this.opcode("pushProgram",W);
this.opcode("pushProgram",V);
this.opcode("emptyHash");
this.opcode("blockValue",Y.path.original)
}else{this.ambiguousSexpr(Y,W,V);
this.opcode("pushProgram",W);
this.opcode("pushProgram",V);
this.opcode("emptyHash");
this.opcode("ambiguousBlockValue")
}}this.opcode("append")
},DecoratorBlock:function t(W){var V=W.program&&this.compileProgram(W.program);
var Y=this.setupFullMustacheParams(W,V,undefined),X=W.path;
this.useDecorators=true;
this.opcode("registerDecorator",Y.length,X.original)
},PartialStatement:function M(Y){this.usePartial=true;
var X=Y.program;
if(X){X=this.compileProgram(Y.program)
}var aa=Y.params;
if(aa.length>1){throw new v["default"]("Unsupported number of partial arguments: "+aa.length,Y)
}else{if(!aa.length){if(this.options.explicitPartialContext){this.opcode("pushLiteral","undefined")
}else{aa.push({type:"PathExpression",parts:[],depth:0})
}}}var Z=Y.name.original,W=Y.name.type==="SubExpression";
if(W){this.accept(Y.name)
}this.setupFullMustacheParams(Y,X,undefined,true);
var V=Y.indent||"";
if(this.options.preventIndent&&V){this.opcode("appendContent",V);
V=""
}this.opcode("invokePartial",W,Z,V);
this.opcode("append")
},PartialBlockStatement:function I(V){this.PartialStatement(V)
},MustacheStatement:function B(V){this.SubExpression(V);
if(V.escaped&&!this.options.noEscape){this.opcode("appendEscaped")
}else{this.opcode("append")
}},Decorator:function h(V){this.DecoratorBlock(V)
},ContentStatement:function k(V){if(V.value){this.opcode("appendContent",V.value)
}},CommentStatement:function A(){},SubExpression:function J(W){g(W);
var V=this.classifySexpr(W);
if(V==="simple"){this.simpleSexpr(W)
}else{if(V==="helper"){this.helperSexpr(W)
}else{this.ambiguousSexpr(W)
}}},ambiguousSexpr:function w(Z,X,W){var aa=Z.path,Y=aa.parts[0],V=X!=null||W!=null;
this.opcode("getContext",aa.depth);
this.opcode("pushProgram",X);
this.opcode("pushProgram",W);
aa.strict=true;
this.accept(aa);
this.opcode("invokeAmbiguous",Y,V)
},simpleSexpr:function G(V){var W=V.path;
W.strict=true;
this.accept(W);
this.opcode("resolvePossibleLambda")
},helperSexpr:function s(Y,W,V){var aa=this.setupFullMustacheParams(Y,W,V),Z=Y.path,X=Z.parts[0];
if(this.options.knownHelpers[X]){this.opcode("invokeKnownHelper",aa.length,X)
}else{if(this.options.knownHelpersOnly){throw new v["default"]("You specified knownHelpersOnly, but used the unknown helper "+X,Y)
}else{Z.strict=true;
Z.falsy=true;
this.accept(Z);
this.opcode("invokeHelper",aa.length,Z.original,e["default"].helpers.simpleId(Z))
}}},PathExpression:function c(W){this.addDepth(W.depth);
this.opcode("getContext",W.depth);
var V=W.parts[0],Y=e["default"].helpers.scopedId(W),X=!W.depth&&!Y&&this.blockParamIndex(V);
if(X){this.opcode("lookupBlockParam",X,W.parts)
}else{if(!V){this.opcode("pushContext")
}else{if(W.data){this.options.data=true;
this.opcode("lookupData",W.depth,W.parts,W.strict)
}else{this.opcode("lookupOnContext",W.parts,W.falsy,W.strict,Y)
}}}},StringLiteral:function n(V){this.opcode("pushString",V.value)
},NumberLiteral:function E(V){this.opcode("pushLiteral",V.value)
},BooleanLiteral:function O(V){this.opcode("pushLiteral",V.value)
},UndefinedLiteral:function o(){this.opcode("pushLiteral","undefined")
},NullLiteral:function P(){this.opcode("pushLiteral","null")
},Hash:function D(Y){var X=Y.pairs,W=0,V=X.length;
this.opcode("pushHash");
for(;
W<V;
W++){this.pushParam(X[W].value)
}while(W--){this.opcode("assignToHash",X[W].key)
}this.opcode("popHash")
},opcode:function F(V){this.opcodes.push({opcode:V,args:r.call(arguments,1),loc:this.sourceNode[0].loc})
},addDepth:function u(V){if(!V){return
}this.useDepths=true
},classifySexpr:function K(Y){var Z=e["default"].helpers.simpleId(Y.path);
var aa=Z&&!!this.blockParamIndex(Y.path.parts[0]);
var X=!aa&&e["default"].helpers.helperExpression(Y);
var ab=!aa&&(X||Z);
if(ab&&!X){var V=Y.path.parts[0],W=this.options;
if(W.knownHelpers[V]){X=true
}else{if(W.knownHelpersOnly){ab=false
}}}if(X){return"helper"
}else{if(ab){return"ambiguous"
}else{return"simple"
}}},pushParams:function N(X){for(var W=0,V=X.length;
W<V;
W++){this.pushParam(X[W])
}},pushParam:function H(Y){var X=Y.value!=null?Y.value:Y.original||"";
if(this.stringParams){if(X.replace){X=X.replace(/^(\.?\.\/)*/g,"").replace(/\//g,".")
}if(Y.depth){this.addDepth(Y.depth)
}this.opcode("getContext",Y.depth||0);
this.opcode("pushStringParam",X,Y.type);
if(Y.type==="SubExpression"){this.accept(Y)
}}else{if(this.trackIds){var W=undefined;
if(Y.parts&&!e["default"].helpers.scopedId(Y)&&!Y.depth){W=this.blockParamIndex(Y.parts[0])
}if(W){var V=Y.parts.slice(1).join(".");
this.opcode("pushId","BlockParam",W,V)
}else{X=Y.original||X;
if(X.replace){X=X.replace(/^this(?:\.|$)/,"").replace(/^\.\//,"").replace(/^\.$/,"")
}this.opcode("pushId",Y.type,X)
}}this.accept(Y)
}},setupFullMustacheParams:function C(Y,W,V,X){var Z=Y.params;
this.pushParams(Z);
this.opcode("pushProgram",W);
this.opcode("pushProgram",V);
if(Y.hash){this.accept(Y.hash)
}else{this.opcode("emptyHash",X)
}return Z
},blockParamIndex:function p(W){for(var Z=0,V=this.options.blockParams.length;
Z<V;
Z++){var X=this.options.blockParams[Z],Y=X&&b.indexOf(X,W);
if(X&&Y>=0){return[Z,Y]
}}}};
function T(X,Y,Z){if(X==null||typeof X!=="string"&&X.type!=="Program"){throw new v["default"]("You must pass a string or Handlebars AST to Handlebars.precompile. You passed "+X)
}Y=Y||{};
if(!("data" in Y)){Y.data=true
}if(Y.compat){Y.useDepths=true
}var W=Z.parse(X,Y),V=new Z.Compiler().compile(W,Y);
return new Z.JavaScriptCompiler().compile(V,Y)
}function m(V,X,Y){if(X===undefined){X={}
}if(V==null||typeof V!=="string"&&V.type!=="Program"){throw new v["default"]("You must pass a string or Handlebars AST to Handlebars.compile. You passed "+V)
}X=b.extend({},X);
if(!("data" in X)){X.data=true
}if(X.compat){X.useDepths=true
}var aa=undefined;
function Z(){var ad=Y.parse(V,X),ac=new Y.Compiler().compile(ad,X),ab=new Y.JavaScriptCompiler().compile(ac,X,undefined,true);
return Y.template(ab)
}function W(ab,ac){if(!aa){aa=Z()
}return aa.call(this,ab,ac)
}W._setup=function(ab){if(!aa){aa=Z()
}return aa._setup(ab)
};
W._child=function(ab,ad,ac,ae){if(!aa){aa=Z()
}return aa._child(ab,ad,ac,ae)
};
return W
}function x(W,V){if(W===V){return true
}if(b.isArray(W)&&b.isArray(V)&&W.length===V.length){for(var X=0;
X<W.length;
X++){if(!x(W[X],V[X])){return false
}}return true
}}function g(W){if(!W.path.parts){var V=W.path;
W.path={type:"PathExpression",data:false,depth:0,parts:[V.original+""],original:V.original+"",loc:V.loc}
}}}),(function(g,ai,r){var ar=r(1)["default"];
ai.__esModule=true;
var G=r(4);
var an=r(6);
var K=ar(an);
var b=r(5);
var aa=r(43);
var L=ar(aa);
function l(au){this.value=au
}function c(){}c.prototype={nameLookup:function Q(av,au){if(c.isValidJavaScriptVariableName(au)){return[av,".",au]
}else{return[av,"[",JSON.stringify(au),"]"]
}},depthedLookup:function R(au){return[this.aliasable("container.lookup"),'(depths, "',au,'")']
},compilerInfo:function B(){var av=G.COMPILER_REVISION,au=G.REVISION_CHANGES[av];
return[av,au]
},appendToBuffer:function ap(aw,au,av){if(!b.isArray(aw)){aw=[aw]
}aw=this.source.wrap(aw,au);
if(this.environment.isSimple){return["return ",aw,";"]
}else{if(av){return["buffer += ",aw,";"]
}else{aw.appendToBuffer=true;
return aw
}}},initializeBuffer:function al(){return this.quotedString("")
},compile:function s(ay,aH,av,aB){this.environment=ay;
this.options=aH;
this.stringParams=this.options.stringParams;
this.trackIds=this.options.trackIds;
this.precompile=!aB;
this.name=this.environment.name;
this.isChild=!!av;
this.context=av||{decorators:[],programs:[],environments:[]};
this.preamble();
this.stackSlot=0;
this.stackVars=[];
this.aliases={};
this.registers={list:[]};
this.hashes=[];
this.compileStack=[];
this.inlineStack=[];
this.blockParams=[];
this.compileChildren(ay,aH);
this.useDepths=this.useDepths||ay.useDepths||ay.useDecorators||this.options.compat;
this.useBlockParams=this.useBlockParams||ay.useBlockParams;
var aD=ay.opcodes,az=undefined,aF=undefined,aA=undefined,ax=undefined;
for(aA=0,ax=aD.length;
aA<ax;
aA++){az=aD[aA];
this.source.currentLocation=az.loc;
aF=aF||az.loc;
this[az.opcode].apply(this,az.args)
}this.source.currentLocation=aF;
this.pushSource("");
if(this.stackSlot||this.inlineStack.length||this.compileStack.length){throw new K["default"]("Compile completed with content left on stack")
}if(!this.decorators.isEmpty()){this.useDecorators=true;
this.decorators.prepend("var decorators = container.decorators;\n");
this.decorators.push("return fn;");
if(aB){this.decorators=Function.apply(this,["fn","props","container","depth0","data","blockParams","depths",this.decorators.merge()])
}else{this.decorators.prepend("function(fn, props, container, depth0, data, blockParams, depths) {\n");
this.decorators.push("}\n");
this.decorators=this.decorators.merge()
}}else{this.decorators=undefined
}var aE=this.createFunctionContext(aB);
if(!this.isChild){var aC={compiler:this.compilerInfo(),main:aE};
if(this.decorators){aC.main_d=this.decorators;
aC.useDecorators=true
}var au=this.context;
var aw=au.programs;
var aG=au.decorators;
for(aA=0,ax=aw.length;
aA<ax;
aA++){if(aw[aA]){aC[aA]=aw[aA];
if(aG[aA]){aC[aA+"_d"]=aG[aA];
aC.useDecorators=true
}}}if(this.environment.usePartial){aC.usePartial=true
}if(this.options.data){aC.useData=true
}if(this.useDepths){aC.useDepths=true
}if(this.useBlockParams){aC.useBlockParams=true
}if(this.options.compat){aC.compat=true
}if(!aB){aC.compiler=JSON.stringify(aC.compiler);
this.source.currentLocation={start:{line:1,column:0}};
aC=this.objectLiteral(aC);
if(aH.srcName){aC=aC.toStringWithSourceMap({file:aH.destName});
aC.map=aC.map&&aC.map.toString()
}else{aC=aC.toString()
}}else{aC.compilerOptions=this.options
}return aC
}else{return aE
}},preamble:function Z(){this.lastContext=0;
this.source=new L["default"](this.options.srcName);
this.decorators=new L["default"](this.options.srcName)
},createFunctionContext:function ao(av){var aA="";
var az=this.stackVars.concat(this.registers.list);
if(az.length>0){aA+=", "+az.join(", ")
}var ay=0;
for(var au in this.aliases){var aw=this.aliases[au];
if(this.aliases.hasOwnProperty(au)&&aw.children&&aw.referenceCount>1){aA+=", alias"+ ++ay+"="+au;
aw.children[0]="alias"+ay
}}var aB=["container","depth0","helpers","partials","data"];
if(this.useBlockParams||this.useDepths){aB.push("blockParams")
}if(this.useDepths){aB.push("depths")
}var ax=this.mergeSource(aA);
if(av){aB.push(ax);
return Function.apply(this,aB)
}else{return this.source.wrap(["function(",aB.join(","),") {\n  ",ax,"}"])
}},mergeSource:function E(az){var ax=this.environment.isSimple,aw=!this.forceBuffer,au=undefined,av=undefined,ay=undefined,aA=undefined;
this.source.each(function(aB){if(aB.appendToBuffer){if(ay){aB.prepend("  + ")
}else{ay=aB
}aA=aB
}else{if(ay){if(!av){au=true
}else{ay.prepend("buffer += ")
}aA.add(";");
ay=aA=undefined
}av=true;
if(!ax){aw=false
}}});
if(aw){if(ay){ay.prepend("return ");
aA.add(";")
}else{if(!av){this.source.push('return "";')
}}}else{az+=", buffer = "+(au?"":this.initializeBuffer());
if(ay){ay.prepend("return buffer + ");
aA.add(";")
}else{this.source.push("return buffer;")
}}if(az){this.source.prepend("var "+az.substring(2)+(au?"":";\n"))
}return this.source.merge()
},blockValue:function d(av){var aw=this.aliasable("helpers.blockHelperMissing"),ax=[this.contextName(0)];
this.setupHelperArgs(av,0,ax);
var au=this.popStack();
ax.splice(1,0,au);
this.push(this.source.functionCall(aw,"call",ax))
},ambiguousBlockValue:function V(){var au=this.aliasable("helpers.blockHelperMissing"),aw=[this.contextName(0)];
this.setupHelperArgs("",0,aw,true);
this.flushInline();
var av=this.topStack();
aw.splice(1,0,av);
this.pushSource(["if (!",this.lastHelper,") { ",av," = ",this.source.functionCall(au,"call",aw),"}"])
},appendContent:function f(au){if(this.pendingContent){au=this.pendingContent+au
}else{this.pendingLocation=this.source.currentLocation
}this.pendingContent=au
},append:function ac(){if(this.isInline()){this.replaceStack(function(av){return[" != null ? ",av,' : ""']
});
this.pushSource(this.appendToBuffer(this.popStack()))
}else{var au=this.popStack();
this.pushSource(["if (",au," != null) { ",this.appendToBuffer(au,undefined,true)," }"]);
if(this.environment.isSimple){this.pushSource(["else { ",this.appendToBuffer("''",undefined,true)," }"])
}}},appendEscaped:function o(){this.pushSource(this.appendToBuffer([this.aliasable("container.escapeExpression"),"(",this.popStack(),")"]))
},getContext:function C(au){this.lastContext=au
},pushContext:function v(){this.pushStackLiteral(this.contextName(this.lastContext))
},lookupOnContext:function at(ax,aw,au,ay){var av=0;
if(!ay&&this.options.compat&&!this.lastContext){this.push(this.depthedLookup(ax[av++]))
}else{this.pushContext()
}this.resolvePath("context",ax,av,aw,au)
},lookupBlockParam:function D(av,au){this.useBlockParams=true;
this.push(["blockParams[",av[0],"][",av[1],"]"]);
this.resolvePath("context",au,1)
},lookupData:function q(aw,av,au){if(!aw){this.pushStackLiteral("data")
}else{this.pushStackLiteral("container.data(data, "+aw+")")
}this.resolvePath("data",av,0,true,au)
},resolvePath:function S(ax,az,aw,ay,av){var aA=this;
if(this.options.strict||this.options.assumeObjects){this.push(X(this.options.strict&&av,this,az,ax));
return
}var au=az.length;
for(;
aw<au;
aw++){this.replaceStack(function(aC){var aB=aA.nameLookup(aC,az[aw],ax);
if(!ay){return[" != null ? ",aB," : ",aC]
}else{return[" && ",aB]
}})
}},resolvePossibleLambda:function ah(){this.push([this.aliasable("container.lambda"),"(",this.popStack(),", ",this.contextName(0),")"])
},pushStringParam:function aq(au,av){this.pushContext();
this.pushString(av);
if(av!=="SubExpression"){if(typeof au==="string"){this.pushString(au)
}else{this.pushStackLiteral(au)
}}},emptyHash:function z(au){if(this.trackIds){this.push("{}")
}if(this.stringParams){this.push("{}");
this.push("{}")
}this.pushStackLiteral(au?"undefined":"{}")
},pushHash:function T(){if(this.hash){this.hashes.push(this.hash)
}this.hash={values:[],types:[],contexts:[],ids:[]}
},popHash:function P(){var au=this.hash;
this.hash=this.hashes.pop();
if(this.trackIds){this.push(this.objectLiteral(au.ids))
}if(this.stringParams){this.push(this.objectLiteral(au.contexts));
this.push(this.objectLiteral(au.types))
}this.push(this.objectLiteral(au.values))
},pushString:function m(au){this.pushStackLiteral(this.quotedString(au))
},pushLiteral:function J(au){this.pushStackLiteral(au)
},pushProgram:function x(au){if(au!=null){this.pushStackLiteral(this.programExpression(au))
}else{this.pushStackLiteral(null)
}},registerDecorator:function M(ax,aw){var au=this.nameLookup("decorators",aw,"decorator"),av=this.setupHelperArgs(aw,ax);
this.decorators.push(["fn = ",this.decorators.functionCall(au,"",["fn","props","container",av])," || fn;"])
},invokeHelper:function ab(ay,au,aw){var aA=this.popStack(),av=this.setupHelper(ay,au),az=aw?[av.name," || "]:"";
var ax=["("].concat(az,aA);
if(!this.options.strict){ax.push(" || ",this.aliasable("helpers.helperMissing"))
}ax.push(")");
this.push(this.source.functionCall(ax,"call",av.callParams))
},invokeKnownHelper:function p(aw,au){var av=this.setupHelper(aw,au);
this.push(this.source.functionCall(av.name,"call",av.callParams))
},invokeAmbiguous:function am(au,ay){this.useRegister("helper");
var az=this.popStack();
this.emptyHash();
var av=this.setupHelper(0,au,ay);
var aw=this.lastHelper=this.nameLookup("helpers",au,"helper");
var ax=["(","(helper = ",aw," || ",az,")"];
if(!this.options.strict){ax[0]="(helper = ";
ax.push(" != null ? helper : ",this.aliasable("helpers.helperMissing"))
}this.push(["(",ax,av.paramsInit?["),(",av.paramsInit]:[],"),","(typeof helper === ",this.aliasable('"function"')," ? ",this.source.functionCall("helper","call",av.callParams)," : helper))"])
},invokePartial:function U(av,ax,au){var ay=[],aw=this.setupParams(ax,1,ay);
if(av){ax=this.popStack();
delete aw.name
}if(au){aw.indent=JSON.stringify(au)
}aw.helpers="helpers";
aw.partials="partials";
aw.decorators="container.decorators";
if(!av){ay.unshift(this.nameLookup("partials",ax,"partial"))
}else{ay.unshift(ax)
}if(this.options.compat){aw.depths="depths"
}aw=this.objectLiteral(aw);
ay.push(aw);
this.push(this.source.functionCall("container.invokePartial","",ay))
},assignToHash:function k(av){var ax=this.popStack(),au=undefined,aw=undefined,az=undefined;
if(this.trackIds){az=this.popStack()
}if(this.stringParams){aw=this.popStack();
au=this.popStack()
}var ay=this.hash;
if(au){ay.contexts[av]=au
}if(aw){ay.types[av]=aw
}if(az){ay.ids[av]=az
}ay.values[av]=ax
},pushId:function e(av,au,aw){if(av==="BlockParam"){this.pushStackLiteral("blockParams["+au[0]+"].path["+au[1]+"]"+(aw?" + "+JSON.stringify("."+aw):""))
}else{if(av==="PathExpression"){this.pushString(au)
}else{if(av==="SubExpression"){this.pushStackLiteral("true")
}else{this.pushStackLiteral("null")
}}}},compiler:c,compileChildren:function H(az,aC){var ax=az.children,aw=undefined,au=undefined;
for(var aA=0,ay=ax.length;
aA<ay;
aA++){aw=ax[aA];
au=new this.compiler();
var av=this.matchExistingProgram(aw);
if(av==null){this.context.programs.push("");
var aB=this.context.programs.length;
aw.index=aB;
aw.name="program"+aB;
this.context.programs[aB]=au.compile(aw,aC,this.context,!this.precompile);
this.context.decorators[aB]=au.decorators;
this.context.environments[aB]=aw;
this.useDepths=this.useDepths||au.useDepths;
this.useBlockParams=this.useBlockParams||au.useBlockParams;
aw.useDepths=this.useDepths;
aw.useBlockParams=this.useBlockParams
}else{aw.index=av.index;
aw.name="program"+av.index;
this.useDepths=this.useDepths||av.useDepths;
this.useBlockParams=this.useBlockParams||av.useBlockParams
}}},matchExistingProgram:function u(ax){for(var aw=0,av=this.context.environments.length;
aw<av;
aw++){var au=this.context.environments[aw];
if(au&&au.equals(ax)){return au
}}},programExpression:function I(au){var aw=this.environment.children[au],av=[aw.index,"data",aw.blockParams];
if(this.useBlockParams||this.useDepths){av.push("blockParams")
}if(this.useDepths){av.push("depths")
}return"container.program("+av.join(", ")+")"
},useRegister:function Y(au){if(!this.registers[au]){this.registers[au]=true;
this.registers.list.push(au)
}},push:function ag(au){if(!(au instanceof l)){au=this.source.wrap(au)
}this.inlineStack.push(au);
return au
},pushStackLiteral:function n(au){this.push(new l(au))
},pushSource:function w(au){if(this.pendingContent){this.source.push(this.appendToBuffer(this.source.quotedString(this.pendingContent),this.pendingLocation));
this.pendingContent=undefined
}if(au){this.source.push(au)
}},replaceStack:function a(aB){var az=["("],au=undefined,ay=undefined,aw=undefined;
if(!this.isInline()){throw new K["default"]("replaceStack on non-inline")
}var aA=this.popStack(true);
if(aA instanceof l){au=[aA.value];
az=["(",au];
aw=true
}else{ay=true;
var av=this.incrStack();
az=["((",this.push(av)," = ",aA,")"];
au=this.topStack()
}var ax=aB.call(this,au);
if(!aw){this.popStack()
}if(ay){this.stackSlot--
}this.push(az.concat(ax,")"))
},incrStack:function W(){this.stackSlot++;
if(this.stackSlot>this.stackVars.length){this.stackVars.push("stack"+this.stackSlot)
}return this.topStackName()
},topStackName:function ak(){return"stack"+this.stackSlot
},flushInline:function aj(){var ax=this.inlineStack;
this.inlineStack=[];
for(var aw=0,av=ax.length;
aw<av;
aw++){var ay=ax[aw];
if(ay instanceof l){this.compileStack.push(ay)
}else{var au=this.incrStack();
this.pushSource([au," = ",ay,";"]);
this.compileStack.push(au)
}}},isInline:function y(){return this.inlineStack.length
},popStack:function h(au){var aw=this.isInline(),av=(aw?this.inlineStack:this.compileStack).pop();
if(!au&&av instanceof l){return av.value
}else{if(!aw){if(!this.stackSlot){throw new K["default"]("Invalid stack pop")
}this.stackSlot--
}return av
}},topStack:function F(){var au=this.isInline()?this.inlineStack:this.compileStack,av=au[au.length-1];
if(av instanceof l){return av.value
}else{return av
}},contextName:function af(au){if(this.useDepths&&au){return"depths["+au+"]"
}else{return"depth"+au
}},quotedString:function A(au){return this.source.quotedString(au)
},objectLiteral:function t(au){return this.source.objectLiteral(au)
},aliasable:function ae(av){var au=this.aliases[av];
if(au){au.referenceCount++;
return au
}au=this.aliases[av]=this.source.wrap(av);
au.aliasable=true;
au.referenceCount=1;
return au
},setupHelper:function ad(ay,aw,av){var ax=[],aA=this.setupHelperArgs(aw,ay,ax,av);
var au=this.nameLookup("helpers",aw,"helper"),az=this.aliasable(this.contextName(0)+" != null ? "+this.contextName(0)+" : (container.nullContext || {})");
return{params:ax,paramsInit:aA,name:au,callParams:[az].concat(ax)}
},setupParams:function O(aw,aA,ay){var aF={},aD=[],aE=[],av=[],au=!ay,ax=undefined;
if(au){ay=[]
}aF.name=this.quotedString(aw);
aF.hash=this.popStack();
if(this.trackIds){aF.hashIds=this.popStack()
}if(this.stringParams){aF.hashTypes=this.popStack();
aF.hashContexts=this.popStack()
}var az=this.popStack(),aC=this.popStack();
if(aC||az){aF.fn=aC||"container.noop";
aF.inverse=az||"container.noop"
}var aB=aA;
while(aB--){ax=this.popStack();
ay[aB]=ax;
if(this.trackIds){av[aB]=this.popStack()
}if(this.stringParams){aE[aB]=this.popStack();
aD[aB]=this.popStack()
}}if(au){aF.args=this.source.generateArray(ay)
}if(this.trackIds){aF.ids=this.source.generateArray(av)
}if(this.stringParams){aF.types=this.source.generateArray(aE);
aF.contexts=this.source.generateArray(aD)
}if(this.options.data){aF.data="data"
}if(this.useBlockParams){aF.blockParams="blockParams"
}return aF
},setupHelperArgs:function N(aw,ay,ax,av){var au=this.setupParams(aw,ay,ax);
au=this.objectLiteral(au);
if(av){this.useRegister("options");
ax.push("options");
return["options=",au]
}else{if(ax){ax.push(au);
return""
}else{return au
}}}};
(function(){var au=("break else new var case finally return void catch for switch while continue function this with default if throw delete in try do instanceof typeof abstract enum int short boolean export interface static byte extends long super char final native synchronized class float package throws const goto private transient debugger implements protected volatile double import public let yield await null true false").split(" ");
var ax=c.RESERVED_WORDS={};
for(var aw=0,av=au.length;
aw<av;
aw++){ax[au[aw]]=true
}})();
c.isValidJavaScriptVariableName=function(au){return !c.RESERVED_WORDS[au]&&/^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test(au)
};
function X(aA,ay,az,ax){var av=ay.popStack(),aw=0,au=az.length;
if(aA){au--
}for(;
aw<au;
aw++){av=ay.nameLookup(av,az[aw],ax)
}if(aA){return[ay.aliasable("container.strict"),"(",av,", ",ay.quotedString(az[aw]),")"]
}else{return av
}}ai["default"]=c;
g.exports=ai["default"]
}),(function(b,z,d){z.__esModule=true;
var r=d(5);
var g=undefined;
try{if(false){var u=require("source-map");
g=u.SourceNode
}}catch(h){}if(!g){g=function(A,B,C,D){this.src="";
if(D){this.add(D)
}};
g.prototype={add:function t(A){if(r.isArray(A)){A=A.join("")
}this.src+=A
},prepend:function y(A){if(r.isArray(A)){A=A.join("")
}this.src=A+this.src
},toStringWithSourceMap:function e(){return{code:this.toString()}
},toString:function w(){return this.src
}}
}function c(D,B,F){if(r.isArray(D)){var C=[];
for(var E=0,A=D.length;
E<A;
E++){C.push(B.wrap(D[E],F))
}return C
}else{if(typeof D==="boolean"||typeof D==="number"){return D+""
}}return D
}function m(A){this.srcFile=A;
this.source=[]
}m.prototype={isEmpty:function s(){return !this.source.length
},prepend:function y(A,B){this.source.unshift(this.wrap(A,B))
},push:function n(A,B){this.source.push(this.wrap(A,B))
},merge:function k(){var A=this.empty();
this.each(function(B){A.add(["  ",B,"\n"])
});
return A
},each:function f(B){for(var C=0,A=this.source.length;
C<A;
C++){B(this.source[C])
}},empty:function q(){var A=this.currentLocation||{start:{}};
return new g(A.start.line,A.start.column,this.srcFile)
},wrap:function p(A){var B=arguments.length<=1||arguments[1]===undefined?this.currentLocation||{start:{}}:arguments[1];
if(A instanceof g){return A
}A=c(A,this,B);
return new g(B.start.line,B.start.column,this.srcFile,A)
},functionCall:function x(B,A,C){C=this.generateList(C);
return this.wrap([B,A?"."+A+"(":"(",C,")"])
},quotedString:function o(A){return'"'+(A+"").replace(/\\/g,"\\\\").replace(/"/g,'\\"').replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\u2028/g,"\\u2028").replace(/\u2029/g,"\\u2029")+'"'
},objectLiteral:function v(E){var D=[];
for(var B in E){if(E.hasOwnProperty(B)){var C=c(E[B],this);
if(C!=="undefined"){D.push([this.quotedString(B),":",C])
}}}var A=this.generateList(D);
A.prepend("{");
A.add("}");
return A
},generateList:function a(B){var C=this.empty();
for(var D=0,A=B.length;
D<A;
D++){if(D){C.add(",")
}C.add(c(B[D],this))
}return C
},generateArray:function l(A){var B=this.generateList(A);
B.prepend("[");
B.add("]");
return B
}};
z["default"]=m;
b.exports=z["default"]
})])
});
(function(b,k,f){function g(n){var m=n.length;
if(m!=0){return true
}else{return false
}}function c(n,p,m){if(n.length){var o={};
b.ajax({type:"GET",url:"/content/siliconlabs/en/community"+p,async:true,cache:false,data:o,success:function(q){n.text(m(q))
}})
}}function e(n){var p=n+"=";
var m=k.cookie.split(";");
for(var o=0;
o<m.length;
o++){var q=m[o];
while(q.charAt(0)==" "){q=q.substring(1,q.length)
}if(q.indexOf(p)==0){return q.substring(p.length,q.length)
}}return null
}function h(n){var o=new RegExp("(?:^"+n+"|;s*"+n+")=(.*?)(?:;|$)","g");
var m=o.exec(k.cookie.replace(/\s/g,""));
return(m===null)?null:m[1]
}function a(m,o,n){if(h(m)){d(m,"",-1,o,n)
}}function d(n,q,m,r,p){var o=n+"="+escape(q)+";";
if(m){if(m instanceof Date){if(isNaN(m.getTime())){m=new Date()
}}else{m=new Date(new Date().getTime()+parseInt(m)*1000*60*60*48)
}o+="expires="+m.toGMTString()+";"
}if(r){o+="path="+r+";"
}else{o+="path=/;"
}if(p){o+="domain="+p+";"
}k.cookie=o
}function l(m){m++;
if(_.isUndefined(SCF.Session.attributes.id)&&m<12){setTimeout(l(m),250)
}}b(k).ready(function(){var n=f.navigator.userAgent;
var m=n.indexOf("MSIE ");
if(m<0){setTimeout(function(){if(!b("#isInternalsite").length){var w="";
if(sl.currentRunMode==="dev"||sl.currentRunMode===""){w=e("silabsprofiledev")
}if(sl.currentRunMode==="qa"||sl.currentRunMode==="stage"){w=e("silabsprofileqa")
}if(sl.currentRunMode==="prod"){w=e("silabsprofile")
}var G="";
var x="";
var D="";
var q="";
var z="";
var s="";
var E="";
var y="";
var v=b("body").innerWidth();
var B=b(".menu-account");
E=b(location).attr("href");
var F=false;
if((w!=null&&w!="")){F=true
}if(E.indexOf("/community")>0){var H=0;
l(H);
var I=_.isUndefined(SCF.Session.attributes.id)?"":SCF.Session.attributes.id.substr(SCF.Session.attributes.id.lastIndexOf("/")+1);
if(!I){F=false
}}if(F){var r=b("#top_nav_login");
var C=b("#top_nav_register");
r.hide();
C.hide();
w=w.replace(/\s/g,"");
var J=w.split("|");
var K={};
for(i=0;
i<J.length;
i++){var M=J[i].split("=");
K[M[0]]=decodeURIComponent(M[1])
}var u=K.FirstName;
if(u!=null&&u!=""){B.empty();
B.addClass("logged-in");
var L="<a href='#' class='nav-link'>Welcome, ";
if(v<=689){if(u.length>18){L=L+u.substring(0,18)
}else{L=L+u
}}else{L=L+u
}L=L+"</a>";
var t="<a class='sub-nav-link sl-cta' href='"+K.AccountURL+"'>Account</a>".replace("http://","https://");
if(typeof K.BrainSharkURL!=="undefined"&&K.BrainSharkURL!=""){q="<a class='sub-nav-link sl-cta' href='"+K.BrainSharkURL+"'>Training</a>".replace("http://","https://")
}if(typeof K.SalesGuideURL!=="undefined"&&K.SalesGuideURL!=""){D="<a class='sub-nav-link sl-cta' href='"+K.SalesGuideURL+"'>Sales Guide</a>".replace("http://","https://")
}if(typeof K.NdaURL!=="undefined"&&K.NdaURL!=""){s="<a class='sub-nav-link sl-cta' href='"+K.NdaURL+"'>NDA Content</a>".replace("http://","https://")
}if(typeof K.PartnerPortalURL!=="undefined"&&K.PartnerPortalURL!=""){z="<a class='sub-nav-link sl-cta' href='"+K.PartnerPortalURL+"'>Partner Portal</a>".replace("http://","https://")
}G="<a class='sub-nav-link sl-cta' href='/community/notifications'>Notifications</a>";
x="<a class='sub-nav-link sl-cta' href='/community/messaging'>Messaging</a>";
y="<a class='sub-nav-link sl-cta js-logout' href='"+sl.ssoLogoutUrl+"'>Log Off</a>";
var A="<li class='nav-item'>"+L+"<ul class='sub-nav'>";
A=A+"<li class='sub-nav-item'>"+t+"</li>";
if(E.indexOf("/community")>0){A=A+"<li class='sub-nav-item'>"+G+"</li>";
A=A+"<li class='sub-nav-item'>"+x+"</li>"
}if(q!=""){A=A+"<li class='sub-nav-item'>"+q+"</li>"
}if(D!=""){A=A+"<li class='sub-nav-item'>"+D+"</li>"
}if(s!=""){A=A+"<li class='sub-nav-item'>"+s+"</li>"
}if(z!=""){A=A+"<li class='sub-nav-item'>"+z+"</li>"
}A=A+"<li class='sub-nav-item'>"+y+"</li></ul></li></ul>";
b(".menu-account").html(A)
}}else{B.removeClass("logged-in");
var r=b("#top_nav_login");
var C=b("#top_nav_register");
r.show();
C.show()
}}},200)
}var p=p||{};
p.sl=p.sl||{};
p.sl.login=p.sl.login||function(q,r){b.ajax({url:"/sl/login",type:"GET",success:function(s,u,t){console.log("success!")
},error:function(s,u,t){}})
};
b("#top_nav_login, .login-link").on("click",function(q){q.preventDefault();
d("saml_request_path",k.location.pathname);
k.location=sl.ssoEndpointUrl
});
b("#top_nav_register, .register-link").on("click",function(q){q.preventDefault();
k.location=sl.registrationUrl
});
function o(){var r=k.location.hostname;
var q=r.split(".");
q.shift();
q=q.join(".");
q="."+q;
return q
}b(k).on("click",".js-logout",function(r){r.preventDefault();
a("saml_request_path");
var q=o();
if(sl.currentRunMode==="dev"){a("silabsprofiledev","/",o())
}if(sl.currentRunMode==="qa"||sl.currentRunMode==="stage"){a("silabsprofileqa","/",o())
}if(sl.currentRunMode==="prod"){a("silabsprofile","/",o())
}k.location=sl.ssoLogoutUrl
})
});
b(f).load(function(){var n=f.navigator.userAgent;
var m=n.indexOf("MSIE ");
if(m>0){setTimeout(function(){if(!b("#isInternalsite").length){var u="";
if(sl.currentRunMode==="dev"||sl.currentRunMode===""){u=e("silabsprofiledev")
}if(sl.currentRunMode==="qa"||sl.currentRunMode==="stage"){u=e("silabsprofileqa")
}if(sl.currentRunMode==="prod"){u=e("silabsprofile")
}var E="";
var v="";
var B="";
var o="";
var x="";
var q="";
var C="";
var w="";
var t=b("body").innerWidth();
var z=b(".menu-account");
C=b(location).attr("href");
var D=false;
if((u!=null&&u!="")){D=true
}if(C.indexOf("/community")>0){var F=0;
l(F);
var G=_.isUndefined(SCF.Session.attributes.id)?"":SCF.Session.attributes.id.substr(SCF.Session.attributes.id.lastIndexOf("/")+1);
if(!G){D=false
}}if(D){var p=b("#top_nav_login");
var A=b("#top_nav_register");
p.hide();
A.hide();
u=u.replace(/\s/g,"");
var H=u.split("|");
var I={};
for(i=0;
i<H.length;
i++){var K=H[i].split("=");
I[K[0]]=decodeURIComponent(K[1])
}var s=I.FirstName;
if(s!=null&&s!=""){z.empty();
z.addClass("logged-in");
var J="<a href='#' class='nav-link'>Welcome, ";
if(t<=689){if(s.length>18){J=J+s.substring(0,18)
}else{J=J+s
}}else{J=J+s
}J=J+"</a>";
var r="<a class='sub-nav-link sl-cta' href='"+I.AccountURL+"'>Account</a>".replace("http://","https://");
if(typeof I.BrainSharkURL!=="undefined"&&I.BrainSharkURL!=""){o="<a class='sub-nav-link sl-cta' href='"+I.BrainSharkURL+"'>Training</a>".replace("http://","https://")
}if(typeof I.SalesGuideURL!=="undefined"&&I.SalesGuideURL!=""){B="<a class='sub-nav-link sl-cta' href='"+I.SalesGuideURL+"'>Sales Guide</a>".replace("http://","https://")
}if(typeof I.NdaURL!=="undefined"&&I.NdaURL!=""){q="<a class='sub-nav-link sl-cta' href='"+I.NdaURL+"'>NDA Content</a>".replace("http://","https://")
}if(typeof I.PartnerPortalURL!=="undefined"&&I.PartnerPortalURL!=""){x="<a class='sub-nav-link sl-cta' href='"+I.PartnerPortalURL+"'>Partner Portal</a>".replace("http://","https://")
}E="<a class='sub-nav-link sl-cta' href='/community/notifications'>Notifications</a>";
v="<a class='sub-nav-link sl-cta' href='/community/messaging'>Messaging</a>";
w="<a class='sub-nav-link sl-cta js-logout' href='"+sl.ssoLogoutUrl+"'>Log Off</a>";
var y="<li class='nav-item'>"+J+"<ul class='sub-nav'>";
y=y+"<li class='sub-nav-item'>"+r+"</li>";
if(C.indexOf("/community")>0){y=y+"<li class='sub-nav-item'>"+E+"</li>";
y=y+"<li class='sub-nav-item'>"+v+"</li>"
}if(o!=""){y=y+"<li class='sub-nav-item'>"+o+"</li>"
}if(B!=""){y=y+"<li class='sub-nav-item'>"+B+"</li>"
}if(q!=""){y=y+"<li class='sub-nav-item'>"+q+"</li>"
}if(x!=""){y=y+"<li class='sub-nav-item'>"+x+"</li>"
}y=y+"<li class='sub-nav-item'>"+w+"</li></ul></li></ul>";
b(".menu-account").html(y)
}}else{z.removeClass("logged-in");
var p=b("#top_nav_login");
var A=b("#top_nav_register");
p.show();
A.show()
}}},200)
}})
})(jQuery,document,window);
(function(f,b,e){var a={utils:{setIEClasses:function(){if("ActiveXObject" in e){f("body").addClass("ie")
}if(!String.prototype.startsWith){String.prototype.startsWith=function(h,g){g=g||0;
return this.indexOf(h,g)===g
}
}if(!String.prototype.endsWith){String.prototype.endsWith=function(g){return this.indexOf(g,this.length-suffix.length)!==-1
}
}},setOSClasses:function(){if(navigator.platform.indexOf("Win")>-1){f("body").addClass("os-win")
}if(navigator.platform.indexOf("Mac")>-1){f("body").addClass("os-mac")
}},toggleLanguageSpecificContent:function(){var g="en";
if(f('meta[http-equiv="Content-Language"]').length>0){g=f('meta[http-equiv="Content-Language"]').attr("content").replace("-","_")
}else{var h=e.location.hostname;
if(h.indexOf("tw.")==0){g="zh_tw"
}else{if(h.indexOf("cn.")==0){g="zh_cn"
}else{if(h.indexOf("jp.")==0){g="ja"
}}}}if(location.search.indexOf("lang=")>-1){g=location.search.replace(/.*lang=(.+?)(\&|$).*/g,"$1")
}if(g=="ja_jp"){g="ja"
}f(".hide.lang-"+g).removeClass("hide");
f('.hide[class*="lang-"]').each(function(){var k=f(this);
if(k.siblings(":visible").length==0&&k.is(".lang-en")){k.removeClass("hide")
}})
}},components:{globalHeader:function(){var g=f(".slbs2-header");
f(b).on("click",".nav-link",function(h){h.preventDefault();
if(!Foundation.MediaQuery.atLeast("large")){var l=f(h.currentTarget),k=l.parent();
if(l.hasClass("open")){l.removeClass("open");
k.find(".sub-nav").css({"max-height":0})
}else{f(".nav-link.open").removeClass("open").parent().find(".sub-nav").css({"max-height":0});
l.addClass("open");
k.find(".sub-nav").css({"max-height":"1000px"})
}}});
g.find(".menu-button").on("click",function(){f("body").toggleClass("show-menu")
});
f(e).on("changed.zf.mediaquery",function(l,h,k){if(h!=="medium"&&h!=="small"){f("body").removeClass("show-menu")
}});
a.components.setNavHeight();
f(e).on("resize",function(){a.components.setNavHeight()
})
},setNavHeight:function(){var g=f(".slbs2-header .nav-content");
if(Foundation.MediaQuery.atLeast("large")){g.removeAttr("style")
}else{var h=f(e).innerHeight();
g.css("height",h)
}},headerSearch:function(){var g=function(l){l.preventDefault();
var h="",k="";
h=f("#toptxtKeywordSearch").val();
if(!!h){k="http://search.silabs.com/search?q="+h+"&site=english&client=silabs&proxystylesheet=silabs&getfields=*&filter=0&entsp=a__en_biasing_policy"
}else{k="http://search.silabs.com/search?q="+h+"&site=english&client=silabs&proxystylesheet=silabs"
}b.location=k
};
f("#searchForm").on("submit",g);
f(".js-search-button").on("click",g)
}}};
f(b).on("ready",function(){a.utils.setIEClasses();
a.utils.setOSClasses();
a.utils.toggleLanguageSpecificContent();
if(!isEdit&&!isDesign){a.components.globalHeader();
a.components.headerSearch()
}else{console.log("EDIT MODE: No custom scripts running")
}});
f(b).on("sl.reinit",function(){a.utils.toggleLanguageSpecificContent()
});
var d=f(".scf-journal-entry");
var c=f(".userprofile");
if(c.length>0){f(".scf-user-info-right").append(f(".scf-site-aux"));
f(".scf-site-aux").removeClass("col-md-4 scf-site-content-right");
f(".scf-site-primary").css("width","100%").css("padding","0");
f(".scf-user-profile").show()
}if(d.length>0){f(".scf-site-primary").css("width","100%")
}else{f(".scf-site-aux").show()
}if(f(e).width()<689){f(".sidebar-container").click(function(){f(this).toggleClass("blue");
f(this).find(".fa-caret-right").toggle();
f(this).find(".fa-caret-down").toggle();
f(this).find(".scf-journal-sidebar").toggle("slow",function(){})
})
}})(jQuery,document,window);
function trackMMenuLinks(f,b,c){var d=c.href,g="";
g=-1==f.indexOf("/")?f:f.split("/")[1]+" Menu",_gaq.push(["_trackEvent",g,b,d]),setTimeout(function(){location.href=d
},400)
}function OneLink(c,b){var d="http://"+c+document.location.pathname+document.location.search;
setTimeout(function(){location.href=d
},400)
}var _gaq=_gaq||[],silabsga_uaNumber="";
switch(document.location.hostname){case"cn":case"cn.silabs.com":silabsga_uaNumber="UA-39450515-1";
_gaq.push(["_setAccount",silabsga_uaNumber]),_gaq.push(["_setDomainName","none"]);
break;
case"jp":case"jp.silabs.com":silabsga_uaNumber="UA-39445112-1";
_gaq.push(["_setAccount",silabsga_uaNumber]),_gaq.push(["_setDomainName","none"]);
break
}var silabsga_href=document.location.href,silabsga_loweredHref=silabsga_href.toLowerCase();
if(-1!=silabsga_loweredHref.indexOf("pages/logindocupdate.aspx")){var silabsga_url=silabsGetQueryStringParameterValue(document.location.search,"dl");
silabsga_url="/"==silabsga_url.charAt(0)?silabsga_url:"/"+silabsga_url,silabsTrackDocument(silabsga_url,"","")
}var pluginUrl="https://www.google-analytics.com/plugins/ga/inpage_linkid.js";
_gaq.push(["_require","inpage_linkid",pluginUrl]);
/*!
 DataTables 1.10.13
 ©2008-2016 SpryMedia Ltd - datatables.net/license
*/
(function(a){"function"===typeof define&&define.amd?define(["jquery"],function(b){return a(b,window,document)
}):"object"===typeof exports?module.exports=function(c,b){c||(c=window);
b||(b="undefined"!==typeof window?require("jquery"):require("jquery")(c));
return a(b,c,c.document)
}:a(jQuery,window,document)
})(function(bo,bS,bN,bn){function bx(k){var h,p,m={};
bo.each(k,function(s){if((h=s.match(/^([^A-Z]+?)([A-Z])/))&&-1!=="a aa ai ao as b fn i m o s ".indexOf(h[1]+" ")){p=s.replace(h[0],h[2].toLowerCase()),m[p]=s,"o"===h[1]&&bx(k[s])
}});
k._hungarianMap=m
}function bK(k,h,p){k._hungarianMap||bx(k);
var m;
bo.each(h,function(s){m=k._hungarianMap[s];
if(m!==bn&&(p||h[m]===bn)){"o"===m.charAt(0)?(h[m]||(h[m]={}),bo.extend(!0,h[m],h[s]),bK(k[m],h[m],p)):h[m]=h[s]
}})
}function l(k){var h=bm.defaults.oLanguage,m=k.sZeroRecords;
!k.sEmptyTable&&(m&&"No data available in table"===h.sEmptyTable)&&bR(k,k,"sZeroRecords","sEmptyTable");
!k.sLoadingRecords&&(m&&"Loading..."===h.sLoadingRecords)&&bR(k,k,"sZeroRecords","sLoadingRecords");
k.sInfoThousands&&(k.sThousands=k.sInfoThousands);
(k=k.sDecimal)&&aM(k)
}function aB(k){bW(k,"ordering","bSort");
bW(k,"orderMulti","bSortMulti");
bW(k,"orderClasses","bSortClasses");
bW(k,"orderCellsTop","bSortCellsTop");
bW(k,"order","aaSorting");
bW(k,"orderFixed","aaSortingFixed");
bW(k,"paging","bPaginate");
bW(k,"pagingType","sPaginationType");
bW(k,"pageLength","iDisplayLength");
bW(k,"searching","bFilter");
"boolean"===typeof k.sScrollX&&(k.sScrollX=k.sScrollX?"100%":"");
"boolean"===typeof k.scrollX&&(k.scrollX=k.scrollX?"100%":"");
if(k=k.aoSearchCols){for(var h=0,m=k.length;
h<m;
h++){k[h]&&bK(bm.models.oSearch,k[h])
}}}function at(k){bW(k,"orderable","bSortable");
bW(k,"orderData","aDataSort");
bW(k,"orderSequence","asSorting");
bW(k,"orderDataType","sortDataType");
var h=k.aDataSort;
h&&!bo.isArray(h)&&(k.aDataSort=[h])
}function ak(k){if(!bm.__browser){var h={};
bm.__browser=h;
var s=bo("<div/>").css({position:"fixed",top:0,left:-1*bo(bS).scrollLeft(),height:1,width:1,overflow:"hidden"}).append(bo("<div/>").css({position:"absolute",top:1,left:1,width:100,overflow:"scroll"}).append(bo("<div/>").css({width:"100%",height:10}))).appendTo("body"),p=s.children(),m=p.children();
h.barWidth=p[0].offsetWidth-p[0].clientWidth;
h.bScrollOversize=100===m[0].offsetWidth&&100!==p[0].clientWidth;
h.bScrollbarLeft=1!==Math.round(m.offset().left);
h.bBounding=s[0].getBoundingClientRect().width?!0:!1;
s.remove()
}bo.extend(k.oBrowser,bm.__browser);
k.oScroll.iBarWidth=bm.__browser.barWidth
}function ac(k,h,v,u,t,s){var p,m=!1;
v!==bn&&(p=v,m=!0);
for(;
u!==t;
){k.hasOwnProperty(u)&&(p=m?h(p,k[u],u,k):k[u],m=!0,u+=s)
}return p
}function cm(k,h){var p=bm.defaults.column,m=k.aoColumns.length,p=bo.extend({},bm.models.oColumn,p,{nTh:h?h:bN.createElement("th"),sTitle:p.sTitle?p.sTitle:h?h.innerHTML:"",aDataSort:p.aDataSort?p.aDataSort:[m],mData:p.mData?p.mData:m,idx:m});
k.aoColumns.push(p);
p=k.aoPreSearchCols;
p[m]=bo.extend({},bm.models.oSearch,p[m]);
b(k,m,bo(h).data())
}function b(w,v,u){var v=w.aoColumns[v],t=w.oClasses,s=bo(v.nTh);
if(!v.sWidthOrig){v.sWidthOrig=s.attr("width")||null;
var p=(s.attr("style")||"").match(/width:\s*(\d+[pxem%]+)/);
p&&(v.sWidthOrig=p[1])
}u!==bn&&null!==u&&(at(u),bK(bm.defaults.column,u),u.mDataProp!==bn&&!u.mData&&(u.mData=u.mDataProp),u.sType&&(v._sManualType=u.sType),u.className&&!u.sClass&&(u.sClass=u.className),bo.extend(v,u),bR(v,u,"sWidth","sWidthOrig"),u.iDataSort!==bn&&(v.aDataSort=[u.iDataSort]),bR(v,u,"aDataSort"));
var m=v.mData,h=bE(m),k=v.mRender?bE(v.mRender):null,u=function(x){return"string"===typeof x&&-1!==x.indexOf("@")
};
v._bAttrSrc=bo.isPlainObject(m)&&(u(m.sort)||u(m.type)||u(m.filter));
v._setter=null;
v.fnGetData=function(y,x,A){var z=h(y,x,bn,A);
return k&&x?k(z,x,y,A):z
};
v.fnSetData=function(y,x,z){return bC(m)(y,x,z)
};
"number"!==typeof m&&(w._rowReadObject=!0);
w.oFeatures.bSort||(v.bSortable=!1,s.addClass(t.sSortableNone));
w=-1!==bo.inArray("asc",v.asSorting);
u=-1!==bo.inArray("desc",v.asSorting);
!v.bSortable||!w&&!u?(v.sSortingClass=t.sSortableNone,v.sSortingClassJUI=""):w&&!u?(v.sSortingClass=t.sSortableAsc,v.sSortingClassJUI=t.sSortJUIAscAllowed):!w&&u?(v.sSortingClass=t.sSortableDesc,v.sSortingClassJUI=t.sSortJUIDescAllowed):(v.sSortingClass=t.sSortable,v.sSortingClassJUI=t.sSortJUI)
}function bw(k){if(!1!==k.oFeatures.bAutoWidth){var h=k.aoColumns;
ce(k);
for(var p=0,m=h.length;
p<m;
p++){h[p].nTh.style.width=h[p].sWidth
}}h=k.oScroll;
(""!==h.sY||""!==h.sX)&&cg(k);
bj(k,null,"column-sizing",[k])
}function b9(k,h){var m=b2(k,"bVisible");
return"number"===typeof m[h]?m[h]:null
}function b8(k,h){var m=b2(k,"bVisible"),m=bo.inArray(h,m);
return -1!==m?m:null
}function bQ(k){var h=0;
bo.each(k.aoColumns,function(m,p){p.bVisible&&"none"!==bo(p.nTh).css("display")&&h++
});
return h
}function b2(k,h){var m=[];
bo.map(k.aoColumns,function(p,s){p[h]&&m.push(s)
});
return m
}function b0(B){var A=B.aoColumns,z=B.aoData,y=bm.ext.type.detect,x,w,v,s,t,u,p,m,k;
x=0;
for(w=A.length;
x<w;
x++){if(p=A[x],k=[],!p.sType&&p._sManualType){p.sType=p._sManualType
}else{if(!p.sType){v=0;
for(s=y.length;
v<s;
v++){t=0;
for(u=z.length;
t<u;
t++){k[t]===bn&&(k[t]=bV(B,t,x,"type"));
m=y[v](k[t],B);
if(!m&&v!==y.length-1){break
}if("html"===m){break
}}if(m){p.sType=m;
break
}}p.sType||(p.sType="string")
}}}}function n(z,y,x,w){var v,u,t,p,s,k,m=z.aoColumns;
if(y){for(v=y.length-1;
0<=v;
v--){k=y[v];
var h=k.targets!==bn?k.targets:k.aTargets;
bo.isArray(h)||(h=[h]);
u=0;
for(t=h.length;
u<t;
u++){if("number"===typeof h[u]&&0<=h[u]){for(;
m.length<=h[u];
){cm(z)
}w(h[u],k)
}else{if("number"===typeof h[u]&&0>h[u]){w(m.length+h[u],k)
}else{if("string"===typeof h[u]){p=0;
for(s=m.length;
p<s;
p++){("_all"==h[u]||bo(m[p].nTh).hasClass(h[u]))&&w(p,k)
}}}}}}}if(x){v=0;
for(z=x.length;
v<z;
v++){w(v,x[v])
}}}function bH(w,v,u,t){var s=w.aoData.length,p=bo.extend(!0,{},bm.models.oRow,{src:u?"dom":"data",idx:s});
p._aData=v;
w.aoData.push(p);
for(var m=w.aoColumns,h=0,k=m.length;
h<k;
h++){m[h].sType=null
}w.aiDisplayMaster.push(s);
v=w.rowIdFn(v);
v!==bn&&(w.aIds[v]=p);
(u||!w.oFeatures.bDeferRender)&&bt(w,s,u,t);
return s
}function bv(k,h){var m;
h instanceof bo||(h=bo(h));
return h.map(function(p,s){m=a6(k,s);
return bH(k,m.data,s,m.cells)
})
}function bV(w,v,u,t){var s=w.iDraw,p=w.aoColumns[u],m=w.aoData[v]._aData,h=p.sDefaultContent,k=p.fnGetData(m,t,{settings:w,row:v,col:u});
if(k===bn){return w.iDrawError!=s&&null===h&&(bJ(w,0,"Requested unknown parameter "+("function"==typeof p.mData?"{function}":"'"+p.mData+"'")+" for row "+v+", column "+u,4),w.iDrawError=s),h
}if((k===m||null===k)&&null!==h&&t!==bn){k=h
}else{if("function"===typeof k){return k.call(m)
}}return null===k&&"display"==t?"":k
}function a(k,h,p,m){k.aoColumns[p].fnSetData(k.aoData[h]._aData,m,{settings:k,row:h,col:p})
}function aX(h){return bo.map(h.match(/(\\.|[^\.])+/g)||[""],function(k){return k.replace(/\\\./g,".")
})
}function bE(k){if(bo.isPlainObject(k)){var h={};
bo.each(k,function(p,s){s&&(h[p]=bE(s))
});
return function(p,v,u,t){var s=h[v]||h._;
return s!==bn?s(p,v,u,t):p
}
}if(null===k){return function(p){return p
}
}if("function"===typeof k){return function(p,u,t,s){return k(p,u,t,s)
}
}if("string"===typeof k&&(-1!==k.indexOf(".")||-1!==k.indexOf("[")||-1!==k.indexOf("("))){var m=function(s,p,w){var v,t;
if(""!==w){t=aX(w);
for(var u=0,x=t.length;
u<x;
u++){w=t[u].match(br);
v=t[u].match(bA);
if(w){t[u]=t[u].replace(br,"");
""!==t[u]&&(s=s[t[u]]);
v=[];
t.splice(0,u+1);
t=t.join(".");
if(bo.isArray(s)){u=0;
for(x=s.length;
u<x;
u++){v.push(m(s[u],p,t))
}}s=w[0].substring(1,w[0].length-1);
s=""===s?v:v.join(s);
break
}else{if(v){t[u]=t[u].replace(bA,"");
s=s[t[u]]();
continue
}}if(null===s||s[t[u]]===bn){return bn
}s=s[t[u]]
}}return s
};
return function(p,s){return m(p,s,k)
}
}return function(p){return p[k]
}
}function bC(k){if(bo.isPlainObject(k)){return bC(k._)
}if(null===k){return function(){}
}if("function"===typeof k){return function(m,s,p){k(m,"set",s,p)
}
}if("string"===typeof k&&(-1!==k.indexOf(".")||-1!==k.indexOf("[")||-1!==k.indexOf("("))){var h=function(m,w,v){var v=aX(v),u;
u=v[v.length-1];
for(var t,p,s=0,x=v.length-1;
s<x;
s++){t=v[s].match(br);
p=v[s].match(bA);
if(t){v[s]=v[s].replace(br,"");
m[v[s]]=[];
u=v.slice();
u.splice(0,s+1);
t=u.join(".");
if(bo.isArray(w)){p=0;
for(x=w.length;
p<x;
p++){u={},h(u,w[p],t),m[v[s]].push(u)
}}else{m[v[s]]=w
}return
}p&&(v[s]=v[s].replace(bA,""),m=m[v[s]](w));
if(null===m[v[s]]||m[v[s]]===bn){m[v[s]]={}
}m=m[v[s]]
}if(u.match(bA)){m[u.replace(bA,"")](w)
}else{m[u.replace(br,"")]=w
}};
return function(p,m){return h(p,m,k)
}
}return function(m,p){m[k]=p
}
}function aP(h){return bT(h.aoData,"_aData")
}function a9(h){h.aoData.length=0;
h.aiDisplayMaster.length=0;
h.aiDisplay.length=0;
h.aIds={}
}function aZ(k,h,t){for(var s=-1,p=0,m=k.length;
p<m;
p++){k[p]==h?s=p:k[p]>h&&k[p]--
}-1!=s&&t===bn&&k.splice(s,1)
}function a4(k,h,v,u){var t=k.aoData[h],s,p=function(x,w){for(;
x.childNodes.length;
){x.removeChild(x.firstChild)
}x.innerHTML=bV(k,h,w,"display")
};
if("dom"===v||(!v||"auto"===v)&&"dom"===t.src){t._aData=a6(k,t,u,u===bn?bn:t._aData).data
}else{var m=t.anCells;
if(m){if(u!==bn){p(m[u],u)
}else{v=0;
for(s=m.length;
v<s;
v++){p(m[v],v)
}}}}t._aSortData=null;
t._aFilterData=null;
p=k.aoColumns;
if(u!==bn){p[u].sType=null
}else{v=0;
for(s=p.length;
v<s;
v++){p[v].sType=null
}aE(k,t)
}}function a6(C,B,A,z){var y=[],x=B.firstChild,w,u,v=0,p,t=C.aoColumns,k=C._rowReadObject,z=z!==bn?z:k?{}:[],h=function(D,m){if("string"===typeof D){var E=D.indexOf("@");
-1!==E&&(E=D.substring(E+1),bC(D)(z,m.getAttribute(E)))
}},s=function(m){if(A===bn||A===v){u=t[v],p=bo.trim(m.innerHTML),u&&u._bAttrSrc?(bC(u.mData._)(z,p),h(u.mData.sort,m),h(u.mData.type,m),h(u.mData.filter,m)):k?(u._setter||(u._setter=bC(u.mData)),u._setter(z,p)):z[v]=p
}v++
};
if(x){for(;
x;
){w=x.nodeName.toUpperCase();
if("TD"==w||"TH"==w){s(x),y.push(x)
}x=x.nextSibling
}}else{y=B.anCells;
x=0;
for(w=y.length;
x<w;
x++){s(y[x])
}}if(B=B.firstChild?B:B.nTr){(B=B.getAttribute("id"))&&bC(C.rowId)(z,B)
}return{data:z,cells:y}
}function bt(z,y,x,w){var v=z.aoData[y],u=v._aData,t=[],p,s,k,m,h;
if(null===v.nTr){p=x||bN.createElement("tr");
v.nTr=p;
v.anCells=t;
p._DT_RowIndex=y;
aE(z,v);
m=0;
for(h=z.aoColumns.length;
m<h;
m++){k=z.aoColumns[m];
s=x?w[m]:bN.createElement(k.sCellType);
s._DT_CellIndex={row:y,column:m};
t.push(s);
if((!x||k.mRender||k.mData!==m)&&(!bo.isPlainObject(k.mData)||k.mData._!==m+".display")){s.innerHTML=bV(z,y,m,"display")
}k.sClass&&(s.className+=" "+k.sClass);
k.bVisible&&!x?p.appendChild(s):!k.bVisible&&x&&s.parentNode.removeChild(s);
k.fnCreatedCell&&k.fnCreatedCell.call(z.oInstance,s,bV(z,y,m),u,y,m)
}bj(z,"aoRowCreatedCallback",null,[p,u,y])
}v.nTr.setAttribute("role","row")
}function aE(k,h){var s=h.nTr,p=h._aData;
if(s){var m=k.rowIdFn(p);
m&&(s.id=m);
p.DT_RowClass&&(m=p.DT_RowClass.split(" "),h.__rowc=h.__rowc?aG(h.__rowc.concat(m)):m,bo(s).removeClass(h.__rowc.join(" ")).addClass(p.DT_RowClass));
p.DT_RowAttr&&bo(s).attr(p.DT_RowAttr);
p.DT_RowData&&bo(s).data(p.DT_RowData)
}}function cf(y){var x,w,v,u,t,s=y.nTHead,m=y.nTFoot,p=0===bo("th, td",s).length,h=y.oClasses,k=y.aoColumns;
p&&(u=bo("<tr/>").appendTo(s));
x=0;
for(w=k.length;
x<w;
x++){t=k[x],v=bo(t.nTh).addClass(t.sClass),p&&v.appendTo(u),y.oFeatures.bSort&&(v.addClass(t.sSortingClass),!1!==t.bSortable&&(v.attr("tabindex",y.iTabIndex).attr("aria-controls",y.sTableId),aw(y,t.nTh,x))),t.sTitle!=v[0].innerHTML&&v.html(t.sTitle),an(y,"header")(y,v,t,h)
}p&&aV(y.aoHeader,s);
bo(s).find(">tr").attr("role","row");
bo(s).find(">tr>th, >tr>td").addClass(h.sHeaderTH);
bo(m).find(">tr>th, >tr>td").addClass(h.sFooterTH);
if(null!==m){y=y.aoFooter[0];
x=0;
for(w=y.length;
x<w;
x++){t=k[x],t.nTf=y[x].cell,t.sClass&&bo(t.nTf).addClass(t.sClass)
}}}function aN(x,w,v){var u,t,s,p=[],k=[],m=x.aoColumns.length,h;
if(w){v===bn&&(v=!1);
u=0;
for(t=w.length;
u<t;
u++){p[u]=w[u].slice();
p[u].nTr=w[u].nTr;
for(s=m-1;
0<=s;
s--){!x.aoColumns[s].bVisible&&!v&&p[u].splice(s,1)
}k.push([])
}u=0;
for(t=p.length;
u<t;
u++){if(x=p[u].nTr){for(;
s=x.firstChild;
){x.removeChild(s)
}}s=0;
for(w=p[u].length;
s<w;
s++){if(h=m=1,k[u][s]===bn){x.appendChild(p[u][s].cell);
for(k[u][s]=1;
p[u+m]!==bn&&p[u][s].cell==p[u+m][s].cell;
){k[u+m][s]=1,m++
}for(;
p[u][s+h]!==bn&&p[u][s].cell==p[u][s+h].cell;
){for(v=0;
v<m;
v++){k[u+v][s+h]=1
}h++
}bo(p[u][s].cell).attr("rowspan",m).attr("colspan",h)
}}}}}function bG(A){var z=bj(A,"aoPreDrawCallback","preDraw",[A]);
if(-1!==bo.inArray(!1,z)){bU(A,!1)
}else{var z=[],y=0,x=A.asStripeClasses,w=x.length,v=A.oLanguage,u=A.iInitDisplayStart,s="ssp"==bd(A),t=A.aiDisplay;
A.bDrawing=!0;
u!==bn&&-1!==u&&(A._iDisplayStart=s?u:u>=A.fnRecordsDisplay()?0:u,A.iInitDisplayStart=-1);
var u=A._iDisplayStart,m=A.fnDisplayEnd();
if(A.bDeferLoading){A.bDeferLoading=!1,A.iDraw++,bU(A,!1)
}else{if(s){if(!A.bDestroying&&!b1(A)){return
}}else{A.iDraw++
}}if(0!==t.length){v=s?A.aoData.length:m;
for(s=s?0:u;
s<v;
s++){var p=t[s],k=A.aoData[p];
null===k.nTr&&bt(A,p);
p=k.nTr;
if(0!==w){var h=x[y%w];
k._sRowStripe!=h&&(bo(p).removeClass(k._sRowStripe).addClass(h),k._sRowStripe=h)
}bj(A,"aoRowCallback",null,[p,k._aData,y,s]);
z.push(p);
y++
}}else{y=v.sZeroRecords,1==A.iDraw&&"ajax"==bd(A)?y=v.sLoadingRecords:v.sEmptyTable&&0===A.fnRecordsTotal()&&(y=v.sEmptyTable),z[0]=bo("<tr/>",{"class":w?x[0]:""}).append(bo("<td />",{valign:"top",colSpan:bQ(A),"class":A.oClasses.sRowEmpty}).html(y))[0]
}bj(A,"aoHeaderCallback","header",[bo(A.nTHead).children("tr")[0],aP(A),u,m,t]);
bj(A,"aoFooterCallback","footer",[bo(A.nTFoot).children("tr")[0],aP(A),u,m,t]);
x=bo(A.nTBody);
x.children().detach();
x.append(bo(z));
bj(A,"aoDrawCallback","draw",[A]);
A.bSorted=!1;
A.bFiltered=!1;
A.bDrawing=!1
}}function bB(k,h){var p=k.oFeatures,m=p.bFilter;
p.bSort&&bu(k);
m?aC(k,k.oPreviousSearch):k.aiDisplay=k.aiDisplayMaster.slice();
!0!==h&&(k._iDisplayStart=0);
k._drawHold=h;
bG(k);
k._drawHold=!1
}function a7(B){var A=B.oClasses,z=bo(B.nTable),z=bo("<div/>").insertBefore(z),y=B.oFeatures,x=bo("<div/>",{id:B.sTableId+"_wrapper","class":A.sWrapper+(B.nTFoot?"":" "+A.sNoFooter)});
B.nHolding=z[0];
B.nTableWrapper=x[0];
B.nTableReinsertBefore=B.nTable.nextSibling;
for(var w=B.sDom.split(""),v,t,u,m,p,h,s=0;
s<w.length;
s++){v=null;
t=w[s];
if("<"==t){u=bo("<div/>")[0];
m=w[s+1];
if("'"==m||'"'==m){p="";
for(h=2;
w[s+h]!=m;
){p+=w[s+h],h++
}"H"==p?p=A.sJUIHeader:"F"==p&&(p=A.sJUIFooter);
-1!=p.indexOf(".")?(m=p.split("."),u.id=m[0].substr(1,m[0].length-1),u.className=m[1]):"#"==p.charAt(0)?u.id=p.substr(1,p.length-1):u.className=p;
s+=h
}x.append(u);
x=bo(u)
}else{if(">"==t){x=x.parent()
}else{if("l"==t&&y.bPaginate&&y.bLengthChange){v=aY(B)
}else{if("f"==t&&y.bFilter){v=aQ(B)
}else{if("r"==t&&y.bProcessing){v=aF(B)
}else{if("t"==t){v=ax(B)
}else{if("i"==t&&y.bInfo){v=ao(B)
}else{if("p"==t&&y.bPaginate){v=ag(B)
}else{if(0!==bm.ext.feature.length){u=bm.ext.feature;
h=0;
for(m=u.length;
h<m;
h++){if(t==u[h].cFeature){v=u[h].fnInit(B);
break
}}}}}}}}}}}v&&(u=B.aanFeatures,u[t]||(u[t]=[]),u[t].push(v),x.append(v))
}z.replaceWith(x);
B.nHolding=null
}function aV(B,A){var z=bo(A).children("tr"),y,x,w,v,t,u,m,p,h,s;
B.splice(0,B.length);
w=0;
for(u=z.length;
w<u;
w++){B.push([])
}w=0;
for(u=z.length;
w<u;
w++){y=z[w];
for(x=y.firstChild;
x;
){if("TD"==x.nodeName.toUpperCase()||"TH"==x.nodeName.toUpperCase()){p=1*x.getAttribute("colspan");
h=1*x.getAttribute("rowspan");
p=!p||0===p||1===p?1:p;
h=!h||0===h||1===h?1:h;
v=0;
for(t=B[w];
t[v];
){v++
}m=v;
s=1===p?!0:!1;
for(t=0;
t<p;
t++){for(v=0;
v<h;
v++){B[w+v][m+t]={cell:x,unique:s},B[w+v].nTr=y
}}}x=x.nextSibling
}}}function ay(k,h,u){var t=[];
u||(u=k.aoHeader,h&&(u=[],aV(u,h)));
for(var h=0,s=u.length;
h<s;
h++){for(var p=0,m=u[h].length;
p<m;
p++){if(u[h][p].unique&&(!t[p]||!k.bSortCellsTop)){t[p]=u[h][p].cell
}}}return t
}function ap(x,w,v){bj(x,"aoServerParams","serverParams",[w]);
if(w&&bo.isArray(w)){var u={},t=/(.*?)\[\]$/;
bo.each(w,function(z,y){var A=y.name.match(t);
A?(A=A[0],u[A]||(u[A]=[]),u[A].push(y.value)):u[y.name]=y.value
});
w=u
}var s,p=x.ajax,k=x.oInstance,m=function(y){bj(x,null,"xhr",[x,y,x.jqXHR]);
v(y)
};
if(bo.isPlainObject(p)&&p.data){s=p.data;
var h=bo.isFunction(s)?s(w,x):s,w=bo.isFunction(s)&&h?h:bo.extend(!0,w,h);
delete p.data
}h={data:w,success:function(y){var z=y.error||y.sError;
z&&bJ(x,0,z);
x.json=y;
m(y)
},dataType:"json",cache:!1,type:x.sServerMethod,error:function(y,A){var z=bj(x,null,"xhr",[x,null,x.jqXHR]);
-1===bo.inArray(!0,z)&&("parsererror"==A?bJ(x,0,"Invalid JSON response",1):4===y.readyState&&bJ(x,0,"Ajax error",7));
bU(x,!1)
}};
x.oAjaxData=w;
bj(x,null,"preXhr",[x,w]);
x.fnServerData?x.fnServerData.call(k,x.sAjaxSource,bo.map(w,function(z,y){return{name:y,value:z}
}),m,x):x.sAjaxSource||"string"===typeof p?x.jqXHR=bo.ajax(bo.extend(h,{url:p||x.sAjaxSource})):bo.isFunction(p)?x.jqXHR=p.call(k,w,m,x):(x.jqXHR=bo.ajax(bo.extend(h,p)),p.data=s)
}function b1(h){return h.bAjaxDataGet?(h.iDraw++,bU(h,!0),ap(h,L(h),function(k){e(h,k)
}),!1):!0
}function L(C){var B=C.aoColumns,A=B.length,z=C.oFeatures,y=C.oPreviousSearch,x=C.aoPreSearchCols,w,u=[],v,p,s,t=bz(C);
w=C._iDisplayStart;
v=!1!==z.bPaginate?C._iDisplayLength:-1;
var h=function(D,k){u.push({name:D,value:k})
};
h("sEcho",C.iDraw);
h("iColumns",A);
h("sColumns",bT(B,"sName").join(","));
h("iDisplayStart",w);
h("iDisplayLength",v);
var m={draw:C.iDraw,columns:[],order:[],start:w,length:v,search:{value:y.sSearch,regex:y.bRegex}};
for(w=0;
w<A;
w++){p=B[w],s=x[w],v="function"==typeof p.mData?"function":p.mData,m.columns.push({data:v,name:p.sName,searchable:p.bSearchable,orderable:p.bSortable,search:{value:s.sSearch,regex:s.bRegex}}),h("mDataProp_"+w,v),z.bFilter&&(h("sSearch_"+w,s.sSearch),h("bRegex_"+w,s.bRegex),h("bSearchable_"+w,p.bSearchable)),z.bSort&&h("bSortable_"+w,p.bSortable)
}z.bFilter&&(h("sSearch",y.sSearch),h("bRegex",y.bRegex));
z.bSort&&(bo.each(t,function(D,k){m.order.push({column:k.col,dir:k.dir});
h("iSortCol_"+D,k.col);
h("sSortDir_"+D,k.dir)
}),h("iSortingCols",t.length));
B=bm.ext.legacy.ajax;
return null===B?C.sAjaxSource?u:m:B?u:m
}function e(k,h){var t=ah(k,h),s=h.sEcho!==bn?h.sEcho:h.draw,p=h.iTotalRecords!==bn?h.iTotalRecords:h.recordsTotal,m=h.iTotalDisplayRecords!==bn?h.iTotalDisplayRecords:h.recordsFiltered;
if(s){if(1*s<k.iDraw){return
}k.iDraw=1*s
}a9(k);
k._iRecordsTotal=parseInt(p,10);
k._iRecordsDisplay=parseInt(m,10);
s=0;
for(p=t.length;
s<p;
s++){bH(k,t[s])
}k.aiDisplay=k.aiDisplayMaster.slice();
k.bAjaxDataGet=!1;
bG(k);
k._bInitComplete||P(k,h);
k.bAjaxDataGet=!0;
bU(k,!1)
}function ah(k,h){var m=bo.isPlainObject(k.ajax)&&k.ajax.dataSrc!==bn?k.ajax.dataSrc:k.sAjaxDataProp;
return"data"===m?h.aaData||h[m]:""!==m?bE(m)(h):h
}function aQ(w){var v=w.oClasses,u=w.sTableId,t=w.oLanguage,s=w.oPreviousSearch,p=w.aanFeatures,m='<input type="search" class="'+v.sFilterInput+'"/>',h=t.sSearch,h=h.match(/_INPUT_/)?h.replace("_INPUT_",m):h+m,v=bo("<div/>",{id:!p.f?u+"_filter":null,"class":v.sFilter}).append(bo("<label/>").append(h)),p=function(){var x=!this.value?"":this.value;
x!=s.sSearch&&(aC(w,{sSearch:x,bRegex:s.bRegex,bSmart:s.bSmart,bCaseInsensitive:s.bCaseInsensitive}),w._iDisplayStart=0,bG(w))
},m=null!==w.searchDelay?w.searchDelay:"ssp"===bd(w)?400:0,k=bo("input",v).val(s.sSearch).attr("placeholder",t.sSearchPlaceholder).on("keyup.DT search.DT input.DT paste.DT cut.DT",m?af(p,m):p).on("keypress.DT",function(x){if(13==x.keyCode){return !1
}}).attr("aria-controls",u);
bo(w.nTable).on("search.dt.DT",function(x,z){if(w===z){try{k[0]!==bN.activeElement&&k.val(s.sSearch)
}catch(y){}}});
return v[0]
}function aC(k,h,t){var s=k.oPreviousSearch,p=k.aoPreSearchCols,m=function(u){s.sSearch=u.sSearch;
s.bRegex=u.bRegex;
s.bSmart=u.bSmart;
s.bCaseInsensitive=u.bCaseInsensitive
};
b0(k);
if("ssp"!=bd(k)){cj(k,h.sSearch,t,h.bEscapeRegex!==bn?!h.bEscapeRegex:h.bRegex,h.bSmart,h.bCaseInsensitive);
m(h);
for(h=0;
h<p.length;
h++){b5(k,p[h].sSearch,h,p[h].bEscapeRegex!==bn?!p[h].bEscapeRegex:p[h].bRegex,p[h].bSmart,p[h].bCaseInsensitive)
}aJ(k)
}else{m(h)
}k.bFiltered=!0;
bj(k,null,"search",[k])
}function aJ(x){for(var w=bm.ext.search,v=x.aiDisplay,u,t,s=0,p=w.length;
s<p;
s++){for(var k=[],m=0,h=v.length;
m<h;
m++){t=v[m],u=x.aoData[t],w[s](x,u._aFilterData,t,u._aData,m)&&k.push(t)
}v.length=0;
bo.merge(v,k)
}}function b5(k,h,v,u,t,s){if(""!==h){for(var p=[],m=k.aiDisplay,u=r(h,u,t,s),t=0;
t<m.length;
t++){h=k.aoData[m[t]]._aFilterData[v],u.test(h)&&p.push(m[t])
}k.aiDisplay=p
}}function cj(k,h,v,u,t,s){var u=r(h,u,t,s),s=k.oPreviousSearch.sSearch,p=k.aiDisplayMaster,m,t=[];
0!==bm.ext.search.length&&(v=!0);
m=az(k);
if(0>=h.length){k.aiDisplay=p.slice()
}else{if(m||v||s.length>h.length||0!==h.indexOf(s)||k.bSorted){k.aiDisplay=p.slice()
}h=k.aiDisplay;
for(v=0;
v<h.length;
v++){u.test(k.aoData[h[v]]._sFilterRow)&&t.push(h[v])
}k.aiDisplay=t
}}function r(k,h,p,m){k=h?k:d(k);
p&&(k="^(?=.*?"+bo.map(k.match(/"[^"]+"|[^ ]+/g)||[""],function(t){if('"'===t.charAt(0)){var s=t.match(/^"(.*)"$/),t=s?s[1]:t
}return t.replace('"',"")
}).join(")(?=.*?")+").*$");
return RegExp(k,m?"i":"")
}function az(z){var y=z.aoColumns,x,w,v,u,t,m,p,s,k=bm.ext.type.search;
x=!1;
w=0;
for(u=z.aoData.length;
w<u;
w++){if(s=z.aoData[w],!s._aFilterData){m=[];
v=0;
for(t=y.length;
v<t;
v++){x=y[v],x.bSearchable?(p=bV(z,w,v,"filter"),k[x.sType]&&(p=k[x.sType](p)),null===p&&(p=""),"string"!==typeof p&&p.toString&&(p=p.toString())):p="",p.indexOf&&-1!==p.indexOf("&")&&(f.innerHTML=p,p=bX?f.textContent:f.innerText),p.replace&&(p=p.replace(/[\r\n]/g,"")),m.push(p)
}s._aFilterData=m;
s._sFilterRow=m.join("  ");
x=!0
}}return x
}function aq(h){return{search:h.sSearch,smart:h.bSmart,regex:h.bRegex,caseInsensitive:h.bCaseInsensitive}
}function ai(h){return{sSearch:h.search,bSmart:h.smart,bRegex:h.regex,bCaseInsensitive:h.caseInsensitive}
}function ao(k){var h=k.sTableId,p=k.aanFeatures.i,m=bo("<div/>",{"class":k.oClasses.sInfo,id:!p?h+"_info":null});
p||(k.aoDrawCallback.push({fn:Q,sName:"information"}),m.attr("role","status").attr("aria-live","polite"),bo(k.nTable).attr("aria-describedby",h+"_info"));
return m[0]
}function Q(k){var h=k.aanFeatures.i;
if(0!==h.length){var v=k.oLanguage,u=k._iDisplayStart+1,t=k.fnDisplayEnd(),s=k.fnRecordsTotal(),p=k.fnRecordsDisplay(),m=p?v.sInfo:v.sInfoEmpty;
p!==s&&(m+=" "+v.sInfoFiltered);
m+=v.sInfoPostFix;
m=g(k,m);
v=v.fnInfoCallback;
null!==v&&(m=v.call(k.oInstance,k,u,t,s,p,m));
bo(h).html(m)
}}function g(k,h){var u=k.fnFormatNumber,t=k._iDisplayStart+1,s=k._iDisplayLength,p=k.fnRecordsDisplay(),m=-1===s;
return h.replace(/_START_/g,u.call(k,t)).replace(/_END_/g,u.call(k,k.fnDisplayEnd())).replace(/_MAX_/g,u.call(k,k.fnRecordsTotal())).replace(/_TOTAL_/g,u.call(k,p)).replace(/_PAGE_/g,u.call(k,m?1:Math.ceil(t/s))).replace(/_PAGES_/g,u.call(k,m?1:Math.ceil(p/s)))
}function au(k){var h,u,t=k.iInitDisplayStart,s=k.aoColumns,p;
u=k.oFeatures;
var m=k.bDeferLoading;
if(k.bInitialised){a7(k);
cf(k);
aN(k,k.aoHeader);
aN(k,k.aoFooter);
bU(k,!0);
u.bAutoWidth&&ce(k);
h=0;
for(u=s.length;
h<u;
h++){p=s[h],p.sWidth&&(p.nTh.style.width=bg(p.sWidth))
}bj(k,null,"preInit",[k]);
bB(k);
s=bd(k);
if("ssp"!=s||m){"ajax"==s?ap(k,[],function(w){var v=ah(k,w);
for(h=0;
h<v.length;
h++){bH(k,v[h])
}k.iInitDisplayStart=t;
bB(k);
bU(k,!1);
P(k,w)
},k):(bU(k,!1),P(k))
}}else{setTimeout(function(){au(k)
},200)
}}function P(k,h){k._bInitComplete=!0;
(h||k.oInit.aaData)&&bw(k);
bj(k,null,"plugin-init",[k,h]);
bj(k,"aoInitComplete","init",[k,h])
}function ci(k,h){var m=parseInt(h,10);
k._iDisplayLength=m;
b4(k);
bj(k,null,"length",[k,m])
}function aY(w){for(var v=w.oClasses,u=w.sTableId,t=w.aLengthMenu,s=bo.isArray(t[0]),p=s?t[0]:t,t=s?t[1]:t,s=bo("<select/>",{name:u+"_length","aria-controls":u,"class":v.sLengthSelect}),m=0,h=p.length;
m<h;
m++){s[0][m]=new Option(t[m],p[m])
}var k=bo("<div><label/></div>").addClass(v.sLength);
w.aanFeatures.l||(k[0].id=u+"_length");
k.children().append(w.oLanguage.sLengthMenu.replace("_MENU_",s[0].outerHTML));
bo("select",k).val(w._iDisplayLength).on("change.DT",function(){ci(w,bo(this).val());
bG(w)
});
bo(w.nTable).on("length.dt.DT",function(x,z,y){w===z&&bo("select",k).val(y)
});
return k[0]
}function ag(k){var h=k.sPaginationType,t=bm.ext.pager[h],s="function"===typeof t,p=function(u){bG(u)
},h=bo("<div/>").addClass(k.oClasses.sPaging+h)[0],m=k.aanFeatures;
s||t.fnInit(k,h,p);
m.p||(h.id=k.sTableId+"_paginate",k.aoDrawCallback.push({fn:function(w){if(s){var u=w._iDisplayStart,y=w._iDisplayLength,z=w.fnRecordsDisplay(),v=-1===y,u=v?0:Math.ceil(u/y),y=v?1:Math.ceil(z/y),z=t(u,y),x,v=0;
for(x=m.p.length;
v<x;
v++){an(w,"pageButton")(w,m.p[v],v,z,u,y)
}}else{t.fnUpdate(w,p)
}},sName:"pagination"}));
return h
}function bF(k,h,t){var s=k._iDisplayStart,p=k._iDisplayLength,m=k.fnRecordsDisplay();
0===m||-1===p?s=0:"number"===typeof h?(s=h*p,s>m&&(s=0)):"first"==h?s=0:"previous"==h?(s=0<=p?s-p:0,0>s&&(s=0)):"next"==h?s+p<m&&(s+=p):"last"==h?s=Math.floor((m-1)/p)*p:bJ(k,0,"Unknown paging action: "+h,5);
h=k._iDisplayStart!==s;
k._iDisplayStart=s;
h&&(bj(k,null,"page",[k]),t&&bG(k));
return h
}function aF(h){return bo("<div/>",{id:!h.aanFeatures.r?h.sTableId+"_processing":null,"class":h.oClasses.sProcessing}).html(h.oLanguage.sProcessing).insertBefore(h.nTable)[0]
}function bU(k,h){k.oFeatures.bProcessing&&bo(k.aanFeatures.r).css("display",h?"block":"none");
bj(k,null,"processing",[k,h])
}function ax(B){var A=bo(B.nTable);
A.attr("role","grid");
var z=B.oScroll;
if(""===z.sX&&""===z.sY){return B.nTable
}var y=z.sX,x=z.sY,w=B.oClasses,v=A.children("caption"),t=v.length?v[0]._captionSide:null,u=bo(A[0].cloneNode(!1)),m=bo(A[0].cloneNode(!1)),p=A.children("tfoot");
p.length||(p=null);
u=bo("<div/>",{"class":w.sScrollWrapper}).append(bo("<div/>",{"class":w.sScrollHead}).css({overflow:"hidden",position:"relative",border:0,width:y?!y?null:bg(y):"100%"}).append(bo("<div/>",{"class":w.sScrollHeadInner}).css({"box-sizing":"content-box",width:z.sXInner||"100%"}).append(u.removeAttr("id").css("margin-left",0).append("top"===t?v:null).append(A.children("thead"))))).append(bo("<div/>",{"class":w.sScrollBody}).css({position:"relative",overflow:"auto",width:!y?null:bg(y)}).append(A));
p&&u.append(bo("<div/>",{"class":w.sScrollFoot}).css({overflow:"hidden",border:0,width:y?!y?null:bg(y):"100%"}).append(bo("<div/>",{"class":w.sScrollFootInner}).append(m.removeAttr("id").css("margin-left",0).append("bottom"===t?v:null).append(A.children("tfoot")))));
var A=u.children(),s=A[0],w=A[1],h=p?A[2]:null;
if(y){bo(w).on("scroll.DT",function(){var k=this.scrollLeft;
s.scrollLeft=k;
p&&(h.scrollLeft=k)
})
}bo(w).css(x&&z.bCollapse?"max-height":"height",x);
B.nScrollHead=s;
B.nScrollBody=w;
B.nScrollFoot=h;
B.aoDrawCallback.push({fn:cg,sName:"scrolling"});
return u[0]
}function cg(co){var cn=co.oScroll,cb=cn.sX,ca=cn.sXInner,bc=cn.sY,cn=cn.iBarWidth,bb=bo(co.nScrollHead),ba=bb[0].style,aa=bb.children("div"),ab=aa[0].style,X=aa.children("table"),aa=co.nScrollBody,Z=bo(aa),S=aa.style,O=bo(co.nScrollFoot).children("div"),Y=O.children("table"),V=bo(co.nTHead),W=bo(co.nTable),I=W[0],M=I.style,J=co.nTFoot?bo(co.nTFoot):null,G=co.oBrowser,h=G.bScrollOversize,cc=bT(co.aoColumns,"nTh"),v,D,k,H,K=[],F=[],E=[],T=[],R,N=function(m){m=m.style;
m.paddingTop="0";
m.paddingBottom="0";
m.borderTopWidth="0";
m.borderBottomWidth="0";
m.height=0
};
D=aa.scrollHeight>aa.clientHeight;
if(co.scrollBarVis!==D&&co.scrollBarVis!==bn){co.scrollBarVis=D,bw(co)
}else{co.scrollBarVis=D;
W.children("thead, tfoot").remove();
J&&(k=J.clone().prependTo(W),v=J.find("tr"),k=k.find("tr"));
H=V.clone().prependTo(W);
V=V.find("tr");
D=H.find("tr");
H.find("th, td").removeAttr("tabindex");
cb||(S.width="100%",bb[0].style.width="100%");
bo.each(ay(co,H),function(m,p){R=b9(co,m);
p.style.width=co.aoColumns[R].sWidth
});
J&&bL(function(m){m.style.width=""
},k);
bb=W.outerWidth();
if(""===cb){M.width="100%";
if(h&&(W.find("tbody").height()>aa.offsetHeight||"scroll"==Z.css("overflow-y"))){M.width=bg(W.outerWidth()-cn)
}bb=W.outerWidth()
}else{""!==ca&&(M.width=bg(ca),bb=W.outerWidth())
}bL(N,D);
bL(function(m){E.push(m.innerHTML);
K.push(bg(bo(m).css("width")))
},D);
bL(function(p,m){if(bo.inArray(p,cc)!==-1){p.style.width=K[m]
}},V);
bo(D).height(0);
J&&(bL(N,k),bL(function(m){T.push(m.innerHTML);
F.push(bg(bo(m).css("width")))
},k),bL(function(p,m){p.style.width=F[m]
},v),bo(k).height(0));
bL(function(p,m){p.innerHTML='<div class="dataTables_sizing" style="height:0;overflow:hidden;">'+E[m]+"</div>";
p.style.width=K[m]
},D);
J&&bL(function(p,m){p.innerHTML='<div class="dataTables_sizing" style="height:0;overflow:hidden;">'+T[m]+"</div>";
p.style.width=F[m]
},k);
if(W.outerWidth()<bb){v=aa.scrollHeight>aa.offsetHeight||"scroll"==Z.css("overflow-y")?bb+cn:bb;
if(h&&(aa.scrollHeight>aa.offsetHeight||"scroll"==Z.css("overflow-y"))){M.width=bg(v-cn)
}(""===cb||""!==ca)&&bJ(co,1,"Possible column misalignment",6)
}else{v="100%"
}S.width=bg(v);
ba.width=bg(v);
J&&(co.nScrollFoot.style.width=bg(v));
!bc&&h&&(S.height=bg(I.offsetHeight+cn));
cb=W.outerWidth();
X[0].style.width=bg(cb);
ab.width=bg(cb);
ca=W.height()>aa.clientHeight||"scroll"==Z.css("overflow-y");
bc="padding"+(G.bScrollbarLeft?"Left":"Right");
ab[bc]=ca?cn+"px":"0px";
J&&(Y[0].style.width=bg(cb),O[0].style.width=bg(cb),O[0].style[bc]=ca?cn+"px":"0px");
W.children("colgroup").insertBefore(W.children("thead"));
Z.scroll();
if((co.bSorted||co.bFiltered)&&!co._drawHold){aa.scrollTop=0
}}}function bL(k,h,v){for(var u=0,t=0,s=h.length,p,m;
t<s;
){p=h[t].firstChild;
for(m=v?v[t].firstChild:null;
p;
){1===p.nodeType&&(v?k(p,m,u):k(p,u),u++),p=p.nextSibling,m=v?m.nextSibling:null
}t++
}}function ce(H){var G=H.nTable,F=H.aoColumns,E=H.oScroll,D=E.sY,C=E.sX,B=E.sXInner,z=F.length,A=b2(H,"bVisible"),v=bo("th",H.nTHead),x=G.getAttribute("width"),y=G.parentNode,h=!1,w,s,t=H.oBrowser,E=t.bScrollOversize;
(w=G.style.width)&&-1!==w.indexOf("%")&&(x=w);
for(w=0;
w<A.length;
w++){s=F[A[w]],null!==s.sWidth&&(s.sWidth=cl(s.sWidthOrig,y),h=!0)
}if(E||!h&&!C&&!D&&z==bQ(H)&&z==v.length){for(w=0;
w<z;
w++){A=b9(H,w),null!==A&&(F[A].sWidth=bg(v.eq(w).width()))
}}else{z=bo(G).clone().css("visibility","hidden").removeAttr("id");
z.find("tbody tr").remove();
var I=bo("<tr/>").appendTo(z.find("tbody"));
z.find("thead, tfoot").remove();
z.append(bo(H.nTHead).clone()).append(bo(H.nTFoot).clone());
z.find("tfoot th, tfoot td").css("width","");
v=ay(H,z.find("thead")[0]);
for(w=0;
w<A.length;
w++){s=F[A[w]],v[w].style.width=null!==s.sWidthOrig&&""!==s.sWidthOrig?bg(s.sWidthOrig):"",s.sWidthOrig&&C&&bo(v[w]).append(bo("<div/>").css({width:s.sWidthOrig,margin:0,padding:0,border:0,height:1}))
}if(H.aoData.length){for(w=0;
w<A.length;
w++){h=A[w],s=F[h],bo(cd(H,h)).clone(!1).append(s.sContentPadding).appendTo(I)
}}bo("[name]",z).removeAttr("name");
s=bo("<div/>").css(C||D?{position:"absolute",top:0,left:0,height:1,right:0,overflow:"hidden"}:{}).append(z).appendTo(y);
C&&B?z.width(B):C?(z.css("width","auto"),z.removeAttr("width"),z.width()<y.clientWidth&&x&&z.width(y.clientWidth)):D?z.width(y.clientWidth):x&&z.width(x);
for(w=D=0;
w<A.length;
w++){y=bo(v[w]),B=y.outerWidth()-y.width(),y=t.bBounding?Math.ceil(v[w].getBoundingClientRect().width):y.outerWidth(),D+=y,F[A[w]].sWidth=bg(y-B)
}G.style.width=bg(D);
s.remove()
}x&&(G.style.width=bg(x));
if((x||C)&&!H._reszEvt){G=function(){bo(bS).on("resize.DT-"+H.sInstance,af(function(){bw(H)
}))
},E?setTimeout(G,1000):G(),H._reszEvt=!0
}}function cl(k,h){if(!k){return 0
}var p=bo("<div/>").css("width",bg(k)).appendTo(h||bN.body),m=p[0].offsetWidth;
p.remove();
return m
}function cd(k,h){var p=bZ(k,h);
if(0>p){return null
}var m=k.aoData[p];
return !m.nTr?bo("<td/>").html(bV(k,p,h,"display"))[0]:m.anCells[h]
}function bZ(k,h){for(var u,t=-1,s=-1,p=0,m=k.aoData.length;
p<m;
p++){u=bV(k,p,h,"display")+"",u=u.replace(bM,""),u=u.replace(/&nbsp;/g," "),u.length>t&&(t=u.length,s=p)
}return s
}function bg(h){return null===h?"0px":"number"==typeof h?0>h?"0px":h+"px":h.match(/\d$/)?h+"px":h
}function bz(x){var w,v,u=[],t=x.aoColumns,s,p,k,m;
w=x.aaSortingFixed;
v=bo.isPlainObject(w);
var h=[];
s=function(y){y.length&&!bo.isArray(y[0])?h.push(y):bo.merge(h,y)
};
bo.isArray(w)&&s(w);
v&&w.pre&&s(w.pre);
s(x.aaSorting);
v&&w.post&&s(w.post);
for(x=0;
x<h.length;
x++){m=h[x][0];
s=t[m].aDataSort;
w=0;
for(v=s.length;
w<v;
w++){p=s[w],k=t[p].sType||"string",h[x]._idx===bn&&(h[x]._idx=bo.inArray(h[x][1],t[p].asSorting)),u.push({src:m,col:p,dir:h[x][1],index:h[x]._idx,type:k,formatter:bm.ext.type.order[k+"-pre"]})
}}return u
}function bu(y){var x,w,v=[],u=bm.ext.type.order,t=y.aoData,s=0,k,m=y.aiDisplayMaster,p;
b0(y);
p=bz(y);
x=0;
for(w=p.length;
x<w;
x++){k=p[x],k.formatter&&s++,bs(y,k.col)
}if("ssp"!=bd(y)&&0!==p.length){x=0;
for(w=m.length;
x<w;
x++){v[m[x]]=x
}s===p.length?m.sort(function(G,F){var E,D,C,A,B=p.length,z=t[G]._aSortData,h=t[F]._aSortData;
for(C=0;
C<B;
C++){if(A=p[C],E=z[A.col],D=h[A.col],E=E<D?-1:E>D?1:0,0!==E){return"asc"===A.dir?E:-E
}}E=v[G];
D=v[F];
return E<D?-1:E>D?1:0
}):m.sort(function(G,F){var E,D,B,C,A=p.length,z=t[G]._aSortData,h=t[F]._aSortData;
for(B=0;
B<A;
B++){if(C=p[B],E=z[C.col],D=h[C.col],C=u[C.type+"-"+C.dir]||u["string-"+C.dir],E=C(E,D),0!==E){return E
}}E=v[G];
D=v[F];
return E<D?-1:E>D?1:0
})
}y.bSorted=!0
}function a5(w){for(var v,u,t=w.aoColumns,s=bz(w),w=w.oLanguage.oAria,p=0,m=t.length;
p<m;
p++){u=t[p];
var h=u.asSorting;
v=u.sTitle.replace(/<.*?>/g,"");
var k=u.nTh;
k.removeAttribute("aria-sort");
u.bSortable&&(0<s.length&&s[0].col==p?(k.setAttribute("aria-sort","asc"==s[0].dir?"ascending":"descending"),u=h[s[0].index+1]||h[0]):u=h[0],v+="asc"===u?w.sSortAscending:w.sSortDescending);
k.setAttribute("aria-label",v)
}}function a1(k,h,u,t){var s=k.aaSorting,p=k.aoColumns[h].asSorting,m=function(w,v){var x=w._idx;
x===bn&&(x=bo.inArray(w[1],p));
return x+1<p.length?x+1:v?null:0
};
"number"===typeof s[0]&&(s=k.aaSorting=[s]);
u&&k.oFeatures.bSortMulti?(u=bo.inArray(h,bT(s,"0")),-1!==u?(h=m(s[u],!0),null===h&&1===s.length&&(h=0),null===h?s.splice(u,1):(s[u][1]=p[h],s[u]._idx=h)):(s.push([h,p[0],0]),s[s.length-1]._idx=0)):s.length&&s[0][0]==h?(h=m(s[0]),s.length=1,s[0][1]=p[h],s[0]._idx=h):(s.length=0,s.push([h,p[0]]),s[0]._idx=0);
bB(k);
"function"==typeof t&&t(k)
}function aw(k,h,s,p){var m=k.aoColumns[s];
aS(h,{},function(t){!1!==m.bSortable&&(k.oFeatures.bProcessing?(bU(k,!0),setTimeout(function(){a1(k,s,t.shiftKey,p);
"ssp"!==bd(k)&&bU(k,!1)
},0)):a1(k,s,t.shiftKey,p))
})
}function ck(k){var h=k.aLastSort,u=k.oClasses.sSortColumn,t=bz(k),s=k.oFeatures,p,m;
if(s.bSort&&s.bSortClasses){s=0;
for(p=h.length;
s<p;
s++){m=h[s].src,bo(bT(k.aoData,"anCells",m)).removeClass(u+(2>s?s+1:3))
}s=0;
for(p=t.length;
s<p;
s++){m=t[s].src,bo(bT(k.aoData,"anCells",m)).addClass(u+(2>s?s+1:3))
}}k.aLastSort=t
}function bs(w,v){var u=w.aoColumns[v],t=bm.ext.order[u.sSortDataType],s;
t&&(s=t.call(w.oInstance,w,v,b8(w,v)));
for(var p,m=bm.ext.type.order[u.sType+"-pre"],h=0,k=w.aoData.length;
h<k;
h++){if(u=w.aoData[h],u._aSortData||(u._aSortData=[]),!u._aSortData[v]||t){p=t?s[h]:bV(w,h,v,"sort"),u._aSortData[v]=m?m(p):p
}}}function b6(k){if(k.oFeatures.bStateSave&&!k.bDestroying){var h={time:+new Date,start:k._iDisplayStart,length:k._iDisplayLength,order:bo.extend(!0,[],k.aaSorting),search:aq(k.oPreviousSearch),columns:bo.map(k.aoColumns,function(m,p){return{visible:m.bVisible,search:aq(k.aoPreSearchCols[p])}
})};
bj(k,"aoStateSaveParams","stateSaveParams",[k,h]);
k.oSavedState=h;
k.fnStateSaveCallback.call(k.oInstance,k,h)
}}function aW(k,h,u){var t,s,p=k.aoColumns,h=function(v){if(v&&v.time){var w=bj(k,"aoStateLoadParams","stateLoadParams",[k,m]);
if(-1===bo.inArray(!1,w)&&(w=k.iStateDuration,!(0<w&&v.time<+new Date-1000*w)&&!(v.columns&&p.length!==v.columns.length))){k.oLoadedState=bo.extend(!0,{},m);
v.start!==bn&&(k._iDisplayStart=v.start,k.iInitDisplayStart=v.start);
v.length!==bn&&(k._iDisplayLength=v.length);
v.order!==bn&&(k.aaSorting=[],bo.each(v.order,function(x,y){k.aaSorting.push(y[0]>=p.length?[0,y[1]]:y)
}));
v.search!==bn&&bo.extend(k.oPreviousSearch,ai(v.search));
if(v.columns){t=0;
for(s=v.columns.length;
t<s;
t++){w=v.columns[t],w.visible!==bn&&(p[t].bVisible=w.visible),w.search!==bn&&bo.extend(k.aoPreSearchCols[t],ai(w.search))
}}bj(k,"aoStateLoaded","stateLoaded",[k,m])
}}u()
};
if(k.oFeatures.bStateSave){var m=k.fnStateLoadCallback.call(k.oInstance,k,h);
m!==bn&&h(m)
}else{u()
}}function aK(k){var h=bm.settings,k=bo.inArray(k,bT(h,"nTable"));
return -1!==k?h[k]:null
}function bJ(k,h,p,m){p="DataTables warning: "+(k?"table id="+k.sTableId+" - ":"")+p;
m&&(p+=". For more information about this error, please see http://datatables.net/tn/"+m);
if(h){bS.console&&console.log&&console.log(p)
}else{if(h=bm.ext,h=h.sErrMode||h.errMode,k&&bj(k,null,"error",[k,m,p]),"alert"==h){alert(p)
}else{if("throw"==h){throw Error(p)
}"function"==typeof h&&h(k,m,p)
}}}function bR(k,h,p,m){bo.isArray(p)?bo.each(p,function(t,s){bo.isArray(s)?bR(k,h,s[0],s[1]):bR(k,h,s)
}):(m===bn&&(m=p),h[p]!==bn&&(k[m]=h[p]))
}function aO(k,h,s){var p,m;
for(m in h){h.hasOwnProperty(m)&&(p=h[m],bo.isPlainObject(p)?(bo.isPlainObject(k[m])||(k[m]={}),bo.extend(!0,k[m],p)):k[m]=s&&"data"!==m&&"aaData"!==m&&bo.isArray(p)?p.slice():p)
}return k
}function aS(k,h,m){bo(k).on("click.DT",h,function(p){k.blur();
m(p)
}).on("keypress.DT",h,function(p){13===p.which&&(p.preventDefault(),m(p))
}).on("selectstart.DT",function(){return !1
})
}function a8(k,h,p,m){p&&k[h].push({fn:p,sName:m})
}function bj(k,h,s,p){var m=[];
h&&(m=bo.map(k[h].slice().reverse(),function(t){return t.fn.apply(k.oInstance,p)
}));
null!==s&&(h=bo.Event(s+".dt"),bo(k.nTable).trigger(h,p),m.push(h.result));
return m
}function b4(k){var h=k._iDisplayStart,p=k.fnDisplayEnd(),m=k._iDisplayLength;
h>=p&&(h=p-m);
h-=h%m;
if(-1===m||0>h){h=0
}k._iDisplayStart=h
}function an(k,h){var p=k.renderer,m=bm.ext.renderer[h];
return bo.isPlainObject(p)&&p[h]?m[p[h]]||m._:"string"===typeof p?m[p]||m._:m._
}function bd(h){return h.oFeatures.bServerSide?"ssp":h.ajax||h.sAjaxSource?"ajax":"dom"
}function al(k,h){var p=[],p=aD.numbers_length,m=Math.floor(p/2);
h<=p?p=by(0,h):k<=m?(p=by(0,p-2),p.push("ellipsis"),p.push(h-1)):(k>=h-1-m?p=by(h-(p-2),h):(p=by(k-m+2,k+m-1),p.push("ellipsis"),p.push(h-1)),p.splice(0,0,"ellipsis"),p.splice(0,0,0));
p.DT_el="span";
return p
}function aM(h){bo.each({num:function(k){return aA(k,h)
},"num-fmt":function(k){return aA(k,h,aI)
},"html-num":function(k){return aA(k,h,ar)
},"html-num-fmt":function(k){return aA(k,h,ar,aI)
}},function(k,m){be.type.order[k+h+"-pre"]=m;
k.match(/^html\-/)&&(be.type.search[k+h]=be.type.search.html)
})
}function av(h){return function(){var k=[aK(this[bm.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));
return bm.ext.internal[h].apply(this,k)
}
}var bm=function(k){this.$=function(u,t){return this.api(!0).$(u,t)
};
this._=function(u,t){return this.api(!0).rows(u,t).data()
};
this.api=function(t){return t?new bh(aK(this[be.iApiIndex])):new bh(this)
};
this.fnAddData=function(u,t){var w=this.api(!0),v=bo.isArray(u)&&(bo.isArray(u[0])||bo.isPlainObject(u[0]))?w.rows.add(u):w.row.add(u);
(t===bn||t)&&w.draw();
return v.flatten().toArray()
};
this.fnAdjustColumnSizing=function(u){var t=this.api(!0).columns.adjust(),w=t.settings()[0],v=w.oScroll;
u===bn||u?t.draw(!1):(""!==v.sX||""!==v.sY)&&cg(w)
};
this.fnClearTable=function(u){var t=this.api(!0).clear();
(u===bn||u)&&t.draw()
};
this.fnClose=function(t){this.api(!0).row(t).child.hide()
};
this.fnDeleteRow=function(u,t,y){var x=this.api(!0),u=x.rows(u),w=u.settings()[0],v=w.aoData[u[0][0]];
u.remove();
t&&t.call(this,w,v);
(y===bn||y)&&x.draw();
return v
};
this.fnDestroy=function(t){this.api(!0).destroy(t)
};
this.fnDraw=function(t){this.api(!0).draw(t)
};
this.fnFilter=function(u,t,y,x,w,v){w=this.api(!0);
null===t||t===bn?w.search(u,y,x,v):w.column(t).search(u,y,x,v);
w.draw()
};
this.fnGetData=function(u,t){var w=this.api(!0);
if(u!==bn){var v=u.nodeName?u.nodeName.toLowerCase():"";
return t!==bn||"td"==v||"th"==v?w.cell(u,t).data():w.row(u).data()||null
}return w.data().toArray()
};
this.fnGetNodes=function(u){var t=this.api(!0);
return u!==bn?t.row(u).node():t.rows().nodes().flatten().toArray()
};
this.fnGetPosition=function(u){var t=this.api(!0),v=u.nodeName.toUpperCase();
return"TR"==v?t.row(u).index():"TD"==v||"TH"==v?(u=t.cell(u).index(),[u.row,u.columnVisible,u.column]):null
};
this.fnIsOpen=function(t){return this.api(!0).row(t).child.isShown()
};
this.fnOpen=function(u,t,v){return this.api(!0).row(u).child(t,v).show().child()[0]
};
this.fnPageChange=function(u,t){var v=this.api(!0).page(u);
(t===bn||t)&&v.draw(!1)
};
this.fnSetColumnVis=function(u,t,v){u=this.api(!0).column(u).visible(t);
(v===bn||v)&&u.columns.adjust().draw()
};
this.fnSettings=function(){return aK(this[be.iApiIndex])
};
this.fnSort=function(t){this.api(!0).order(t).draw()
};
this.fnSortListener=function(u,t,v){this.api(!0).order.listener(u,t,v)
};
this.fnUpdate=function(u,t,y,x,w){var v=this.api(!0);
y===bn||null===y?v.row(t).data(u):v.cell(t,y).data(u);
(w===bn||w)&&v.columns.adjust();
(x===bn||x)&&v.draw();
return 0
};
this.fnVersionCheck=be.fnVersionCheck;
var h=this,s=k===bn,p=this.length;
s&&(k={});
this.oApi=this.internal=be.internal;
for(var m in bm.ext.internal){m&&(this[m]=av(m))
}this.each(function(){var I={},H=1<p?aO(I,k,!0):k,F=0,G,I=this.getAttribute("id"),C=!1,E=bm.defaults,z=bo(this);
if("table"!=this.nodeName.toLowerCase()){bJ(null,0,"Non-table node initialisation ("+this.nodeName+")",2)
}else{aB(E);
at(E.column);
bK(E,E,!0);
bK(E.column,E.column,!0);
bK(E,bo.extend(H,z.data()));
var y=bm.settings,F=0;
for(G=y.length;
F<G;
F++){var A=y[F];
if(A.nTable==this||A.nTHead.parentNode==this||A.nTFoot&&A.nTFoot.parentNode==this){var N=H.bRetrieve!==bn?H.bRetrieve:E.bRetrieve;
if(s||N){return A.oInstance
}if(H.bDestroy!==bn?H.bDestroy:E.bDestroy){A.oInstance.fnDestroy();
break
}else{bJ(A,0,"Cannot reinitialise DataTable",3);
return
}}if(A.sTableId==this.id){y.splice(F,1);
break
}}if(null===I||""===I){this.id=I="DataTables_Table_"+bm.ext._unique++
}var B=bo.extend(!0,{},bm.models.oSettings,{sDestroyWidth:z[0].style.width,sInstance:I,sTableId:I});
B.nTable=this;
B.oApi=h.internal;
B.oInit=H;
y.push(B);
B.oInstance=1===h.length?h:z.dataTable();
aB(H);
H.oLanguage&&l(H.oLanguage);
H.aLengthMenu&&!H.iDisplayLength&&(H.iDisplayLength=bo.isArray(H.aLengthMenu[0])?H.aLengthMenu[0][0]:H.aLengthMenu[0]);
H=aO(bo.extend(!0,{},E),H);
bR(B.oFeatures,H,"bPaginate bLengthChange bFilter bSort bSortMulti bInfo bProcessing bAutoWidth bSortClasses bServerSide bDeferRender".split(" "));
bR(B,H,["asStripeClasses","ajax","fnServerData","fnFormatNumber","sServerMethod","aaSorting","aaSortingFixed","aLengthMenu","sPaginationType","sAjaxSource","sAjaxDataProp","iStateDuration","sDom","bSortCellsTop","iTabIndex","fnStateLoadCallback","fnStateSaveCallback","renderer","searchDelay","rowId",["iCookieDuration","iStateDuration"],["oSearch","oPreviousSearch"],["aoSearchCols","aoPreSearchCols"],["iDisplayLength","_iDisplayLength"],["bJQueryUI","bJUI"]]);
bR(B.oScroll,H,[["sScrollX","sX"],["sScrollXInner","sXInner"],["sScrollY","sY"],["bScrollCollapse","bCollapse"]]);
bR(B.oLanguage,H,"fnInfoCallback");
a8(B,"aoDrawCallback",H.fnDrawCallback,"user");
a8(B,"aoServerParams",H.fnServerParams,"user");
a8(B,"aoStateSaveParams",H.fnStateSaveParams,"user");
a8(B,"aoStateLoadParams",H.fnStateLoadParams,"user");
a8(B,"aoStateLoaded",H.fnStateLoaded,"user");
a8(B,"aoRowCallback",H.fnRowCallback,"user");
a8(B,"aoRowCreatedCallback",H.fnCreatedRow,"user");
a8(B,"aoHeaderCallback",H.fnHeaderCallback,"user");
a8(B,"aoFooterCallback",H.fnFooterCallback,"user");
a8(B,"aoInitComplete",H.fnInitComplete,"user");
a8(B,"aoPreDrawCallback",H.fnPreDrawCallback,"user");
B.rowIdFn=bE(H.rowId);
ak(B);
var O=B.oClasses;
H.bJQueryUI?(bo.extend(O,bm.ext.oJUIClasses,H.oClasses),H.sDom===E.sDom&&"lfrtip"===E.sDom&&(B.sDom='<"H"lfr>t<"F"ip>'),B.renderer)?bo.isPlainObject(B.renderer)&&!B.renderer.header&&(B.renderer.header="jqueryui"):B.renderer="jqueryui":bo.extend(O,bm.ext.classes,H.oClasses);
z.addClass(O.sTable);
B.iInitDisplayStart===bn&&(B.iInitDisplayStart=H.iDisplayStart,B._iDisplayStart=H.iDisplayStart);
null!==H.iDeferLoading&&(B.bDeferLoading=!0,I=bo.isArray(H.iDeferLoading),B._iRecordsDisplay=I?H.iDeferLoading[0]:H.iDeferLoading,B._iRecordsTotal=I?H.iDeferLoading[1]:H.iDeferLoading);
var M=B.oLanguage;
bo.extend(!0,M,H.oLanguage);
M.sUrl&&(bo.ajax({dataType:"json",url:M.sUrl,success:function(t){l(t);
bK(E.oLanguage,t);
bo.extend(true,M,t);
au(B)
},error:function(){au(B)
}}),C=!0);
null===H.asStripeClasses&&(B.asStripeClasses=[O.sStripeOdd,O.sStripeEven]);
var I=B.asStripeClasses,J=z.children("tbody").find("tr").eq(0);
-1!==bo.inArray(!0,bo.map(I,function(t){return J.hasClass(t)
}))&&(bo("tbody tr",this).removeClass(I.join(" ")),B.asDestroyStripes=I.slice());
I=[];
y=this.getElementsByTagName("thead");
0!==y.length&&(aV(B.aoHeader,y[0]),I=ay(B));
if(null===H.aoColumns){y=[];
F=0;
for(G=I.length;
F<G;
F++){y.push(null)
}}else{y=H.aoColumns
}F=0;
for(G=y.length;
F<G;
F++){cm(B,I?I[F]:null)
}n(B,H.aoColumnDefs,y,function(u,t){b(B,u,t)
});
if(J.length){var K=function(u,t){return u.getAttribute("data-"+t)!==null?t:null
};
bo(J[0]).children("th, td").each(function(u,t){var x=B.aoColumns[u];
if(x.mData===u){var w=K(t,"sort")||K(t,"order"),v=K(t,"filter")||K(t,"search");
if(w!==null||v!==null){x.mData={_:u+".display",sort:w!==null?u+".@data-"+w:bn,type:w!==null?u+".@data-"+w:bn,filter:v!==null?u+".@data-"+v:bn};
b(B,u)
}}})
}var D=B.oFeatures,I=function(){if(H.aaSorting===bn){var u=B.aaSorting;
F=0;
for(G=u.length;
F<G;
F++){u[F][1]=B.aoColumns[F].asSorting[0]
}}ck(B);
D.bSort&&a8(B,"aoDrawCallback",function(){if(B.bSorted){var w=bz(B),v={};
bo.each(w,function(x,R){v[R.src]=R.dir
});
bj(B,null,"order",[B,w,v]);
a5(B)
}});
a8(B,"aoDrawCallback",function(){(B.bSorted||bd(B)==="ssp"||D.bDeferRender)&&ck(B)
},"sc");
var u=z.children("caption").each(function(){this._captionSide=bo(this).css("caption-side")
}),t=z.children("thead");
t.length===0&&(t=bo("<thead/>").appendTo(z));
B.nTHead=t[0];
t=z.children("tbody");
t.length===0&&(t=bo("<tbody/>").appendTo(z));
B.nTBody=t[0];
t=z.children("tfoot");
if(t.length===0&&u.length>0&&(B.oScroll.sX!==""||B.oScroll.sY!=="")){t=bo("<tfoot/>").appendTo(z)
}if(t.length===0||t.children().length===0){z.addClass(O.sNoFooter)
}else{if(t.length>0){B.nTFoot=t[0];
aV(B.aoFooter,B.nTFoot)
}}if(H.aaData){for(F=0;
F<H.aaData.length;
F++){bH(B,H.aaData[F])
}}else{(B.bDeferLoading||bd(B)=="dom")&&bv(B,bo(B.nTBody).children("tr"))
}B.aiDisplay=B.aiDisplayMaster.slice();
B.bInitialised=true;
C===false&&au(B)
};
H.bStateSave?(D.bStateSave=!0,a8(B,"aoDrawCallback",b6,"state_save"),aW(B,H,I)):I()
}});
h=null;
return this
},be,bh,bl,bi,bY={},am=/[\r\n]/g,ar=/<.*?>/g,bp=/^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/,a2=RegExp("(\\/|\\.|\\*|\\+|\\?|\\||\\(|\\)|\\[|\\]|\\{|\\}|\\\\|\\$|\\^|\\-)","g"),aI=/[',$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfk]/gi,bI=function(h){return !h||!0===h||"-"===h?!0:!1
},ae=function(k){var h=parseInt(k,10);
return !isNaN(h)&&isFinite(k)?h:null
},q=function(k,h){bY[h]||(bY[h]=RegExp(d(h),"g"));
return"string"===typeof k&&"."!==h?k.replace(/\./g,"").replace(bY[h],"."):k
},b7=function(k,h,p){var m="string"===typeof k;
if(bI(k)){return !0
}h&&m&&(k=q(k,h));
p&&m&&(k=k.replace(aI,""));
return !isNaN(parseFloat(k))&&isFinite(k)
},c=function(k,h,m){return bI(k)?!0:!(bI(k)||"string"===typeof k)?null:b7(k.replace(ar,""),h,m)?!0:null
},bT=function(k,h,t){var s=[],p=0,m=k.length;
if(t!==bn){for(;
p<m;
p++){k[p]&&k[p][h]&&s.push(k[p][h][t])
}}else{for(;
p<m;
p++){k[p]&&s.push(k[p][h])
}}return s
},ad=function(k,h,u,t){var s=[],p=0,m=h.length;
if(t!==bn){for(;
p<m;
p++){k[h[p]][u]&&s.push(k[h[p]][u][t])
}}else{for(;
p<m;
p++){s.push(k[h[p]][u])
}}return s
},by=function(k,h){var s=[],p;
h===bn?(h=0,p=k):(p=h,h=k);
for(var m=h;
m<p;
m++){s.push(m)
}return s
},ch=function(k){for(var h=[],p=0,m=k.length;
p<m;
p++){k[p]&&h.push(k[p])
}return h
},aG=function(k){var h=[],u,t,s=k.length,p,m=0;
t=0;
k:for(;
t<s;
t++){u=k[t];
for(p=0;
p<m;
p++){if(h[p]===u){continue k
}}h.push(u);
m++
}return h
};
bm.util={throttle:function(k,h){var s=h!==bn?h:200,p,m;
return function(){var t=this,v=+new Date,u=arguments;
p&&v<p+s?(clearTimeout(m),m=setTimeout(function(){p=bn;
k.apply(t,u)
},s)):(p=v,k.apply(t,u))
}
},escapeRegex:function(h){return h.replace(a2,"\\$1")
}};
var bW=function(k,h,m){k[h]!==bn&&(k[m]=k[h])
},br=/\[.*?\]$/,bA=/\(\)$/,d=bm.util.escapeRegex,f=bo("<div>")[0],bX=f.textContent!==bn,bM=/<.*?>/g,af=bm.util.throttle,b3=[],bf=Array.prototype,aT=function(k){var h,s,p=bm.settings,m=bo.map(p,function(t){return t.nTable
});
if(k){if(k.nTable&&k.oApi){return[k]
}if(k.nodeName&&"table"===k.nodeName.toLowerCase()){return h=bo.inArray(k,m),-1!==h?[p[h]]:null
}if(k&&"function"===typeof k.settings){return k.settings().toArray()
}"string"===typeof k?s=bo(k):k instanceof bo&&(s=k)
}else{return[]
}if(s){return s.map(function(){h=bo.inArray(this,m);
return -1!==h?p[h]:null
}).toArray()
}};
bh=function(k,h){if(!(this instanceof bh)){return new bh(k,h)
}var t=[],s=function(u){(u=aT(u))&&(t=t.concat(u))
};
if(bo.isArray(k)){for(var p=0,m=k.length;
p<m;
p++){s(k[p])
}}else{s(k)
}this.context=aG(t);
h&&bo.merge(this,h);
this.selector={rows:null,cols:null,opts:null};
bh.extend(this,this,b3)
};
bm.Api=bh;
bo.extend(bh.prototype,{any:function(){return 0!==this.count()
},concat:bf.concat,context:[],count:function(){return this.flatten().length
},each:function(k){for(var h=0,m=this.length;
h<m;
h++){k.call(this,this[h],h,this)
}return this
},eq:function(k){var h=this.context;
return h.length>k?new bh(h[k],this[k]):null
},filter:function(k){var h=[];
if(bf.filter){h=bf.filter.call(this,k,this)
}else{for(var p=0,m=this.length;
p<m;
p++){k.call(this,this[p],p,this)&&h.push(this[p])
}}return new bh(this.context,h)
},flatten:function(){var h=[];
return new bh(this.context,h.concat.apply(h,this.toArray()))
},join:bf.join,indexOf:bf.indexOf||function(k,h){for(var p=h||0,m=this.length;
p<m;
p++){if(this[p]===k){return p
}}return -1
},iterator:function(F,E,D,C){var B=[],A,z,y,x,u,w=this.context,v,k,G=this.selector;
"string"===typeof F&&(C=D,D=E,E=F,F=!1);
z=0;
for(y=w.length;
z<y;
z++){var H=new bh(w[z]);
if("table"===E){A=D.call(H,w[z],z),A!==bn&&B.push(A)
}else{if("columns"===E||"rows"===E){A=D.call(H,w[z],this[z],z),A!==bn&&B.push(A)
}else{if("column"===E||"column-rows"===E||"row"===E||"cell"===E){k=this[z];
"column-rows"===E&&(v=aj(w[z],G.opts));
x=0;
for(u=k.length;
x<u;
x++){A=k[x],A="cell"===E?D.call(H,w[z],A.row,A.column,z,x):D.call(H,w[z],A,z,x,v),A!==bn&&B.push(A)
}}}}}return B.length||C?(F=new bh(w,F?B.concat.apply([],B):B),E=F.selector,E.rows=G.rows,E.cols=G.cols,E.opts=G.opts,F):this
},lastIndexOf:bf.lastIndexOf||function(k,h){return this.indexOf.apply(this.toArray.reverse(),arguments)
},length:0,map:function(k){var h=[];
if(bf.map){h=bf.map.call(this,k,this)
}else{for(var p=0,m=this.length;
p<m;
p++){h.push(k.call(this,this[p],p))
}}return new bh(this.context,h)
},pluck:function(h){return this.map(function(k){return k[h]
})
},pop:bf.pop,push:bf.push,reduce:bf.reduce||function(k,h){return ac(this,k,h,0,this.length,1)
},reduceRight:bf.reduceRight||function(k,h){return ac(this,k,h,this.length-1,-1,-1)
},reverse:bf.reverse,selector:null,shift:bf.shift,sort:bf.sort,splice:bf.splice,toArray:function(){return bf.slice.call(this)
},to$:function(){return bo(this)
},toJQuery:function(){return bo(this)
},unique:function(){return new bh(this.context,aG(this))
},unshift:bf.unshift});
bh.extend=function(k,h,u){if(u.length&&h&&(h instanceof bh||h.__dt_wrapper)){var t,s,p,m=function(w,v,x){return function(){var y=v.apply(w,arguments);
bh.extend(y,y,x.methodExt);
return y
}
};
t=0;
for(s=u.length;
t<s;
t++){p=u[t],h[p.name]="function"===typeof p.val?m(k,p.val,p):bo.isPlainObject(p.val)?{}:p.val,h[p.name].__dt_wrapper=!0,bh.extend(k,h[p.name],p.propExt)
}}};
bh.register=bl=function(x,w){if(bo.isArray(x)){for(var v=0,u=x.length;
v<u;
v++){bh.register(x[v],w)
}}else{for(var t=x.split("."),s=b3,p,k,v=0,u=t.length;
v<u;
v++){p=(k=-1!==t[v].indexOf("()"))?t[v].replace("()",""):t[v];
var m;
x:{m=0;
for(var h=s.length;
m<h;
m++){if(s[m].name===p){m=s[m];
break x
}}m=null
}m||(m={name:p,val:{},methodExt:[],propExt:[]},s.push(m));
v===u-1?m.val=w:s=k?m.methodExt:m.propExt
}}};
bh.registerPlural=bi=function(k,h,m){bh.register(k,m);
bh.register(h,function(){var p=m.apply(this,arguments);
return p===this?this:p instanceof bh?p.length?bo.isArray(p[0])?new bh(p.context,p[0]):p[0]:bn:p
})
};
bl("tables()",function(k){var h;
if(k){h=bh;
var p=this.context;
if("number"===typeof k){k=[p[k]]
}else{var m=bo.map(p,function(s){return s.nTable
}),k=bo(m).filter(k).map(function(){var s=bo.inArray(this,m);
return p[s]
}).toArray()
}h=new h(k)
}else{h=this
}return h
});
bl("table()",function(k){var k=this.tables(k),h=k.context;
return h.length?new bh(h[0]):k
});
bi("tables().nodes()","table().node()",function(){return this.iterator("table",function(h){return h.nTable
},1)
});
bi("tables().body()","table().body()",function(){return this.iterator("table",function(h){return h.nTBody
},1)
});
bi("tables().header()","table().header()",function(){return this.iterator("table",function(h){return h.nTHead
},1)
});
bi("tables().footer()","table().footer()",function(){return this.iterator("table",function(h){return h.nTFoot
},1)
});
bi("tables().containers()","table().container()",function(){return this.iterator("table",function(h){return h.nTableWrapper
},1)
});
bl("draw()",function(h){return this.iterator("table",function(k){"page"===h?bG(k):("string"===typeof h&&(h="full-hold"===h?!1:!0),bB(k,!1===h))
})
});
bl("page()",function(h){return h===bn?this.page.info().page:this.iterator("table",function(k){bF(k,h)
})
});
bl("page.info()",function(){if(0===this.context.length){return bn
}var k=this.context[0],h=k._iDisplayStart,s=k.oFeatures.bPaginate?k._iDisplayLength:-1,p=k.fnRecordsDisplay(),m=-1===s;
return{page:m?0:Math.floor(h/s),pages:m?1:Math.ceil(p/s),start:h,end:k.fnDisplayEnd(),length:s,recordsTotal:k.fnRecordsTotal(),recordsDisplay:p,serverSide:"ssp"===bd(k)}
});
bl("page.len()",function(h){return h===bn?0!==this.context.length?this.context[0]._iDisplayLength:bn:this.iterator("table",function(k){ci(k,h)
})
});
var bD=function(k,h,s){if(s){var p=new bh(k);
p.one("draw",function(){s(p.ajax.json())
})
}if("ssp"==bd(k)){bB(k,h)
}else{bU(k,!0);
var m=k.jqXHR;
m&&4!==m.readyState&&m.abort();
ap(k,[],function(v){a9(k);
for(var v=ah(k,v),u=0,t=v.length;
u<t;
u++){bH(k,v[u])
}bB(k,h);
bU(k,!1)
})
}};
bl("ajax.json()",function(){var h=this.context;
if(0<h.length){return h[0].json
}});
bl("ajax.params()",function(){var h=this.context;
if(0<h.length){return h[0].oAjaxData
}});
bl("ajax.reload()",function(k,h){return this.iterator("table",function(m){bD(m,!1===h,k)
})
});
bl("ajax.url()",function(k){var h=this.context;
if(k===bn){if(0===h.length){return bn
}h=h[0];
return h.ajax?bo.isPlainObject(h.ajax)?h.ajax.url:h.ajax:h.sAjaxSource
}return this.iterator("table",function(m){bo.isPlainObject(m.ajax)?m.ajax.url=k:m.ajax=k
})
});
bl("ajax.url().load()",function(k,h){return this.iterator("table",function(m){bD(m,!1===h,k)
})
});
var bO=function(A,z,y,x,w){var v=[],u,s,t,h,p,k;
t=typeof z;
if(!z||"string"===t||"function"===t||z.length===bn){z=[z]
}t=0;
for(h=z.length;
t<h;
t++){s=z[t]&&z[t].split&&!z[t].match(/[\[\(:]/)?z[t].split(","):[z[t]];
p=0;
for(k=s.length;
p<k;
p++){(u=y("string"===typeof s[p]?bo.trim(s[p]):s[p]))&&u.length&&(v=v.concat(u))
}}A=be.selector[A];
if(A.length){t=0;
for(h=A.length;
t<h;
t++){v=A[t](x,w,v)
}}return aG(v)
},bq=function(h){h||(h={});
h.filter&&h.search===bn&&(h.search=h.filter);
return bo.extend({search:"none",order:"current",page:"all"},h)
},a3=function(k){for(var h=0,m=k.length;
h<m;
h++){if(0<k[h].length){return k[0]=k[h],k[0].length=1,k.length=1,k.context=[k.context[h]],k
}}k.length=0;
return k
},aj=function(k,h){var v,u,t,s=[],p=k.aiDisplay;
v=k.aiDisplayMaster;
var m=h.search;
u=h.order;
t=h.page;
if("ssp"==bd(k)){return"removed"===m?[]:by(0,v.length)
}if("current"==t){v=k._iDisplayStart;
for(u=k.fnDisplayEnd();
v<u;
v++){s.push(p[v])
}}else{if("current"==u||"applied"==u){s="none"==m?v.slice():"applied"==m?p.slice():bo.map(v,function(w){return -1===bo.inArray(w,p)?w:null
})
}else{if("index"==u||"original"==u){v=0;
for(u=k.aoData.length;
v<u;
v++){"none"==m?s.push(v):(t=bo.inArray(v,p),(-1===t&&"removed"==m||0<=t&&"applied"==m)&&s.push(v))
}}}}return s
};
bl("rows()",function(k,h){k===bn?k="":bo.isPlainObject(k)&&(h=k,k="");
var h=bq(h),m=this.iterator("table",function(t){var s=h,p;
return bO("row",k,function(v){var u=ae(v);
if(u!==null&&!s){return[u]
}p||(p=aj(t,s));
if(u!==null&&bo.inArray(u,p)!==-1){return[u]
}if(v===null||v===bn||v===""){return p
}if(typeof v==="function"){return bo.map(p,function(x){var y=t.aoData[x];
return v(x,y._aData,y.nTr)?x:null
})
}u=ch(ad(t.aoData,p,"nTr"));
if(v.nodeName){if(v._DT_RowIndex!==bn){return[v._DT_RowIndex]
}if(v._DT_CellIndex){return[v._DT_CellIndex.row]
}u=bo(v).closest("*[data-dt-row]");
return u.length?[u.data("dt-row")]:[]
}if(typeof v==="string"&&v.charAt(0)==="#"){var w=t.aIds[v.replace(/^#/,"")];
if(w!==bn){return[w.idx]
}}return bo(u).filter(v).map(function(){return this._DT_RowIndex
}).toArray()
},t,s)
},1);
m.selector.rows=k;
m.selector.opts=h;
return m
});
bl("rows().nodes()",function(){return this.iterator("row",function(k,h){return k.aoData[h].nTr||bn
},1)
});
bl("rows().data()",function(){return this.iterator(!0,"rows",function(k,h){return ad(k.aoData,h,"_aData")
},1)
});
bi("rows().cache()","row().cache()",function(h){return this.iterator("row",function(k,p){var m=k.aoData[p];
return"search"===h?m._aFilterData:m._aSortData
},1)
});
bi("rows().invalidate()","row().invalidate()",function(h){return this.iterator("row",function(k,m){a4(k,m,h)
})
});
bi("rows().indexes()","row().index()",function(){return this.iterator("row",function(k,h){return h
},1)
});
bi("rows().ids()","row().id()",function(m){for(var k=[],w=this.context,v=0,u=w.length;
v<u;
v++){for(var t=0,s=this[v].length;
t<s;
t++){var p=w[v].rowIdFn(w[v].aoData[this[v][t]]._aData);
k.push((!0===m?"#":"")+p)
}}return new bh(w,k)
});
bi("rows().remove()","row().remove()",function(){var h=this;
this.iterator("row",function(y,x,w){var v=y.aoData,u=v[x],t,s,p,k,m;
v.splice(x,1);
t=0;
for(s=v.length;
t<s;
t++){if(p=v[t],m=p.anCells,null!==p.nTr&&(p.nTr._DT_RowIndex=t),null!==m){p=0;
for(k=m.length;
p<k;
p++){m[p]._DT_CellIndex.row=t
}}}aZ(y.aiDisplayMaster,x);
aZ(y.aiDisplay,x);
aZ(h[w],x,!1);
b4(y);
x=y.rowIdFn(u._aData);
x!==bn&&delete y.aIds[x]
});
this.iterator("table",function(k){for(var p=0,m=k.aoData.length;
p<m;
p++){k.aoData[p].idx=p
}});
return this
});
bl("rows.add()",function(k){var h=this.iterator("table",function(p){var v,u,t,s=[];
u=0;
for(t=k.length;
u<t;
u++){v=k[u],v.nodeName&&"TR"===v.nodeName.toUpperCase()?s.push(bv(p,v)[0]):s.push(bH(p,v))
}return s
},1),m=this.rows(-1);
m.pop();
bo.merge(m,h);
return m
});
bl("row()",function(k,h){return a3(this.rows(k,h))
});
bl("row().data()",function(k){var h=this.context;
if(k===bn){return h.length&&this.length?h[0].aoData[this[0]]._aData:bn
}h[0].aoData[this[0]]._aData=k;
a4(h[0],this[0],"data");
return this
});
bl("row().node()",function(){var h=this.context;
return h.length&&this.length?h[0].aoData[this[0]].nTr||null:null
});
bl("row.add()",function(k){k instanceof bo&&k.length&&(k=k[0]);
var h=this.iterator("table",function(m){return k.nodeName&&"TR"===k.nodeName.toUpperCase()?bv(m,k)[0]:bH(m,k)
});
return this.row(h[0])
});
var aU=function(k,h){var m=k.context;
if(m.length&&(m=m[0].aoData[h!==bn?h:k[0]])&&m._details){m._details.remove(),m._detailsShow=bn,m._details=bn
}},bk=function(k,h){var u=k.context;
if(u.length&&k.length){var t=u[0].aoData[k[0]];
if(t._details){(t._detailsShow=h)?t._details.insertAfter(t.nTr):t._details.detach();
var s=u[0],p=new bh(s),m=s.aoData;
p.off("draw.dt.DT_details column-visibility.dt.DT_details destroy.dt.DT_details");
0<bT(m,"_details").length&&(p.on("draw.dt.DT_details",function(w,v){s===v&&p.rows({page:"current"}).eq(0).each(function(x){x=m[x];
x._detailsShow&&x._details.insertAfter(x.nTr)
})
}),p.on("column-visibility.dt.DT_details",function(w,v){if(s===v){for(var A,z=bQ(v),y=0,x=m.length;
y<x;
y++){A=m[y],A._details&&A._details.children("td[colspan]").attr("colspan",z)
}}}),p.on("destroy.dt.DT_details",function(w,v){if(s===v){for(var y=0,x=m.length;
y<x;
y++){m[y]._details&&aU(p,y)
}}}))
}}};
bl("row().child()",function(k,h){var t=this.context;
if(k===bn){return t.length&&this.length?t[0].aoData[this[0]]._details:bn
}if(!0===k){this.child.show()
}else{if(!1===k){aU(this)
}else{if(t.length&&this.length){var s=t[0],t=t[0].aoData[this[0]],p=[],m=function(v,u){if(bo.isArray(v)||v instanceof bo){for(var x=0,w=v.length;
x<w;
x++){m(v[x],u)
}}else{v.nodeName&&"tr"===v.nodeName.toLowerCase()?p.push(v):(x=bo("<tr><td/></tr>").addClass(u),bo("td",x).addClass(u).html(v)[0].colSpan=bQ(s),p.push(x[0]))
}};
m(k,h);
t._details&&t._details.detach();
t._details=bo(p);
t._detailsShow&&t._details.insertAfter(t.nTr)
}}}return this
});
bl(["row().child.show()","row().child().show()"],function(){bk(this,!0);
return this
});
bl(["row().child.hide()","row().child().hide()"],function(){bk(this,!1);
return this
});
bl(["row().child.remove()","row().child().remove()"],function(){aU(this);
return this
});
bl("row().child.isShown()",function(){var h=this.context;
return h.length&&this.length?h[0].aoData[this[0]]._detailsShow||!1:!1
});
var aL=/^([^:]+):(name|visIdx|visible)$/,a0=function(k,h,t,s,p){for(var t=[],s=0,m=p.length;
s<m;
s++){t.push(bV(k,p[s],h))
}return t
};
bl("columns()",function(k,h){k===bn?k="":bo.isPlainObject(k)&&(h=k,k="");
var h=bq(h),m=this.iterator("table",function(w){var v=k,u=h,t=w.aoColumns,p=bT(t,"sName"),s=bT(t,"nTh");
return bO("column",v,function(z){var y=ae(z);
if(z===""){return by(t.length)
}if(y!==null){return[y>=0?y:t.length+y]
}if(typeof z==="function"){var B=aj(w,u);
return bo.map(t,function(C,D){return z(D,a0(w,D,0,0,B),s[D])?D:null
})
}var A=typeof z==="string"?z.match(aL):"";
if(A){switch(A[2]){case"visIdx":case"visible":y=parseInt(A[1],10);
if(y<0){var x=bo.map(t,function(D,C){return D.bVisible?C:null
});
return[x[x.length+y]]
}return[b9(w,y)];
case"name":return bo.map(p,function(D,C){return D===A[1]?C:null
});
default:return[]
}}if(z.nodeName&&z._DT_CellIndex){return[z._DT_CellIndex.column]
}y=bo(s).filter(z).map(function(){return bo.inArray(this,s)
}).toArray();
if(y.length||!z.nodeName){return y
}y=bo(z).closest("*[data-dt-column]");
return y.length?[y.data("dt-column")]:[]
},w,u)
},1);
m.selector.cols=k;
m.selector.opts=h;
return m
});
bi("columns().header()","column().header()",function(){return this.iterator("column",function(k,h){return k.aoColumns[h].nTh
},1)
});
bi("columns().footer()","column().footer()",function(){return this.iterator("column",function(k,h){return k.aoColumns[h].nTf
},1)
});
bi("columns().data()","column().data()",function(){return this.iterator("column-rows",a0,1)
});
bi("columns().dataSrc()","column().dataSrc()",function(){return this.iterator("column",function(k,h){return k.aoColumns[h].mData
},1)
});
bi("columns().cache()","column().cache()",function(h){return this.iterator("column-rows",function(k,t,s,p,m){return ad(k.aoData,m,"search"===h?"_aFilterData":"_aSortData",t)
},1)
});
bi("columns().nodes()","column().nodes()",function(){return this.iterator("column-rows",function(k,h,s,p,m){return ad(k.aoData,m,"anCells",h)
},1)
});
bi("columns().visible()","column().visible()",function(k,h){var m=this.iterator("column",function(z,y){if(k===bn){return z.aoColumns[y].bVisible
}var x=z.aoColumns,w=x[y],u=z.aoData,v,p,t;
if(k!==bn&&w.bVisible!==k){if(k){var s=bo.inArray(!0,bT(x,"bVisible"),y+1);
v=0;
for(p=u.length;
v<p;
v++){t=u[v].nTr,x=u[v].anCells,t&&t.insertBefore(x[y],x[s]||null)
}}else{bo(bT(z.aoData,"anCells",y)).detach()
}w.bVisible=k;
aN(z,z.aoHeader);
aN(z,z.aoFooter);
b6(z)
}});
k!==bn&&(this.iterator("column",function(s,p){bj(s,null,"column-visibility",[s,p,k,h])
}),(h===bn||h)&&this.columns.adjust());
return m
});
bi("columns().indexes()","column().index()",function(h){return this.iterator("column",function(k,m){return"visible"===h?b8(k,m):m
},1)
});
bl("columns.adjust()",function(){return this.iterator("table",function(h){bw(h)
},1)
});
bl("column.index()",function(k,h){if(0!==this.context.length){var m=this.context[0];
if("fromVisible"===k||"toData"===k){return b9(m,h)
}if("fromData"===k||"toVisible"===k){return b8(m,h)
}}});
bl("column()",function(k,h){return a3(this.columns(k,h))
});
bl("cells()",function(y,x,w){bo.isPlainObject(y)&&(y.row===bn?(w=y,y=null):(w=x,x=null));
bo.isPlainObject(x)&&(w=x,x=null);
if(null===x||x===bn){return this.iterator("table",function(J){var I=y,H=bq(w),G=J.aoData,F=aj(J,H),E=ch(ad(G,F,"anCells")),D=bo([].concat.apply([],E)),C,A=J.aoColumns.length,B,z,N,M,O,K;
return bO("cell",I,function(R){var S=typeof R==="function";
if(R===null||R===bn||S){B=[];
z=0;
for(N=F.length;
z<N;
z++){C=F[z];
for(M=0;
M<A;
M++){O={row:C,column:M};
if(S){K=G[C];
R(O,bV(J,C,M),K.anCells?K.anCells[M]:null)&&B.push(O)
}else{B.push(O)
}}}return B
}if(bo.isPlainObject(R)){return[R]
}S=D.filter(R).map(function(V,T){return{row:T._DT_CellIndex.row,column:T._DT_CellIndex.column}
}).toArray();
if(S.length||!R.nodeName){return S
}K=bo(R).closest("*[data-dt-row]");
return K.length?[{row:K.data("dt-row"),column:K.data("dt-column")}]:[]
},J,H)
})
}var v=this.columns(x,w),u=this.rows(y,w),t,s,m,p,h,k=this.iterator("table",function(A,z){t=[];
s=0;
for(m=u[z].length;
s<m;
s++){p=0;
for(h=v[z].length;
p<h;
p++){t.push({row:u[z][s],column:v[z][p]})
}}return t
},1);
bo.extend(k.selector,{cols:x,rows:y,opts:w});
return k
});
bi("cells().nodes()","cell().node()",function(){return this.iterator("cell",function(k,h,m){return(k=k.aoData[h])&&k.anCells?k.anCells[m]:bn
},1)
});
bl("cells().data()",function(){return this.iterator("cell",function(k,h,m){return bV(k,h,m)
},1)
});
bi("cells().cache()","cell().cache()",function(h){h="search"===h?"_aFilterData":"_aSortData";
return this.iterator("cell",function(k,p,m){return k.aoData[p][h][m]
},1)
});
bi("cells().render()","cell().render()",function(h){return this.iterator("cell",function(k,p,m){return bV(k,p,m,h)
},1)
});
bi("cells().indexes()","cell().index()",function(){return this.iterator("cell",function(k,h,m){return{row:h,column:m,columnVisible:b8(k,m)}
},1)
});
bi("cells().invalidate()","cell().invalidate()",function(h){return this.iterator("cell",function(k,p,m){a4(k,p,h,m)
})
});
bl("cell()",function(k,h,m){return a3(this.cells(k,h,m))
});
bl("cell().data()",function(k){var h=this.context,m=this[0];
if(k===bn){return h.length&&m.length?bV(h[0],m[0].row,m[0].column):bn
}a(h[0],m[0].row,m[0].column,k);
a4(h[0],m[0].row,"data",m[0].column);
return this
});
bl("order()",function(k,h){var m=this.context;
if(k===bn){return 0!==m.length?m[0].aaSorting:bn
}"number"===typeof k?k=[[k,h]]:k.length&&!bo.isArray(k[0])&&(k=Array.prototype.slice.call(arguments));
return this.iterator("table",function(p){p.aaSorting=k.slice()
})
});
bl("order.listener()",function(k,h,m){return this.iterator("table",function(p){aw(p,k,h,m)
})
});
bl("order.fixed()",function(k){if(!k){var h=this.context,h=h.length?h[0].aaSortingFixed:bn;
return bo.isArray(h)?{pre:h}:h
}return this.iterator("table",function(m){m.aaSortingFixed=bo.extend(!0,{},k)
})
});
bl(["columns().order()","column().order()"],function(k){var h=this;
return this.iterator("table",function(s,p){var m=[];
bo.each(h[p],function(t,u){m.push([u,k])
});
s.aaSorting=m
})
});
bl("search()",function(k,h,s,p){var m=this.context;
return k===bn?0!==m.length?m[0].oPreviousSearch.sSearch:bn:this.iterator("table",function(t){t.oFeatures.bFilter&&aC(t,bo.extend({},t.oPreviousSearch,{sSearch:k+"",bRegex:null===h?!1:h,bSmart:null===s?!0:s,bCaseInsensitive:null===p?!0:p}),1)
})
});
bi("columns().search()","column().search()",function(k,h,p,m){return this.iterator("column",function(u,t){var s=u.aoPreSearchCols;
if(k===bn){return s[t].sSearch
}u.oFeatures.bFilter&&(bo.extend(s[t],{sSearch:k+"",bRegex:null===h?!1:h,bSmart:null===p?!0:p,bCaseInsensitive:null===m?!0:m}),aC(u,u.oPreviousSearch,1))
})
});
bl("state()",function(){return this.context.length?this.context[0].oSavedState:null
});
bl("state.clear()",function(){return this.iterator("table",function(h){h.fnStateSaveCallback.call(h.oInstance,h,{})
})
});
bl("state.loaded()",function(){return this.context.length?this.context[0].oLoadedState:null
});
bl("state.save()",function(){return this.iterator("table",function(h){b6(h)
})
});
bm.versionCheck=bm.fnVersionCheck=function(k){for(var h=bm.version.split("."),k=k.split("."),t,s,p=0,m=k.length;
p<m;
p++){if(t=parseInt(h[p],10)||0,s=parseInt(k[p],10)||0,t!==s){return t>s
}}return !0
};
bm.isDataTable=bm.fnIsDataTable=function(k){var h=bo(k).get(0),m=!1;
if(k instanceof bm.Api){return !0
}bo.each(bm.settings,function(p,u){var t=u.nScrollHead?bo("table",u.nScrollHead)[0]:null,s=u.nScrollFoot?bo("table",u.nScrollFoot)[0]:null;
if(u.nTable===h||t===h||s===h){m=!0
}});
return m
};
bm.tables=bm.fnTables=function(k){var h=!1;
bo.isPlainObject(k)&&(h=k.api,k=k.visible);
var m=bo.map(bm.settings,function(p){if(!k||k&&bo(p.nTable).is(":visible")){return p.nTable
}});
return h?new bh(m):m
};
bm.camelToHungarian=bK;
bl("$()",function(k,h){var m=this.rows(h).nodes(),m=bo(m);
return bo([].concat(m.filter(k).toArray(),m.find(k).toArray()))
});
bo.each(["on","one","off"],function(k,h){bl(h+"()",function(){var m=Array.prototype.slice.call(arguments);
m[0]=bo.map(m[0].split(/\s/),function(s){return !s.match(/\.dt\b/)?s+".dt":s
}).join(" ");
var p=bo(this.tables().nodes());
p[h].apply(p,m);
return this
})
});
bl("clear()",function(){return this.iterator("table",function(h){a9(h)
})
});
bl("settings()",function(){return new bh(this.context,this.context)
});
bl("init()",function(){var h=this.context;
return h.length?h[0].oInit:null
});
bl("data()",function(){return this.iterator("table",function(h){return bT(h.aoData,"_aData")
}).flatten()
});
bl("destroy()",function(h){h=h||!1;
return this.iterator("table",function(B){var A=B.nTableWrapper.parentNode,z=B.oClasses,y=B.nTable,x=B.nTBody,w=B.nTHead,u=B.nTFoot,v=bo(y),x=bo(x),t=bo(B.nTableWrapper),s=bo.map(B.aoData,function(k){return k.nTr
}),m;
B.bDestroying=!0;
bj(B,"aoDestroyCallback","destroy",[B]);
h||(new bh(B)).columns().visible(!0);
t.off(".DT").find(":not(tbody *)").off(".DT");
bo(bS).off(".DT-"+B.sInstance);
y!=w.parentNode&&(v.children("thead").detach(),v.append(w));
u&&y!=u.parentNode&&(v.children("tfoot").detach(),v.append(u));
B.aaSorting=[];
B.aaSortingFixed=[];
ck(B);
bo(s).removeClass(B.asStripeClasses.join(" "));
bo("th, td",w).removeClass(z.sSortable+" "+z.sSortableAsc+" "+z.sSortableDesc+" "+z.sSortableNone);
B.bJUI&&(bo("th span."+z.sSortIcon+", td span."+z.sSortIcon,w).detach(),bo("th, td",w).each(function(){var k=bo("div."+z.sSortJUIWrapper,this);
bo(this).append(k.contents());
k.detach()
}));
x.children().detach();
x.append(s);
w=h?"remove":"detach";
v[w]();
t[w]();
!h&&A&&(A.insertBefore(y,B.nTableReinsertBefore),v.css("width",B.sDestroyWidth).removeClass(z.sTable),(m=B.asDestroyStripes.length)&&x.children().each(function(k){bo(this).addClass(B.asDestroyStripes[k%m])
}));
A=bo.inArray(B,bm.settings);
-1!==A&&bm.settings.splice(A,1)
})
});
bo.each(["column","row","cell"],function(k,h){bl(h+"s().every()",function(m){var s=this.selector.opts,p=this;
return this.iterator(h,function(x,w,v,u,t){m.call(p[h](w,"cell"===h?v:s,"cell"===h?s:bn),w,v,u,t)
})
})
});
bl("i18n()",function(k,h,p){var m=this.context[0],k=bE(k)(m.oLanguage);
k===bn&&(k=h);
p!==bn&&bo.isPlainObject(k)&&(k=k[p]!==bn?k[p]:k._);
return k.replace("%d",p)
});
bm.version="1.10.13";
bm.settings=[];
bm.models={};
bm.models.oSearch={bCaseInsensitive:!0,sSearch:"",bRegex:!1,bSmart:!0};
bm.models.oRow={nTr:null,anCells:null,_aData:[],_aSortData:null,_aFilterData:null,_sFilterRow:null,_sRowStripe:"",src:null,idx:-1};
bm.models.oColumn={idx:null,aDataSort:null,asSorting:null,bSearchable:null,bSortable:null,bVisible:null,_sManualType:null,_bAttrSrc:!1,fnCreatedCell:null,fnGetData:null,fnSetData:null,mData:null,mRender:null,nTh:null,nTf:null,sClass:null,sContentPadding:null,sDefaultContent:null,sName:null,sSortDataType:"std",sSortingClass:null,sSortingClassJUI:null,sTitle:null,sType:null,sWidth:null,sWidthOrig:null};
bm.defaults={aaData:null,aaSorting:[[0,"asc"]],aaSortingFixed:[],ajax:null,aLengthMenu:[10,25,50,100],aoColumns:null,aoColumnDefs:null,aoSearchCols:[],asStripeClasses:null,bAutoWidth:!0,bDeferRender:!1,bDestroy:!1,bFilter:!0,bInfo:!0,bJQueryUI:!1,bLengthChange:!0,bPaginate:!0,bProcessing:!1,bRetrieve:!1,bScrollCollapse:!1,bServerSide:!1,bSort:!0,bSortMulti:!0,bSortCellsTop:!1,bSortClasses:!0,bStateSave:!1,fnCreatedRow:null,fnDrawCallback:null,fnFooterCallback:null,fnFormatNumber:function(h){return h.toString().replace(/\B(?=(\d{3})+(?!\d))/g,this.oLanguage.sThousands)
},fnHeaderCallback:null,fnInfoCallback:null,fnInitComplete:null,fnPreDrawCallback:null,fnRowCallback:null,fnServerData:null,fnServerParams:null,fnStateLoadCallback:function(k){try{return JSON.parse((-1===k.iStateDuration?sessionStorage:localStorage).getItem("DataTables_"+k.sInstance+"_"+location.pathname))
}catch(h){}},fnStateLoadParams:null,fnStateLoaded:null,fnStateSaveCallback:function(k,h){try{(-1===k.iStateDuration?sessionStorage:localStorage).setItem("DataTables_"+k.sInstance+"_"+location.pathname,JSON.stringify(h))
}catch(m){}},fnStateSaveParams:null,iStateDuration:7200,iDeferLoading:null,iDisplayLength:10,iDisplayStart:0,iTabIndex:0,oClasses:{},oLanguage:{oAria:{sSortAscending:": activate to sort column ascending",sSortDescending:": activate to sort column descending"},oPaginate:{sFirst:"First",sLast:"Last",sNext:"Next",sPrevious:"Previous"},sEmptyTable:"No data available in table",sInfo:"Showing _START_ to _END_ of _TOTAL_ entries",sInfoEmpty:"Showing 0 to 0 of 0 entries",sInfoFiltered:"(filtered from _MAX_ total entries)",sInfoPostFix:"",sDecimal:"",sThousands:",",sLengthMenu:"Show _MENU_ entries",sLoadingRecords:"Loading...",sProcessing:"Processing...",sSearch:"Search:",sSearchPlaceholder:"",sUrl:"",sZeroRecords:"No matching records found"},oSearch:bo.extend({},bm.models.oSearch),sAjaxDataProp:"data",sAjaxSource:null,sDom:"lfrtip",searchDelay:null,sPaginationType:"simple_numbers",sScrollX:"",sScrollXInner:"",sScrollY:"",sServerMethod:"GET",renderer:null,rowId:"DT_RowId"};
bx(bm.defaults);
bm.defaults.column={aDataSort:null,iDataSort:-1,asSorting:["asc","desc"],bSearchable:!0,bSortable:!0,bVisible:!0,fnCreatedCell:null,mData:null,mRender:null,sCellType:"td",sClass:"",sContentPadding:"",sDefaultContent:null,sName:"",sSortDataType:"std",sTitle:null,sType:null,sWidth:null};
bx(bm.defaults.column);
bm.models.oSettings={oFeatures:{bAutoWidth:null,bDeferRender:null,bFilter:null,bInfo:null,bLengthChange:null,bPaginate:null,bProcessing:null,bServerSide:null,bSort:null,bSortMulti:null,bSortClasses:null,bStateSave:null},oScroll:{bCollapse:null,iBarWidth:0,sX:null,sXInner:null,sY:null},oLanguage:{fnInfoCallback:null},oBrowser:{bScrollOversize:!1,bScrollbarLeft:!1,bBounding:!1,barWidth:0},ajax:null,aanFeatures:[],aoData:[],aiDisplay:[],aiDisplayMaster:[],aIds:{},aoColumns:[],aoHeader:[],aoFooter:[],oPreviousSearch:{},aoPreSearchCols:[],aaSorting:null,aaSortingFixed:[],asStripeClasses:null,asDestroyStripes:[],sDestroyWidth:0,aoRowCallback:[],aoHeaderCallback:[],aoFooterCallback:[],aoDrawCallback:[],aoRowCreatedCallback:[],aoPreDrawCallback:[],aoInitComplete:[],aoStateSaveParams:[],aoStateLoadParams:[],aoStateLoaded:[],sTableId:"",nTable:null,nTHead:null,nTFoot:null,nTBody:null,nTableWrapper:null,bDeferLoading:!1,bInitialised:!1,aoOpenRows:[],sDom:null,searchDelay:null,sPaginationType:"two_button",iStateDuration:0,aoStateSave:[],aoStateLoad:[],oSavedState:null,oLoadedState:null,sAjaxSource:null,sAjaxDataProp:null,bAjaxDataGet:!0,jqXHR:null,json:bn,oAjaxData:bn,fnServerData:null,aoServerParams:[],sServerMethod:null,fnFormatNumber:null,aLengthMenu:null,iDraw:0,bDrawing:!1,iDrawError:-1,_iDisplayLength:10,_iDisplayStart:0,_iRecordsTotal:0,_iRecordsDisplay:0,bJUI:null,oClasses:{},bFiltered:!1,bSorted:!1,bSortCellsTop:null,oInit:null,aoDestroyCallback:[],fnRecordsTotal:function(){return"ssp"==bd(this)?1*this._iRecordsTotal:this.aiDisplayMaster.length
},fnRecordsDisplay:function(){return"ssp"==bd(this)?1*this._iRecordsDisplay:this.aiDisplay.length
},fnDisplayEnd:function(){var k=this._iDisplayLength,h=this._iDisplayStart,t=h+k,s=this.aiDisplay.length,p=this.oFeatures,m=p.bPaginate;
return p.bServerSide?!1===m||-1===k?h+s:Math.min(h+k,this._iRecordsDisplay):!m||t>s||-1===k?s:t
},oInstance:null,sInstance:null,iTabIndex:0,nScrollHead:null,nScrollFoot:null,aLastSort:[],oPlugins:{},rowIdFn:null,rowId:null};
bm.ext=be={buttons:{},classes:{},builder:"-source-",errMode:"alert",feature:[],search:[],selector:{cell:[],column:[],row:[]},internal:{},legacy:{ajax:null},pager:{},renderer:{pageButton:{},header:{}},order:{},type:{detect:[],search:{},order:{}},_unique:0,fnVersionCheck:bm.fnVersionCheck,iApiIndex:0,oJUIClasses:{},sVersion:bm.version};
bo.extend(be,{afnFiltering:be.search,aTypes:be.type.detect,ofnSearch:be.type.search,oSort:be.type.order,afnSortData:be.order,aoFeatures:be.feature,oApi:be.internal,oStdClasses:be.classes,oPagination:be.pager});
bo.extend(bm.ext.classes,{sTable:"dataTable",sNoFooter:"no-footer",sPageButton:"paginate_button",sPageButtonActive:"current",sPageButtonDisabled:"disabled",sStripeOdd:"odd",sStripeEven:"even",sRowEmpty:"dataTables_empty",sWrapper:"dataTables_wrapper",sFilter:"dataTables_filter",sInfo:"dataTables_info",sPaging:"dataTables_paginate paging_",sLength:"dataTables_length",sProcessing:"dataTables_processing",sSortAsc:"sorting_asc",sSortDesc:"sorting_desc",sSortable:"sorting",sSortableAsc:"sorting_asc_disabled",sSortableDesc:"sorting_desc_disabled",sSortableNone:"sorting_disabled",sSortColumn:"sorting_",sFilterInput:"",sLengthSelect:"",sScrollWrapper:"dataTables_scroll",sScrollHead:"dataTables_scrollHead",sScrollHeadInner:"dataTables_scrollHeadInner",sScrollBody:"dataTables_scrollBody",sScrollFoot:"dataTables_scrollFoot",sScrollFootInner:"dataTables_scrollFootInner",sHeaderTH:"",sFooterTH:"",sSortJUIAsc:"",sSortJUIDesc:"",sSortJUI:"",sSortJUIAscAllowed:"",sSortJUIDescAllowed:"",sSortJUIWrapper:"",sSortIcon:"",sJUIHeader:"",sJUIFooter:""});
var U="",U="",bP=U+"ui-state-default",o=U+"css_right ui-icon ui-icon-",aR=U+"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix";
bo.extend(bm.ext.oJUIClasses,bm.ext.classes,{sPageButton:"fg-button ui-button "+bP,sPageButtonActive:"ui-state-disabled",sPageButtonDisabled:"ui-state-disabled",sPaging:"dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",sSortAsc:bP+" sorting_asc",sSortDesc:bP+" sorting_desc",sSortable:bP+" sorting",sSortableAsc:bP+" sorting_asc_disabled",sSortableDesc:bP+" sorting_desc_disabled",sSortableNone:bP+" sorting_disabled",sSortJUIAsc:o+"triangle-1-n",sSortJUIDesc:o+"triangle-1-s",sSortJUI:o+"carat-2-n-s",sSortJUIAscAllowed:o+"carat-1-n",sSortJUIDescAllowed:o+"carat-1-s",sSortJUIWrapper:"DataTables_sort_wrapper",sSortIcon:"DataTables_sort_icon",sScrollHead:"dataTables_scrollHead "+bP,sScrollFoot:"dataTables_scrollFoot "+bP,sHeaderTH:bP,sFooterTH:bP,sJUIHeader:aR+" ui-corner-tl ui-corner-tr",sJUIFooter:aR+" ui-corner-bl ui-corner-br"});
var aD=bm.ext.pager;
bo.extend(aD,{simple:function(){return["previous","next"]
},full:function(){return["first","previous","next","last"]
},numbers:function(k,h){return[al(k,h)]
},simple_numbers:function(k,h){return["previous",al(k,h),"next"]
},full_numbers:function(k,h){return["first","previous",al(k,h),"next","last"]
},first_last_numbers:function(k,h){return["first",al(k,h),"last"]
},_numbers:al,numbers_length:7});
bo.extend(!0,bm.ext.renderer,{pageButton:{_:function(E,D,C,B,A,z){var y=E.oClasses,w=E.oLanguage.oPaginate,x=E.oLanguage.oAria.paginate||{},s,v,k=0,h=function(m,M){var H,J,I,K,p=function(t){bF(E,t.data.action,true)
};
H=0;
for(J=M.length;
H<J;
H++){K=M[H];
if(bo.isArray(K)){I=bo("<"+(K.DT_el||"div")+"/>").appendTo(m);
h(I,K)
}else{s=null;
v="";
switch(K){case"ellipsis":m.append('<span class="ellipsis">&#x2026;</span>');
break;
case"first":s=w.sFirst;
v=K+(A>0?"":" "+y.sPageButtonDisabled);
break;
case"previous":s=w.sPrevious;
v=K+(A>0?"":" "+y.sPageButtonDisabled);
break;
case"next":s=w.sNext;
v=K+(A<z-1?"":" "+y.sPageButtonDisabled);
break;
case"last":s=w.sLast;
v=K+(A<z-1?"":" "+y.sPageButtonDisabled);
break;
default:s=K+1;
v=A===K?y.sPageButtonActive:""
}if(s!==null){I=bo("<a>",{"class":y.sPageButton+" "+v,"aria-controls":E.sTableId,"aria-label":x[K],"data-dt-idx":k,tabindex:E.iTabIndex,id:C===0&&typeof K==="string"?E.sTableId+"_"+K:null}).html(s).appendTo(m);
aS(I,{action:K},p);
k++
}}}},G;
try{G=bo(D).find(bN.activeElement).data("dt-idx")
}catch(F){}h(bo(D).empty(),B);
G!==bn&&bo(D).find("[data-dt-idx="+G+"]").focus()
}}});
bo.extend(bm.ext.type.detect,[function(k,h){var m=h.oLanguage.sDecimal;
return b7(k,m)?"num"+m:null
},function(k){if(k&&!(k instanceof Date)&&!bp.test(k)){return null
}var h=Date.parse(k);
return null!==h&&!isNaN(h)||bI(k)?"date":null
},function(k,h){var m=h.oLanguage.sDecimal;
return b7(k,m,!0)?"num-fmt"+m:null
},function(k,h){var m=h.oLanguage.sDecimal;
return c(k,m)?"html-num"+m:null
},function(k,h){var m=h.oLanguage.sDecimal;
return c(k,m,!0)?"html-num-fmt"+m:null
},function(h){return bI(h)||"string"===typeof h&&-1!==h.indexOf("<")?"html":null
}]);
bo.extend(bm.ext.type.search,{html:function(h){return bI(h)?h:"string"===typeof h?h.replace(am," ").replace(ar,""):""
},string:function(h){return bI(h)?h:"string"===typeof h?h.replace(am," "):h
}});
var aA=function(k,h,p,m){if(0!==k&&(!k||"-"===k)){return -Infinity
}h&&(k=q(k,h));
k.replace&&(p&&(k=k.replace(p,"")),m&&(k=k.replace(m,"")));
return 1*k
};
bo.extend(be.type.order,{"date-pre":function(h){return Date.parse(h)||-Infinity
},"html-pre":function(h){return bI(h)?"":h.replace?h.replace(/<.*?>/g,"").toLowerCase():h+""
},"string-pre":function(h){return bI(h)?"":"string"===typeof h?h.toLowerCase():!h.toString?"":h.toString()
},"string-asc":function(k,h){return k<h?-1:k>h?1:0
},"string-desc":function(k,h){return k<h?1:k>h?-1:0
}});
aM("");
bo.extend(!0,bm.ext.renderer,{header:{_:function(k,h,p,m){bo(k.nTable).on("order.dt.DT",function(v,u,t,s){if(k===u){v=p.idx;
h.removeClass(p.sSortingClass+" "+m.sSortAsc+" "+m.sSortDesc).addClass(s[v]=="asc"?m.sSortAsc:s[v]=="desc"?m.sSortDesc:p.sSortingClass)
}})
},jqueryui:function(k,h,p,m){bo("<div/>").addClass(m.sSortJUIWrapper).append(h.contents()).append(bo("<span/>").addClass(m.sSortIcon+" "+p.sSortingClassJUI)).appendTo(h);
bo(k.nTable).on("order.dt.DT",function(v,u,t,s){if(k===u){v=p.idx;
h.removeClass(m.sSortAsc+" "+m.sSortDesc).addClass(s[v]=="asc"?m.sSortAsc:s[v]=="desc"?m.sSortDesc:p.sSortingClass);
h.find("span."+m.sSortIcon).removeClass(m.sSortJUIAsc+" "+m.sSortJUIDesc+" "+m.sSortJUI+" "+m.sSortJUIAscAllowed+" "+m.sSortJUIDescAllowed).addClass(s[v]=="asc"?m.sSortJUIAsc:s[v]=="desc"?m.sSortJUIDesc:p.sSortingClassJUI)
}})
}}});
var aH=function(h){return"string"===typeof h?h.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;"):h
};
bm.render={number:function(k,h,s,p,m){return{display:function(v){if("number"!==typeof v&&"string"!==typeof v){return v
}var u=0>v?"-":"",t=parseFloat(v);
if(isNaN(t)){return aH(v)
}t=t.toFixed(s);
v=Math.abs(t);
t=parseInt(v,10);
v=s?h+(v-t).toFixed(s).substring(2):"";
return u+(p||"")+t.toString().replace(/\B(?=(\d{3})+(?!\d))/g,k)+v+(m||"")
}}
},text:function(){return{display:aH}
}};
bo.extend(bm.ext.internal,{_fnExternApiFunc:av,_fnBuildAjax:ap,_fnAjaxUpdate:b1,_fnAjaxParameters:L,_fnAjaxUpdateDraw:e,_fnAjaxDataSrc:ah,_fnAddColumn:cm,_fnColumnOptions:b,_fnAdjustColumnSizing:bw,_fnVisibleToColumnIndex:b9,_fnColumnIndexToVisible:b8,_fnVisbleColumns:bQ,_fnGetColumns:b2,_fnColumnTypes:b0,_fnApplyColumnDefs:n,_fnHungarianMap:bx,_fnCamelToHungarian:bK,_fnLanguageCompat:l,_fnBrowserDetect:ak,_fnAddData:bH,_fnAddTr:bv,_fnNodeToDataIndex:function(k,h){return h._DT_RowIndex!==bn?h._DT_RowIndex:null
},_fnNodeToColumnIndex:function(k,h,m){return bo.inArray(m,k.aoData[h].anCells)
},_fnGetCellData:bV,_fnSetCellData:a,_fnSplitObjNotation:aX,_fnGetObjectDataFn:bE,_fnSetObjectDataFn:bC,_fnGetDataMaster:aP,_fnClearTable:a9,_fnDeleteIndex:aZ,_fnInvalidate:a4,_fnGetRowElements:a6,_fnCreateTr:bt,_fnBuildHead:cf,_fnDrawHead:aN,_fnDraw:bG,_fnReDraw:bB,_fnAddOptionsHtml:a7,_fnDetectHeader:aV,_fnGetUniqueThs:ay,_fnFeatureHtmlFilter:aQ,_fnFilterComplete:aC,_fnFilterCustom:aJ,_fnFilterColumn:b5,_fnFilter:cj,_fnFilterCreateSearch:r,_fnEscapeRegex:d,_fnFilterData:az,_fnFeatureHtmlInfo:ao,_fnUpdateInfo:Q,_fnInfoMacros:g,_fnInitialise:au,_fnInitComplete:P,_fnLengthChange:ci,_fnFeatureHtmlLength:aY,_fnFeatureHtmlPaginate:ag,_fnPageChange:bF,_fnFeatureHtmlProcessing:aF,_fnProcessingDisplay:bU,_fnFeatureHtmlTable:ax,_fnScrollDraw:cg,_fnApplyToChildren:bL,_fnCalculateColumnWidths:ce,_fnThrottle:af,_fnConvertToWidth:cl,_fnGetWidestNode:cd,_fnGetMaxLenString:bZ,_fnStringToCss:bg,_fnSortFlatten:bz,_fnSort:bu,_fnSortAria:a5,_fnSortListener:a1,_fnSortAttachListener:aw,_fnSortingClasses:ck,_fnSortData:bs,_fnSaveState:b6,_fnLoadState:aW,_fnSettingsFromNode:aK,_fnLog:bJ,_fnMap:bR,_fnBindAction:aS,_fnCallbackReg:a8,_fnCallbackFire:bj,_fnLengthOverflow:b4,_fnRenderer:an,_fnDataSource:bd,_fnRowAttributes:aE,_fnCalculateEnd:function(){}});
bo.fn.dataTable=bm;
bm.$=bo;
bo.fn.dataTableSettings=bm.settings;
bo.fn.dataTableExt=bm.ext;
bo.fn.DataTable=function(h){return bo(this).dataTable(h).api()
};
bo.each(bm,function(k,h){bo.fn.DataTable[k]=h
});
return bo.fn.dataTable
});
/*!
 FixedColumns 3.2.2
 ©2010-2016 SpryMedia Ltd - datatables.net/license
*/
(function(a){"function"===typeof define&&define.amd?define(["jquery","datatables.net"],function(b){return a(b,window,document)
}):"object"===typeof exports?module.exports=function(c,b){c||(c=window);
if(!b||!b.fn.dataTable){b=require("datatables.net")(c,b).$
}return a(b,c,c.document)
}:a(jQuery,window,document)
})(function(h,g,f,c){var e=h.fn.dataTable,b,a=function(k,d){var m=this;
if(this instanceof a){if(d===c||!0===d){d={}
}var l=h.fn.dataTable.camelToHungarian;
l&&(l(a.defaults,a.defaults,!0),l(a.defaults,d));
l=(new h.fn.dataTable.Api(k)).settings()[0];
this.s={dt:l,iTableColumns:l.aoColumns.length,aiOuterWidths:[],aiInnerWidths:[],rtl:"rtl"===h(l.nTable).css("direction")};
this.dom={scroller:null,header:null,body:null,footer:null,grid:{wrapper:null,dt:null,left:{wrapper:null,head:null,body:null,foot:null},right:{wrapper:null,head:null,body:null,foot:null}},clone:{left:{header:null,body:null,footer:null},right:{header:null,body:null,footer:null}}};
if(l._oFixedColumns){throw"FixedColumns already initialised on this table"
}l._oFixedColumns=this;
l._bInitComplete?this._fnConstruct(d):l.oApi._fnCallbackReg(l,"aoInitComplete",function(){m._fnConstruct(d)
},"FixedColumns")
}else{alert("FixedColumns warning: FixedColumns must be initialised with the 'new' keyword.")
}};
h.extend(a.prototype,{fnUpdate:function(){this._fnDraw(!0)
},fnRedrawLayout:function(){this._fnColCalc();
this._fnGridLayout();
this.fnUpdate()
},fnRecalculateHeight:function(d){delete d._DTTC_iHeight;
d.style.height="auto"
},fnSetRowHeight:function(k,d){k.style.height=d+"px"
},fnGetPosition:function(k){var d=this.s.dt.oInstance;
if(h(k).parents(".DTFC_Cloned").length){if("tr"===k.nodeName.toLowerCase()){return k=h(k).index(),d.fnGetPosition(h("tr",this.s.dt.nTBody)[k])
}var l=h(k).index(),k=h(k.parentNode).index();
return[d.fnGetPosition(h("tr",this.s.dt.nTBody)[k]),l,d.oApi._fnVisibleToColumnIndex(this.s.dt,l)]
}return d.fnGetPosition(k)
},_fnConstruct:function(k){var d=this;
if("function"!=typeof this.s.dt.oInstance.fnVersionCheck||!0!==this.s.dt.oInstance.fnVersionCheck("1.8.0")){alert("FixedColumns "+a.VERSION+" required DataTables 1.8.0 or later. Please upgrade your DataTables installation")
}else{if(""===this.s.dt.oScroll.sX){this.s.dt.oInstance.oApi._fnLog(this.s.dt,1,"FixedColumns is not needed (no x-scrolling in DataTables enabled), so no action will be taken. Use 'FixedHeader' for column fixing when scrolling is not enabled")
}else{this.s=h.extend(!0,this.s,a.defaults,k);
k=this.s.dt.oClasses;
this.dom.grid.dt=h(this.s.dt.nTable).parents("div."+k.sScrollWrapper)[0];
this.dom.scroller=h("div."+k.sScrollBody,this.dom.grid.dt)[0];
this._fnColCalc();
this._fnGridSetup();
var p,o=!1;
h(this.s.dt.nTableWrapper).on("mousedown.DTFC",function(){o=!0;
h(f).one("mouseup",function(){o=!1
})
});
h(this.dom.scroller).on("mouseover.DTFC touchstart.DTFC",function(){o||(p="main")
}).on("scroll.DTFC",function(q){!p&&q.originalEvent&&(p="main");
if("main"===p&&(0<d.s.iLeftColumns&&(d.dom.grid.left.liner.scrollTop=d.dom.scroller.scrollTop),0<d.s.iRightColumns)){d.dom.grid.right.liner.scrollTop=d.dom.scroller.scrollTop
}});
var n="onwheel" in f.createElement("div")?"wheel.DTFC":"mousewheel.DTFC";
if(0<d.s.iLeftColumns){h(d.dom.grid.left.liner).on("mouseover.DTFC touchstart.DTFC",function(){o||(p="left")
}).on("scroll.DTFC",function(q){!p&&q.originalEvent&&(p="left");
"left"===p&&(d.dom.scroller.scrollTop=d.dom.grid.left.liner.scrollTop,0<d.s.iRightColumns&&(d.dom.grid.right.liner.scrollTop=d.dom.grid.left.liner.scrollTop))
}).on(n,function(q){d.dom.scroller.scrollLeft-="wheel"===q.type?-q.originalEvent.deltaX:q.originalEvent.wheelDeltaX
})
}if(0<d.s.iRightColumns){h(d.dom.grid.right.liner).on("mouseover.DTFC touchstart.DTFC",function(){o||(p="right")
}).on("scroll.DTFC",function(q){!p&&q.originalEvent&&(p="right");
"right"===p&&(d.dom.scroller.scrollTop=d.dom.grid.right.liner.scrollTop,0<d.s.iLeftColumns&&(d.dom.grid.left.liner.scrollTop=d.dom.grid.right.liner.scrollTop))
}).on(n,function(q){d.dom.scroller.scrollLeft-="wheel"===q.type?-q.originalEvent.deltaX:q.originalEvent.wheelDeltaX
})
}h(g).on("resize.DTFC",function(){d._fnGridLayout.call(d)
});
var m=!0,l=h(this.s.dt.nTable);
l.on("draw.dt.DTFC",function(){d._fnColCalc();
d._fnDraw.call(d,m);
m=!1
}).on("column-sizing.dt.DTFC",function(){d._fnColCalc();
d._fnGridLayout(d)
}).on("column-visibility.dt.DTFC",function(q,u,t,s,r){if(r===c||r){d._fnColCalc(),d._fnGridLayout(d),d._fnDraw(!0)
}}).on("select.dt.DTFC deselect.dt.DTFC",function(q){"dt"===q.namespace&&d._fnDraw(!1)
}).on("destroy.dt.DTFC",function(){l.off(".DTFC");
h(d.dom.scroller).off(".DTFC");
h(g).off(".DTFC");
h(d.s.dt.nTableWrapper).off(".DTFC");
h(d.dom.grid.left.liner).off(".DTFC "+n);
h(d.dom.grid.left.wrapper).remove();
h(d.dom.grid.right.liner).off(".DTFC "+n);
h(d.dom.grid.right.wrapper).remove()
});
this._fnGridLayout();
this.s.dt.oInstance.fnDraw(!1)
}}},_fnColCalc:function(){var k=this,d=0,l=0;
this.s.aiInnerWidths=[];
this.s.aiOuterWidths=[];
h.each(this.s.dt.aoColumns,function(q,p){var o=h(p.nTh),n;
if(o.filter(":visible").length){var m=o.outerWidth();
0===k.s.aiOuterWidths.length&&(n=h(k.s.dt.nTable).css("border-left-width"),m+="string"===typeof n?1:parseInt(n,10));
k.s.aiOuterWidths.length===k.s.dt.aoColumns.length-1&&(n=h(k.s.dt.nTable).css("border-right-width"),m+="string"===typeof n?1:parseInt(n,10));
k.s.aiOuterWidths.push(m);
k.s.aiInnerWidths.push(o.width());
q<k.s.iLeftColumns&&(d+=m);
k.s.iTableColumns-k.s.iRightColumns<=q&&(l+=m)
}else{k.s.aiInnerWidths.push(0),k.s.aiOuterWidths.push(0)
}});
this.s.iLeftWidth=d;
this.s.iRightWidth=l
},_fnGridSetup:function(){var k=this._fnDTOverflow(),d;
this.dom.body=this.s.dt.nTable;
this.dom.header=this.s.dt.nTHead.parentNode;
this.dom.header.parentNode.parentNode.style.position="relative";
var n=h('<div class="DTFC_ScrollWrapper" style="position:relative; clear:both;"><div class="DTFC_LeftWrapper" style="position:absolute; top:0; left:0;"><div class="DTFC_LeftHeadWrapper" style="position:relative; top:0; left:0; overflow:hidden;"></div><div class="DTFC_LeftBodyWrapper" style="position:relative; top:0; left:0; overflow:hidden;"><div class="DTFC_LeftBodyLiner" style="position:relative; top:0; left:0; overflow-y:scroll;"></div></div><div class="DTFC_LeftFootWrapper" style="position:relative; top:0; left:0; overflow:hidden;"></div></div><div class="DTFC_RightWrapper" style="position:absolute; top:0; right:0;"><div class="DTFC_RightHeadWrapper" style="position:relative; top:0; left:0;"><div class="DTFC_RightHeadBlocker DTFC_Blocker" style="position:absolute; top:0; bottom:0;"></div></div><div class="DTFC_RightBodyWrapper" style="position:relative; top:0; left:0; overflow:hidden;"><div class="DTFC_RightBodyLiner" style="position:relative; top:0; left:0; overflow-y:scroll;"></div></div><div class="DTFC_RightFootWrapper" style="position:relative; top:0; left:0;"><div class="DTFC_RightFootBlocker DTFC_Blocker" style="position:absolute; top:0; bottom:0;"></div></div></div></div>')[0],m=n.childNodes[0],l=n.childNodes[1];
this.dom.grid.dt.parentNode.insertBefore(n,this.dom.grid.dt);
n.appendChild(this.dom.grid.dt);
this.dom.grid.wrapper=n;
0<this.s.iLeftColumns&&(this.dom.grid.left.wrapper=m,this.dom.grid.left.head=m.childNodes[0],this.dom.grid.left.body=m.childNodes[1],this.dom.grid.left.liner=h("div.DTFC_LeftBodyLiner",n)[0],n.appendChild(m));
0<this.s.iRightColumns&&(this.dom.grid.right.wrapper=l,this.dom.grid.right.head=l.childNodes[0],this.dom.grid.right.body=l.childNodes[1],this.dom.grid.right.liner=h("div.DTFC_RightBodyLiner",n)[0],l.style.right=k.bar+"px",d=h("div.DTFC_RightHeadBlocker",n)[0],d.style.width=k.bar+"px",d.style.right=-k.bar+"px",this.dom.grid.right.headBlock=d,d=h("div.DTFC_RightFootBlocker",n)[0],d.style.width=k.bar+"px",d.style.right=-k.bar+"px",this.dom.grid.right.footBlock=d,n.appendChild(l));
if(this.s.dt.nTFoot&&(this.dom.footer=this.s.dt.nTFoot.parentNode,0<this.s.iLeftColumns&&(this.dom.grid.left.foot=m.childNodes[2]),0<this.s.iRightColumns)){this.dom.grid.right.foot=l.childNodes[2]
}this.s.rtl&&h("div.DTFC_RightHeadBlocker",n).css({left:-k.bar+"px",right:""})
},_fnGridLayout:function(){var r=this,q=this.dom.grid;
h(q.wrapper).width();
var p=h(this.s.dt.nTable.parentNode).outerHeight(),o=h(this.s.dt.nTable.parentNode.parentNode).outerHeight(),n=this._fnDTOverflow(),m=this.s.iLeftWidth,l=this.s.iRightWidth,k="rtl"===h(this.dom.body).css("direction"),d=function(s,t){n.bar?r._firefoxScrollError()?34<h(s).height()&&(s.style.width=t+n.bar+"px"):s.style.width=t+n.bar+"px":(s.style.width=t+20+"px",s.style.paddingRight="20px",s.style.boxSizing="border-box")
};
n.x&&(p-=n.bar);
q.wrapper.style.height=o+"px";
0<this.s.iLeftColumns&&(o=q.left.wrapper,o.style.width=m+"px",o.style.height="1px",k?(o.style.left="",o.style.right=0):(o.style.left=0,o.style.right=""),q.left.body.style.height=p+"px",q.left.foot&&(q.left.foot.style.top=(n.x?n.bar:0)+"px"),d(q.left.liner,m),q.left.liner.style.height=p+"px");
0<this.s.iRightColumns&&(o=q.right.wrapper,o.style.width=l+"px",o.style.height="1px",this.s.rtl?(o.style.left=n.y?n.bar+"px":0,o.style.right=""):(o.style.left="",o.style.right=n.y?n.bar+"px":0),q.right.body.style.height=p+"px",q.right.foot&&(q.right.foot.style.top=(n.x?n.bar:0)+"px"),d(q.right.liner,l),q.right.liner.style.height=p+"px",q.right.headBlock.style.display=n.y?"block":"none",q.right.footBlock.style.display=n.y?"block":"none")
},_fnDTOverflow:function(){var k=this.s.dt.nTable,d=k.parentNode,l={x:!1,y:!1,bar:this.s.dt.oScroll.iBarWidth};
k.offsetWidth>d.clientWidth&&(l.x=!0);
k.offsetHeight>d.clientHeight&&(l.y=!0);
return l
},_fnDraw:function(d){this._fnGridLayout();
this._fnCloneLeft(d);
this._fnCloneRight(d);
null!==this.s.fnDrawCallback&&this.s.fnDrawCallback.call(this,this.dom.clone.left,this.dom.clone.right);
h(this).trigger("draw.dtfc",{leftClone:this.dom.clone.left,rightClone:this.dom.clone.right})
},_fnCloneRight:function(k){if(!(0>=this.s.iRightColumns)){var d,l=[];
for(d=this.s.iTableColumns-this.s.iRightColumns;
d<this.s.iTableColumns;
d++){this.s.dt.aoColumns[d].bVisible&&l.push(d)
}this._fnClone(this.dom.clone.right,this.dom.grid.right,l,k)
}},_fnCloneLeft:function(k){if(!(0>=this.s.iLeftColumns)){var d,l=[];
for(d=0;
d<this.s.iLeftColumns;
d++){this.s.dt.aoColumns[d].bVisible&&l.push(d)
}this._fnClone(this.dom.clone.left,this.dom.grid.left,l,k)
}},_fnCopyLayout:function(x,w,v){for(var u=[],t=[],s=[],r=0,q=x.length;
r<q;
r++){var n=[];
n.nTr=h(x[r].nTr).clone(v,!1)[0];
for(var m=0,k=this.s.iTableColumns;
m<k;
m++){if(-1!==h.inArray(m,w)){var d=h.inArray(x[r][m].cell,s);
-1===d?(d=h(x[r][m].cell).clone(v,!1)[0],t.push(d),s.push(x[r][m].cell),n.push({cell:d,unique:x[r][m].unique})):n.push({cell:t[d],unique:x[r][m].unique})
}}u.push(n)
}return u
},_fnClone:function(F,E,D,C){var B=this,A,z,y,x,v,s,r,t,u,w=this.s.dt;
if(C){h(F.header).remove();
F.header=h(this.dom.header).clone(!0,!1)[0];
F.header.className+=" DTFC_Cloned";
F.header.style.width="100%";
E.head.appendChild(F.header);
t=this._fnCopyLayout(w.aoHeader,D,!0);
x=h(">thead",F.header);
x.empty();
A=0;
for(z=t.length;
A<z;
A++){x[0].appendChild(t[A].nTr)
}w.oApi._fnDrawHead(w,t,!0)
}else{t=this._fnCopyLayout(w.aoHeader,D,!1);
u=[];
w.oApi._fnDetectHeader(u,h(">thead",F.header)[0]);
A=0;
for(z=t.length;
A<z;
A++){y=0;
for(x=t[A].length;
y<x;
y++){u[A][y].cell.className=t[A][y].cell.className,h("span.DataTables_sort_icon",u[A][y].cell).each(function(){this.className=h("span.DataTables_sort_icon",t[A][y].cell)[0].className
})
}}}this._fnEqualiseHeights("thead",this.dom.header,F.header);
"auto"==this.s.sHeightMatch&&h(">tbody>tr",B.dom.body).css("height","auto");
null!==F.body&&(h(F.body).remove(),F.body=null);
F.body=h(this.dom.body).clone(!0)[0];
F.body.className+=" DTFC_Cloned";
F.body.style.paddingBottom=w.oScroll.iBarWidth+"px";
F.body.style.marginBottom=2*w.oScroll.iBarWidth+"px";
null!==F.body.getAttribute("id")&&F.body.removeAttribute("id");
h(">thead>tr",F.body).empty();
h(">tfoot",F.body).remove();
var d=h("tbody",F.body)[0];
h(d).empty();
if(0<w.aiDisplay.length){z=h(">thead>tr",F.body)[0];
for(r=0;
r<D.length;
r++){v=D[r],s=h(w.aoColumns[v].nTh).clone(!0)[0],s.innerHTML="",x=s.style,x.paddingTop="0",x.paddingBottom="0",x.borderTopWidth="0",x.borderBottomWidth="0",x.height=0,x.width=B.s.aiInnerWidths[v]+"px",z.appendChild(s)
}h(">tbody>tr",B.dom.body).each(function(l){var l=B.s.dt.oFeatures.bServerSide===false?B.s.dt.aiDisplay[B.s.dt._iDisplayStart+l]:l,k=B.s.dt.aoData[l].anCells||h(this).children("td, th"),m=this.cloneNode(false);
m.removeAttribute("id");
m.setAttribute("data-dt-row",l);
for(r=0;
r<D.length;
r++){v=D[r];
if(k.length>0){s=h(k[v]).clone(true,true)[0];
s.setAttribute("data-dt-row",l);
s.setAttribute("data-dt-column",r);
m.appendChild(s)
}}d.appendChild(m)
})
}else{h(">tbody>tr",B.dom.body).each(function(){s=this.cloneNode(true);
s.className=s.className+" DTFC_NoData";
h("td",s).html("");
d.appendChild(s)
})
}F.body.style.width="100%";
F.body.style.margin="0";
F.body.style.padding="0";
w.oScroller!==c&&(z=w.oScroller.dom.force,E.forcer?E.forcer.style.height=z.style.height:(E.forcer=z.cloneNode(!0),E.liner.appendChild(E.forcer)));
E.liner.appendChild(F.body);
this._fnEqualiseHeights("tbody",B.dom.body,F.body);
if(null!==w.nTFoot){if(C){null!==F.footer&&F.footer.parentNode.removeChild(F.footer);
F.footer=h(this.dom.footer).clone(!0,!0)[0];
F.footer.className+=" DTFC_Cloned";
F.footer.style.width="100%";
E.foot.appendChild(F.footer);
t=this._fnCopyLayout(w.aoFooter,D,!0);
E=h(">tfoot",F.footer);
E.empty();
A=0;
for(z=t.length;
A<z;
A++){E[0].appendChild(t[A].nTr)
}w.oApi._fnDrawHead(w,t,!0)
}else{t=this._fnCopyLayout(w.aoFooter,D,!1);
E=[];
w.oApi._fnDetectHeader(E,h(">tfoot",F.footer)[0]);
A=0;
for(z=t.length;
A<z;
A++){y=0;
for(x=t[A].length;
y<x;
y++){E[A][y].cell.className=t[A][y].cell.className
}}}this._fnEqualiseHeights("tfoot",this.dom.footer,F.footer)
}E=w.oApi._fnGetUniqueThs(w,h(">thead",F.header)[0]);
h(E).each(function(k){v=D[k];
this.style.width=B.s.aiInnerWidths[v]+"px"
});
null!==B.s.dt.nTFoot&&(E=w.oApi._fnGetUniqueThs(w,h(">tfoot",F.footer)[0]),h(E).each(function(k){v=D[k];
this.style.width=B.s.aiInnerWidths[v]+"px"
}))
},_fnGetTrNodes:function(l){for(var k=[],n=0,m=l.childNodes.length;
n<m;
n++){"TR"==l.childNodes[n].nodeName.toUpperCase()&&k.push(l.childNodes[n])
}return k
},_fnEqualiseHeights:function(k,d,p){if(!("none"==this.s.sHeightMatch&&"thead"!==k&&"tfoot"!==k)){var o,n,m=d.getElementsByTagName(k)[0],p=p.getElementsByTagName(k)[0],k=h(">"+k+">tr:eq(0)",d).children(":first");
k.outerHeight();
k.height();
for(var m=this._fnGetTrNodes(m),d=this._fnGetTrNodes(p),l=[],p=0,k=d.length;
p<k;
p++){o=m[p].offsetHeight,n=d[p].offsetHeight,o=n>o?n:o,"semiauto"==this.s.sHeightMatch&&(m[p]._DTTC_iHeight=o),l.push(o)
}p=0;
for(k=d.length;
p<k;
p++){d[p].style.height=l[p]+"px",m[p].style.height=l[p]+"px"
}}},_firefoxScrollError:function(){if(b===c){var d=h("<div/>").css({position:"absolute",top:0,left:0,height:10,width:50,overflow:"scroll"}).appendTo("body");
b=d[0].clientWidth===d[0].offsetWidth&&0!==this._fnDTOverflow().bar;
d.remove()
}return b
}});
a.defaults={iLeftColumns:1,iRightColumns:0,fnDrawCallback:null,sHeightMatch:"semiauto"};
a.version="3.2.2";
e.Api.register("fixedColumns()",function(){return this
});
e.Api.register("fixedColumns().update()",function(){return this.iterator("table",function(d){d._oFixedColumns&&d._oFixedColumns.fnUpdate()
})
});
e.Api.register("fixedColumns().relayout()",function(){return this.iterator("table",function(d){d._oFixedColumns&&d._oFixedColumns.fnRedrawLayout()
})
});
e.Api.register("rows().recalcHeight()",function(){return this.iterator("row",function(k,d){k._oFixedColumns&&k._oFixedColumns.fnRecalculateHeight(this.row(d).node())
})
});
e.Api.register("fixedColumns().rowIndex()",function(d){d=h(d);
return d.parents(".DTFC_Cloned").length?this.rows({page:"current"}).indexes()[d.index()]:this.row(d).index()
});
e.Api.register("fixedColumns().cellIndex()",function(k){k=h(k);
if(k.parents(".DTFC_Cloned").length){var d=k.parent().index(),d=this.rows({page:"current"}).indexes()[d],k=k.parents(".DTFC_LeftWrapper").length?k.index():this.columns().flatten().length-this.context[0]._oFixedColumns.s.iRightColumns+k.index();
return{row:d,column:this.column.index("toData",k),columnVisible:k}
}return this.cell(k).index()
});
h(f).on("init.dt.fixedColumns",function(k,d){if("dt"===k.namespace){var m=d.oInit.fixedColumns,l=e.defaults.fixedColumns;
if(m||l){l=h.extend({},m,l),!1!==m&&new a(d,l)
}}});
h.fn.dataTable.FixedColumns=a;
return h.fn.DataTable.FixedColumns=a
});
!function(b){"function"==typeof define&&define.amd?define(["jquery"],b):"undefined"!=typeof exports?module.exports=b(require("jquery")):b(jQuery)
}(function(d){var c=window.Slick||{};
c=function(){function e(k,h){var b,g=this;
g.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:d(k),appendDots:d(k),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3000,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(l,f){return'<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">'+(f+1)+"</button>"
},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:0.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!1,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1000},g.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},d.extend(g,g.initials),g.activeBreakpoint=null,g.animType=null,g.animProp=null,g.breakpoints=[],g.breakpointSettings=[],g.cssTransitions=!1,g.hidden="hidden",g.paused=!1,g.positionProp=null,g.respondTo=null,g.rowCount=1,g.shouldClick=!0,g.$slider=d(k),g.$slidesCache=null,g.transformType=null,g.transitionType=null,g.visibilityChange="visibilitychange",g.windowWidth=0,g.windowTimer=null,b=d(k).data("slick")||{},g.options=d.extend({},g.defaults,b,h),g.currentSlide=g.options.initialSlide,g.originalSettings=g.options,"undefined"!=typeof document.mozHidden?(g.hidden="mozHidden",g.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(g.hidden="webkitHidden",g.visibilityChange="webkitvisibilitychange"),g.autoPlay=d.proxy(g.autoPlay,g),g.autoPlayClear=d.proxy(g.autoPlayClear,g),g.changeSlide=d.proxy(g.changeSlide,g),g.clickHandler=d.proxy(g.clickHandler,g),g.selectHandler=d.proxy(g.selectHandler,g),g.setPosition=d.proxy(g.setPosition,g),g.swipeHandler=d.proxy(g.swipeHandler,g),g.dragHandler=d.proxy(g.dragHandler,g),g.keyHandler=d.proxy(g.keyHandler,g),g.autoPlayIterator=d.proxy(g.autoPlayIterator,g),g.instanceUid=a++,g.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,g.registerBreakpoints(),g.init(!0),g.checkResponsive(!0)
}var a=0;
return e
}(),c.prototype.addSlide=c.prototype.slickAdd=function(a,h,g){var f=this;
if("boolean"==typeof h){g=h,h=null
}else{if(0>h||h>=f.slideCount){return !1
}}f.unload(),"number"==typeof h?0===h&&0===f.$slides.length?d(a).appendTo(f.$slideTrack):g?d(a).insertBefore(f.$slides.eq(h)):d(a).insertAfter(f.$slides.eq(h)):g===!0?d(a).prependTo(f.$slideTrack):d(a).appendTo(f.$slideTrack),f.$slides=f.$slideTrack.children(this.options.slide),f.$slideTrack.children(this.options.slide).detach(),f.$slideTrack.append(f.$slides),f.$slides.each(function(e,k){d(k).attr("data-slick-index",e)
}),f.$slidesCache=f.$slides,f.reinit()
},c.prototype.animateHeight=function(){var f=this;
if(1===f.options.slidesToShow&&f.options.adaptiveHeight===!0&&f.options.vertical===!1){var e=f.$slides.eq(f.currentSlide).outerHeight(!0);
f.$list.animate({height:e},f.options.speed)
}},c.prototype.animateSlide=function(a,h){var g={},f=this;
f.animateHeight(),f.options.rtl===!0&&f.options.vertical===!1&&(a=-a),f.transformsEnabled===!1?f.options.vertical===!1?f.$slideTrack.animate({left:a},f.options.speed,f.options.easing,h):f.$slideTrack.animate({top:a},f.options.speed,f.options.easing,h):f.cssTransitions===!1?(f.options.rtl===!0&&(f.currentLeft=-f.currentLeft),d({animStart:f.currentLeft}).animate({animStart:a},{duration:f.options.speed,easing:f.options.easing,step:function(b){b=Math.ceil(b),f.options.vertical===!1?(g[f.animType]="translate("+b+"px, 0px)",f.$slideTrack.css(g)):(g[f.animType]="translate(0px,"+b+"px)",f.$slideTrack.css(g))
},complete:function(){h&&h.call()
}})):(f.applyTransition(),a=Math.ceil(a),f.options.vertical===!1?g[f.animType]="translate3d("+a+"px, 0px, 0px)":g[f.animType]="translate3d(0px,"+a+"px, 0px)",f.$slideTrack.css(g),h&&setTimeout(function(){f.disableTransition(),h.call()
},f.options.speed))
},c.prototype.asNavFor=function(a){var f=this,e=f.options.asNavFor;
e&&null!==e&&(e=d(e).not(f.$slider)),null!==e&&"object"==typeof e&&e.each(function(){var b=d(this).slick("getSlick");
b.unslicked||b.slideHandler(a,!0)
})
},c.prototype.applyTransition=function(f){var e=this,g={};
e.options.fade===!1?g[e.transitionType]=e.transformType+" "+e.options.speed+"ms "+e.options.cssEase:g[e.transitionType]="opacity "+e.options.speed+"ms "+e.options.cssEase,e.options.fade===!1?e.$slideTrack.css(g):e.$slides.eq(f).css(g)
},c.prototype.autoPlay=function(){var b=this;
b.autoPlayTimer&&clearInterval(b.autoPlayTimer),b.slideCount>b.options.slidesToShow&&b.paused!==!0&&(b.autoPlayTimer=setInterval(b.autoPlayIterator,b.options.autoplaySpeed))
},c.prototype.autoPlayClear=function(){var b=this;
b.autoPlayTimer&&clearInterval(b.autoPlayTimer)
},c.prototype.autoPlayIterator=function(){var b=this;
b.options.infinite===!1?1===b.direction?(b.currentSlide+1===b.slideCount-1&&(b.direction=0),b.slideHandler(b.currentSlide+b.options.slidesToScroll)):(b.currentSlide-1===0&&(b.direction=1),b.slideHandler(b.currentSlide-b.options.slidesToScroll)):b.slideHandler(b.currentSlide+b.options.slidesToScroll)
},c.prototype.buildArrows=function(){var a=this;
a.options.arrows===!0&&(a.$prevArrow=d(a.options.prevArrow).addClass("slick-arrow"),a.$nextArrow=d(a.options.nextArrow).addClass("slick-arrow"),a.slideCount>a.options.slidesToShow?(a.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),a.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),a.htmlExpr.test(a.options.prevArrow)&&a.$prevArrow.prependTo(a.options.appendArrows),a.htmlExpr.test(a.options.nextArrow)&&a.$nextArrow.appendTo(a.options.appendArrows),a.options.infinite!==!0&&a.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):a.$prevArrow.add(a.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))
},c.prototype.buildDots=function(){var f,e,a=this;
if(a.options.dots===!0&&a.slideCount>a.options.slidesToShow){for(e='<ul class="'+a.options.dotsClass+'">',f=0;
f<=a.getDotCount();
f+=1){e+="<li>"+a.options.customPaging.call(this,a,f)+"</li>"
}e+="</ul>",a.$dots=d(e).appendTo(a.options.appendDots),a.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")
}},c.prototype.buildOut=function(){var a=this;
a.$slides=a.$slider.children(a.options.slide+":not(.slick-cloned)").addClass("slick-slide"),a.slideCount=a.$slides.length,a.$slides.each(function(e,f){d(f).attr("data-slick-index",e).data("originalStyling",d(f).attr("style")||"")
}),a.$slider.addClass("slick-slider"),a.$slideTrack=0===a.slideCount?d('<div class="slick-track"/>').appendTo(a.$slider):a.$slides.wrapAll('<div class="slick-track"/>').parent(),a.$list=a.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),a.$slideTrack.css("opacity",0),(a.options.centerMode===!0||a.options.swipeToSlide===!0)&&(a.options.slidesToScroll=1),d("img[data-lazy]",a.$slider).not("[src]").addClass("slick-loading"),a.setupInfinite(),a.buildArrows(),a.buildDots(),a.updateDots(),a.setSlideClasses("number"==typeof a.currentSlide?a.currentSlide:0),a.options.draggable===!0&&a.$list.addClass("draggable")
},c.prototype.buildRows=function(){var u,t,s,r,q,p,o,v=this;
if(r=document.createDocumentFragment(),p=v.$slider.children(),v.options.rows>1){for(o=v.options.slidesPerRow*v.options.rows,q=Math.ceil(p.length/o),u=0;
q>u;
u++){var n=document.createElement("div");
for(t=0;
t<v.options.rows;
t++){var m=document.createElement("div");
for(s=0;
s<v.options.slidesPerRow;
s++){var l=u*o+(t*v.options.slidesPerRow+s);
p.get(l)&&m.appendChild(p.get(l))
}n.appendChild(m)
}r.appendChild(n)
}v.$slider.html(r),v.$slider.children().children().children().css({width:100/v.options.slidesPerRow+"%",display:"inline-block"})
}},c.prototype.checkResponsive=function(r,q){var o,n,m,p=this,l=!1,k=p.$slider.width(),a=window.innerWidth||d(window).width();
if("window"===p.respondTo?m=a:"slider"===p.respondTo?m=k:"min"===p.respondTo&&(m=Math.min(a,k)),p.options.responsive&&p.options.responsive.length&&null!==p.options.responsive){n=null;
for(o in p.breakpoints){p.breakpoints.hasOwnProperty(o)&&(p.originalSettings.mobileFirst===!1?m<p.breakpoints[o]&&(n=p.breakpoints[o]):m>p.breakpoints[o]&&(n=p.breakpoints[o]))
}null!==n?null!==p.activeBreakpoint?(n!==p.activeBreakpoint||q)&&(p.activeBreakpoint=n,"unslick"===p.breakpointSettings[n]?p.unslick(n):(p.options=d.extend({},p.originalSettings,p.breakpointSettings[n]),r===!0&&(p.currentSlide=p.options.initialSlide),p.refresh(r)),l=n):(p.activeBreakpoint=n,"unslick"===p.breakpointSettings[n]?p.unslick(n):(p.options=d.extend({},p.originalSettings,p.breakpointSettings[n]),r===!0&&(p.currentSlide=p.options.initialSlide),p.refresh(r)),l=n):null!==p.activeBreakpoint&&(p.activeBreakpoint=null,p.options=p.originalSettings,r===!0&&(p.currentSlide=p.options.initialSlide),p.refresh(r),l=n),r||l===!1||p.$slider.trigger("breakpoint",[p,l])
}},c.prototype.changeSlide=function(a,q){var n,m,l,p=this,o=d(a.target);
switch(o.is("a")&&a.preventDefault(),o.is("li")||(o=o.closest("li")),l=p.slideCount%p.options.slidesToScroll!==0,n=l?0:(p.slideCount-p.currentSlide)%p.options.slidesToScroll,a.data.message){case"previous":m=0===n?p.options.slidesToScroll:p.options.slidesToShow-n,p.slideCount>p.options.slidesToShow&&p.slideHandler(p.currentSlide-m,!1,q);
break;
case"next":m=0===n?p.options.slidesToScroll:n,p.slideCount>p.options.slidesToShow&&p.slideHandler(p.currentSlide+m,!1,q);
break;
case"index":var k=0===a.data.index?0:a.data.index||o.index()*p.options.slidesToScroll;
p.slideHandler(p.checkNavigable(k),!1,q),o.children().trigger("focus");
break;
default:return
}},c.prototype.checkNavigable=function(g){var l,k,f=this;
if(l=f.getNavigableIndexes(),k=0,g>l[l.length-1]){g=l[l.length-1]
}else{for(var h in l){if(g<l[h]){g=k;
break
}k=l[h]
}}return g
},c.prototype.cleanUpEvents=function(){var a=this;
a.options.dots&&null!==a.$dots&&(d("li",a.$dots).off("click.slick",a.changeSlide),a.options.pauseOnDotsHover===!0&&a.options.autoplay===!0&&d("li",a.$dots).off("mouseenter.slick",d.proxy(a.setPaused,a,!0)).off("mouseleave.slick",d.proxy(a.setPaused,a,!1))),a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow&&a.$prevArrow.off("click.slick",a.changeSlide),a.$nextArrow&&a.$nextArrow.off("click.slick",a.changeSlide)),a.$list.off("touchstart.slick mousedown.slick",a.swipeHandler),a.$list.off("touchmove.slick mousemove.slick",a.swipeHandler),a.$list.off("touchend.slick mouseup.slick",a.swipeHandler),a.$list.off("touchcancel.slick mouseleave.slick",a.swipeHandler),a.$list.off("click.slick",a.clickHandler),d(document).off(a.visibilityChange,a.visibility),a.$list.off("mouseenter.slick",d.proxy(a.setPaused,a,!0)),a.$list.off("mouseleave.slick",d.proxy(a.setPaused,a,!1)),a.options.accessibility===!0&&a.$list.off("keydown.slick",a.keyHandler),a.options.focusOnSelect===!0&&d(a.$slideTrack).children().off("click.slick",a.selectHandler),d(window).off("orientationchange.slick.slick-"+a.instanceUid,a.orientationChange),d(window).off("resize.slick.slick-"+a.instanceUid,a.resize),d("[draggable!=true]",a.$slideTrack).off("dragstart",a.preventDefault),d(window).off("load.slick.slick-"+a.instanceUid,a.setPosition),d(document).off("ready.slick.slick-"+a.instanceUid,a.setPosition)
},c.prototype.cleanUpRows=function(){var e,f=this;
f.options.rows>1&&(e=f.$slides.children().children(),e.removeAttr("style"),f.$slider.html(e))
},c.prototype.clickHandler=function(f){var e=this;
e.shouldClick===!1&&(f.stopImmediatePropagation(),f.stopPropagation(),f.preventDefault())
},c.prototype.destroy=function(a){var e=this;
e.autoPlayClear(),e.touchObject={},e.cleanUpEvents(),d(".slick-cloned",e.$slider).detach(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.$prevArrow.length&&(e.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove()),e.$nextArrow&&e.$nextArrow.length&&(e.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove()),e.$slides&&(e.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){d(this).attr("style",d(this).data("originalStyling"))
}),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.detach(),e.$list.detach(),e.$slider.append(e.$slides)),e.cleanUpRows(),e.$slider.removeClass("slick-slider"),e.$slider.removeClass("slick-initialized"),e.unslicked=!0,a||e.$slider.trigger("destroy",[e])
},c.prototype.disableTransition=function(f){var e=this,g={};
g[e.transitionType]="",e.options.fade===!1?e.$slideTrack.css(g):e.$slides.eq(f).css(g)
},c.prototype.fadeSlide=function(f,e){var g=this;
g.cssTransitions===!1?(g.$slides.eq(f).css({zIndex:g.options.zIndex}),g.$slides.eq(f).animate({opacity:1},g.options.speed,g.options.easing,e)):(g.applyTransition(f),g.$slides.eq(f).css({opacity:1,zIndex:g.options.zIndex}),e&&setTimeout(function(){g.disableTransition(f),e.call()
},g.options.speed))
},c.prototype.fadeSlideOut=function(f){var e=this;
e.cssTransitions===!1?e.$slides.eq(f).animate({opacity:0,zIndex:e.options.zIndex-2},e.options.speed,e.options.easing):(e.applyTransition(f),e.$slides.eq(f).css({opacity:0,zIndex:e.options.zIndex-2}))
},c.prototype.filterSlides=c.prototype.slickFilter=function(f){var e=this;
null!==f&&(e.$slidesCache=e.$slides,e.unload(),e.$slideTrack.children(this.options.slide).detach(),e.$slidesCache.filter(f).appendTo(e.$slideTrack),e.reinit())
},c.prototype.getCurrent=c.prototype.slickCurrentSlide=function(){var b=this;
return b.currentSlide
},c.prototype.getDotCount=function(){var f=this,e=0,h=0,g=0;
if(f.options.infinite===!0){for(;
e<f.slideCount;
){++g,e=h+f.options.slidesToScroll,h+=f.options.slidesToScroll<=f.options.slidesToShow?f.options.slidesToScroll:f.options.slidesToShow
}}else{if(f.options.centerMode===!0){g=f.slideCount
}else{for(;
e<f.slideCount;
){++g,e=h+f.options.slidesToScroll,h+=f.options.slidesToScroll<=f.options.slidesToShow?f.options.slidesToScroll:f.options.slidesToShow
}}}return g-1
},c.prototype.getLeft=function(h){var n,m,k,g=this,l=0;
return g.slideOffset=0,m=g.$slides.first().outerHeight(!0),g.options.infinite===!0?(g.slideCount>g.options.slidesToShow&&(g.slideOffset=g.slideWidth*g.options.slidesToShow*-1,l=m*g.options.slidesToShow*-1),g.slideCount%g.options.slidesToScroll!==0&&h+g.options.slidesToScroll>g.slideCount&&g.slideCount>g.options.slidesToShow&&(h>g.slideCount?(g.slideOffset=(g.options.slidesToShow-(h-g.slideCount))*g.slideWidth*-1,l=(g.options.slidesToShow-(h-g.slideCount))*m*-1):(g.slideOffset=g.slideCount%g.options.slidesToScroll*g.slideWidth*-1,l=g.slideCount%g.options.slidesToScroll*m*-1))):h+g.options.slidesToShow>g.slideCount&&(g.slideOffset=(h+g.options.slidesToShow-g.slideCount)*g.slideWidth,l=(h+g.options.slidesToShow-g.slideCount)*m),g.slideCount<=g.options.slidesToShow&&(g.slideOffset=0,l=0),g.options.centerMode===!0&&g.options.infinite===!0?g.slideOffset+=g.slideWidth*Math.floor(g.options.slidesToShow/2)-g.slideWidth:g.options.centerMode===!0&&(g.slideOffset=0,g.slideOffset+=g.slideWidth*Math.floor(g.options.slidesToShow/2)),n=g.options.vertical===!1?h*g.slideWidth*-1+g.slideOffset:h*m*-1+l,g.options.variableWidth===!0&&(k=g.slideCount<=g.options.slidesToShow||g.options.infinite===!1?g.$slideTrack.children(".slick-slide").eq(h):g.$slideTrack.children(".slick-slide").eq(h+g.options.slidesToShow),n=g.options.rtl===!0?k[0]?-1*(g.$slideTrack.width()-k[0].offsetLeft-k.width()):0:k[0]?-1*k[0].offsetLeft:0,g.options.centerMode===!0&&(k=g.slideCount<=g.options.slidesToShow||g.options.infinite===!1?g.$slideTrack.children(".slick-slide").eq(h):g.$slideTrack.children(".slick-slide").eq(h+g.options.slidesToShow+1),n=g.options.rtl===!0?k[0]?-1*(g.$slideTrack.width()-k[0].offsetLeft-k.width()):0:k[0]?-1*k[0].offsetLeft:0,n+=(g.$list.width()-k.outerWidth())/2)),n
},c.prototype.getOption=c.prototype.slickGetOption=function(f){var e=this;
return e.options[f]
},c.prototype.getNavigableIndexes=function(){var h,g=this,f=0,l=0,k=[];
for(g.options.infinite===!1?h=g.slideCount:(f=-1*g.options.slidesToScroll,l=-1*g.options.slidesToScroll,h=2*g.slideCount);
h>f;
){k.push(f),f=l+g.options.slidesToScroll,l+=g.options.slidesToScroll<=g.options.slidesToShow?g.options.slidesToScroll:g.options.slidesToShow
}return k
},c.prototype.getSlick=function(){return this
},c.prototype.getSlideCount=function(){var h,g,f,a=this;
return f=a.options.centerMode===!0?a.slideWidth*Math.floor(a.options.slidesToShow/2):0,a.options.swipeToSlide===!0?(a.$slideTrack.find(".slick-slide").each(function(e,b){return b.offsetLeft-f+d(b).outerWidth()/2>-1*a.swipeLeft?(g=b,!1):void 0
}),h=Math.abs(d(g).attr("data-slick-index")-a.currentSlide)||1):a.options.slidesToScroll
},c.prototype.goTo=c.prototype.slickGoTo=function(f,e){var g=this;
g.changeSlide({data:{message:"index",index:parseInt(f)}},e)
},c.prototype.init=function(a){var e=this;
d(e.$slider).hasClass("slick-initialized")||(d(e.$slider).addClass("slick-initialized"),e.buildRows(),e.buildOut(),e.setProps(),e.startLoad(),e.loadSlider(),e.initializeEvents(),e.updateArrows(),e.updateDots()),a&&e.$slider.trigger("init",[e]),e.options.accessibility===!0&&e.initADA()
},c.prototype.initArrowEvents=function(){var b=this;
b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow.on("click.slick",{message:"previous"},b.changeSlide),b.$nextArrow.on("click.slick",{message:"next"},b.changeSlide))
},c.prototype.initDotEvents=function(){var a=this;
a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&d("li",a.$dots).on("click.slick",{message:"index"},a.changeSlide),a.options.dots===!0&&a.options.pauseOnDotsHover===!0&&a.options.autoplay===!0&&d("li",a.$dots).on("mouseenter.slick",d.proxy(a.setPaused,a,!0)).on("mouseleave.slick",d.proxy(a.setPaused,a,!1))
},c.prototype.initializeEvents=function(){var a=this;
a.initArrowEvents(),a.initDotEvents(),a.$list.on("touchstart.slick mousedown.slick",{action:"start"},a.swipeHandler),a.$list.on("touchmove.slick mousemove.slick",{action:"move"},a.swipeHandler),a.$list.on("touchend.slick mouseup.slick",{action:"end"},a.swipeHandler),a.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},a.swipeHandler),a.$list.on("click.slick",a.clickHandler),d(document).on(a.visibilityChange,d.proxy(a.visibility,a)),a.$list.on("mouseenter.slick",d.proxy(a.setPaused,a,!0)),a.$list.on("mouseleave.slick",d.proxy(a.setPaused,a,!1)),a.options.accessibility===!0&&a.$list.on("keydown.slick",a.keyHandler),a.options.focusOnSelect===!0&&d(a.$slideTrack).children().on("click.slick",a.selectHandler),d(window).on("orientationchange.slick.slick-"+a.instanceUid,d.proxy(a.orientationChange,a)),d(window).on("resize.slick.slick-"+a.instanceUid,d.proxy(a.resize,a)),d("[draggable!=true]",a.$slideTrack).on("dragstart",a.preventDefault),d(window).on("load.slick.slick-"+a.instanceUid,a.setPosition),d(document).on("ready.slick.slick-"+a.instanceUid,a.setPosition)
},c.prototype.initUI=function(){var b=this;
b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow.show(),b.$nextArrow.show()),b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&b.$dots.show(),b.options.autoplay===!0&&b.autoPlay()
},c.prototype.keyHandler=function(f){var e=this;
f.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===f.keyCode&&e.options.accessibility===!0?e.changeSlide({data:{message:"previous"}}):39===f.keyCode&&e.options.accessibility===!0&&e.changeSlide({data:{message:"next"}}))
},c.prototype.lazyLoad=function(){function h(e){d("img[data-lazy]",e).each(function(){var f=d(this),o=d(this).attr("data-lazy"),g=document.createElement("img");
g.onload=function(){f.animate({opacity:0},100,function(){f.attr("src",o).animate({opacity:1},200,function(){f.removeAttr("data-lazy").removeClass("slick-loading")
})
})
},g.src=o
})
}var n,m,l,k,a=this;
a.options.centerMode===!0?a.options.infinite===!0?(l=a.currentSlide+(a.options.slidesToShow/2+1),k=l+a.options.slidesToShow+2):(l=Math.max(0,a.currentSlide-(a.options.slidesToShow/2+1)),k=2+(a.options.slidesToShow/2+1)+a.currentSlide):(l=a.options.infinite?a.options.slidesToShow+a.currentSlide:a.currentSlide,k=l+a.options.slidesToShow,a.options.fade===!0&&(l>0&&l--,k<=a.slideCount&&k++)),n=a.$slider.find(".slick-slide").slice(l,k),h(n),a.slideCount<=a.options.slidesToShow?(m=a.$slider.find(".slick-slide"),h(m)):a.currentSlide>=a.slideCount-a.options.slidesToShow?(m=a.$slider.find(".slick-cloned").slice(0,a.options.slidesToShow),h(m)):0===a.currentSlide&&(m=a.$slider.find(".slick-cloned").slice(-1*a.options.slidesToShow),h(m))
},c.prototype.loadSlider=function(){var b=this;
b.setPosition(),b.$slideTrack.css({opacity:1}),b.$slider.removeClass("slick-loading"),b.initUI(),"progressive"===b.options.lazyLoad&&b.progressiveLazyLoad()
},c.prototype.next=c.prototype.slickNext=function(){var b=this;
b.changeSlide({data:{message:"next"}})
},c.prototype.orientationChange=function(){var b=this;
b.checkResponsive(),b.setPosition()
},c.prototype.pause=c.prototype.slickPause=function(){var b=this;
b.autoPlayClear(),b.paused=!0
},c.prototype.play=c.prototype.slickPlay=function(){var b=this;
b.paused=!1,b.autoPlay()
},c.prototype.postSlide=function(f){var e=this;
e.$slider.trigger("afterChange",[e,f]),e.animating=!1,e.setPosition(),e.swipeLeft=null,e.options.autoplay===!0&&e.paused===!1&&e.autoPlay(),e.options.accessibility===!0&&e.initADA()
},c.prototype.prev=c.prototype.slickPrev=function(){var b=this;
b.changeSlide({data:{message:"previous"}})
},c.prototype.preventDefault=function(b){b.preventDefault()
},c.prototype.progressiveLazyLoad=function(){var f,e,a=this;
f=d("img[data-lazy]",a.$slider).length,f>0&&(e=d("img[data-lazy]",a.$slider).first(),e.attr("src",null),e.attr("src",e.attr("data-lazy")).removeClass("slick-loading").load(function(){e.removeAttr("data-lazy"),a.progressiveLazyLoad(),a.options.adaptiveHeight===!0&&a.setPosition()
}).error(function(){e.removeAttr("data-lazy"),a.progressiveLazyLoad()
}))
},c.prototype.refresh=function(a){var g,f,h=this;
f=h.slideCount-h.options.slidesToShow,h.options.infinite||(h.slideCount<=h.options.slidesToShow?h.currentSlide=0:h.currentSlide>f&&(h.currentSlide=f)),g=h.currentSlide,h.destroy(!0),d.extend(h,h.initials,{currentSlide:g}),h.init(),a||h.changeSlide({data:{message:"index",index:g}},!1)
},c.prototype.registerBreakpoints=function(){var l,k,h,a=this,g=a.options.responsive||null;
if("array"===d.type(g)&&g.length){a.respondTo=a.options.respondTo||"window";
for(l in g){if(h=a.breakpoints.length-1,k=g[l].breakpoint,g.hasOwnProperty(l)){for(;
h>=0;
){a.breakpoints[h]&&a.breakpoints[h]===k&&a.breakpoints.splice(h,1),h--
}a.breakpoints.push(k),a.breakpointSettings[k]=g[l].settings
}}a.breakpoints.sort(function(b,e){return a.options.mobileFirst?b-e:e-b
})
}},c.prototype.reinit=function(){var a=this;
a.$slides=a.$slideTrack.children(a.options.slide).addClass("slick-slide"),a.slideCount=a.$slides.length,a.currentSlide>=a.slideCount&&0!==a.currentSlide&&(a.currentSlide=a.currentSlide-a.options.slidesToScroll),a.slideCount<=a.options.slidesToShow&&(a.currentSlide=0),a.registerBreakpoints(),a.setProps(),a.setupInfinite(),a.buildArrows(),a.updateArrows(),a.initArrowEvents(),a.buildDots(),a.updateDots(),a.initDotEvents(),a.checkResponsive(!1,!0),a.options.focusOnSelect===!0&&d(a.$slideTrack).children().on("click.slick",a.selectHandler),a.setSlideClasses(0),a.setPosition(),a.$slider.trigger("reInit",[a]),a.options.autoplay===!0&&a.focusHandler()
},c.prototype.resize=function(){var a=this;
d(window).width()!==a.windowWidth&&(clearTimeout(a.windowDelay),a.windowDelay=window.setTimeout(function(){a.windowWidth=d(window).width(),a.checkResponsive(),a.unslicked||a.setPosition()
},50))
},c.prototype.removeSlide=c.prototype.slickRemove=function(f,e,h){var g=this;
return"boolean"==typeof f?(e=f,f=e===!0?0:g.slideCount-1):f=e===!0?--f:f,g.slideCount<1||0>f||f>g.slideCount-1?!1:(g.unload(),h===!0?g.$slideTrack.children().remove():g.$slideTrack.children(this.options.slide).eq(f).remove(),g.$slides=g.$slideTrack.children(this.options.slide),g.$slideTrack.children(this.options.slide).detach(),g.$slideTrack.append(g.$slides),g.$slidesCache=g.$slides,void g.reinit())
},c.prototype.setCSS=function(g){var k,h,f=this,l={};
f.options.rtl===!0&&(g=-g),k="left"==f.positionProp?Math.ceil(g)+"px":"0px",h="top"==f.positionProp?Math.ceil(g)+"px":"0px",l[f.positionProp]=g,f.transformsEnabled===!1?f.$slideTrack.css(l):(l={},f.cssTransitions===!1?(l[f.animType]="translate("+k+", "+h+")",f.$slideTrack.css(l)):(l[f.animType]="translate3d("+k+", "+h+", 0px)",f.$slideTrack.css(l)))
},c.prototype.setDimensions=function(){var f=this;
f.options.vertical===!1?f.options.centerMode===!0&&f.$list.css({padding:"0px "+f.options.centerPadding}):(f.$list.height(f.$slides.first().outerHeight(!0)*f.options.slidesToShow),f.options.centerMode===!0&&f.$list.css({padding:f.options.centerPadding+" 0px"})),f.listWidth=f.$list.width(),f.listHeight=f.$list.height(),f.options.vertical===!1&&f.options.variableWidth===!1?(f.slideWidth=Math.ceil(f.listWidth/f.options.slidesToShow),f.$slideTrack.width(Math.ceil(f.slideWidth*f.$slideTrack.children(".slick-slide").length))):f.options.variableWidth===!0?f.$slideTrack.width(5000*f.slideCount):(f.slideWidth=Math.ceil(f.listWidth),f.$slideTrack.height(Math.ceil(f.$slides.first().outerHeight(!0)*f.$slideTrack.children(".slick-slide").length)));
var e=f.$slides.first().outerWidth(!0)-f.$slides.first().width();
f.options.variableWidth===!1&&f.$slideTrack.children(".slick-slide").width(f.slideWidth-e)
},c.prototype.setFade=function(){var e,a=this;
a.$slides.each(function(f,b){e=a.slideWidth*f*-1,a.options.rtl===!0?d(b).css({position:"relative",right:e,top:0,zIndex:a.options.zIndex-2,opacity:0}):d(b).css({position:"relative",left:e,top:0,zIndex:a.options.zIndex-2,opacity:0})
}),a.$slides.eq(a.currentSlide).css({zIndex:a.options.zIndex-1,opacity:1})
},c.prototype.setHeight=function(){var f=this;
if(1===f.options.slidesToShow&&f.options.adaptiveHeight===!0&&f.options.vertical===!1){var e=f.$slides.eq(f.currentSlide).outerHeight(!0);
f.$list.css("height",e)
}},c.prototype.setOption=c.prototype.slickSetOption=function(a,n,m){var k,h,l=this;
if("responsive"===a&&"array"===d.type(n)){for(h in n){if("array"!==d.type(l.options.responsive)){l.options.responsive=[n[h]]
}else{for(k=l.options.responsive.length-1;
k>=0;
){l.options.responsive[k].breakpoint===n[h].breakpoint&&l.options.responsive.splice(k,1),k--
}l.options.responsive.push(n[h])
}}}else{l.options[a]=n
}m===!0&&(l.unload(),l.reinit())
},c.prototype.setPosition=function(){var b=this;
b.setDimensions(),b.setHeight(),b.options.fade===!1?b.setCSS(b.getLeft(b.currentSlide)):b.setFade(),b.$slider.trigger("setPosition",[b])
},c.prototype.setProps=function(){var f=this,e=document.body.style;
f.positionProp=f.options.vertical===!0?"top":"left","top"===f.positionProp?f.$slider.addClass("slick-vertical"):f.$slider.removeClass("slick-vertical"),(void 0!==e.WebkitTransition||void 0!==e.MozTransition||void 0!==e.msTransition)&&f.options.useCSS===!0&&(f.cssTransitions=!0),f.options.fade&&("number"==typeof f.options.zIndex?f.options.zIndex<3&&(f.options.zIndex=3):f.options.zIndex=f.defaults.zIndex),void 0!==e.OTransform&&(f.animType="OTransform",f.transformType="-o-transform",f.transitionType="OTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(f.animType=!1)),void 0!==e.MozTransform&&(f.animType="MozTransform",f.transformType="-moz-transform",f.transitionType="MozTransition",void 0===e.perspectiveProperty&&void 0===e.MozPerspective&&(f.animType=!1)),void 0!==e.webkitTransform&&(f.animType="webkitTransform",f.transformType="-webkit-transform",f.transitionType="webkitTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(f.animType=!1)),void 0!==e.msTransform&&(f.animType="msTransform",f.transformType="-ms-transform",f.transitionType="msTransition",void 0===e.msTransform&&(f.animType=!1)),void 0!==e.transform&&f.animType!==!1&&(f.animType="transform",f.transformType="transform",f.transitionType="transition"),f.transformsEnabled=f.options.useTransform&&null!==f.animType&&f.animType!==!1
},c.prototype.setSlideClasses=function(h){var n,m,l,k,g=this;
m=g.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),g.$slides.eq(h).addClass("slick-current"),g.options.centerMode===!0?(n=Math.floor(g.options.slidesToShow/2),g.options.infinite===!0&&(h>=n&&h<=g.slideCount-1-n?g.$slides.slice(h-n,h+n+1).addClass("slick-active").attr("aria-hidden","false"):(l=g.options.slidesToShow+h,m.slice(l-n+1,l+n+2).addClass("slick-active").attr("aria-hidden","false")),0===h?m.eq(m.length-1-g.options.slidesToShow).addClass("slick-center"):h===g.slideCount-1&&m.eq(g.options.slidesToShow).addClass("slick-center")),g.$slides.eq(h).addClass("slick-center")):h>=0&&h<=g.slideCount-g.options.slidesToShow?g.$slides.slice(h,h+g.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):m.length<=g.options.slidesToShow?m.addClass("slick-active").attr("aria-hidden","false"):(k=g.slideCount%g.options.slidesToShow,l=g.options.infinite===!0?g.options.slidesToShow+h:h,g.options.slidesToShow==g.options.slidesToScroll&&g.slideCount-h<g.options.slidesToShow?m.slice(l-(g.options.slidesToShow-k),l+k).addClass("slick-active").attr("aria-hidden","false"):m.slice(l,l+g.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===g.options.lazyLoad&&g.lazyLoad()
},c.prototype.setupInfinite=function(){var h,g,f,a=this;
if(a.options.fade===!0&&(a.options.centerMode=!1),a.options.infinite===!0&&a.options.fade===!1&&(g=null,a.slideCount>a.options.slidesToShow)){for(f=a.options.centerMode===!0?a.options.slidesToShow+1:a.options.slidesToShow,h=a.slideCount;
h>a.slideCount-f;
h-=1){g=h-1,d(a.$slides[g]).clone(!0).attr("id","").attr("data-slick-index",g-a.slideCount).prependTo(a.$slideTrack).addClass("slick-cloned")
}for(h=0;
f>h;
h+=1){g=h,d(a.$slides[g]).clone(!0).attr("id","").attr("data-slick-index",g+a.slideCount).appendTo(a.$slideTrack).addClass("slick-cloned")
}a.$slideTrack.find(".slick-cloned").find("[id]").each(function(){d(this).attr("id","")
})
}},c.prototype.setPaused=function(f){var e=this;
e.options.autoplay===!0&&e.options.pauseOnHover===!0&&(e.paused=f,f?e.autoPlayClear():e.autoPlay())
},c.prototype.selectHandler=function(a){var h=this,g=d(a.target).is(".slick-slide")?d(a.target):d(a.target).parents(".slick-slide"),f=parseInt(g.attr("data-slick-index"));
return f||(f=0),h.slideCount<=h.options.slidesToShow?(h.setSlideClasses(f),void h.asNavFor(f)):void h.slideHandler(f)
},c.prototype.slideHandler=function(s,r,q){var p,o,n,m,l=null,k=this;
return r=r||!1,k.animating===!0&&k.options.waitForAnimate===!0||k.options.fade===!0&&k.currentSlide===s||k.slideCount<=k.options.slidesToShow?void 0:(r===!1&&k.asNavFor(s),p=s,l=k.getLeft(p),m=k.getLeft(k.currentSlide),k.currentLeft=null===k.swipeLeft?m:k.swipeLeft,k.options.infinite===!1&&k.options.centerMode===!1&&(0>s||s>k.getDotCount()*k.options.slidesToScroll)?void (k.options.fade===!1&&(p=k.currentSlide,q!==!0?k.animateSlide(m,function(){k.postSlide(p)
}):k.postSlide(p))):k.options.infinite===!1&&k.options.centerMode===!0&&(0>s||s>k.slideCount-k.options.slidesToScroll)?void (k.options.fade===!1&&(p=k.currentSlide,q!==!0?k.animateSlide(m,function(){k.postSlide(p)
}):k.postSlide(p))):(k.options.autoplay===!0&&clearInterval(k.autoPlayTimer),o=0>p?k.slideCount%k.options.slidesToScroll!==0?k.slideCount-k.slideCount%k.options.slidesToScroll:k.slideCount+p:p>=k.slideCount?k.slideCount%k.options.slidesToScroll!==0?0:p-k.slideCount:p,k.animating=!0,k.$slider.trigger("beforeChange",[k,k.currentSlide,o]),n=k.currentSlide,k.currentSlide=o,k.setSlideClasses(k.currentSlide),k.updateDots(),k.updateArrows(),k.options.fade===!0?(q!==!0?(k.fadeSlideOut(n),k.fadeSlide(o,function(){k.postSlide(o)
})):k.postSlide(o),void k.animateHeight()):void (q!==!0?k.animateSlide(l,function(){k.postSlide(o)
}):k.postSlide(o))))
},c.prototype.startLoad=function(){var b=this;
b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow.hide(),b.$nextArrow.hide()),b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&b.$dots.hide(),b.$slider.addClass("slick-loading")
},c.prototype.swipeDirection=function(){var g,f,l,k,h=this;
return g=h.touchObject.startX-h.touchObject.curX,f=h.touchObject.startY-h.touchObject.curY,l=Math.atan2(f,g),k=Math.round(180*l/Math.PI),0>k&&(k=360-Math.abs(k)),45>=k&&k>=0?h.options.rtl===!1?"left":"right":360>=k&&k>=315?h.options.rtl===!1?"left":"right":k>=135&&225>=k?h.options.rtl===!1?"right":"left":h.options.verticalSwiping===!0?k>=35&&135>=k?"left":"right":"vertical"
},c.prototype.swipeEnd=function(f){var g,e=this;
if(e.dragging=!1,e.shouldClick=e.touchObject.swipeLength>10?!1:!0,void 0===e.touchObject.curX){return !1
}if(e.touchObject.edgeHit===!0&&e.$slider.trigger("edge",[e,e.swipeDirection()]),e.touchObject.swipeLength>=e.touchObject.minSwipe){switch(e.swipeDirection()){case"left":g=e.options.swipeToSlide?e.checkNavigable(e.currentSlide+e.getSlideCount()):e.currentSlide+e.getSlideCount(),e.slideHandler(g),e.currentDirection=0,e.touchObject={},e.$slider.trigger("swipe",[e,"left"]);
break;
case"right":g=e.options.swipeToSlide?e.checkNavigable(e.currentSlide-e.getSlideCount()):e.currentSlide-e.getSlideCount(),e.slideHandler(g),e.currentDirection=1,e.touchObject={},e.$slider.trigger("swipe",[e,"right"])
}}else{e.touchObject.startX!==e.touchObject.curX&&(e.slideHandler(e.currentSlide),e.touchObject={})
}},c.prototype.swipeHandler=function(f){var e=this;
if(!(e.options.swipe===!1||"ontouchend" in document&&e.options.swipe===!1||e.options.draggable===!1&&-1!==f.type.indexOf("mouse"))){switch(e.touchObject.fingerCount=f.originalEvent&&void 0!==f.originalEvent.touches?f.originalEvent.touches.length:1,e.touchObject.minSwipe=e.listWidth/e.options.touchThreshold,e.options.verticalSwiping===!0&&(e.touchObject.minSwipe=e.listHeight/e.options.touchThreshold),f.data.action){case"start":e.swipeStart(f);
break;
case"move":e.swipeMove(f);
break;
case"end":e.swipeEnd(f)
}}},c.prototype.swipeMove=function(l){var q,p,o,n,m,k=this;
return m=void 0!==l.originalEvent?l.originalEvent.touches:null,!k.dragging||m&&1!==m.length?!1:(q=k.getLeft(k.currentSlide),k.touchObject.curX=void 0!==m?m[0].pageX:l.clientX,k.touchObject.curY=void 0!==m?m[0].pageY:l.clientY,k.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(k.touchObject.curX-k.touchObject.startX,2))),k.options.verticalSwiping===!0&&(k.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(k.touchObject.curY-k.touchObject.startY,2)))),p=k.swipeDirection(),"vertical"!==p?(void 0!==l.originalEvent&&k.touchObject.swipeLength>4&&l.preventDefault(),n=(k.options.rtl===!1?1:-1)*(k.touchObject.curX>k.touchObject.startX?1:-1),k.options.verticalSwiping===!0&&(n=k.touchObject.curY>k.touchObject.startY?1:-1),o=k.touchObject.swipeLength,k.touchObject.edgeHit=!1,k.options.infinite===!1&&(0===k.currentSlide&&"right"===p||k.currentSlide>=k.getDotCount()&&"left"===p)&&(o=k.touchObject.swipeLength*k.options.edgeFriction,k.touchObject.edgeHit=!0),k.options.vertical===!1?k.swipeLeft=q+o*n:k.swipeLeft=q+o*(k.$list.height()/k.listWidth)*n,k.options.verticalSwiping===!0&&(k.swipeLeft=q+o*n),k.options.fade===!0||k.options.touchMove===!1?!1:k.animating===!0?(k.swipeLeft=null,!1):void k.setCSS(k.swipeLeft)):void 0)
},c.prototype.swipeStart=function(f){var g,e=this;
return 1!==e.touchObject.fingerCount||e.slideCount<=e.options.slidesToShow?(e.touchObject={},!1):(void 0!==f.originalEvent&&void 0!==f.originalEvent.touches&&(g=f.originalEvent.touches[0]),e.touchObject.startX=e.touchObject.curX=void 0!==g?g.pageX:f.clientX,e.touchObject.startY=e.touchObject.curY=void 0!==g?g.pageY:f.clientY,void (e.dragging=!0))
},c.prototype.unfilterSlides=c.prototype.slickUnfilter=function(){var b=this;
null!==b.$slidesCache&&(b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.appendTo(b.$slideTrack),b.reinit())
},c.prototype.unload=function(){var a=this;
d(".slick-cloned",a.$slider).remove(),a.$dots&&a.$dots.remove(),a.$prevArrow&&a.htmlExpr.test(a.options.prevArrow)&&a.$prevArrow.remove(),a.$nextArrow&&a.htmlExpr.test(a.options.nextArrow)&&a.$nextArrow.remove(),a.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")
},c.prototype.unslick=function(f){var e=this;
e.$slider.trigger("unslick",[e,f]),e.destroy()
},c.prototype.updateArrows=function(){var e,f=this;
e=Math.floor(f.options.slidesToShow/2),f.options.arrows===!0&&f.slideCount>f.options.slidesToShow&&!f.options.infinite&&(f.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),f.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===f.currentSlide?(f.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),f.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):f.currentSlide>=f.slideCount-f.options.slidesToShow&&f.options.centerMode===!1?(f.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),f.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):f.currentSlide>=f.slideCount-1&&f.options.centerMode===!0&&(f.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),f.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))
},c.prototype.updateDots=function(){var b=this;
null!==b.$dots&&(b.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),b.$dots.find("li").eq(Math.floor(b.currentSlide/b.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))
},c.prototype.visibility=function(){var b=this;
document[b.hidden]?(b.paused=!0,b.autoPlayClear()):b.options.autoplay===!0&&(b.paused=!1,b.autoPlay())
},c.prototype.initADA=function(){var a=this;
a.$slides.add(a.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),a.$slideTrack.attr("role","listbox"),a.$slides.not(a.$slideTrack.find(".slick-cloned")).each(function(b){d(this).attr({role:"option","aria-describedby":"slick-slide"+a.instanceUid+b})
}),null!==a.$dots&&a.$dots.attr("role","tablist").find("li").each(function(b){d(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+a.instanceUid+b,id:"slick-slide"+a.instanceUid+b})
}).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),a.activateADA()
},c.prototype.activateADA=function(){var b=this;
b.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})
},c.prototype.focusHandler=function(){var a=this;
a.$slider.on("focus.slick blur.slick","*",function(e){e.stopImmediatePropagation();
var b=d(this);
setTimeout(function(){a.isPlay&&(b.is(":focus")?(a.autoPlayClear(),a.paused=!0):(a.paused=!1,a.autoPlay()))
},0)
})
},d.fn.slick=function(){var k,h,b=this,n=arguments[0],m=Array.prototype.slice.call(arguments,1),l=b.length;
for(k=0;
l>k;
k++){if("object"==typeof n||"undefined"==typeof n?b[k].slick=new c(b[k],n):h=b[k].slick[n].apply(b[k].slick,m),"undefined"!=typeof h){return h
}}return b
}
});
/*!
 * imagesLoaded PACKAGED v4.1.1
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
;
!function(a,b){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",b):"object"==typeof module&&module.exports?module.exports=b():a.EvEmitter=b()
}("undefined"!=typeof window?window:this,function(){function a(){}var b=a.prototype;
return b.on=function(d,f){if(d&&f){var c=this._events=this._events||{},g=c[d]=c[d]||[];
return -1==g.indexOf(f)&&g.push(f),this
}},b.once=function(d,f){if(d&&f){this.on(d,f);
var c=this._onceEvents=this._onceEvents||{},g=c[d]=c[d]||{};
return g[f]=!0,this
}},b.off=function(d,f){var c=this._events&&this._events[d];
if(c&&c.length){var g=c.indexOf(f);
return -1!=g&&c.splice(g,1),this
}},b.emitEvent=function(d,h){var c=this._events&&this._events[d];
if(c&&c.length){var l=0,k=c[l];
h=h||[];
for(var g=this._onceEvents&&this._onceEvents[d];
k;
){var f=g&&g[k];
f&&(this.off(d,k),delete g[k]),k.apply(this,h),l+=f?0:1,k=c[l]
}return this
}},a
}),function(a,b){"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter"],function(c){return b(a,c)
}):"object"==typeof module&&module.exports?module.exports=b(a,require("ev-emitter")):a.imagesLoaded=b(a,a.EvEmitter)
}(window,function(q,l){function g(d,h){for(var a in h){d[a]=h[a]
}return d
}function f(d){var h=[];
if(Array.isArray(d)){h=d
}else{if("number"==typeof d.length){for(var a=0;
a<d.length;
a++){h.push(d[a])
}}else{h.push(d)
}}return h
}function c(a,h,d){return this instanceof c?("string"==typeof a&&(a=document.querySelectorAll(a)),this.elements=f(a),this.options=g({},this.options),"function"==typeof h?d=h:g(this.options,h),d&&this.on("always",d),this.getImages(),k&&(this.jqDeferred=new k.Deferred),void setTimeout(function(){this.check()
}.bind(this))):new c(a,h,d)
}function b(a){this.img=a
}function u(a,d){this.url=a,this.element=d,this.img=new Image
}var k=q.jQuery,p=q.console;
c.prototype=Object.create(l.prototype),c.prototype.options={},c.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)
},c.prototype.addElementImages=function(d){"IMG"==d.nodeName&&this.addImage(d),this.options.background===!0&&this.addElementBackgroundImages(d);
var w=d.nodeType;
if(w&&m[w]){for(var a=d.querySelectorAll("img"),y=0;
y<a.length;
y++){var x=a[y];
this.addImage(x)
}if("string"==typeof this.options.background){var v=d.querySelectorAll(this.options.background);
for(y=0;
y<v.length;
y++){var h=v[y];
this.addElementBackgroundImages(h)
}}}};
var m={1:!0,9:!0,11:!0};
return c.prototype.addElementBackgroundImages=function(d){var h=getComputedStyle(d);
if(h){for(var a=/url\((['"])?(.*?)\1\)/gi,s=a.exec(h.backgroundImage);
null!==s;
){var r=s&&s[2];
r&&this.addBackground(r,d),s=a.exec(h.backgroundImage)
}}},c.prototype.addImage=function(a){var d=new b(a);
this.images.push(d)
},c.prototype.addBackground=function(d,h){var a=new u(d,h);
this.images.push(a)
},c.prototype.check=function(){function a(h,e,o){setTimeout(function(){d.progress(h,e,o)
})
}var d=this;
return this.progressedCount=0,this.hasAnyBroken=!1,this.images.length?void this.images.forEach(function(h){h.once("progress",a),h.check()
}):void this.complete()
},c.prototype.progress=function(d,h,a){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!d.isLoaded,this.emitEvent("progress",[this,d,h]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,d),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&p&&p.log("progress: "+a,d,h)
},c.prototype.complete=function(){var a=this.hasAnyBroken?"fail":"done";
if(this.isComplete=!0,this.emitEvent(a,[this]),this.emitEvent("always",[this]),this.jqDeferred){var d=this.hasAnyBroken?"reject":"resolve";
this.jqDeferred[d](this)
}},b.prototype=Object.create(l.prototype),b.prototype.check=function(){var a=this.getIsImageComplete();
return a?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),void (this.proxyImage.src=this.img.src))
},b.prototype.getIsImageComplete=function(){return this.img.complete&&void 0!==this.img.naturalWidth
},b.prototype.confirm=function(a,d){this.isLoaded=a,this.emitEvent("progress",[this,this.img,d])
},b.prototype.handleEvent=function(a){var d="on"+a.type;
this[d]&&this[d](a)
},b.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()
},b.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()
},b.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)
},u.prototype=Object.create(b.prototype),u.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url;
var a=this.getIsImageComplete();
a&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())
},u.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)
},u.prototype.confirm=function(a,d){this.isLoaded=a,this.emitEvent("progress",[this,this.element,d])
},c.makeJQueryPlugin=function(a){a=a||q.jQuery,a&&(k=a,k.fn.imagesLoaded=function(h,n){var d=new c(this,h,n);
return d.jqDeferred.promise(k(this))
})
},c.makeJQueryPlugin(),c
});
if(!Array.prototype.includes){Object.defineProperty(Array.prototype,"includes",{value:function(d,e){if(this==null){throw new TypeError('"this" is null or not defined')
}var f=Object(this);
var a=f.length>>>0;
if(a===0){return false
}var g=e|0;
var c=Math.max(g>=0?g:a-Math.abs(g),0);
function b(h,k){return h===k||(typeof h==="number"&&typeof k==="number"&&isNaN(h)&&isNaN(k))
}while(c<a){if(b(f[c],d)){return true
}c++
}return false
}})
}if(!Array.prototype.find){Object.defineProperty(Array.prototype,"find",{value:function(b){if(this==null){throw new TypeError('"this" is null or not defined')
}var f=Object(this);
var a=f.length>>>0;
if(typeof b!=="function"){throw new TypeError("predicate must be a function")
}var c=arguments[1];
var d=0;
while(d<a){var e=f[d];
if(b.call(c,e,d,f)){return e
}d++
}return undefined
}})
}if(!String.prototype.includes){String.prototype.includes=function(a,b){if(typeof b!=="number"){b=0
}if(b+a.length>this.length){return false
}else{return this.indexOf(a,b)!==-1
}}
}$(document).ready(function(){var b=$(document);
b.foundation();
window.sl=window.sl||{};
window.sl.pageProperties={};
window.sl.pageProperties.videoViewers={};
var a={findViewers:function(){$(".video-viewer").each(function(){var c=$(this);
if(c.hasClass("medium")||c.hasClass("large")){if(c.hasClass("large")&&Foundation.MediaQuery.atLeast("large")){a.initViewer($(this)[0])
}else{if(c.hasClass("medium")&&!Foundation.MediaQuery.atLeast("large")){a.initViewer($(this)[0])
}}}else{a.initViewer($(this)[0])
}})
},initViewer:function(d){var e=JSON.parse($(d).attr("data-video-settings"));
var c=new s7viewers.VideoViewer();
c.setParam("serverurl",e.serverurl);
c.setParam("videoserverurl",e.videoserverurl);
c.setParam("contenturl",e.contenturl);
c.setParam("asset",e.asset);
if(e.posterimage!==""||e.posterimage!==null){c.setParam("posterimage",e.posterimage)
}c.setParam("config",e.config);
c.setParam("config2",e.config2);
c.setHandlers({initComplete:function(){var f=c.getComponent("videoPlayer");
f.addEventListener(s7sdk.event.CapabilityStateEvent.NOTF_VIDEO_CAPABILITY_STATE,a.onVideoStateChange,false);
if(($(".slick-active").find(".video-viewer"))&&e.autoplay===1){if($(".slick-active").find(".video-viewer").attr("id")===c.containerId){f.play()
}}if(($(".slick-active").closest(".slbs2-video-gallery").length)&&($(".slick-active").attr("data-slick-index")!=="0")){if(Foundation.MediaQuery.atLeast("medium")){f.play()
}}if("ontouchstart" in window){$(document).on("touchstart","#"+c.containerId+" .s7iconeffect",function(){f.play()
})
}}});
c.setContainerId(e.containerId);
if($("#"+c.containerId).closest(".slbs2-video-gallery").length>0){if(window.sl.pageProperties.videoViewersToInit&&window.sl.pageProperties.videoViewersToInit[c.containerId]){c.init()
}}else{c.init()
}window.sl.pageProperties.videoViewers[e.containerId]=c
},onVideoStateChange:function(e){var g=$(this);
var f=false;
var d=g.closest(".carousel");
if(d.length>0){f=true
}var c=e.s7event.state;
if(c.hasCapability(s7sdk.VideoCapabilityState.PAUSE)){if(f){d.find(".slick-initialized").slick("slickPause");
d.find(".autoplay-toggle").fadeOut()
}}else{if(c.hasCapability(s7sdk.VideoCapabilityState.PLAY)||c.hasCapability(s7sdk.VideoCapabilityState.REPLAY)){if(f&&d.hasClass("autoplay")){d.find(".slick-initialized").slick("slickPlay");
d.find(".autoplay-toggle").fadeIn()
}}}},modalEvents:function(){b.on("open.zf.reveal",function(e){var c=$(e.target).find(".video-modal-viewer");
if(c.length>0){var d=JSON.parse(c.attr("data-video-settings"));
var f=d.containerId;
if(window.sl.pageProperties.videoViewers[f]===undefined){a.initViewer(c[0])
}else{a.reactivateVideo(f,d)
}}});
b.on("closed.zf.reveal",function(c){var d=$(c.target).find(".video-modal-viewer");
$(d).each(function(){a.pausePreviouslyShownVideo($(this))
})
})
},viewportChangeEvents:function(){$(window).on("changed.zf.mediaquery",function(e,c,d){if(Foundation.MediaQuery.atLeast("large")&&(d==="medium"||d==="small")){$(".video-viewer.large").each(function(){a.initPreviouslyHiddenVideo($(this))
})
}if((c==="medium"||c==="small")&&(d==="large"||d==="xlarge"||d==="xxlarge")){$(".video-viewer.medium").each(function(){a.initPreviouslyHiddenVideo($(this))
})
}})
},initPreviouslyHiddenVideo:function(c){var d=JSON.parse(c.attr("data-video-settings"));
var e=d.containerId;
if(Foundation.MediaQuery.atLeast("large")){$(".video-viewer.medium").each(function(){a.pausePreviouslyShownVideo($(this))
})
}else{$(".video-viewer.large").each(function(){a.pausePreviouslyShownVideo($(this))
})
}if(window.sl.pageProperties.videoViewers[e]===undefined){a.initViewer(c[0])
}else{a.reactivateVideo(e,d)
}},pausePreviouslyShownVideo:function(c){var f=JSON.parse(c.attr("data-video-settings"));
var e=f.containerId;
var d=false;
if(window.sl.pageProperties.videoViewers&&window.sl.pageProperties.videoViewers[e]&&window.sl.pageProperties.videoViewers[e].getComponent("videoPlayer")){d=window.sl.pageProperties.videoViewers[e].getComponent("videoPlayer");
d.pause()
}},reactivateVideo:function(e,c){var d=false;
if(window.sl.pageProperties.videoViewers&&window.sl.pageProperties.videoViewers[e]&&window.sl.pageProperties.videoViewers[e].getComponent("videoPlayer")){d=window.sl.pageProperties.videoViewers[e].getComponent("videoPlayer")
}if(d&&c.autoplay==="1"){if((d.getDuration()-d.getCurrentTime())<=1){d.seek(0)
}d.play()
}}};
a.findViewers();
a.modalEvents();
a.viewportChangeEvents();
return a
});
(function(c,a,b){var d={caseStudyList:null,filterBar:c(".js-case-study-filter-bar"),labelList:c(".js-label-list"),articleList:c(".js-case-study-articles"),caseStudyArticleTemplate:c("#caseStudyArticleTemplate"),filters:[],init:function(){var h=this;
h.populateArticles();
h.filterBar.removeClass("active");
c(".js-clear-all").on("click",function f(k){k.preventDefault();
c.each(h.labelList.find(".label"),function(l,m){h.removeFilter(c(m),c(m).attr("data-label"))
})
});
c(".slbs2-page-case-study-landing").on("click",".js-filter-tag",function g(l){l.preventDefault();
var k=c(this).attr("data-tagName");
if(c('.label[data-label="'+k+'"]').length===0){h.addFilter(k,c(this).attr("data-displayName"))
}h.scrollToFilters()
});
c(".js-label-list").on("click",".label",function e(){h.removeFilter(c(this),c(this).attr("data-label"))
})
},scrollToFilters:function(){var e=c(".case-study-filter-bar").offset().top;
if(!Foundation.MediaQuery.atLeast("large")){e=e-c(".slbs2-header").height()
}c(b).scrollTop(e)
},initializeHash:function(){var h=this;
if(!!a.location.hash){var e=a.location.hash.substring(1).split(",");
for(var f=0;
f<e.length;
f++){var g=c('[data-tagName="'+e[f]+'"]');
if(g.length>0){h.addFilter(e[f],c(g[0]).attr("data-displayName"))
}}h.scrollToFilters()
}},populateArticles:function(){var f=this;
var e=Handlebars.compile(f.caseStudyArticleTemplate.html());
c.getJSON("/sl/stat/casestudylist.json",function(g){c.each(g,function(h,l){var k=e(l);
f.articleList.append(k)
});
f.initializeHash()
});
Handlebars.registerHelper("attributeList",function(g){var h=[];
c.each(g,function(k,l){h.push(l.id)
});
return h.join(",")
});
Handlebars.registerHelper("ellipses",function(g){if(g.length>=30){return g.substring(0,30)+"..."
}else{return g
}})
},addFilter:function(f,e){var h=this;
h.filterBar.addClass("active");
var g=c('<span class="label" data-label="'+f+'">');
g.html(e);
h.labelList.append(g);
h.filters.push(f);
h.applyFilters()
},removeFilter:function(g,f){var h=this;
var e=h.filters.indexOf(f);
if(e>-1){h.filters.splice(e,1)
}g.remove();
if(h.filters.length===0){h.filterBar.removeClass("active")
}h.applyFilters()
},applyFilters:function(){var g=this;
var f="";
if(g.filters.length>0){c(".case-study-article").hide();
for(var e=0;
e<g.filters.length;
e++){f+='[data-article-tags*="'+g.filters[e]+'"]'
}c(".case-study-article").filter(f).show()
}else{c(".case-study-article").show()
}}};
c(a).on("ready",function(){if(!isEdit&&!isDesign&&typeof isCaseStudyTemplate!="undefined"){d.init()
}})
})(jQuery,document,window);
!function(){function W(a){return"undefined"==typeof this||Object.getPrototypeOf(this)!==W.prototype?new W(a):(z=this,z.version="3.3.1",z.tools=new G,z.isSupported()?(z.tools.extend(z.defaults,a||{}),H(z.defaults),z.store={elements:{},containers:[]},z.sequences={},z.history=[],z.uid=0,z.initialized=!1):"undefined"!=typeof console&&null!==console,z)
}function H(a){if(a&&a.container){if("string"==typeof a.container){return window.document.documentElement.querySelector(a.container)
}if(z.tools.isNode(a.container)){return a.container
}}return z.defaults.container
}function N(b,a){return"string"==typeof b?Array.prototype.slice.call(a.querySelectorAll(b)):z.tools.isNode(b)?[b]:z.tools.isNodeList(b)?Array.prototype.slice.call(b):[]
}function R(){return ++z.uid
}function M(b,a,c){a.container&&(a.container=c),b.config?b.config=z.tools.extendClone(b.config,a):b.config=z.tools.extendClone(z.defaults,a),"top"===b.config.origin||"bottom"===b.config.origin?b.config.axis="Y":b.config.axis="X"
}function J(b){var a=window.getComputedStyle(b.domEl);
b.styles||(b.styles={transition:{},transform:{},computed:{}},b.styles.inline=b.domEl.getAttribute("style")||"",b.styles.inline+="; visibility: visible; ",b.styles.computed.opacity=a.opacity,a.transition&&"all 0s ease 0s"!==a.transition?b.styles.computed.transition=a.transition+", ":b.styles.computed.transition=""),b.styles.transition.instant=I(b,0),b.styles.transition.delayed=I(b,b.config.delay),b.styles.transform.initial=" -webkit-transform:",b.styles.transform.target=" -webkit-transform:",aa(b),b.styles.transform.initial+="transform:",b.styles.transform.target+="transform:",aa(b)
}function I(b,a){var c=b.config;
return"-webkit-transition: "+b.styles.computed.transition+"-webkit-transform "+c.duration/1000+"s "+c.easing+" "+a/1000+"s, opacity "+c.duration/1000+"s "+c.easing+" "+a/1000+"s; transition: "+b.styles.computed.transition+"transform "+c.duration/1000+"s "+c.easing+" "+a/1000+"s, opacity "+c.duration/1000+"s "+c.easing+" "+a/1000+"s; "
}function aa(c){var b,d=c.config,a=c.styles.transform;
b="top"===d.origin||"left"===d.origin?/^-/.test(d.distance)?d.distance.substr(1):"-"+d.distance:d.distance,parseInt(d.distance)&&(a.initial+=" translate"+d.axis+"("+b+")",a.target+=" translate"+d.axis+"(0)"),d.scale&&(a.initial+=" scale("+d.scale+")",a.target+=" scale(1)"),d.rotate.x&&(a.initial+=" rotateX("+d.rotate.x+"deg)",a.target+=" rotateX(0)"),d.rotate.y&&(a.initial+=" rotateY("+d.rotate.y+"deg)",a.target+=" rotateY(0)"),d.rotate.z&&(a.initial+=" rotateZ("+d.rotate.z+"deg)",a.target+=" rotateZ(0)"),a.initial+="; opacity: "+d.opacity+";",a.target+="; opacity: "+c.styles.computed.opacity+";"
}function Q(b){var a=b.config.container;
a&&z.store.containers.indexOf(a)===-1&&z.store.containers.push(b.config.container),z.store.elements[b.id]=b
}function Y(c,b,d){var a={target:c,config:b,interval:d};
z.history.push(a)
}function V(){if(z.isSupported()){A();
for(var a=0;
a<z.store.containers.length;
a++){z.store.containers[a].addEventListener("scroll",X),z.store.containers[a].addEventListener("resize",X)
}z.initialized||(window.addEventListener("scroll",X),window.addEventListener("resize",X),z.initialized=!0)
}return z
}function X(){k(A)
}function F(){var c,b,d,a;
z.tools.forOwn(z.sequences,function(f){a=z.sequences[f],c=!1;
for(var e=0;
e<a.elemIds.length;
e++){d=a.elemIds[e],b=z.store.elements[d],K(b)&&!c&&(c=!0)
}a.active=c
})
}function A(){var b,a;
F(),z.tools.forOwn(z.store.elements,function(c){a=z.store.elements[c],b=C(a),U(a)?(a.config.beforeReveal(a.domEl),b?a.domEl.setAttribute("style",a.styles.inline+a.styles.transform.target+a.styles.transition.delayed):a.domEl.setAttribute("style",a.styles.inline+a.styles.transform.target+a.styles.transition.instant),L("reveal",a,b),a.revealing=!0,a.seen=!0,a.sequence&&P(a,b)):D(a)&&(a.config.beforeReset(a.domEl),a.domEl.setAttribute("style",a.styles.inline+a.styles.transform.initial+a.styles.transition.instant),L("reset",a),a.revealing=!1)
})
}function P(c,b){var f=0,a=0,d=z.sequences[c.sequence.id];
d.blocked=!0,b&&"onload"===c.config.useDelay&&(a=c.config.delay),c.sequence.timer&&(f=Math.abs(c.sequence.timer.started-new Date),window.clearTimeout(c.sequence.timer)),c.sequence.timer={started:new Date},c.sequence.timer.clock=window.setTimeout(function(){d.blocked=!1,c.sequence.timer=null,X()
},Math.abs(d.interval)+a-f)
}function L(d,b,g){var a=0,f=0,c="after";
switch(d){case"reveal":f=b.config.duration,g&&(f+=b.config.delay),c+="Reveal";
break;
case"reset":f=b.config.duration,c+="Reset"
}b.timer&&(a=Math.abs(b.timer.started-new Date),window.clearTimeout(b.timer.clock)),b.timer={started:new Date},b.timer.clock=window.setTimeout(function(){b.config[c](b.domEl),b.timer=null
},f-a)
}function U(b){if(b.sequence){var a=z.sequences[b.sequence.id];
return a.active&&!a.blocked&&!b.revealing&&!b.disabled
}return K(b)&&!b.revealing&&!b.disabled
}function C(b){var a=b.config.useDelay;
return"always"===a||"onload"===a&&!z.initialized||"once"===a&&!b.seen
}function D(b){if(b.sequence){var a=z.sequences[b.sequence.id];
return !a.active&&b.config.reset&&b.revealing&&!b.disabled
}return !K(b)&&b.config.reset&&b.revealing&&!b.disabled
}function Z(a){return{width:a.clientWidth,height:a.clientHeight}
}function S(b){if(b&&b!==window.document.documentElement){var a=B(b);
return{x:b.scrollLeft+a.left,y:b.scrollTop+a.top}
}return{x:window.pageXOffset,y:window.pageYOffset}
}function B(c){var b=0,f=0,a=c.offsetHeight,d=c.offsetWidth;
do{isNaN(c.offsetTop)||(b+=c.offsetTop),isNaN(c.offsetLeft)||(f+=c.offsetLeft),c=c.offsetParent
}while(c);
return{top:b,left:f,height:a,width:d}
}function K(v){function O(){var e=x+y*T,r=q+m*T,d=w-y*T,o=E-m*T,a=b.y+v.config.viewOffset.top,l=b.x+v.config.viewOffset.left,f=b.y-v.config.viewOffset.bottom+g.height,c=b.x-v.config.viewOffset.right+g.width;
return e<f&&d>a&&r>l&&o<c
}function h(){return"fixed"===window.getComputedStyle(v.domEl).position
}var p=B(v.domEl),g=Z(v.config.container),b=S(v.config.container),T=v.config.viewFactor,y=p.height,m=p.width,x=p.top,q=p.left,w=x+y,E=q+m;
return O()||h()
}function G(){}var z,k;
W.prototype.defaults={origin:"bottom",distance:"20px",duration:500,delay:0,rotate:{x:0,y:0,z:0},opacity:0,scale:0.9,easing:"cubic-bezier(0.6, 0.2, 0.1, 1)",container:window.document.documentElement,mobile:!0,reset:!1,useDelay:"always",viewFactor:0.2,viewOffset:{top:0,right:0,bottom:0,left:0},beforeReveal:function(a){},afterReveal:function(a){},beforeReset:function(a){},afterReset:function(a){}},W.prototype.isSupported=function(){var a=document.documentElement.style;
return"WebkitTransition" in a&&"WebkitTransform" in a||"transition" in a&&"transform" in a
},W.prototype.reveal=function(h,x,o,l){var t,n,c,b,f,q;
if(void 0!==x&&"number"==typeof x?(o=x,x={}):void 0!==x&&null!==x||(x={}),t=H(x),n=N(h,t),!n.length){return z
}o&&"number"==typeof o&&(q=R(),f=z.sequences[q]={id:q,interval:o,elemIds:[],active:!1});
for(var r=0;
r<n.length;
r++){b=n[r].getAttribute("data-sr-id"),b?c=z.store.elements[b]:(c={id:R(),domEl:n[r],seen:!1,revealing:!1},c.domEl.setAttribute("data-sr-id",c.id)),f&&(c.sequence={id:f.id,index:f.elemIds.length},f.elemIds.push(c.id)),M(c,x,t),J(c),Q(c),z.tools.isMobile()&&!c.config.mobile||!z.isSupported()?(c.domEl.setAttribute("style",c.styles.inline),c.disabled=!0):c.revealing||c.domEl.setAttribute("style",c.styles.inline+c.styles.transform.initial)
}return !l&&z.isSupported()&&(Y(h,x,o),z.initTimeout&&window.clearTimeout(z.initTimeout),z.initTimeout=window.setTimeout(V,0)),z
},W.prototype.sync=function(){if(z.history.length&&z.isSupported()){for(var b=0;
b<z.history.length;
b++){var a=z.history[b];
z.reveal(a.target,a.config,a.interval,!0)
}V()
}return z
},G.prototype.isObject=function(a){return null!==a&&"object"==typeof a&&a.constructor===Object
},G.prototype.isNode=function(a){return"object"==typeof window.Node?a instanceof window.Node:a&&"object"==typeof a&&"number"==typeof a.nodeType&&"string"==typeof a.nodeName
},G.prototype.isNodeList=function(b){var a=Object.prototype.toString.call(b),c=/^\[object (HTMLCollection|NodeList|Object)\]$/;
return"object"==typeof window.NodeList?b instanceof window.NodeList:b&&"object"==typeof b&&c.test(a)&&"number"==typeof b.length&&(0===b.length||this.isNode(b[0]))
},G.prototype.forOwn=function(b,a){if(!this.isObject(b)){throw new TypeError('Expected "object", but received "'+typeof b+'".')
}for(var c in b){b.hasOwnProperty(c)&&a(c)
}},G.prototype.extend=function(b,a){return this.forOwn(a,function(c){this.isObject(a[c])?(b[c]&&this.isObject(b[c])||(b[c]={}),this.extend(b[c],a[c])):b[c]=a[c]
}.bind(this)),b
},G.prototype.extendClone=function(b,a){return this.extend(this.extend({},b),a)
},G.prototype.isMobile=function(){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
},k=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||function(a){window.setTimeout(a,1000/60)
},"function"==typeof define&&"object"==typeof define.amd&&define.amd?define(function(){return W
}):"undefined"!=typeof module&&module.exports?module.exports=W:window.ScrollReveal=W
}();
/*! jQuery & Zepto Lazy v1.7.4 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2016 Daniel 'Eisbehr' Kern */
;
!function(d,g){function f(U,ah,aa,Z,M){function ad(){q=d.devicePixelRatio>1,af(aa),ah.delay>=0&&setTimeout(function(){R(!0)
},ah.delay),(ah.delay<0||ah.combined)&&(Z.e=J(ah.throttle,function(a){"resize"===a.type&&(H=V=-1),R(a.all)
}),Z.a=function(a){af(a),aa.push.apply(aa,a)
},Z.g=function(){return aa=k(aa).filter(function(){return !k(this).data(ah.loadedName)
})
},Z.f=function(a){for(var m=0;
m<a.length;
m++){var l=aa.filter(a[m]);
l.length&&R(!1,l)
}},R(),k(ah.appendScroll).on("scroll."+M+" resize."+M,Z.e))
}function af(D){var r=ah.defaultImage,m=ah.placeholder,p=ah.imageBase,C=ah.srcsetAttribute,y=ah.loaderAttribute,B=ah._f||{};
D=k(D).filter(function(){var l=k(this),s=Y(this);
return !l.data(ah.handledName)&&(l.attr(ah.attribute)||l.attr(C)||l.attr(y)||B[s]!==g)
}).data("plugin_"+ah.name,U);
for(var E=0,z=D.length;
E<z;
E++){var a=k(D[E]),w=Y(D[E]),v=a.attr(ah.imageBaseAttribute)||p;
w==o&&v&&a.attr(C)&&a.attr(C,ag(a.attr(C),v)),B[w]===g||a.attr(y)||a.attr(y,B[w]),w==o&&r&&!a.attr(P)?a.attr(P,r):w==o||!m||a.css(n)&&"none"!=a.css(n)||a.css(n,"url('"+m+"')")
}}function R(F,A){if(!aa.length){return void (ah.autoDestroy&&U.destroy())
}for(var m=A||aa,r=!1,E=ah.imageBase||"",z=ah.srcsetAttribute,B=ah.handledName,I=0;
I<m.length;
I++){if(F||A||W(m[I])){var y=k(m[I]),w=Y(m[I]),C=y.attr(ah.attribute),D=y.attr(ah.imageBaseAttribute)||E,a=y.attr(ah.loaderAttribute);
y.data(B)||ah.visibleOnly&&!y.is(":visible")||!((C||y.attr(z))&&(w==o&&(D+C!=y.attr(P)||y.attr(z)!=y.attr(K))||w!=o&&D+C!=y.css(n))||a)||(r=!0,y.data(B,!0),ae(y,w,D,a))
}}r&&(aa=k(aa).filter(function(){return !k(this).data(B)
}))
}function ae(N,E,a,z){++t;
var v=function(){x("onError",N),X(),v=k.noop
};
x("beforeLoad",N);
var y=ah.attribute,L=ah.srcsetAttribute,D=ah.sizesAttribute,I=ah.retinaAttribute,O=ah.removeAttribute,F=ah.loadedName,p=N.attr(I);
if(z){var C=function(){O&&N.removeAttr(ah.loaderAttribute),N.data(F,!0),x(e,N),setTimeout(X,1),C=k.noop
};
N.off(G).one(G,v).one(Q,C),x(z,N,function(l){l?(N.off(Q),C()):(N.off(G),v())
})||N.trigger(G)
}else{var B=k(new Image);
B.one(G,v).one(Q,function(){N.hide(),E==o?N.attr(S,B.attr(S)).attr(K,B.attr(K)).attr(P,B.attr(P)):N.css(n,"url('"+B.attr(P)+"')"),N[ah.effect](ah.effectTime),O&&(N.removeAttr(y+" "+L+" "+I+" "+ah.imageBaseAttribute),D!==S&&N.removeAttr(D)),N.data(F,!0),x(e,N),B.remove(),X()
});
var w=(q&&p?p:N.attr(y))||"";
B.attr(S,N.attr(D)).attr(K,N.attr(L)).attr(P,w?a+w:null),B.complete&&B.trigger(Q)
}}function W(l){var p=l.getBoundingClientRect(),m=ah.scrollDirection,u=ah.threshold,a=ab()+u>p.top&&-u<p.bottom,s=ac()+u>p.left&&-u<p.right;
return"vertical"==m?a:"horizontal"==m?s:a&&s
}function ac(){return H>=0?H:H=k(d).width()
}function ab(){return V>=0?V:V=k(d).height()
}function Y(a){return a.tagName.toLowerCase()
}function ag(m,s){if(s){var p=m.split(",");
m="";
for(var l=0,u=p.length;
l<u;
l++){m+=s+p[l].trim()+(l!==u-1?",":"")
}}return m
}function J(l,m){var p,a=0;
return function(w,r){function s(){a=+new Date,m.call(U,w)
}var v=+new Date-a;
p&&clearTimeout(p),v>l||!ah.enableThrottle||r?s():p=setTimeout(s,l-v)
}
}function X(){--t,aa.length||t||x("onFinishedAll")
}function x(a,l,m){return !!(a=ah[a])&&(a.apply(U,[].slice.call(arguments,1)),!0)
}var t=0,H=-1,V=-1,q=!1,e="afterLoad",Q="load",G="error",o="img",P="src",K="srcset",S="sizes",n="background-image";
"event"==ah.bind||h?ad():k(d).on(Q+"."+M,ad)
}function b(m,q){var e=this,n=k.extend({},e.config,q),p={},r=n.name+"-"+ ++c;
return e.config=function(a,l){return l===g?n[a]:(n[a]=l,e)
},e.addItems=function(a){return p.a&&p.a("string"===k.type(a)?k(a):a),e
},e.getItems=function(){return p.g?p.g():{}
},e.update=function(a){return p.e&&p.e({},!a),e
},e.force=function(a){return p.f&&p.f("string"===k.type(a)?k(a):a),e
},e.loadAll=function(){return p.e&&p.e({all:!0},!0),e
},e.destroy=function(){return k(n.appendScroll).off("."+r,p.e),k(d).off("."+r),p={},g
},f(e,n,m,p,r),n.chainable?m:e
}var k=d.jQuery||d.Zepto,c=0,h=!1;
k.fn.Lazy=k.fn.lazy=function(a){return new b(this,a)
},k.Lazy=k.lazy=function(w,a,n){if(k.isFunction(a)&&(n=a,a=[]),k.isFunction(n)){w=k.isArray(w)?w:[w],a=k.isArray(a)?a:[a];
for(var e=b.prototype.config,m=e._f||(e._f={}),v=0,p=w.length;
v<p;
v++){(e[w[v]]===g||k.isFunction(e[w[v]]))&&(e[w[v]]=n)
}for(var q=0,x=a.length;
q<x;
q++){m[a[q]]=w[0]
}}},b.prototype.config={name:"lazy",chainable:!0,autoDestroy:!0,bind:"load",threshold:500,visibleOnly:!1,appendScroll:d,scrollDirection:"both",imageBase:null,defaultImage:"data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",placeholder:null,delay:-1,combined:!1,attribute:"data-src",srcsetAttribute:"data-srcset",sizesAttribute:"data-sizes",retinaAttribute:"data-retina",loaderAttribute:"data-loader",imageBaseAttribute:"data-imagebase",removeAttribute:!0,handledName:"handled",loadedName:"loaded",effect:"show",effectTime:0,enableThrottle:!0,throttle:250,beforeLoad:g,afterLoad:g,onError:g,onFinishedAll:g},k(d).on("load",function(){h=!0
})
}(window);
/*!
 * Responsive Tables v5.0.4 (http://gergeo.se/RWD-Table-Patterns)
 * This is an awesome solution for responsive tables with complex data.
 * Authors: Nadan Gergeo <nadan.gergeo@gmail.com> (www.gergeo.se) & Maggie Wachs (www.filamentgroup.com)
 * Licensed under MIT (https://github.com/nadangergeo/RWD-Table-Patterns/blob/master/LICENSE-MIT)
 */
(function(e){var c=function(h,g){var k=this;
this.options=g;
this.$tableWrapper=null;
this.$tableScrollWrapper=e(h);
this.$table=e(h).find("table");
if(this.$table.length!==1){throw new Error("Exactly one table is expected in a .table-responsive div.")
}this.$tableScrollWrapper.attr("data-pattern",this.options.pattern);
this.id=this.$table.prop("id")||this.$tableScrollWrapper.prop("id")||"id"+Math.random().toString(16).slice(2);
this.$tableClone=null;
this.$stickyTableHeader=null;
this.$thead=this.$table.find("thead");
this.$tbody=this.$table.find("tbody");
this.$hdrCells=this.$thead.find("th");
this.$bodyRows=this.$tbody.find("tr");
this.$btnToolbar=null;
this.$dropdownGroup=null;
this.$dropdownBtn=null;
this.$dropdownContainer=null;
this.$displayAllBtn=null;
this.$focusGroup=null;
this.$focusBtn=null;
this.displayAllTrigger="display-all-"+this.id+".responsive-table";
this.idPrefix=this.id+"-col-";
this.iOS=a();
this.wrapTable();
this.createButtonToolbar();
this.setupHdrCells();
this.setupStandardCells();
if(this.options.stickyTableHeader){this.createStickyTableHeader()
}if(this.$dropdownContainer.is(":empty")){this.$dropdownGroup.hide()
}this.makeSticky("resize");
e(window).bind("orientationchange resize "+this.displayAllTrigger,function(){k.$dropdownContainer.find("input").trigger("updateCheck");
e.proxy(k.updateSpanningCells(),k);
c.prototype.makeSticky("resize")
})
};
c.DEFAULTS={pattern:"priority-columns",stickyTableHeader:false,fixedNavbar:".navbar-fixed-top",addDisplayAllBtn:true,addFocusBtn:true,focusBtnIcon:"glyphicon glyphicon-screenshot"};
c.prototype.makeSticky=function(g){var p=e('ul.dropdown-menu li input[type="checkbox"]');
var n=null;
var h=false;
var m=e("#product-data:not(.fixed-column)");
var o=function(){p.each(function(){if(e(this).is(":checked")&&h==false){n=e(this).val();
h=true;
if(n=="on"){n="product-data-col-0"
}}})
};
var l=function(){e(".table.fixed-column").remove();
o();
var q=m.clone().insertBefore(m).addClass("fixed-column");
q.find('th:not("#'+n+'"),td:not([data-columns="'+n+'"])').remove();
var s=e(".table:not(.fixed-column)");
var r=s.find("#"+n+"").outerWidth();
e(".table.fixed-column").find("th, td").css("width",r-9);
if(q.find("input").attr("type")=="checkbox"){q.remove()
}};
var k=function(){e(".table.fixed-column tr").each(function(s){var t=e(this);
var r=m.find("tbody tr").eq(s-1);
var q=r[0].getBoundingClientRect().height;
t.css("height",q)
})
};
if(g=="resize"){l();
k()
}else{o();
k()
}};
c.prototype.wrapTable=function(){this.$tableScrollWrapper.wrap('<div class="table-wrapper"/>');
this.$tableWrapper=this.$tableScrollWrapper.parent()
};
c.prototype.createButtonToolbar=function(){var h=this;
this.$btnToolbar=e('<div class="btn-toolbar" />');
this.$dropdownGroup=e('<div class="btn-group dropdown-btn-group pull-right" />');
this.$dropdownBtn=e('<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Display <span class="caret"></span></button>');
this.$dropdownContainer=e('<ul class="dropdown-menu"/>');
if(this.options.addFocusBtn){this.$focusGroup=e('<div class="btn-group focus-btn-group" />');
this.$focusBtn=e('<button class="btn btn-default">Focus</button>');
if(this.options.focusBtnIcon){this.$focusBtn.prepend('<span class="'+this.options.focusBtnIcon+'"></span> ')
}this.$focusGroup.append(this.$focusBtn);
this.$btnToolbar.append(this.$focusGroup);
this.$focusBtn.click(function(){e.proxy(h.activateFocus(),h)
});
this.$bodyRows.click(function(){e.proxy(h.focusOnRow(e(this)),h)
})
}if(this.options.addDisplayAllBtn){this.$displayAllBtn=e('<li class="checkbox-row"><input type="checkbox" id="chkDisplayAll" /> <label>Show All</label></li><hr style="margin-top:10px;margin-bottom:10px;" />');
this.$dropdownContainer.append(this.$displayAllBtn);
if(this.$table.hasClass("display-all")){this.$displayAllBtn.addClass("btn-primary")
}var g=this.$displayAllBtn.find("input");
this.$displayAllBtn.click(function(){g.prop("checked",!g.prop("checked"));
g.trigger("change")
});
if(e("html").hasClass("lt-ie9")){g.click(function(){e(this).trigger("change")
})
}this.$displayAllBtn.find("input").click(function(k){k.stopPropagation()
}).change(function(){e.proxy(h.displayAll(null,true),h)
})
}this.$dropdownGroup.append(this.$dropdownBtn).append(this.$dropdownContainer);
this.$btnToolbar.append(this.$dropdownGroup);
this.$tableScrollWrapper.before(this.$btnToolbar)
};
c.prototype.clearAllFocus=function(){this.$bodyRows.removeClass("unfocused");
this.$bodyRows.removeClass("focused")
};
c.prototype.activateFocus=function(){this.clearAllFocus();
if(this.$focusBtn){this.$focusBtn.toggleClass("btn-primary")
}this.$table.toggleClass("focus-on")
};
c.prototype.focusOnRow=function(h){if(this.$table.hasClass("focus-on")){var g=e(h).hasClass("focused");
this.clearAllFocus();
if(!g){this.$bodyRows.addClass("unfocused");
e(h).addClass("focused")
}}};
c.prototype.displayAll=function(h,g){this.$table.toggleClass("display-all",h);
if(this.$tableClone){this.$tableClone.toggleClass("display-all",h)
}if(g){e(window).trigger(this.displayAllTrigger)
}};
c.prototype.preserveDisplayAll=function(){var g="table-cell";
if(e("html").hasClass("lt-ie9")){g="inline"
}e(this.$table).find("th, td").css("display",g);
if(this.$tableClone){e(this.$tableClone).find("th, td").css("display",g)
}};
c.prototype.createStickyTableHeader=function(){var g=this;
g.$tableClone=g.$table.clone();
g.$tableClone.prop("id",this.id+"-clone");
g.$tableClone.find("[id]").each(function(){e(this).prop("id",e(this).prop("id")+"-clone")
});
g.$tableClone.wrap('<div class="sticky-table-header"/>');
g.$stickyTableHeader=g.$tableClone.parent();
g.$stickyTableHeader.css("height",g.$thead.height()+2);
if(e("html").hasClass("lt-ie10")){g.$tableWrapper.prepend(g.$stickyTableHeader)
}else{g.$table.before(g.$stickyTableHeader)
}e(window).bind("scroll resize",function(){e.proxy(g.updateStickyTableHeader(),g)
});
e(g.$tableScrollWrapper).bind("scroll",function(){e.proxy(g.updateStickyTableHeader(),g)
})
};
c.prototype.updateStickyTableHeader=function(){var n=this,r=0,h=n.$table.offset().top,g=e(window).scrollTop()-1,l=n.$table.height()-n.$stickyTableHeader.height(),k=(g+e(window).height())-e(document).height(),o=!n.iOS,m=0;
if(e(n.options.fixedNavbar).length){var p=e(n.options.fixedNavbar).first();
m=p.height();
g=g+m
}var s=(g>h)&&(g<h+n.$table.height());
if(o){n.$stickyTableHeader.scrollLeft(n.$tableScrollWrapper.scrollLeft());
n.$stickyTableHeader.addClass("fixed-solution");
r=m-1;
if(((g-h)>l)){r-=((g-h)-l);
n.$stickyTableHeader.addClass("border-radius-fix")
}else{n.$stickyTableHeader.removeClass("border-radius-fix")
}if(s){n.$stickyTableHeader.css({visibility:"visible",top:r+"px",width:n.$tableScrollWrapper.innerWidth()+"px"});
return
}else{n.$stickyTableHeader.css({visibility:"hidden",width:"auto"})
}}else{n.$stickyTableHeader.removeClass("fixed-solution");
var q=400;
r=g-h-1;
if(r<0){r=0
}else{if(r>l){r=l
}}if(k>0){r=r-k
}if(s){n.$stickyTableHeader.css({visibility:"visible"});
n.$stickyTableHeader.animate({top:r+"px"},q);
n.$thead.css({visibility:"hidden"})
}else{n.$stickyTableHeader.animate({top:"0"},q,function(){n.$thead.css({visibility:"visible"});
n.$stickyTableHeader.css({visibility:"hidden"})
})
}}};
c.prototype.setupHdrCells=function(){var g=this;
g.$hdrCells.each(function(k){var m=e(this),o=m.prop("id"),n=m.text();
if(!o){o=g.idPrefix+k;
m.prop("id",o)
}if(n===""){n=m.attr("data-col-name")
}if(m.is("[data-priority]")){var h=e('<li class="checkbox-row"><input type="checkbox" name="toggle-'+o+'" id="toggle-'+o+'" value="'+o+'" /> <label for="toggle-'+o+'">'+n+"</label></li>");
var l=h.find("input");
g.$dropdownContainer.append(h);
h.click(function(){l.prop("checked",!l.prop("checked"));
l.trigger("change")
});
if(e("html").hasClass("lt-ie9")){l.click(function(){e(this).trigger("change")
})
}h.find("label").click(function(p){p.stopPropagation()
});
h.find("input").click(function(p){p.stopPropagation()
}).change(function(){var r=e(this),s=r.val(),p=g.$tableWrapper.find("#"+s+", #"+s+"-clone, [data-columns~="+s+"]");
if(g.$table.hasClass("display-all")){e.proxy(g.preserveDisplayAll(),g);
g.$table.removeClass("display-all");
if(g.$tableClone){g.$tableClone.removeClass("display-all")
}g.$displayAllBtn.removeClass("btn-primary");
var q=g.$displayAllBtn.find("input");
q.prop("checked",false);
c.prototype.makeSticky("resize")
}p.each(function(){var t=e(this);
if(r.is(":checked")){if(t.css("display")!=="none"){t.prop("colSpan",parseInt(t.prop("colSpan"))+1)
}t.show()
}else{if(parseInt(t.prop("colSpan"))>1){t.prop("colSpan",parseInt(t.prop("colSpan"))-1)
}else{t.hide()
}}});
c.prototype.makeSticky("resize")
}).bind("updateCheck",function(){if(m.css("display")!=="none"){e(this).prop("checked",true)
}else{e(this).prop("checked",false)
}}).trigger("updateCheck")
}})
};
c.prototype.setupStandardCells=function(){var g=this;
g.$bodyRows.each(function(){var h=0;
e(this).find("th, td").each(function(){var q=e(this);
var n="";
var p=q.prop("colSpan");
var m=0;
for(var o=h;
o<(h+p);
o++){n=n+" "+g.idPrefix+o;
var l=g.$tableScrollWrapper.find("#"+g.idPrefix+o);
var r=l.attr("data-priority");
if(r){q.attr("data-priority",r)
}if(l.css("display")==="none"){m++
}}if(p>1){q.addClass("spn-cell");
if(m!==p){q.show()
}else{q.hide()
}}q.prop("colSpan",Math.max((p-m),1));
n=n.substring(1);
q.attr("data-columns",n);
h=h+p
})
})
};
c.prototype.updateSpanningCells=function(){var g=this;
g.$table.find(".spn-cell").each(function(){var n=e(this);
var k=n.attr("data-columns").split(" ");
var m=k.length;
var h=0;
for(var l=0;
l<m;
l++){if(e("#"+k[l]).css("display")==="none"){h++
}}if(h!==m){n.show()
}else{n.hide()
}n.prop("colSpan",Math.max((m-h),1))
})
};
var b=e.fn.responsiveTable;
e.fn.responsiveTable=function(g){return this.each(function(){var l=e(this);
var k=l.data("responsiveTable");
var h=e.extend({},c.DEFAULTS,l.data(),typeof g==="object"&&g);
if(h.pattern===""){return
}if(!k){l.data("responsiveTable",(k=new c(this,h)))
}if(typeof g==="string"){k[g]()
}})
};
e.fn.responsiveTable.Constructor=c;
e.fn.responsiveTable.noConflict=function(){e.fn.responsiveTable=b;
return this
};
e(document).on("ready.responsive-table.data-api",function(){e("[data-pattern]").each(function(){var g=e(this);
g.responsiveTable(g.data())
})
});
e(document).on("click.dropdown.data-api",".dropdown-menu .checkbox-row",function(g){g.stopPropagation()
});
function f(){return(typeof window.matchMedia!=="undefined"||typeof window.msMatchMedia!=="undefined"||typeof window.styleMedia!=="undefined")
}function d(){return"ontouchstart" in window
}function a(){return !!(navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i))
}e(document).ready(function(){e("html").removeClass("no-js").addClass("js");
if(f()){e("html").addClass("mq")
}else{e("html").addClass("no-mq")
}if(d()){e("html").addClass("touch")
}else{e("html").addClass("no-touch")
}})
})(jQuery);
/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
;
if("undefined"==typeof jQuery){throw new Error("Bootstrap's JavaScript requires jQuery")
}+function(d){var c=d.fn.jquery.split(" ")[0].split(".");
if(c[0]<2&&c[1]<9||1==c[0]&&9==c[1]&&c[2]<1||c[0]>2){throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")
}}(jQuery),+function(d){function c(){var f=document.createElement("bootstrap"),e={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};
for(var g in e){if(void 0!==f.style[g]){return{end:e[g]}
}}return !1
}d.fn.emulateTransitionEnd=function(a){var h=!1,g=this;
d(this).one("bsTransitionEnd",function(){h=!0
});
var f=function(){h||d(g).trigger(d.support.transition.end)
};
return setTimeout(f,a),this
},d(function(){d.support.transition=c(),d.support.transition&&(d.event.special.bsTransitionEnd={bindType:d.support.transition.end,delegateType:d.support.transition.end,handle:function(a){return d(a.target).is(this)?a.handleObj.handler.apply(this,arguments):void 0
}})
})
}(jQuery),+function(g){function f(a){return this.each(function(){var d=g(this),b=d.data("bs.alert");
b||d.data("bs.alert",b=new k(this)),"string"==typeof a&&b[a].call(d)
})
}var l='[data-dismiss="alert"]',k=function(a){g(a).on("click",l,this.close)
};
k.VERSION="3.3.6",k.TRANSITION_DURATION=150,k.prototype.close=function(a){function o(){d.detach().trigger("closed.bs.alert").remove()
}var n=g(this),m=n.attr("data-target");
m||(m=n.attr("href"),m=m&&m.replace(/.*(?=#[^\s]*$)/,""));
var d=g(m);
a&&a.preventDefault(),d.length||(d=n.closest(".alert")),d.trigger(a=g.Event("close.bs.alert")),a.isDefaultPrevented()||(d.removeClass("in"),g.support.transition&&d.hasClass("fade")?d.one("bsTransitionEnd",o).emulateTransitionEnd(k.TRANSITION_DURATION):o())
};
var h=g.fn.alert;
g.fn.alert=f,g.fn.alert.Constructor=k,g.fn.alert.noConflict=function(){return g.fn.alert=h,this
},g(document).on("click.bs.alert.data-api",l,k.prototype.close)
}(jQuery),+function(f){function e(a){return this.each(function(){var k=f(this),c=k.data("bs.button"),b="object"==typeof a&&a;
c||k.data("bs.button",c=new h(this,b)),"toggle"==a?c.toggle():a&&c.setState(a)
})
}var h=function(a,c){this.$element=f(a),this.options=f.extend({},h.DEFAULTS,c),this.isLoading=!1
};
h.VERSION="3.3.6",h.DEFAULTS={loadingText:"loading..."},h.prototype.setState=function(a){var n="disabled",m=this.$element,l=m.is("input")?"val":"html",k=m.data();
a+="Text",null==k.resetText&&m.data("resetText",m[l]()),setTimeout(f.proxy(function(){m[l](null==k[a]?this.options[a]:k[a]),"loadingText"==a?(this.isLoading=!0,m.addClass(n).attr(n,n)):this.isLoading&&(this.isLoading=!1,m.removeClass(n).removeAttr(n))
},this),0)
},h.prototype.toggle=function(){var k=!0,d=this.$element.closest('[data-toggle="buttons"]');
if(d.length){var l=this.$element.find("input");
"radio"==l.prop("type")?(l.prop("checked")&&(k=!1),d.find(".active").removeClass("active"),this.$element.addClass("active")):"checkbox"==l.prop("type")&&(l.prop("checked")!==this.$element.hasClass("active")&&(k=!1),this.$element.toggleClass("active")),l.prop("checked",this.$element.hasClass("active")),k&&l.trigger("change")
}else{this.$element.attr("aria-pressed",!this.$element.hasClass("active")),this.$element.toggleClass("active")
}};
var g=f.fn.button;
f.fn.button=e,f.fn.button.Constructor=h,f.fn.button.noConflict=function(){return f.fn.button=g,this
},f(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(b){var a=f(b.target);
a.hasClass("btn")||(a=a.closest(".btn")),e.call(a,"toggle"),f(b.target).is('input[type="radio"]')||f(b.target).is('input[type="checkbox"]')||b.preventDefault()
}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(a){f(a.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(a.type))
})
}(jQuery),+function(g){function f(a){return this.each(function(){var n=g(this),m=n.data("bs.carousel"),c=g.extend({},l.DEFAULTS,n.data(),"object"==typeof a&&a),b="string"==typeof a?a:c.slide;
m||n.data("bs.carousel",m=new l(this,c)),"number"==typeof a?m.to(a):b?m[b]():c.interval&&m.pause().cycle()
})
}var l=function(a,d){this.$element=g(a),this.$indicators=this.$element.find(".carousel-indicators"),this.options=d,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",g.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart" in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",g.proxy(this.pause,this)).on("mouseleave.bs.carousel",g.proxy(this.cycle,this))
};
l.VERSION="3.3.6",l.TRANSITION_DURATION=600,l.DEFAULTS={interval:5000,pause:"hover",wrap:!0,keyboard:!0},l.prototype.keydown=function(b){if(!/input|textarea/i.test(b.target.tagName)){switch(b.which){case 37:this.prev();
break;
case 39:this.next();
break;
default:return
}b.preventDefault()
}},l.prototype.cycle=function(a){return a||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(g.proxy(this.next,this),this.options.interval)),this
},l.prototype.getItemIndex=function(b){return this.$items=b.parent().children(".item"),this.$items.index(b||this.$active)
},l.prototype.getItemForDirection=function(n,m){var r=this.getItemIndex(m),q="prev"==n&&0===r||"next"==n&&r==this.$items.length-1;
if(q&&!this.options.wrap){return m
}var p="prev"==n?-1:1,o=(r+p)%this.$items.length;
return this.$items.eq(o)
},l.prototype.to=function(e){var d=this,m=this.getItemIndex(this.$active=this.$element.find(".item.active"));
return e>this.$items.length-1||0>e?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){d.to(e)
}):m==e?this.pause().cycle():this.slide(e>m?"next":"prev",this.$items.eq(e))
},l.prototype.pause=function(a){return a||(this.paused=!0),this.$element.find(".next, .prev").length&&g.support.transition&&(this.$element.trigger(g.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this
},l.prototype.next=function(){return this.sliding?void 0:this.slide("next")
},l.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")
},l.prototype.slide=function(v,u){var t=this.$element.find(".item.active"),s=u||this.getItemForDirection(v,t),r=this.interval,q="next"==v?"left":"right",p=this;
if(s.hasClass("active")){return this.sliding=!1
}var o=s[0],n=g.Event("slide.bs.carousel",{relatedTarget:o,direction:q});
if(this.$element.trigger(n),!n.isDefaultPrevented()){if(this.sliding=!0,r&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");
var c=g(this.$indicators.children()[this.getItemIndex(s)]);
c&&c.addClass("active")
}var a=g.Event("slid.bs.carousel",{relatedTarget:o,direction:q});
return g.support.transition&&this.$element.hasClass("slide")?(s.addClass(v),s[0].offsetWidth,t.addClass(q),s.addClass(q),t.one("bsTransitionEnd",function(){s.removeClass([v,q].join(" ")).addClass("active"),t.removeClass(["active",q].join(" ")),p.sliding=!1,setTimeout(function(){p.$element.trigger(a)
},0)
}).emulateTransitionEnd(l.TRANSITION_DURATION)):(t.removeClass("active"),s.addClass("active"),this.sliding=!1,this.$element.trigger(a)),r&&this.cycle(),this
}};
var k=g.fn.carousel;
g.fn.carousel=f,g.fn.carousel.Constructor=l,g.fn.carousel.noConflict=function(){return g.fn.carousel=k,this
};
var h=function(p){var o,n=g(this),m=g(n.attr("data-target")||(o=n.attr("href"))&&o.replace(/.*(?=#[^\s]+$)/,""));
if(m.hasClass("carousel")){var b=g.extend({},m.data(),n.data()),a=n.attr("data-slide-to");
a&&(b.interval=!1),f.call(m,b),a&&m.data("bs.carousel").to(a),p.preventDefault()
}};
g(document).on("click.bs.carousel.data-api","[data-slide]",h).on("click.bs.carousel.data-api","[data-slide-to]",h),g(window).on("load",function(){g('[data-ride="carousel"]').each(function(){var a=g(this);
f.call(a,a.data())
})
})
}(jQuery),+function(g){function f(a){var m,e=a.attr("data-target")||(m=a.attr("href"))&&m.replace(/.*(?=#[^\s]+$)/,"");
return g(e)
}function l(a){return this.each(function(){var m=g(this),d=m.data("bs.collapse"),b=g.extend({},k.DEFAULTS,m.data(),"object"==typeof a&&a);
!d&&b.toggle&&/show|hide/.test(a)&&(b.toggle=!1),d||m.data("bs.collapse",d=new k(this,b)),"string"==typeof a&&d[a]()
})
}var k=function(a,d){this.$element=g(a),this.options=g.extend({},k.DEFAULTS,d),this.$trigger=g('[data-toggle="collapse"][href="#'+a.id+'"],[data-toggle="collapse"][data-target="#'+a.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()
};
k.VERSION="3.3.6",k.TRANSITION_DURATION=350,k.DEFAULTS={toggle:!0},k.prototype.dimension=function(){var b=this.$element.hasClass("width");
return b?"width":"height"
},k.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var a,o=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");
if(!(o&&o.length&&(a=o.data("bs.collapse"),a&&a.transitioning))){var n=g.Event("show.bs.collapse");
if(this.$element.trigger(n),!n.isDefaultPrevented()){o&&o.length&&(l.call(o,"hide"),a||o.data("bs.collapse",null));
var m=this.dimension();
this.$element.removeClass("collapse").addClass("collapsing")[m](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;
var d=function(){this.$element.removeClass("collapsing").addClass("collapse in")[m](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")
};
if(!g.support.transition){return d.call(this)
}var c=g.camelCase(["scroll",m].join("-"));
this.$element.one("bsTransitionEnd",g.proxy(d,this)).emulateTransitionEnd(k.TRANSITION_DURATION)[m](this.$element[0][c])
}}}},k.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var a=g.Event("hide.bs.collapse");
if(this.$element.trigger(a),!a.isDefaultPrevented()){var m=this.dimension();
this.$element[m](this.$element[m]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;
var d=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
};
return g.support.transition?void this.$element[m](0).one("bsTransitionEnd",g.proxy(d,this)).emulateTransitionEnd(k.TRANSITION_DURATION):d.call(this)
}}},k.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()
},k.prototype.getParent=function(){return g(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(g.proxy(function(m,b){var a=g(b);
this.addAriaAndCollapsedClass(f(a),a)
},this)).end()
},k.prototype.addAriaAndCollapsedClass=function(e,d){var m=e.hasClass("in");
e.attr("aria-expanded",m),d.toggleClass("collapsed",!m).attr("aria-expanded",m)
};
var h=g.fn.collapse;
g.fn.collapse=l,g.fn.collapse.Constructor=k,g.fn.collapse.noConflict=function(){return g.fn.collapse=h,this
},g(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(n){var m=g(this);
m.attr("data-target")||n.preventDefault();
var c=f(m),b=c.data("bs.collapse"),a=b?"toggle":m.data();
l.call(c,a)
})
}(jQuery),+function(l){function k(a){var f=a.attr("data-target");
f||(f=a.attr("href"),f=f&&/#[A-Za-z]/.test(f)&&f.replace(/.*(?=#[^\s]*$)/,""));
var e=f&&l(f);
return e&&e.length?e:a.parent()
}function r(a){a&&3===a.which||(l(p).remove(),l(o).each(function(){var g=l(this),c=k(g),b={relatedTarget:this};
c.hasClass("open")&&(a&&"click"==a.type&&/input|textarea/i.test(a.target.tagName)&&l.contains(c[0],a.target)||(c.trigger(a=l.Event("hide.bs.dropdown",b)),a.isDefaultPrevented()||(g.attr("aria-expanded","false"),c.removeClass("open").trigger(l.Event("hidden.bs.dropdown",b)))))
}))
}function q(a){return this.each(function(){var e=l(this),b=e.data("bs.dropdown");
b||e.data("bs.dropdown",b=new n(this)),"string"==typeof a&&b[a].call(e)
})
}var p=".dropdown-backdrop",o='[data-toggle="dropdown"]',n=function(a){l(a).on("click.bs.dropdown",this.toggle)
};
n.VERSION="3.3.6",n.prototype.toggle=function(t){var s=l(this);
if(!s.is(".disabled, :disabled")){var c=k(s),b=c.hasClass("open");
if(r(),!b){"ontouchstart" in document.documentElement&&!c.closest(".navbar-nav").length&&l(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(l(this)).on("click",r);
var a={relatedTarget:this};
if(c.trigger(t=l.Event("show.bs.dropdown",a)),t.isDefaultPrevented()){return
}s.trigger("focus").attr("aria-expanded","true"),c.toggleClass("open").trigger(l.Event("shown.bs.dropdown",a))
}return !1
}},n.prototype.keydown=function(v){if(/(38|40|27|32)/.test(v.which)&&!/input|textarea/i.test(v.target.tagName)){var u=l(this);
if(v.preventDefault(),v.stopPropagation(),!u.is(".disabled, :disabled")){var t=k(u),s=t.hasClass("open");
if(!s&&27!=v.which||s&&27==v.which){return 27==v.which&&t.find(o).trigger("focus"),u.trigger("click")
}var f=" li:not(.disabled):visible a",b=t.find(".dropdown-menu"+f);
if(b.length){var a=b.index(v.target);
38==v.which&&a>0&&a--,40==v.which&&a<b.length-1&&a++,~a||(a=0),b.eq(a).trigger("focus")
}}}};
var m=l.fn.dropdown;
l.fn.dropdown=q,l.fn.dropdown.Constructor=n,l.fn.dropdown.noConflict=function(){return l.fn.dropdown=m,this
},l(document).on("click.bs.dropdown.data-api",r).on("click.bs.dropdown.data-api",".dropdown form",function(b){b.stopPropagation()
}).on("click.bs.dropdown.data-api",o,n.prototype.toggle).on("keydown.bs.dropdown.data-api",o,n.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",n.prototype.keydown)
}(jQuery),+function(f){function e(a,c){return this.each(function(){var k=f(this),d=k.data("bs.modal"),b=f.extend({},h.DEFAULTS,k.data(),"object"==typeof a&&a);
d||k.data("bs.modal",d=new h(this,b)),"string"==typeof a?d[a](c):b.show&&d.show(c)
})
}var h=function(a,d){this.options=d,this.$body=f(document.body),this.$element=f(a),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,f.proxy(function(){this.$element.trigger("loaded.bs.modal")
},this))
};
h.VERSION="3.3.6",h.TRANSITION_DURATION=300,h.BACKDROP_TRANSITION_DURATION=150,h.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},h.prototype.toggle=function(b){return this.isShown?this.hide():this.show(b)
},h.prototype.show=function(a){var k=this,c=f.Event("show.bs.modal",{relatedTarget:a});
this.$element.trigger(c),this.isShown||c.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',f.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){k.$element.one("mouseup.dismiss.bs.modal",function(d){f(d.target).is(k.$element)&&(k.ignoreBackdropClick=!0)
})
}),this.backdrop(function(){var d=f.support.transition&&k.$element.hasClass("fade");
k.$element.parent().length||k.$element.appendTo(k.$body),k.$element.show().scrollTop(0),k.adjustDialog(),d&&k.$element[0].offsetWidth,k.$element.addClass("in"),k.enforceFocus();
var b=f.Event("shown.bs.modal",{relatedTarget:a});
d?k.$dialog.one("bsTransitionEnd",function(){k.$element.trigger("focus").trigger(b)
}).emulateTransitionEnd(h.TRANSITION_DURATION):k.$element.trigger("focus").trigger(b)
}))
},h.prototype.hide=function(a){a&&a.preventDefault(),a=f.Event("hide.bs.modal"),this.$element.trigger(a),this.isShown&&!a.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),f(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),f.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",f.proxy(this.hideModal,this)).emulateTransitionEnd(h.TRANSITION_DURATION):this.hideModal())
},h.prototype.enforceFocus=function(){f(document).off("focusin.bs.modal").on("focusin.bs.modal",f.proxy(function(b){this.$element[0]===b.target||this.$element.has(b.target).length||this.$element.trigger("focus")
},this))
},h.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",f.proxy(function(b){27==b.which&&this.hide()
},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")
},h.prototype.resize=function(){this.isShown?f(window).on("resize.bs.modal",f.proxy(this.handleUpdate,this)):f(window).off("resize.bs.modal")
},h.prototype.hideModal=function(){var b=this;
this.$element.hide(),this.backdrop(function(){b.$body.removeClass("modal-open"),b.resetAdjustments(),b.resetScrollbar(),b.$element.trigger("hidden.bs.modal")
})
},h.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null
},h.prototype.backdrop=function(a){var m=this,l=this.$element.hasClass("fade")?"fade":"";
if(this.isShown&&this.options.backdrop){var k=f.support.transition&&l;
if(this.$backdrop=f(document.createElement("div")).addClass("modal-backdrop "+l).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",f.proxy(function(b){return this.ignoreBackdropClick?void (this.ignoreBackdropClick=!1):void (b.target===b.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))
},this)),k&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!a){return
}k?this.$backdrop.one("bsTransitionEnd",a).emulateTransitionEnd(h.BACKDROP_TRANSITION_DURATION):a()
}else{if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");
var c=function(){m.removeBackdrop(),a&&a()
};
f.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",c).emulateTransitionEnd(h.BACKDROP_TRANSITION_DURATION):c()
}else{a&&a()
}}},h.prototype.handleUpdate=function(){this.adjustDialog()
},h.prototype.adjustDialog=function(){var b=this.$element[0].scrollHeight>document.documentElement.clientHeight;
this.$element.css({paddingLeft:!this.bodyIsOverflowing&&b?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!b?this.scrollbarWidth:""})
},h.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})
},h.prototype.checkScrollbar=function(){var d=window.innerWidth;
if(!d){var c=document.documentElement.getBoundingClientRect();
d=c.right-Math.abs(c.left)
}this.bodyIsOverflowing=document.body.clientWidth<d,this.scrollbarWidth=this.measureScrollbar()
},h.prototype.setScrollbar=function(){var b=parseInt(this.$body.css("padding-right")||0,10);
this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",b+this.scrollbarWidth)
},h.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)
},h.prototype.measureScrollbar=function(){var d=document.createElement("div");
d.className="modal-scrollbar-measure",this.$body.append(d);
var c=d.offsetWidth-d.clientWidth;
return this.$body[0].removeChild(d),c
};
var g=f.fn.modal;
f.fn.modal=e,f.fn.modal.Constructor=h,f.fn.modal.noConflict=function(){return f.fn.modal=g,this
},f(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(m){var l=f(this),k=l.attr("href"),b=f(l.attr("data-target")||k&&k.replace(/.*(?=#[^\s]+$)/,"")),a=b.data("bs.modal")?"toggle":f.extend({remote:!/#/.test(k)&&k},b.data(),l.data());
l.is("a")&&m.preventDefault(),b.one("show.bs.modal",function(c){c.isDefaultPrevented()||b.one("hidden.bs.modal",function(){l.is(":visible")&&l.trigger("focus")
})
}),e.call(b,a,this)
})
}(jQuery),+function(f){function e(a){return this.each(function(){var k=f(this),c=k.data("bs.tooltip"),b="object"==typeof a&&a;
(c||!/destroy|hide/.test(a))&&(c||k.data("bs.tooltip",c=new h(this,b)),"string"==typeof a&&c[a]())
})
}var h=function(d,c){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.inState=null,this.init("tooltip",d,c)
};
h.VERSION="3.3.6",h.TRANSITION_DURATION=150,h.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},h.prototype.init=function(a,q,p){if(this.enabled=!0,this.type=a,this.$element=f(q),this.options=this.getOptions(p),this.$viewport=this.options.viewport&&f(f.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0] instanceof document.constructor&&!this.options.selector){throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!")
}for(var o=this.options.trigger.split(" "),n=o.length;
n--;
){var m=o[n];
if("click"==m){this.$element.on("click."+this.type,this.options.selector,f.proxy(this.toggle,this))
}else{if("manual"!=m){var l="hover"==m?"mouseenter":"focusin",k="hover"==m?"mouseleave":"focusout";
this.$element.on(l+"."+this.type,this.options.selector,f.proxy(this.enter,this)),this.$element.on(k+"."+this.type,this.options.selector,f.proxy(this.leave,this))
}}}this.options.selector?this._options=f.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()
},h.prototype.getDefaults=function(){return h.DEFAULTS
},h.prototype.getOptions=function(a){return a=f.extend({},this.getDefaults(),this.$element.data(),a),a.delay&&"number"==typeof a.delay&&(a.delay={show:a.delay,hide:a.delay}),a
},h.prototype.getDelegateOptions=function(){var a={},d=this.getDefaults();
return this._options&&f.each(this._options,function(b,c){d[b]!=c&&(a[b]=c)
}),a
},h.prototype.enter=function(a){var d=a instanceof this.constructor?a:f(a.currentTarget).data("bs."+this.type);
return d||(d=new this.constructor(a.currentTarget,this.getDelegateOptions()),f(a.currentTarget).data("bs."+this.type,d)),a instanceof f.Event&&(d.inState["focusin"==a.type?"focus":"hover"]=!0),d.tip().hasClass("in")||"in"==d.hoverState?void (d.hoverState="in"):(clearTimeout(d.timeout),d.hoverState="in",d.options.delay&&d.options.delay.show?void (d.timeout=setTimeout(function(){"in"==d.hoverState&&d.show()
},d.options.delay.show)):d.show())
},h.prototype.isInStateTrue=function(){for(var b in this.inState){if(this.inState[b]){return !0
}}return !1
},h.prototype.leave=function(a){var d=a instanceof this.constructor?a:f(a.currentTarget).data("bs."+this.type);
return d||(d=new this.constructor(a.currentTarget,this.getDelegateOptions()),f(a.currentTarget).data("bs."+this.type,d)),a instanceof f.Event&&(d.inState["focusout"==a.type?"focus":"hover"]=!1),d.isInStateTrue()?void 0:(clearTimeout(d.timeout),d.hoverState="out",d.options.delay&&d.options.delay.hide?void (d.timeout=setTimeout(function(){"out"==d.hoverState&&d.hide()
},d.options.delay.hide)):d.hide())
},h.prototype.show=function(){var D=f.Event("show.bs."+this.type);
if(this.hasContent()&&this.enabled){this.$element.trigger(D);
var C=f.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);
if(D.isDefaultPrevented()||!C){return
}var B=this,A=this.tip(),z=this.getUID(this.type);
this.setContent(),A.attr("id",z),this.$element.attr("aria-describedby",z),this.options.animation&&A.addClass("fade");
var y="function"==typeof this.options.placement?this.options.placement.call(this,A[0],this.$element[0]):this.options.placement,x=/\s?auto?\s?/i,w=x.test(y);
w&&(y=y.replace(x,"")||"top"),A.detach().css({top:0,left:0,display:"block"}).addClass(y).data("bs."+this.type,this),this.options.container?A.appendTo(this.options.container):A.insertAfter(this.$element),this.$element.trigger("inserted.bs."+this.type);
var v=this.getPosition(),u=A[0].offsetWidth,t=A[0].offsetHeight;
if(w){var s=y,r=this.getPosition(this.$viewport);
y="bottom"==y&&v.bottom+t>r.bottom?"top":"top"==y&&v.top-t<r.top?"bottom":"right"==y&&v.right+u>r.width?"left":"left"==y&&v.left-u<r.left?"right":y,A.removeClass(s).addClass(y)
}var c=this.getCalculatedOffset(y,v,u,t);
this.applyPlacement(c,y);
var a=function(){var b=B.hoverState;
B.$element.trigger("shown.bs."+B.type),B.hoverState=null,"out"==b&&B.leave(B)
};
f.support.transition&&this.$tip.hasClass("fade")?A.one("bsTransitionEnd",a).emulateTransitionEnd(h.TRANSITION_DURATION):a()
}},h.prototype.applyPlacement=function(z,y){var x=this.tip(),w=x[0].offsetWidth,v=x[0].offsetHeight,u=parseInt(x.css("margin-top"),10),t=parseInt(x.css("margin-left"),10);
isNaN(u)&&(u=0),isNaN(t)&&(t=0),z.top+=u,z.left+=t,f.offset.setOffset(x[0],f.extend({using:function(b){x.css({top:Math.round(b.top),left:Math.round(b.left)})
}},z),0),x.addClass("in");
var s=x[0].offsetWidth,r=x[0].offsetHeight;
"top"==y&&r!=v&&(z.top=z.top+v-r);
var q=this.getViewportAdjustedDelta(y,z,s,r);
q.left?z.left+=q.left:z.top+=q.top;
var p=/top|bottom/.test(y),o=p?2*q.left-w+s:2*q.top-v+r,a=p?"offsetWidth":"offsetHeight";
x.offset(z),this.replaceArrow(o,x[0][a],p)
},h.prototype.replaceArrow=function(k,d,l){this.arrow().css(l?"left":"top",50*(1-k/d)+"%").css(l?"top":"left","")
},h.prototype.setContent=function(){var d=this.tip(),c=this.getTitle();
d.find(".tooltip-inner")[this.options.html?"html":"text"](c),d.removeClass("fade in top bottom left right")
},h.prototype.hide=function(a){function m(){"in"!=l.hoverState&&k.detach(),l.$element.removeAttr("aria-describedby").trigger("hidden.bs."+l.type),a&&a()
}var l=this,k=f(this.$tip),c=f.Event("hide.bs."+this.type);
return this.$element.trigger(c),c.isDefaultPrevented()?void 0:(k.removeClass("in"),f.support.transition&&k.hasClass("fade")?k.one("bsTransitionEnd",m).emulateTransitionEnd(h.TRANSITION_DURATION):m(),this.hoverState=null,this)
},h.prototype.fixTitle=function(){var b=this.$element;
(b.attr("title")||"string"!=typeof b.attr("data-original-title"))&&b.attr("data-original-title",b.attr("title")||"").attr("title","")
},h.prototype.hasContent=function(){return this.getTitle()
},h.prototype.getPosition=function(a){a=a||this.$element;
var p=a[0],o="BODY"==p.tagName,n=p.getBoundingClientRect();
null==n.width&&(n=f.extend({},n,{width:n.right-n.left,height:n.bottom-n.top}));
var m=o?{top:0,left:0}:a.offset(),l={scroll:o?document.documentElement.scrollTop||document.body.scrollTop:a.scrollTop()},k=o?{width:f(window).width(),height:f(window).height()}:null;
return f.extend({},n,l,k,m)
},h.prototype.getCalculatedOffset=function(l,k,n,m){return"bottom"==l?{top:k.top+k.height,left:k.left+k.width/2-n/2}:"top"==l?{top:k.top-m,left:k.left+k.width/2-n/2}:"left"==l?{top:k.top+k.height/2-m/2,left:k.left-n}:{top:k.top+k.height/2-m/2,left:k.left+k.width}
},h.prototype.getViewportAdjustedDelta=function(v,u,t,s){var r={top:0,left:0};
if(!this.$viewport){return r
}var q=this.options.viewport&&this.options.viewport.padding||0,p=this.getPosition(this.$viewport);
if(/right|left/.test(v)){var o=u.top-q-p.scroll,n=u.top+q-p.scroll+s;
o<p.top?r.top=p.top-o:n>p.top+p.height&&(r.top=p.top+p.height-n)
}else{var m=u.left-q,l=u.left+q+t;
m<p.left?r.left=p.left-m:l>p.right&&(r.left=p.left+p.width-l)
}return r
},h.prototype.getTitle=function(){var k,d=this.$element,l=this.options;
return k=d.attr("data-original-title")||("function"==typeof l.title?l.title.call(d[0]):l.title)
},h.prototype.getUID=function(b){do{b+=~~(1000000*Math.random())
}while(document.getElementById(b));
return b
},h.prototype.tip=function(){if(!this.$tip&&(this.$tip=f(this.options.template),1!=this.$tip.length)){throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!")
}return this.$tip
},h.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")
},h.prototype.enable=function(){this.enabled=!0
},h.prototype.disable=function(){this.enabled=!1
},h.prototype.toggleEnabled=function(){this.enabled=!this.enabled
},h.prototype.toggle=function(a){var d=this;
a&&(d=f(a.currentTarget).data("bs."+this.type),d||(d=new this.constructor(a.currentTarget,this.getDelegateOptions()),f(a.currentTarget).data("bs."+this.type,d))),a?(d.inState.click=!d.inState.click,d.isInStateTrue()?d.enter(d):d.leave(d)):d.tip().hasClass("in")?d.leave(d):d.enter(d)
},h.prototype.destroy=function(){var b=this;
clearTimeout(this.timeout),this.hide(function(){b.$element.off("."+b.type).removeData("bs."+b.type),b.$tip&&b.$tip.detach(),b.$tip=null,b.$arrow=null,b.$viewport=null
})
};
var g=f.fn.tooltip;
f.fn.tooltip=e,f.fn.tooltip.Constructor=h,f.fn.tooltip.noConflict=function(){return f.fn.tooltip=g,this
}
}(jQuery),+function(f){function e(a){return this.each(function(){var k=f(this),c=k.data("bs.popover"),b="object"==typeof a&&a;
(c||!/destroy|hide/.test(a))&&(c||k.data("bs.popover",c=new h(this,b)),"string"==typeof a&&c[a]())
})
}var h=function(d,c){this.init("popover",d,c)
};
if(!f.fn.tooltip){throw new Error("Popover requires tooltip.js")
}h.VERSION="3.3.6",h.DEFAULTS=f.extend({},f.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),h.prototype=f.extend({},f.fn.tooltip.Constructor.prototype),h.prototype.constructor=h,h.prototype.getDefaults=function(){return h.DEFAULTS
},h.prototype.setContent=function(){var k=this.tip(),d=this.getTitle(),l=this.getContent();
k.find(".popover-title")[this.options.html?"html":"text"](d),k.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof l?"html":"append":"text"](l),k.removeClass("fade top bottom left right in"),k.find(".popover-title").html()||k.find(".popover-title").hide()
},h.prototype.hasContent=function(){return this.getTitle()||this.getContent()
},h.prototype.getContent=function(){var d=this.$element,c=this.options;
return d.attr("data-content")||("function"==typeof c.content?c.content.call(d[0]):c.content)
},h.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")
};
var g=f.fn.popover;
f.fn.popover=e,f.fn.popover.Constructor=h,f.fn.popover.noConflict=function(){return f.fn.popover=g,this
}
}(jQuery),+function(f){function e(b,a){this.$body=f(document.body),this.$scrollElement=f(f(b).is(document.body)?window:b),this.options=f.extend({},e.DEFAULTS,a),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",f.proxy(this.process,this)),this.refresh(),this.process()
}function h(a){return this.each(function(){var k=f(this),c=k.data("bs.scrollspy"),b="object"==typeof a&&a;
c||k.data("bs.scrollspy",c=new e(this,b)),"string"==typeof a&&c[a]()
})
}e.VERSION="3.3.6",e.DEFAULTS={offset:10},e.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)
},e.prototype.refresh=function(){var a=this,l="offset",k=0;
this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),f.isWindow(this.$scrollElement[0])||(l="position",k=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var c=f(this),m=c.data("target")||c.attr("href"),d=/^#./.test(m)&&f(m);
return d&&d.length&&d.is(":visible")&&[[d[l]().top+k,m]]||null
}).sort(function(d,c){return d[0]-c[0]
}).each(function(){a.offsets.push(this[0]),a.targets.push(this[1])
})
},e.prototype.process=function(){var l,k=this.$scrollElement.scrollTop()+this.options.offset,q=this.getScrollHeight(),p=this.options.offset+q-this.$scrollElement.height(),o=this.offsets,n=this.targets,m=this.activeTarget;
if(this.scrollHeight!=q&&this.refresh(),k>=p){return m!=(l=n[n.length-1])&&this.activate(l)
}if(m&&k<o[0]){return this.activeTarget=null,this.clear()
}for(l=o.length;
l--;
){m!=n[l]&&k>=o[l]&&(void 0===o[l+1]||k<o[l+1])&&this.activate(n[l])
}},e.prototype.activate=function(a){this.activeTarget=a,this.clear();
var l=this.selector+'[data-target="'+a+'"],'+this.selector+'[href="'+a+'"]',k=f(l).parents("li").addClass("active");
k.parent(".dropdown-menu").length&&(k=k.closest("li.dropdown").addClass("active")),k.trigger("activate.bs.scrollspy")
},e.prototype.clear=function(){f(this.selector).parentsUntil(this.options.target,".active").removeClass("active")
};
var g=f.fn.scrollspy;
f.fn.scrollspy=h,f.fn.scrollspy.Constructor=e,f.fn.scrollspy.noConflict=function(){return f.fn.scrollspy=g,this
},f(window).on("load.bs.scrollspy.data-api",function(){f('[data-spy="scroll"]').each(function(){var a=f(this);
h.call(a,a.data())
})
})
}(jQuery),+function(g){function f(a){return this.each(function(){var c=g(this),b=c.data("bs.tab");
b||c.data("bs.tab",b=new l(this)),"string"==typeof a&&b[a]()
})
}var l=function(a){this.element=g(a)
};
l.VERSION="3.3.6",l.TRANSITION_DURATION=150,l.prototype.show=function(){var a=this.element,r=a.closest("ul:not(.dropdown-menu)"),q=a.data("target");
if(q||(q=a.attr("href"),q=q&&q.replace(/.*(?=#[^\s]*$)/,"")),!a.parent("li").hasClass("active")){var p=r.find(".active:last a"),o=g.Event("hide.bs.tab",{relatedTarget:a[0]}),n=g.Event("show.bs.tab",{relatedTarget:p[0]});
if(p.trigger(o),a.trigger(n),!n.isDefaultPrevented()&&!o.isDefaultPrevented()){var m=g(q);
this.activate(a.closest("li"),r),this.activate(m,m.parent(),function(){p.trigger({type:"hidden.bs.tab",relatedTarget:a[0]}),a.trigger({type:"shown.bs.tab",relatedTarget:p[0]})
})
}}},l.prototype.activate=function(a,p,o){function n(){m.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),a.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),c?(a[0].offsetWidth,a.addClass("in")):a.removeClass("fade"),a.parent(".dropdown-menu").length&&a.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),o&&o()
}var m=p.find("> .active"),c=o&&g.support.transition&&(m.length&&m.hasClass("fade")||!!p.find("> .fade").length);
m.length&&c?m.one("bsTransitionEnd",n).emulateTransitionEnd(l.TRANSITION_DURATION):n(),m.removeClass("in")
};
var k=g.fn.tab;
g.fn.tab=f,g.fn.tab.Constructor=l,g.fn.tab.noConflict=function(){return g.fn.tab=k,this
};
var h=function(a){a.preventDefault(),f.call(g(this),"show")
};
g(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',h).on("click.bs.tab.data-api",'[data-toggle="pill"]',h)
}(jQuery),+function(f){function e(a){return this.each(function(){var k=f(this),c=k.data("bs.affix"),b="object"==typeof a&&a;
c||k.data("bs.affix",c=new h(this,b)),"string"==typeof a&&c[a]()
})
}var h=function(a,c){this.options=f.extend({},h.DEFAULTS,c),this.$target=f(this.options.target).on("scroll.bs.affix.data-api",f.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",f.proxy(this.checkPositionWithEventLoop,this)),this.$element=f(a),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()
};
h.VERSION="3.3.6",h.RESET="affix affix-top affix-bottom",h.DEFAULTS={offset:0,target:window},h.prototype.getState=function(t,s,r,q){var p=this.$target.scrollTop(),o=this.$element.offset(),n=this.$target.height();
if(null!=r&&"top"==this.affixed){return r>p?"top":!1
}if("bottom"==this.affixed){return null!=r?p+this.unpin<=o.top?!1:"bottom":t-q>=p+n?!1:"bottom"
}var m=null==this.affixed,l=m?p:o.top,k=m?n:s;
return null!=r&&r>=p?"top":null!=q&&l+k>=t-q?"bottom":!1
},h.prototype.getPinnedOffset=function(){if(this.pinnedOffset){return this.pinnedOffset
}this.$element.removeClass(h.RESET).addClass("affix");
var d=this.$target.scrollTop(),c=this.$element.offset();
return this.pinnedOffset=c.top-d
},h.prototype.checkPositionWithEventLoop=function(){setTimeout(f.proxy(this.checkPosition,this),1)
},h.prototype.checkPosition=function(){if(this.$element.is(":visible")){var a=this.$element.height(),p=this.options.offset,o=p.top,n=p.bottom,m=Math.max(f(document).height(),f(document.body).height());
"object"!=typeof p&&(n=o=p),"function"==typeof o&&(o=p.top(this.$element)),"function"==typeof n&&(n=p.bottom(this.$element));
var l=this.getState(m,a,o,n);
if(this.affixed!=l){null!=this.unpin&&this.$element.css("top","");
var k="affix"+(l?"-"+l:""),c=f.Event(k+".bs.affix");
if(this.$element.trigger(c),c.isDefaultPrevented()){return
}this.affixed=l,this.unpin="bottom"==l?this.getPinnedOffset():null,this.$element.removeClass(h.RESET).addClass(k).trigger(k.replace("affix","affixed")+".bs.affix")
}"bottom"==l&&this.$element.offset({top:m-a-n})
}};
var g=f.fn.affix;
f.fn.affix=e,f.fn.affix.Constructor=h,f.fn.affix.noConflict=function(){return f.fn.affix=g,this
},f(window).on("load",function(){f('[data-spy="affix"]').each(function(){var b=f(this),a=b.data();
a.offset=a.offset||{},null!=a.offsetBottom&&(a.offset.bottom=a.offsetBottom),null!=a.offsetTop&&(a.offset.top=a.offsetTop),e.call(b,a)
})
})
}(jQuery);
function SetClearFiltersFunction(b){var a=document.getElementById(b+"ClearFiltersButton");
a.addEventListener?a.addEventListener("click",function(){ClearFilters(b)
},!1):a.attachEvent("onclick",function(){ClearFilters(b)
})
}function ClearFilters(a){ResetTable(),$("[id^="+a+"]input:text").val(""),$("[id^="+a+"]input:checked").removeAttr("checked"),$("[id^="+a+"]select option").prop("selected",!1),$("[class^=capability-button-]").attr("data-capability-activated","false"),$("[class^=capability-button-]").attr("class","capability-button-deactivated capability-button-inner")
}function InitializeRangeFilter(k,g,c,f,b,h,d){document.getElementById(k+"FilterTitle").innerHTML=g,document.getElementById(k+"FilterRange").innerHTML="("+f+" - "+b+")";
var f=document.getElementById(k+"FilterMinimumValue");
f.addEventListener?f.addEventListener("change",d,!1):f.attachEvent("onchange",d);
var b=document.getElementById(k+"FilterMaximumValue");
b.addEventListener?b.addEventListener("change",d,!1):b.attachEvent("onchange",d)
}function InitializeYesNoFilter(g,f,c,d){document.getElementById(g+"FilterTitle").innerHTML=f;
var b=document.getElementById(g+"FilterCheckbox");
b.addEventListener?b.addEventListener("click",d,!1):b.attachEvent("onclick",d)
}function InitializeCheckboxListFilter(h,q,m,g,b){document.getElementById(h+"FilterTitle").innerHTML=q;
var p=document.getElementById(h+"FilterDiv"),d=g.split("|");
for(i=0;
i<d.length;
i++){var k=document.createElement("div");
k.className="checkbox-list-filter-checkbox-container";
var v=document.createElement("input");
v.type="checkbox",v.name=h,v.id=h+d[i],v.value=d[i],v.addEventListener?v.addEventListener("click",b):v.attachEvent("onclick",b);
var f=document.createElement("label");
f.className="checkbox-list-filter-checkbox-label",f.htmlFor=h+d[i],f.appendChild(document.createTextNode(" "+d[i])),k.appendChild(v),k.appendChild(f),p.appendChild(k)
}}function InitializeMultiSelectListboxFilter(k,g,d,b,h,f){document.getElementById(k+"FilterTitle").innerHTML=g,document.getElementById(k+"FilterSubTitle").innerHTML=b;
var m=h.split("|"),n=document.getElementById(k+"FilterSelect");
for(i=0;
i<m.length;
i++){n.options[n.options.length]=new Option(m[i],m[i])
}n.addEventListener?n.addEventListener("change",f):n.attachEvent("onchange",f)
}function ResetTable(){var a=document.getElementById("ProductFamiliesTable");
a.className="",$("#ProductFamiliesTable tbody tr").css("background-color","transparent"),$("#ProductFamiliesTable tbody tr").css("color","#222222"),a.className="table table-small-font table-bordered table-striped"
}function RowValuesMatch(f,d,b){var c=!1;
for(filterValuesIndex=0;
filterValuesIndex<b.length;
filterValuesIndex++){-1!=f.getAttribute(d).indexOf(b[filterValuesIndex])&&(c=!0)
}return c
}function RowValuesInRange(k,g,c,f){var b=!1,h=k.getAttribute(g),d=h.split("|");
for(j=0;
j<d.length;
j++){value=parseFloat(d[j]),value>=c&&value<=f&&(b=!0)
}return b
}function RowValueIsSet(b,a){return 1==b.getAttribute(a)
}function IsCapabilityActivated(d,c){var b=document.getElementById(d+c+"Button");
return"true"==b.getAttribute("data-capability-activated")
}function CapabilityLinkClicked(d,c){var b=document.getElementById(d+c+"Button");
CapabilityButtonClicked(b)
}function CapabilityButtonClicked(d){var c=d.getAttribute("data-capability-activated");
"false"==c?(d.className="capability-button-activated capability-button-inner",d.setAttribute("data-capability-activated","true")):(d.className="capability-button-deactivated capability-button-inner",d.setAttribute("data-capability-activated","false"));
var b=d.id;
-1!=b.indexOf("EightBitMcuProducts")?ApplyEightBitMcuFiltersAndCapabilities():-1!=b.indexOf("ThirtyTwoBitMcuProducts")?ApplyThirtyTwoBitMcuFiltersAndCapabilities():-1!=b.indexOf("ClockProducts")?ApplyClockFiltersAndCapabilities():-1!=b.indexOf("BufferProducts")&&ApplyBufferFiltersAndCapabilities()
}function ApplyCapabilityToFilteredTable(d,c,b){1==c?"1"==b.getAttribute(d)&&SetRowStyle(b,"Green"):"1"==b.getAttribute(d)&&SetRowStyle(b,"LightGreen")
}function ApplyCapabilityToUnfilteredTable(b,a){return"1"==a.getAttribute(b)?(SetRowStyle(a,"Green"),!0):(SetRowStyle(a,"Gray"),!1)
}function SetRowStyle(b,a){"Blue"==a?b.setAttribute("style","color:#fff;background-color:#70a3ba"):"Green"==a?b.setAttribute("style","color:#fff;background-color:#a1b92e"):"LightGreen"==a?b.setAttribute("style","color:#bbbbbb;background-color:#d7ddbd"):"Gray"==a&&b.setAttribute("style","color:#bbbbbb;background-color:#f4f4f4")
}function ProductFamilyLinkClick(h,f){var c="",d=null,b="";
switch(h){case"EightBitMcuProducts":c="L2_8-Bit_"+f,d=GetEightBitMcuProductsAppliedCapabilitiesAndFilters(),b="8-bit_Family_Filter";
break;
case"ThirtyTwoBitMcuProducts":c="L2_32-Bit_"+f,d=GetThirtyTwoBitMcuProductsAppliedCapabilitiesAndFilters(),b="32-bit_Family_Filter";
break;
case"ClockProducts":c="L2_Clock_"+f,d=GetClockProductsAppliedCapabilitiesAndFilters(),b="Clock_Family_Filter";
break;
case"BufferProducts":c="L2_Buffer_"+f,d=GetBufferProductsAppliedCapabilitiesAndFilters(),b="Buffer_Family_Filter"
}""!=d&&(c+=d);
var g=document.getElementById(f);
return trackLinks(c,b,g),!1
}function ApplyEightBitMcuFiltersAndCapabilities(){ResetTable();
var ad=document.getElementById("ProductFamiliesTable"),L=!1,ah=parseFloat(document.getElementById("EightBitMcuProductsSpeedFilterMinimumValue").value),Z=parseFloat(document.getElementById("EightBitMcuProductsSpeedFilterMaximumValue").value);
(0==isNaN(ah)||0==isNaN(Z))&&(L=!0,1==isNaN(ah)&&(ah=Number.MIN_VALUE),1==isNaN(Z)&&(Z=Number.MAX_VALUE));
var R=parseFloat(document.getElementById("EightBitMcuProductsFlashMemoryFilterMinimumValue").value),J=parseFloat(document.getElementById("EightBitMcuProductsFlashMemoryFilterMaximumValue").value);
(0==isNaN(R)||0==isNaN(J))&&(L=!0,1==isNaN(R)&&(R=Number.MIN_VALUE),1==isNaN(J)&&(J=Number.MAX_VALUE));
var W=parseFloat(document.getElementById("EightBitMcuProductsTimersFilterMinimumValue").value),af=parseFloat(document.getElementById("EightBitMcuProductsTimersFilterMaximumValue").value);
(0==isNaN(W)||0==isNaN(af))&&(L=!0,1==isNaN(W)&&(W=Number.MIN_VALUE),1==isNaN(af)&&(af=Number.MAX_VALUE));
var O=parseFloat(document.getElementById("EightBitMcuProductsDigitalIoPinsFilterMinimumValue").value),X=parseFloat(document.getElementById("EightBitMcuProductsDigitalIoPinsFilterMaximumValue").value);
(0==isNaN(O)||0==isNaN(X))&&(L=!0,1==isNaN(O)&&(O=Number.MIN_VALUE),1==isNaN(X)&&(X=Number.MAX_VALUE));
var ae=$("input:checkbox[name=EightBitMcuProductsAdcResolution]:checked").map(function(){return this.value
}).get();
ae.length>0&&(L=!0);
var V=$("input:checkbox[name=EightBitMcuProductsDacResolution]:checked").map(function(){return this.value
}).get();
V.length>0&&(L=!0);
var Y=$("#EightBitMcuProductsCapSenseFilterCheckbox").prop("checked");
1==Y&&(L=!0);
var ag=$("#EightBitMcuProductsUsbFilterCheckbox").prop("checked");
1==ag&&(L=!0);
var z=$("#EightBitMcuProductsPackagesFilterSelect option:selected").map(function(){return this.value
}).get();
z.length>0&&(L=!0);
var G=!1,ac=IsCapabilityActivated("EightBitMcuProducts","AnalogIntensive");
1==ac&&(G=!0);
var aa=IsCapabilityActivated("EightBitMcuProducts","GeneralPurpose");
1==aa&&(G=!0);
var ab=IsCapabilityActivated("EightBitMcuProducts","UltraLowPower");
1==ab&&(G=!0);
var w=IsCapabilityActivated("EightBitMcuProducts","Automotive");
1==w&&(G=!0);
var S=IsCapabilityActivated("EightBitMcuProducts","Industrial");
if(1==S&&(G=!0),1==L||1==G){var ad=document.getElementById("ProductFamiliesTable");
ad.className="table table-small-font table-bordered";
var H=!1;
for(i=1;
i<ad.rows.length;
i++){var k=ad.rows[i];
if(1==L){var Q=!0;
1==Q&&0==isNaN(ah)&&0==isNaN(Z)&&(Q=RowValuesInRange(k,"data-speed",ah,Z)),1==Q&&0==isNaN(R)&&0==isNaN(J)&&(Q=RowValuesInRange(k,"data-flash",R,J)),1==Q&&0==isNaN(W)&&0==isNaN(af)&&(Q=RowValuesInRange(k,"data-timers",W,af)),1==Q&&0==isNaN(O)&&0==isNaN(X)&&(Q=RowValuesInRange(k,"data-dig-io",O,X)),1==Q&&ae.length>0&&(Q=RowValuesMatch(k,"data-adc",ae)),1==Q&&V.length>0&&(Q=RowValuesMatch(k,"data-dac",V)),1==Q&&1==Y&&(Q=RowValueIsSet(k,"data-cap-sense")),1==Q&&1==ag&&(Q=RowValueIsSet(k,"data-usb")),1==Q&&z.length>0&&(Q=RowValuesMatch(k,"data-packages",z)),1==Q?SetRowStyle(k,"Blue"):SetRowStyle(k,"Gray"),1==ac&&ApplyCapabilityToFilteredTable("data-analog-intensive",Q,k),1==aa&&ApplyCapabilityToFilteredTable("data-general-purpose",Q,k),1==ab&&ApplyCapabilityToFilteredTable("data-ultra-low-power",Q,k),1==w&&ApplyCapabilityToFilteredTable("data-automotive",Q,k),1==S&&ApplyCapabilityToFilteredTable("data-industrial",Q,k)
}else{if(1==G){if(1==ac){var x=ApplyCapabilityToUnfilteredTable("data-analog-intensive",k);
if(1==x){H=!0;
continue
}}if(1==aa){var D=ApplyCapabilityToUnfilteredTable("data-general-purpose",k);
if(1==D){H=!0;
continue
}}if(1==ab){var q=ApplyCapabilityToUnfilteredTable("data-ultra-low-power",k);
if(1==q){H=!0;
continue
}}if(1==w){var K=ApplyCapabilityToUnfilteredTable("data-automotive",k);
if(1==K){H=!0;
continue
}}if(1==S){var U=ApplyCapabilityToUnfilteredTable("data-industrial",k);
if(1==U){H=!0;
continue
}}SetRowStyle(k,"Gray")
}}}0==L&&0==H&&ResetTable()
}}function GetEightBitMcuProductsAppliedCapabilitiesAndFilters(){var x="",F="";
1==IsCapabilityActivated("EightBitMcuProducts","AnalogIntensive")&&(F+="Analog Intensive:"),1==IsCapabilityActivated("EightBitMcuProducts","GeneralPurpose")&&(F+="General Purpose:"),1==IsCapabilityActivated("EightBitMcuProducts","UltraLowPower")&&(F+="Ultra-Low Power:"),1==IsCapabilityActivated("EightBitMcuProducts","Automotive")&&(F+="Automotive:"),1==IsCapabilityActivated("EightBitMcuProducts","Industrial")&&(F+="Industrial:"),""!=F&&(x+="_"+F.substring(0,F.length-1));
var D="",w=parseFloat(document.getElementById("EightBitMcuProductsSpeedFilterMinimumValue").value),v=parseFloat(document.getElementById("EightBitMcuProductsSpeedFilterMaximumValue").value);
(0==isNaN(w)||0==isNaN(v))&&(D+="Speed:");
var f=parseFloat(document.getElementById("EightBitMcuProductsFlashMemoryFilterMinimumValue").value),E=parseFloat(document.getElementById("EightBitMcuProductsFlashMemoryFilterMaximumValue").value);
(0==isNaN(f)||0==isNaN(E))&&(D+="Flash Memory:");
var h=parseFloat(document.getElementById("EightBitMcuProductsTimersFilterMinimumValue").value),A=parseFloat(document.getElementById("EightBitMcuProductsTimersFilterMaximumValue").value);
(0==isNaN(h)||0==isNaN(A))&&(D+="Timers:");
var G=parseFloat(document.getElementById("EightBitMcuProductsDigitalIoPinsFilterMinimumValue").value),k=parseFloat(document.getElementById("EightBitMcuProductsDigitalIoPinsFilterMaximumValue").value);
(0==isNaN(G)||0==isNaN(k))&&(D+="Digital I/O Pins:");
var z=$("input:checkbox[name=EightBitMcuProductsAdcResolution]:checked").map(function(){return this.value
}).get();
z.length>0&&(D+="ADC Resolution:");
var g=$("input:checkbox[name=EightBitMcuProductsDacResolution]:checked").map(function(){return this.value
}).get();
g.length>0&&(D+="DAC Resolution:");
var q=$("#EightBitMcuProductsCapSenseFilterCheckbox").prop("checked");
1==q&&(D+="Cap Sense:");
var C=$("#EightBitMcuProductsUsbFilterCheckbox").prop("checked");
1==C&&(D+="USB:");
var B=$("#EightBitMcuProductsPackagesFilterSelect option:selected").map(function(){return this.value
}).get();
return B.length>0&&(D+="Packages:"),""!=D&&(x+="_"+D.substring(0,D.length-1)),x
}function ApplyThirtyTwoBitMcuFiltersAndCapabilities(){ResetTable();
var K=document.getElementById("ProductFamiliesTable"),z=!1,P=$("input:checkbox[name=ThirtyTwoBitMcuProductsCore]:checked").map(function(){return this.value
}).get();
P.length>0&&(z=!0);
var G=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsSpeedFilterMinimumValue").value),B=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsSpeedFilterMaximumValue").value);
(0==isNaN(G)||0==isNaN(B))&&(z=!0,1==isNaN(G)&&(G=Number.MIN_VALUE),1==isNaN(B)&&(B=Number.MAX_VALUE));
var x=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsFlashMemoryFilterMinimumValue").value),D=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsFlashMemoryFilterMaximumValue").value);
(0==isNaN(x)||0==isNaN(D))&&(z=!0,1==isNaN(x)&&(x=Number.MIN_VALUE),1==isNaN(D)&&(D=Number.MAX_VALUE));
var M=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsDigitalIoPinsFilterMinimumValue").value),A=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsDigitalIoPinsFilterMaximumValue").value);
(0==isNaN(M)||0==isNaN(A))&&(z=!0,1==isNaN(M)&&(M=Number.MIN_VALUE),1==isNaN(A)&&(A=Number.MAX_VALUE));
var E=$("#ThirtyTwoBitMcuProductsLcdFilterCheckbox").prop("checked");
1==E&&(z=!0);
var L=$("#ThirtyTwoBitMcuProductsUsbFilterCheckbox").prop("checked");
1==L&&(z=!0);
var C=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsI2cFilterMinimumValue").value),F=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsI2cFilterMaximumValue").value);
(0==isNaN(C)||0==isNaN(F))&&(z=!0,1==isNaN(C)&&(C=Number.MIN_VALUE),1==isNaN(F)&&(F=Number.MAX_VALUE));
var O=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUartFilterMinimumValue").value),q=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUartFilterMaximumValue").value);
(0==isNaN(O)||0==isNaN(q))&&(z=!0,1==isNaN(O)&&(O=Number.MIN_VALUE),1==isNaN(q)&&(q=Number.MAX_VALUE));
var w=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUsartFilterMinimumValue").value),J=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUsartFilterMaximumValue").value);
(0==isNaN(w)||0==isNaN(J))&&(z=!0,1==isNaN(w)&&(w=Number.MIN_VALUE),1==isNaN(J)&&(J=Number.MAX_VALUE));
var H=$("#ThirtyTwoBitMcuProductsPackagesFilterSelect option:selected").map(function(){return this.value
}).get();
if(H.length>0&&(z=!0),1==z){var K=document.getElementById("ProductFamiliesTable");
for(K.className="table table-small-font table-bordered",i=1;
i<K.rows.length;
i++){var I=K.rows[i],k=!0;
1==k&&P.length>0&&(k=RowValuesMatch(I,"data-core",P)),1==k&&0==isNaN(G)&&0==isNaN(B)&&(k=RowValuesInRange(I,"data-speed",G,B)),1==k&&0==isNaN(x)&&0==isNaN(D)&&(k=RowValuesInRange(I,"data-flash",x,D)),1==k&&0==isNaN(M)&&0==isNaN(A)&&(k=RowValuesInRange(I,"data-dig-io",M,A)),1==k&&1==E&&(k=RowValueIsSet(I,"data-lcd")),1==k&&1==L&&(k=RowValueIsSet(I,"data-usb")),1==k&&H.length>0&&(k=RowValuesMatch(I,"data-packages",H)),1==k&&0==isNaN(C)&&0==isNaN(F)&&(k=RowValuesInRange(I,"data-i2c",C,F)),1==k&&0==isNaN(O)&&0==isNaN(q)&&(k=RowValuesInRange(I,"data-uart",O,q)),1==k&&0==isNaN(w)&&0==isNaN(J)&&(k=RowValuesInRange(I,"data-usart",w,J)),1==k?SetRowStyle(I,"Blue"):SetRowStyle(I,"Gray")
}}}function GetThirtyTwoBitMcuProductsAppliedCapabilitiesAndFilters(){var I="",w="";
""!=w&&(I+="_"+w.substring(0,w.length-1));
var M="",F=$("input:checkbox[name=ThirtyTwoBitMcuProductsCore]:checked").map(function(){return this.value
}).get();
F.length>0&&(M+="Core:");
var E=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsSpeedFilterMinimumValue").value),z=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsSpeedFilterMaximumValue").value);
(0==isNaN(E)||0==isNaN(z))&&(M+="Speed:");
var q=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsFlashMemoryFilterMinimumValue").value),B=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsFlashMemoryFilterMaximumValue").value);
(0==isNaN(q)||0==isNaN(B))&&(M+="Flash Memory:");
var K=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsDigitalIoPinsFilterMinimumValue").value),x=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsDigitalIoPinsFilterMaximumValue").value);
(0==isNaN(K)||0==isNaN(x))&&(M+="Digital I/O Pins:");
var C=$("#ThirtyTwoBitMcuProductsLcdFilterCheckbox").prop("checked");
1==C&&(M+="LCD:");
var J=$("#ThirtyTwoBitMcuProductsUsbFilterCheckbox").prop("checked");
1==J&&(M+="USB:");
var A=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsI2cFilterMinimumValue").value),D=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsI2cFilterMaximumValue").value);
(0==isNaN(A)||0==isNaN(D))&&(M+="I2C:");
var L=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUartFilterMinimumValue").value),g=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUartFilterMaximumValue").value);
(0==isNaN(L)||0==isNaN(g))&&(M+="UART:");
var k=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUsartFilterMinimumValue").value),H=parseFloat(document.getElementById("ThirtyTwoBitMcuProductsUsartFilterMaximumValue").value);
(0==isNaN(k)||0==isNaN(H))&&(M+="USART:");
var G=$("#ThirtyTwoBitMcuProductsPackagesFilterSelect option:selected").map(function(){return this.value
}).get();
return G.length>0&&(M+="Packages:"),""!=M&&(I+="_"+M.substring(0,M.length-1)),I
}function ApplyClockFiltersAndCapabilities(){ResetTable();
var S=document.getElementById("ProductFamiliesTable"),D=!1,X=$("input:checkbox[name=ClockProductsPhaseJitter]:checked").map(function(){return this.value
}).get();
X.length>0&&(D=!0);
var O=$("input:checkbox[name=ClockProductsMaxOutputFrequency]:checked").map(function(){return this.value
}).get();
O.length>0&&(D=!0);
var H=$("input:checkbox[name=ClockProductsOutputFormatType]:checked").map(function(){return this.value
}).get();
H.length>0&&(D=!0);
var A=parseFloat(document.getElementById("ClockProductsNumberOfOutputsFilterMinimumValue").value),K=parseFloat(document.getElementById("ClockProductsNumberOfOutputsFilterMaximumValue").value);
(0==isNaN(A)||0==isNaN(K))&&(D=!0,1==isNaN(A)&&(A=Number.MIN_VALUE),1==isNaN(K)&&(K=Number.MAX_VALUE));
var V=$("#ClockProductsPCIeCompliantFilterCheckbox").prop("checked");
1==V&&(D=!0);
var E=!1,L=IsCapabilityActivated("ClockProducts","ClockGenerators");
1==L&&(E=!0);
var U=IsCapabilityActivated("ClockProducts","JitterAttenuatingClocks");
1==U&&(E=!0);
var J=IsCapabilityActivated("ClockProducts","SynchronousEthernet1588");
1==J&&(E=!0);
var M=IsCapabilityActivated("ClockProducts","PCIExpressClocks");
1==M&&(E=!0);
var W=IsCapabilityActivated("ClockProducts","4GLTEWirelessClocks");
1==W&&(E=!0);
var w=IsCapabilityActivated("ClockProducts","IntelX86Clocks");
if(1==w&&(E=!0),1==D||1==E){var S=document.getElementById("ProductFamiliesTable");
S.className="table table-small-font table-bordered";
var x=!1;
for(i=1;
i<S.rows.length;
i++){var R=S.rows[i];
if(1==D){var P=!0;
1==P&&X.length>0&&(P=RowValuesMatch(R,"data-phase-jitter",X)),1==P&&O.length>0&&(P=RowValuesMatch(R,"data-max-output-frequency",O)),1==P&&H.length>0&&(P=RowValuesMatch(R,"data-output-format-type",H)),1==P&&0==isNaN(A)&&0==isNaN(K)&&(P=RowValuesInRange(R,"data-number-of-outputs",A,K)),1==P&&1==V&&(P=RowValueIsSet(R,"data-pci-express-clocks")),1==P?SetRowStyle(R,"Blue"):SetRowStyle(R,"Gray"),1==L&&ApplyCapabilityToFilteredTable("data-clock-generators",P,R),1==U&&ApplyCapabilityToFilteredTable("data-jitter-attenuating-clocks",P,R),1==J&&ApplyCapabilityToFilteredTable("data-synchronous-ethernet-1588",P,R),1==M&&ApplyCapabilityToFilteredTable("data-pci-express-clocks",P,R),1==W&&ApplyCapabilityToFilteredTable("data-four-g-lte-wireless-clocks",P,R),1==w&&ApplyCapabilityToFilteredTable("data-intel-x86-clocks",P,R)
}else{if(1==E){if(1==L){var Q=ApplyCapabilityToUnfilteredTable("data-clock-generators",R);
if(1==Q){x=!0;
continue
}}if(1==U){var q=ApplyCapabilityToUnfilteredTable("data-jitter-attenuating-clocks",R);
if(1==q){x=!0;
continue
}}if(1==J){var I=ApplyCapabilityToUnfilteredTable("data-synchronous-ethernet-1588",R);
if(1==I){x=!0;
continue
}}if(1==M){var z=ApplyCapabilityToUnfilteredTable("data-pci-express-clocks",R);
if(1==z){x=!0;
continue
}}if(1==W){var k=ApplyCapabilityToUnfilteredTable("data-four-g-lte-wireless-clocks",R);
if(1==k){x=!0;
continue
}}if(1==w){var G=ApplyCapabilityToUnfilteredTable("data-intel-x86-clocks",R);
if(1==G){x=!0;
continue
}}SetRowStyle(R,"Gray")
}}}0==D&&0==x&&ResetTable()
}}function GetClockProductsAppliedCapabilitiesAndFilters(){var h="",p="";
1==IsCapabilityActivated("ClockProducts","ClockGenerators")&&(p+="Clock Generators:"),1==IsCapabilityActivated("ClockProducts","JitterAttenuatingClocks")&&(p+="Jitter Attenuating Clocks:"),1==IsCapabilityActivated("ClockProducts","SynchronousEthernet1588")&&(p+="Synchronous Ethernet/1588:"),1==IsCapabilityActivated("ClockProducts","PCIExpressClocks")&&(p+="PCI Express Clocks:"),1==IsCapabilityActivated("ClockProducts","4GLTEWirelessClocks")&&(p+="4G/LTE Wireless Clocks:"),1==IsCapabilityActivated("ClockProducts","IntelX86Clocks")&&(p+="Intel x86 Clocks:"),""!=p&&(h+="_"+p.substring(0,p.length-1));
var m="",g=$("input:checkbox[name=ClockProductsPhaseJitter]:checked").map(function(){return this.value
}).get();
g.length>0&&(m+="Phase Jitter:");
var f=$("input:checkbox[name=ClockProductsMaxOutputFrequency]:checked").map(function(){return this.value
}).get();
f.length>0&&(m+="Max Output Frequency:");
var b=$("input:checkbox[name=ClockProductsOutputFormatType]:checked").map(function(){return this.value
}).get();
b.length>0&&(m+="Output Format Type:");
var n=parseFloat(document.getElementById("ClockProductsNumberOfOutputsFilterMinimumValue").value),d=parseFloat(document.getElementById("ClockProductsNumberOfOutputsFilterMaximumValue").value);
(0==isNaN(n)||0==isNaN(d))&&(m+="Number of Outputs:");
var k=$("#ClockProductsPCIeCompliantFilterCheckbox").prop("checked");
return 1==k&&(m+="PCIe Compliant:"),""!=m&&(h+="_"+m.substring(0,m.length-1)),h
}function ApplyBufferFiltersAndCapabilities(){ResetTable();
var M=document.getElementById("ProductFamiliesTable"),z=!1,R=$("input:checkbox[name=BufferProductsAdditiveJitter]:checked").map(function(){return this.value
}).get();
R.length>0&&(z=!0);
var I=$("input:checkbox[name=BufferProductsOutputFormatType]:checked").map(function(){return this.value
}).get();
I.length>0&&(z=!0);
var C=$("#BufferProductsPCIeCompliantFilterCheckbox").prop("checked");
1==C&&(z=!0);
var x=parseFloat(document.getElementById("BufferProductsNumberOfOutputsFilterMinimumValue").value),F=parseFloat(document.getElementById("BufferProductsNumberOfOutputsFilterMaximumValue").value);
(0==isNaN(x)||0==isNaN(F))&&(z=!0,1==isNaN(x)&&(x=Number.MIN_VALUE),1==isNaN(F)&&(F=Number.MAX_VALUE));
var P=!1,A=IsCapabilityActivated("BufferProducts","UniversalBuffers");
1==A&&(P=!0);
var G=IsCapabilityActivated("BufferProducts","DifferentialBuffers");
1==G&&(P=!0);
var O=IsCapabilityActivated("BufferProducts","LVCMOSBuffers");
1==O&&(P=!0);
var E=IsCapabilityActivated("BufferProducts","PCIExpressClockBuffers");
1==E&&(P=!0);
var H=IsCapabilityActivated("BufferProducts","ZeroDelayBuffers");
if(1==H&&(P=!0),1==z||1==P){var M=document.getElementById("ProductFamiliesTable");
M.className="table table-small-font table-bordered";
var Q=!1;
for(i=1;
i<M.rows.length;
i++){var q=M.rows[i];
if(1==z){var w=!0;
1==w&&R.length>0&&(w=RowValuesMatch(q,"data-additive-jitter",R)),1==w&&I.length>0&&(w=RowValuesMatch(q,"data-output-format-type",I)),1==w&&1==C&&(w=RowValueIsSet(q,"data-pci-express-buffers")),1==w&&0==isNaN(x)&&0==isNaN(F)&&(w=RowValuesInRange(q,"data-number-of-outputs",x,F)),1==w?SetRowStyle(q,"Blue"):SetRowStyle(q,"Gray"),1==A&&ApplyCapabilityToFilteredTable("data-universal-buffers",w,q),1==G&&ApplyCapabilityToFilteredTable("data-differential-buffers",w,q),1==O&&ApplyCapabilityToFilteredTable("data-lvcmos-buffers",w,q),1==E&&ApplyCapabilityToFilteredTable("data-pci-express-buffers",w,q),1==H&&ApplyCapabilityToFilteredTable("data-zero-delay-buffers",w,q)
}else{if(1==P){if(1==A){var L=ApplyCapabilityToUnfilteredTable("data-universal-buffers",q);
if(1==L){Q=!0;
continue
}}if(1==G){var J=ApplyCapabilityToUnfilteredTable("data-differential-buffers",q);
if(1==J){Q=!0;
continue
}}if(1==O){var K=ApplyCapabilityToUnfilteredTable("data-lvcmos-buffers",q);
if(1==K){Q=!0;
continue
}}if(1==E){var k=ApplyCapabilityToUnfilteredTable("data-pci-express-buffers",q);
if(1==k){Q=!0;
continue
}}if(1==H){var D=ApplyCapabilityToUnfilteredTable("data-zero-delay-buffers",q);
if(1==D){Q=!0;
continue
}}SetRowStyle(q,"Gray")
}}}0==z&&0==Q&&ResetTable()
}}function GetBufferProductsAppliedCapabilitiesAndFilters(){var k="",g="";
1==IsCapabilityActivated("BufferProducts","UniversalBuffers")&&(g+="Universal Buffers:"),1==IsCapabilityActivated("BufferProducts","DifferentialBuffers")&&(g+="Differential Buffers:"),1==IsCapabilityActivated("BufferProducts","LVCMOSBuffers")&&(g+="LVCMOS Buffers:"),1==IsCapabilityActivated("BufferProducts","PCIExpressClockBuffers")&&(g+="PCI Express Buffers:"),1==IsCapabilityActivated("BufferProducts","ZeroDelayBuffers")&&(g+="Zero Delay Buffers:"),""!=g&&(k+="_"+g.substring(0,g.length-1));
var c="",f=$("input:checkbox[name=BufferProductsAdditiveJitter]:checked").map(function(){return this.value
}).get();
f.length>0&&(c+="Additive Jitter:");
var b=$("input:checkbox[name=BufferProductsOutputFormatType]:checked").map(function(){return this.value
}).get();
b.length>0&&(c+="Output Format Type:");
var h=$("#BufferProductsPCIeCompliantFilterCheckbox").prop("checked");
1==h&&(c+="PCIe Compliant:");
var d=parseFloat(document.getElementById("BufferProductsNumberOfOutputsFilterMinimumValue").value),m=parseFloat(document.getElementById("BufferProductsNumberOfOutputsFilterMaximumValue").value);
return(0==isNaN(d)||0==isNaN(m))&&(c+="Number of Outputs:"),""!=c&&(k+="_"+c.substring(0,c.length-1)),k
}
/*! jQuery UI - v1.12.1 - 2016-10-18
* http://jqueryui.com
* Includes: widget.js, position.js, keycode.js, unique-id.js, widgets/autocomplete.js, widgets/menu.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */
(function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)
})(function(b){b.ui=b.ui||{},b.ui.version="1.12.1";
var c=0,a=Array.prototype.slice;
b.cleanData=function(d){return function(f){var g,k,h;
for(h=0;
null!=(k=f[h]);
h++){try{g=b._data(k,"events"),g&&g.remove&&b(k).triggerHandler("remove")
}catch(e){}}d(f)
}
}(b.cleanData),b.widget=function(q,m,u){var g,f,t,d={},k=q.split(".")[0];
q=q.split(".")[1];
var p=k+"-"+q;
return u||(u=m,m=b.Widget),b.isArray(u)&&(u=b.extend.apply(null,[{}].concat(u))),b.expr[":"][p.toLowerCase()]=function(h){return !!b.data(h,p)
},b[k]=b[k]||{},g=b[k][q],f=b[k][q]=function(h,l){return this._createWidget?(arguments.length&&this._createWidget(h,l),void 0):new f(h,l)
},b.extend(f,g,{version:u.version,_proto:b.extend({},u),_childConstructors:[]}),t=new m,t.options=b.widget.extend({},t.options),b.each(u,function(l,h){return b.isFunction(h)?(d[l]=function(){function e(){return m.prototype[l].apply(this,arguments)
}function o(n){return m.prototype[l].apply(this,n)
}return function(){var r,n=this._super,s=this._superApply;
return this._super=e,this._superApply=o,r=h.apply(this,arguments),this._super=n,this._superApply=s,r
}
}(),void 0):(d[l]=h,void 0)
}),f.prototype=b.widget.extend(t,{widgetEventPrefix:g?t.widgetEventPrefix||q:q},d,{constructor:f,namespace:k,widgetName:q,widgetFullName:p}),g?(b.each(g._childConstructors,function(n,h){var l=h.prototype;
b.widget(l.namespace+"."+l.widgetName,f,h._proto)
}),delete g._childConstructors):m._childConstructors.push(f),b.widget.bridge(q,f),f
},b.widget.extend=function(h){for(var f,l,k=a.call(arguments,1),d=0,g=k.length;
g>d;
d++){for(f in k[d]){l=k[d][f],k[d].hasOwnProperty(f)&&void 0!==l&&(h[f]=b.isPlainObject(l)?b.isPlainObject(h[f])?b.widget.extend({},h[f],l):b.widget.extend({},l):l)
}}return h
},b.widget.bridge=function(f,d){var g=d.prototype.widgetFullName||f;
b.fn[f]=function(m){var h="string"==typeof m,k=a.call(arguments,1),e=this;
return h?this.length||"instance"!==m?this.each(function(){var l,n=b.data(this,g);
return"instance"===m?(e=n,!1):n?b.isFunction(n[m])&&"_"!==m.charAt(0)?(l=n[m].apply(n,k),l!==n&&void 0!==l?(e=l&&l.jquery?e.pushStack(l.get()):l,!1):void 0):b.error("no such method '"+m+"' for "+f+" widget instance"):b.error("cannot call methods on "+f+" prior to initialization; attempted to call method '"+m+"'")
}):e=void 0:(k.length&&(m=b.widget.extend.apply(null,[m].concat(k))),this.each(function(){var l=b.data(this,g);
l?(l.option(m||{}),l._init&&l._init()):b.data(this,g,new d(m,this))
})),e
}
},b.Widget=function(){},b.Widget._childConstructors=[],b.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{classes:{},disabled:!1,create:null},_createWidget:function(d,e){e=b(e||this.defaultElement||this)[0],this.element=b(e),this.uuid=c++,this.eventNamespace="."+this.widgetName+this.uuid,this.bindings=b(),this.hoverable=b(),this.focusable=b(),this.classesElementLookup={},e!==this&&(b.data(e,this.widgetFullName,this),this._on(!0,this.element,{remove:function(f){f.target===e&&this.destroy()
}}),this.document=b(e.style?e.ownerDocument:e.document||e),this.window=b(this.document[0].defaultView||this.document[0].parentWindow)),this.options=b.widget.extend({},this.options,this._getCreateOptions(),d),this._create(),this.options.disabled&&this._setOptionDisabled(this.options.disabled),this._trigger("create",null,this._getCreateEventData()),this._init()
},_getCreateOptions:function(){return{}
},_getCreateEventData:b.noop,_create:b.noop,_init:b.noop,destroy:function(){var d=this;
this._destroy(),b.each(this.classesElementLookup,function(f,e){d._removeClass(e,f)
}),this.element.off(this.eventNamespace).removeData(this.widgetFullName),this.widget().off(this.eventNamespace).removeAttr("aria-disabled"),this.bindings.off(this.eventNamespace)
},_destroy:b.noop,widget:function(){return this.element
},option:function(h,f){var g,l,k,d=h;
if(0===arguments.length){return b.widget.extend({},this.options)
}if("string"==typeof h){if(d={},g=h.split("."),h=g.shift(),g.length){for(l=d[h]=b.widget.extend({},this.options[h]),k=0;
g.length-1>k;
k++){l[g[k]]=l[g[k]]||{},l=l[g[k]]
}if(h=g.pop(),1===arguments.length){return void 0===l[h]?null:l[h]
}l[h]=f
}else{if(1===arguments.length){return void 0===this.options[h]?null:this.options[h]
}d[h]=f
}}return this._setOptions(d),this
},_setOptions:function(d){var f;
for(f in d){this._setOption(f,d[f])
}return this
},_setOption:function(d,f){return"classes"===d&&this._setOptionClasses(f),this.options[d]=f,"disabled"===d&&this._setOptionDisabled(f),this
},_setOptionClasses:function(g){var d,f,h;
for(d in g){h=this.classesElementLookup[d],g[d]!==this.options.classes[d]&&h&&h.length&&(f=b(h.get()),this._removeClass(h,d),f.addClass(this._classes({element:f,keys:d,classes:g,add:!0})))
}},_setOptionDisabled:function(d){this._toggleClass(this.widget(),this.widgetFullName+"-disabled",null,!!d),d&&(this._removeClass(this.hoverable,null,"ui-state-hover"),this._removeClass(this.focusable,null,"ui-state-focus"))
},enable:function(){return this._setOptions({disabled:!1})
},disable:function(){return this._setOptions({disabled:!0})
},_classes:function(g){function d(k,m){var e,l;
for(l=0;
k.length>l;
l++){e=h.classesElementLookup[k[l]]||b(),e=g.add?b(b.unique(e.get().concat(g.element.get()))):b(e.not(g.element).get()),h.classesElementLookup[k[l]]=e,f.push(k[l]),m&&g.classes[k[l]]&&f.push(g.classes[k[l]])
}}var f=[],h=this;
return g=b.extend({element:this.element,classes:this.options.classes||{}},g),this._on(g.element,{remove:"_untrackClassesElement"}),g.keys&&d(g.keys.match(/\S+/g)||[],!0),g.extra&&d(g.extra.match(/\S+/g)||[]),f.join(" ")
},_untrackClassesElement:function(f){var d=this;
b.each(d.classesElementLookup,function(e,g){-1!==b.inArray(f.target,g)&&(d.classesElementLookup[e]=b(g.not(f.target).get()))
})
},_removeClass:function(f,g,d){return this._toggleClass(f,g,d,!1)
},_addClass:function(f,g,d){return this._toggleClass(f,g,d,!0)
},_toggleClass:function(f,h,d,g){g="boolean"==typeof g?g:d;
var l="string"==typeof f||null===f,k={extra:l?h:d,keys:l?f:h,element:l?this.element:f,add:g};
return k.element.toggleClass(this._classes(k),g),this
},_on:function(g,d,f){var k,h=this;
"boolean"!=typeof g&&(f=d,d=g,g=!1),f?(d=k=b(d),this.bindings=this.bindings.add(d)):(f=d,d=this.element,k=this.widget()),b.each(f,function(o,m){function p(){return g||h.options.disabled!==!0&&!b(this).hasClass("ui-state-disabled")?("string"==typeof m?h[m]:m).apply(h,arguments):void 0
}"string"!=typeof m&&(p.guid=m.guid=m.guid||p.guid||b.guid++);
var e=o.match(/^([\w:-]*)\s*(.*)$/),n=e[1]+h.eventNamespace,q=e[2];
q?k.on(n,q,p):d.on(n,p)
})
},_off:function(f,d){d=(d||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,f.off(d).off(d),this.bindings=b(this.bindings.not(f).get()),this.focusable=b(this.focusable.not(f).get()),this.hoverable=b(this.hoverable.not(f).get())
},_delay:function(f,h){function d(){return("string"==typeof f?g[f]:f).apply(g,arguments)
}var g=this;
return setTimeout(d,h||0)
},_hoverable:function(d){this.hoverable=this.hoverable.add(d),this._on(d,{mouseenter:function(f){this._addClass(b(f.currentTarget),null,"ui-state-hover")
},mouseleave:function(f){this._removeClass(b(f.currentTarget),null,"ui-state-hover")
}})
},_focusable:function(d){this.focusable=this.focusable.add(d),this._on(d,{focusin:function(f){this._addClass(b(f.currentTarget),null,"ui-state-focus")
},focusout:function(f){this._removeClass(b(f.currentTarget),null,"ui-state-focus")
}})
},_trigger:function(h,f,g){var l,k,d=this.options[h];
if(g=g||{},f=b.Event(f),f.type=(h===this.widgetEventPrefix?h:this.widgetEventPrefix+h).toLowerCase(),f.target=this.element[0],k=f.originalEvent){for(l in k){l in f||(f[l]=k[l])
}}return this.element.trigger(f,g),!(b.isFunction(d)&&d.apply(this.element[0],[f].concat(g))===!1||f.isDefaultPrevented())
}},b.each({show:"fadeIn",hide:"fadeOut"},function(f,d){b.Widget.prototype["_"+f]=function(g,l,k){"string"==typeof l&&(l={effect:l});
var e,h=l?l===!0||"number"==typeof l?d:l.effect||d:f;
l=l||{},"number"==typeof l&&(l={duration:l}),e=!b.isEmptyObject(l),l.complete=k,l.delay&&g.delay(l.delay),e&&b.effects&&b.effects.effect[h]?g[f](l):h!==f&&g[h]?g[h](l.duration,l.easing,k):g.queue(function(m){b(this)[f](),k&&k.call(g[0]),m()
})
}
}),b.widget,function(){function t(h,l,d){return[parseFloat(h[0])*(y.test(h[0])?l/100:1),parseFloat(h[1])*(y.test(h[1])?d/100:1)]
}function p(h,d){return parseInt(b.css(h,d),10)||0
}function z(h){var d=h[0];
return 9===d.nodeType?{width:h.width(),height:h.height(),offset:{top:0,left:0}}:b.isWindow(d)?{width:h.width(),height:h.height(),offset:{top:h.scrollTop(),left:h.scrollLeft()}}:d.preventDefault?{width:0,height:0,offset:{top:d.pageY,left:d.pageX}}:{width:h.outerWidth(),height:h.outerHeight(),offset:h.offset()}
}var k,g=Math.max,x=Math.abs,f=/left|center|right/,m=/top|center|bottom/,q=/[\+\-]\d+(\.[\d]+)?%?/,w=/^\w+/,y=/%$/,v=b.fn.position;
b.position={scrollbarWidth:function(){if(void 0!==k){return k
}var l,d,h=b("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),n=h.children()[0];
return b("body").append(h),l=n.offsetWidth,h.css("overflow","scroll"),d=n.offsetWidth,l===d&&(d=h[0].clientWidth),h.remove(),k=l-d
},getScrollInfo:function(l){var d=l.isWindow||l.isDocument?"":l.element.css("overflow-x"),h=l.isWindow||l.isDocument?"":l.element.css("overflow-y"),u="scroll"===d||"auto"===d&&l.width<l.element[0].scrollWidth,r="scroll"===h||"auto"===h&&l.height<l.element[0].scrollHeight;
return{width:r?b.position.scrollbarWidth():0,height:u?b.position.scrollbarWidth():0}
},getWithinInfo:function(l){var d=b(l||window),h=b.isWindow(d[0]),u=!!d[0]&&9===d[0].nodeType,r=!h&&!u;
return{element:d,isWindow:h,isDocument:u,offset:r?b(l).offset():{left:0,top:0},scrollLeft:d.scrollLeft(),scrollTop:d.scrollTop(),width:d.outerWidth(),height:d.outerHeight()}
}},b.fn.position=function(e){if(!e||!e.of){return v.apply(this,arguments)
}e=b.extend({},e);
var E,d,r,o,h,C,D=b(e.of),A=b.position.getWithinInfo(e.within),s=b.position.getScrollInfo(A),B=(e.collision||"flip").split(" "),l={};
return C=z(D),D[0].preventDefault&&(e.at="left top"),d=C.width,r=C.height,o=C.offset,h=b.extend({},o),b.each(["my","at"],function(){var u,F,n=(e[this]||"").split(" ");
1===n.length&&(n=f.test(n[0])?n.concat(["center"]):m.test(n[0])?["center"].concat(n):["center","center"]),n[0]=f.test(n[0])?n[0]:"center",n[1]=m.test(n[1])?n[1]:"center",u=q.exec(n[0]),F=q.exec(n[1]),l[this]=[u?u[0]:0,F?F[0]:0],e[this]=[w.exec(n[0])[0],w.exec(n[1])[0]]
}),1===B.length&&(B[1]=B[0]),"right"===e.at[0]?h.left+=d:"center"===e.at[0]&&(h.left+=d/2),"bottom"===e.at[1]?h.top+=r:"center"===e.at[1]&&(h.top+=r/2),E=t(l.at,d,r),h.left+=E[0],h.top+=E[1],this.each(function(){var N,u,G=b(this),I=G.outerWidth(),K=G.outerHeight(),J=p(this,"marginLeft"),M=p(this,"marginTop"),L=I+J+p(this,"marginRight")+s.width,F=K+M+p(this,"marginBottom")+s.height,n=b.extend({},h),H=t(l.my,G.outerWidth(),G.outerHeight());
"right"===e.my[0]?n.left-=I:"center"===e.my[0]&&(n.left-=I/2),"bottom"===e.my[1]?n.top-=K:"center"===e.my[1]&&(n.top-=K/2),n.left+=H[0],n.top+=H[1],N={marginLeft:J,marginTop:M},b.each(["left","top"],function(P,O){b.ui.position[B[P]]&&b.ui.position[B[P]][O](n,{targetWidth:d,targetHeight:r,elemWidth:I,elemHeight:K,collisionPosition:N,collisionWidth:L,collisionHeight:F,offset:[E[0]+H[0],E[1]+H[1]],my:e.my,at:e.at,within:A,elem:G})
}),e.using&&(u=function(Q){var T=o.left-n.left,P=T+d-I,R=o.top-n.top,S=R+r-K,O={target:{element:D,left:o.left,top:o.top,width:d,height:r},element:{element:G,left:n.left,top:n.top,width:I,height:K},horizontal:0>P?"left":T>0?"right":"center",vertical:0>S?"top":R>0?"bottom":"middle"};
I>d&&d>x(T+P)&&(O.horizontal="center"),K>r&&r>x(R+S)&&(O.vertical="middle"),O.important=g(x(T),x(P))>g(x(R),x(S))?"horizontal":"vertical",e.using.call(this,Q,O)
}),G.offset(b.extend(n,{using:u}))
})
},b.ui.position={fit:{left:function(E,C){var A,F=C.within,o=F.isWindow?F.scrollLeft:F.offset.left,D=F.width,d=E.left-C.collisionPosition.marginLeft,u=o-d,B=d+C.collisionWidth-D-o;
C.collisionWidth>D?u>0&&0>=B?(A=E.left+u+C.collisionWidth-D-o,E.left+=u-A):E.left=B>0&&0>=u?o:u>B?o+D-C.collisionWidth:o:u>0?E.left+=u:B>0?E.left-=B:E.left=g(E.left-d,E.left)
},top:function(E,C){var A,F=C.within,o=F.isWindow?F.scrollTop:F.offset.top,D=C.within.height,d=E.top-C.collisionPosition.marginTop,u=o-d,B=d+C.collisionHeight-D-o;
C.collisionHeight>D?u>0&&0>=B?(A=E.top+u+C.collisionHeight-D-o,E.top+=u-A):E.top=B>0&&0>=u?o:u>B?o+D-C.collisionHeight:o:u>0?E.top+=u:B>0?E.top-=B:E.top=g(E.top-d,E.top)
}},flip:{left:function(M,I){var F,N,D=I.within,C=D.offset.left+D.scrollLeft,A=D.width,E=D.isWindow?D.scrollLeft:D.offset.left,G=M.left-I.collisionPosition.marginLeft,K=G-E,L=G+I.collisionWidth-A-E,J="left"===I.my[0]?-I.elemWidth:"right"===I.my[0]?I.elemWidth:0,B="left"===I.at[0]?I.targetWidth:"right"===I.at[0]?-I.targetWidth:0,H=-2*I.offset[0];
0>K?(F=M.left+J+B+H+I.collisionWidth-A-C,(0>F||x(K)>F)&&(M.left+=J+B+H)):L>0&&(N=M.left-I.collisionPosition.marginLeft+J+B+H-E,(N>0||L>x(N))&&(M.left+=J+B+H))
},top:function(N,J){var F,O,D=J.within,C=D.offset.top+D.scrollTop,A=D.height,E=D.isWindow?D.scrollTop:D.offset.top,G=N.top-J.collisionPosition.marginTop,L=G-E,M=G+J.collisionHeight-A-E,K="top"===J.my[1],B=K?-J.elemHeight:"bottom"===J.my[1]?J.elemHeight:0,I="top"===J.at[1]?J.targetHeight:"bottom"===J.at[1]?-J.targetHeight:0,H=-2*J.offset[1];
0>L?(O=N.top+B+I+H+J.collisionHeight-A-C,(0>O||x(L)>O)&&(N.top+=B+I+H)):M>0&&(F=N.top-J.collisionPosition.marginTop+B+I+H-E,(F>0||M>x(F))&&(N.top+=B+I+H))
}},flipfit:{left:function(){b.ui.position.flip.left.apply(this,arguments),b.ui.position.fit.left.apply(this,arguments)
},top:function(){b.ui.position.flip.top.apply(this,arguments),b.ui.position.fit.top.apply(this,arguments)
}}}
}(),b.ui.position,b.ui.keyCode={BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38},b.fn.extend({uniqueId:function(){var d=0;
return function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++d)
})
}
}(),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&b(this).removeAttr("id")
})
}}),b.ui.safeActiveElement=function(f){var g;
try{g=f.activeElement
}catch(d){g=f.body
}return g||(g=f.body),g.nodeName||(g=f.body),g
},b.widget("ui.menu",{version:"1.12.1",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-caret-1-e"},items:"> *",menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().attr({role:this.options.role,tabIndex:0}),this._addClass("ui-menu","ui-widget ui-widget-content"),this._on({"mousedown .ui-menu-item":function(d){d.preventDefault()
},"click .ui-menu-item":function(g){var d=b(g.target),f=b(b.ui.safeActiveElement(this.document[0]));
!this.mouseHandled&&d.not(".ui-state-disabled").length&&(this.select(g),g.isPropagationStopped()||(this.mouseHandled=!0),d.has(".ui-menu").length?this.expand(g):!this.element.is(":focus")&&f.closest(".ui-menu").length&&(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))
},"mouseenter .ui-menu-item":function(g){if(!this.previousFilter){var d=b(g.target).closest(".ui-menu-item"),f=b(g.currentTarget);
d[0]===f[0]&&(this._removeClass(f.siblings().children(".ui-state-active"),null,"ui-state-active"),this.focus(g,f))
}},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(f,g){var d=this.active||this.element.find(this.options.items).eq(0);
g||this.focus(f,d)
},blur:function(d){this._delay(function(){var e=!b.contains(this.element[0],b.ui.safeActiveElement(this.document[0]));
e&&this.collapseAll(d)
})
},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(d){this._closeOnDocumentClick(d)&&this.collapseAll(d),this.mouseHandled=!1
}})
},_destroy:function(){var f=this.element.find(".ui-menu-item").removeAttr("role aria-disabled"),d=f.children(".ui-menu-item-wrapper").removeUniqueId().removeAttr("tabIndex role aria-haspopup");
this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeAttr("role aria-labelledby aria-expanded aria-hidden aria-disabled tabIndex").removeUniqueId().show(),d.children().each(function(){var g=b(this);
g.data("ui-menu-submenu-caret")&&g.remove()
})
},_keydown:function(h){var f,g,l,k,d=!0;
switch(h.keyCode){case b.ui.keyCode.PAGE_UP:this.previousPage(h);
break;
case b.ui.keyCode.PAGE_DOWN:this.nextPage(h);
break;
case b.ui.keyCode.HOME:this._move("first","first",h);
break;
case b.ui.keyCode.END:this._move("last","last",h);
break;
case b.ui.keyCode.UP:this.previous(h);
break;
case b.ui.keyCode.DOWN:this.next(h);
break;
case b.ui.keyCode.LEFT:this.collapse(h);
break;
case b.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(h);
break;
case b.ui.keyCode.ENTER:case b.ui.keyCode.SPACE:this._activate(h);
break;
case b.ui.keyCode.ESCAPE:this.collapse(h);
break;
default:d=!1,g=this.previousFilter||"",k=!1,l=h.keyCode>=96&&105>=h.keyCode?""+(h.keyCode-96):String.fromCharCode(h.keyCode),clearTimeout(this.filterTimer),l===g?k=!0:l=g+l,f=this._filterMenuItems(l),f=k&&-1!==f.index(this.active.next())?this.active.nextAll(".ui-menu-item"):f,f.length||(l=String.fromCharCode(h.keyCode),f=this._filterMenuItems(l)),f.length?(this.focus(h,f),this.previousFilter=l,this.filterTimer=this._delay(function(){delete this.previousFilter
},1000)):delete this.previousFilter
}d&&h.preventDefault()
},_activate:function(d){this.active&&!this.active.is(".ui-state-disabled")&&(this.active.children("[aria-haspopup='true']").length?this.expand(d):this.select(d))
},refresh:function(){var m,g,h,q,p,f=this,k=this.options.icons.submenu,d=this.element.find(this.options.menus);
this._toggleClass("ui-menu-icons",null,!!this.element.find(".ui-icon").length),h=d.filter(":not(.ui-menu)").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var o=b(this),l=o.prev(),n=b("<span>").data("ui-menu-submenu-caret",!0);
f._addClass(n,"ui-menu-icon","ui-icon "+k),l.attr("aria-haspopup","true").prepend(n),o.attr("aria-labelledby",l.attr("id"))
}),this._addClass(h,"ui-menu","ui-widget ui-widget-content ui-front"),m=d.add(this.element),g=m.find(this.options.items),g.not(".ui-menu-item").each(function(){var l=b(this);
f._isDivider(l)&&f._addClass(l,"ui-menu-divider","ui-widget-content")
}),q=g.not(".ui-menu-item, .ui-menu-divider"),p=q.children().not(".ui-menu").uniqueId().attr({tabIndex:-1,role:this._itemRole()}),this._addClass(q,"ui-menu-item")._addClass(p,"ui-menu-item-wrapper"),g.filter(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!b.contains(this.element[0],this.active[0])&&this.blur()
},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]
},_setOption:function(f,g){if("icons"===f){var d=this.element.find(".ui-menu-icon");
this._removeClass(d,null,this.options.icons.submenu)._addClass(d,null,g.submenu)
}this._super(f,g)
},_setOptionDisabled:function(d){this._super(d),this.element.attr("aria-disabled",d+""),this._toggleClass(null,"ui-state-disabled",!!d)
},focus:function(f,h){var d,g,k;
this.blur(f,f&&"focus"===f.type),this._scrollIntoView(h),this.active=h.first(),g=this.active.children(".ui-menu-item-wrapper"),this._addClass(g,null,"ui-state-active"),this.options.role&&this.element.attr("aria-activedescendant",g.attr("id")),k=this.active.parent().closest(".ui-menu-item").children(".ui-menu-item-wrapper"),this._addClass(k,null,"ui-state-active"),f&&"keydown"===f.type?this._close():this.timer=this._delay(function(){this._close()
},this.delay),d=h.children(".ui-menu"),d.length&&f&&/^mouse/.test(f.type)&&this._startOpening(d),this.activeMenu=h.parent(),this._trigger("focus",f,{item:h})
},_scrollIntoView:function(k){var f,g,m,l,d,h;
this._hasScroll()&&(f=parseFloat(b.css(this.activeMenu[0],"borderTopWidth"))||0,g=parseFloat(b.css(this.activeMenu[0],"paddingTop"))||0,m=k.offset().top-this.activeMenu.offset().top-f-g,l=this.activeMenu.scrollTop(),d=this.activeMenu.height(),h=k.outerHeight(),0>m?this.activeMenu.scrollTop(l+m):m+h>d&&this.activeMenu.scrollTop(l+m-d+h))
},blur:function(d,f){f||clearTimeout(this.timer),this.active&&(this._removeClass(this.active.children(".ui-menu-item-wrapper"),null,"ui-state-active"),this._trigger("blur",d,{item:this.active}),this.active=null)
},_startOpening:function(d){clearTimeout(this.timer),"true"===d.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(d)
},this.delay))
},_open:function(f){var d=b.extend({of:this.active},this.options.position);
clearTimeout(this.timer),this.element.find(".ui-menu").not(f.parents(".ui-menu")).hide().attr("aria-hidden","true"),f.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(d)
},collapseAll:function(f,d){clearTimeout(this.timer),this.timer=this._delay(function(){var e=d?this.element:b(f&&f.target).closest(this.element.find(".ui-menu"));
e.length||(e=this.element),this._close(e),this.blur(f),this._removeClass(e.find(".ui-state-active"),null,"ui-state-active"),this.activeMenu=e
},this.delay)
},_close:function(d){d||(d=this.active?this.active.parent():this.element),d.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false")
},_closeOnDocumentClick:function(d){return !b(d.target).closest(".ui-menu").length
},_isDivider:function(d){return !/[^\-\u2014\u2013\s]/.test(d.text())
},collapse:function(d){var f=this.active&&this.active.parent().closest(".ui-menu-item",this.element);
f&&f.length&&(this._close(),this.focus(d,f))
},expand:function(d){var f=this.active&&this.active.children(".ui-menu ").find(this.options.items).first();
f&&f.length&&(this._open(f.parent()),this._delay(function(){this.focus(d,f)
}))
},next:function(d){this._move("next","first",d)
},previous:function(d){this._move("prev","last",d)
},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length
},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length
},_move:function(f,h,d){var g;
this.active&&(g="first"===f||"last"===f?this.active["first"===f?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[f+"All"](".ui-menu-item").eq(0)),g&&g.length&&this.active||(g=this.activeMenu.find(this.options.items)[h]()),this.focus(d,g)
},nextPage:function(g){var d,f,h;
return this.active?(this.isLastItem()||(this._hasScroll()?(f=this.active.offset().top,h=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return d=b(this),0>d.offset().top-f-h
}),this.focus(g,d)):this.focus(g,this.activeMenu.find(this.options.items)[this.active?"last":"first"]())),void 0):(this.next(g),void 0)
},previousPage:function(g){var d,f,h;
return this.active?(this.isFirstItem()||(this._hasScroll()?(f=this.active.offset().top,h=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return d=b(this),d.offset().top-f+h>0
}),this.focus(g,d)):this.focus(g,this.activeMenu.find(this.options.items).first())),void 0):(this.next(g),void 0)
},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")
},select:function(f){this.active=this.active||b(f.target).closest(".ui-menu-item");
var d={item:this.active};
this.active.has(".ui-menu").length||this.collapseAll(f,!0),this._trigger("select",f,d)
},_filterMenuItems:function(g){var d=g.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&"),f=RegExp("^"+d,"i");
return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function(){return f.test(b.trim(b(this).children(".ui-menu-item-wrapper").text()))
})
}}),b.widget("ui.autocomplete",{version:"1.12.1",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},requestIndex:0,pending:0,_create:function(){var h,f,g,l=this.element[0].nodeName.toLowerCase(),k="textarea"===l,d="input"===l;
this.isMultiLine=k||!d&&this._isContentEditable(this.element),this.valueMethod=this.element[k||d?"val":"text"],this.isNewMenu=!0,this._addClass("ui-autocomplete-input"),this.element.attr("autocomplete","off"),this._on(this.element,{keydown:function(m){if(this.element.prop("readOnly")){return h=!0,g=!0,f=!0,void 0
}h=!1,g=!1,f=!1;
var e=b.ui.keyCode;
switch(m.keyCode){case e.PAGE_UP:h=!0,this._move("previousPage",m);
break;
case e.PAGE_DOWN:h=!0,this._move("nextPage",m);
break;
case e.UP:h=!0,this._keyEvent("previous",m);
break;
case e.DOWN:h=!0,this._keyEvent("next",m);
break;
case e.ENTER:this.menu.active&&(h=!0,m.preventDefault(),this.menu.select(m));
break;
case e.TAB:this.menu.active&&this.menu.select(m);
break;
case e.ESCAPE:this.menu.element.is(":visible")&&(this.isMultiLine||this._value(this.term),this.close(m),m.preventDefault());
break;
default:f=!0,this._searchTimeout(m)
}},keypress:function(e){if(h){return h=!1,(!this.isMultiLine||this.menu.element.is(":visible"))&&e.preventDefault(),void 0
}if(!f){var m=b.ui.keyCode;
switch(e.keyCode){case m.PAGE_UP:this._move("previousPage",e);
break;
case m.PAGE_DOWN:this._move("nextPage",e);
break;
case m.UP:this._keyEvent("previous",e);
break;
case m.DOWN:this._keyEvent("next",e)
}}},input:function(e){return g?(g=!1,e.preventDefault(),void 0):(this._searchTimeout(e),void 0)
},focus:function(){this.selectedItem=null,this.previous=this._value()
},blur:function(e){return this.cancelBlur?(delete this.cancelBlur,void 0):(clearTimeout(this.searching),this.close(e),this._change(e),void 0)
}}),this._initSource(),this.menu=b("<ul>").appendTo(this._appendTo()).menu({role:null}).hide().menu("instance"),this._addClass(this.menu.element,"ui-autocomplete","ui-front"),this._on(this.menu.element,{mousedown:function(m){m.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur,this.element[0]!==b.ui.safeActiveElement(this.document[0])&&this.element.trigger("focus")
})
},menufocus:function(p,m){var o,q;
return this.isNewMenu&&(this.isNewMenu=!1,p.originalEvent&&/^mouse/.test(p.originalEvent.type))?(this.menu.blur(),this.document.one("mousemove",function(){b(p.target).trigger(p.originalEvent)
}),void 0):(q=m.item.data("ui-autocomplete-item"),!1!==this._trigger("focus",p,{item:q})&&p.originalEvent&&/^key/.test(p.originalEvent.type)&&this._value(q.value),o=m.item.attr("aria-label")||q.value,o&&b.trim(o).length&&(this.liveRegion.children().hide(),b("<div>").text(o).appendTo(this.liveRegion)),void 0)
},menuselect:function(p,m){var o=m.item.data("ui-autocomplete-item"),q=this.previous;
this.element[0]!==b.ui.safeActiveElement(this.document[0])&&(this.element.trigger("focus"),this.previous=q,this._delay(function(){this.previous=q,this.selectedItem=o
})),!1!==this._trigger("select",p,{item:o})&&this._value(o.value),this.term=this._value(),this.close(p),this.selectedItem=o
}}),this.liveRegion=b("<div>",{role:"status","aria-live":"assertive","aria-relevant":"additions"}).appendTo(this.document[0].body),this._addClass(this.liveRegion,null,"ui-helper-hidden-accessible"),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")
}})
},_destroy:function(){clearTimeout(this.searching),this.element.removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()
},_setOption:function(d,f){this._super(d,f),"source"===d&&this._initSource(),"appendTo"===d&&this.menu.element.appendTo(this._appendTo()),"disabled"===d&&f&&this.xhr&&this.xhr.abort()
},_isEventTargetInWidget:function(f){var d=this.menu.element[0];
return f.target===this.element[0]||f.target===d||b.contains(d,f.target)
},_closeOnClickOutside:function(d){this._isEventTargetInWidget(d)||this.close()
},_appendTo:function(){var d=this.options.appendTo;
return d&&(d=d.jquery||d.nodeType?b(d):this.document.find(d).eq(0)),d&&d[0]||(d=this.element.closest(".ui-front, dialog")),d.length||(d=this.document[0].body),d
},_initSource:function(){var g,d,f=this;
b.isArray(this.options.source)?(g=this.options.source,this.source=function(e,h){h(b.ui.autocomplete.filter(g,e.term))
}):"string"==typeof this.options.source?(d=this.options.source,this.source=function(h,k){f.xhr&&f.xhr.abort(),f.xhr=b.ajax({url:d,data:h,dataType:"json",success:function(e){k(e)
},error:function(){k([])
}})
}):this.source=this.options.source
},_searchTimeout:function(d){clearTimeout(this.searching),this.searching=this._delay(function(){var h=this.term===this._value(),f=this.menu.element.is(":visible"),g=d.altKey||d.ctrlKey||d.metaKey||d.shiftKey;
(!h||h&&!f&&!g)&&(this.selectedItem=null,this.search(null,d))
},this.options.delay)
},search:function(d,f){return d=null!=d?d:this._value(),this.term=this._value(),d.length<this.options.minLength?this.close(f):this._trigger("search",f)!==!1?this._search(d):void 0
},_search:function(d){this.pending++,this._addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:d},this._response())
},_response:function(){var d=++this.requestIndex;
return b.proxy(function(e){d===this.requestIndex&&this.__response(e),this.pending--,this.pending||this._removeClass("ui-autocomplete-loading")
},this)
},__response:function(d){d&&(d=this._normalize(d)),this._trigger("response",null,{content:d}),!this.options.disabled&&d&&d.length&&!this.cancelSearch?(this._suggest(d),this._trigger("open")):this._close()
},close:function(d){this.cancelSearch=!0,this._close(d)
},_close:function(d){this._off(this.document,"mousedown"),this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",d))
},_change:function(d){this.previous!==this._value()&&this._trigger("change",d,{item:this.selectedItem})
},_normalize:function(d){return d.length&&d[0].label&&d[0].value?d:b.map(d,function(f){return"string"==typeof f?{label:f,value:f}:b.extend({},f,{label:f.label||f.value,value:f.value||f.label})
})
},_suggest:function(f){var d=this.menu.element.empty();
this._renderMenu(d,f),this.isNewMenu=!0,this.menu.refresh(),d.show(),this._resizeMenu(),d.position(b.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next(),this._on(this.document,{mousedown:"_closeOnClickOutside"})
},_resizeMenu:function(){var d=this.menu.element;
d.outerWidth(Math.max(d.width("").outerWidth()+1,this.element.outerWidth()))
},_renderMenu:function(g,d){var f=this;
b.each(d,function(h,e){f._renderItemData(g,e)
})
},_renderItemData:function(d,f){return this._renderItem(d,f).data("ui-autocomplete-item",f)
},_renderItem:function(f,d){return b("<li>").append(b("<div>").text(d.label)).appendTo(f)
},_move:function(d,f){return this.menu.element.is(":visible")?this.menu.isFirstItem()&&/^previous/.test(d)||this.menu.isLastItem()&&/^next/.test(d)?(this.isMultiLine||this._value(this.term),this.menu.blur(),void 0):(this.menu[d](f),void 0):(this.search(null,f),void 0)
},widget:function(){return this.menu.element
},_value:function(){return this.valueMethod.apply(this.element,arguments)
},_keyEvent:function(d,f){(!this.isMultiLine||this.menu.element.is(":visible"))&&(this._move(d,f),f.preventDefault())
},_isContentEditable:function(d){if(!d.length){return !1
}var f=d.prop("contentEditable");
return"inherit"===f?this._isContentEditable(d.parent()):"true"===f
}}),b.extend(b.ui.autocomplete,{escapeRegex:function(d){return d.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")
},filter:function(g,d){var f=RegExp(b.ui.autocomplete.escapeRegex(d),"i");
return b.grep(g,function(e){return f.test(e.label||e.value||e)
})
}}),b.widget("ui.autocomplete",b.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(d){return d+(d>1?" results are":" result is")+" available, use up and down arrow keys to navigate."
}}},__response:function(f){var d;
this._superApply(arguments),this.options.disabled||this.cancelSearch||(d=f&&f.length?this.options.messages.results(f.length):this.options.messages.noResults,this.liveRegion.children().hide(),b("<div>").text(d).appendTo(this.liveRegion))
}}),b.ui.autocomplete
});
(function(a){sl=window.sl||{};
sl.utils={createCookie:function(c,f,b,g,e){var d=c+"="+escape(f)+";";
if(b){if(b instanceof Date){if(isNaN(b.getTime())){b=new Date()
}}else{b=new Date(new Date().getTime()+parseInt(b)*1000*60*60*24)
}d+="expires="+b.toGMTString()+";"
}if(g){d+="path="+g+";"
}else{d+="path=/;"
}if(e){d+="domain="+e+";"
}document.cookie=d
},deleteCookie:function(b){document.cookie=b+"=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;"
},getCookies:function(){var b={};
document.cookie.split(";").forEach(function(e,d){var c=e.trim().split(/=(.+)?/);
b[c[0]]=c[1]
});
return b
},parseCookie:function(d,c){var b={};
c=c?c:"|";
decodeURIComponent(d).split(c).forEach(function(f){var e=f.split("=");
b[e[0]]=e[1]
});
return b
},getURLParams:function(){var c=location.search.replace("?","").split("&"),b={};
c.forEach(function(e){var d=e.split("=");
b[d[0]]=d[1]
});
return b
}};
sl.components=sl.components||{};
sl.components.filterableList={init:function(){a(".slbs2-filterable-list table").each(function(){var d=a(this).DataTable({searching:false,paging:false,info:false});
a(this).find(".filter").on("keyup",function(){d.search(this.value).draw()
});
var e={},h=d.columns().data(),b=d.columns().header();
for(var g=0;
g<h.length;
g++){var l=h[g],k=a(b[g]).text();
e[k]=[];
for(var f=0;
f<l.length;
f++){var c=l[f];
e[k].push(c)
}}})
}};
sl.components.modals={init:function(){var b=this;
a(document).on("closed.zf.reveal","[data-reveal]",function(){a("#modal .modal-content").empty()
});
a(".open-video").on("click",function(f){f.preventDefault();
var d=false;
if(a(this).attr("href").indexOf("http")>-1){d=true
}var c='<div id="video-modal"><div class="video-wrapper">';
if(d){var g=a(this).attr("href").replace(/http(s)?:\/\/(www\.)(youtu\.be|youtube\.com)?(\/watch\?v=|\/v\/)/g,"");
c+='<iframe width="800" src="https://www.youtube.com/embed/'+g+'?autoplay=1" frameborder="0" allowfullscreen></iframe>'
}else{if(a(this).find(".video-template").length>0){c+=a(this).find(".video-template").html()
}else{c+='<video controls autoplay poster="'+a(this).attr("data-video-poster")+'"><source src="'+a(this).attr("href")+'" type="video/mp4"></video>'
}}c+="</div></div>";
b.create(c);
a("#modal").foundation("open");
return false
})
},create:function(b){a("#modal .modal-content").html(b)
},close:function(b){if(!b){b="modalContent"
}a("#"+b).close()
}};
a(document).on("ready",function(){a(".slick-carousel").each(function(){a(this).slick()
});
sl.components.lazyLoading.init();
sl.components.videoGallery.init();
sl.components.shadowBox.init();
sl.components.highlightsCarousel.init();
sl.components.deviceTable.init();
sl.components.productFamilyTable.init();
sl.components.supportsearch.init();
sl.components.technicalResources.init();
sl.components.contentList.init();
sl.components.l4devicepage.init();
sl.components.facetedSearch.init();
sl.components.anchor.init();
sl.components.parametricSearch.init();
sl.components.contactSalesSidebar.init();
sl.components.contactSales.init();
sl.components.marquee.init();
sl.components.horizontalseparator.init();
if(!isEdit&&!isDesign){sl.components.mobileOnlyAccordion.init();
sl.components.filterableList.init();
sl.components.herocarousel.init();
sl.components.stickyBar.init();
sl.components.modals.init();
sl.components.downloadModal.init();
sl.components.tables.init();
sl.components.links.init();
sl.components.loginContainer.init();
sl.components.tablePopup.init();
sl.components.helppopup.init();
sl.components.oscillatorsearch.init()
}else{console.warn("EDIT MODE: No custom scripts running")
}});
a(document).on("sl.reinit",function(){sl.components.deviceTable.init(true)
})
})(jQuery);
sl=sl||{};
sl.components=sl.components||{};
sl.components.l4devicepage={init:function(){var a=this;
if($(".part-group-switcher").length>0){$(".part-group-switcher input, .part-group-switcher select").on("change",function(n){n.preventDefault();
var d=$(this),c=d.parents("fieldset"),t=d.parents("form"),l=t.find("[name] :selected, [name]:checked"),f="";
for(var k=0;
k<l.length;
k++){f+=l.eq(k).val();
if(k<l.length-1){f+="|"
}}c.find("[value]").each(function(){$(this).attr("disabled",false)
});
if(f&&window.sl.deviceMap){if(typeof window.sl.deviceMap[f]!=="undefined"){t.find("[value]").each(function(){$(this).attr("disabled",false)
});
if(window.location.pathname.match(".html$")){var r=window.sl.deviceMap[f].name,p=window.location.pathname.replace(/(.+\.)(.+)(\.html)/g,"$1"+r+"$3");
if(window.location.pathname!==p){window.location.pathname=window.location.pathname.replace(/(.+\.)(.+)(\.html)/g,"$1"+r+"$3")
}}else{var r=window.sl.deviceMap[f].name,p=window.location.pathname.replace(/(.+\.)(.+)$/g,"$1"+r);
if(window.location.pathname!==p){window.location.pathname=window.location.pathname.replace(/(.+\.)(.+)$/g,"$1"+r)
}}}else{var b=c.siblings();
b.addClass("error");
b.find("[value]").each(function(){$(this).attr("disabled",true)
});
var o=window.sl.deviceMap;
for(var q in o){var g=q.split("|"),m=d.val(),h=c.index();
if(g[h]==m){for(var k=0;
k<g.length;
k++){var s=g[k];
t.children("fieldset").eq(k).find('[value="'+s+'"]').each(function(){$(this).attr("disabled",false)
})
}}}}t.find("fieldset").each(function(){if($(this).find("[disabled]:selected, [disabled]:checked").length==0){$(this).removeClass("error");
$(this).find(".error").removeClass("error")
}else{$(this).addClass("error")
}})
}})
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.contactSalesSidebar={init:function(){var f=$("#contact-sales");
if(f.length>0){var b=f.attr("data-url"),c=null,g=null,e=null;
var d=window.digitalData;
if(d&&d.products){var h=d.products[0];
c=h.productInfo.productLine;
productCategory=h.productInfo.productCategory;
g=h.productInfo.productFamily;
e=h.productInfo.partNumber;
var a="productLine="+c+"|productFamily="+g+"|productCategory="+productCategory+"|partNumber="+e+"|referrerUrl="+window.location.href;
sl.utils.createCookie("contactSales",a)
}}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.l4devicepage={init:function(){var a=this;
if($(".part-group-switcher").length>0){$(".part-group-switcher input, .part-group-switcher select").on("change",function(n){n.preventDefault();
var d=$(this),c=d.parents("fieldset"),t=d.parents("form"),l=t.find("[name] :selected, [name]:checked"),f="";
for(var k=0;
k<l.length;
k++){f+=l.eq(k).val();
if(k<l.length-1){f+="|"
}}c.find("[value]").each(function(){$(this).attr("disabled",false)
});
if(f&&window.sl.deviceMap){if(typeof window.sl.deviceMap[f]!=="undefined"){t.find("[value]").each(function(){$(this).attr("disabled",false)
});
if(window.location.pathname.match(".html$")){var r=window.sl.deviceMap[f].name,p=window.location.pathname.replace(/(.+\.)(.+)(\.html)/g,"$1"+r+"$3");
if(window.location.pathname!==p){window.location.pathname=window.location.pathname.replace(/(.+\.)(.+)(\.html)/g,"$1"+r+"$3")
}}else{var r=window.sl.deviceMap[f].name,p=window.location.pathname.replace(/(.+\.)(.+)$/g,"$1"+r);
if(window.location.pathname!==p){window.location.pathname=window.location.pathname.replace(/(.+\.)(.+)$/g,"$1"+r)
}}}else{var b=c.siblings();
b.addClass("error");
b.find("[value]").each(function(){$(this).attr("disabled",true)
});
var o=window.sl.deviceMap;
for(var q in o){var g=q.split("|"),m=d.val(),h=c.index();
if(g[h]==m){for(var k=0;
k<g.length;
k++){var s=g[k];
t.children("fieldset").eq(k).find('[value="'+s+'"]').each(function(){$(this).attr("disabled",false)
})
}}}}t.find("fieldset").each(function(){if($(this).find("[disabled]:selected, [disabled]:checked").length==0){$(this).removeClass("error");
$(this).find(".error").removeClass("error")
}else{$(this).addClass("error")
}})
}})
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.contactSalesSidebar={init:function(){var f=$("#contact-sales");
if(f.length>0){var b=f.attr("data-url"),c=null,g=null,e=null;
var d=window.digitalData;
if(d&&d.products){var h=d.products[0];
c=h.productInfo.productLine;
productCategory=h.productInfo.productCategory;
g=h.productInfo.productFamily;
e=h.productInfo.partNumber;
var a="productLine="+c+"|productFamily="+g+"|productCategory="+productCategory+"|partNumber="+e+"|referrerUrl="+window.location.href;
sl.utils.createCookie("contactSales",a)
}}}};
sl=sl||{};
sl.components=sl.components||{};
var videoContainerIds=[];
sl.components.videoGallery={init:function(){var b=$(".slbs2-video-gallery");
var a=false;
b.each(function(){var e=$(this),c=e.find(".slick-video-thumbnail-carousel"),d=e.find(".main-video");
d.on("init reInit afterChange",function(){a=true;
sl.components.videoGallery.videos.init(d)
});
d.on("beforeChange",function(){sl.components.videoGallery.videos.pauseAll()
});
c.on("init reInit afterChange",function(){if(!a){sl.components.videoGallery.videos.init(d)
}else{a=false
}});
c.slick({asNavFor:d,slidesToShow:5,focusOnSelect:true,centerMode:true,centerPadding:"0",responsive:[{breakpoint:1024,settings:{slidesToShow:3}},{breakpoint:688,settings:{centerMode:false,slidesToShow:2}}]});
sl.components.videoGallery.checkSlickNoSlide(c);
$(window).resize(function(){window.setTimeout(function(){sl.components.videoGallery.checkSlickNoSlide(c)
},200)
})
});
sl.components.videoGallery.videos.prep(videoContainerIds)
},checkSlickNoSlide:function(a){var b=a.slick("getSlick");
if(b.slideCount<=b.options.slidesToShow){a.addClass("slick-no-slide")
}else{a.removeClass("slick-no-slide")
}},videos:{prep:function(a){$(".slick-slide:not(.slick-cloned) .video-viewer").each(function(c,b){var d=JSON.parse($(b).attr("data-video-settings"));
a.push(d.containerId)
})
},init:function(b){if(b.find(".slick-active").children(".video-viewer").length>0){var a=b.find(".slick-active").children(".video-viewer").attr("id");
if(window.sl.pageProperties.videoViewers){if(!window.sl.pageProperties.videoViewers[a].initCalled){window.sl.pageProperties.videoViewers[a].init()
}else{if(Foundation.MediaQuery.atLeast("medium")){sl.components.videoGallery.videos.playActive(b)
}}}else{window.sl.pageProperties.videoViewersToInit={};
window.sl.pageProperties.videoViewersToInit[a]=true
}}},playActive:function(c,b){var d=JSON.parse(c.find(".slick-active").find(".video-viewer").attr("data-video-settings")),a=window.sl.pageProperties.videoViewers[d.containerId];
if(!a.initializationComplete){return false
}var e=window.sl.pageProperties.videoViewers[d.containerId].getComponent("videoPlayer");
if(b&&d.autoplay==="0"){return false
}if(e){if((e.getDuration()-e.getCurrentTime())<=1){e.seek(0)
}e.play()
}},pauseAll:function(){$.each(videoContainerIds,function(b,a){if(window.sl.pageProperties.videoViewers[a].initializationComplete&&window.sl.pageProperties.videoViewers[a].getComponent("videoPlayer")){window.sl.pageProperties.videoViewers[a].getComponent("videoPlayer").pause()
}})
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.mobileOnlyAccordion={init:function(){var a=this;
a.testWindowSize();
$(window).on("resize",function(){if($(window).width()!==a.width){a.testWindowSize()
}})
},width:$(window).width(),height:$(window).height(),testWindowSize:function(){var a=$(".slbs2-mobile-only-accordion");
var b="medium";
a.each(function(){var d=$(this);
if(d.hasClass("accordion-medium")){b="large"
}if(Foundation.MediaQuery.atLeast(b)){if(typeof d.data().zfPlugin!=="undefined"){d.foundation("destroy")
}d.find(".accordion-title").on("click.overrideAccordion",function(f){f.preventDefault();
return false
})
}else{if(!d.hasClass("default-closed")&&d.parents('[class^="column"]:first-child, .parsys_column:first-child').length>0){if(!d.find(".accordion-item").hasClass("is-active")){d.find(".accordion-item").addClass("is-active")
}}var c=new Foundation.Accordion(a);
d.find(".accordion-title").unbind("click.overrideAccordion")
}})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.technicalResources={init:function(){var b=this,a=$(".technical-resources table");
a.each(function(){$(this).DataTable({searching:false,paging:false,info:false})
})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.tables={init:function(){var c=this,b=$(".slbs2-table"),a=b.outerWidth();
c.testForScroll(b);
$(window).on("resize",function(){c.testForScroll(b);
b.each(function(){var d=$(this).find(".table-scroll");
if(d.length>0){c.testScrollEnd(d)
}})
});
b.find(".table-scroll").on("scroll",function(){var d=$(this);
c.testScrollEnd(d)
})
},testScrollEnd:function(a){if(a[0].scrollWidth-a.scrollLeft()===a.width()){a.parent(".table-container").addClass("scroll-end")
}else{a.parent(".table-container").removeClass("scroll-end")
}},testForScroll:function(a){a.each(function(){var d=$(this),c=d.find(".table-scroll");
if(c.length>0){var b=c[0].scrollWidth;
if(b){if($("body").hasClass("ie")){b=b-1
}if(b>c.width()){d.addClass("scrollable")
}else{d.removeClass("scrollable")
}}}})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.supportsearch={init:function(){var a=$(".support-search");
$("input.supportsearch").autocomplete({source:function(c,b){$.getJSON("https://search.silabs.com/suggest?callback=?",{site:"english",q:c.term,format:"os",max:"10",client:"silabs"}).done(function(f){var e=[];
var d=0;
$.each(f[1],function(g,h){if(h&&h!=""){e.push({value:h});
d++
}});
e.length=d;
b(e)
}).fail(function(e,g,f){var d=[];
d.length=0;
b(d);
console.log("getJSON request failed! "+g)
})
},appendTo:".support-search"})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.stickyBar={init:function(){var b=this,a=$(".slbs2-sticky-bar");
$.each(a,function(){b.toStickOrNotToStick($(this))
});
$(window).on("scroll",function(){$.each(a,function(){b.toStickOrNotToStick($(this))
})
})
},toStickOrNotToStick:function(a){var c=function(){if(Foundation.MediaQuery.atLeast("large")){return $(window).scrollTop()>a.offset().top
}else{return $(window).scrollTop()>a.offset().top-$(".slbs2-header .nav-container").outerHeight()
}};
if(c()){if(a.next(".clone").length===0){var b=a.clone().addClass("clone stuck");
a.after(b)
}}else{a.next(".clone").remove()
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.shadowBox={init:function(){$(window).on("resize",function(){var b=$(".slbs2-shadowbox");
if(b.length){var a=b.height();
if((a+100)>window.innerHeight){b.addClass("no-center")
}else{b.removeClass("no-center")
}}})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.productFamilyTable={init:function(){var b=this,a=$(".product-family-table");
$(".filter-section input.range-filter-input").each(function(){$(this).attr("size",$(this).attr("placeholder").length)
});
a.find("input[type=text][data-filter]").on("input",function(){var c=$(this).parents(".product-family-table");
b.doClick($(this),c,false)
});
a.find("input[type=checkbox][data-filter], select[data-filter]").on("change",function(){var c=$(this).parents(".product-family-table");
b.doClick($(this),c,false)
});
a.find("button[data-filter], a[data-filter]").on("click",function(d){if(Foundation.MediaQuery.atLeast("medium")){d.preventDefault()
}var c=$(this).parents(".product-family-table");
b.doClick($(this),c,true)
})
},doClick:function(f,a,e){var c=this,b=f,d=b.data("filter");
if(d===""){a.find("[data-filter]").removeClass("active").prop("checked",false).val("")
}a.find("tr").removeClass("active secondary-active");
if(c.isFilterActive(b)){b.addClass("active")
}else{b.removeClass("active")
}var g=c.getActiveFilters(a);
var e=f.is(".secondary-filter");
if(Object.keys(g).length>0){a.addClass("filtering")
}else{a.removeClass("filtering")
}a.find("tbody tr").each(function(){var p={};
var n=$(this)[0].attributes;
for(var o=0;
o<n.length;
o++){var q=n.item(o);
var l=q.name.replace("data-","").replace("sl-","sl");
var r=q.value;
var h=function(s){return !isNaN(s)
};
var k=function(s){return(s==="true"||s==="false")
};
var r=(h(r)?+r:k(r)?(String(r).toLowerCase()==="true"):r);
p[l]=r
}var m=c.getActiveRowClasses(a,g,p,e);
$(this).addClass(m)
});
if(a.find(".filters .secondary .active").length>0){a.addClass("filtering-secondary")
}else{a.removeClass("filtering-secondary")
}if(a.find(".filters .primary .active").length>0){a.addClass("filtering-primary")
}else{a.removeClass("filtering-primary")
}},getActiveFilters:function(a){var c={},b=a.find("[data-filter].active");
b.each(function(){var g=$(this);
if(g.data("filter")){var h=g.attr("data-filter"),m=true,l=h.replace("sl:","sl");
if(g.is("input[type=checkbox]")&&g.is(":checked")){if(g.val()!=="on"){m=g.val()
}}else{if(g.is("input.range-filter-input")){var e=0,k=g.parents(".filter-fields").find("[data-min-value]").attr("data-min-value"),d=g.parents(".filter-fields").find("[data-max-value]").attr("data-max-value"),f=g.parent().siblings("label").children("input").val();
if(f==""){if(g.hasClass("range-filter-input-min")){m=g.val()+"-"+d
}else{if(g.hasClass("range-filter-input-max")){m=k+"-"+g.val()
}}}else{if(g.hasClass("range-filter-input-min")){m=g.val()+"-"+f
}else{if(g.hasClass("range-filter-input-max")){m=f+"-"+g.val()
}}}}else{if(g.is("select")){m=g.val().join("|")
}else{if(g.val()!==null&&g.val()!==""){m=g.val()
}}}}if(c[l]&&!g.is("input.range-filter-input")){c[l]=c[l]+"|"+m
}else{c[l]=m
}}});
return c
},isFilterActive:function(a){var b=false;
if(a.is("input[type=checkbox]")&&a.is(":checked")){b=true
}else{if(a.is("input:not([type=checkbox]), select")&&a.val()!==null&&a.val()!==""){b=true
}else{if(a.is("button, a")&&!a.hasClass("active")){b=true
}}}return b
},getActiveRowClasses:function(f,b,t){var s="",e=[],q=[];
for(var r in b){var d=false,a=false,n=false,c=b[r],l=r.replace("sl","sl:");
d=f.find('[data-filter="'+l+'"].active').is(".secondary-filter");
a=f.find('[data-filter="'+l+'"].active').parents(".primary").length>0;
if(typeof t[r]!=="undefined"&&t[r]!==null){if(typeof c=="string"){var u=t[r].length>1?t[r].split("|"):[t[r].toString()];
if(c.match(/[0-9.]+\-[0-9.]+/g)!==null){var h=c.split("-"),m=h[0],o=h[1];
for(var p=0;
p<u.length;
p++){var g=parseFloat(u[p]);
if(m<=g&&g<=o){n=true
}}}else{var h=c.split("|");
for(var p=0;
p<h.length;
p++){var k=h[p];
if(u.indexOf(k)>-1){n=true
}}}}if(t[r].toString().indexOf(c)>-1){n=true
}}if(d){q.push(n)
}if(a){e.push(n)
}}if(e.length>0&&e.indexOf(false)==-1){s+=" active"
}if(q.length>0&&q.indexOf(true)>-1){s+=" secondary-active"
}return s
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.parametricSearch={activeFilters:{},activeStatuses:[],init:function(){var c=this,b=".parametric-search",d=$(b),a=d.find("table");
if(d.length>0){$.fn.dataTableExt.afnFiltering.push(function(h,g,f,k,e){if($("#"+h.sTableId).is(".parametric-search-table")){if(c.isTableRowActive(k)){return true
}return false
}});
d.each(function(){var f=$(this),e=f.attr("id");
c.bindEvents(f);
c.parseSelectors(f);
c.initStatusFilters(f,window.sl.parametricSearch[e]);
if(typeof document.fonts!="undefined"){document.fonts.ready.then(function(){if(document.fonts.status=="loaded"){c.createDataTable(d,e)
}else{document.fonts.load("bold 1rem Open Sans").then(function(){setTimeout(function(){c.createDataTable(d,e)
},10)
})
}})
}else{c.createDataTable(d,e)
}})
}},createDataTable:function(f,a){var e=this,d=e.getColumnPriorities(f),c=f.find("table");
if(c&&c.find("tbody").length>0){var b=typeof c.attr("data-num-per-page")!=="undefined"?parseInt(c.attr("data-num-per-page")):10;
e.datatable=c.DataTable({lengthChange:false,info:false,fixedColumns:{leftColumns:1},scrollX:true,pageLength:b,columns:d,data:window.sl.parametricSearch[a],processing:true,deferRender:true,drawCallback:function(){$(document).trigger("sl.reinit")
}});
e.updateResultCount(f);
f.removeClass("loading")
}},initStatusFilters:function(h,b,g){var d=this,f=h.find(".status-filters"),a=f.closest(".table-container");
for(var c=0;
c<b.length;
c++){var k=b[c];
if(typeof k.status!=="undefined"){var e=k.status;
var l=f.find(".template").clone().addClass("clone").removeClass("template");
l.text(e.abbreviation).attr("title",e.displayName).attr("data-filter",e.propertyValue);
if(f.find('[data-filter="'+e.propertyValue+'"]').length==0){f.append(l);
d.activeStatuses.push(e.propertyValue)
}}}if(f.children("a.clone").length>0){f.removeClass("hide")
}},getColumnPriorities:function(c){var b=[];
var a=c.find("table th");
a.each(function(f){var d=$(this),g=d.attr("data-priority"),e=d.attr("data-attribute-name");
if(e&&e.indexOf(".")>-1){e=e.replace(".","")
}var k={visible:true,width:"auto",data:e,name:e,className:""};
if(f==0){k.width="160px";
k.data="product-info";
k.name="product-info";
k.className="product-info"
}if(typeof g!=="undefined"){var h=c.find(".checkbox-row.template").clone().removeClass("template show-all").addClass("clone").attr("data-col-index",d.index());
h.find("label").text(d.text());
h.find("input").attr("id","");
if(g==0){k.visible=false
}else{if(g==1){h.find("input").prop("checked",true)
}}c.find(".dropdown-menu").append(h)
}b.push(k)
});
return b
},isTableRowActive:function(d){var k=this,c=k.activeFilters,a=[],o=false;
if(d&&d.allAttributes){if(typeof d.status!=="undefined"){if(k.activeStatuses.indexOf(d.status.propertyValue)>-1){a.push(true)
}else{a.push(false)
}}if(Object.keys(c).length>0){for(var n in c){var b=c[n],h=d.allAttributes[b.filter];
if(typeof b.val!=="undefined"&&typeof h!=="undefined"){if(b.val.match(/^(-?[0-9.]*)\-(-?[0-9.]*)$/g)!==null){var m=new RegExp("^(-?[0-9.]*)-(-?[0-9.]*)$","g");
var g=m.exec(b.val),e=g[1],l=g[2];
h=parseFloat(h);
if(e<=h&&h<=l){a.push(true)
}else{a.push(false)
}}else{if(b.val.indexOf("|")>-1){var f=b.val.split("|");
if(f.indexOf(h)>-1){a.push(true)
}else{a.push(false)
}}else{if(h==b.val){a.push(true)
}else{a.push(false)
}}}}else{a.push(false)
}}}}if(a.length==0){o=true
}else{if(a.indexOf(false)==-1){o=true
}}return o
},parseSelectors:function(h){var g=this,d=g.getSelectors();
g.deactivateFilter(h);
if(d.length>0){for(var c=0;
c<d.length;
c++){var f=d[c].split("="),e=f[0],b=f[1],a=e.replace("sl-","sl:");
b=b.replace(/\|/g,".");
b=decodeURIComponent(b);
if(b.indexOf("|")>-1){b=b.split("|")
}g.activateFilter(h,a,b,true)
}}},bindEvents:function(c){var b=this;
var a=(function(){var d=0;
return function(f,e){clearTimeout(d);
d=setTimeout(f,e)
}
})();
$(window).on("resize",function(){if(b.datatable){b.datatable.columns.adjust()
}});
c.find(".maximize").on("click",function(){$("body").toggleClass("parametric-search-maximized");
b.datatable.columns.adjust()
});
c.find("input[type=text][data-filter]").on("input",function(){var d=$(this);
a(function(){var e=d.parents(".parametric-search");
b.doClick(d,e)
},500)
});
c.find("input[type=checkbox][data-filter], select[data-filter]").on("change",function(){var d=$(this).parents(".parametric-search");
b.doClick($(this),d)
});
c.on("click",".label.clone[data-filter], .clear-all",function(f){f.preventDefault();
var d=$(this).parents(".parametric-search");
b.doClick($(this),d,true)
});
c.on("click",".status-filters a",function(f){f.preventDefault();
var d=$(this).attr("data-filter");
if($(this).hasClass("active")){b.activeStatuses.splice(b.activeStatuses.indexOf(d),1);
$(this).removeClass("active")
}else{b.activeStatuses.push(d);
$(this).addClass("active")
}b.datatable.draw();
b.updateResultCount(c)
});
c.on("click",".btn-toolbar .checkbox-row",function(){var n=$(this),d=n.siblings(),f=n.children("input"),o=n.attr("data-col-index");
if(f.is(":checked")){f.prop("checked",false)
}else{f.prop("checked",true)
}if(n.is(".show-all")){if(f.is(":checked")){b.datatable.columns().visible(true);
var l=[];
d.each(function(){l.push($(this).find("input").is(":checked"))
});
localStorage.setItem("psPreviousColumns",l);
d.each(function(){$(this).find("input").prop("checked",true)
})
}else{var g=localStorage.getItem("psPreviousColumns");
var p=g.split(",");
for(var k=0;
k<p.length;
k++){var h=p[k]=="true",m=d.eq(k),e=m.attr("data-col-index");
if(typeof e!=="undefined"){m.find("input").prop("checked",h);
b.datatable.column(e).visible(h,false)
}}b.datatable.columns.adjust().draw(false)
}}else{if(o){if(f.is(":checked")){b.datatable.column(o).visible(true)
}else{d.filter(".show-all").find("input").prop("checked",false);
b.datatable.column(o).visible(false)
}}}});
$(window).on("popstate",function(d){b.parseSelectors(c);
b.datatable.draw();
b.updateResultCount(c)
})
},doClick:function(k,g,c){var e=this,h=k.attr("data-filter"),f=g.find('.parametric-search-filter [data-filter="'+h+'"]'),a=e.getFilterValue(f);
var b=false;
if(k.is(".range-filter-input")){var d=k.parents().siblings("label").children("input");
if(k.val().length===0){b=true
}}else{if(k.is(".active")){b=true
}}if(b||c){e.deactivateFilter(g,k)
}else{e.activateFilter(g,h,a)
}e.datatable.draw();
e.updateResultCount(g)
},activateFilter:function(h,k,a,c){var d=this,e=h.find('.parametric-search-filter [data-filter="'+k+'"]'),b=h.find(".filter-bar"),l=b.children(".label-list");
e.parents("fieldset").addClass("active");
e.each(function(o){var r=$(this);
if(r.is(".range-filter-input")){var q=new RegExp("^(-?[0-9.]*)-(-?[0-9.]*)$","g");
var n=q.exec(a),p=n[1],m=n[2];
if(r.is(".range-filter-input-min")){r.val(p)
}else{if(r.is(".range-filter-input-max")){r.val(m)
}}r.addClass("active")
}else{if(r.is('[type="checkbox"]')){if(typeof a=="object"&&a.indexOf(r.val())>-1){r.prop("checked",true).addClass("active")
}else{if(typeof a=="string"&&a.split("|").indexOf(r.val())>-1){r.prop("checked",true).addClass("active")
}else{if(a=="true"){r.prop("checked",true).addClass("active")
}}}}else{if(r.is("select")){if(typeof a=="string"){r.val(a.split("|"))
}else{r.val(a)
}r.addClass("active")
}}}});
var g={filter:k,displayVal:d.getFilterDisplayValue(e),val:d.getFilterValue(e),title:e.parents("fieldset").children("legend").text().trim(),attribute:k.replace("sl:","data-sl-"),selector:k.replace(":","-")+"="+d.getFilterValue(e)};
d.activeFilters[k]=g;
var f=l.children(".label.template").clone().removeClass("template").addClass("clone");
d.updateLabel(h,f,k);
l.children('.label[data-filter="'+k+'"]').remove();
l.append(f);
if(l.children(".label.clone").length>0){b.show().css("opacity",1);
h.find("a.clear-all").show()
}else{b.hide().css("opacity",0);
h.find("a.clear-all").hide()
}d.updateSelectors(g.selector,null,c)
},deactivateFilter:function(k,d){var e=this,c=k.find(".filter-bar"),m=c.children(".label-list");
if(d==null||(d!==null&&d.attr("data-filter")=="")){var b=k.find(".parametric-search-filter");
b.find('select, input[type="text"]').val(null).removeClass("active");
b.find('input[type="checkbox"]').prop("checked",false).removeClass("active");
b.find("fieldset").removeClass("active");
m.children(".label.clone").remove();
e.updateSelectors("",true);
e.activeFilters={}
}else{var l=d.attr("data-filter"),g=k.find('.parametric-search-filter [data-filter="'+l+'"]'),a=d.parents("fieldset");
var f=false;
if(d.is(".label")){d=g;
f=true
}d.removeClass("active");
if(d.is('[type="text"]')){d.val("")
}else{if(d.is('[type="checkbox"]')){d.prop("checked",false)
}else{if(d.is("select")){if(f){d.val(null)
}if(d.val()!==null){d.addClass("active")
}}}}var h=m.children('.label.clone[data-filter="'+l+'"]');
if(a.find(".active").length==0){a.removeClass("active");
h.remove();
e.updateSelectors(e.activeFilters[l].selector,true);
delete e.activeFilters[l]
}else{e.activeFilters[l].val=e.getFilterValue(g);
e.activeFilters[l].displayVal=e.getFilterDisplayValue(g);
e.activeFilters[l].selector=l.replace(":","-")+"="+e.getFilterValue(g);
e.updateLabel(k,h,l);
e.updateSelectors(e.activeFilters[l].selector)
}}if(m.children(".label.clone").length>0){c.show().css("opacity",1);
k.find("a.clear-all").show()
}else{c.hide().css("opacity",0);
k.find("a.clear-all").hide()
}},updateLabel:function(e,a,c){var d=this,b=d.activeFilters[c].displayVal.length>0?": ":"";
a.text(d.activeFilters[c].title+b+d.activeFilters[c].displayVal).attr("data-filter",c).attr("data-filter-val",d.activeFilters[c].val)
},getSelectors:function(){var b=[];
var c=location.pathname.split("/ps-filter/");
if(typeof c[1]!=="undefined"&&c[1].length>0){var a=c[1].split("/");
if(a!==null){a.forEach(function(d){d=d.replace("/","");
b.push(d)
})
}}return b
},updateSelectors:function(b,h,g){var l=this,c=location.pathname,n=h&&h===true,d=b;
var e=l.getSelectors();
if(d==""){e=[]
}else{if(d.indexOf("=")>-1){var p=d.split("=")[0],a=d.split("=")[1];
for(var f=0;
f<e.length;
f++){var k=e[f],o=k.split("=")[0],m=k.split("=")[1];
if(o==p){if(a==""||n){e.splice(f,1)
}else{e[f]=d
}}}if(!n&&e.indexOf(d)==-1&&a!==""){e.push(d)
}}}e.sort();
var b=e.join("/");
if(b.length>0){b="/ps-filter/"+b
}if(c.indexOf("/ps-filter/")>-1){c=c.replace(/(.+?)(\/ps-filter\/)(.*)$/g,"$1"+b)
}else{c=c+b
}if(location.pathname!==encodeURI(c)){if(g){window.history.replaceState({url:c},null,c+location.search)
}else{window.history.pushState({url:c},null,c+location.search)
}}},updateResultCount:function(c){var b=this,a=b.datatable.rows({search:"applied"});
c.find(".result-count").text(a[0].length)
},containsActiveFilters:function(b){var a=false;
b.find("input, select").each(function(){if(($(this).is("[type=checkbox]")&&$(this).is(":checked"))||(!$(this).is("[type=checkbox]")&&$(this).val()&&$(this).val().length>0)){a=true
}});
return a
},getFilterValue:function(b){var c=this;
if(b){if(b.is(".label")){return b.attr("data-filter-val")
}else{if(b.is(".switch-input")){var a=b.val()==="on";
return a.toString()
}else{return c.getFilterDisplayValue(b)
}}}},getFilterDisplayValue:function(a){var c=this,b="";
a.each(function(){var h=$(this),d=h.parents("fieldset"),k=h.val();
if(d.is(".filter-boolean")){if(a.is(":checked")){}}else{if(d.is(".filter-number")){var n=0,o=d.find("[data-min-value]").attr("data-min-value"),e=d.find("[data-max-value]").attr("data-max-value"),m=a.parents().siblings("label").children("input").val(),f=h.is(".range-filter-input-min"),l=h.is(".range-filter-input-max");
if(k.length>0||m.length>0){if(f){k=k.length>0?k:o;
m=m.length>0?m:e;
b=k+"-"+m
}else{if(l){k=k.length>0?k:e;
m=m.length>0?m:o;
b=m+"-"+k
}}}}else{if(d.is(".filter-string")){if(k!==null&&typeof k==="object"){b=k.join("|")
}else{var g=[];
a.filter(":checked").each(function(){var p=$(this);
g.push(p.val())
});
b=g.join("|")
}}}}});
return b
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.downloadModal={init:function(){if(document.location.search.indexOf("download=")>-1){var c={};
document.location.search.replace("?","").split("&").forEach(function(k,h){var g=k.trim().split("=");
c[g[0]]=g[1]
});
var a=decodeURIComponent(c.download).replace(/\+/g," ").split("|"),f=a[0],e=a[1];
var b=$(".download-modal").clone().addClass("clone small");
b.find(".download-name").text(e);
var d=new Foundation.Reveal(b);
b.foundation("open");
b.on("closed.zf.reveal",function(){delete c.download;
var g=$.param(c).length>0?"?"+$.param(c):"";
window.history.pushState(null,null,document.location.pathname+g+document.location.hash)
});
setTimeout(function(){$("body").append('<iframe class="hide" src="'+f+'" frameborder="0"></iframe>')
},600)
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.helppopup={init:function(){var a=this;
$(document).on("click","a.help-icon",function(){var c=$(this),d=c.attr("data-attr"),b=c.attr("data-attr-image"),e=c.attr("data-attr-more");
if(typeof c.attr("href")=="undefined"||c.attr("href")==""){c.addClass("active")
}a.util.createPopup();
a.util.addTheDataToPopup(d,b,e)
});
$(".slbs2-table-popup").on("click","a.popup-close",function(){$(".slbs2-table-popup").hide();
$(".help-icon.active").removeClass("active")
});
$(document).mouseup(function(c){var b=$(".slbs2-table-popup");
if(b){if(!b.is(c.target)&&b.has(c.target).length===0){b.hide();
$(".help-icon.active").removeClass("active")
}}});
$(window).on("resize",function(){var b=$(".slbs2-table-popup");
if(b.css("display")!="none"){tablePopup.util.positionPopup(b)
}})
},util:{createPopup:function(){var b=$(".slbs2-table-popup");
var a=$(".table-popup-container");
a.empty();
if(!b.is(":hidden")){b.hide()
}else{sl.components.helppopup.util.positionPopup(b);
b.show();
$(".loading-div").addClass("icon-loading")
}},positionPopup:function(d){var b=$(".help-icon.active").parent();
if(b.length){var a=b.offset();
var c=a.left;
if(a.left+d.outerWidth()>$(".row").width()&&Foundation.MediaQuery.atLeast("medium")){c=a.left-d.outerWidth()+b.outerWidth()
}else{if(Foundation.MediaQuery.current=="small"){c="15px"
}}d.css({top:a.top+b.outerHeight(),left:c,"z-index":2000})
}},addTheDataToPopup:function(c,b,e){var d=$(".table-popup-container");
var a="";
if(c=="partnumber"){a='<div class="oscillator-help-title">A Part Number is 10 - 16 digits.</div>';
a+='<div class="oscillator-help-text">The number is defined by specifications like output format and frequency. Enter a full or partial number to see matches.</div>';
a+='<div class="oscillator-help-image"><img src="'+b+'" /></div>';
a+="<hr>";
a+='<div class="oscillator-help-more"><a href="'+e+'">More about Part Codes</a></div>'
}else{a='<div class="oscillator-help-title">A Mark Code is a 6 digit number.</div>';
a+='<div class="oscillator-help-text">thecode is the first row of digits on Si500 and Si51X devices. Enter all 6 digits in the search field.</div>';
a+='<div class="oscillator-help-image"><img src="'+b+'" /></div>';
a+="<hr>";
a+='<div class="oscillator-help-more"><a href="'+e+'">More about Mark Numbers</a></div>'
}d.append(a);
$(".loading-div").removeClass("icon-loading")
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.oscillatorsearch={init:function(){var a=this;
$(document).on("click",".oscillator-search-link",function(){var c=$(this),e=c.attr("data-attr-search-link");
if(typeof c.attr("href")=="undefined"||c.attr("href")==""){var b=c.parent().parent().find("input").val();
var d=e+"?term="+encodeURIComponent(b);
window.open(d,"_self")
}});
$(document).ready(function(){$(".oscillator-search-input").keypress(function(b){if(b.which==13){$(".oscillator-search-link").click()
}})
})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.marquee={init:function(){var a=$(".marquee");
var b=$(".slbs2-marquee");
if(b.hasClass("full-bleed")){a.addClass("full-bleed")
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.loginContainer={init:function(){var e=this,b=$(".login-container");
if(b.length>0){var l=b.attr("data-marketo-list-id"),c={listId:null,cookie:null,source:"Website",detail:"Organic"};
var h=sl.utils.getURLParams();
if(typeof h!=="undefined"&&typeof h.source!=="undefined"&&typeof h.detail!=="undefined"){c.source=h.source;
c.detail=h.detail;
sl.utils.createCookie("_mkto_source","source="+h.source+"|detail="+h.detail,90)
}if(sl.components.links.isLoggedIn()){var m=sl.utils.getCookies();
if(m&&m._mkto_trk){c.cookie=m._mkto_trk
}var a=sl.utils.getCookies(),f=null;
if((window.sl.currentRunMode===""||window.sl.currentRunMode==="dev")&&typeof a.silabsprofiledev!=="undefined"){f=a.silabsprofiledev
}else{if(window.sl.currentRunMode==="qa"&&typeof a.silabsprofileqa!=="undefined"){f=a.silabsprofileqa
}else{if(window.sl.currentRunMode==="stage"&&typeof a.silabsprofileqa!=="undefined"){f=a.silabsprofileqa
}else{if(window.sl.currentRunMode==="prod"&&typeof a.silabsprofile!=="undefined"){f=a.silabsprofile
}}}}if(f&&typeof l!=="undefined"){var d=sl.utils.parseCookie(f);
c.listId=l;
c.email=d.Email;
if(typeof a._mkto_source!="undefined"){sl.utils.createCookie("_mkto_source_"+c.email,decodeURIComponent(a._mkto_source),90);
sl.utils.deleteCookie("_mkto_source")
}a=sl.utils.getCookies();
var k=a["_mkto_source_"+c.email];
if(typeof k!="undefined"){var g=sl.utils.parseCookie(k);
c.source=g.source;
c.detail=g.detail
}$.post({url:"/sl/analytics/list-lead",data:c,success:function(n){}})
}}}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.horizontalseparator={init:function(){var a=$(".horizontalseparator");
var b=$(".slbs2-hr");
if(b.hasClass("full-bleed")){a.addClass("full-bleed")
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.highlightsCarousel={init:function(){var a=$(".highlightscarousel");
var b=$(".slbs2-highlights-carousel");
if(b.hasClass("full-bleed")){a.addClass("full-bleed")
}$(".slbs2-highlights-carousel .carousel").each(function(){var e=$(this),d=e.attr("data-num-visible");
var g=e.find("button").css("class","slick-arrow");
e.on("init",function(){e.find(".slick-slide:not(.slick-cloned) img").lazyInterchange()
});
e.on("breakpoint",function(){h()
});
var c={slidesToShow:1,mobileFirst:true,easing:"swing",responsive:[{breakpoint:688,settings:{slidesToShow:d}}]};
if(d==="3"){c.responsive=[{breakpoint:900,settings:{slidesToShow:d}},{breakpoint:688,settings:{slidesToShow:2}}]
}var h=function(){var k=Foundation.MediaQuery.current=="small"?1:d;
if(k<e.find(".slide").length){e.addClass("carousel-active")
}else{e.removeClass("carousel-active")
}};
var f=function(){if(e.hasClass("bg-darkgray")){e.find("button").css("class","slick-arrow").addClass("white")
}};
h();
e.slick(c);
f()
})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.herocarousel={init:function(){$(".hero-carousel .slides").each(function(){var b=$(this).parent().attr("data-speed");
var a=$(".hero-carousel .arrow-prev");
var d=$(".hero-carousel .arrow-next");
var e=$(this);
e.slick({infinite:true,autoplay:true,autoplaySpeed:b,buttons:false,pauseOnHover:false,prevArrow:a,nextArrow:d});
$("[data-slide-url]").on("click",function(){document.location=$(this).attr("data-slide-url")
});
d.on("click",function(){e.slick("slickPause")
});
a.on("click",function(){e.slick("slickPause")
});
e.on("swipe",function(){e.slick("slickPause")
});
function c(){var g;
if(Foundation.MediaQuery.current==="small"){var f=$(".hero-carousel").find(".mobile-img").eq(0).height();
var h=$(".hero-carousel").height()-f;
g=f+h/2-a.height()/2;
a.css("top",g);
d.css("top",g)
}else{g=$(".hero-carousel").height()/2-a.height()/2;
a.css("top",g);
d.css("top",g)
}}c();
$(window).on("resize",function(){c()
})
})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.links={init:function(){var a=this;
if(a.isLoggedIn()){$("body").addClass("logged-in");
$(document).trigger("sl.reinit")
}$(document).unbind("click.sl.documents");
$(document).on("click.sl.documents",'a[href^="/content/dam/siliconlabs/documents/"], a[href^="/documents/"], a[href*="/documents/"]',function(h){h.preventDefault();
var g=$(this),d=g.attr("href"),l="/"+d.replace(/^(?:\/\/|[^\/]+)*\//,"");
if(!l.startsWith("/content/dam/siliconlabs/documents/")&&!l.startsWith("/documents/")){document.location.href=d
}else{d=l
}var k=d.replace(/.+\/(.+)$/g,"$1"),c=g.attr("data-asset-name")?g.attr("data-asset-name"):k,n=document.location.origin+d,m=d,p=document.location.search.indexOf("?")==0?"&":"?";
if(d.match(/\.(pdf|html|htm|txt|jpg|jpeg|png|gif|bmp|tiff|svg|ico)$/g)==null){m=document.location.pathname+p+"download="+encodeURIComponent(d+"|"+c)
}var f=a.getMrktoCookieID();
var b=encodeURIComponent(n);
var o=window.sl.ssoDomain+"/services/apexrest/restassettracking?mktocookie="+f+"&URL="+b;
$.ajax({url:o,type:"GET",dataType:"jsonp",complete:function(q,r){var e=d;
if(d.indexOf("/login/")>-1&&!a.isLoggedIn()){document.cookie="saml_request_path="+m+";path=/;";
e=window.sl.ssoEndpointUrl
}document.location.href=e
}});
return false
});
$(".require-login").on("click",function(){var b=document.location.origin+$(this).attr("href");
if(!a.isLoggedIn()){document.location.href=window.sl.ssoDomain+"/apex/ValidateUser?documentURL="+b;
return false
}})
},isLoggedIn:function(){var b={};
document.cookie.split(";").forEach(function(f,e){var d=f.trim().split(/=(.+)?/);
b[d[0]]=d[1]
});
var c=false,a=window.location.origin;
if((window.sl.currentRunMode===""||window.sl.currentRunMode==="dev")&&typeof b.silabsprofiledev!=="undefined"){c=true
}else{if(window.sl.currentRunMode==="qa"&&typeof b.silabsprofileqa!=="undefined"){c=true
}else{if(window.sl.currentRunMode==="stage"&&typeof b.silabsprofileqa!=="undefined"){c=true
}else{if(window.sl.currentRunMode==="prod"&&typeof b.silabsprofile!=="undefined"){c=true
}}}}return c
},getMrktoCookieID:function(){var b="unknown",a="";
document.cookie.split(";").forEach(function(e,d){var c=e.trim().split("=");
if(c[0]=="_mkto_trk"){a=c[1]
}});
if(a){a.split("&").forEach(function(e,d){var c=e.trim().split(":");
if(c[0]=="token"){b=c[1]
}})
}return b
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.facetedSearch={init:function(){var b=this,c=$(".faceted-search"),a=c.find(".query-builder");
if(c.length>0){window.sl.facetedSearch=window.sl.facetedSearch?window.sl.facetedSearch:{};
b.numPerPage=20;
if(a.attr("data-results-per-page")){b.numPerPage=a.attr("data-results-per-page")
}b.numPerPage=parseInt(b.numPerPage);
b.dataTable=c.find("table").DataTable({searching:false,autoWidth:false,paging:true,lengthChange:false,pageLength:b.numPerPage,ordering:true,info:false,language:{emptyTable:"No results available"}});
b.defaultFilters=[];
c.find("[data-filters]").each(function(){var h=$(this),d=h.attr("data-filters").split(","),e;
for(var g=0;
g<d.length;
g++){var k=d[g];
b.defaultFilters.push(k)
}var f=false;
if(h.is("[data-show-all]")){f=true
}b.getFilterOptions(c,d,h,f)
});
c.on("filtersReady",function(){b.setEvents(c);
a.css("opacity",1);
c.removeClass("loading");
b.setUpResults(c)
})
}},util:{getSelectors:function(){var a=[],b=document.location.pathname.replace(/.+\/(.+?)(\.|$)/g,"").replace(/\.?html/g,"");
b=decodeURIComponent(b);
if(b.length>0){a=b.split(".")
}return a
},updateSelectors:function(a,k){var d=this,e=location.pathname,n=k&&k==true,f=d.convertTagPathToSelector(a);
var g=d.getSelectors();
if(f==""){g=[]
}else{if(f.indexOf("=")>-1){var q=f.split("=")[0],c=f.split("=")[1];
for(var h=0;
h<g.length;
h++){var l=g[h],p=l.split("=")[0],m=l.split("=")[1];
if(p==q){if(c!==""){g[h]=f
}else{g.splice(h,1)
}}}if(g.indexOf(f)==-1&&c!==""){g.push(f)
}}else{if(n||g.indexOf(f)>-1){var r=g.indexOf(f);
g.splice(r,1)
}else{g.push(f)
}}}g.sort();
var b=g.join(".");
if(g.length>0){b="."+b
}if(f.indexOf("sort=")==0||f.indexOf("=")==-1){b=b.replace(/\.page=(.+?)($|\.)/g,"$2")
}var o=e.indexOf(".html")==e.length-5?".html":"";
e=e.replace(/(.+\/)(.+?)(\.|$).*$/g,"$1$2"+b+o);
if(location.pathname!==e){window.history.pushState({url:e},null,e+location.search)
}},getFiltersFromSelectors:function(d){var a=this,c=[],b=this.getSelectors();
var f=decodeURIComponent(location.search).replace("?","").replace(/\+/g," ").split("&");
if(f.toString().length>0){b=b.concat(f)
}for(var e=0;
e<b.length;
e++){var g=b[e],h=a.convertSelectorToTagPath(g);
c.push(h)
}return c
},convertTagPathToSelector:function(b){var a=b.replace(/(\/etc\/tags\/)?(.+?)(\/|:)(.+)/g,"$2-$4");
a=a.replace(/\//g,"_");
if(a.indexOf("content-type")==0){a=a.replace("content-type","ct")
}else{if(a.indexOf("silicon-labs-products")==0){a=a.replace("silicon-labs-products","p")
}}return a
},convertSelectorToTagPath:function(a){var b=a.replace(/_/g,"/");
if(b.indexOf("ct-")==0){b=b.replace("ct-","/etc/tags/content-type/")
}else{if(b.indexOf("p-")==0){b=b.replace("p-","/etc/tags/silicon-labs-products/")
}}return b
}},setUpResults:function(d){var b=this;
var a=d.find(".query-builder");
b.currentPage=0;
d.find(".fulltext-field").val("");
d.find(".filter-options .active").removeClass("active");
d.find('.filter-options input[type="checkbox"][data-filter]').prop("checked",false);
d.find(".selected-filters p[data-filter]").remove();
b.dataTable.order([0,"asc"]);
if(d.find(".selected-filters p[data-filter]").length==0){d.find(".clear-all").addClass("hide")
}else{d.find(".clear-all").removeClass("hide")
}var c=b.util.getFiltersFromSelectors();
c.forEach(function(f){if(f.indexOf("sort=")>-1){var e=f.replace("sort=","");
b.dataTable.order(e.split(","))
}else{if(f.indexOf("query=")>-1){d.find(".fulltext-field").each(function(){$(this).val(f.replace("query=",""))
})
}else{if(f.indexOf("page=")>-1){b.currentPage=parseInt(f.replace("page=",""))
}else{$('[data-filter="'+f+'"]').trigger("setUpFilter");
var g=$('[data-filter="'+f+'"]').parents("[data-accordion-menu]"),h=$('[data-filter="'+f+'"]').parents("[data-submenu]");
if(h.length>0){g.foundation("down",h)
}}}}});
b.getResults(d,function(){b.dataTable.page(b.currentPage).draw("page");
a.removeClass("loading")
})
},getFilterOptions:function(b,k,g,d){var a=this,l={};
a.filterOptions=a.filterOptions?a.filterOptions:{};
a.contentTypes=a.contentTypes?a.contentTypes:[];
var h=function(p){var m=[];
for(var n=0;
n<p.length;
n++){var o=p[n];
if(o.match(/\//g).length>3){o=o.replace(/(\/etc\/tags\/.+?)\/.*/g,"$1")
}if(m.indexOf(o)==-1){m.push(o)
}}return m
};
var c=h(k);
for(var e=0;
e<c.length;
e++){var f=c[e];
if(typeof a.filterOptions[f]=="undefined"){$.ajax({type:"GET",url:f+".tagdetail.json",success:function(r){a.filterOptions[f]=r;
for(var p=0;
p<k.length;
p++){var y=k[p],u=false;
if(y.indexOf("/etc/tags/content-type")==0){u=true
}if(y==f){l=a.filterOptions[f];
if(u){for(var t in r){var n=r[t].path;
if(typeof n!=="undefined"){a.contentTypes.push(a.util.convertTagPathToSelector(n))
}}}}else{if(u){a.contentTypes.push(a.util.convertTagPathToSelector(y))
}var m=y.replace(f+"/","").split("/"),o="",v=a.filterOptions[f];
for(var w=0;
w<m.length;
w++){o=m[w];
if(typeof v[m[w]]!=="undefined"){v=v[m[w]]
}}l[o]=v
}}a.traverseFilterOptions(l,g.find(".nested"),0,"",d);
var s={};
var q=new Foundation.AccordionMenu(g,s);
g.attr("data-initialized",true);
if(b.find("[data-filters][data-initialized]").length===b.find("[data-filters]").length){b.trigger("filtersReady")
}}})
}}},traverseFilterOptions:function(n,m,c,k,l){var b=this,s,g;
if(n.path){k=n.path.replace(/\/etc\/tags\/(.+?)(\/|$)/g,"$1:")
}var o="";
if(l){o="is-active"
}var f=function(){for(var t in n){if(typeof n[t]=="object"&&!(n[t] instanceof Array)){return false
}}return true
};
var h=$('<li class="filter"><a href="#"><span>'+n["jcr:title"]+'</span></a><input data-filter="'+n.path+'" data-text="'+n["jcr:title"]+'" type="checkbox" value="true" /></li>');
if(!f(n)){if(n["jcr:title"]){if(k.indexOf(":")<k.length-1){sl.components.facetedSearch.counter++;
c++;
s=h
}}for(var a in n){if(typeof n[a]=="object"&&!(n[a] instanceof Array)){if(s&&s.children("ul").length==0){g=$('<ul class="'+o+" menu vertical nested level-"+c+'"></ul>');
s.append(g)
}var e="";
if(k){if(k==":"){e=a+":"
}else{if(k.indexOf(":")==-1){e=k+":"+a
}else{e=k+"/"+a
}}}this.traverseFilterOptions(n[a],s?g:m,c,e,l)
}}}else{var r=n.value?n.value:"";
if(!r&&k){r=k
}var q=n["jcr:title"]?n["jcr:title"]:r;
var p=n["o0_"+sl.components.facetedSearch.counter];
if(typeof p!=="undefined"){var d=b.getOptions([p]);
b.traverseFilterOptions(d,m,0,"",l)
}s=h;
sl.components.facetedSearch.counter++
}m.append(s)
},setEvents:function(c){var a=this;
$(window).on("popstate",function(d){a.setUpResults(c)
});
var b=function(k,g,h){k.preventDefault();
var f=$(g).attr("data-filter"),d=c.find('.filter-options [data-filter="'+f+'"]');
if(f==""){c.find(".fulltext-field").val("");
c.find(".filter-options .active").removeClass("active");
c.find(".filter-options [data-filter]:checked").prop("checked",false);
c.find(".selected-filters p[data-filter]").remove();
if(!h){a.util.updateSelectors(f)
}a.dataTable.order([0,"asc"])
}else{if(d.hasClass("active")||d.siblings("a").hasClass("active")){d.prop("checked",false);
d.removeClass("active");
d.siblings("a").removeClass("active");
c.find('.selected-filters p[data-filter="'+f+'"]').remove();
if(!h){a.util.updateSelectors(f,true)
}}else{d.prop("checked",true);
d.siblings("a").addClass("active");
c.find(".selected-filters").append('<p class="active" data-filter="'+f+'">'+d.parents(".vertical.menu").find(".primary").eq(0).text()+": "+d.data("text")+"</p>");
if(!h){a.util.updateSelectors(f);
d.parents("li").each(function(){var e=$(this).children("input:checked");
if(e.length>0&&!e.is(d)){e.click()
}})
}}}if(c.find(".selected-filters p[data-filter]").length==0){c.find(".clear-all").addClass("hide")
}else{c.find(".clear-all").removeClass("hide")
}if(!h){a.getResults(c)
}k.stopPropagation();
return false
};
c.find("a[data-filter]").on("click",function(d){b(d,this,false)
});
c.find("input[data-filter]").on("change",function(d){b(d,this,false)
});
c.find("[data-filter]").on("setUpFilter",function(d){b(d,this,true)
});
c.find(".selected-filters").on("click","[data-filter]",function(d){b(d,this,false)
});
c.find(".results th").on("click",function(g){var f=$(this).index(),d=$(this).attr("aria-sort")=="ascending"?"asc":"desc";
a.util.updateSelectors("sort="+f+","+d);
return false
});
c.find("form.search-field").on("submit",function(k){k.preventDefault();
var n=$(this).find(".fulltext-field").val();
n=n.replace(/[\/\.]/g," ");
var h="query="+n;
var m=location.search.replace("?","").length==0?[]:location.search.replace("?","").split("&");
for(var g=0;
g<m.length;
g++){var f=m[g];
if(f.indexOf("query=")>-1){if(n==""){m.splice(g,1)
}else{m[g]=h
}}}if(n!==""&&m.indexOf(h)==-1){m.push(h)
}var l=m.length>1?m.join("&"):m.toString();
var d=location.pathname+"?"+l;
window.history.pushState({url:d},null,d);
a.getResults(c);
return false
});
c.find(".collapse-all").on("click",function(d){d.preventDefault();
if(!$(this).hasClass("disabled")){c.find("[data-accordion-menu]").each(function(){var e=$(this);
e.foundation("up",e.find("[data-submenu]"))
});
$(this).addClass("disabled")
}$(this).siblings().removeClass("disabled")
});
c.find(".expand-all").on("click",function(d){d.preventDefault();
if(!$(this).hasClass("disabled")){c.find("[data-accordion-menu]").each(function(){var e=$(this);
e.foundation("down",e.find("[data-submenu]"))
});
$(this).addClass("disabled")
}$(this).siblings().removeClass("disabled")
});
$("[data-accordion-menu]").on("up.zf.accordionMenu",function(){c.find(".expand-all").removeClass("disabled")
}).on("down.zf.accordionMenu",function(){c.find(".collapse-all").removeClass("disabled")
});
$("[data-accordion-menu] a").on("click",function(d){d.preventDefault()
});
a.dataTable.on("page.dt",function(){a.util.updateSelectors("page="+a.dataTable.page());
var d=a.dataTable.page.info();
c.find(".page-count").text(d.end-d.start);
sl.components.links.init()
})
},getActiveFilters:function(c){var a=this,b=[];
c.find(".filter-options [data-filter]:checked").each(function(){var d=$(this),e=a.util.convertTagPathToSelector(d.data("filter"));
b.push(e)
});
return b
},getResults:function(c,f){var a=this;
var e=[];
var b=$('<tr class=""><td class="title"></td><td class="version"></td><td class="resource-type"></td></tr>');
var h=c.find(".query-builder");
h.addClass("loading");
var k={};
var g=c.find(".fulltext-field:visible").val();
if(g!==""){k.fulltext=g
}var d=a.getActiveFilters(c);
if(d.length>0){if(d.toString().indexOf("ct-")>-1){e=d
}else{e=d.concat(a.contentTypes)
}}else{e=a.contentTypes
}e="/"+e.sort().join("/")+".json";
$.ajax({url:"/sl/search/facetedsearchquery.json"+e,data:k,success:function(o){var l=o.hits;
c.find(".page-count").text(a.numPerPage>o.total?o.total:a.numPerPage);
c.find(".result-count").text(o.total?o.total:o.results);
sl.components.facetedSearch.dataTable.rows().remove();
if(l.length>0){for(var n=0;
n<l.length;
n++){var p=l[n],m=b.clone().addClass("clone");
if(p.externalPath.indexOf("/login/")>-1){m.addClass("secure")
}m.children(".title").append('<a href="'+p.externalPath+'">'+p.name+"</a>");
m.children(".version").append(p.version);
m.children(".resource-type").append(p.contentTypeTitle);
sl.components.facetedSearch.dataTable.row.add(m)
}}a.dataTable.draw();
sl.components.links.init();
h.removeClass("loading");
if(f){f()
}},error:function(l){console.warn(l);
h.removeClass("loading")
}})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.lazyLoading={init:function(){if(isEdit||isDesign){$("img.lazy-loading").each(function(){$(this).width("auto");
$(this).height("auto")
})
}$.fn.lazyInterchange=function(){var e=this.each(function(){if($(this).attr("data-lazy")){$(this).attr("data-interchange",$(this).attr("data-lazy"));
$(this).removeAttr("data-lazy");
new Foundation.Interchange($(this));
$(this).addClass("lazy-loaded");
$("body,html").scroll()
}});
return e
};
var b={chainable:false,afterLoad:function(e){$(e).lazyInterchange();
Foundation.reInit($(e))
}};
if(location.hash.length>0&&$(location.hash).length>0){$(".lazy-loading").on("replaced.zf.interchange",function(){$(this).imagesLoaded().always(function(e){$(window).trigger("all-lazy-images-loaded")
})
});
b.threshold=100000000000000000000
}var c=$(".lazy-loading").Lazy(b);
var a=false;
$(window).on("load",function(){a=true
});
if(!a){var d=c.config("name");
$(window).trigger("load."+d+"-1")
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.deviceTable={init:function(d){var c=this,b=".device-table, .part-grouping-table",a=$(b);
a.each(function(){var e=$(this),f=e.find(".status-filters");
c.setUpTables(e.find(".table-responsive"));
e.find("tr[data-status]").each(function(){var k=$(this),g=k.data("status"),m=k.find(".status").attr("title"),l=k.find(".status").data("abbr");
var h=f.find(".template").clone().addClass("clone").removeClass("template");
h.text(l).attr("title",m).attr("data-filter",g);
if(f.find('[data-filter="'+g+'"]').length==0){f.append(h)
}});
if(f.children("a.clone").length>0){f.removeClass("hide")
}});
if(!d){a.on("click",".status-filters a",function(k){k.preventDefault();
var f=$(this),g=f.data("filter"),h=f.parents(b);
if(f.hasClass("active")){h.find('tr[data-status="'+g+'"]').hide();
f.removeClass("active")
}else{h.find('tr[data-status="'+g+'"]').show();
f.addClass("active")
}})
}},setUpTables:function(f){var d=f.find("td:first-child, th:first-child"),g=0;
if(typeof f.find("table")!=="undefined"){f.find("table").removeAttr("style");
f.find("td:first-child, th:first-child").removeAttr("style")
}for(var c=0;
c<d.length;
c++){var a=$(d[c]),e=a.height();
if(a.is("th")){a.height(e+2)
}else{a.height(e+1)
}a.siblings().height(e);
if(a.outerWidth()>g){g=a.outerWidth()
}}d.width(g);
var b=d.outerWidth()-1;
f.find("table").css({"margin-left":b,width:"calc(100% - "+b+"px)"});
f.css("visibility","visible")
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.tablePopup={init:function(){var a=this;
$(document).on("click","a.order-link",function(){var f=$(this),g=f.attr("data-device"),c=f.attr("data-opn"),e=f.attr("data-group"),b=f.attr("data-device-path"),d="";
if(typeof e=="undefined"){e=""
}if(typeof f.attr("href")=="undefined"||f.attr("href")==""){f.addClass("active");
if(f.hasClass("order-link-buy")){d="buy"
}else{if(f.hasClass("order-link-sample")){d="sample"
}else{if(f.hasClass("order-link-alternate-sample")){d="alternatesample"
}else{if(f.hasClass("order-link-group-buy")){d="groupbuy"
}else{if(f.hasClass("order-link-group-sample")){d="groupsample"
}else{if(f.hasClass("order-link-group-custom")){d="groupcustom"
}else{f.removeClass("active")
}}}}}}}a.util.getTheProductData(d,g,c,e,b)
});
$(".slbs2-table-popup").on("click","a.popup-close",function(){$(".slbs2-table-popup").hide();
$(".order-link.active").removeClass("active")
});
$(document).mouseup(function(c){var b=$(".slbs2-table-popup");
if(b){if(!b.is(c.target)&&b.has(c.target).length===0){b.hide();
$(".order-link.active").removeClass("active")
}}});
$(window).on("resize",function(){var b=$(".slbs2-table-popup");
if(b.css("display")!="none"){a.util.positionPopup(b)
}})
},util:{getTheProductData:function(e,f,c,d,b){var a="/sl/productdata/buysample?orderType="+e;
if(e=="groupcustom"||e=="groupbuy"||e=="groupsample"){a+="&groupName="+c
}else{a+="&devicePath="+b
}$.ajax({url:a,dataType:"json",success:function(q){var n=q;
if(e=="sample"){if(n.sampleShoppingCartURL!=null){sl.components.tablePopup.util.redirectToSampleCart(c,n.sampleShoppingCartURL)
}else{sl.components.tablePopup.util.showErrorMessage(e,n.noneFoundMessage)
}}else{if(e=="groupcustom"){if(n.buySamplePartGroup!=null&&n.customAvailableConfigUrls!=null){var p=0;
var o="";
var h="";
var m=n.buySamplePartGroup.partGroupDeviceData;
for(var k in m){var l=m[k];
if(l["sl:customavailable"]){var g=l["sl:customavailable"];
if(n.customAvailableConfigUrls[g]){p++;
o=k;
h=n.customAvailableConfigUrls[g]
}}if(p>1){break
}}if(p==1){sl.components.tablePopup.util.redirectToCustomLink(o,h)
}else{sl.components.tablePopup.util.createPopup(f,c,"groupcustom",d);
sl.components.tablePopup.util.getDataObjects(f,c,"groupcustom",d,n)
}}else{sl.components.tablePopup.util.createPopup(f,c,e,d);
sl.components.tablePopup.util.showErrorMessage(e,n.noneFoundMessage)
}}else{sl.components.tablePopup.util.createPopup(f,c,e,d);
if((e=="alternatesample"&&n.sampleShoppingCartURL!=null&&n.alternateSampleData!=null)||(e=="groupbuy"&&n.buySamplePartGroup!=null)||(e=="buy"&&n.buySampleOctopart!=null)||(e=="groupsample"&&n.sampleShoppingCartURL!=null&&n.buySamplePartGroup!=null)){sl.components.tablePopup.util.getDataObjects(f,c,e,d,n)
}else{sl.components.tablePopup.util.showErrorMessage(e,n.noneFoundMessage)
}}}},error:function(k,g,h,m,l){sl.components.tablePopup.util.showErrorMessage(e,"")
}})
},redirectToSampleCart:function(b,c){var a=c.replace("[partnumber]",b);
window.open(a,"_blank")
},redirectToCustomLink:function(b,a){var c=a.replace("{0}",b);
window.open(c,"_self")
},createPopup:function(e,a,c,b){var f=$(".slbs2-table-popup");
var d=$(".table-popup-container");
d.empty();
if(!f.is(":hidden")&&!(b)){f.hide()
}else{sl.components.tablePopup.util.positionPopup(f);
f.show();
$(".loading-div").addClass("icon-loading")
}},positionPopup:function(d){var b=$(".order-link.active").parent();
if(b.length){var a=b.offset();
var c=a.left;
if(a.left+d.outerWidth()>$(".row").width()&&Foundation.MediaQuery.atLeast("medium")){c=a.left-d.outerWidth()+b.outerWidth()
}else{if(Foundation.MediaQuery.current=="small"){c="15px"
}}d.css({top:a.top+b.outerHeight(),left:c,"z-index":2000})
}},getDataObjects:function(o,k,l,p,b){var q=[];
var c="";
var a=new Array();
if(l=="buy"){var t=b.useDataFromSAP;
var e=b.buyOrderPartNumbers;
var u=b.buySampleOctopart.octopartPackagingExclusions;
var h=b.buySampleOctopart.octopartValidDistiList;
var d=b.buySampleOctopart.octopartValidCurrencyList;
var n=b.buySampleOctopart.octopartValidCurrencySymbolList;
var m="&queries=%5b";
if(t){for(var r=0;
r<e.length;
r++){if(r>0){m+=","
}m+="%7b%22mpn%22:%22"+e[r].title+"%22%7D"
}}else{m+="%7b%22mpn%22:%22"+k+"%22%7D"
}m+="%5D";
var s=b.buySampleOctopart.octopartUrl+m;
$.ajax({url:s,dataType:"jsonp",success:function(P){var K=P.results;
for(var C=0;
C<K.length;
C++){var H=K[C];
for(var L=0;
L<H.items.length;
L++){var z=H.items[L].offers;
var F=H.items[L].mpn;
var M="";
if(t){for(var D=0;
D<e.length;
D++){if(e[D].title==F){M=e[D].packingmedia
}}}for(var D=0;
D<z.length;
D++){var O=z[D].seller.name;
var I=z[D].packaging;
var w=true;
if(!t&&I!=null&&u.includes(I.toLowerCase())){w=false
}if(h.includes(O)){if(w){var N=sl.components.tablePopup.util.getOctopartQtyPrice(z[D].prices,d,n);
var G=I;
if(!G){G=sl.components.tablePopup.util.formatTheString(M)
}if(G&&G=="Reelbox"){G="Tape & Reel"
}var Q=N.qty;
var J=N.price;
var B=z[D].in_stock_quantity;
var v="<a target='_blank' href='"+z[D].product_url+"'>Buy</a>";
var A=sl.components.tablePopup.util.getDuplicateDataIndex(q,O,F,G,J,B,t);
if(A<0){var E=new Array();
E.distributorName=O;
if(t){E.orderPartNumber=F;
E.packaging=G;
if(a===undefined||a.length==0||!a.includes(G)){a.push(G)
}}E.priceTier=Q;
E.pricing=J;
E.quantity=B;
E.shoppingCartURL=v;
q.push(E)
}else{if(A<q.length){q[A].priceTier=Q;
q[A].pricing=J
}}}}}}}sl.components.tablePopup.util.buildTheObjects(q,o,k,l,c,p,b,a)
},error:function(x,v,w,z,y){q=[];
sl.components.tablePopup.util.buildTheObjects(q,o,k,l,c,p,b,a)
}})
}else{if(l=="alternatesample"){var g=b.alternateSampleData;
var f=b.sampleShoppingCartURL;
if(g!=null){f=f.replace("[partnumber]",g.recommendedDevice.deviceOpn)
}c='<a class="button primary-indium" href=\''+f+'\' target="_blank" >Continue</a>';
q.push(["Part Number",'<a class="popup-link" href="'+g.requestedDevice.deviceUrl+'" target="_blank" >'+g.requestedDevice.device+"</a>",'<a class="popup-link" href="'+g.recommendedDevice.deviceUrl+'" target="_blank" >'+g.recommendedDevice.device+"</a>"]);
g.displayNames.forEach(function(w){var x=g.requestedDevice.deviceAttributes.find(function(y){return y.name==w.attribute
});
var v=g.recommendedDevice.deviceAttributes.find(function(y){return y.name==w.attribute
});
if(x.value==true){x.value="Yes"
}if(x.value==false){x.value="No"
}if(v.value==true){v.value="Yes"
}if(v.value==false){v.value="No"
}q.push([w.attributeDisplay.replace("_"," "),x.value,v.value])
});
sl.components.tablePopup.util.buildTheObjects(q,o,k,l,c,p,b,a)
}else{if(l=="groupbuy"||l=="groupsample"||l=="groupcustom"){sl.components.tablePopup.util.buildTheObjects(q,o,k,l,c,p,b,a)
}}}},getDuplicateDataIndex:function(a,b,g,c,k,f,e){var l=-1;
for(var h=0;
h<a.length;
h++){var m=a[h];
var d=false;
if(e&&m.distributorName==b&&m.orderPartNumber==g&&m.packaging==c){d=true;
l=1000
}else{if(!e&&m.distributorName==b){d=true;
l=1000
}}if(d){if(m.pricing!="See Supplier"){if(k=="See Supplier"||parseFloat(m.pricing)>parseFloat(k)){l=h
}}else{if(k=="See Supplier"&&f>m.in_stock_quantity){l=h
}}break
}}return l
},formatTheString:function(d){var c=d.toLowerCase();
var e="";
var b=c.split(" ");
for(var a=0;
a<b.length;
a++){if(e){e+=" "
}e+=b[a].charAt(0).toUpperCase()+b[a].slice(1)
}return e
},getOctopartQtyPrice:function(e,f,c){var l=new Array();
l.qty="";
l.price="See Supplier";
var g="";
for(var d in e){g=d
}if(f.includes(g)){var h=f.indexOf(g);
var m=e[g];
if(m.length>1){var a=m[0][0];
if(a==1){var b=m[1][0]-1;
l.qty=a.toString()+" - "+b.toString();
l.price=c[h]+(Math.round(m[0][1]*100)/100).toString()
}}}return l
},buildTheObjects:function(b,k,n,g,w,m,H,c){var e=new Array();
var p=new Array();
var l=new Array();
var F="";
var G="";
var v="";
var d=new Array();
var J=H.useDataFromSAP;
if(g=="buy"){e=[];
for(var C=0;
C<b.length;
C++){var y=b[C];
if(J){e.push([y.distributorName,y.orderPartNumber,y.packaging,y.priceTier,y.pricing,y.quantity,y.shoppingCartURL])
}else{e.push([y.distributorName,y.priceTier,y.pricing,y.quantity,y.shoppingCartURL])
}}p.push({sTitle:"<span>Distributor</span>"});
if(J){p.push({sTitle:"<span>Part Number</span>"});
p.push({sTitle:"<span>Packaging</span>"})
}p.push({sTitle:"<span>Qty</span>"});
p.push({sTitle:"<span>Price</span>"});
p.push({sTitle:"<span>Stock</span>"});
p.push({sTitle:"<span class='table-popup-hdr-nosort'>&nbsp;</span>",orderable:false});
F=('<h3 class="has-mobile">Silicon Labs Authorized Distributors</h3><h3 class="mobile-display">Authorized Distributors</h3>');
if(J){F+=('<div class="opn-title buy">Device:&nbsp;<span class="opn-text">'+k+"</span></div>")
}else{F+=('<div class="opn-title buy">Ordering Part Number:&nbsp;<span class="opn-text">'+n+"</span></div>")
}G="<div class='popup-footer'><span class='popup-footer-left'><a href='https://octopart.com/parts/manufacturer--silicon+labs/' class='powered-by-octopart'>Powered by <img src='/etc.clientlibs/siliconlabs/clientlibs/clientlib-content/resources/images/icon/the-octopart-logo.png' height='40' width='50' alt='Octopart'/></a></span><span class='popup-footer-right'>";
if(m){G+='<a class="order-link order-link-group-buy" data-group="'+m+'" data-opn="'+m+'">< Back</a>'
}G+="</span></div>";
l=[];
d=[[0,"asc"]];
if(c.length){c.sort();
v="<div class='filter-string'>";
var r=0;
for(var C=0;
C<c.length;
C++){if(r>=3){r=0;
v+="<br/>"
}v+="<input type='checkbox' class='popup-filter-option' value='"+c[C]+"' id='"+c[C]+"' name='"+c[C]+"' checked/>";
v+="<label for='"+c[C]+"'>"+c[C]+"</label>";
r++
}v+="</div>"
}}else{if(g=="alternatesample"){e=b;
p.push({sTitle:"",orderable:false});
p.push({sTitle:"Requested Device",orderable:false});
p.push({sTitle:"Recommended Device",orderable:false});
F="<h3>Sample</h3>";
F+='<div class="opn-title">The device requested is not available for sampling. We recommend the compatible device below with the noted functional difference:</div>';
if(w.length>0){G="<div class='popup-footer'><span class='popup-footer-left alternatesample'></span><span class='popup-footer-right alternatesample'>"+w+"</span></div>"
}l=[];
d=[]
}else{if(g=="groupbuy"){e=[];
var A=H.buySamplePartGroup.partGroupDeviceData;
var a=H.buySamplePartGroup.partGroupFilterList;
var f=false;
for(var s in A){var q=new Array();
var o=A[s];
var B=s;
if(o.topdevicestatusdisplay){B+=" "+o.topdevicestatusdisplay
}q.push(B);
var D=false;
var h=false;
for(var E in a){if(o[E]){var M=o[E];
q.push(M)
}}if(o["sl:orderpartnumber"]){var M=o["sl:orderpartnumber"];
if(M.length>0){h=true
}}if(h&&o.displayInBuyPopup=="true"){M='<a class="order-link order-link-buy" data-group="'+n+'" data-opn="'+o["sl:orderpartnumber"]+'" data-device="'+s+'" data-device-path="'+o.devicePath+'">Select</a>';
q.push(M);
f=true
}else{q.push("")
}if(o["sl:buyavailable"]){if(o["sl:buyavailable"]=="true"){D=true
}}if(D&&h){e.push(q)
}}var p=[];
p.push({sTitle:"<span>Part Number</span>"});
var a=H.buySamplePartGroup.partGroupFilterList;
for(var E in a){p.push({sTitle:"<span>"+a[E]+"</span>"})
}if(f){p.push({sTitle:"<span class='table-popup-hdr-nosort'>&nbsp;</span>",orderable:false})
}var z=H.buySamplePartGroup.partGroupDisplayName;
F="<h3>"+z+" Variations</h3>";
l=[];
d=[[0,"asc"]]
}else{if(g=="groupsample"){e=[];
var t=H.sampleShoppingCartURL;
var A=H.buySamplePartGroup.partGroupDeviceData;
var a=H.buySamplePartGroup.partGroupFilterList;
for(var s in A){var q=new Array();
var o=A[s];
var B=s;
if(o.topdevicestatusdisplay){B+=" "+o.topdevicestatusdisplay
}q.push(B);
var K;
for(var E in a){if(o[E]){var M=o[E];
q.push(M)
}}K="";
if(o["sl:sampleavailable"]){var M=o["sl:sampleavailable"];
if(M=="true"){if(o["sl:orderpartnumber"]){var M=o["sl:orderpartnumber"];
if(M.length>0){var u=t.replace("[partnumber]",o["sl:orderpartnumber"]);
K='<a href="'+u+'" target="_blank">Sample</a>'
}}}}q.push(K);
e.push(q)
}var p=[];
p.push({sTitle:"<span>Part Number</span>"});
var a=H.buySamplePartGroup.partGroupFilterList;
for(var E in a){p.push({sTitle:"<span>"+a[E]+"</span>"})
}p.push({sTitle:"<span class='table-popup-hdr-nosort'>&nbsp;</span>",orderable:false});
F="<h3>Sample</h3>";
l=[];
d=[[0,"asc"]]
}else{if(g=="groupcustom"){e=[];
var t=H.sampleShoppingCartURL;
var A=H.buySamplePartGroup.partGroupDeviceData;
var a=H.buySamplePartGroup.partGroupFilterList;
for(var s in A){var q=new Array();
q.push(s);
var o=A[s];
var L="";
for(var E in a){if(o[E]){var M=o[E];
q.push(M)
}}if(o["sl:customavailable"]){var I=o["sl:customavailable"];
if(H.customAvailableConfigUrls[I]){var u=H.customAvailableConfigUrls[I].replace("{0}",s);
L='<a href="'+u+'">Customize</a>'
}}q.push(L);
e.push(q)
}var p=[];
p.push({sTitle:"<span>Part Number</span>"});
var a=H.buySamplePartGroup.partGroupFilterList;
for(var E in a){p.push({sTitle:"<span>"+a[E]+"</span>"})
}p.push({sTitle:"<span class='table-popup-hdr-nosort'>&nbsp;</span>",orderable:false});
var z=H.buySamplePartGroup.partGroupDisplayName;
F+="<h3>Customize</h3>";
F+='<div class="opn-title"><span class="opn-text">'+z+"</span></div>";
l=[];
d=[[0,"asc"]]
}}}}}sl.components.tablePopup.util.addTheDataToPopup(e,p,F,G,l,d,g,k,n,H.noneFoundMessage,v)
},addTheDataToPopup:function(o,a,c,l,b,k,h,n,e,v,p){var t=$(".table-popup-container");
var u="";
if(o.length==0){sl.components.tablePopup.util.showErrorMessage(h,v)
}else{if(c.length>0){t.append(c)
}var g='<table class="table-popup-data-grid row-border" cellspacing="0" border="0" cellspacing="0" width="100%"></table>';
t.append(g);
var r=$(".table-popup-data-grid").DataTable({destroy:true,paging:false,info:false,searching:false,data:o,columns:a,columnDefs:b,order:k});
if(h=="buy"||h=="groupbuy"){u='<div class="mobile-display">';
for(var q=0;
q<o.length;
q++){u+='<div class="mobile-display-outer-div '+h+'">';
u+='<div class="mobile-display-outer-div-row">';
u+='<div class="mobile-display-outer-div-col title">';
if(h=="buy"){u+="Part No"
}else{u+="Device"
}u+="</div>";
u+='<div class="mobile-display-outer-div-col">'+o[q][0]+"</div>";
u+="</div>";
var s="";
for(var f=0;
f<a.length;
f++){if(!a[f].sTitle.includes("Part Number")){if(a[f].sTitle.includes("Distributor")){s+='<div class="mobile-display-outer-div-row">';
s+='<div class="mobile-display-outer-div-col title">'+a[f].sTitle+"</div>";
s+='<div class="mobile-display-outer-div-col">'+o[q][f]+"</div>";
s+="</div>"
}else{if(a[f].sTitle.includes("Stock")){u+=s
}if(a[f].sTitle.includes("&nbsp;")){u+='<div class="mobile-display-outer-div-button">';
u+=o[q][f];
u+="</div>"
}else{u+='<div class="mobile-display-outer-div-row">';
u+='<div class="mobile-display-outer-div-col title">'+a[f].sTitle+"</div>";
u+='<div class="mobile-display-outer-div-col">'+o[q][f]+"</div>";
u+="</div>"
}}}}u+="</div>"
}u+="</div>"
}if(p.length>0){t.append(p);
var d=document.getElementsByClassName("popup-filter-option");
var m=function(){var x=$(this);
var w=x.attr("name");
$("table.table-popup-data-grid tr").each(function(){if(w==this.children[2].textContent){if(x.is(":checked")){$(this).removeClass("filtered")
}else{$(this).addClass("filtered")
}}});
$("div.mobile-display-outer-div-col").each(function(){if(w==this.textContent){if(x.is(":checked")){$(this.parentElement.parentElement).removeClass("filtered")
}else{$(this.parentElement.parentElement).addClass("filtered")
}}})
};
if(!Array.from){Array.from=function(w){return[].slice.call(w)
}
}else{Array.from(d).forEach(function(w){w.addEventListener("click",m)
})
}}if(u!=""){t.append(u);
$("div.dataTables_wrapper").addClass("has-mobile");
$("div.mobile-display-outer-div-button > a").addClass("button");
if(h=="buy"){$("div.mobile-display-outer-div-button > a").addClass("primary-bromine");
$("div.mobile-display-outer-div-button").addClass("primary-bromine")
}else{$("div.mobile-display-outer-div-button > a").addClass("primary-indium");
$("div.mobile-display-outer-div-button").addClass("primary-indium")
}}if(l.length>0){t.append(l)
}if(h=="buy"||h=="groupsample"||h=="groupcustom"){$(".table-popup-data-grid tr").each(function(){$(this).find("a:last").addClass("button primary-bromine")
})
}else{if(h=="groupbuy"){$(".table-popup-data-grid tr").each(function(){$(this).find("a:last").addClass("button primary-indium")
})
}}$(".loading-div").removeClass("icon-loading")
}},showErrorMessage:function(a,c){var b=$(".table-popup-container");
if(c){b.append(c)
}else{b.append("<h3>No data was found at this time.</h3>")
}$(".loading-div").removeClass("icon-loading")
}}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.contentList={init:function(){var b=this,a=$(".content-list");
var d=$('<tr class=""><td class="title"></td><td class="topic"></td><td class="last-modified"></td></tr>');
var c=[];
if(a.length>0){$.fn.dataTable.ext.search.push(function(g,h,e){if($(g.nTable).parents(".content-list").length>0){var f=false;
if(g.oPreviousSearch.sSearch==""){f=true
}else{var k=h[1].replace(/, /g,",").split(",");
k.forEach(function(m,l){if(c.indexOf(m)>-1){f=true
}})
}return f
}else{return true
}})
}a.each(function(){var l=$(this),h=l.data("contentType");
var g=l.data("resultsPerPage");
g=g&&g>0?g:20;
var k=l.data("ordering");
var f=l.find("table").DataTable({searching:true,autoWidth:false,paging:true,lengthChange:false,pageLength:g,ordering:true,order:k=="title"?[[0,"asc"]]:[[2,"desc"]],info:false,language:{emptyTable:"No results available"},columns:[{searchable:false},null,{visible:false}]});
var e=sl.components.facetedSearch.util.convertTagPathToSelector(h);
$.get("/sl/search/contentlistquery.json/"+e+".json",function(p){for(var o=0;
o<p.length;
o++){var n=p[o],m=d.clone().addClass("clone");
m.children(".title").append('<a href="'+n.path+'">'+n.title+"</a>");
m.children(".last-modified").append(n.lastModifiedDate);
m.attr("data-topics",n.tags);
n.tags.forEach(function(q,r){var s=r<n.tags.length-1?", ":"";
m.children(".topic").append('<a href="#" data-filter="'+q+'">'+q+"</a>"+s)
});
f.row.add(m)
}f.draw();
l.show()
})
});
a.on("click","[data-filter]",function(m){m.preventDefault();
var l=$(this).parents(".content-list"),n=l.find("table").DataTable(),h=l.find(".filter-bar"),f=l.find(".label-list"),k=$(this).data("filter");
if(k==""){c=[];
h.removeClass("active");
f.children().remove();
n.search("")
}else{var g=$('<span class="label" data-filter="'+k+'">'+k+"</span>");
if(f.children('[data-filter="'+k+'"]').length>0){if($(this).is(".label")){f.children('[data-filter="'+k+'"]').remove();
c.splice(c.indexOf(k),1)
}}else{f.append(g);
c.push(k)
}if(f.children().length>0){h.addClass("active")
}else{h.removeClass("active");
n.search("")
}c.forEach(function(o,e){n.search(o,false,false,false)
})
}n.draw();
return false
})
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.contactSales={init:function(){var e=this,b=".contact-sales",d=$(b);
if(d.length>0){e.setEvents(d);
e.initDeepLinking(d);
e.prePopulateFormData(d);
var c=sl.utils.getCookies()["contactSales"];
var a=null;
if(typeof c!="undefined"){var a=sl.utils.parseCookie(c);
d.find('[name="hdnPartNumber"]').val(a.partNumber);
d.find('[name="hdnProductFamily"]').val(a.productFamily);
d.find('[name="hdnProductCategory"]').val(a.productCategory);
d.find('[name="hdnProductLine"]').val(a.productLine);
d.find('[name="hdnReferrerURL"]').val(a.referrerUrl);
d.find(".return-to-previous-page").attr("href",a.referrerUrl)
}}},initDeepLinking:function(g){var f=this;
if(document.location.search.indexOf("view=")>-1){var a=f.utils.getParameter("view"),d=g.find('.tab[data-tab="'+a+'"]');
if(d.length>0&&!d.hasClass("active")){d.trigger("click")
}}if(document.location.search.indexOf("SearchLocation=")>-1){var e=f.utils.getParameter("SearchLocation");
var h=e,c=null;
if(e.indexOf(",")>-1){c=e.split(",")[0];
h=e.split(",")[1]
}else{var b=g.find('.state-dropdown option[value="'+e+'"]');
if(b.length>0){c=e;
h=b.parent().attr("data-country")
}}g.find('.tab[data-tab="map"]').trigger("click");
g.find('.region-item[data-country="'+h+'"]').trigger("click");
f.updateSalesLocations(g,e)
}},updateSalesLocations:function(k,a){var l=this,n=k.find(".regions"),o=n.siblings(".sales-locations");
if(a!=null){l.utils.updateParameters("SearchLocation",a);
var d=a,b=null,c=false;
if(a.indexOf(",")>-1){b=a.split(",")[0];
d=a.split(",")[1]
}else{var g=k.find('.state-dropdown option[value="'+a+'"]');
if(g.length>0){b=a;
d=g.parent().attr("data-country")
}}o.find("span.country").text((b?b+", ":"")+d);
o.find("#country").val(d);
o.find(".state-dropdown").hide();
var e=o.find('.state-dropdown[data-country="'+d+'"]');
if(e.length>0){e.show().val(b);
c=true
}var m=o.find("tbody tr.sales-location");
m.hide();
var f=m.filter(function(p,s){var r=false,u=$(s),t=u.attr("data-state")?u.attr("data-state").split(","):[],q=u.attr("data-country")?u.attr("data-country").split(","):[];
if(b||c){if(t.indexOf(b)>-1){r=true
}}else{if(d&&q.indexOf(d)>-1){r=true
}}return(r)
});
f.show();
o.find("tbody tr.sales-type-header").hide();
f.each(function(){var p=$(this).attr("data-type");
$(this).siblings('.sales-type-header[data-sales-type="'+p+'"]').show()
});
if(f.length>0){o.find(".has-sales-locations").show();
o.find(".no-sales-locations").hide()
}else{o.find(".has-sales-locations").hide();
o.find(".no-sales-locations").show()
}var h=o.find(".sales-locations-body");
if(c){if(b==null){h.hide()
}else{h.show()
}}n.hide();
o.show()
}else{l.utils.updateParameters("SearchLocation",null);
n.show();
o.hide()
}},setEvents:function(b){var a=this;
b.find(".tab").on("click",function(){var c=$(this).data("tab");
$(this).siblings().removeClass("active");
$(this).addClass("active");
b.find(".tab-content[data-tab]").removeClass("active");
b.find('.tab-content[data-tab="'+c+'"]').addClass("active");
a.utils.updateParameters("view",c);
if(c!=="map"){a.utils.updateParameters("SearchLocation",null)
}});
b.find(".back-to-map").on("click",function(){a.updateSalesLocations(b)
});
b.find(".region-item").on("click",function(){var c=$(this).text().trim();
a.updateSalesLocations(b,c)
});
b.find(".region-dropdowns select").on("change",function(){var d=$(this).val().trim();
if($(this).is(".state-dropdown")){var c=$(this).attr("data-country");
d=d+","+c
}a.updateSalesLocations(b,d)
});
b.find("#ddlCountry").on("change",function(){var c=$(this).val(),e=$(this).find("option:selected").text();
if(sl.contactSalesCountries){var d=null;
sl.contactSalesCountries.forEach(function(g){if(g.countryName.toLowerCase()===e.toLowerCase()){d=g;
return false
}});
var f=b.find("#ddlState");
f.empty();
f.removeAttr("required");
if(d&&d.states){f.attr("required",true);
f.append('<option value="">Select a state</option>');
d.states.forEach(function(g){f.append('<option value="'+g+'">'+g+"</option>")
})
}}});
b.find("#ddlCountry, #ddlState").on("change",function(){var d=b.find("#ddlDistributor");
d.empty();
d.parents(".conditional-field").hide();
if(sl.contactSalesLocations){var g=b.find("#ddlCountry option:selected").text(),e=b.find("#ddlState option:selected").text();
if(g=="Canada"){if(e=="Alberta"||e=="British Columbia"||e=="Northwest Territories"||e=="Yukon Territory"||e=="Saskatchewan"||e=="Manitoba"){e="Western Canada"
}else{e="Eastern Canada"
}}for(var c=0;
c<sl.contactSalesLocations.length;
c++){var f=sl.contactSalesLocations[c];
if(f.regions.indexOf(g)>-1||f.regions.indexOf(e)>-1){d.append('<option value="'+f.salesLocatorName+'">'+f.salesLocatorName+"</option>");
d.parents(".conditional-field").show()
}}}});
b.find("#contact-sales-reason").on("change",function(){var c=$(this).val();
b.find("[data-reason]").hide();
b.find('[data-reason*="'+c+'"]').show();
if(b.find('[data-reason*="'+c+'"]').find("#hdnContactSalesReason")){b.find('[data-reason*="'+c+'"]').find("#hdnContactSalesReason").val(c)
}});
$(window).on("popstate",function(c){a.initDeepLinking(b)
});
b.find(".submit-button").on("click",function(){var c=$(this).parents("form"),e=c.find(".error-messages"),f=[],d=c.find("input, select, textarea");
d.each(function(){var g=$(this),k=g[0].validity.valid,h=g.attr("data-error-message");
if(k){g.removeClass("invalid")
}else{g.addClass("invalid");
if(f.indexOf(h)==-1){f.push(h)
}}});
if(f.length>0){e.show();
e.find("ul").empty();
f.forEach(function(g){e.find("ul").append("<li>"+g+"</li>")
})
}else{e.hide();
c.submit()
}});
b.find("form").on("submit",function(){var d=$(this),c={};
d.find("input, select, textarea").each(function(){c[$(this).attr("name")]=$(this).val()
});
$.post({url:"/sl/contact-sales",data:c,success:function(e){d.hide();
d.siblings(".thank-you").show();
sl.utils.deleteCookie("contactSales")
}});
return false
})
},prePopulateFormData:function(d){var c=sl.utils.getCookies();
if(typeof window.silabsProfileCookie!=="undefined"&&typeof c[window.silabsProfileCookie]!=="undefined"){var b=sl.utils.parseCookie(c[window.silabsProfileCookie],"|");
for(var a in b){d.find('form [name*="'+a+'"]').val(b[a]).trigger("change")
}}},utils:{getParameter:function(a){var c=new RegExp(".+"+a+"=([^&?]+).*$","g"),b=document.location.search.replace(c,"$1");
return decodeURIComponent(b)
},updateParameters:function(c,g){var b=window.location.pathname,d=window.location.search.replace("?","").split("&"),e={},h=[];
d.forEach(function(l){var m=l.split("=")[0],k=l.split("=")[1];
e[m]=k
});
if(g!==null&&g!==""){e[c]=g
}else{delete e[c]
}var a=[];
for(var c in e){a.push(c+"="+e[c])
}var f="?"+a.join("&");
window.history.pushState({url:b},null,b+f)
},createMarketoLead:function(){}}};
var hljs=new function(){function F(a){return a.replace(/&/gm,"&amp;").replace(/</gm,"&lt;").replace(/>/gm,"&gt;")
}function w(a){return a.nodeName.toLowerCase()
}function H(b,a){var c=b&&b.exec(a);
return c&&c.index==0
}function M(a){return Array.prototype.map.call(a.childNodes,function(b){if(b.nodeType==3){return O.useBR?b.nodeValue.replace(/\n/g,""):b.nodeValue
}if(w(b)=="br"){return"\n"
}return M(b)
}).join("")
}function y(a){var b=(a.className+" "+(a.parentNode?a.parentNode.className:"")).split(/\s+/);
b=b.map(function(c){return c.replace(/^language-/,"")
});
return b.filter(function(c){return G(c)||c=="no-highlight"
})[0]
}function B(a,d){var c={};
for(var b in a){c[b]=a[b]
}if(d){for(var b in d){c[b]=d[b]
}}return c
}function v(a){var c=[];
(function b(f,e){for(var d=f.firstChild;
d;
d=d.nextSibling){if(d.nodeType==3){e+=d.nodeValue.length
}else{if(w(d)=="br"){e+=1
}else{if(d.nodeType==1){c.push({event:"start",offset:e,node:d});
e=b(d,e);
c.push({event:"stop",offset:e,node:d})
}}}}return e
})(a,0);
return c
}function z(h,f,b){var g=0;
var l="";
var e=[];
function c(){if(!h.length||!f.length){return h.length?h:f
}if(h[0].offset!=f[0].offset){return(h[0].offset<f[0].offset)?h:f
}return f[0].event=="start"?h:f
}function d(n){function o(p){return" "+p.nodeName+'="'+F(p.value)+'"'
}l+="<"+w(n)+Array.prototype.map.call(n.attributes,o).join("")+">"
}function m(n){l+="</"+w(n)+">"
}function k(n){(n.event=="start"?d:m)(n.node)
}while(h.length||f.length){var a=c();
l+=F(b.substr(g,a[0].offset-g));
g=a[0].offset;
if(a==h){e.reverse().forEach(m);
do{k(a.splice(0,1)[0]);
a=c()
}while(a==h&&a.length&&a[0].offset==g);
e.reverse().forEach(d)
}else{if(a[0].event=="start"){e.push(a[0].node)
}else{e.pop()
}k(a.splice(0,1)[0])
}}return l+F(b.substr(g))
}function D(d){function c(e){return(e&&e.source)||e
}function b(e,f){return RegExp(c(e),"m"+(d.cI?"i":"")+(f?"g":""))
}function a(g,k){if(g.compiled){return
}g.compiled=true;
g.k=g.k||g.bK;
if(g.k){var h={};
function f(m,n){if(d.cI){n=n.toLowerCase()
}n.split(" ").forEach(function(p){var o=p.split("|");
h[o[0]]=[m,o[1]?Number(o[1]):1]
})
}if(typeof g.k=="string"){f("keyword",g.k)
}else{Object.keys(g.k).forEach(function(m){f(m,g.k[m])
})
}g.k=h
}g.lR=b(g.l||/\b[A-Za-z0-9_]+\b/,true);
if(k){if(g.bK){g.b=g.bK.split(" ").join("|")
}if(!g.b){g.b=/\B|\b/
}g.bR=b(g.b);
if(!g.e&&!g.eW){g.e=/\B|\b/
}if(g.e){g.eR=b(g.e)
}g.tE=c(g.e)||"";
if(g.eW&&k.tE){g.tE+=(g.e?"|":"")+k.tE
}}if(g.i){g.iR=b(g.i)
}if(g.r===undefined){g.r=1
}if(!g.c){g.c=[]
}var l=[];
g.c.forEach(function(m){if(m.v){m.v.forEach(function(n){l.push(B(m,n))
})
}else{l.push(m=="self"?g:m)
}});
g.c=l;
g.c.forEach(function(m){a(m,g)
});
if(g.starts){a(g.starts,k)
}var e=g.c.map(function(m){return m.bK?"\\.?\\b("+m.b+")\\b\\.?":m.b
}).concat([g.tE]).concat([g.i]).map(c).filter(Boolean);
g.t=e.length?b(e.join("|"),true):{exec:function(m){return null
}};
g.continuation={}
}a(d)
}function N(a,h,n,b){function t(R,Q){for(var S=0;
S<Q.c.length;
S++){if(H(Q.c[S].bR,R)){return Q.c[S]
}}}function l(Q,R){if(H(Q.eR,R)){return Q
}if(Q.eW){return l(Q.parent,R)
}}function X(R,Q){return !n&&H(Q.iR,R)
}function T(Q,S){var R=g.cI?S[0].toLowerCase():S[0];
return Q.k.hasOwnProperty(R)&&Q.k[R]
}function r(ab,ad,Q,R){var aa=R?"":O.classPrefix,S='<span class="'+aa,ac=Q?"":"</span>";
S+=ab+'">';
return S+ad+ac
}function f(){var S=F(V);
if(!p.k){return S
}var Y="";
var Z=0;
p.lR.lastIndex=0;
var R=p.lR.exec(S);
while(R){Y+=S.substr(Z,R.index-Z);
var Q=T(p,R);
if(Q){q+=Q[1];
Y+=r(Q[0],R[0])
}else{Y+=R[0]
}Z=p.lR.lastIndex;
R=p.lR.exec(S)
}return Y+S.substr(Z)
}function u(){if(p.sL&&!K[p.sL]){return F(V)
}var Q=p.sL?N(p.sL,V,true,p.continuation.top):J(V);
if(p.r>0){q+=Q.r
}if(p.subLanguageMode=="continuous"){p.continuation.top=Q.top
}return r(Q.language,Q.value,false,true)
}function c(){return p.sL!==undefined?u():f()
}function d(Q,R){var S=Q.cN?r(Q.cN,"",true):"";
if(Q.rB){U+=S;
V=""
}else{if(Q.eB){U+=F(R)+S;
V=""
}else{U+=S;
V=R
}}p=Object.create(Q,{parent:{value:p}})
}function s(Y,Z){V+=Y;
if(Z===undefined){U+=c();
return 0
}var R=t(Z,p);
if(R){U+=c();
d(R,Z);
return R.rB?0:Z.length
}var Q=l(p,Z);
if(Q){var S=p;
if(!(S.rE||S.eE)){V+=Z
}U+=c();
do{if(p.cN){U+="</span>"
}q+=p.r;
p=p.parent
}while(p!=Q.parent);
if(S.eE){U+=F(Z)
}V="";
if(Q.starts){d(Q.starts,"")
}return S.rE?0:Z.length
}if(X(Z,p)){throw new Error('Illegal lexeme "'+Z+'" for mode "'+(p.cN||"<unnamed>")+'"')
}V+=Z;
return Z.length||1
}var g=G(a);
if(!g){throw new Error('Unknown language: "'+a+'"')
}D(g);
var p=b||g;
var U="";
for(var k=p;
k!=g;
k=k.parent){if(k.cN){U=r(k.cN,U,true)
}}var V="";
var q=0;
try{var W,m,o=0;
while(true){p.t.lastIndex=o;
W=p.t.exec(h);
if(!W){break
}m=s(h.substr(o,W.index-o),W[0]);
o=W.index+m
}s(h.substr(o));
for(var k=p;
k.parent;
k=k.parent){if(k.cN){U+="</span>"
}}return{r:q,value:U,language:a,top:p}
}catch(e){if(e.message.indexOf("Illegal")!=-1){return{r:0,value:F(h)}
}else{throw e
}}}function J(d,a){a=a||O.languages||Object.keys(K);
var c={r:0,value:F(d)};
var b=c;
a.forEach(function(f){if(!G(f)){return
}var e=N(f,d,false);
e.language=f;
if(e.r>b.r){b=e
}if(e.r>c.r){b=c;
c=e
}});
if(b.language){c.second_best=b
}return c
}function I(a){if(O.tabReplace){a=a.replace(/^((<[^>]+>|\t)+)/gm,function(c,d,e,b){return d.replace(/\t/g,O.tabReplace)
})
}if(O.useBR){a=a.replace(/\n/g,"<br>")
}return a
}function A(e){var f=M(e);
var b=y(e);
if(b=="no-highlight"){return
}var d=b?N(b,f,true):J(f);
var c=v(e);
if(c.length){var a=document.createElementNS("http://www.w3.org/1999/xhtml","pre");
a.innerHTML=d.value;
d.value=z(c,v(a),f)
}d.value=I(d.value);
e.innerHTML=d.value;
e.className+=" hljs "+(!b&&d.language||"");
e.result={language:d.language,re:d.r}
}var O={classPrefix:"hljs-",tabReplace:null,useBR:false,languages:undefined};
function x(a){O=B(O,a)
}function E(){if(E.called){return
}E.called=true;
var a=document.querySelectorAll("pre code");
Array.prototype.forEach.call(a,A)
}function P(){addEventListener("DOMContentLoaded",E,false);
addEventListener("load",E,false)
}var K={};
var C={};
function L(c,a){var b=K[c]=a(this);
if(b.aliases){b.aliases.forEach(function(d){C[d]=c
})
}}function G(a){return K[a]||K[C[a]]
}this.highlight=N;
this.highlightAuto=J;
this.fixMarkup=I;
this.highlightBlock=A;
this.configure=x;
this.initHighlighting=E;
this.initHighlightingOnLoad=P;
this.registerLanguage=L;
this.getLanguage=G;
this.inherit=B;
this.IR="[a-zA-Z][a-zA-Z0-9_]*";
this.UIR="[a-zA-Z_][a-zA-Z0-9_]*";
this.NR="\\b\\d+(\\.\\d+)?";
this.CNR="(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)";
this.BNR="\\b(0b[01]+)";
this.RSR="!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~";
this.BE={b:"\\\\[\\s\\S]",r:0};
this.ASM={cN:"string",b:"'",e:"'",i:"\\n",c:[this.BE]};
this.QSM={cN:"string",b:'"',e:'"',i:"\\n",c:[this.BE]};
this.CLCM={cN:"comment",b:"//",e:"$"};
this.CBLCLM={cN:"comment",b:"/\\*",e:"\\*/"};
this.HCM={cN:"comment",b:"#",e:"$"};
this.NM={cN:"number",b:this.NR,r:0};
this.CNM={cN:"number",b:this.CNR,r:0};
this.BNM={cN:"number",b:this.BNR,r:0};
this.REGEXP_MODE={cN:"regexp",b:/\//,e:/\/[gim]*/,i:/\n/,c:[this.BE,{b:/\[/,e:/\]/,r:0,c:[this.BE]}]};
this.TM={cN:"title",b:this.IR,r:0};
this.UTM={cN:"title",b:this.UIR,r:0}
}();
hljs.registerLanguage("bash",function(e){var f={cN:"variable",v:[{b:/\$[\w\d#@][\w\d_]*/},{b:/\$\{(.*?)\}/}]};
var g={cN:"string",b:/"/,e:/"/,c:[e.BE,f,{cN:"variable",b:/\$\(/,e:/\)/,c:[e.BE]}]};
var h={cN:"string",b:/'/,e:/'/};
return{l:/-?[a-z\.]+/,k:{keyword:"if then else elif fi for break continue while in do done exit return set declare case esac export exec",literal:"true false",built_in:"printf echo read cd pwd pushd popd dirs let eval unset typeset readonly getopts source shopt caller type hash bind help sudo",operator:"-ne -eq -lt -gt -f -d -e -s -l -a"},c:[{cN:"shebang",b:/^#![^\n]+sh\s*$/,r:10},{cN:"function",b:/\w[\w\d_]*\s*\(\s*\)\s*\{/,rB:true,c:[e.inherit(e.TM,{b:/\w[\w\d_]*/})],r:0},e.HCM,e.NM,g,h,f]}
});
hljs.registerLanguage("cs",function(c){var d="abstract as base bool break byte case catch char checked const continue decimal default delegate do double else enum event explicit extern false finally fixed float for foreach goto if implicit in int interface internal is lock long new null object operator out override params private protected public readonly ref return sbyte sealed short sizeof stackalloc static string struct switch this throw true try typeof uint ulong unchecked unsafe ushort using virtual volatile void while async await ascending descending from get group into join let orderby partial select set value var where yield";
return{k:d,c:[{cN:"comment",b:"///",e:"$",rB:true,c:[{cN:"xmlDocTag",b:"///|<!--|-->"},{cN:"xmlDocTag",b:"</?",e:">"}]},c.CLCM,c.CBLCLM,{cN:"preprocessor",b:"#",e:"$",k:"if else elif endif define undef warning error line region endregion pragma checksum"},{cN:"string",b:'@"',e:'"',c:[{b:'""'}]},c.ASM,c.QSM,c.CNM,{bK:"protected public private internal",e:/[{;=]/,k:d,c:[{bK:"class namespace interface",starts:{c:[c.TM]}},{b:c.IR+"\\s*\\(",rB:true,c:[c.TM]}]}]}
});
hljs.registerLanguage("ruby",function(o){var l="[a-zA-Z_]\\w*[!?=]?|[-+~]\\@|<<|>>|=~|===?|<=>|[<>]=?|\\*\\*|[-/+%^&*~`|]|\\[\\]=?";
var m="and false then defined module in return redo if BEGIN retry end for true self when next until do begin unless END rescue nil else break undef not super class case require yield alias while ensure elsif or include attr_reader attr_writer attr_accessor";
var s={cN:"yardoctag",b:"@[A-Za-z]+"};
var k={cN:"comment",v:[{b:"#",e:"$",c:[s]},{b:"^\\=begin",e:"^\\=end",c:[s],r:10},{b:"^__END__",e:"\\n$"}]};
var q={cN:"subst",b:"#\\{",e:"}",k:m};
var p={cN:"string",c:[o.BE,q],v:[{b:/'/,e:/'/},{b:/"/,e:/"/},{b:"%[qw]?\\(",e:"\\)"},{b:"%[qw]?\\[",e:"\\]"},{b:"%[qw]?{",e:"}"},{b:"%[qw]?<",e:">",r:10},{b:"%[qw]?/",e:"/",r:10},{b:"%[qw]?%",e:"%",r:10},{b:"%[qw]?-",e:"-",r:10},{b:"%[qw]?\\|",e:"\\|",r:10},{b:/\B\?(\\\d{1,3}|\\x[A-Fa-f0-9]{1,2}|\\u[A-Fa-f0-9]{4}|\\?\S)\b/}]};
var r={cN:"params",b:"\\(",e:"\\)",k:m};
var n=[p,k,{cN:"class",bK:"class module",e:"$|;",i:/=/,c:[o.inherit(o.TM,{b:"[A-Za-z_]\\w*(::\\w+)*(\\?|\\!)?"}),{cN:"inheritance",b:"<\\s*",c:[{cN:"parent",b:"("+o.IR+"::)?"+o.IR}]},k]},{cN:"function",bK:"def",e:" |$|;",r:0,c:[o.inherit(o.TM,{b:l}),r,k]},{cN:"constant",b:"(::)?(\\b[A-Z]\\w*(::)?)+",r:0},{cN:"symbol",b:":",c:[p,{b:l}],r:0},{cN:"symbol",b:o.UIR+"(\\!|\\?)?:",r:0},{cN:"number",b:"(\\b0[0-7_]+)|(\\b0x[0-9a-fA-F_]+)|(\\b[1-9][0-9_]*(\\.[0-9_]+)?)|[0_]\\b",r:0},{cN:"variable",b:"(\\$\\W)|((\\$|\\@\\@?)(\\w+))"},{b:"("+o.RSR+")\\s*",c:[k,{cN:"regexp",c:[o.BE,q],i:/\n/,v:[{b:"/",e:"/[a-z]*"},{b:"%r{",e:"}[a-z]*"},{b:"%r\\(",e:"\\)[a-z]*"},{b:"%r!",e:"![a-z]*"},{b:"%r\\[",e:"\\][a-z]*"}]}],r:0}];
q.c=n;
r.c=n;
return{k:m,c:n}
});
hljs.registerLanguage("diff",function(b){return{c:[{cN:"chunk",r:10,v:[{b:/^\@\@ +\-\d+,\d+ +\+\d+,\d+ +\@\@$/},{b:/^\*\*\* +\d+,\d+ +\*\*\*\*$/},{b:/^\-\-\- +\d+,\d+ +\-\-\-\-$/}]},{cN:"header",v:[{b:/Index: /,e:/$/},{b:/=====/,e:/=====$/},{b:/^\-\-\-/,e:/$/},{b:/^\*{3} /,e:/$/},{b:/^\+\+\+/,e:/$/},{b:/\*{5}/,e:/\*{5}$/}]},{cN:"addition",b:"^\\+",e:"$"},{cN:"deletion",b:"^\\-",e:"$"},{cN:"change",b:"^\\!",e:"$"}]}
});
hljs.registerLanguage("javascript",function(b){return{aliases:["js"],k:{keyword:"in if for while finally var new function do return void else break catch instanceof with throw case default try this switch continue typeof delete let yield const class",literal:"true false null undefined NaN Infinity",built_in:"eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape Object Function Boolean Error EvalError InternalError RangeError ReferenceError StopIteration SyntaxError TypeError URIError Number Math Date String RegExp Array Float32Array Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Uint8Array Uint8ClampedArray ArrayBuffer DataView JSON Intl arguments require"},c:[{cN:"pi",b:/^\s*('|")use strict('|")/,r:10},b.ASM,b.QSM,b.CLCM,b.CBLCLM,b.CNM,{b:"("+b.RSR+"|\\b(case|return|throw)\\b)\\s*",k:"return throw case",c:[b.CLCM,b.CBLCLM,b.REGEXP_MODE,{b:/</,e:/>;/,r:0,sL:"xml"}],r:0},{cN:"function",bK:"function",e:/\{/,c:[b.inherit(b.TM,{b:/[A-Za-z$_][0-9A-Za-z$_]*/}),{cN:"params",b:/\(/,e:/\)/,c:[b.CLCM,b.CBLCLM],i:/["'\(]/}],i:/\[|%/},{b:/\$[(.]/},{b:"\\."+b.IR,r:0}]}
});
hljs.registerLanguage("xml",function(f){var h="[A-Za-z0-9\\._:-]+";
var g={b:/<\?(php)?(?!\w)/,e:/\?>/,sL:"php",subLanguageMode:"continuous"};
var e={eW:true,i:/</,r:0,c:[g,{cN:"attribute",b:h,r:0},{b:"=",r:0,c:[{cN:"value",v:[{b:/"/,e:/"/},{b:/'/,e:/'/},{b:/[^\s\/>]+/}]}]}]};
return{aliases:["html"],cI:true,c:[{cN:"doctype",b:"<!DOCTYPE",e:">",r:10,c:[{b:"\\[",e:"\\]"}]},{cN:"comment",b:"<!--",e:"-->",r:10},{cN:"cdata",b:"<\\!\\[CDATA\\[",e:"\\]\\]>",r:10},{cN:"tag",b:"<style(?=\\s|>|$)",e:">",k:{title:"style"},c:[e],starts:{e:"</style>",rE:true,sL:"css"}},{cN:"tag",b:"<script(?=\\s|>|$)",e:">",k:{title:"script"},c:[e],starts:{e:"<\/script>",rE:true,sL:"javascript"}},{b:"<%",e:"%>",sL:"vbscript"},g,{cN:"pi",b:/<\?\w+/,e:/\?>/,r:10},{cN:"tag",b:"</?",e:"/?>",c:[{cN:"title",b:"[^ /><]+",r:0},e]}]}
});
hljs.registerLanguage("markdown",function(b){return{c:[{cN:"header",v:[{b:"^#{1,6}",e:"$"},{b:"^.+?\\n[=-]{2,}$"}]},{b:"<",e:">",sL:"xml",r:0},{cN:"bullet",b:"^([*+-]|(\\d+\\.))\\s+"},{cN:"strong",b:"[*_]{2}.+?[*_]{2}"},{cN:"emphasis",v:[{b:"\\*.+?\\*"},{b:"_.+?_",r:0}]},{cN:"blockquote",b:"^>\\s+",e:"$"},{cN:"code",v:[{b:"`.+?`"},{b:"^( {4}|\t)",e:"$",r:0}]},{cN:"horizontal_rule",b:"^[-\\*]{3,}",e:"$"},{b:"\\[.+?\\][\\(\\[].+?[\\)\\]]",rB:true,c:[{cN:"link_label",b:"\\[",e:"\\]",eB:true,rE:true,r:0},{cN:"link_url",b:"\\]\\(",e:"\\)",eB:true,eE:true},{cN:"link_reference",b:"\\]\\[",e:"\\]",eB:true,eE:true}],r:10},{b:"^\\[.+\\]:",e:"$",rB:true,c:[{cN:"link_reference",b:"\\[",e:"\\]",eB:true,eE:true},{cN:"link_url",b:"\\s",e:"$"}]}]}
});
hljs.registerLanguage("css",function(e){var d="[a-zA-Z-][a-zA-Z0-9_-]*";
var f={cN:"function",b:d+"\\(",e:"\\)",c:["self",e.NM,e.ASM,e.QSM]};
return{cI:true,i:"[=/|']",c:[e.CBLCLM,{cN:"id",b:"\\#[A-Za-z0-9_-]+"},{cN:"class",b:"\\.[A-Za-z0-9_-]+",r:0},{cN:"attr_selector",b:"\\[",e:"\\]",i:"$"},{cN:"pseudo",b:":(:)?[a-zA-Z0-9\\_\\-\\+\\(\\)\\\"\\']+"},{cN:"at_rule",b:"@(font-face|page)",l:"[a-z-]+",k:"font-face page"},{cN:"at_rule",b:"@",e:"[{;]",c:[{cN:"keyword",b:/\S+/},{b:/\s/,eW:true,eE:true,r:0,c:[f,e.ASM,e.QSM,e.NM]}]},{cN:"tag",b:d,r:0},{cN:"rules",b:"{",e:"}",i:"[^\\s]",r:0,c:[e.CBLCLM,{cN:"rule",b:"[^\\s]",rB:true,e:";",eW:true,c:[{cN:"attribute",b:"[A-Z\\_\\.\\-]+",e:":",eE:true,i:"[^\\s]",starts:{cN:"value",eW:true,eE:true,c:[f,e.NM,e.QSM,e.ASM,e.CBLCLM,{cN:"hexcolor",b:"#[0-9A-Fa-f]+"},{cN:"important",b:"!important"}]}}]}]}]}
});
hljs.registerLanguage("http",function(b){return{i:"\\S",c:[{cN:"status",b:"^HTTP/[0-9\\.]+",e:"$",c:[{cN:"number",b:"\\b\\d{3}\\b"}]},{cN:"request",b:"^[A-Z]+ (.*?) HTTP/[0-9\\.]+$",rB:true,e:"$",c:[{cN:"string",b:" ",e:" ",eB:true,eE:true}]},{cN:"attribute",b:"^\\w",e:": ",eE:true,i:"\\n|\\s|=",starts:{cN:"string",e:"$"}},{b:"\\n\\n",starts:{sL:"",eW:true}}]}
});
hljs.registerLanguage("java",function(c){var d="false synchronized int abstract float private char boolean static null if const for true while long throw strictfp finally protected import native final return void enum else break transient new catch instanceof byte super volatile case assert short package default double public try this switch continue throws";
return{k:d,i:/<\//,c:[{cN:"javadoc",b:"/\\*\\*",e:"\\*/",c:[{cN:"javadoctag",b:"(^|\\s)@[A-Za-z]+"}],r:10},c.CLCM,c.CBLCLM,c.ASM,c.QSM,{bK:"protected public private",e:/[{;=]/,k:d,c:[{cN:"class",bK:"class interface",eW:true,i:/[:"<>]/,c:[{bK:"extends implements",r:10},c.UTM]},{b:c.UIR+"\\s*\\(",rB:true,c:[c.UTM]}]},c.CNM,{cN:"annotation",b:"@[A-Za-z]+"}]}
});
hljs.registerLanguage("php",function(f){var h={cN:"variable",b:"\\$+[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*"};
var g={cN:"preprocessor",b:/<\?(php)?|\?>/};
var l={cN:"string",c:[f.BE,g],v:[{b:'b"',e:'"'},{b:"b'",e:"'"},f.inherit(f.ASM,{i:null}),f.inherit(f.QSM,{i:null})]};
var k={v:[f.BNM,f.CNM]};
return{cI:true,k:"and include_once list abstract global private echo interface as static endswitch array null if endwhile or const for endforeach self var while isset public protected exit foreach throw elseif include __FILE__ empty require_once do xor return parent clone use __CLASS__ __LINE__ else break print eval new catch __METHOD__ case exception default die require __FUNCTION__ enddeclare final try switch continue endfor endif declare unset true false trait goto instanceof insteadof __DIR__ __NAMESPACE__ yield finally",c:[f.CLCM,f.HCM,{cN:"comment",b:"/\\*",e:"\\*/",c:[{cN:"phpdoc",b:"\\s@[A-Za-z]+"},g]},{cN:"comment",b:"__halt_compiler.+?;",eW:true,k:"__halt_compiler",l:f.UIR},{cN:"string",b:"<<<['\"]?\\w+['\"]?$",e:"^\\w+;",c:[f.BE]},g,h,{cN:"function",bK:"function",e:/[;{]/,i:"\\$|\\[|%",c:[f.UTM,{cN:"params",b:"\\(",e:"\\)",c:["self",h,f.CBLCLM,l,k]}]},{cN:"class",bK:"class interface",e:"{",i:/[:\(\$"]/,c:[{bK:"extends implements",r:10},f.UTM]},{bK:"namespace",e:";",i:/[\.']/,c:[f.UTM]},{bK:"use",e:";",c:[f.UTM]},{b:"=>"},l,k]}
});
hljs.registerLanguage("python",function(h){var k={cN:"prompt",b:/^(>>>|\.\.\.) /};
var g={cN:"string",c:[h.BE],v:[{b:/(u|b)?r?'''/,e:/'''/,c:[k],r:10},{b:/(u|b)?r?"""/,e:/"""/,c:[k],r:10},{b:/(u|r|ur)'/,e:/'/,r:10},{b:/(u|r|ur)"/,e:/"/,r:10},{b:/(b|br)'/,e:/'/},{b:/(b|br)"/,e:/"/},h.ASM,h.QSM]};
var m={cN:"number",r:0,v:[{b:h.BNR+"[lLjJ]?"},{b:"\\b(0o[0-7]+)[lLjJ]?"},{b:h.CNR+"[lLjJ]?"}]};
var l={cN:"params",b:/\(/,e:/\)/,c:["self",k,m,g]};
var n={e:/:/,i:/[${=;\n]/,c:[h.UTM,l]};
return{k:{keyword:"and elif is global as in if from raise for except finally print import pass return exec else break not with class assert yield try while continue del or def lambda nonlocal|10 None True False",built_in:"Ellipsis NotImplemented"},i:/(<\/|->|\?)/,c:[k,m,g,h.HCM,h.inherit(n,{cN:"function",bK:"def",r:10}),h.inherit(n,{cN:"class",bK:"class"}),{cN:"decorator",b:/@/,e:/$/},{b:/\b(print|exec)\(/}]}
});
hljs.registerLanguage("sql",function(b){return{cI:true,i:/[<>]/,c:[{cN:"operator",b:"\\b(begin|end|start|commit|rollback|savepoint|lock|alter|create|drop|rename|call|delete|do|handler|insert|load|replace|select|truncate|update|set|show|pragma|grant|merge)\\b(?!:)",e:";",eW:true,k:{keyword:"all partial global month current_timestamp using go revoke smallint indicator end-exec disconnect zone with character assertion to add current_user usage input local alter match collate real then rollback get read timestamp session_user not integer bit unique day minute desc insert execute like ilike|2 level decimal drop continue isolation found where constraints domain right national some module transaction relative second connect escape close system_user for deferred section cast current sqlstate allocate intersect deallocate numeric public preserve full goto initially asc no key output collation group by union session both last language constraint column of space foreign deferrable prior connection unknown action commit view or first into float year primary cascaded except restrict set references names table outer open select size are rows from prepare distinct leading create only next inner authorization schema corresponding option declare precision immediate else timezone_minute external varying translation true case exception join hour default double scroll value cursor descriptor values dec fetch procedure delete and false int is describe char as at in varchar null trailing any absolute current_time end grant privileges when cross check write current_date pad begin temporary exec time update catalog user sql date on identity timezone_hour natural whenever interval work order cascade diagnostics nchar having left call do handler load replace truncate start lock show pragma exists number trigger if before after each row merge matched database",aggregate:"count sum min max avg"},c:[{cN:"string",b:"'",e:"'",c:[b.BE,{b:"''"}]},{cN:"string",b:'"',e:'"',c:[b.BE,{b:'""'}]},{cN:"string",b:"`",e:"`",c:[b.BE]},b.CNM]},b.CBLCLM,{cN:"comment",b:"--",e:"$"}]}
});
hljs.registerLanguage("ini",function(b){return{cI:true,i:/\S/,c:[{cN:"comment",b:";",e:"$"},{cN:"title",b:"^\\[",e:"\\]"},{cN:"setting",b:"^[a-z0-9\\[\\]_-]+[ \\t]*=[ \\t]*",e:"$",c:[{cN:"value",eW:true,k:"on off true false yes no",c:[b.QSM,b.NM],r:0}]}]}
});
hljs.registerLanguage("perl",function(r){var q="getpwent getservent quotemeta msgrcv scalar kill dbmclose undef lc ma syswrite tr send umask sysopen shmwrite vec qx utime local oct semctl localtime readpipe do return format read sprintf dbmopen pop getpgrp not getpwnam rewinddir qqfileno qw endprotoent wait sethostent bless s|0 opendir continue each sleep endgrent shutdown dump chomp connect getsockname die socketpair close flock exists index shmgetsub for endpwent redo lstat msgctl setpgrp abs exit select print ref gethostbyaddr unshift fcntl syscall goto getnetbyaddr join gmtime symlink semget splice x|0 getpeername recv log setsockopt cos last reverse gethostbyname getgrnam study formline endhostent times chop length gethostent getnetent pack getprotoent getservbyname rand mkdir pos chmod y|0 substr endnetent printf next open msgsnd readdir use unlink getsockopt getpriority rindex wantarray hex system getservbyport endservent int chr untie rmdir prototype tell listen fork shmread ucfirst setprotoent else sysseek link getgrgid shmctl waitpid unpack getnetbyname reset chdir grep split require caller lcfirst until warn while values shift telldir getpwuid my getprotobynumber delete and sort uc defined srand accept package seekdir getprotobyname semop our rename seek if q|0 chroot sysread setpwent no crypt getc chown sqrt write setnetent setpriority foreach tie sin msgget map stat getlogin unless elsif truncate exec keys glob tied closedirioctl socket readlink eval xor readline binmode setservent eof ord bind alarm pipe atan2 getgrent exp time push setgrent gt lt or ne m|0 break given say state when";
var o={cN:"subst",b:"[$@]\\{",e:"\\}",k:q};
var n={b:"->{",e:"}"};
var l={cN:"variable",v:[{b:/\$\d/},{b:/[\$\%\@\*](\^\w\b|#\w+(\:\:\w+)*|{\w+}|\w+(\:\:\w*)*)/},{b:/[\$\%\@\*][^\s\w{]/,r:0}]};
var p={cN:"comment",b:"^(__END__|__DATA__)",e:"\\n$",r:5};
var m=[r.BE,o,l];
var k=[l,r.HCM,p,{cN:"comment",b:"^\\=\\w",e:"\\=cut",eW:true},n,{cN:"string",c:m,v:[{b:"q[qwxr]?\\s*\\(",e:"\\)",r:5},{b:"q[qwxr]?\\s*\\[",e:"\\]",r:5},{b:"q[qwxr]?\\s*\\{",e:"\\}",r:5},{b:"q[qwxr]?\\s*\\|",e:"\\|",r:5},{b:"q[qwxr]?\\s*\\<",e:"\\>",r:5},{b:"qw\\s+q",e:"q",r:5},{b:"'",e:"'",c:[r.BE]},{b:'"',e:'"'},{b:"`",e:"`",c:[r.BE]},{b:"{\\w+}",c:[],r:0},{b:"-?\\w+\\s*\\=\\>",c:[],r:0}]},{cN:"number",b:"(\\b0[0-7_]+)|(\\b0x[0-9a-fA-F_]+)|(\\b[1-9][0-9_]*(\\.[0-9_]+)?)|[0_]\\b",r:0},{b:"(\\/\\/|"+r.RSR+"|\\b(split|return|print|reverse|grep)\\b)\\s*",k:"split return print reverse grep",r:0,c:[r.HCM,p,{cN:"regexp",b:"(s|tr|y)/(\\\\.|[^/])*/(\\\\.|[^/])*/[a-z]*",r:10},{cN:"regexp",b:"(m|qr)?/",e:"/[a-z]*",c:[r.BE],r:0}]},{cN:"sub",bK:"sub",e:"(\\s*\\(.*?\\))?[;{]",r:5},{cN:"operator",b:"-\\w\\b",r:0}];
o.c=k;
n.c=k;
return{k:q,c:k}
});
hljs.registerLanguage("objectivec",function(f){var g={keyword:"int float while char export sizeof typedef const struct for union unsigned long volatile static bool mutable if do return goto void enum else break extern asm case short default double register explicit signed typename this switch continue wchar_t inline readonly assign self synchronized id nonatomic super unichar IBOutlet IBAction strong weak @private @protected @public @try @property @end @throw @catch @finally @synthesize @dynamic @selector @optional @required",literal:"false true FALSE TRUE nil YES NO NULL",built_in:"NSString NSDictionary CGRect CGPoint UIButton UILabel UITextView UIWebView MKMapView UISegmentedControl NSObject UITableViewDelegate UITableViewDataSource NSThread UIActivityIndicator UITabbar UIToolBar UIBarButtonItem UIImageView NSAutoreleasePool UITableView BOOL NSInteger CGFloat NSException NSLog NSMutableString NSMutableArray NSMutableDictionary NSURL NSIndexPath CGSize UITableViewCell UIView UIViewController UINavigationBar UINavigationController UITabBarController UIPopoverController UIPopoverControllerDelegate UIImage NSNumber UISearchBar NSFetchedResultsController NSFetchedResultsChangeType UIScrollView UIScrollViewDelegate UIEdgeInsets UIColor UIFont UIApplication NSNotFound NSNotificationCenter NSNotification UILocalNotification NSBundle NSFileManager NSTimeInterval NSDate NSCalendar NSUserDefaults UIWindow NSRange NSArray NSError NSURLRequest NSURLConnection UIInterfaceOrientation MPMoviePlayerController dispatch_once_t dispatch_queue_t dispatch_sync dispatch_async dispatch_once"};
var h=/[a-zA-Z@][a-zA-Z0-9_]*/;
var e="@interface @class @protocol @implementation";
return{k:g,l:h,i:"</",c:[f.CLCM,f.CBLCLM,f.CNM,f.QSM,{cN:"string",b:"'",e:"[^\\\\]'",i:"[^\\\\][^']"},{cN:"preprocessor",b:"#import",e:"$",c:[{cN:"title",b:'"',e:'"'},{cN:"title",b:"<",e:">"}]},{cN:"preprocessor",b:"#",e:"$"},{cN:"class",b:"("+e.split(" ").join("|")+")\\b",e:"({|$)",k:e,l:h,c:[f.UTM]},{cN:"variable",b:"\\."+f.UIR,r:0}]}
});
hljs.registerLanguage("coffeescript",function(n){var g={keyword:"in if for while finally new do return else break catch instanceof throw try this switch continue typeof delete debugger super then unless until loop of by when and or is isnt not",literal:"true false null undefined yes no on off",reserved:"case default function var void with const let enum export import native __hasProp __extends __slice __bind __indexOf",built_in:"npm require console print module exports global window document"};
var h="[A-Za-z$_][0-9A-Za-z$_]*";
var k=n.inherit(n.TM,{b:h});
var l={cN:"subst",b:/#\{/,e:/}/,k:g};
var m=[n.BNM,n.inherit(n.CNM,{starts:{e:"(\\s*/)?",r:0}}),{cN:"string",v:[{b:/'''/,e:/'''/,c:[n.BE]},{b:/'/,e:/'/,c:[n.BE]},{b:/"""/,e:/"""/,c:[n.BE,l]},{b:/"/,e:/"/,c:[n.BE,l]}]},{cN:"regexp",v:[{b:"///",e:"///",c:[l,n.HCM]},{b:"//[gim]*",r:0},{b:"/\\S(\\\\.|[^\\n])*?/[gim]*(?=\\s|\\W|$)"}]},{cN:"property",b:"@"+h},{b:"`",e:"`",eB:true,eE:true,sL:"javascript"}];
l.c=m;
return{k:g,c:m.concat([{cN:"comment",b:"###",e:"###"},n.HCM,{cN:"function",b:"("+h+"\\s*=\\s*)?(\\(.*\\))?\\s*\\B[-=]>",e:"[-=]>",rB:true,c:[k,{cN:"params",b:"\\(",rB:true,c:[{b:/\(/,e:/\)/,k:g,c:["self"].concat(m)}]}]},{cN:"class",bK:"class",e:"$",i:/[:="\[\]]/,c:[{bK:"extends",eW:true,i:/[:="\[\]]/,c:[k]},k]},{cN:"attribute",b:h+":",e:":",rB:true,eE:true,r:0}])}
});
hljs.registerLanguage("nginx",function(f){var d={cN:"variable",v:[{b:/\$\d+/},{b:/\$\{/,e:/}/},{b:"[\\$\\@]"+f.UIR}]};
var e={eW:true,l:"[a-z/_]+",k:{built_in:"on off yes no true false none blocked debug info notice warn error crit select break last permanent redirect kqueue rtsig epoll poll /dev/poll"},r:0,i:"=>",c:[f.HCM,{cN:"string",c:[f.BE,d],v:[{b:/"/,e:/"/},{b:/'/,e:/'/}]},{cN:"url",b:"([a-z]+):/",e:"\\s",eW:true,eE:true},{cN:"regexp",c:[f.BE,d],v:[{b:"\\s\\^",e:"\\s|{|;",rE:true},{b:"~\\*?\\s+",e:"\\s|{|;",rE:true},{b:"\\*(\\.[a-z\\-]+)+"},{b:"([a-z\\-]+\\.)+\\*"}]},{cN:"number",b:"\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(:\\d{1,5})?\\b"},{cN:"number",b:"\\b\\d+[kKmMgGdshdwy]*\\b",r:0},d]};
return{c:[f.HCM,{b:f.UIR+"\\s",e:";|{",rB:true,c:[f.inherit(f.UTM,{starts:e})],r:0}],i:"[^\\s\\}]"}
});
hljs.registerLanguage("json",function(h){var l={literal:"true false null"};
var m=[h.QSM,h.CNM];
var n={cN:"value",e:",",eW:true,eE:true,c:m,k:l};
var g={b:"{",e:"}",c:[{cN:"attribute",b:'\\s*"',e:'"\\s*:\\s*',eB:true,eE:true,c:[h.BE],i:"\\n",starts:n}],i:"\\S"};
var k={b:"\\[",e:"\\]",c:[h.inherit(n,{cN:null})],i:"\\S"};
m.splice(m.length,0,g,k);
return{c:m,k:l,i:"\\S"}
});
hljs.registerLanguage("apache",function(d){var c={cN:"number",b:"[\\$%]\\d+"};
return{cI:true,c:[d.HCM,{cN:"tag",b:"</?",e:">"},{cN:"keyword",b:/\w+/,r:0,k:{common:"order deny allow setenv rewriterule rewriteengine rewritecond documentroot sethandler errordocument loadmodule options header listen serverroot servername"},starts:{e:/$/,r:0,k:{literal:"on off all"},c:[{cN:"sqbracket",b:"\\s\\[",e:"\\]$"},{cN:"cbracket",b:"[\\$%]\\{",e:"\\}",c:["self",c]},c,d.QSM]}}],i:/\S/}
});
hljs.registerLanguage("cpp",function(d){var c={keyword:"false int float while private char catch export virtual operator sizeof dynamic_cast|10 typedef const_cast|10 const struct for static_cast|10 union namespace unsigned long throw volatile static protected bool template mutable if public friend do return goto auto void enum else break new extern using true class asm case typeid short reinterpret_cast|10 default double register explicit signed typename try this switch continue wchar_t inline delete alignof char16_t char32_t constexpr decltype noexcept nullptr static_assert thread_local restrict _Bool complex _Complex _Imaginary",built_in:"std string cin cout cerr clog stringstream istringstream ostringstream auto_ptr deque list queue stack vector map set bitset multiset multimap unordered_set unordered_map unordered_multiset unordered_multimap array shared_ptr abort abs acos asin atan2 atan calloc ceil cosh cos exit exp fabs floor fmod fprintf fputs free frexp fscanf isalnum isalpha iscntrl isdigit isgraph islower isprint ispunct isspace isupper isxdigit tolower toupper labs ldexp log10 log malloc memchr memcmp memcpy memset modf pow printf putchar puts scanf sinh sin snprintf sprintf sqrt sscanf strcat strchr strcmp strcpy strcspn strlen strncat strncmp strncpy strpbrk strrchr strspn strstr tanh tan vfprintf vprintf vsprintf"};
return{aliases:["c"],k:c,i:"</",c:[d.CLCM,d.CBLCLM,d.QSM,{cN:"string",b:"'\\\\?.",e:"'",i:"."},{cN:"number",b:"\\b(\\d+(\\.\\d*)?|\\.\\d+)(u|U|l|L|ul|UL|f|F)"},d.CNM,{cN:"preprocessor",b:"#",e:"$",c:[{b:"include\\s*<",e:">",i:"\\n"},d.CLCM]},{cN:"stl_container",b:"\\b(deque|list|queue|stack|vector|map|set|bitset|multiset|multimap|unordered_map|unordered_set|unordered_multiset|unordered_multimap|array)\\s*<",e:">",k:c,r:10,c:["self"]}]}
});
hljs.registerLanguage("makefile",function(d){var c={cN:"variable",b:/\$\(/,e:/\)/,c:[d.BE]};
return{c:[d.HCM,{b:/^\w+\s*\W*=/,rB:true,r:0,starts:{cN:"constant",e:/\s*\W*=/,eE:true,starts:{e:/$/,r:0,c:[c]}}},{cN:"title",b:/^[\w]+:\s*$/},{cN:"phony",b:/^\.PHONY:/,e:/$/,k:".PHONY",l:/[\.\w]+/},{b:/^\t+/,e:/$/,c:[d.QSM,c]}]}
});
sl=sl||{};
sl.components=sl.components||{};
sl.components.bannerSearch={init:function(){var d=function(g,f){var e=location.protocol+"//"+location.host;
e+=g;
e+=f;
return e
};
var b=function(g,e){g=g.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
var k=new RegExp("[\\?&]"+g+"=([^&#]*)");
var h;
if(e==null||e==""||e.length<=0){h=k.exec(location.search)
}if(e!=null&&e.length>1){var f=e.replace(new RegExp(";","g"),"&");
h=k.exec(f)
}return h===null?"":decodeURIComponent(h[1].replace(/\+/g," "))
};
var c=this,a=$(".slbs2-banner .search-container");
$.each(a,function(){var t=$(this).find("#communitySearch");
var r=$(this).find(".community-search-button");
var u=$(this).find("#communitySearchForm");
var w=$(this).attr("data-search-path");
var f=$(this).attr("data-search-endpoint");
var v=$(this).attr("data-group-filter");
var n=$(this).attr("data-ct-filter");
var m=$(this).attr("data-wild-char");
var s;
var k=location.search;
var h=b("q",k);
var e="";
if(typeof v!=="undefined"){var p=v;
var l="product-category";
e+="&x1="+l;
e+="&q1="+p
}if(typeof n!=="undefined"){var o=encodeURI(n.replace(/,/g,"|"));
var g="content-type";
e+="&x2="+g;
e+="&q2="+o
}t.val(decodeURIComponent(h));
r.on("click",function(x){x.preventDefault();
var q=t.val();
s="";
if(q){s="?q="+encodeURIComponent(q)+e;
document.location=d(w,s)
}});
u.on("submit",function(x){x.preventDefault();
var q=t.val();
s="";
if(q){s="?q="+encodeURIComponent(q)+e;
document.location=d(w,s)
}});
c.initAutoComplete(t,f,v,n,m)
})
},initAutoComplete:function(l,a,m,g,f){var m=m;
var b="";
if(f=="yes"){b="*;search=community"
}else{b=";search=community"
}if(m){var k=m;
var d="product-category";
b+=";x1="+d;
b+=";q1="+k
}if(typeof g!=="undefined"){var h=encodeURI(g.replace(/,/g,"|"));
var c="content-type";
b+=";x2="+c;
b+=";q2="+h
}b+=";";
var e={minInputLength:3,minInputDelay:50,formId:"communitySearchForm",submitOnSelect:true,containers:[{requestUrl:a+"?do=sayt&i=1",ajaxDataType:"script",containerType:"WSCS",appendToQuery:b,highlightWords:false,queryParam:"q"}]};
l.TargetComplete(e)
}};
sl=sl||{};
sl.components=sl.components||{};
sl.components.anchor={init:function(){if(location.hash.length>0&&$(location.hash).length>0){var a=Foundation.MediaQuery.atLeast("large")?0:-$(".slbs2-header").outerHeight();
if("scrollRestoration" in history){history.scrollRestoration="manual"
}if($(".lazy-loading").length>0){$(window).on("all-lazy-images-loaded",function(){window.scroll(0,$(location.hash).offset().top+a)
})
}else{window.scroll(0,$(location.hash).offset().top+a)
}}}};