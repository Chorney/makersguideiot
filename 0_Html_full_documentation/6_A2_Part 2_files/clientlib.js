sl=sl||{};
sl.components=sl.components||{};
sl.components.searchResults={init:function(){var N="Application Notes",ac="Blog Posts",K="Data Sheets",o="Data Shorts",b="Discussion Forums",u="Example Code",G="Getting Started",m="Knowledge Base Articles",I="User Guides",p="Reference Manuals",S="Quick Start Guides",d="Manuals",V="Projects",Q="Release Notes",J="Reference Designs",e="RoHS Environmental",ai="Schematic and Layout Files",c="Software",D="Training Videos",A="Webinars",F="White Papers";
var x=$(".no-results");
var ah=$(".search-results");
var C=ah.attr("data-search-endpoint");
if(!C){return
}var O=location.search;
var ab=f("q",O);
var ad=f("i",O);
var j=f("page",O);
var L=f("q1",O);
var w=f("x1",O);
var H=f("q2",O);
var v=f("x2",O);
var af=f("sort",O);
var ag=C+"?";
ag+=ab?"q="+ab+";":"";
ag+=ad?"i="+ad+";":"";
ag+=j?"page="+j+";":"";
ag+=L?"q1="+L+";":"";
ag+=w?"x1="+w+";":"";
ag+=H?"q2="+H+";":"";
ag+=v?"x2="+v+";":"";
ag+=af?"sort="+af+";":"";
ag+="search=community;";
if(ab){$.get(ag,function(i){aa(i)
}).done(function(){}).fail(function(i,ak,q){}).always(function(){})
}function aa(q){var i=R(q,"resultcount.total");
if(i<=0){x.removeClass("hidden");
return
}y(q);
W(q);
g(q);
Y(q);
t(q);
ah.removeClass("hidden")
}function y(ak){var q=R(ak,"general.total");
var i=ah.find(".header h2");
i.text(q+" Results")
}function W(aD){var au=ah.find(".result-list");
var ao=R(aD,"resultsets");
if(ao&&ao.length>0){var ay=R(ao[0],"results");
if(ay&&ay.length>0){for(var aA=0;
aA<ay.length;
aA++){var ar=ay[aA];
var ax=$($.parseHTML(T));
var aw=ax.find(".title a");
var az=ax.find(".description");
var aq=R(ar,"url");
aw.text(R(ar,"title"));
aw.attr("href",aq);
az.text(R(ar,"desc"));
if(aq&&aq.indexOf("/login/")>-1){ax.addClass("secure")
}var aB=R(ar,"content-type");
var ap=R(ar,"software-version");
var an=R(ar,"posted-by");
var ak=R(ar,"screen-name");
var al=R(ar,"profile-link");
var aC=R(ar,"createDate");
var am=R(ar,"product-line");
var q=R(ar,"duration");
var av=R(ar,"number-of-replies");
var at=B(ar);
switch(aB){case N:l(ax,aB);
z(ax,ap);
break;
case ac:l(ax,aB);
h(ax,an,ak,al);
s(ax,aC);
break;
case K:l(ax,aB);
z(ax,ap);
break;
case o:l(ax,aB);
z(ax,ap);
break;
case b:l(ax,aB);
h(ax,an,ak,al);
s(ax,aC);
Z(ax,av,at);
break;
case u:l(ax,aB);
z(ax,ap);
break;
case G:l(ax,aB);
break;
case m:l(ax,aB);
h(ax,an,ak,al);
break;
case d:l(ax,aB);
z(ax,ap);
break;
case I:l(ax,aB);
z(ax,ap);
break;
case p:l(ax,aB);
z(ax,ap);
break;
case S:l(ax,aB);
z(ax,ap);
break;
case V:l(ax,aB);
h(ax,an,ak,al);
s(ax,aC);
break;
case Q:l(ax,aB);
z(ax,ap);
break;
case J:l(ax,aB);
z(ax,ap);
break;
case e:l(ax,aB);
z(ax,ap);
break;
case ai:l(ax,aB);
z(ax,ap);
break;
case c:l(ax,aB);
z(ax,ap);
break;
case D:l(ax,aB);
break;
case A:l(ax,aB);
break;
case F:l(ax,aB);
break;
default:l(ax,aB);
break
}M(ax,aB);
au.append(ax)
}}}}function M(ak,al){var q=ak.find(".content-type-icon");
var i;
if(al){al=al.replace(/[^a-zA-Z ]/g,"");
al=al.replace(/ +(?= )/g,"");
i=al.toLowerCase();
i=i.split(" ").join("-");
i="icon-"+i;
q.addClass(i)
}}function h(q,al,ak,am){var i=q.find(".details");
var an=$($.parseHTML(ae));
if(am){an.find("a").attr("href",am)
}if(ak){an.find("a").text(ak);
i.append(an)
}}function s(ak,al){var q=ak.find(".details");
var an=$($.parseHTML(aj));
if(al){var i=new Date(al);
var am=X(i);
an.text(am);
q.append(an)
}}function l(q,al){var i=q.find(".details");
var ak=$($.parseHTML(r));
if(al){ak.text(al);
i.append(ak)
}}function z(ak,q){var i=ak.find(".details");
var al=$($.parseHTML(k));
if(q){al.text("Version: "+q);
i.append(al)
}}function Z(i,ak,am){var al=i.find(".qna-metadata-wrapper");
var q=$($.parseHTML(E));
if(ak){q.find(".bubble").text(ak)
}if(am){q.find(".scf-qna-answered").text("Answered");
q.find(".scf-qna-glyphicon").removeClass("glyphicon-remove-sign");
q.find(".scf-qna-glyphicon").addClass("glyphicon-ok-sign")
}al.append(q)
}function g(aG){var at=ah.find(".pagination"),aB=R(aG,"pagination"),aw=R(aB[0],"last"),aq=R(aG,"general.page_total"),ar,am,an=[];
if(aB&&aB.length>0){var ay=R(aB[0],"pages");
if(ay&&ay.length>0){for(var aA=0;
aA<ay.length;
aA++){var ap=ay[aA];
var av=R(ap,"selected");
var aF=$($.parseHTML(U));
aF.find("a").attr("href",R(ap,"link"));
aF.find("a").text(R(ap,"page"));
if(av===true||av==="true"){am=aA;
aF.find("a").addClass("active")
}if(ap.page==="1"||ap.page===1){ar=ap
}if(aq<=5){at.append(aF)
}else{if(aA<=4&&aq>5){an.push(aF)
}}}if(am<4&&aq>5){$.each(an,function(aH,i){at.append(i)
})
}if(aq>5&&am===4||am>4){var aC=[ay[am-1],ay[am],ay[am+1]];
$.each(aC,function(aI,aH){var i=$($.parseHTML(U));
if(aH!==undefined){i.find("a").attr("href",aH.link);
i.find("a").text(aH.page)
}if(aI===1){i.find("a").addClass("active")
}at.append(i)
});
var q=$($.parseHTML(U));
var ak=$($.parseHTML(P));
var au=R(ay[0],"link").replace(/(page=)[^\&]+;/,"page=1;");
if(au){q.find("a").attr("href",au);
q.find("a").text("1");
at.prepend(ak).prepend(q)
}}}if(aw&&aq>5&&(ay[am+1].link!==aw)){var ao=$($.parseHTML(U));
var aE=$($.parseHTML(P));
ao.find("a").attr("href",aw);
ao.find("a").text(aq);
at.append(aE);
at.append(ao)
}var al=R(aB[0],"previous");
if(al){var ax=ah.find(".pagination-wrapper .previous").attr("href",al);
ax.removeClass("hidden")
}var az=R(aB[0],"next");
if(az){var aD=ah.find(".pagination-wrapper .next").attr("href",az);
aD.removeClass("hidden")
}}}function Y(ar){var aw=ah.find(".content-type.filter");
var au=aw.find(".filter-label");
var ax=aw.find(".filter-dropdown");
var ak=aw.find(".dropdown-toggle");
var al=location.search;
var am=f("q",al)||"";
ax.find("li").first().find("a").attr("href","?q="+am+";");
var av=R(ar,"facets");
if(av&&av.length>0){for(var aq=0;
aq<av.length;
aq++){var an=av[aq];
var ay=R(an,"label");
var aA=R(an,"values");
if(ay==="Content Type"&&aA&&aA.length>0){var ap=false;
for(var ao=0;
ao<aA.length;
ao++){var az=aA[ao];
var aB=$($.parseHTML(a));
aB.find("a").text(R(az,"value"));
aB.find("a").attr("href",R(az,"link"));
var at=R(az,"selected");
if(at==="true"||at===true){ap=true;
aB.find("a").addClass("selected");
au.text(R(az,"value"))
}ax.append(aB)
}if(!ap){ax.find("li").first().find("a").addClass("selected")
}break
}}}ak.on("click",function(i){i.stopPropagation();
ax.toggleClass("hidden")
});
$(document).click(function(){if(!ax.hasClass("hidden")){ax.addClass("hidden")
}});
ax.find("li > a.selected").on("click",function(i){i.preventDefault()
})
}function t(ap){var an=R(ap,"menus");
var ar=ah.find(".sort-by");
if(an&&an.length>0){for(var aq=0;
aq<an.length;
aq++){var al=an[aq];
var ak=R(al,"name");
if(ak==="sort"){var at=R(al,"items");
for(var ao=0;
ao<at.length;
ao++){var av=at[ao];
var au=R(av,"label");
var aw=R(av,"path");
var am=R(av,"selected");
var q=$($.parseHTML(n));
if(au&&aw){q.attr("href",aw);
q.text(au);
if(am==="true"||am===true){q.addClass("active")
}ar.append(q)
}}break
}}}}function f(ak,i){ak=ak.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
var am=new RegExp("[\\?&]"+ak+"=([^&#]*)");
var al;
if(i==null||i==""||i.length<=0){al=am.exec(location.search)
}if(i!=null&&i.length>1){var q=i.replace(new RegExp(";","g"),"&");
al=am.exec(q)
}return al===null?"":decodeURIComponent(al[1].replace(/\+/g," "))
}function R(q,i){return i.split(".").reduce(function(al,ak){return(typeof al=="undefined"||al===null)?al:al[ak]
},q)
}function X(q){var i=q.getDate(),al=q.getMonth(),ak=q.getFullYear();
hours=q.getHours(),minutes=q.getMinutes(),period=(hours>=12)?"PM":"AM";
hours=hours%12;
hours=hours?hours:12;
minutes=("0"+minutes).slice(-2);
return(al+1)+"/"+i+"/"+ak+" | "+hours+":"+minutes+period
}function B(q){var an=false;
var am=R(q,"keys");
if(am){var ao=am.split(",");
for(var al=0;
al<ao.length;
al++){var ak=ao[al].trim();
if(ak==="Answered"){an=true;
break
}}}return an
}var T='<div class="row result-item">    <div class="col-xs-4 col-sm-2 section-left">        <div class="icon-wrapper"><div class="content-type-icon"></div></div>        <div class="details">        </div>    </div><div class="col-xs-8 col-sm-10 section-right">    <div class="row section-right-inner">        <div class="col-xs-12 col-sm-9">            <div class="title">               <a></a>            </div>            <div class="description">            </div>        </div>        <div class="col-xs-12 col-sm-3 qna-metadata-wrapper">        </div>        </div>    </div></div>';
var ae='<div class="author"><a href="#"></a></div>';
var aj='<div class="date"></div>';
var r='<div class="content-type"></div>';
var k='<div class="version"></div>';
var U='<li class="page-item"><a class="page-link" href="#"></a></li>';
var P='<li class="page-item"><span class="dots">...</span></li>';
var E='<div class="qna-metadata">    <div class="num-comments">        <div class="bubble">0</div>        <div class="text">Replies</div>    </div>    <div class="is-answered">        <span aria-hidden="true" class="glyphicon glyphicon-remove-sign scf-qna-glyphicon"></span>        <span class="scf-qna-answered">Unanswered</span>    </div></div>';
var a='<li><a href="#"></a></li>';
var n='<a class="btn" href="#">Popular</a>'
}};