(function(d,a,c,e,b){e(a).ready(function(){var k={};
var h=0;
var j=CQ.shared.HTTP.getContextPath();
var g=j+"/content/communities/templates/functions";
if(e("#scf-js-notificationsUnreadCount").length){var f=e("#scf-js-notificationsUnreadCount").data("siteurl");
if(f&&!b.Util.startsWith(f,g)){e.ajax({type:"GET",url:f+"/notifications/jcr:content/content/primary/notifications.social.0.0.json",async:true,data:k,success:function(l){h=l.unreadCount;
e("#scf-js-notificationsUnreadCount").text(h)
}})
}}e(a).on("socialNotificationMarkAsRead",function(m,l){if(l=="all"){h=0
}else{h=h-1
}e("#scf-js-notificationsUnreadCount").text(h)
})
})
})(window,document,Granite,Granite.$,SCF);
$CQ(document).ready(function(){if(!String.prototype.trim){String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")
}
}function a(o){if(o==="undefined"||o===null){return""
}o=o.split("+").join(" ");
var r={},q,p=/[?&]?([^=]+)=([^&]*)/g;
q=p.exec(o);
while(q!==null){r[decodeURIComponent(q[1]).trim()]=decodeURIComponent(q[2]);
q=p.exec(o)
}return r
}function m(q){if(q==="undefined"||q===null){return""
}var o=[];
var s=/('.*?'|".*?"|\S+)/g;
for(var r=0;
r<q.length;
r++){var u=q[r].trim().match(s);
if(u.length>=3){var t=u[2].trim();
if(t.charAt(0)=="'"&&t.charAt(t.length-1)=="'"){t=t.substring(1,t.length-1)
}var p=u[0].trim();
o[p]=t
}}return o
}function b(o){if(!_.isEmpty(o)){return o.trim().split(",")
}else{return""
}}var d=a(document.location.search);
var n=b(d.filter);
var l=m(n);
var f;
if(Object.keys){f=(typeof l==="undefined"||l===null)?{}:Object.keys(l)
}else{f=$CQ().map(function(p,o){return o
})
}if(d!==null&&d!==undefined){var g=d.expLanguage;
if(g!==null&&g!==undefined){$CQ(".form-language-select").val(g)
}}$CQ("input.scf-js-searchresult-value").val(function(){return(typeof l==="undefined"||l===null)?"":l[f[0]]
});
$CQ("input.scf-js-searchresult-jcrtitle").prop("checked",function(){if(typeof l==="undefined"||l===null){return false
}else{return(l["jcr:title"]!==null&&l["jcr:title"]!==undefined)?true:false
}});
$CQ("input.scf-js-searchresult-jcrdescription").prop("checked",function(){if(typeof l==="undefined"||l===null){return false
}else{return(l["jcr:description"]!==null&&l["jcr:description"]!==undefined)?true:false
}});
$CQ("input.scf-js-searchresult-tag").prop("checked",function(){if(typeof l==="undefined"||l===null){return false
}else{return(l.tag!==null&&l.tag!==undefined)?true:false
}});
$CQ("input.scf-js-searchresult-userIdentifier").prop("checked",function(){if(typeof l==="undefined"||l===null){return false
}else{return(l.userIdentifier!==null&&l.userIdentifier!==undefined)?true:false
}});
if(d!==null&&d!==undefined){if(d.searchText!==null&&d.searchText!==undefined){$CQ("input.scf-js-searchresult-value").val(d.searchText)
}}$CQ("form.scf-js-searchresultform").submit(function(p){p.preventDefault();
var v=""+$CQ("input.scf-js-searchresult-value").val();
var u=true;
var w=true;
var y=true;
var s=true;
var t="default";
var q="";
v=v.trim();
v=v.replace(/,/gi,"\\,");
if((v&&v.length>0)&&(u||w||y||s)){if(u){q="filter=jcr:title like '"+v+"'"
}if(w){q+=(q.length>0)?",":"filter=";
q+="jcr:description like '"+v+"'"
}if(y){q+=(q.length>0)?", ":"filter=";
q+="tag like '"+v+"'"
}if(s){q+=(q.length>0)?", ":"filter=";
q+="author_display_name like '"+v+"'"
}if(t){q+="&expLanguage="+t;
q+="&searchText="+v
}else{q+="&expLanguage=default";
q+="&searchText="+v
}}var o=document.URL.substring(0,document.URL.indexOf("?"))+"?"+q;
var x=$CQ(this).data("paths");
var r;
if(typeof(x)!="undefined"&&x!==null&&x.length>0){x=x.substring(1,x.length-1);
r=x.split(",")
}if(typeof(r)!="undefined"){for(i=0;
i<r.length;
i++){o+="&path="+r[i].trim()
}}o+="&_charset_=utf-8";
if(q.length>0){window.location.replace(o)
}});
var j="more";
var h="less";
var k=100;
var c="...";
$CQ(".scf-js-searchresult-comment").each(function(){var q=$CQ(this).html();
if(q.length>k){var r=q.substr(0,k);
var p=q.substr(k-1,q.length-k);
var o=r+'<span class="moreelipses">'+c+'</span>&nbsp;<span class="scf-searchresult-morecontent"><span>'+p+'</span>&nbsp;&nbsp;<a href="" class="scf-searchresult-morelink">'+j+"</a></span>";
$CQ(this).html(o)
}});
$CQ(".scf-searchresult-morelink").click(function(){if($CQ(this).hasClass("less")){$CQ(this).removeClass("less");
$CQ(this).html(j)
}else{$CQ(this).addClass("less");
$CQ(this).html(h)
}$CQ(this).parent().prev().toggle();
$CQ(this).prev().toggle();
return false
});
function e(o,p){var q=o.length-p.length;
return q>=0&&o.lastIndexOf(p)===q
}$CQ("a.scf-js-searchresult-detaillink").each(function(){var p=$CQ(this).attr("href");
var o="/_jcr_content";
var s=p.indexOf(o);
if(s===-1){return
}else{var t=p.indexOf(".html");
if(t>s){var q=p.substring(0,s);
var r=p.substring(t);
q+=r;
$CQ(this).attr("href",q)
}}});
$CQ(".scf-search-results a.scf-page").click(function(q){q.preventDefault();
var u=$(q.currentTarget).data("pageSuffix");
u=u.toString();
var s=u.split(".");
var w=window.location.href;
var r=$(q.currentTarget).data("baseUrl");
var p=w.substring(0,w.indexOf(r));
var t=w.indexOf(".html")+5;
var v=w.substring(t);
var o=p+r+"."+s[0]+"."+s[1]+".html"+v;
window.location.href=o
})
});
$CQ(document).ready(function(){$CQ("form.scf-js-searchform").submit(function(d){d.preventDefault();
var f=$(d.target);
var o=""+f.find("input.scf-js-search-value").val();
var n=f.find("input.scf-js-search-jcrtitle").is(":checked");
var p=f.find("input.scf-js-search-jcrdescription").is(":checked");
var s=f.find("input.scf-js-search-tag").is(":checked");
var l=f.find("input.scf-js-search-userIdentifier").is(":checked");
var m=f.find(".form-language-select").val();
var k;
o=o.trim();
o=o.replace(/,/gi,"\\,");
var e="";
if((o&&o.length>0)&&(n||p||s||l)){if(n){e="filter=jcr:title like '"+o+"'"
}if(p){e+=(e.length>0)?",":"filter=";
var q=$CQ("<div/>").text(o).html();
e+="jcr:description like '"+encodeURIComponent(q)+"'"
}if(s){e+=(e.length>0)?", ":"filter=";
e+="tag like '"+o+"'"
}if(l){e+=(e.length>0)?", ":"filter=";
e+="author_display_name like '"+o+"'"
}if(m){e+="&expLanguage="+m;
e+="&searchText="+encodeURIComponent(o)
}else{e+="&expLanguage=default";
e+="&searchText="+encodeURIComponent(o)
}}var j=f.find(".scf-js-seach-resultPage").val();
var r=f.data("paths");
var h;
if(typeof(r)!=="undefined"&&r!==null&&r.length>0){r=r.substring(1,r.length-1);
h=r.split(",")
}if(e.length>0&&typeof(j)!="undefined"&&j.length>0){if(typeof(h)!="undefined"){for(i=0;
i<h.length;
i++){e+="&path="+h[i].trim()
}}var g=CQ.shared.HTTP.getContextPath();
e+="&_charset_=utf-8";
var c=(g!==null&&g.length>0)?g+"/"+j+".html?"+e:j+".html?"+e;
window.location.replace(c)
}});
function b(e,d){for(var c in d){if(d[c]==e){return c
}}}function a(){var g=location.search.substr(1);
var f=g.split("&");
var c={};
for(var d=0;
d<f.length;
d++){var e=f[d].split("=");
c[e[0]]=e[1]
}return c
}});