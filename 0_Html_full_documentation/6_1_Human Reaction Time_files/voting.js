(function(b){window.CQ_Analytics=window.CQ_Analytics||{};
var a=window.CQ_Analytics;
var d=b.Model.extend({ANALYTICS_BASE_RESOURCE_TYPE:"social/commons/components/analyticsbase",modelName:"VotingModel",defaults:{totalNumberOfResponses:0,netCount:0,positiveCount:0,negativeCount:0},_updateVoting:function(g){var h=this.get("currentUserResponse");
var m=this.get("netCount");
var j=this.get("positiveCount");
var k=this.get("negativeCount");
var f=this.get("totalNumberOfResponses");
var l=this.get("responseTallies")||{};
var i;
var e;
if(g==="unset"){if(h==="DISLIKE"){m=m+1;
k--
}else{if(h==="LIKE"){m=m-1;
j--
}}f--;
i=false;
e=undefined
}else{if(g==-1){if(h==="LIKE"){m=m-2;
k++;
j--
}else{m=m-1;
k++;
f++
}e="DISLIKE";
i=false
}else{if(g==1){if(h==="DISLIKE"){m=m+2;
j++;
k--
}else{m=m+1;
j++;
f++
}e="LIKE";
i=true
}}}l[1]=j;
l[-1]=k;
this.set({currentUserResponse:e,currentUserLike:i,netCount:m,positiveCount:j,negativeCount:k,totalNumberOfResponses:f,responseTallies:l},{silent:true});
if(e===undefined){this.unset("currentUserResponse",{silent:true})
}},createVoting:function(i){var g=b.config.urlRoot+this.get("id")+b.constants.URL_EXT;
var j=_.bind(function(k){this._updateVoting(i);
this.trigger("voting:added",{model:this});
var l=$(".scf-js-site-title").attr("href");
l=_.isUndefined(l)?"":l.substring(0,l.lastIndexOf(".html"));
this.sitePath=l;
var p=(this.attributes.properties&&this.attributes.properties["social:parentid"])?this.attributes.properties["social:parentid"]:this.id.split("/voting")[0];
var o=b.Model.findLocal(p);
if(k.response.currentUserResponse){var m=k.response.currentUserResponse;
var n;
if(a.Sitecatalyst){n=m==="LIKE"?"SCFVoteUp":"SCFVoteDown";
a.record({event:n,values:{functionType:b.Context.communityFunction?b.Context.communityFunction:o.view.COMMUNITY_FUNCTION,path:b.Context.path?b.Context.path:p,type:b.Context.type?b.Context.type:o.get("resourceType"),ugcTitle:b.Context.ugcTitle?b.Context.ugcTitle:"",siteTitle:b.Context.siteTitle?b.Context.siteTitle:$(".scf-js-site-title").text(),sitePath:b.Context.sitePath?b.Context.sitePath:this.sitePath,groupTitle:b.Context.groupTitle,groupPath:b.Context.groupPath,user:b.Context.user?b.Context.user:b.Session.get("authorizableId")},collect:false,componentPath:b.constants.ANALYTICS_BASE_RESOURCE_TYPE})
}}},this);
var f=_.bind(function(m,l,k){console.log("error creating vote "+k);
this.trigger("voting:adderror",{error:k})
},this);
var e={response:i,tallyType:"Voting",":operation":"social:postTallyResponse"};
var h=this.get("properties");
if(h&&typeof h.useReferrer!="undefined"&&h.useReferrer){e.referer=window.location.href
}$.ajax(g,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:e,success:j,error:f})
}});
var c=b.View.extend({viewName:"Voting",tagName:"div",className:"voting",init:function(){this.listenTo(this.model,"voting:added",this.update);
this.listenTo(this.model,"voting:adderror",this.showError)
},update:function(){this.isVoteInProgress=false;
this.render()
},showError:function(e){console.log(e)
},vote:function(h){if(this.isVoteInProgress){return
}var f=$CQ(h.target);
f.addClass("scf-is-disabled");
var g=f.attr("data-voting-value");
this.isVoteInProgress=true;
this.model.createVoting(g);
h.preventDefault();
return false
}});
b.Voting=d;
b.VotingView=c;
b.registerComponent("social/tally/components/hbs/voting",b.Voting,b.VotingView)
})(SCF);