$CQ(document).ready(function(){var e=function(g){var f=$(g);
f.autocomplete({source:function(j,h){$.get(a("*"+$CQ(g).val()+"*"),function(l){var k=l.items;
h(k)
})
},minLength:3,appendTo:".scf-quicksearch-form-group",position:{my:"right top",at:"right bottom"},focus:function(h,j){h.preventDefault();
$(this).val(d(j.item))
},change:function(h,j){},select:function(j,k){var h=(j.keyCode?j.keyCode:j.which);
if(h===13){window.location.replace(k.item.friendlyUrl)
}},setvalue:function(h){}}).data("uiAutocomplete")._renderItem=function(h,j){return $CQ("<li class='scf-quicksearch-item-container'></li>").append("<a href='"+j.friendlyUrl+"'><span class='glyphicon scf-icon-"+c(j.resourceType)+"' aria-hidden='true'></span><span class='scf-quicksearch-item'>"+d(j)+"</span><span class='small scf-quicksearch-item-url'>"+j.friendlyUrl+"</span></a>").data("item.autocomplete",j).appendTo(h)
};
$("#scf-js-quicksearch-input-inline").keypress(function(o){var h=(o.keyCode?o.keyCode:o.which);
if(h===13){o.preventDefault();
var n=$("#form-search-input-inline");
var l=n.find(".scf-js-seach-resultPage").val();
var m=b($CQ(this).val(),true);
var k=CQ.shared.HTTP.getContextPath();
var j=(k!==null&&k.length>0)?k+"/"+l+".html?"+m:l+".html?"+m;
window.location.replace(j)
}})
};
var d=function(f){var g=(f.subject&&f.subject.length>0)?f.subject:(f.name&&f.name.length>0)?f.name:f.resourceType.split("/")[f.resourceType.split("/").length-1];
return g
};
var c=function(f){return f.replace(/\//g,"-")
};
var a=function(g){var f=$CQ(".scf-js-search-endpoint").val()+".social.json?"+b(g);
return f
};
var b=function(j,f){var h="";
var l=$CQ(".scf-js-searchform").data("paths");
var k;
if(typeof(l)!=="undefined"&&l!==null&&l.length>0){l=l.substring(1,l.length-1);
k=l.split(",")
}j=j.trim();
j=j.replace(/,/gi,"\\,");
if(j&&j.length>0){h="filter=jcr:title like '"+j+"'";
h+=(h.length>0)?", ":"filter=";
h+="author_display_name like '"+j+"'";
if(f){h+=", jcr:description like '"+j+"'";
h+=", tag like '"+j+"'"
}h+="&expLanguage=default";
h+="&searchText="+encodeURIComponent(j)
}if(typeof(k)!="undefined"){for(i=0;
i<k.length;
i++){h+="&path="+k[i].trim()
}}var g=CQ.shared.HTTP.getContextPath();
g=(g!==null&&g.length>0)?g:CQ.shared.HTTP.getPath();
h+="&_charset_=utf-8";
return h
};
if($CQ("#scf-js-quicksearch-input-inline").length!==0){e($CQ("#scf-js-quicksearch-input-inline"))
}});