(function(c,b){c.soco=c.soco||{};
c.soco.commons=c.soco.commons||{};
c.soco.TEMPLATE_PARAMNAME=":templatename";
c.soco.filterHTMLFragment=c.soco.filterHTMLFragment||function(d,f){try{f.call(null,b(d))
}catch(g){throw g
}};
var a={};
a.CLEAR="lcl.cq.soco.events.clear";
c.soco.commons.handleOnBlur=function(d,e){if((b(d).val()==="")||(b(d).val()==="<br/>")){b(d).val(e)
}};
c.soco.commons.handleOnFocus=function(d,e){if(b(d).val()===e){b(d).val("")
}};
c.soco.commons.getMessage=function(d){var f=b(d).first().val();
if(typeof CKEDITOR!=="undefined"){var e=CKEDITOR.instances[b(d).attr("id")];
if(e){f=e.getData()
}}return f
};
c.soco.commons.validateFieldNotEmptyOrDefaultMessage=function(f,e){c.soco.commons.syncRTE(f);
var d=b(f).val();
if(!e){e=""
}var g="tempRTEValidate_"+Math.floor(Math.random()*1001);
b(f).after("<div id='"+g+"' height='0px' style='visibility:hidden;' width='0px'></div>");
d=c.soco.commons.stripNonText(d);
e=c.soco.commons.stripNonText(e);
b("#"+g).append(d);
b("#"+g+" div:empty").filter(function(){return b(this)
}).remove();
b("#"+g+" p:empty").filter(function(){return b(this)
}).remove();
d=b("#"+g).html();
b("#"+g).remove();
if(b.trim(d).length===0||b.trim(d)===e){alert(c.I18n.getMessage("Comment field cannot be empty or default message."));
return false
}else{return true
}};
c.soco.commons.stripNonText=function(d){d=d.replace(/\s|&nbsp;/g,"");
d=d.replace(/\r?\n|\r/g,"");
d=d.replace(/(<|&lt;)br\s*\/*(>|&gt;)/g,"");
return d
};
c.soco.commons.syncRTE=function(d){if(typeof CKEDITOR!=="undefined"){var e=CKEDITOR.instances[b(d).attr("name")];
if(!e){e=CKEDITOR.instances[b(d).attr("id")]
}if(e&&e.checkDirty()){e.updateElement();
e.resetDirty()
}}};
c.soco.commons.clientSideComposer=function(i,k,j,e,h,g,f){var l=g||i.attr("action"),d=f||i.attr("method")||"POST";
i.find(":submit").click(function(s){var o=$.map(i.find(":input[type='file']"),function(v){var u=$(v);
if(u.val()===""||u.val()===undefined){return null
}return true
}).length>0;
if(o){return
}s.preventDefault();
var r=b(s.target).closest("form")[0],t;
if(b.isFunction(r.onsubmit)){if(!r.onsubmit.call(r,s)){return
}}var q=i.find("textarea");
c.soco.commons.syncRTE(q);
t=b(r).serialize();
if(k){t+="&"+encodeURIComponent(c.soco.TEMPLATE_PARAMNAME)+"="+encodeURIComponent(k)
}for(var p in h){t+="&"+encodeURIComponent(p)+"="+encodeURIComponent(h[p])
}b(r).find(":input:visible").each(function(){b(this).attr("disabled","disabled")
});
var n=function(v,u,w){if((w.status===201)||(w.status===200)){b(r).find(":input:visible").each(function(){switch(this.type){case"password":case"select-multiple":case"select-one":case"text":case"textarea":b(this).val("");
break;
case"checkbox":case"radio":this.checked=false
}b(this).removeAttr("disabled")
});
b(r).find(":input:hidden").each(function(){b(this).trigger(a.CLEAR)
});
j.call(null,v,u,w)
}else{b(r).find(":input:visible").each(function(){b(this).removeAttr("disabled")
});
e.call(null,w,u)
}};
var m=function(v,u){b(r).find(":input:visible").each(function(){b(this).removeAttr("disabled")
});
e.call(null,v,u)
};
b.ajax(l,{data:t,success:n,fail:m,type:d})
})
};
c.soco.commons.fillInputFromClientContext=function(d,e){if(window.CQ_Analytics&&CQ_Analytics.CCM){b(function(){var f=CQ_Analytics.CCM.getRegisteredStore(CQ_Analytics.ProfileDataMgr.STORENAME);
if(f){var g=f.getProperty(e,true)||"";
d.val(g)
}CQ_Analytics.CCM.addListener("storesloaded",function(){var h=CQ_Analytics.CCM.getRegisteredStore(CQ_Analytics.ProfileDataMgr.STORENAME);
if(h&&h.addListener){var i=h.getProperty(e,true)||"";
d.val(i);
h.addListener("update",function(){var j=h.getProperty(e,true)||"";
d.val(j)
})
}})
})
}};
c.soco.commons.activateRTE=function(f,d){var e=f.find("textarea");
c.soco.commons.convertTextAreaToRTE(e,d,true)
};
c.soco.commons.convertTextAreaToRTE=function(d,g,j){var e=d.width(),q=d.height(),p=[{name:"basicstyles",items:["Bold","Italic","Underline"]}],n={},m=d[0],o,k;
var h=g||["onfocus","onblur"];
if(m!==null&&typeof m!=="undefined"){for(k=0;
k<h.length;
k++){o=h[k];
if(null!==m[o]){n[o.substring(2)]=m[o]
}}}o=null;
b(d).height(d.height()+60);
var f={width:e,height:q,toolbar:p};
if(!j){f.width=d.width()===0?"100%":e;
f.height=d.height()===0?"100%":q;
f.toolbarLocation="bottom";
f.resize_enabled=false;
f.removePlugins="elementspath"
}else{f.width=e+4;
f.height=q+60
}var l=CKEDITOR.replace(b(d).attr("name"),f);
l.on("key",function(i){c.soco.commons.syncRTE(d)
});
d.on(a.CLEAR,function(i){b(m).val("");
l.setData("",function(){b(l).blur();
l.resetDirty()
})
});
return l
};
c.soco.commons.openModeration=function(){var d="";
if(c.WCM!==undefined&&c.WCM.getPagePath!==undefined){d=c.WCM.getPagePath()
}else{if(Granite.author!==undefined&&Granite.author.page!==undefined&&Granite.author.page.path!==undefined){d=Granite.author.page.path
}}c.shared.Util.open(c.shared.HTTP.externalize("/communities/moderation.html"))
};
c.soco.commons.showUGCFormAsDialog=function(h,f){var g=b(f);
var d=g.attr("id");
var e="modalIframeParent"+Math.random().toString(36).substring(2,4);
if(!d){g.attr("id",e);
d=e
}g.dialog({modal:true,height:500,width:750,buttons:{Submit:function(){var i=b("iframe.modalIframeClass",g).contents().find("form");
i.submit();
b(this).dialog("close")
},Cancel:function(){b(this).dialog("close")
}}});
g.html("<iframe class='modalIframeClass' width='100%' height='100%' marginWidth='0' marginHeight='0' frameBorder='0' />").dialog("open");
b("#"+d+" .modalIframeClass").attr("src",h);
return false
}
})(CQ,$CQ);
var CQ_collab_comments_loadedForms={};
var CQ_collab_comments_defaultMessage="";
var CQ_collab_comments_requireLogin=false;
var CQ_collab_comments_enterComment="Please enter a comment";
function CQ_collab_comments_toggleForm(c,h,a){var d=document.getElementById(h);
var b=document.getElementById(c);
try{a=CQ.shared.HTTP.noCaching(a);
CQ.shared.HTTP.get(a,function(p,k,j){var e=j.responseText;
$CQ(d).html(e);
var n=$CQ(d).children("form").attr("id");
if(n){var m=n.split("-");
m.length=m.length-1;
var l=m.join("-");
if(CQ_Analytics&&CQ_Analytics.CCM){var i=CQ_Analytics.CCM.getRegisteredStore(CQ_Analytics.ProfileDataMgr.STORENAME);
if(i){CQ_collab_comments_formStateChanged(l,i)
}}}})
}catch(g){alert("Error loading form: "+a)
}var f=d.style.display!="block";
d.style.display=f?"block":"none";
b.innerHTML=f?"Cancel":"Reply"
}function CQ_collab_comments_handleOnFocus(a,b){if(a.value==CQ_collab_comments_getDefaultMessage(b)){a.value=""
}a.style.color="#333"
}function CQ_collab_comments_handleOnBlur(a,b){if(a.value===""){a.value=CQ_collab_comments_getDefaultMessage(b);
a.style.color="#888"
}else{a.style.color="#333"
}}function CQ_collab_comments_validateFields(b){var a=document.getElementById(b+"-text");
if(a.value===""||a.value===CQ_collab_comments_getDefaultMessage(b)){CQ_collab_comments_showError(CQ_collab_comments_enterComment,b);
return false
}return true
}function CQ_collab_comments_validateSubmit(d){if(!CQ_collab_comments_validateFields(d)){return false
}try{var a=document.getElementById(d+"-id");
if(!a){var b=document.getElementById(d+"-form");
a=document.createElement("input");
a.id=d+"-id";
a.type="hidden";
a.name="id";
a.value="nobot";
b.appendChild(a)
}}catch(c){return false
}return true
}function CQ_collab_comments_showError(b,c){var a=document.getElementById(c+"-error");
if(!a){alert(b)
}else{a.innerHTML=b
}}function CQ_collab_comments_getDefaultMessage(a){if(a&&document.getElementById(a+"-rating")){return CQ_collab_ratings_defaultMessage
}return CQ_collab_comments_defaultMessage
}function CQ_collab_comments_openCollabAdmin(){CQ.shared.Util.open(CQ.shared.HTTP.externalize("/socoadmin.html#/content/usergenerated"+CQ.WCM.getPagePath()))
}function CQ_collab_comments_activate(a,b){if(!a){a="Activate"
}CQ.HTTP.post("/bin/replicate.json",function(d,e,c){if(a==="Delete"){CQ.Notification.notify(null,e?CQ.I18n.getMessage("Comment deleted"):CQ.I18n.getMessage("Unable to delete comment"))
}else{CQ.Notification.notify(null,e?CQ.I18n.getMessage("Comment activated"):CQ.I18n.getMessage("Unable to activate comment"))
}if(b){b.call(this,d,e,c)
}},{_charset_:"utf-8",path:this.path,cmd:a})
}function CQ_collab_comments_refresh(){if(this.refreshCommentSystem){this.refreshCommentSystem()
}else{CQ.wcm.EditBase.refreshPage()
}}function CQ_collab_comments_afterEdit(a){CQ_collab_comments_activate.call(a,"Activate",CQ_collab_comments_refresh)
}function CQ_collab_comments_afterDelete(a){CQ_collab_comments_activate.call(a,"Delete",CQ_collab_comments_refresh)
}function CQ_collab_comments_initFormState(a){if(CQ_Analytics&&CQ_Analytics.CCM){$CQ(function(){CQ_Analytics.ClientContextUtils.onStoreRegistered(CQ_Analytics.ProfileDataMgr.STORENAME,function(b){CQ_collab_comments_formStateChanged(a,b);
b.addListener("update",function(){CQ_collab_comments_formStateChanged(a,b)
})
})
})
}}function CQ_collab_comments_formStateChanged(a,g){var c=g.getData();
if(c){var h=a+"-form";
var j=a+"-text";
var b=a+"-userIdentifier";
var e=a+"-email";
var i=a+"-url";
var f=c.authorizableId;
var d=c.formattedName;
if(!d){d=f
}if(f&&f=="anonymous"){if(CQ_collab_comments_requireLogin){$CQ("#"+h).hide();
$CQ("[id$=-reply-button]").hide();
$CQ("[id$=-reply-arrow]").hide()
}else{$CQ("#"+h).show();
$CQ("[id$=-reply-button]").show();
$CQ("[id$=-reply-arrow]").show();
$CQ("#"+b).attr("value","");
$CQ("#"+b+"-comment-block").show();
$CQ("#"+e).attr("value","");
$CQ("#"+e+"-comment-block").show();
$CQ("#"+i).attr("value","");
$CQ("#"+i+"-comment-block").show();
$CQ("[id$=-signed-in-text]").hide();
$CQ("[id$=-signed-in-user]").text("")
}}else{$CQ("[id$=-reply-button]").show();
$CQ("[id$=-reply-arrow]").show();
$CQ("#"+b+"-comment-block").hide();
$CQ("#"+b).attr("value",f);
$CQ("#"+e+"-comment-block").hide();
$CQ("#"+i+"-comment-block").hide();
$CQ("[id$=-signed-in-user]").text(d);
$CQ("[id$=-signed-in-text]").show()
}}}(function(d){window.CQ_Analytics=window.CQ_Analytics||{};
var b=window.CQ_Analytics;
var c=d.Model.extend({modelName:"SubscriptionModel",events:{UPDATED:"subscribe:update",UPDATE_ERROR:"subscribe:updateError"},doToggle:function(g){var i=_.bind(function(l){var m=l.response;
this.set("states",m.states);
this.set("subscribed",m.subscribed);
this.trigger(this.events.UPDATED,{model:this});
var k=this.get("states");
var n=this.get("subscribed");
if(g==="following"&&n&&k.following.selected){if(b.Sitecatalyst){b.record({event:"SCFFollow",values:{functionType:d.Context.communityFunction,path:d.Context.path,type:d.Context.type,ugcTitle:d.Context.ugcTitle,siteTitle:d.Context.siteTitle,sitePath:d.Context.sitePath,groupTitle:d.Context.groupTitle,groupPath:d.Context.groupPath,user:d.Context.user},collect:false,componentPath:d.constants.ANALYTICS_BASE_RESOURCE_TYPE})
}}},this);
var f=_.bind(function(m,l,k){d.log.error("error toggle follow state %o",k)
},this);
var e=this.get("url");
var j=[];
var h=[];
$CQ.each(this.get("states"),function(k,m){var l=m.selected;
if(g=="unfollow-all"){l=false
}else{if(k==g){l=!l
}}j.push(k);
h.push(l.toString())
});
$CQ.ajax(e,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:{":operation":"social:updatesubscriptions",types:j,states:h,subscribedId:this.get("subscribedId")},success:i,error:f})
}});
var a=d.View.extend({viewName:"SubscriptionView",init:function(){this.listenTo(this.model,this.model.events.UPDATED,this.update);
this.listenTo(this.model,this.model.events.UPDATE_ERROR,this.showError)
},handleClick:function(g){var e=g.currentTarget;
var f=e.getAttribute("data_type");
this.model.doToggle(f)
},update:function(){this.render();
$("li[data_type='following']").hide()
}});
d.Subscription=c;
d.SubscriptionView=a;
d.registerComponent("social/subscriptions/components/hbs/subscriptions",d.Subscription,d.SubscriptionView)
})(SCF);
(function(l,j,i,h){window.CQ_Analytics=window.CQ_Analytics||{};
var b=CQ_Analytics;
var f=h.Model.extend({modelName:"CommentSystemModel",relationships:{items:{collection:"CommentList",model:"CommentModel"}},createOperation:"social:createComment",MOVE_OPERATION:"social:moveComment",getCustomProperties:function(m,n){return j.extend(m,n)
},events:{MOVED:"comment:moved",MOVE_ERROR:"comment:moveError",ADD:"comment:added",ADD_ERROR:"comment:adderror"},_fixCachedProperties:function(){if(h.hasOwnProperty("Session")&&(h.Session!==null)){var m=h.Session.checkIfUserCanPost(this.attributes.id);
this.attributes.mayPost=m;
this.fixCachedProperties();
this.cacheFixed=true;
this.trigger("model:cacheFixed",this)
}},fixCachedProperties:function(){},constructor:function(m,n){h.Model.prototype.constructor.apply(this,[m,n]);
if(h.Session.isReady()){this._fixCachedProperties()
}else{h.Session.on("model:loaded",j.bind(this._fixCachedProperties,this))
}},shouldCommentBeAddedToList:function(m){return true
},addCommentSuccess:function(o){var t=o.response;
var n=h.Models[this.constructor.prototype.relationships.items.model];
var r=new n(t);
if(this.shouldCommentBeAddedToList(r)){r.set("_isNew",true);
r._isReady=true;
var s=this.get("items");
var q=false;
if(!s){var p=h.Collections[this.constructor.prototype.relationships.items.collection]||i.Collection;
s=new p();
s.model=n;
s.parent=this;
q=true
}s.unshift(r);
var m=this.get("totalSize");
if(q){this.set("items",s)
}this.set("totalSize",m+1);
r.constructor.prototype._cachedModels[t.id]=r;
this.trigger(this.events.ADD,{model:this,newItem:r});
h.Util.announce(this.events.ADD,r.attributes)
}},addComment:function(q,o,t){l(".scf-attachment-error").remove();
var s=j.bind(this.addCommentSuccess,this);
var p=j.bind(function(x,w,u){if(500==x.status){var v=l(".scf-composer-block")[0];
if(null===v){v=l(document.body)
}l('<div class="scf-attachment-error"><h3 class="scf-js-error-message">Server error. Please try again.</h3><div>').appendTo(v);
return false
}this.trigger(this.events.ADD_ERROR,this.parseServerError(x,w,u))
},this);
var n;
var m=(typeof q.files!=="undefined");
var r=(typeof q.tags!=="undefined");
if(m){if(window.FormData){n=new FormData()
}if(n){l.each(q.files,function(u,v){n.append("file",v)
});
n.append("id","nobot");
n.append(":operation",this.createOperation);
delete q.files;
if(r){l.each(q.tags,function(u,v){n.append("tags",v)
})
}delete q.tags;
l.each(q,function(u,v){n.append(u,v)
})
}}else{n={id:"nobot",":operation":this.createOperation};
j.extend(n,q);
n=this.getCustomProperties(n,q)
}l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",processData:!m,contentType:(m)?false:"application/x-www-form-urlencoded; charset=UTF-8",xhrFields:{withCredentials:true},data:this.addEncoding(n),success:s,error:p})
},addIncludeHint:function(m){j.extend(m,{"scf:included":this.get("pageInfo").includedPath,"scf:resourceType":this.get("resourceType")})
},move:function(n){var m=j.bind(function(r,q,p){this.trigger(this.events.MOVE_ERROR,{error:p})
},this);
var o=j.bind(this.addCommentSuccess,this);
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:{":operation":this.MOVE_OPERATION,resourcePath:n.resourcePath,parentPath:n.parentPath},success:o,error:m})
},translateAll:function(s){h.CommentSystem.thisRef=this;
if(this.get("showingTranslationAll")===undefined){this.set("showingTranslationAll",false)
}var n=this.get("items");
var r=[];
r.push(this);
var p;
for(p in n.models){r.push(n.models[p])
}var o=0;
var m=[];
var q=function(){o--;
if(o<=0){h.CommentSystem.thisRef.set("translateAllInProgress",false);
if(h.CommentSystem.thisRef.get("showingTranslationAll")===true){h.CommentSystem.thisRef.set("showingTranslationAll",false)
}else{h.CommentSystem.thisRef.set("showingTranslationAll",true)
}if(h.CommentSystem.thisRef.view){h.CommentSystem.thisRef.view.render()
}}};
if(this.get("showingTranslationAll")===false){for(p in r){if(r[p].get("canTranslate")&&!r[p].get("showingTranslation")){o++;
m.push(r[p])
}}if(o>0){this.set("translateAllInProgress",true);
if(this.view){this.view.render()
}}if(o===0){this.set("showingTranslationAll",true);
if(this.view){this.view.render()
}return
}}else{for(p in r){if(r[p].get("canTranslate")&&r[p].get("showingTranslation")){o++;
m.push(r[p])
}}if(o===0){this.set("showingTranslationAll",false);
if(this.view){this.view.render()
}return
}}for(p in m){m[p].translate(q)
}},refetchUsingSort:function(n,q){var m,p=this.get("pageInfo"),o=this.get("properties"),s=["views","posts","follows","likes"],r=j.intersection(o.sortBy,s);
if(!q){q="DESC"
}n=(n==="newest")?"added":n;
if(r.length>0&&j.contains(r,n)){m=this.get("id")+h.constants.SOCIAL_SELECTOR+"."+n+"_"+o.timeSelector+"."
}else{m=this.get("id")+h.constants.SOCIAL_SELECTOR+"."+n+"."
}m=m+0+".";
if(q=="DESC"){m=m+Math.abs(p.pageSize)
}else{m=m+"-"+Math.abs(p.pageSize)
}m=m+h.constants.JSON_EXT;
this.url=m;
this.reload()
},getSortOrder:function(){var m=[];
m.push({text:CQ.I18n.get("Newest"),value:"newest"});
m.push({text:CQ.I18n.get("Oldest"),value:"added"});
m.push({text:CQ.I18n.get("Last Updated"),value:"latestActivityDate_dt"});
m.push({text:CQ.I18n.get("Most Viewed"),value:"views"});
m.push({text:CQ.I18n.get("Most Active"),value:"posts"});
m.push({text:CQ.I18n.get("Most Followed"),value:"follows"});
m.push({text:CQ.I18n.get("Most Liked"),value:"likes"});
return m
}});
var e=h.View.extend({viewName:"CommentSystem",className:"comment-system",CREATE_EVENT:"SCFCreate",COMMUNITY_FUNCTION:"Comment System",getOtherProperties:function(){return{}
},PAGE_EVENT:"pageComments",PAGE_URL_PREFIX:"comments",init:function(){this.listenTo(this.model,this.model.events.ADD,this.update);
this.listenTo(this.model,this.model.events.ADD_ERROR,this.showErrorOnAdd);
this.listenTo(this.model.get("items"),"comment:deleted",function(m){this.model.set({totalSize:this.model.get("totalSize")-1});
if(h.Util.mayCall(this.model.get("items"),"remove")){this.model.get("items").remove(m.model)
}this.render()
});
this.listenTo(this.model,this.model.events.ADD,this.recordCreate);
this.listenTo(h.Router,"route:"+this.PAGE_EVENT,this.paginate);
h.Router.route(/^(.*?)\.([0-9]*)\.(-?[0-9]*)\.htm.*?$/,this.PAGE_EVENT);
this.model.view=this
},afterRender:function(){var x=this.model.get("pageInfo"),t=this.model.get("properties"),s=this.model.get("configuration"),v=0,u,q,o,w,m,n,r,p;
if(x&&x.sortIndex){q=u=x.sortIndex;
v=x.pageSize
}else{u=s.sortFields[0].key;
v=s.pageSize
}if(!u){u=""
}p=t&&t.sortFieldOrder?t.sortFieldOrder:"desc";
o=typeof(Storage)!=="undefined"&&localStorage.getItem("sortOrderLs")?localStorage.getItem("sortOrderLs"):p;
o=o.toLowerCase();
n=x.orderReversed;
if(!n&&o==="asc"&&u==="added"){n=true
}w=["views","posts","likes","follows"];
m=u.split("_")[0];
if(j.contains(w,m)){this.model.get("pageInfo").sortIndex=m;
u=m
}if(u){switch(true){case (u==="newest"||(q==="added"&&!n)||(u==="added"&&!n)):r=CQ.I18n.get("Newest");
break;
case (u==="latestActivityDate_dt"):r=CQ.I18n.get("Last Updated");
break;
case (u==="added"):r=CQ.I18n.get("Oldest");
break;
case (u==="views"):r=CQ.I18n.get("Most Viewed");
break;
case (u==="posts"):r=CQ.I18n.get("Most Active");
break;
case (u==="follows"):r=CQ.I18n.get("Most Followed");
break;
case (u==="likes"):r=CQ.I18n.get("Most Liked");
break;
default:r=CQ.I18n.get("Sort By")
}this.$el.find(".scf-sort-btngrp-btnlabel").text(r)
}},recordCreate:function(){if(b.Sitecatalyst){b.record({event:this.CREATE_EVENT,values:{functionType:h.Context.communityFunction?h.Context.communityFunction:this.COMMUNITY_FUNCTION,path:h.Context.path?h.Context.path:this.model.get("id"),type:h.Context.type?h.Context.type:this.model.get("resourceType"),ugcTitle:h.Context.ugcTitle,siteTitle:h.Context.siteTitle?h.Context.siteTitle:$(".scf-js-site-title").text(),sitePath:h.Context.sitePath?h.Context.sitePath:this.sitePath,groupTitle:h.Context.groupTitle,groupPath:h.Context.groupPath,user:h.Context.user?h.Context.user:h.Session.get("authorizableId")},collect:false,componentPath:h.constants.ANALYTICS_BASE_RESOURCE_TYPE})
}},paginate:function(){var p=h.config.urlRoot+this.model.get("id")+h.constants.SOCIAL_SELECTOR+".",v=this.model.get("pageInfo"),q=this.model.get("configuration"),r=(v&&!j.isEmpty(v.sortIndex))?v.sortIndex:"",n=q&&q.analyticsTimeSelector?q.analyticsTimeSelector:"total_tl",u=arguments[0],w=arguments[1],s=arguments[2],m=(arguments.length<=3)?null:arguments[3],o=null,t=["views","posts","likes","follows"];
if(j.contains(t,r)){r=r+"_"+n
}if(arguments.length<=3){if(!j.isEmpty(r)){o=p+r+"."+w+"."+s+h.constants.JSON_EXT
}else{o=p+w+"."+s+h.constants.JSON_EXT
}}else{o=p+"index."+w+"."+s+"."+m+h.constants.JSON_EXT
}this.model.url=o+window.location.search;
this.model.reload()
},update:function(){this.files=void 0;
this.render()
},requiresSession:true,showErrorOnAdd:function(m){m.details.status.message=CQ.I18n.get("Comment text is empty");
if(m.details.status.code===400){m.details.status.message=CQ.I18n.get("Required fields are missing")
}else{if(this.isExceptionOnUGCLimit(m)){m.details.status.message=CQ.I18n.get("Exceeded contribution limit");
this.showUGCLimitDialog(m.details.error.message)
}else{m.details.status.message=CQ.I18n.get("Server error occurred. Please try again later.")
}}this.addErrorMessage(this.$el.find(".scf-js-composer-block input[type='text'], textarea").first(),m);
this.log.error(m)
},isExceptionOnUGCLimit:function(n){if(n.details&&n.details.error){var m=n.details.error;
if(m&&m["class"]==="com.adobe.cq.social.scf.OperationException"&&m.message.indexOf("Exceeded contribution limit")>=0){return true
}else{return false
}}return false
},showUGCLimitDialog:function(p){if(!l(".scf-comment-ugclimitdialog").length){var o=h.CommentSystemView.templates.ugcLimitDialog;
var n=this.compileTemplate(o);
o=l(n(this.getContextForTemplate()));
l(o).appendTo(this.$el)
}var m=CQ.I18n.get("We are sorry, but as new member you are limited to the number of user generated content that you can create. You may contact a Community Manager or Community Moderator if you like");
l(".scf-comment-ugclimitdialog-text").text(m);
l(".scf-comment-ugclimitdialog").modal("show")
},hideError:function(){},expandComposer:function(){this.$el.find(".scf-js-composer-block:first").removeClass("scf-is-collapsed");
var n=this.$el.find(".scf-js-composer-block");
var p=this.getField("message");
var o=CQ.I18n.get("Write a comment");
var m=p.substring(0,o.length);
if(o==m){this.setField("message","")
}if(n.is(":visible")){if(""===p){this.setField("message","")
}}else{this.files=void 0;
this.$el.find(".scf-attachment-list").first().empty()
}this.focus("message")
},cancelComposer:function(){this.$el.find(".scf-js-composer-block:first").addClass("scf-is-collapsed");
this.files=undefined;
l(".scf-js-composer-att").empty();
this.setField("message","");
this.clearErrorMessages()
},toggleComposer:function(m){l(".scf-attachment-error").remove()
},addCommentDraft:function(n){var m=this.extractCommentData(n);
m.isDraft=true;
return this.addToCommentModel(m,n)
},addToCommentModel:function(m,n){this.model.addComment(m);
if(n.target){n.preventDefault()
}return false
},addComment:function(n){var m=this.extractCommentData(n);
if(m===false){return false
}return this.addToCommentModel(m,n)
},extractCommentData:function(p){var n=this.getForm("new-comment");
if(n===null||n.validate()){var q=this.getField("message");
var m=this.getField("tags");
var o=j.extend(this.getOtherProperties(),{message:q,tags:m});
if(!h.Session.get("loggedIn")){o.userIdentifier=this.getField("anon-name");
o.email=this.getField("anon-email");
o.url=this.getField("anon-web")
}if(typeof this.files!=="undefined"){o.files=this.files
}return o
}else{return false
}},navigate:function(u){var w=window.location.protocol+"//"+window.location.host;
var z=l(u.currentTarget).data("page-suffix");
var A=this.model.get("pageInfo");
var p=h.config.urlRoot;
var s=A.basePageURL;
var n=window.location.search;
var t=window.location.pathname;
var m;
if(t.lastIndexOf(".html")<0){m=t.substr(t.length)
}else{m=t.substr(t.lastIndexOf(".html")+5)
}var y=/(.*)\.(\w*)?\.(\d*)?\.(\d*)?\.html(\/.*)?/;
var r=/(.*)\.(\w*)?\.html(\/.*)?/;
var v=/(.*)\.html(\/.*)?/;
if(j.isUndefined(s)||s===null){var B=t;
var o=y.exec(B);
if(o){B=o[1]+"."+o[2]
}else{o=r.exec(B);
if(o){B=o[1]+"."+o[2]
}else{o=v.exec(B);
B=o[1]
}}s=B
}if(w.indexOf(h.config.urlRoot)!=-1){var x=s+"."+z+".html"+m;
x=j.isEmpty(n)?x:x+n;
h.Router.navigate(x,{trigger:true})
}else{z=$(u.currentTarget).data("pageSuffix");
var q=z.split(".");
if(A.sortIndex!==null){this.paginate(s,q[0],q[1],A.sortIndex)
}else{this.paginate(s,q[0],q[1])
}}},renderAttachmentList:function(p){p.preventDefault();
this.files=p.target.files;
var m=l(".scf-js-composer-att");
m.empty();
for(var n=0;
n<this.files.length;
n++){var o=this.files[n];
var q=l('<li class="scf-is-attached">'+j.escape(o.name)+" - "+o.size+" bytes</li>");
m.append(q)
}},openAttachmentDialog:function(m){if(h.Util.mayCall(m,"preventDefault")){m.preventDefault()
}this.$el.find("input[type='file']").first().click()
},translateAll:function(){this.model.translateAll()
},refetchBasedOnSort:function(p){var o=l(p.target).find(".scf-sort-type").addBack(".scf-sort-type");
var m=o.data("sort-field");
var n=o.data("sort-order");
if(typeof(Storage)!=="undefined"){localStorage.setItem("sortOrderLs",n)
}else{throw new Error("Sorry, your browser does not support Web Storage...")
}this.$el.find(".scf-sort-btngrp-btnlabel").text(o.text());
this.model.refetchUsingSort(m,n)
}});
var d=h.Model.extend({modelName:"CommentModel",DELETE_OPERATION:"social:deleteComment",UPDATE_OPERATION:"social:updateComment",CREATE_OPERATION:"social:createComment",EDIT_TRANSLATION_OPERATION:"social:editTranslation",CHANGESTATE_OPERATION:"social:changeState",events:{ADDED:"comment:added",UPDATED:"comment:updated",DELETED:"comment:deleted",ADD_ERROR:"comment:addError",UPDATE_ERROR:"comment:updateError",DELETE_ERROR:"comment:deleteError",TRANSLATED:"comment:translated",TRANSLATE_ERROR:"comment:translateError"},initialize:function(){if(this.isTranslationPresent()){this.set("showingTranslation",true);
h.Comment.isSmartRenderingOn=true
}this.on("change",function(){if(h.Comment.isSmartRenderingOn===true){if(this.get("showingTranslation")===undefined&&this.isTranslationPresent()){this.set("showingTranslation",true)
}}})
},_fixCachedProperties:function(){if(!this._isReady){this.on("model:loaded",j.bind(this._fixCachedProperties,this));
return
}this.off("model:loaded",j.bind(this._fixCachedProperties,this));
if(h.hasOwnProperty("Session")&&(h.Session!==null)){var n=this.attributes.canEdit;
var m=this.attributes.canDelete;
var w=this.attributes.isFlaggedByUser;
var u=this.attributes.approved;
var t=this.attributes.moderatorActions||{};
var v=h.Session.attributes.loggedIn;
var o=j.isUndefined(h.Session.attributes.id)?"":h.Session.attributes.id.substr(h.Session.attributes.id.lastIndexOf("/")+1);
var s=(this.attributes.author||false)&&h.Session.attributes.loggedIn&&((this.attributes.author.id===h.Session.attributes.id)||((this.attributes.properties||false)&&this.attributes.properties.composedBy===o));
var p=(this.attributes.moderatorStatus||false)&&this.attributes.moderatorStatus.hasOwnProperty("isFlagged")?this.attributes.moderatorStatus.isFlagged:false;
var q=(this.attributes.moderatorStatus||false)&&this.attributes.moderatorStatus.hasOwnProperty("isSpam")?this.attributes.moderatorStatus.isSpam:false;
var r=h.Session.checkIfModeratorFor(this.attributes.sourceComponentId);
t.canAllow=r&&!this.attributes.isClosed&&(q||p||!this.attributes.approved);
t.canDeny=(this.attributes.configuration||false)&&r&&!q&&this.attributes.configuration.isDenyAllowed;
t.canClose=(this.attributes.configuration||false)&&r&&this.attributes.topLevel&&this.attributes.configuration.isCloseAllowed;
t.canFlag=!this.attributes.isClosed&&!w&&v&&u&&!s&&!q&&!p&&this.attributes.configuration.isFlaggingAllowed;
this.attributes.canEdit=r||((this.attributes.configuration||false)&&s&&this.attributes.configuration.isEditAllowed&&!this.attributes.isClosed);
this.attributes.canDelete=r||((this.attributes.configuration||false)&&s&&this.attributes.configuration.isDeleteAllowed&&!this.attributes.isClosed);
this.attributes.canReply=h.Session.attributes.mayReply&&(this.attributes.configuration.isReplyAllowed&&!this.attributes.isClosed);
this.attributes.moderatorActions=t;
this.fixCachedProperties(r);
this.cacheFixed=true;
this.trigger("model:cacheFixed",this)
}},fixCachedProperties:function(m){},constructor:function(m,n){h.Model.prototype.constructor.apply(this,[m,n]);
if(h.Session.isReady()){this._fixCachedProperties()
}else{h.Session.on("model:loaded",j.bind(this._fixCachedProperties,this))
}},remove:function(){var m=j.bind(function(q,p,o){this.trigger(this.events.DELETE_ERROR,{error:o})
},this);
var n=j.bind(function(o){this.trigger(this.events.DELETED,{model:this});
this.trigger("destroy",this)
},this);
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:{":operation":this.DELETE_OPERATION},success:n,error:m})
},saveEditTranslation:function(){var n=j.bind(function(r,q,p){this.trigger(this.events.UPDATE_ERROR,this.parseServerError(r,q,p))
},this);
var o=j.bind(function(p){this.reset(p.response,{silent:true});
this.set("showingTranslation",true,{silent:true});
this.set("editTranslationInProgress",false,{silent:true});
if(this.get("translationDisplay")==="side"){this.set("displaySideBySide",true,{silent:true})
}if(this.get("isVisible")){this.trigger(this.events.UPDATED,{model:this})
}else{this.trigger(this.events.DELETED,{model:this});
this.trigger("destroy",this)
}},this);
var m={translatedText:this.get("message"),id:"nobot",":operation":this.EDIT_TRANSLATION_OPERATION};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.TRANSLATE_URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:o,error:n})
},saveEdits:function(){var p=j.bind(function(u,t,s){this.trigger(this.events.UPDATE_ERROR,this.parseServerError(u,t,s))
},this);
var r=j.bind(function(s){this.reset(s.response,{silent:true});
if(this.get("isVisible")){this.trigger(this.events.UPDATED,{model:this})
}else{if(this.get("draft")){this.trigger(this.events.UPDATED,{model:this})
}else{this.trigger(this.events.DELETED,{model:this});
this.trigger("destroy",this)
}}},this);
var n=null;
var q=this.get("files");
var m=(typeof q!=="undefined");
var o=this.get("tags");
if(m){if(window.FormData){n=new FormData()
}if(n){l.each(q,function(s,t){n.append("file",t)
});
n.append("id","nobot");
n.append(":operation",this.UPDATE_OPERATION);
n.append("message",this.get("message"));
l.each(this.getCustomProperties(),function(s,t){n.append(s,t)
});
if(o){l.each(o,function(s,t){n.append("tags",t)
})
}}}if(n===null){n={message:this.get("message"),id:"nobot",":operation":this.UPDATE_OPERATION};
if(o){n.tags=o
}n=j.extend(this.getCustomProperties(),n)
}l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",processData:!m,contentType:(m)?false:"application/x-www-form-urlencoded; charset=UTF-8",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(n),success:r,error:p})
},loadMore:function(){var m=this.get("pageInfo").nextPageURL;
if(m.indexOf(".html",this.length-5)!==-1){m=m.substr(0,m.lastIndexOf(".html"))+".json"
}var o=this;
var n=this.constructor.find(m,function(q){var p=q.get("items");
var r=q.get("pageInfo");
o.set("pageInfo",r);
var s=o.get("items");
s.add(p.models,{silent:true,merge:true});
o.trigger(o.events.UPDATED,{model:o})
},true)
},getCustomProperties:function(){return{}
},isTranslationPresent:function(){var m=this.get("translationDescription");
return(!j.isEmpty(m))
},translate:function(o){if(this.get("translationAjaxInProgress")!==true){if(this.isTranslationPresent()){var q=true;
if(this.get("showingTranslation")===true){q=false
}this.set({showingTranslation:q});
this.trigger(this.events.UPDATED,{model:this});
if(o){o()
}return
}this.set("translationAjaxInProgress",true);
var n=j.bind(function(t,s,r){this.set("translationAjaxInProgress",false);
this.trigger(this.events.TRANSLATE_ERROR,this.parseServerError(t,s,r))
},this);
var p=j.bind(function(r){this.set("translationAjaxInProgress",false);
this.set({showingTranslation:true});
this.set({translationDescription:r.translationDescription});
this.set({translationAttribution:r.translationAttribution});
this.set({translationTitle:r.translationTitle});
if(this.get("translationDisplay")==="side"){this.set("displaySideBySide",true,{silent:true})
}this.trigger(this.events.UPDATED,{model:this});
if(o){o()
}},this);
var m=h.config.urlRoot+this.get("id")+h.constants.TRANSLATE_URL_EXT;
l.ajax({type:"GET",url:m,dataType:"json",success:p,error:n});
this.trigger(this.events.UPDATED,{model:this})
}},addReply:function(q,o,s){var r=j.bind(function(w){var y=w.response;
var u=this.constructor.prototype.relationships.items.model;
var v=h.Models[u];
if(j.isUndefined(v)){this.log.error("reply model not found: "+u);
return
}var A=new v(y);
A._isReady=true;
A.set("_isNew",true);
var z=this.get("items");
var B=false;
if(!z){var x=h.Collections[this.constructor.prototype.relationships.items.collection]||i.Collection;
z=new x();
z.model=this.constructor;
z.parent=this;
B=true
}z.push(A);
var t=this.get("totalSize");
if(B){this.set("items",z)
}this.set("totalSize",t+1);
A.constructor.prototype._cachedModels[y.id]=A;
this.trigger(this.events.ADDED,{model:this});
h.Util.announce(this.events.ADDED,A.attributes)
},this);
var p=j.bind(function(v,u,t){this.trigger(this.events.ADD_ERROR,this.parseServerError(v,u,t))
},this);
var n;
var m=(typeof q.files!="undefined");
if(m){if(window.FormData){n=new FormData()
}if(n){l.each(q.files,function(t,u){n.append("file",u)
});
n.append("id","nobot");
n.append(":operation",this.CREATE_OPERATION);
delete q.files;
l.each(q,function(t,u){n.append(t,u)
})
}}else{n={id:"nobot",":operation":this.CREATE_OPERATION};
j.extend(n,q)
}l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",processData:!m,contentType:(m)?false:"application/x-www-form-urlencoded; charset=UTF-8",xhrFields:{withCredentials:true},data:this.addEncoding(n),success:r,error:p})
},changeCommentState:function(q,o,s){var r=j.bind(function(w){this.set("state",q.toState);
var y=w.response;
var u=this.constructor.prototype.relationships.items.model;
var v=h.Models[u];
if(j.isUndefined(v)){this.log.error("reply model not found: "+u);
return
}var A=new v(y);
A._isReady=true;
A.set("_isNew",true);
var z=this.get("items");
var B=false;
if(!z){var x=h.Collections[this.constructor.prototype.relationships.items.collection]||i.Collection;
z=new x();
z.model=this.constructor;
z.parent=this;
B=true
}z.push(A);
var t=this.get("totalSize");
if(B){this.set("items",z)
}this.set("totalSize",t+1);
A.constructor.prototype._cachedModels[y.id]=A;
this.trigger(this.events.ADDED,{model:this})
},this);
var p=j.bind(function(v,u,t){this.trigger(this.events.ADD_ERROR,this.parseServerError(v,u,t))
},this);
var n=false;
var m={id:"nobot",":operation":this.CHANGESTATE_OPERATION};
j.extend(m,q);
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",processData:!n,contentType:(n)?false:"application/x-www-form-urlencoded; charset=UTF-8",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:r,error:p})
},flag:function(o,p){var n=j.bind(function(t,s,r){this.log.error("error flagging comment "+r);
this.trigger("comment:flagerror",{error:r})
},this);
var q=j.bind(function(r){this.reset(r.response,{silent:true});
if(this.get("isVisible")){this.trigger("comment:flagged",{model:this});
this.trigger(this.events.UPDATED,{model:this})
}else{this.trigger(this.events.DELETED,{model:this});
this.trigger("destroy",this)
}},this);
var m={id:"nobot",":operation":"social:flag","social:flagformtext":o,"social:doFlag":p};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:q,error:n})
},allow:function(){var n=j.bind(function(r,q,p){this.log.error("error allowing comment "+p);
this.trigger("comment:allowerror",{error:p})
},this);
var o=j.bind(function(p){this.reset(p.response);
this.trigger("comment:allowed",{model:this});
this.trigger(this.events.UPDATED,{model:this})
},this);
var m={id:"nobot",":operation":"social:allow"};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:o,error:n})
},resetChildren:function(){var m=this.get("items");
m.each(function(n){n.destroy()
})
},close:function(n){var o=j.bind(function(t,s,r){this.log.error("error closing/opening comment "+r);
this.trigger("comment:closeopenerror",{error:r})
},this);
var q=j.bind(function(r){this.resetChildren();
this.reset(r.response);
this.trigger("comment:closedOpened",{model:this});
this.trigger(this.events.UPDATED,{model:this})
},this);
var p=n?"true":"false";
var m={id:"nobot",":operation":"social:close","social:doClose":p};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:q,error:o})
},deny:function(){var n=j.bind(function(r,q,p){this.log.error("error denying comment "+p);
this.trigger("comment:denyerror",{error:p})
},this);
var o=j.bind(function(p){this.reset(p.response);
this.trigger("comment:denied",{model:this});
this.trigger(this.events.UPDATED,{model:this})
},this);
var m={id:"nobot",":operation":"social:deny"};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:o,error:n})
},pin:function(){this.doPin(true)
},unpin:function(){this.doPin(false)
},doPin:function(o){var n="comment:pin";
var q="comment:pinerror";
if(!o){n="comment:unpin";
q="comment:unpinerror"
}var p=j.bind(function(u,t,s){this.log.error("error pinunpin comment "+s);
this.trigger(q,{error:s})
},this);
var r=j.bind(function(s){this.reset(s.response);
this.trigger(q,{model:this});
this.trigger(this.events.UPDATED,{model:this})
},this);
var m={":operation":"social:pin","social:doPin":o};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:r,error:p})
},markFeatured:function(){this.makeFeatured(true)
},unmarkFeatured:function(){this.makeFeatured(false)
},makeFeatured:function(o){var n="Successfully marked as featured!!";
var q="Failed to mark as featured";
if(!o){n="Successfully unmarked as featured";
q="Failed to unmarked as featured"
}var p=j.bind(function(u,t,s){this.log.error("error toggle topic as featured "+s);
this.trigger(q,{error:s})
},this);
var r=j.bind(function(s){this.reset(s.response);
this.trigger(q,{model:this});
this.trigger(this.events.UPDATED,{model:this})
},this);
var m={":operation":"social:featured","social:markFeatured":o};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:r,error:p})
},addIncludeHint:function(o){var m=this;
var n=null;
while(m!==null){if(m.hasOwnProperty("collection")){m=m.collection
}else{n=m.parent.get("resourceType");
m=null
}}j.extend(o,{"scf:included":this.get("pageInfo").includedPath,"scf:resourceType":n})
},deleteAttachment:function(p){var o=j.bind(function(t,s,r){this.log.error("error deleting attachment "+r);
this.trigger("comment:deleteAttError",{error:r})
},this);
var q=j.bind(function(r){this.trigger("comment:attDeleted",{model:this,attachment:p});
var t=this.get("attachments");
var s=j.omit(t,p);
this.set({attachments:s})
},this);
var n=p.substr(p.lastIndexOf("/")+1);
var m={attToRemove:n,":operation":"social:deleteCommentAttachment"};
l.ajax(h.config.urlRoot+this.get("id")+h.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:this.addEncoding(m),success:q,error:o})
},relationships:{items:{collection:"CommentList",model:"CommentModel"},votes:{model:"VotingModel"}}});
var a=h.View.extend({viewName:"Comment",CREATE_EVENT:"SCFCreate",COMMUNITY_FUNCTION:"Comment System",isExceptionOnUGCLimit:e.prototype.isExceptionOnUGCLimit,showUGCLimitDialog:e.prototype.showUGCLimitDialog,init:function(){this.listenTo(this.model,this.model.events.ADDED,this.update);
this.listenTo(this.model,this.model.events.ADD_ERROR,this.showError);
this.listenTo(this.model,this.model.events.UPDATED,this.commentViewUpdate);
this.listenTo(this.model,this.model.events.DELETED,this.removeView);
this.listenTo(this.model,"comment:attDeleted",this.updateEditableAttachments);
this.listenTo(this.model,this.model.events.ADDED,this.recordCreate);
this.model.view=this;
if(this.model.isTranslationPresent()){if(this.model.get("translationDisplay")==="side"){this.model.set("displaySideBySide",true)
}}},loadMore:function(m){m.preventDefault();
this.model.loadMore()
},removeView:function(){i.View.prototype.remove.apply(this,arguments);
if(this.model.get("topLevel")&&this.model.get("topLevel")===true){location.href=this.model.get("pageInfo").basePageURL+".html"
}},commentViewUpdate:function(){this.render()
},update:function(){this.files=undefined;
this.render()
},recordCreate:function(){if(b.Sitecatalyst){b.record({event:this.CREATE_EVENT,values:{functionType:h.Context.communityFunction?h.Context.communityFunction:this.COMMUNITY_FUNCTION,path:h.Context.path?h.Context.path:this.model.get("id"),type:h.Context.type?h.Context.type:this.model.get("resourceType"),ugcTitle:h.Context.ugcTitle,siteTitle:h.Context.siteTitle?h.Context.siteTitle:$(".scf-js-site-title").text(),sitePath:h.Context.sitePath?h.Context.sitePath:this.sitePath,groupTitle:h.Context.groupTitle,groupPath:h.Context.groupPath,user:h.Context.user?h.Context.user:h.Session.get("authorizableId")},collect:false,componentPath:h.constants.ANALYTICS_BASE_RESOURCE_TYPE})
}},afterRender:function(){this.model.unset("_isNew");
var n=$(".generic-translation-all-button");
if(n){if(this.model.get("canTranslate")===true&&!n.show()){n.show()
}}var m=this.$el.find(".scf-js-attachments").not(this.$("[data-scf-component] .scf-js-attachments"));
var o=this;
if(l().imagesLoaded){m.imagesLoaded(function(){o.attachments=new SCFCards(m)
})
}},showError:function(m){var n=this.$el.find(".scf-js-comment-reply-box:first textarea");
this.log.error(m);
if(this.isExceptionOnUGCLimit(m)){this.showUGCLimitDialog(m.details.error.message);
m.details.status.message=CQ.I18n.get("Exceeded contribution limit")
}this.addErrorMessage(n,m)
},hideError:function(){},edittranslation:function(m){this.model.set("editTranslationInProgress",true);
m.stopPropagation();
var o=this.$el.find(".scf-js-comment-edit-box:first");
o.toggle();
this.$el.find(".scf-js-comment-msg:first").toggle();
var n=this.model.get("translationDescription");
this.setField("editMessage",n);
this.focus("editMessage")
},translate:function(){this.model.translate()
},addReply:function(o){var p=this.getField("replyMessage");
var n=true;
var m=j.extend(this.getOtherProperties(n),{message:p});
if(!h.Session.get("loggedIn")){m.userIdentifier=this.getField("anon-name");
m.email=this.getField("anon-email");
m.url=this.getField("anon-web")
}if(typeof this.files!="undefined"){m.files=this.files
}this.clearErrorMessages();
this.model.addReply(m);
o.preventDefault();
return false
},addReplyFromData:function(n,m){if(!h.Session.get("loggedIn")){m.userIdentifier=this.getField("anon-name");
m.email=this.getField("anon-email");
m.url=this.getField("anon-web")
}if(typeof this.files!="undefined"){m.files=this.files
}this.clearErrorMessages();
this.model.addReply(m);
n.preventDefault();
return false
},reply:function(n){n.stopPropagation();
var m=this.$el.find(".scf-js-comment-reply-box:first");
m.toggle();
this.focus("replyMessage");
this.setField("replyMessage","")
},remove:function(m){m.stopPropagation();
var n=this.$el.find(".comment-delete-box:first");
this._closeModal=this.launchModal(n,CQ.I18n.get("Delete"))
},noDelete:function(m){m.stopPropagation();
this._closeModal();
this._closeModal=undefined
},reallyDelete:function(m){m.stopPropagation();
this.model.remove();
this._closeModal();
this._closeModal=undefined
},edit:function(o){o.stopPropagation();
this.model.set("editTranslationInProgress",false);
var q=this.$el.find(".scf-js-comment-edit-box:first");
q.toggle();
this.$el.find(".scf-js-comment-msg:first").toggle();
this.$el.find(".scf-comment-action").hide();
var p=this.model.get("message");
if(!this.model.getConfigValue("isRTEEnabled")){p=l("<div/>").html(p).text()
}var m=this.$el.find(".scf-js-edit-attachments").not(this.$("[data-scf-component] .scf-js-edit-attachments"));
var n=this;
if(l().imagesLoaded){m.imagesLoaded(function(){n.editableAttachments=new SCFCards(m)
})
}this.setField("editMessage",p);
this.focus("editMessage")
},save:function(n){var m=this.getData();
this.saveOperation(n,m)
},saveDraft:function(n){var m=this.getData();
m.isDraft=true;
this.saveOperation(n,m)
},publishDraft:function(n){var m=this.getData();
this.saveOperation(n,m)
},changeCommentState:function(n){var o=this.getField("message");
var m=j.extend(this.getOtherProperties(),{message:o});
this.clearErrorMessages();
if(j.isEmpty(o)===false){this.model.changeCommentState(m)
}n.preventDefault();
return false
},saveOperation:function(o,n){o.stopPropagation();
o.preventDefault();
var m=this.model.get("editTranslationInProgress");
this.clearErrorMessages();
this.model.set(n);
if(m){this.model.saveEditTranslation()
}else{this.model.saveEdits()
}this.files=undefined;
l(".scf-js-composer-att").empty();
return false
},getData:function(){var m=this.getField("editMessage");
var n=this.getField("editTags");
var o=j.extend(this.getOtherProperties(),{message:m,tags:n});
if(typeof this.files!="undefined"){o.files=this.files
}return o
},cancelComposer:function(n){n.stopPropagation();
this.clearErrorMessages();
var m=this.$el.find(".scf-js-comment-reply-box:first");
m.toggle();
this.files=undefined
},cancel:function(p){p.stopPropagation();
this.clearErrorMessages();
var q=this.$el.find(".scf-js-comment-edit-box:first");
q.hide();
var o=this.model.changedAttributes();
if(o){var n=j.keys(o);
var m=j.pick(this.model.previousAttributes(),n);
this.model.set(m)
}this.$el.find(".scf-js-comment-msg:first").show();
this.$el.find(".scf-comment-action").show()
},getOtherProperties:function(){return{}
},editFlagReason:function(n){n.preventDefault();
var m=this.$el.find(".scf-js-flagreason-box:first");
this._closeDialog=this.launchModal(m,CQ.I18n.get("Flag"))
},cancelFlagging:function(m){m.preventDefault();
this._closeDialog();
this._closeDialog=undefined
},addFlagReason:function(n){n.preventDefault();
var m=this.getField("flagReason");
if(m.length===0||m==="custom"){m=this.getField("customFlagReason")
}if(m.length!==0){this.model.flag(m,true)
}else{this.model.flag("",true)
}this._closeDialog();
this._closeDialog=undefined
},toggleFlag:function(n){n.preventDefault();
var m=this.$el.find(".scf-js-toggleFlag-box:first");
m.toggle()
},removeFlag:function(m){m.preventDefault();
this.model.flag(null,false)
},close:function(m){m.preventDefault();
this.model.close(true)
},open:function(m){m.preventDefault();
this.model.close(false)
},allow:function(m){m.preventDefault();
this.model.allow()
},deny:function(m){m.preventDefault();
this.model.deny()
},pin:function(m){m.preventDefault();
this.model.pin()
},unpin:function(m){m.preventDefault();
this.model.unpin()
},markFeatured:function(m){m.preventDefault();
this.model.markFeatured()
},unmarkFeatured:function(m){m.preventDefault();
this.model.unmarkFeatured()
},getLastPath:function(){var m=this.model.get("id");
var n=m.lastIndexOf("/");
n=m.slice(n+1);
return n
},renderAttachmentList:function(p){p.preventDefault();
this.files=p.target.files;
var m=l(".scf-js-composer-att");
m.empty();
for(var n=0;
n<this.files.length;
n++){var o=this.files[n];
var q=l('<li class="scf-is-attached">'+j.escape(o.name)+" - "+o.size+" bytes</li>");
m.append(q)
}},openAttachmentDialog:function(m){if(h.Util.mayCall(m,"preventDefault")){m.preventDefault()
}this.$el.find("input[type='file']").first().click()
},toggleAttachmentOverlay:function(n){var m=$(n.target).closest(".scf-js-comment-att");
if(m.hasClass("scf-is-card-overlay-on")){m.removeClass("scf-is-card-overlay-on")
}else{m.addClass("scf-is-card-overlay-on")
}},deleteAttachment:function(o){var n=$(o.target).closest(".scf-js-comment-att");
var m=n.data("attachment-path");
this.model.deleteAttachment(m)
},updateEditableAttachments:function(m){var o=m.attachment;
var n=this.$el.find('[data-attachment-path="'+o+'"]');
n.remove();
this.editableAttachments.redraw(true)
},confirmDeleteAttachment:function(n){var m=$(n.target).closest(".scf-js-comment-att");
m.addClass("scf-is-att-confirm-overlay-on")
},cancelDeleteAttachment:function(n){var m=$(n.target).closest(".scf-js-comment-att");
m.removeClass("scf-is-att-confirm-overlay-on")
}});
var k=i.Collection.extend({collectionName:"CommentList"});
var g=h.Model.extend({modelName:"AuthorModel"});
var c=h.View.extend({viewName:"Author",tagName:"div",className:"author"});
h.Comment=d;
h.CommentSystem=f;
h.CommentView=a;
h.CommentSystemView=e;
h.CommentList=k;
h.Author=g;
h.AuthorView=c;
h.registerComponent("social/commons/components/hbs/comments/comment",h.Comment,h.CommentView);
h.registerComponent("social/commons/components/hbs/comments",h.CommentSystem,h.CommentSystemView)
})($CQ,_,Backbone,SCF);
(function(c,a,d,b){b.CommentSystemView.templates=b.CommentSystemView.templates||{};
b.CommentSystemView.templates.ugcLimitDialog='<div class="modal fade scf-comment-ugclimitdialog" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close scf-modal-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h1 class="modal-title">{{i18n "Content Notice"}}</h1></div><div class="modal-body"><div class="clearfix"></div><p class="text-xs-center scf-comment-ugclimitdialog-text"></p></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal">{{i18n "Close"}}</button></div></div><!-- /.modal-content --></div><!-- /.modal-dialog --></div><!-- /.modal -->'
})($CQ,_,Backbone,SCF);
(function(e,b,f,d){var c=window.location.protocol+"//"+window.location.host;
var a=d.View.extend({afterRender:function(){var h=this.model.get("subject");
var l=this.model.get("message");
var k=document.createElement("div");
k.innerHTML=l;
var j=k.innerText||"";
var g=c+this.model.get("friendlyUrl");
var i="mailto:?Subject="+encodeURIComponent(h)+"&Body="+g+"%0D%0A%0D%0A"+encodeURIComponent(j)+"%0D%0A";
this.$el.find(".scf-js-email-share").attr("href",i)
},share:function(g){var i=$(g.target).data("href");
var h=i+c+this.model.get("friendlyUrl");
window.open(h,"_BLANK")
}});
d.ShareView=a;
d.registerComponent("social/commons/components/hbs/socialshare",d.Model,d.ShareView)
})($CQ,_,Backbone,SCF);
(function(m,k,j,h){window.CQ_Analytics=window.CQ_Analytics||{};
var b=CQ_Analytics;
var f=h.CommentSystem.extend({modelName:"ForumModel",relationships:{items:{collection:"TopicList",model:"TopicModel"}},fixCachedProperties:function(){var n=h.Session.checkIfModeratorFor(this.get("id"));
this.loadClipboard(n)
},loadClipboard:function(n){var o=localStorage.getItem(e.prototype.FORUM_CLIPBOARD);
if(o==null||k.isUndefined(o)){this.attributes.canPaste=false;
return
}o=JSON.parse(o);
this.attributes.canPaste=n&&o.count>0;
this.attributes.pasteCount=o.count
},clearClipboard:function(){localStorage.removeItem(e.prototype.FORUM_CLIPBOARD);
this.attributes.canPaste=false;
this.attributes.pasteCount=0;
this.trigger(this.events.UPDATED,{model:this})
},createOperation:"social:createForumPost",events:{ADD:"topic:added",ADD_ERROR:"topic:adderror",UPDATED:"topic:updated"}});
var g=h.CommentSystemView.extend({viewName:"Forum",tagName:"div",className:"forum",FOLLOW_EVENT:"SCFFollow",VIEW_EVENT:"SCFView",COMMUNITY_FUNCTION:"Forum",init:function(){h.CommentSystemView.prototype.init.apply(this);
this.listenTo(this.model,this.model.events.UPDATED,this.update);
h.Router.route(/^(.*?)\.index\.(.*)\.(-?[0-9]*)\.([0-9])*\.htm.*?$/,"pageTopics")
},render:function(){h.CommentSystemView.prototype.render.apply(this);
if(b.Sitecatalyst){if(k.contains(b.Sitecatalyst.frameworkComponents,h.constants.ANALYTICS_BASE_RESOURCE_TYPE)){this.$el.find(".scf-js-analytics-view-metrics").show()
}}},getOtherProperties:function(){var n=this.getField("subject");
return{subject:n}
},PAGE_EVENT:"pageTopics",PAGE_URL_PREFIX:"topics",toggleComposer:function(o){var n=this.$el.find(".scf-js-composer-block");
n.toggleClass("scf-is-collapsed");
this.$el.find(".scf-js-newtopic").toggleClass("scf-is-collapsed");
if(n.is(":visible")){this.focus("subject");
this.setField("message","")
}else{this.files=void 0;
this.$el.find(".scf-attachment-list").first().empty()
}h.CommentSystemView.prototype.toggleComposer.apply(this,[o])
},paste:function(){h.log.debug("pasting");
var o=localStorage.getItem("scf:forum:clipboard");
if(k.isUndefined(o)||o==null||o.count<=0){h.log.error("There are no clipboard items to paste")
}else{var n=this.model.get("id");
o=JSON.parse(o);
k.each(o.itemsToMove,function(p){this.model.move({resourcePath:p.id,parentPath:n})
},this);
this.model.clearClipboard()
}},clearClipboard:function(){h.log.debug("clear board");
this.model.clearClipboard()
},initAnalytics:function(){h.Context.communityFunction=this.COMMUNITY_FUNCTION;
h.Context.path=this.model.id;
h.Context.type=this.model.get("resourceType");
h.Context.ugcTitle=this.model.get("subject")?this.model.get("subject"):"";
if(b.Sitecatalyst){window.s.abort=true;
b.record({event:this.VIEW_EVENT,values:{functionType:h.Context.communityFunction?h.Context.communityFunction:this.COMMUNITY_FUNCTION,path:h.Context.path?h.Context.path:this.model.get("id"),type:h.Context.type?h.Context.type:this.model.get("resourceType"),ugcTitle:h.Context.ugcTitle,siteTitle:h.Context.siteTitle?h.Context.siteTitle:$(".scf-js-site-title").text(),sitePath:h.Context.sitePath?h.Context.sitePath:this.sitePath,groupTitle:h.Context.groupTitle,groupPath:h.Context.groupPath,user:h.Context.user?h.Context.user:h.Session.get("authorizableId")},collect:false,componentPath:h.constants.ANALYTICS_BASE_RESOURCE_TYPE})
}}});
var d=h.Comment.extend({modelName:"PostModel",DELETE_OPERATION:"social:deleteForumPost",UPDATE_OPERATION:"social:updateForumPost",CREATE_OPERATION:"social:createForumPost",events:{ADDED:"post:added",UPDATED:"post:updated",DELETED:"post:deleted",ADD_ERROR:"post:addError",UPDATE_ERROR:"post:updateError",DELETE_ERROR:"post:deleteError",TRANSLATED:"post:translated",TRANSLATE_ERROR:"post:translateError"},relationships:{items:{collection:"PostList",model:"PostModel"},votes:{model:"VotingModel"}},fixCachedProperties:function(){if(!h.Session.attributes.loggedIn){this.attributes.canReply=false
}}});
var c=h.CommentView.extend({viewName:"Post",tagName:"li",className:"post",requiresSession:true});
var e=h.Comment.extend({modelName:"TopicModel",DELETE_OPERATION:"social:deleteForumPost",UPDATE_OPERATION:"social:updateForumPost",CREATE_OPERATION:"social:createForumPost",FORUM_CLIPBOARD:"scf:forum:clipboard",events:{ADDED:"post:added",UPDATED:"topic:updated",DELETED:"topic:deleted",ADD_ERROR:"post:addError",UPDATE_ERROR:"topic:updateError",DELETE_ERROR:"topic:deleteError",TRANSLATED:"topic:translated",TRANSLATE_ERROR:"topic:translateError"},relationships:{items:{collection:"TopicList",model:"TopicModel"},votes:{model:"VotingModel"}},addToClipBoard:function(){if(typeof Storage=="undefined"){h.log.error("Unable to move topic, please use a supported browser")
}var n=localStorage.getItem(this.FORUM_CLIPBOARD);
if(k.isUndefined(n)||n==null){n={count:0,itemsToMove:{}}
}else{n=JSON.parse(n)
}var o={id:this.id,title:this.get("title"),url:this.get("friendlyURL")};
n.itemsToMove[this.id]=o;
n.count++;
this.attributes.hasBeenCut=true;
localStorage.setItem(this.FORUM_CLIPBOARD,JSON.stringify(n));
this.trigger(this.events.UPDATED,{model:this})
},removeFromClipboard:function(){if(typeof Storage=="undefined"){h.log.error("Unable to move topic, please use a supported browser")
}var n=localStorage.getItem(this.FORUM_CLIPBOARD);
n=JSON.parse(n);
n.count--;
n.itemsToMove[this.id]=undefined;
this.attributes.hasBeenCut=false;
localStorage.setItem(this.FORUM_CLIPBOARD,JSON.stringify(n));
this.trigger(this.events.UPDATED,{model:this})
},getCustomProperties:function(){if(this.get("subject")!==undefined){return{subject:this.get("subject")}
}else{return{}
}},fixCachedProperties:function(o){this.attributes.canReply=h.Session.attributes.loggedIn&&h.Session.attributes.mayReply&&!this.attributes.isClosed;
this.attributes.moderatorActions.canMove=o&&!this.attributes.isClosed&&this.attributes.topLevel&&this.attributes.configuration.moveAllowed;
var n=this.hasBeenCut();
this.attributes.hasBeenCut=n
},hasBeenCut:function(){var o=localStorage.getItem(this.FORUM_CLIPBOARD);
if(o==null||k.isUndefined(o)){return false
}o=JSON.parse(o);
var n=o.itemsToMove[this.id];
if(k.isUndefined(n)){return false
}else{return true
}},translateAll:function(n){h.CommentSystem.prototype.translateAll.call(this,n)
}});
var l=h.CommentView.extend({viewName:"Topic",tagName:"li",className:"topic",requiresSession:true,FOLLOW_EVENT:"SCFFollow",VIEW_EVENT:"SCFView",COMMUNITY_FUNCTION:"Forum",init:function(){h.CommentView.prototype.init.apply(this);
h.Router.route(/^(.*?)topic\.([0-9]*)\.(-?[0-9]*)\.htm.*?$/,"pagePosts");
h.Router.route(/^(.*?)topic\.index\.(.*)\.(-?[0-9]*)\.([0-9])*\.htm.*?$/,"pagePosts");
this.listenTo(h.Router,"route:pagePosts",this.paginate)
},initAnalytics:function(){h.Context.communityFunction=this.COMMUNITY_FUNCTION;
h.Context.path=this.model.id;
h.Context.type=this.model.get("resourceType");
h.Context.ugcTitle=this.model.get("subject")?this.model.get("subject"):"";
if(b.Sitecatalyst){window.s.abort=true;
b.record({event:this.VIEW_EVENT,values:{functionType:h.Context.communityFunction?h.Context.communityFunction:this.COMMUNITY_FUNCTION,path:h.Context.path?h.Context.path:this.model.get("id"),type:h.Context.type?h.Context.type:this.model.get("resourceType"),ugcTitle:h.Context.ugcTitle,siteTitle:h.Context.siteTitle?h.Context.siteTitle:$(".scf-js-site-title").text(),sitePath:h.Context.sitePath?h.Context.sitePath:this.sitePath,groupTitle:h.Context.groupTitle,groupPath:h.Context.groupPath,user:h.Context.user?h.Context.user:h.Session.get("authorizableId")},collect:false,componentPath:h.constants.ANALYTICS_BASE_RESOURCE_TYPE})
}},afterRender:function(){h.CommentView.prototype.afterRender.apply(this);
this.updateCrumbs()
},updateCrumbs:function(){if(this.$el.attr("data-scf-template")){return
}var n=[];
n.push({title:CQ.I18n.get("Forum"),url:this.model.get("pageInfo").basePageURL+".html"});
n.push({title:this.model.get("subject"),url:"",active:true});
h.Util.announce("crumbs",{crumbs:n})
},paginate:function(){var q=h.config.urlRoot+this.model.get("id")+h.constants.SOCIAL_SELECTOR+".";
var o=arguments[1];
var p=arguments[2];
var r=(arguments.length<=3)?null:arguments[3];
var n;
if(arguments.length<=3){n=q+o+"."+p+h.constants.JSON_EXT
}else{n=q+"index."+o+"."+p+"."+r+h.constants.JSON_EXT
}this.model.url=n;
this.model.reload()
},navigate:function(t){var p=window.location.protocol+"//"+window.location.host;
var s=m(t.currentTarget).data("page-suffix");
var o=this.model.get("pageInfo");
if(p.indexOf(h.config.urlRoot)!==-1){var r=this.model.get("id");
r=r.substring(r.lastIndexOf("/"));
var n=o.basePageURL+".topic."+s+".html"+r;
h.Router.navigate(n,{trigger:true})
}else{s=$(t.currentTarget).data("pageSuffix");
var q=s.split(".");
if(o.sortIndex!==null){this.paginate(o.basePageURL,q[0],q[1],o.sortIndex)
}else{this.paginate(o.basePageURL,q[0],q[1])
}}},edittranslation:function(o){h.CommentView.prototype.edittranslation.call(this,o);
this.$el.find(".scf-js-topic-details").hide();
var n=this.model.get("translationTitle");
this.setField("editSubject",n);
this.focus("editSubject")
},edit:function(o){h.CommentView.prototype.edit.call(this,o);
this.$el.find(".scf-js-topic-details").hide();
var n=this.model.get("subject");
this.setField("editSubject",n);
this.focus("editSubject")
},cancel:function(n){h.CommentView.prototype.cancel.call(this,n);
this.$el.find(".scf-js-topic-details").show()
},cut:function(n){h.log.debug("cutting "+this.model.get("id"));
this.model.addToClipBoard()
},putBack:function(n){h.log.debug("putting back "+this.model.get("id"));
this.model.removeFromClipboard()
},getOtherProperties:function(){var n=this.getField("editSubject");
return{subject:n}
},toggleComposerCollapse:function(n){n.preventDefault();
$(n.currentTarget).closest(".scf-js-composer-block").toggleClass("scf-is-collapsed");
this.focus("replyMessage");
$(window).scrollTop($(n.currentTarget).closest(".scf-js-composer-block").offset().top)
},translateAll:function(){this.model.translateAll()
}});
var i=j.Collection.extend({collectionName:"TopicList"});
var a=j.Collection.extend({collectionName:"PostList"});
h.Post=d;
h.Topic=e;
h.Forum=f;
h.TopicView=l;
h.PostView=c;
h.ForumView=g;
h.TopicList=i;
h.PostList=a;
h.registerComponent("social/forum/components/hbs/post",h.Post,h.PostView);
h.registerComponent("social/forum/components/hbs/topic",h.Topic,h.TopicView);
h.registerComponent("social/forum/components/hbs/forum",h.Forum,h.ForumView)
})($CQ,_,Backbone,SCF);
(function(n,k,i,g){var j=g.Forum.extend({modelName:"JournalModel",relationships:{items:{collection:"BlogTopicList",model:"BlogTopicModel"}},createOperation:"social:createJournalComment",events:{ADD:"blogtopic:added",ADD_ERROR:"blogtopic:adderror"},shouldCommentBeAddedToList:function(q){var r=this.view.listTypes.PUBLISHED;
var t=this.view.listTypes.PUBLISHED;
var s=false;
if(q){if(q.get("draft")){s=q.get("draft");
if(s){var p=q.get("publishDate");
if(p){var o=new Date(p);
if(o>new Date()){t=this.view.listTypes.SCHEDULED_LATER
}else{t=this.view.listTypes.DRAFTS
}}else{t=this.view.listTypes.DRAFTS
}}}}var u=this.get("pageInfo").nextPageURL;
if(typeof u=="string"&&u.indexOf("filter")>-1){if(u.indexOf(this.view.filterURLParam.SCHEDULED_LATER_URL_FILTER)>0){r=this.view.listTypes.SCHEDULED_LATER
}else{r=this.view.listTypes.DRAFTS
}}if(t==r){return true
}else{if(!s){this.trigger(this.events.ADD,{model:this,newItem:q})
}return false
}}});
var c=g.ForumView.extend({viewName:"Journal",COMMUNITY_FUNCTION:"Blog",eventBinded:false,filterURLParam:{DRAFT_URL_FILTER:"?filter=isDraft%20eq%20%27true%27&filter=publishDate%20eq%20null",SCHEDULED_LATER_URL_FILTER:"?filter=isDraft%20eq%20%27true%27&filter=publishDate%20ne%20null"},listTypes:{PUBLISHED:"published",SCHEDULED_LATER:"scheduledLater",DRAFTS:"drafts"},PAGE_EVENT:"pageBlogArticles",afterRender:function(){g.ForumView.prototype.afterRender.apply(this);
if(this.pageUrlSortHasFilterParams()){this.renderWithTemplate(true)
}},init:function(){g.ForumView.prototype.init.apply(this);
var p=this.model.get("resourceType");
var o="journallists";
this.listenTo(this.model,"change:composedForValid",this.composedForChanged);
this.listTemplateC=g.findTemplate(this.model.id,o,p);
g.Router.route(/^(.*?)\.([0-9]*)\.(-?[0-9]*)\.htm.*?$/,this.PAGE_EVENT)
},getUrl:function(){var p=this.model.url;
if(typeof p=="string"){return p
}else{var o=this.model.url();
return o?o:this.model.get("url")
}},composedForChanged:function(){if(this.model.get("composedForValid")){this.$el.find(".scf-js-userfilter").removeClass("scf-error");
this.$el.find(".scf-js-invalid-user").addClass("scf-is-hidden").removeClass("scf-js-error-message");
this.$el.find(".scf-js-publish-btn").prop("disabled",false)
}else{this.$el.find(".scf-js-userfilter").addClass("scf-error");
this.$el.find(".scf-js-invalid-user").addClass("scf-js-error-message").removeClass("scf-is-hidden");
this.$el.find(".scf-js-publish-btn").prop("disabled",true)
}},update:function(p){if(p&&p.newItem){var o=p.newItem;
if(!o.get("draft")){g.ForumView.prototype.update.apply(this)
}}},activateTabs:function(p,o){this.$el.find(p).parent().addClass("active");
this.$el.find(o).addClass("active")
},pageUrlSortHasFilterParams:function(){var q=this.model.get("pageInfo");
q=q?q.nextPageURL:"";
var p=CQ.shared.HTTP.getParameters(q,"filter");
var r=this;
var o=function(){return k.contains(p,"filter=isDraft%20ne%20true")||k.contains(p,"isDraft+ne+true")
};
if(p&&p.length>0&&!o()){return true
}return false
},renderWithTemplate:function(t){var q=this.getUrl();
this.$el.find(".tab-pane").empty();
var p=n(this.listTemplateC(this.getContextForTemplate(),{data:{parentView:this}}));
var o=false;
if(!k.isNull(this.model.url)&&q.indexOf(this.filterURLParam.SCHEDULED_LATER_URL_FILTER)>0){this.$el.find("#scf-js-laterPosts").empty().append(p)
}else{this.$el.find("#scf-js-draftPosts").empty().append(p);
o=true
}var s=this;
k.each(this._childViews,function(u){s.renderChildView(u)
});
var r=k.bind(function(){this.bindView();
this._rendered=true;
if(this.afterRender&&!t){this.afterRender()
}this.trigger("view:rendered",{view:this})
},this);
n.when(this._renderedChildren).done(r);
this.$el.find("li.scf-journal-tab").removeClass("active");
this.$el.find(".tab-pane").removeClass("active");
if(o){this.activateTabs(".scf-js-draftPosts","#scf-js-draftPosts")
}else{this.activateTabs(".scf-js-laterPosts","#scf-js-laterPosts")
}return this
},toggleComposer:function(p){this.$el.find(".scf-js-journal-tab").toggleClass("scf-is-hidden");
this.$el.find(".scf-topic-list").toggleClass("scf-is-hidden");
this.$el.find(".scf-pages").toggleClass("scf-is-hidden");
var o=this.$el.find(".scf-js-composer-block");
if(o.hasClass("scf-is-collapsed")){this.eventBinded=false
}if(!this.eventBinded){this.bindDatePicker(p);
this.eventBinded=true
}g.ForumView.prototype.toggleComposer.apply(this,[p])
},bindDatePicker:function(p){var o=function(r,q,t){if(r.has("option").length<=0){var s=this;
n.each(k.range(q,t),function(u,v){v=v<10?("0"+v):v;
v=CQ.I18n.get(v);
r.append($("<option/>",{value:v,text:v}))
})
}};
this.$el.find(".scf-js-pubish-type .dropdown-menu li a").click(function(){$(".scf-js-pubish-type .btn:first-child").html('<span id="button_label">'+$(this).text()+'</span> <span class="caret"></span>');
$(".scf-js-pubish-type .btn:first-child").val($(this).text())
});
this.$el.find(".scf-js-event-basics-start-input").datepicker({changeMonth:true,numberOfMonths:2,setDate:"0",beforeShow:function(q,r){$("#ui-datepicker-div").wrap("<div class='scf-datepicker'></div>")
},onClose:function(q){n(".scf-event-basics-start-input").datepicker("option","minDate",q)
}}).datepicker("setDate",new Date());
o(this.$el.find(".scf-js-event-basics-start-hour"),1,13);
o(this.$el.find(".scf-js-event-basics-start-min"),0,60);
d(new Date(),this)
},showDraftOption:function(o){this.$el.find(".scf-js-publish-time-input").addClass("scf-is-hidden");
this.$el.find(".scf-js-save-draft-btn").show();
this.$el.find(".scf-js-publish-btn").hide()
},showPublishOption:function(o){this.$el.find(".scf-js-publish-time-input").removeClass("scf-is-hidden");
this.$el.find(".scf-js-save-draft-btn").hide();
this.$el.find(".scf-js-publish-btn").show()
},showImmediatelyOption:function(o){this.$el.find(".scf-js-publish-time-input").addClass("scf-is-hidden");
this.$el.find(".scf-js-save-draft-btn").hide();
this.$el.find(".scf-js-publish-btn").show()
},getOtherProperties:function(){var q=this.getField("subject").trim();
var t=$(this.$el.find(".scf-js-pubish-type > button > span")).text();
var s=false;
var p=null;
var o=false;
if(!k.isEmpty(t)&&t==$(this.$el.find(".scf-js-pubish-type > ul > li > a")[1]).text()){s=true
}else{if(!k.isEmpty(t)&&t==$(this.$el.find(".scf-js-pubish-type > ul > li > a")[2]).text()){s=true;
o=true;
p=e(this.$el.find(".scf-js-event-basics-start-input").val(),this.$el.find(".scf-js-event-basics-start-hour").val(),this.$el.find(".scf-js-event-basics-start-min").val(),this.$el.find(".scf-js-event-basics-start-time-ampm").val())
}}var r={subject:q,isDraft:s};
if(o){r.isScheduled=true;
r.publishDate=p
}if(this.model.getConfigValue("usingPrivilegedUsers")){var u=this.getField("composedFor");
if(!k.isEmpty(u)){r.composedFor=u
}}this.eventBinded=false;
return r
},fetchDrafts:function(){this.switchView(this.filterURLParam.DRAFT_URL_FILTER,".scf-js-draftPosts","#scf-js-draftPosts")
},fetchLaterPosts:function(){this.switchView(this.filterURLParam.SCHEDULED_LATER_URL_FILTER,".scf-js-laterPosts","#scf-js-laterPosts")
},fetchAllPosts:function(){this.switchView("",".scf-js-allPosts","#scf-js-allPosts")
},switchView:function(r,t,p){this.model.url=this.model.id+g.constants.URL_EXT+r;
var s=this;
g.log.debug("switchView:"+this.model.url);
var q=this.model.get("pageInfo");
var u=q.basePageURL;
var o=u+".html"+r;
this.model.reload({success:function(){s.activateTabs(t,p);
g.Router.navigate(o,{trigger:true,replace:true})
},error:function(){g.log.error("Error reloading model")
}})
}});
var e=function(r,p,s,q){if(!r||!p||!s){return undefined
}try{if(q==="PM"&&p!=="12"){p=parseInt(p);
p=p+12
}else{if(q==="AM"&&p==="12"){p=0
}}var u=((parseInt(p)*60)+parseInt(s))*60000;
var o=Date.parse(r)+u;
return(new Date(o).toISOString())
}catch(t){return NaN
}};
var d=function(q,s){var o=q.getHours();
var r=q.getMinutes();
s.$el.find(".scf-js-event-basics-start-input").datepicker("setDate",q);
var p=o>=12?"PM":"AM";
o=o>12?o-12:o;
o=o<10?("0"+o):o;
r=r<10?("0"+r):r;
o=o.toString();
if(o==="00"){o="12"
}r=r.toString();
s.$el.find(".scf-js-event-basics-start-hour").val(o);
s.$el.find(".scf-js-event-basics-start-min").val(r);
s.$el.find(".scf-js-event-basics-start-time-ampm").val(p)
};
var m=g.Post.extend({modelName:"BlogPostModel",DELETE_OPERATION:"social:deleteJournalComment",UPDATE_OPERATION:"social:updateJournalComment",CREATE_OPERATION:"social:createJournalComment",relationships:{items:{collection:"BlogPostList",model:"BlogPostModel"},votes:{model:"VotingModel"}}});
var f=g.PostView.extend({viewName:"BlogPost"});
var a=g.Topic.extend({modelName:"BlogTopicModel",DELETE_OPERATION:"social:deleteJournalComment",UPDATE_OPERATION:"social:updateJournalComment",CREATE_OPERATION:"social:createJournalComment",relationships:{items:{collection:"BlogTopicList",model:"BlogTopicModel"},votes:{model:"VotingModel"}},getCustomProperties:function(){var q={subject:this.get("subject")};
if(this.has("isDraft")){q.isDraft=this.get("isDraft");
var o=this.get("publishDate");
if(!k.isEmpty(o)){q.publishDate=o;
q.isScheduled=true
}}if(this.getConfigValue("usingPrivilegedUsers")){var p=this.get("composedFor");
if(!k.isEmpty(p)){q.composedFor=p
}}return q
}});
var l=g.TopicView.extend({viewName:"BlogTopic",COMMUNITY_FUNCTION:"Blog",eventBinded:false,bindDatePicker:c.prototype.bindDatePicker,showImmediatelyOption:c.prototype.showImmediatelyOption,showPublishOption:c.prototype.showPublishOption,showDraftOption:c.prototype.showDraftOption,init:function(){g.TopicView.prototype.init.call(this);
this.listenTo(this.model,this.model.events.DELETED,this.navigateCancel)
},edit:function(p){this.$el.find(".scf-js-journal-comment-section").toggleClass("scf-is-hidden");
g.TopicView.prototype.edit.call(this,p);
this.$el.find(".scf-js-topic-details").hide();
this.$el.find(".scf-js-topic-details-tags-editable").show();
this.$el.find(".scf-comment-toolbar .scf-comment-edit").hide();
var o=this.model.get("subject");
this.setField("editSubject",o);
this.focus("editSubject");
if(!this.eventBinded){this.bindDatePicker(p);
this.eventBinded=true
}},afterRender:function(){g.TopicView.prototype.afterRender.call(this);
if(!this.eventBinded){this.bindDatePicker();
this.eventBinded=true
}var r=this.$el.find(".scf-js-pubish-type > button > span")[0];
if(r===undefined){return
}var q=false;
if(this.model.get("properties")&&this.model.get("properties").isDraft){q=this.model.get("properties").isDraft
}var o;
if(this.model.get("publishDate")){o=this.model.get("publishDate")
}if(q&&o!=null){$(r).text(CQ.I18n.getMessage("At scheduled date and time"));
var p=new Date(o);
d(p,this);
this.showPublishOption()
}else{if(q){$(r).text(CQ.I18n.getMessage("Draft"));
this.showDraftOption()
}}},navigateCancel:function(o){window.location.href=this.model.get("pageInfo").basePageURL+".html"
},cancel:function(o){this.$el.find(".scf-js-journal-comment-section").toggleClass("scf-is-hidden");
g.TopicView.prototype.cancel.call(this,o);
this.$el.find(".scf-js-topic-details").show();
this.$el.find(".scf-js-topic-details-tags-editable").hide();
this.$el.find(".scf-comment-toolbar .scf-comment-edit").show()
},getOtherProperties:function(s){var q=this.getField("editSubject").trim();
var p=this.getField("editTags");
var r={tags:p};
if(!s){r.subject=q
}var t=$(this.$el.find(".scf-js-pubish-type > button > span")).text();
var o=null;
if(!k.isEmpty(t)&&t==$(this.$el.find(".scf-js-pubish-type > ul > li > a")[1]).text()){r.isDraft=true
}else{if(!k.isEmpty(t)&&t==$(this.$el.find(".scf-js-pubish-type > ul > li > a")[2]).text()){r.isDraft=true;
r.isScheduled=true;
r.publishDate=e(this.$el.find(".scf-js-event-basics-start-input").val(),this.$el.find(".scf-js-event-basics-start-hour").val(),this.$el.find(".scf-js-event-basics-start-min").val(),this.$el.find(".scf-js-event-basics-start-time-ampm").val())
}else{r.isDraft=false
}}if(this.model.getConfigValue("usingPrivilegedUsers")){var u=this.getField("composedFor");
if(!k.isEmpty(u)){r.composedFor=u
}}this.eventBinded=false;
return r
},deleteArticle:function(o){o.stopPropagation();
this.unbindDataFields();
this.model.remove()
},toggleComposerCollapse:function(o){o.preventDefault();
$(o.currentTarget).closest(".scf-js-composer-block").toggleClass("scf-is-collapsed");
this.focus("replyMessage");
$(window).scrollTop($(o.currentTarget).closest(".scf-js-composer-block").offset().top)
}});
var h=i.Collection.extend({collectionName:"BlogTopicList"});
var b=i.Collection.extend({collectionName:"BlogPostList"});
g.BlogPost=m;
g.BlogTopic=a;
g.Journal=j;
g.BlogTopicView=l;
g.BlogPostView=f;
g.JournalView=c;
g.BlogTopicList=h;
g.BlogPostList=b;
g.registerComponent("social/journal/components/hbs/comment",g.BlogPost,g.BlogPostView);
g.registerComponent("social/journal/components/hbs/entry_topic",g.BlogTopic,g.BlogTopicView);
g.registerComponent("social/journal/components/hbs/journal",g.Journal,g.JournalView)
})($CQ,_,Backbone,SCF);