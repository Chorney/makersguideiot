(function(f,e){var b=false;
var c=0;
var d="";
var a=function(k){var n=k.crumbs;
var g=f(".scf-js-site-breadcrumbs");
var h=f(g).find("li:last");
f(h).remove();
if(!b){d=f(h).text();
b=true;
c=f(g).find("li").length-1
}else{var m=c==-1?f(g).find("li"):f(g).find("li:gt("+c+")");
m.remove()
}for(var l=0;
l<n.length;
l++){var q=n[l];
var o=f("<li></li>");
var p=l===0?d:q.title;
if(q.active){o.text(p).addClass("active")
}else{var j=f("<a></a>").text(p).attr("href",q.url);
o.append(j)
}f(g).append(o)
}};
e.Util.listenTo("crumbs",a)
})($CQ,SCF);