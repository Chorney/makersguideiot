(function(b,c){var a=b.Model.extend({modelName:"SignUpModel",createUser:function(k){var j=_.bind(function(){this.trigger("signup:success",{model:this})
},this);
var h=_.bind(function(l){this.trigger("signup:error",l.responseJSON.error)
},this);
if(this.get("allowCaptcha")){var i=c.grep(k,function(m,l){return m.name===":cq:captcha"
});
var f=c.grep(k,function(m,l){return m.name===":cq:captchakey"
});
if(_.isEmpty(i[0].value)||_.isEmpty(f[0].value)){var e={message:"Missing Captcha"};
this.trigger("signup:error",e);
return
}i[0].name="captcha";
f[0].name="captchakey"
}var g=k;
g.push({name:":operation",value:"social:createTenantUser"});
g.push({name:"_charset_",value:"UTF-8"});
$.ajax(b.config.urlRoot+this.get("id")+b.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:g,success:j,error:h})
},resendEmail:function(h){var g=_.bind(function(){this.trigger("resend:success",{model:this})
},this);
var f=_.bind(function(i){this.trigger("resend:error",i.responseJSON.error)
},this);
var e=h;
e.push({name:":operation",value:"social:resendEmailValidation"});
$.ajax(b.config.urlRoot+this.get("id")+b.constants.URL_EXT,{dataType:"json",type:"POST",xhrFields:{withCredentials:true},data:e,success:g,error:f})
}});
var d=b.View.extend({viewName:"SignUp",tagName:"div",className:"scf-signup-container",tempRatingBarWidth:null,userNameContainerSel:".scf-username-container",init:function(){this.listenTo(this.model,"signup:success",this.update);
this.listenTo(this.model,"signup:error",this.showError);
this.$el.find(".form_captcha_refresh input").val(CQ.I18n.get("Refresh"));
this.listenTo(this.model,"resend:success",this.resendSuccess);
this.listenTo(this.model,"resend:error",this.resendError)
},showError:function(e){this.log.error(e);
c(".scf-js-signup-error").text(CQ.I18n.get(e.message)).show()
},update:function(){this.log.info("registration successful");
c(".scf-js-signup-form").hide();
c(".scf-js-signup-success").show()
},register:function(f){f.preventDefault();
if(this.comparePasswordFileds()){c(".scf-js-signup-error").text("").hide();
var g=c(".scf-js-signup-form").serializeArray();
this.model.createUser(g)
}else{c(".scf-js-signup-error").text(CQ.I18n.get("Password fileds do not match")).show()
}return false
},confirmPassword:function(g){g.preventDefault();
var f=c(g.target).parents(".form-group")[0];
if(this.comparePasswordFileds()){c(f).removeClass("has-error")
}else{c(f).addClass("has-error")
}},comparePasswordFileds:function(){return c(".scf-js-password").val()===c(".scf-js-confirm-password").val()
},toggleUserName:function(g){var f=g.target;
if(c(f).is(":checked")){c(this.userNameContainerSel).find('input[type="text"]').val("");
c(this.userNameContainerSel).removeClass("required").hide()
}else{c(this.userNameContainerSel).addClass("required").show()
}},resend:function(f){f.preventDefault();
c(".scf-js-resend-error").text("").hide();
var g=c(".scf-js-resend-form").serializeArray();
this.model.resendEmail(g);
return false
},resendError:function(e){this.log.error(e);
c(".scf-js-resend-error").text(e.message).show()
},resendSuccess:function(){this.log.info("resend email successful");
c(".scf-js-resend-form").hide();
c(".scf-js-resend-success").show()
}});
b.SignUp=a;
b.SignUpView=d;
b.registerComponent("social/console/components/hbs/signup",b.SignUp,b.SignUpView)
})(SCF,$CQ);