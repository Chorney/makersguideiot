var SCFValidator=function(c,b){var a=this;
this.items=[];
if(c!==null){this.context=c
}else{this.context=$(document.body)
}this.batch_processing=false;
var d=$(this.context).attr("data-scf-validator-batch-processing");
if(typeof(d)!=="undefined"&&d==="true"){this.batch_processing=true
}this.elements=$("[data-scf-validator]");
$.each(this.elements,function(e,f){if($($(f).closest("form")).is(a.context)){a.items.push(new SCFValidatorItem(f,a.batch_processing))
}})
};
SCFValidator.prototype.validate=function(){var b=[];
$.each(this.items,function(c,d){b.push(d.validate())
});
var a=function(d,c,e){return d===true
};
return b.every(a)
};
SCFValidator.prototype.reset=function(){$.each(this.items,function(a,b){b.reset()
})
};
var SCFValidatorItem=function(b,d){var a=this;
this.element=b;
var c=$(this.element).data("scf-validator");
if(baseRules[c.validation]!==null){this.validation=baseRules[c.validation].regexp;
this.message=baseRules[c.validation].text
}else{this.validation=baseRules["default"].regexp;
this.message=baseRules["default"].text
}this.callbacks={init:null,success:null,error:null,complete:null};
this.evt="change";
this.resetevt="focus";
this.notification="highlight";
this.notification_str="";
this.error_element=null;
this.anchor_element=null;
this.error_css=null;
if(typeof(c.evt)!=="undefined"){this.evt=c.evt
}if(typeof(c.resetevt)!=="undefined"){this.resetevt=c.resetevt
}if(typeof(c.notification)!=="undefined"){this.notification=c.notification
}if(typeof(c.callbacks)!=="undefined"&&typeof(c.callbacks)==="object"){this.callbacks=c.callbacks
}if(this.callbacks.init!==null){window[this.callbacks.init].call()
}if(typeof(this.notification)==="object"){this.anchor_element=$("#"+this.notification.anchor);
this.error_css=this.notification.css;
this.notification_str="custom"
}else{this.anchor_element=this.element;
this.notification_str=this.notification;
this.error_css="scf-validation-highlight-error"
}$(this.element).on(a.resetevt,function(){a.reset()
});
if(d===false){$(this.element).on(a.evt,function(){if(a.validate()){if(a.callbacks.success!==null){window[a.callbacks.success].call()
}}else{if(a.callbacks.error!==null){window[a.callbacks.error].call()
}}if(a.callbacks.complete!==null){window[a.callbacks.complete].call()
}})
}return this
};
SCFValidatorItem.prototype.validate=function(){if($(this.element).hasClass(this.error_css)){this.reset()
}var a=$(this.element).val();
var b=new RegExp(this.validation);
if(b.test(a)){return true
}$(this.element).addClass(this.error_css);
this.error_element=$('<div class="alert alert-danger">'+this.message+"</div>");
switch(this.notification_str){case"highlight":$(this.error_element).insertAfter($(this.anchor_element));
break;
case"popup":alert(this.message);
break;
case"custom":$(this.error_element).appendTo($(this.anchor_element));
break;
default:alert("Validation error message")
}return false
};
SCFValidatorItem.prototype.reset=function(){$(this.element).removeClass(this.error_css);
if(this.error_element!==null){$(this.error_element).remove()
}};
var baseRules={"default":{text:CQ.I18n.getMessage("Default match. Matches everything."),regexp:"[sS]*"},integer:{text:CQ.I18n.getMessage("Integer only"),regexp:/^\s*(\+|-)?\d+\s*$/},text:{text:CQ.I18n.getMessage("Text only. No digits"),regexp:"^[a-zA-Z ]*$"},numbers:{text:CQ.I18n.getMessage("Numbers only. No special chars, or letters"),regexp:"^[0-9]*$"},messagesubject:{text:CQ.I18n.getMessage("Please enter min 2, max 10, numbers, letters and/or some characters"),regexp:"^([a-zA-Z0-9 \"~&|'!@#$%*()-_=+;?/{}]{2,10})$"},email:{text:CQ.I18n.getMessage("Please enter valid email address"),regexp:/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i},noempty:{text:CQ.I18n.getMessage("Cannot be empty"),regexp:/\S/}};
(function(h,f,b,g){var d={VERSION:"0.0.1",Views:{},Models:{},Collections:{},config:{urlRoot:""},constants:{SOCIAL_SELECTOR:".social",JSON_EXT:".json",URL_EXT:".social.json",TRANSLATE_URL_EXT:".social.translate.json",ANALYTICS_BASE_RESOURCE_TYPE:"social/commons/components/analyticsbase"},Components:{},loadedComponents:{},templates:{},fieldManagers:{},DEBUG:1,INFO:2,WARN:3,ERROR:4};
d.LOG_LEVEL=d.INFO;
var e={debug:function(){if(d.LOG_LEVEL<=d.DEBUG){window.console.debug.apply(window.console,arguments)
}},info:function(){if(d.LOG_LEVEL<=d.INFO){window.console.info.apply(window.console,arguments)
}},warn:function(){if(d.LOG_LEVEL<=d.WARN){window.console.warn.apply(window.console,arguments)
}},error:function(){if(d.LOG_LEVEL<=d.ERROR){window.console.error.apply(window.console,arguments)
}}};
var c=function(k,j){var l=k.firstChild;
var i=null;
while(l){switch(l.nodeType){case 1:i=c(l,j);
break;
case 8:if(l.nodeValue.match(j)){return l
}break
}if(i!==null){break
}l=l.nextSibling
}return i
};
d.Router=new h.Router();
h.history.start({pushState:true,hashChange:false});
d.View=h.View.extend({initialize:function(){this._rendered=false;
this._childViews={};
this._parentView=undefined;
this._modelReady=false;
this._sessionReady=false;
this._renderedChildren=[];
this._replacementTarget=null;
this._destroyed=false;
if(this.$el.html()!==""){this.bindView();
this._rendered=true
}this.listenTo(this.model,"model:loaded",function(){this._modelReady=true;
this.render()
});
this.listenTo(this.model,"model:cacheFixed",function(){this.render()
});
if(this.requiresSession&&!d.Session.isReady()){this.listenTo(d.Session,"model:loaded",function(i){if(!this._sessionReady){this._sessionReady=true;
this.render()
}})
}this._sessionReady=d.Session.isReady();
if(b.isFunction(this.init)){this.init.apply(this,arguments)
}if(d.Session.isReady()){this.initContext()
}else{d.Session.on("model:loaded",b.bind(this.initContext,this))
}},initContext:function(){if(b.isUndefined(d.Context)){d.Context={};
var i=".scf-js-group-navbar";
var j=$(".scf-js-site-title").attr("href");
j=b.isUndefined(j)?"":j.substring(0,j.lastIndexOf(".html"));
this.sitePath=j;
d.Context.siteTitle=$(".scf-js-site-title").length!==0?$(".scf-js-site-title").text():"";
d.Context.sitePath=this.sitePath;
d.Context.groupTitle=$(i).length!==0&&!b.isUndefined($(i).attr("data-group-title"))?$(i).data("group-title"):"";
d.Context.groupPath=$(i).length!==0&&!b.isUndefined($(i).attr("data-group-path"))?$(i).data("group-path"):"";
d.Context.user=d.Session.get("authorizableId")
}if(b.isFunction(this.initAnalytics)){this.initAnalytics.apply(this,arguments)
}},getContextForTemplate:function(){var i=(this.model!==undefined)?this.model.toJSON():this.context;
return this.getMergedContexts(i)
},getMergedContexts:function(i){if(!b.isObject(i)){i={}
}i.loggedInUser=d.Session.toJSON();
i.environment={};
i.environment.client=true;
return i
},appendTo:function(i){if(!this._rendered){this.render()
}f(i).append(this.el);
this.trigger("view:ready",{view:this})
},replaceElement:function(i){if(!this._rendered){this.render()
}if(this._rendered){f(i).replaceWith(this.$el);
this._replacementTarget=null;
this.trigger("view:ready",{view:this})
}else{this._replacementTarget=i
}},render:function(){if(this._destroyed){return
}var k=this;
if(!(this._modelReady||this.model._isReady)||(this.requiresSession&&!this._sessionReady)){return this
}this.unbindDataFields();
for(var l in this._childViews){this._childViews[l].destroy();
delete this._childViews[l]
}this._renderedChildren=[];
var i=f(this.template(this.getContextForTemplate(),{data:{parentView:this}}));
if(this._rendered||this.$el.parent().length>0){this.$el.html(i.html())
}else{this.setElement(i)
}b.each(this._childViews,function(m){k.renderChildView(m)
});
var j=b.bind(function(){this.bindView();
this._rendered=true;
if(this.afterRender){this.afterRender()
}this.trigger("view:rendered",{view:this})
},this);
f.when(this._renderedChildren).done(j);
if(this._replacementTarget!==null){this.replaceElement(this._replacementTarget)
}return this
},bindView:function(){var i=this;
this.unbindDataFields();
this.$("[evt]").not(this.$("[data-scf-component] [evt]")).each(function(j,k){d.View.bindEvents(i,f(k))
});
this.$("[data-attrib]").not(this.$("[data-scf-component] [data-attrib]")).each(function(j,k){d.View.bindDataFields(i,f(k))
});
this.$("[data-form]").not(this.$("[data-scf-component] [data-form]")).each(function(j,k){d.View.bindDataForms(i,f(k))
})
},addChildView:function(j){this._childViews[j.cid]=j;
var i=f.Deferred();
this._renderedChildren[j.cid]=i.promise();
this.listenTo(j,"view:rendered",function(){i.resolve()
});
this.listenTo(j,"view:destroyed",function(k){this.removeChildView(k.view.cid)
});
return this
},getChildView:function(i){return this._childViews[i]
},removeChildView:function(j){if(this._renderedChildren.hasOwnProperty(j)){this._renderedChildren[j].fail()
}if(this._childViews.hasOwnProperty(j)){var i=this._childViews[j];
i.stopListening();
this.stopListening(i,"view:rendered");
this._childViews[j]=undefined;
delete this._childViews[j]
}return this
},getChildViews:function(){return this._childViews
},setParent:function(i){this._parentView=i;
i.addChildView(this);
return this
},renderChildView:function(j){j.render();
var n=this;
if(n.el===null){return
}var m=null;
var l=null;
var k=new RegExp("s*?data-view='"+j.cid+"'");
if(document.createNodeIterator&&NodeFilter&&NodeFilter.SHOW_COMMENT){var i=document.createNodeIterator(n.el,NodeFilter.SHOW_COMMENT,function(o){if(o.data.match(k)){return NodeFilter.FILTER_ACCEPT
}},false);
l=i.nextNode();
while(l!==null){m=l;
l=i.nextNode();
break
}j.replaceElement(m)
}else{m=c(n.el,k);
j.replaceElement(m)
}},getField:function(j){var i=this._fields[j];
if(i){return i.val()
}return""
},setField:function(k,j){var i=this._fields[k];
if(!i){return
}i.val(j)
},focus:function(j){var i=this._fields[j];
if(!i){return
}i.focus()
},getForm:function(i){if(typeof this._forms==="undefined"){return null
}else{return this._forms[i]
}},destroy:function(){this.undelegateEvents();
this.unbindDataFields();
this.stopListening();
this.trigger("view:destroyed",{view:this});
this._destroyed=true
},unbindDataFields:function(){for(var i in this._fields){if(this._fields.hasOwnProperty(i)){if(b.isFunction(this._fields[i].destroy)){this._fields[i].destroy()
}}}this._fields={}
},log:e});
d.View.extend=function(){var j=h.View.extend.apply(this,arguments);
var i=arguments[0].viewName;
d.Views[i]=j;
return j
};
d.Model=h.Model.extend({_cachedModels:{},_hasLoadedChildren:false,parse:function(i){this._parseRelations(i);
return i
},addEncoding:function(i){if((window.FormData)&&(i instanceof window.FormData)){i.append("_charset_","UTF-8")
}if(!i.hasOwnProperty("_charset_")){i._charset_="UTF-8"
}return i
},reload:function(l){this._isReady=false;
var j="";
var i;
if(b.isFunction(this.url)){j=this.url();
i=this.url
}else{j=this.url
}this.clear();
if(!b.isEmpty(j)){this.url=j
}var k=this;
this.fetch({dataType:"json",xhrFields:{withCredentials:true},error:function(n,m){d.log.error("Error fetching model");
d.log.error(m);
n.clear();
n._isReady=true;
n.trigger("model:loaded",n);
if(l&&typeof(l.error)==="function"){l.error()
}},success:function(m){if(i!==undefined){m.url=i
}m._isReady=true;
m.trigger("model:loaded",m);
if(l&&typeof(l.success)==="function"){l.success()
}}})
},reset:function(j,k){this.clear().set(b.clone(this.defaults));
var i=this._parseRelations(j);
this.set(i,k);
return this
},initialize:function(i){this.listenTo(d.Session,"logout:success",function(){this.reload()
});
this.listenTo(d.Session,"login:success",function(){this.reload()
})
},constructor:function(j,k){var i=this._parseRelations(j);
h.Model.apply(this,[i,k])
},url:function(){var i;
if(this.urlRoot){i=this.urlRoot+this.id+d.constants.URL_EXT
}else{if(d.config.urlRoot){i=d.config.urlRoot+this.id+d.constants.URL_EXT
}else{i=this.id+d.constants.URL_EXT
}}return i
},_parseRelations:function(i){var j=b.bind(function(n,r){if(!i[r]&&!n.path){i[r]=[]
}if(i[r]||n.path){var m=i[r];
var s,p;
if(b.isArray(m)){var k=[],t=[];
b.each(m,function(u){if(b.isObject(u)){s=!b.isUndefined(d.Models[n.model])?d.Models[n.model]:d.Components[u.resourceType].Model;
p=s.findLocal(u.id);
if(!p){p=s.createLocally(u)
}else{p.reset(u)
}k.push(p)
}else{if(!b.isEmpty(u)){var v=u.substr(d.config.urlRoot.length);
v=v.substr(0,v.lastIndexOf(d.constants.URL_EXT));
s=d.Models[n.model];
p=s.findLocal("idFromUrl");
if(!p){p=n.autofetch?s.find(v):new s({url:u})
}s.prototype._cachedModels[v]=p;
k.push(p)
}}});
var o=d.Collections[n.collection]||h.Collection;
var q=new o();
q.model=s;
q.parent=this;
q.set(k,{silent:true});
i[r]=q
}else{if(b.isObject(m)){if(b.isUndefined(d.Models[n.model])&&b.isUndefined(d.Components[m.resourceType])){this.log.error("A relation key %s requested model %s but it is not available nor is the component type: %s",r,n.model,m.resourceType);
return
}s=d.Models[n.model]||d.Components[m.resourceType].Model;
p=s.findLocal(m.id)||s.createLocally(m);
i[r]=p
}else{var l=m;
if(!l){if(n.path){if(n.path.substr(0,1)==="/"){l=n.path
}else{l=d.config.urlRoot+i.id+"/"+n.path+d.constants.URL_EXT
}}else{return
}}s=d.Models[n.model];
if(n.autofetch){p=s.find(l,undefined,true)
}else{p=s.findLocal(l,true)||new s({url:l})
}i[r]=p
}}}},this);
b.each(this.relationships,j);
return i
},toJSON:function(){var i=h.Model.prototype.toJSON.apply(this);
b.each(this.relationships,function(j,k){var m=i[k];
if(m.length<=0){delete i[k];
return
}if(b.isArray(m)){var l=[];
b.each(m,function(n){if(n instanceof h.Model){l.push(n.toJSON())
}else{l.push(n)
}});
i[k]=l
}else{if(m instanceof h.Collection){i[k]=m.toJSON()
}else{if(m instanceof h.Model){i[k]=m.toJSON()
}}}});
return i
},log:e});
d.Model.extend=function(){var j=h.Model.extend.apply(this,arguments);
var i=arguments[0].modelName;
d.Models[i]=j;
return j
};
d.View.bindEvents=function(i,j){var k=j.attr("evt");
b.each(k.split(","),function(o){var p=o.split("=");
var l=f.trim(p[0]);
var n=f.trim(p[1]);
if(i[n]){var m=b.bind(i[n],i);
j.off(l);
j.on(l,m)
}})
};
d.View.bindDataFields=function(k,m){var n=m.attr("data-attrib");
if(!k._fields){k._fields={}
}if(!b.isUndefined(k._fields[n])){return
}var j=m.attr("data-field-type");
var i=(b.isUndefined(d.fieldManagers[j]))?a:d.fieldManagers[j];
var l=new i(m,{},k.model);
k._fields[n]=(function(){return{val:function(){if(arguments.length===0){return l.getValue()
}else{return l.setValue(arguments[0])
}},focus:function(){return l.focus()
},destroy:function(){if(b.isFunction(l.destroy)){l.destroy()
}}}
})()
};
d.View.bindDataForms=function(i,j){var k=j.attr("data-form");
if(!i._forms){i._forms={}
}i._forms[k]=new SCFValidator($(j),false)
};
d.Model.findLocal=function(i,j){var k=j?i.substr(d.config.urlRoot.length):i;
if(this.prototype._cachedModels&&this.prototype._cachedModels[k]){return this.prototype._cachedModels[k]
}};
d.Model.createLocally=function(i){var j=new this.prototype.constructor(i);
j._isReady=true;
this.prototype._cachedModels[j.get("id")]=j;
return j
};
d.Model.prototype.load=function(i){if(i){this.set({id:i},{silent:true})
}this.fetch({success:function(j){j._isReady=true;
j.trigger("model:loaded",j)
},xhrFields:{withCredentials:true}})
};
d.Model.prototype.getConfigValue=function(j){var i=this.get("configuration");
if(!b.isEmpty(i)){return i[j]
}return null
};
d.Model.prototype.destroy=function(j){var i=this;
this.constructor.prototype._cachedModels[i.get("id")]=undefined;
i.trigger("destroy",i,i.collection,j)
};
d.Model.prototype.parseServerError=function(l,k,j){var i=f.parseJSON(l.responseText);
if(i.hasOwnProperty("status.code")){i.status=i.status||{};
i.status.code=i["status.code"];
delete i["status.code"]
}if(i.hasOwnProperty("status.message")){i.status=i.status||{};
i.status.message=i["status.message"];
delete i["status.message"]
}return{error:j,details:i}
};
d.Model.find=function(j,n,m){var l=this;
if(this.prototype._cachedModels&&this.prototype._cachedModels[j]){var i=this.prototype._cachedModels[j];
if(b.isFunction(n)){n(i)
}return i
}else{var k=new this.prototype.constructor({id:j});
if(m){k.url=j
}this.prototype._cachedModels[j]=k;
k.fetch({dataType:"json",xhrFields:{withCredentials:true},error:function(p,o){if(o.status===204||o.status===404){d.log.debug("non existing resource");
p._isReady=true;
p.trigger("model:loaded",p);
if(b.isFunction(n)){n(p)
}}else{d.log.error("Error fetching model");
d.log.error(o);
l.prototype._cachedModels[j]=undefined
}},success:function(o){o._isReady=true;
o.trigger("model:loaded",o);
if(b.isFunction(n)){n(o)
}}});
return k
}};
d.Collection=h.Collection.extend({});
d.Collection.extend=function(){var j=h.Collection.extend.apply(this,arguments);
var i=arguments[0].collectioName;
d.Collections[i]=j;
return j
};
d.registerComponent=function(j,k,i){d.Components[j]={Model:k,View:i,name:j}
};
d.addLoadedComponent=function(k,j,i){if(!d.Components[k]){return
}if(!d.loadedComponents[k]){d.loadedComponents[k]={}
}d.loadedComponents[k][j.id]={model:j,view:i};
return d.loadedComponents[k][j.id]
};
d.findTemplate=function(m,i,k){if(arguments.length==2){k=i;
i=""
}var l=k+"/"+i;
if(d.templates[l]){return d.templates[l]
}var j;
f.ajax({async:false,url:d.config.urlRoot+"/services/social/templates?resourceType="+k+"&ext=hbs&selector="+i}).done(function(o,n){if(n=="success"){j=g.compile(o);
d.templates[l]=j
}});
return j
};
d.log=e;
d.registerFieldType=function(i,j){if(!(b.isFunction(j.prototype.setValue))){this.log.error('%s does not implement required method, "setValue"',i);
return
}if(!(b.isFunction(j.prototype.getValue))){this.log.error('%s does not implement required method, "getValue"',i);
return
}if(!(b.isFunction(j.prototype.focus))){this.log.error('%s does not implement required method, "focus"',i);
return
}if(!(b.isFunction(j.prototype.destroy))){this.log.error('%s does not implement required method, "destroy"',i);
return
}this.fieldManagers[i]=j
};
var a=function(k,j,i){this.$el=k
};
a.prototype.setValue=function(i){return this.$el.val(i)
};
a.prototype.getValue=function(){return this.$el.val()
};
a.prototype.focus=function(){this.$el.focus()
};
a.prototype.destroy=function(){};
d.View.prototype.launchModal=function(m,n,l){var o=f('<div class="scf scf-modal-screen"></div>');
var k=f('<div class="scf scf-modal-dialog" style="display:none;"><h2 class="scf-modal-header">'+n+'</h2><div class="scf-modal-close">X</div></div>');
var j=m;
var p=j.parent();
k.append(j);
j.show();
var r=function(s){if(d.Util.mayCall(s,"preventDefault")){s.preventDefault()
}j.hide();
p.append(j);
o.remove();
k.remove();
if(b.isFunction(l)){l()
}};
k.find(".scf-modal-close").click(r);
k.find(".scf-js-modal-close").click(r);
f("body").append(o);
f("body").append(k);
var i=(window.innerWidth-k.innerWidth())/2;
var q=(window.innerHeight-k.innerHeight())/2;
k.css({top:q,left:i});
k.show();
return r
};
d.View.prototype.overlayTemplate='<div class="scf-overlay"><div class="scf-overlay-header btn-toolbar"><button class="btn btn-primary scf-ovelay-back-button" title="{{i18n "Back"}}"><span class="scf-icon-left"></span></button><h3>{{header}}</h3></div></div>';
d.View.prototype.loadOverlay=function(j,l,o,n){var k=g.compile(this.overlayTemplate);
var i=f(k({header:o}));
var m=function(){i.remove();
l.find(".scf-is-overlay-hidden").each(function(){f(this).removeClass("scf-is-overlay-hidden")
});
if(n&&b.isFunction(closeCallBack)){closeCallBack()
}};
l.children().each(function(){f(this).addClass("scf-is-overlay-hidden")
});
i.append(j);
l.append(i);
i.find(".scf-ovelay-back-button").click(m);
return m
};
d.View.prototype.errorTemplate="<h3>{{details.status.message}}</h3>";
d.View.prototype.addErrorMessage=function(l,j){var m=g.compile(this.errorTemplate);
var k=f(l);
var i=f(m(j));
i.addClass("scf-js-error-message");
k.before(i)
};
d.View.prototype.compileTemplate=function(i){return g.compile(i)
};
d.View.prototype.clearErrorMessages=function(j,i){this.$el.find(".scf-js-error-message").remove();
this.$el.find(".scf-error").removeClass("scf-error")
};
d.ChildView=d.View.extend({bindView:function(){},bindDataForms:function(){},bindDataFields:function(){},bindEvents:function(){},viewName:"ChildView"});
d.Util={mayCall:function(j,i){if(b.isUndefined(j)||b.isNull(j)){return false
}return(j.hasOwnProperty(i)||j[i]!==null)&&b.isFunction(j[i])
},announce:function(i,j){f(document).trigger(i,j)
},listenTo:function(i,j){f(document).on(i,function(l,k){j(k)
})
},startsWith:function(j,i){return j.substr(0,i.length)===i
},getContextPath:function(){var i=CQ.shared.HTTP.getPath();
var j=CQ.shared.HTTP.getExtension();
var k=i.split(j);
if(k&&k!==undefined){if(k.length>1){return k[1]
}else{return k[0]
}}return""
}};
window.SCF=d
})(Backbone,$CQ,_,Handlebars);
(function(g,f,c,d,b,e){var a=function(l,h,k){var i="";
var m={resourcePath:l};
if(k){c.log.warn("Forcing resource type is not supported when sling including on the client side")
}if(h){m.selector=h
}var j=c.config.urlRoot+l;
j+=h?"."+h+".html":".html";
d.ajax({async:false,url:j}).done(function(o,n){if(n=="success"){i=o
}});
return new g.SafeString(i)
};
g.registerHelper("include",function(h,i){if(arguments.length===1){i=h;
h=undefined
}var j=i.data.parentView;
var D=function(F){if(!F){return undefined
}var E=F.lastIndexOf("View");
if(E!==-1){return F.substr(0,E)+"Model"
}else{return F+"Model"
}};
var A=b.isUndefined(i.hash.bind)?true:i.hash.bind;
var z=i.hash.view;
var m=i.hash.template;
var n=i.hash.resourceType;
var v=i.hash.path;
var u=i.hash.model||D(z);
var s,k,w,C,x,p;
if(b.isObject(h)){n=n||h.resourceType;
p=c.Components[n];
if((b.isUndefined(p))&&(n.match(/^(\/apps)|(\/libs)/))){var q=n.substring(6);
p=c.Components[q]
}var r;
x=h.id;
if(!x){var l=h.url;
if(!l){c.log.warn("No resource id found for context: ");
c.log.warn(h)
}var y=l.substr(c.config.urlRoot.length);
y=y.substr(0,y.lastIndexOf(c.constants.URL_EXT));
x=y
}if(m){r=c.findTemplate(x,m,n)
}else{r=c.findTemplate(x,n)
}var B=function(){if(j.model.get("resourceType")===n&&j.model.id===x){return c.ChildView
}return p?p.View:undefined
};
w=z?c.Views[z]:B();
w=A?w:undefined;
C=u?c.Models[u]:p?p.Model:undefined;
C=A?C:undefined;
if(!w&&!r){if(x){return a(x,m,n)
}c.log.error("No view or template found for "+n+" and template "+m);
return""
}if(!w&&r){return new g.SafeString(r(c.View.prototype.getMergedContexts(h)))
}if(w&&!r){c.log.error("No template found for "+n+" and template "+m);
return""
}if(!C||!x){s=new w({context:h})
}else{k=C.findLocal(x);
if(!k){k=C.createLocally(h)
}if(k.isNew()){k.load(x)
}s=new w({model:k})
}if(m&&r){s.template=r
}else{if(r){w.prototype.template=r
}}}else{var t=v?v.slice(0,1)==="/":false;
if(!h&&!t){c.log.error("Must provide context path when including "+n);
return""
}x=t?v:h+"/"+v;
if(n){p=c.Components[n]
}if(A&&(p||(z&&u))){w=!p?c.Views[z]:p.View;
C=!p?c.Models[u]:p.Model
}if(w&&C){var o=x.indexOf("http://")===0;
k=C.find(x,undefined,o);
s=new w({model:k});
if(m){s.template=c.findTemplate(x,m,n)
}else{if(typeof s.template==="undefined"){c.log.info("Getting default template for "+n);
s.template=c.findTemplate(x,n,n)
}}}else{return a(x,m,n)
}}s.setParent(j);
if(!w.prototype.template&&s.template&&w!=c.ChildView){w.prototype.template=c.findTemplate(k.get("id"),n)
}s.templateName=m||"default";
s.resource=x;
return new g.SafeString("<!-- data-view='"+s.cid+"'-->")
});
g.registerHelper("equals",function(h,j,i){if(arguments.length<3){throw new Error("Handlebars Helper equal needs 2 parameters")
}if(h!=j){return i.inverse(this)
}else{return i.fn(this)
}});
g.registerHelper("lastPath",function(j,i){var h=j.lastIndexOf("/");
return j.slice(h+1)
});
g.registerHelper("pretty-time",function(i,q){if(!i){return""
}var l=new Date(i);
var j=new Date();
var p=j.getTime()-l.getTime();
var k=1000;
var m=k*60;
var n=m*60;
var o=n*24;
f.locale(e.shared.I18n.getLocale());
var h=q.hash.daysCutoff?q.hash.daysCutoff:60;
if(p<m){l=Math.round(p/k)+"";
if(l==1){return new g.SafeString(e.I18n.get("{0} second ago",l))
}return new g.SafeString(e.I18n.get("{0} seconds ago",l))
}else{if(p<n){l=Math.round(p/m);
if(l==1){return new g.SafeString(e.I18n.get("{0} minute ago",l))
}return new g.SafeString(e.I18n.get("{0} minutes ago",l))
}else{if(p<o){l=Math.round(p/n);
if(l==1){return new g.SafeString(e.I18n.get("{0} hour ago",l))
}return new g.SafeString(e.I18n.get("{0} hours ago",l))
}else{if(p<o*h){l=Math.round(p/o);
if(l==1){return new g.SafeString(e.I18n.get("{0} day ago",l))
}return new g.SafeString(e.I18n.get("{0} days ago",l))
}else{return new g.SafeString(f(l).format(e.I18n.get("MMM DD YYYY, h:mm A",null,"moment.js, communities moderation")))
}}}}});
g.registerHelper("pages",function(h,r){var p=h;
if(p.hasOwnProperty("selectedPage")&&p.hasOwnProperty("totalPages")&&p.hasOwnProperty("pageSize")&&p.hasOwnProperty("basePageURL")){var j="";
if(p.totalPages<=1){return j
}var n=Math.abs(p.pageSize);
var o=(p.orderReversed)?"-":"";
var l=p.selectedPage;
var m=l;
if((m-2)>0&&p.totalPages>5){m=m-2
}if(p.totalPages<=5){m=1
}else{if(p.totalPages-l<2){m=p.totalPages-4
}}var q=m+5;
if(q>p.totalPages){q=p.totalPages+1
}for(var k=m;
k<q;
k++){p.pageNumber=k;
p.currentPageUrl=p.basePageURL+"."+((k-1)*n)+"."+o+n+".html";
p.currentPage=k==p.selectedPage;
p.suffix=((k-1)*n)+"."+o+n;
j+=r.fn(p)
}return j
}else{return""
}});
g.registerHelper("loadmore",function(k,j){var m=k.pageInfo;
var h=k.items;
if(!k.totalSize||!m){return""
}if(!(!b.isUndefined(m.selectedPage)&&k.totalSize&&m.pageSize)){return""
}if(k.totalSize<=0){return""
}var n={};
n.suffix=m.nextSuffix;
var l=this.totalSize;
if(!b.isUndefined(h)){l=l-h.length
}if(l===0){return""
}var i=m.nextPageURL;
if(!b.isUndefined(i)&&i.indexOf(".json",i.length-5)!==-1){i=i.substr(0,i.length-5);
i+=".html"
}n.remaining=l;
n.moreURL=i;
return j.fn(n)
});
g.registerHelper("dateUtil",function(j,i){var h=j;
var l=i.hash.format;
var k=i.hash.timezone;
if(!h||typeof h!="number"){h=new Date().getTime()
}else{h=new Date(h)
}l=l.replace(/y/g,"Y");
l=l.replace(/\bdd\b/gi,"DD");
l=l.replace(/\bd\b/gi,"D");
l=l.replace(/\bEEEE\b/gi,"dddd");
f.locale(e.shared.I18n.getLocale());
if(k&&f.tz){return new g.SafeString(f.tz(h,k).format(l))
}return new g.SafeString(f(h).format(l))
});
g.registerHelper("i18n",function(j,i){if(arguments.length>1){var h=b.rest(arguments);
return e.I18n.get(j,h)
}else{return e.I18n.get(j)
}});
g.registerHelper("xss-htmlAttr",function(j,i){var h=d("div");
h.attr("data-xss",j);
var k=h.attr("data-xss");
return e.shared.XSS.getXSSValue(k)
});
g.registerHelper("xss-jsString",function(i,h){return e.shared.XSS.getXSSValue(i)
});
g.registerHelper("xss-html",function(i,h){return d("<div/>").text(i).html()
});
g.registerHelper("xss-validHref",function(i,h){return encodeURI(i)
});
g.registerHelper("dom-id",function(i,h){if(!i){return""
}var j=d.trim(i);
j=j.replace(/\./g,"-");
j=j.replace(/\//g,"-");
j=j.replace(/:/g,"-");
return j
});
g.registerHelper("abbreviate",function(h,q){if(!h){return""
}var k=q.hash.maxWords;
var i=q.hash.maxLength;
var j=q.hash.safeString;
var p=d.trim(h);
var n=p.length;
var l=p.substring(0,i).split(" ");
var m=l.slice(0,l.length>k?k:l.length).join(" ");
var o=n!=m.length&&q.fn?q.fn(this):"";
if(j){return new g.SafeString(m)+o
}return m+o
});
g.registerHelper("includeClientLib",function(i,h){return""
});
g.registerHelper("if-wcm-mode",function(i,h){return""
});
g.registerHelper("getContextPath",function(j,i){var h="";
if(Granite&&Granite.HTTP.getContextPath()){h=Granite.HTTP.getContextPath()
}return h
})
})(Handlebars,moment,SCF,$CQ,_,CQ);
(function(f,e,b,g,d){var c=e.View.extend({viewName:"Login",tagName:"div",className:"scf-login",init:function(){this._isReady=false;
this.listenTo(this.model,this.model.events.LOGGED_IN_SUCCESS,this.render);
this.listenTo(this.model,this.model.events.LOGGED_OUT,this.render)
},loginAction:function(){if(this.model.get("loggedIn")){this.$el.children(".login-dialog").hide();
this.logout()
}else{var h=this.$el.children(".login-dialog").toggle();
h.find("input:first").focus()
}},logout:function(){this.model.logout()
},login:function(){var i=this.getField("username");
var h=this.getField("password");
if(i===""||h===""){return
}this.model.login(i,h)
}});
var a=e.Model.extend({moderatorCheckAttribute:"moderatorCheck",events:{LOGGED_IN_SUCCESS:"login:success",LOGGED_IN_FAIL:"login:failed",LOGGED_OUT:"logout:success"},initialize:function(h,i){this._isReady=false;
if(g.shared.User.data===undefined||g.shared.User.data===null){this.initUser(i)
}else{this.getLoggedInUser(i)
}},defaults:{loggedIn:false},isReady:function(){return this._isReady
},checkIfModeratorFor:function(i){var h=this.attributes.hasOwnProperty(this.moderatorCheckAttribute)?this.attributes[this.moderatorCheckAttribute]:[];
return this.attributes.loggedIn&&b.contains(h,i)
},checkIfUserCanPost:function(i){var h=this.attributes.hasOwnProperty("mayPost")?this.attributes.mayPost:[];
return this.attributes.loggedIn&&b.contains(h,i)
},setLanguage:function(h){var i=h.preferences&&h.preferences.language?h.preferences.language:"en";
var j=document.documentElement.lang||i;
g.shared.I18n.setLocale(j)
},initUser:function(h){var j=g.shared.HTTP.externalize("/libs/granite/security/currentuser"+g.shared.HTTP.EXTENSION_JSON+"?props=preferences/language");
j=g.shared.HTTP.noCaching(j);
var i=this;
f.ajax({url:j,type:"GET",success:function(k){i.getLoggedInUser(h,k.home);
i.setLanguage(k)
},async:false})
},getLoggedInUser:function(i,h){var j=this;
var l;
if(i.hasOwnProperty(a.moderatorCheckAttribute)){l="?"+a.moderatorCheckAttribute+"=";
b.each(i[a.moderatorCheckAttribute],function(n){l+=n+","
});
l=l.substring(0,l.length-1)
}var m="";
if(h!==undefined){m=h
}else{if(g.shared.User.initialized){m=g.shared.User.data.home
}else{var k=g.shared.User.init();
m=g.shared.User.data.home
}}f.ajax({url:e.config.urlRoot+"/services/social/getLoggedInUser"+l+"&h="+m,xhrFields:{withCredentials:true},type:"GET"}).done(function(n){if(n.name){j.set({loggedIn:true});
j.set(n)
}j._isReady=true;
if(typeof i!=="undefined"&&i.silent){j.trigger("model:loaded",{model:j,silent:true})
}else{j.trigger("model:loaded",{model:j,silent:false})
}})
},logout:function(){var h=this;
f.ajax({url:e.config.urlRoot+"/services/social/logout",xhrFields:{withCredentials:true},type:"GET"}).always(function(){h.clear();
h.trigger(h.events.LOGGED_OUT)
})
},login:function(j,h){var i=this;
f.ajax({url:e.config.urlRoot+"/libs/login.html/j_security_check",xhrFields:{withCredentials:true},data:{j_username:j,j_password:h,j_validate:"true"},type:"POST"}).success(function(k,o,m,n){var l=m.getResponseHeader("Set-Cookie")===null||m.getResponseHeader("Set-Cookie")!=="";
if(!l){this.trigger(this.events.LOGGED_IN_FAIL,{user:j})
}else{i.getLoggedInUser();
i.trigger(i.events.LOGGED_IN_SUCCESS,{user:j})
}})
}});
a.moderatorCheckAttribute="moderatorCheck";
e.LoginView=c;
e.LoginModel=a;
e.registerComponent("login",e.LoginModel,e.LoginView)
})($CQ,SCF,_,CQ,Granite);
(function(j,k,i,d,f){var e=CQ.shared.HTTP.getContextPath();
f.events=f.events||{};
f.events.BOOTSTRAP_REQUEST="scf-bootstrap-request";
f.config.urlRoot=window.location.protocol+"//"+window.location.host;
if(e!==null&&e.length>0){f.config.urlRoot+=e
}var g=function(o){var n=o.model;
if(f.Components[o.type]){var m=o.template?f.findTemplate(o.id,o.template,o.type):f.findTemplate(o.id,o.type);
var l=new f.Components[o.type].View({model:n,el:o.el});
if(o.template){l.template=m
}else{f.Components[o.type].View.prototype.template=m
}l.templateName=o.template||"default";
l.resource=o.id;
j.each(f.loadedComponents,function(p){j.each(p,function(s,w){if(n.attributes.hasOwnProperty("parentId")){if(w===n.attributes.parentId){l.setParent(s.view)
}}else{if(l._parentView===undefined||l._parentView===null){var r=l.$el.parents("[data-component-id][data-scf-component]");
if(r&&r.length===1){var v=r.attr("data-component-id");
var t=r.attr("data-scf-component");
var u=f.loadedComponents[t];
if(u!==undefined){var q=u[v];
if(q!==undefined&&q.hasOwnProperty("view")){l.setParent(q.view)
}}}}}})
});
if(n.cacheFixed){l.render()
}o.view=l
}};
var h=function(n){if(f.Components[n.type]){var m;
var o=n.modelHolder;
var l=f.Components[n.type].Model;
if(o.length>0){var p=k(o[0]).text();
if(p===""){p=o[0].text
}var q=k.parseJSON(p);
n.id=q.id;
m=l.findLocal(n.id);
if(!m){m=f.Components[n.type].Model.createLocally(q)
}}else{m=l.findLocal(n.id);
if(!m){m=f.Components[n.type].Model.find(n.id)
}}n.model=m
}};
var c=function(s,q,p,o){var r=k("script[type='application/json'][id='"+s+"']");
var n={id:s,type:q,template:p,modelHolder:r,el:o};
var m=h(n);
var l=g(n);
return f.addLoadedComponent(q,m,l)
};
var b=function(m){var l={id:m.attr("data-component-id"),type:m.data("scf-component"),template:m.data("scf-template"),modelHolder:k("script[type='application/json'][id='"+m.attr("data-component-id")+"']"),el:m};
return l
};
var a=function(){var m=k("[data-scf-component]");
var o=[],p=[];
m.each(function(q,s){var r=b($(s));
if(!f.loadedComponents[r.type]||!f.loadedComponents[r.type][r.id]){p.push(r)
}o.push(r)
});
if(p.length>0){var l={};
l.silent=true;
l[f.LoginModel.moderatorCheckAttribute]=j.map(o,function(r){var q;
if(r.id.indexOf("/content/usergenerated")===-1){return r.id
}try{q=JSON.parse(r.modelHolder.text())
}catch(s){return false
}if(!(q.hasOwnProperty("sourceComponentId"))){return false
}if(q.sourceComponentId.indexOf("/content/usergenerated")!==-1){return false
}return q.sourceComponentId
});
l[f.LoginModel.moderatorCheckAttribute]=j.compact(l[f.LoginModel.moderatorCheckAttribute]);
if(f.Session){f.Session.getLoggedInUser(l,undefined)
}else{var n=new f.LoginModel({},l);
f.Session=n
}}j.each(p,function(q){h(q)
});
j.each(p,function(q){g(q);
f.addLoadedComponent(q.type,q.model,q.view)
})
};
k(document).ready(a);
if(!i.History.started){i.history.start({pushState:true,hasChange:false})
}$(document).on(f.events.BOOTSTRAP_REQUEST,a);
f.addComponent=function(n){var m=$(n);
if(m.length===0){throw"Could not find requested element on page."
}var l=b(m);
if(l===null){throw"Component is already loaded."
}if(!l.id){throw"Component does not have a data-component-id attribute, which is required"
}if(!l.type){throw"Component does not have a data-scf-component attribute, which is required."
}return c(l.id,l.type,l.template,l.el)
};
f.unloadComponent=function(o,m){var n=f.loadedComponents[m];
if(n===null){throw"Type "+m+" is not registered with SCF."
}var l=f.loadedComponents[m][o];
if(l===null||l===undefined){throw"Could not find component with ID: "+o
}l.view.destroy();
l.model=null;
delete f.loadedComponents[m][o]
}
})(_,$CQ,Backbone,Handlebars,SCF);
(function(e,c,b,d){var a=function(j,h,k,p){var l={};
l=b.extend(h,l);
l=b.extend(this.config,l);
var g=j.get()[0];
if(b.isUndefined(window.CKEDITOR)){SCF.log.error('Rich text editor requested but unable to find CKEDITOR please include client library category: "%s" or disable RTE',"ckeditor");
return
}this.$el=j;
var t=this.$el.data("editorheight");
var o=SCF.config.urlRoot+k.get("id")+SCF.constants.URL_EXT;
var f=k.get("configuration");
f=f&&f.isAttachmentAllowed;
if(l.extraPlugins===undefined){l.extraPlugins=(window.CKEDITOR.config.extraPlugins)?window.CKEDITOR.config.extraPlugins+",":undefined
}var s=l.toolbar[0].items;
var m;
if(f){l.filebrowserUploadUrl=o;
l.uploadUrl=o;
m=s.indexOf("Image");
if(m===-1){l.toolbar[0].items.push("Image")
}if(l.extraPlugins===undefined){l.extraPlugins="image2,uploadimage"
}else{if(l.extraPlugins.length>0&&l.extraPlugins.indexOf("image2,uploadimage")===-1){l.extraPlugins=l.extraPlugins.concat(",image2,uploadimage")
}}}else{m=s.indexOf("Image");
if(m===-1){l.toolbar[0].items.push("Image")
}if(l.extraPlugins===undefined){l.extraPlugins="image2"
}else{if(l.extraPlugins.length>0&&l.extraPlugins.indexOf("image2")===-1){l.extraPlugins=l.extraPlugins.concat(",image2")
}}}var u=c(g).attr("name");
if(b.isEmpty(u)){var r=k.get("id");
var q=r.lastIndexOf("/");
r=r.slice(q+1);
var i=c(g).data("attrib");
r=i+"-"+r;
c(g).attr("name",r);
u=r
}var n=this.$el.data("editorresize");
if(n){l.resize_enabled=true
}if(b.isNumber(t)){l.height=t
}if(!window.CKEDITOR.instances[u]){this.editor=window.CKEDITOR.replace(g,l)
}else{if(this.editor===undefined){this.editor=window.CKEDITOR.instances[u]
}}this.model=k;
if(f){this.editor.on("fileUploadRequest",this.attachFileFromDragAndDrop);
this.editor.on("fileUploadResponse",this.handleFileUploadResponse);
this.changeImagePluginDialog()
}};
a.prototype.destroy=function(){if(this.editor){try{if(this.editor.filter&&this.editor.window&&this.editor.window.getFrame()){this.editor.destroy(true);
this.editor.removeAllListeners()
}else{this.editor.removeAllListeners();
window.CKEDITOR.remove(this.editor);
window.CKEDITOR.fire("instanceDestroyed",null,this.editor)
}}catch(f){SCF.log.error("Couldn't destroy editor: %o",f)
}}delete this.editor;
return
};
a.prototype.getFileIFrameFromDialog=function(h){var q=h;
var l=q.contents;
for(var p=0;
p<l.length;
p++){var k=l[p];
var g=k.id;
if(g=="Upload"){var f=k.elements;
for(var m=0;
m<f.length;
m++){var o=f[m];
var n=o.id;
if(n=="uploadButton"){o.onClick=this.setCustomFileButtonClick;
o["for"]=["Upload","file"]
}if(n=="upload"){o.id="file"
}}}}};
a.prototype.setCustomFileButtonClick=function(q){var m=q.sender["for"];
var n=q.data.dialog;
var j=n.getContentElement(m[0],m[1]);
var k=c("#"+j.domId+" iframe");
var o=k.contents().find("form");
var r=b.bind(function(t){var s=t.response.url;
s=CQ.shared.HTTP.encodePath(s);
n.getContentElement("info","src").setValue(s);
n.selectPage("info");
if(c(".cke_dialog_ui_input_text").length!==0){c(".cke_dialog_ui_input_text").focus()
}},this);
var p=b.bind(function(s){SCF.log.error("Failed to upload file"+s)
},this);
var i;
var l=o.find("input:file");
var h=l[0].files;
var f=(typeof h!="undefined");
if(f){var g=o.attr("action");
a.prototype.attachFile.call(this,h,g,r,p)
}q.stop();
return false
};
a.prototype.handleFileUploadResponse=function(f){f.stop();
var h=f.data;
var i=h.fileLoader.xhr;
var g=i.responseText;
g=JSON.parse(g);
if(i.status==200){h.uploaded=1;
h.url=CQ.shared.HTTP.encodePath(g.response.url);
h.name=g.response.properties.name
}};
a.prototype.attachFileFromDragAndDrop=function(h){var g=h.data.fileLoader;
var i=g.xhr;
var f;
if(window.FormData){f=new FormData()
}if(f){f.append("file",g.file);
f.append("id","nobot");
f.append(":operation","social:uploadImage");
f.append("_charset_","UTF-8");
i.open("POST",g.uploadUrl,true);
i.setRequestHeader("Accept","application/json");
i.withCredentials=true;
i.send(f)
}h.stop()
};
a.prototype.attachFile=function(i,h,j,g){var f;
if(window.FormData){f=new FormData()
}if(f){c.each(i,function(k,l){f.append("file",l)
});
f.append("id","nobot");
f.append(":operation","social:uploadImage");
f.append("_charset_","UTF-8");
c.ajax(h,{dataType:"json",type:"POST",processData:false,contentType:false,xhrFields:{withCredentials:true},data:f,success:j,error:g})
}};
a.prototype.changeImagePluginDialog=function(){if(!a.isImageDialogDefinitionChanged){a.isImageDialogDefinitionChanged=true;
var f=this;
window.CKEDITOR.on("dialogDefinition",function(g){var h=g.data.name;
if(h=="image2"){f.getFileIFrameFromDialog(g.data.definition)
}})
}};
a.prototype.config={toolbar:[{name:"basicstyles",items:["Bold","Italic","Underline","NumberedList","BulletedList","Outdent","Indent","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock","TextColor"]},{name:"links",items:["Link","Unlink"]},{name:"styles",items:["Format"]}],autoParagraph:false,autoUpdateElement:false,removePlugins:"elementspath",disableNativeSpellChecker:false,resize_enabled:false};
a.prototype.setValue=function(f){this.editor.setData(f)
};
a.prototype.getValue=function(){return this.editor.getData()
};
a.prototype.focus=function(){return this.editor.focus()
};
a.isImageDialogDefinitionChanged=false;
SCF.registerFieldType("ckeditor",a);
SCF.registerFieldType("rte",a)
})(Backbone,$CQ,_,Handlebars);
(function(b,d,g,f,c){var a=function(h,j,i){this.containerEl=h;
var l=d(h).data("tag-filter");
var k=d(h).data("tag-limit");
d(h).tagit({fieldName:name,allowSpaces:false,placeholderText:CQ.I18n.getMessage("Add a tag"),animate:false,minLength:2,removeConfirmation:true,showAutocompleteOnFocus:false,tagSource:function(n,m){d.ajax({url:c.config.urlRoot+"/services/tagfilter",data:{term:n.term,tagfilter:l,tagFilterLimit:k,_charset_:"UTF-8"},dataType:"json",success:function(o){m(d.map(o,function(p){return{label:p.label,value:p.value,id:p.tagid}
}))
}})
}});
if(!b.isEmpty(i.get("tags"))){d.each(i.get("tags"),function(m,n){d(h).tagit("createTag",n.title,n.tagId,n.value)
})
}};
a.prototype.getValue=function(){var h=[];
d(this.containerEl).find("li").each(function(){var j=$(this);
var i=j.find("input").attr("value");
if(!b.isEmpty(i)){h.push(i)
}});
return h
};
a.prototype.setValue=function(){};
a.prototype.focus=function(){d(this.el).focus()
};
a.prototype.destroy=function(){};
var e=function(o,k,l){var n=function(s){var q={};
for(var r in s){q[r]=f.compile(s[r])
}return q
};
this.modelTags=l.get("tags");
this.templatesSource=this.defaultTemplates;
if(k&&k.hasOwnProperty("templates")){this.templatesSource=b.extend(this.defaultTemplates,k.templates)
}this.compiledTemplates=n(this.templatesSource);
var i=o.get()[0];
var h=d(i).data("tag-filter");
var j=d(i).data("tag-limit");
var p=e.tagsByFilterVal[h];
if(!p){var m=this;
d.ajax({url:c.config.urlRoot+"/services/tagfilter",data:{tagfilter:h,tagFilterLimit:j},dataType:"json",async:false,success:function(q){p=q;
e.tagsByFilterVal[h]=p;
m.initTagFields(p,i)
}})
}else{this.initTagFields(p,i)
}};
e.prototype.initTagFields=function(j,o){var p=d(this.compiledTemplates.inputField(j));
this.selectedTags={};
var n=this;
var m=d(o);
m.after(p);
var h=m.prop("attributes");
d.each(h,function(){p.attr(this.name,this.value)
});
p.removeAttr("data-attrib");
var l=d(this.compiledTemplates.tagsContainer(this.modelTags));
if(!b.isUndefined(this.modelTags)&&this.modelTags!==null&&this.modelTags.hasOwnProperty("length")){for(var k=0;
k<this.modelTags.length;
k++){this.selectedTags[this.modelTags[k].tagId]=this.modelTags[k]
}}p.after(l);
l.find(".scf-js-remove-tag").click(function(q){var i=d(q.target).closest("[data-attrib]");
delete n.selectedTags[i.attr("data-attrib")];
i.remove()
});
m.remove();
p.change(function(){d(p).find("option:selected").each(function(){var i=d(this).text();
var q=d(this).val();
d(this).removeAttr("selected");
if(q in n.selectedTags){return
}var r=d(n.compiledTemplates.tag({tagid:q,label:i}));
l.append(r);
n.selectedTags[q]=i;
r.find(".scf-js-remove-tag").click(function(){r.remove();
delete n.selectedTags[q]
})
});
d(d(this).find("option[disabled]")[0]).removeAttr("disabled").attr("selected","selected").attr("disabled","disabled")
})
};
e.prototype.getValue=function(){var h=[];
for(var i in this.selectedTags){h.push(i)
}return h
};
e.prototype.setValue=function(){if(tags instanceof Array){for(var j;
j<tags.length;
j++){var h=tags[j];
this.selectedTags[h.tagId]=h.title
}}};
e.prototype.focus=function(){d(this.el).focus()
};
e.prototype.destroy=function(){};
e.prototype.defaultTemplates={inputField:'<select size="1"><option disabled selected>add a tag</option>{{#each this}}<option value="{{tagid}}">{{label}}</option>{{/each}}</select>',tagsContainer:'<ul class="scf-horizontal-tag-list">{{#each this}}<li class="scf-selected-tag " data-attrib="{{tagId}}"><span class="scf-js-remove-tag scf-remove-tag"></span> {{title}}</li>{{/each}}</div>',tag:'<li class="scf-selected-tag "><span class="scf-js-remove-tag scf-remove-tag"></span> {{label}}</li>'};
e.tagsByFilterVal={};
c.registerFieldType("tags",e);
c.registerFieldType("smarttags",a);
c.TagManager=e;
c.SmartTagManager=a
})(_,$CQ,Backbone,Handlebars,SCF);
(function(a,d,h,f,c){var b=function(l,k,j){var i=d(l);
var m=k;
i.autocomplete({source:function(r,n){var p=d(l).val();
var s={operation:"CONTAINS","./@rep:principalName":p};
s=[s];
var t={operation:"like","profile/@givenName":p};
s.push(t);
var q={operation:"like","profile/@familyName":p};
s.push(q);
s=JSON.stringify(s);
var o=m+".social.0.20.json";
o=CQ.shared.HTTP.addParameter(o,"filter",s);
o=CQ.shared.HTTP.addParameter(o,"type","users");
o=CQ.shared.HTTP.addParameter(o,"fromPublisher","true");
o=CQ.shared.HTTP.addParameter(o,"_charset_","utf-8");
o=CQ.shared.HTTP.addParameter(o,"groupId","community-members");
$.get(o,function(u){var v=u.items;
i.data("lastQueryResult",v);
n(v)
})
},minLength:3,change:function(n,o){j.model.set("composedForValid",j.validateUser(i.val()))
},select:function(n,o){j.model.set("composedForValid",true);
d(this).val(o.item.authorizableId);
return false
},setvalue:function(n){this.element.val(n);
this.input.val(n);
d(this).val(n)
}}).data("uiAutocomplete")._renderItem=function(n,o){if(o.avatarUrl){return d("<li></li>").append("<a><img src='"+o.avatarUrl+"' width='30' height='30'/>&nbsp;"+o.name+"</a>").data("item.autocomplete",o).appendTo(n)
}else{return d("<li></li>").append("<a>"+o.name+"</a>").data("item.autocomplete",o).appendTo(n)
}}
};
var e=function(k,j,i){this.$el=d(k);
this.model=i;
this.config=j;
this.modelId=this.model.get("forumId");
if(a.isEmpty(this.modelId)){this.modelId=this.model.get("id")
}b(d(k),this.modelId+"/userlist",this)
};
var g=function(i,k){for(var j in i){if(i[j].authorizableId===k){return true
}}return false
};
e.prototype.validateUser=function(i){var j=false;
if(i.trim().length===0){return true
}if(this.$el.data("lastQueryResult")){if(g(this.$el.data("lastQueryResult"),i)){j=true
}}if(!j){var k=this.searchUsers(i);
j=g(k,i)
}return j
};
e.prototype.searchUsers=function(j){var k=this.modelId+"/userlist";
var i=k+".social.0.20.json?search="+j+"&showUsers=true";
var l;
$.get(i,function(m){l=m.items
});
l=l||[];
return l
};
e.prototype.getValue=function(){return this.$el.val()
};
e.prototype.setValue=function(){this.$el.autocomplete().setValue(this.model.get("author").id)
};
e.prototype.focus=function(){d(this.el).focus()
};
e.prototype.destroy=function(){if(this.$el.data("autocomplete")||this.$el.data("lastQueryResult")){this.$el.autocomplete("destroy")
}};
c.registerFieldType("userdropdown",e);
c.UserDropDown=e
})(_,$CQ,Backbone,Handlebars,SCF);