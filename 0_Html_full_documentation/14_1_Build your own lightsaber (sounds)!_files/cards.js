/*!
 * imagesLoaded PACKAGED v4.1.1
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
(function(b,a){if(typeof define=="function"&&define.amd){define("ev-emitter/ev-emitter",a)
}else{if(typeof module=="object"&&module.exports){module.exports=a()
}else{b.EvEmitter=a()
}}}(typeof window!="undefined"?window:this,function(){function a(){}var b=a.prototype;
b.on=function(c,f){if(!c||!f){return
}var d=this._events=this._events||{};
var e=d[c]=d[c]||[];
if(e.indexOf(f)==-1){e.push(f)
}return this
};
b.once=function(d,e){if(!d||!e){return
}this.on(d,e);
var c=this._onceEvents=this._onceEvents||{};
var f=c[d]=c[d]||{};
f[e]=true;
return this
};
b.off=function(c,f){var e=this._events&&this._events[c];
if(!e||!e.length){return
}var d=e.indexOf(f);
if(d!=-1){e.splice(d,1)
}return this
};
b.emitEvent=function(c,d){var f=this._events&&this._events[c];
if(!f||!f.length){return
}var e=0;
var h=f[e];
d=d||[];
var j=this._onceEvents&&this._onceEvents[c];
while(h){var g=j&&j[h];
if(g){this.off(c,h);
delete j[h]
}h.apply(this,d);
e+=g?0:1;
h=f[e]
}return this
};
return a
}));
/*!
 * imagesLoaded v4.1.1
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
(function(b,a){if(typeof define=="function"&&define.amd){define(["ev-emitter/ev-emitter"],function(c){return a(b,c)
})
}else{if(typeof module=="object"&&module.exports){module.exports=a(b,require("ev-emitter"))
}else{b.imagesLoaded=a(b,b.EvEmitter)
}}})(window,function factory(g,c){var e=g.jQuery;
var d=g.console;
function h(l,k){for(var m in k){l[m]=k[m]
}return l
}function a(m){var l=[];
if(Array.isArray(m)){l=m
}else{if(typeof m.length=="number"){for(var k=0;
k<m.length;
k++){l.push(m[k])
}}else{l.push(m)
}}return l
}function f(m,l,k){if(!(this instanceof f)){return new f(m,l,k)
}if(typeof m=="string"){m=document.querySelectorAll(m)
}this.elements=a(m);
this.options=h({},this.options);
if(typeof l=="function"){k=l
}else{h(this.options,l)
}if(k){this.on("always",k)
}this.getImages();
if(e){this.jqDeferred=new e.Deferred()
}setTimeout(function(){this.check()
}.bind(this))
}f.prototype=Object.create(c.prototype);
f.prototype.options={};
f.prototype.getImages=function(){this.images=[];
this.elements.forEach(this.addElementImages,this)
};
f.prototype.addElementImages=function(o){if(o.nodeName=="IMG"){this.addImage(o)
}if(this.options.background===true){this.addElementBackgroundImages(o)
}var k=o.nodeType;
if(!k||!b[k]){return
}var p=o.querySelectorAll("img");
for(var n=0;
n<p.length;
n++){var l=p[n];
this.addImage(l)
}if(typeof this.options.background=="string"){var m=o.querySelectorAll(this.options.background);
for(n=0;
n<m.length;
n++){var q=m[n];
this.addElementBackgroundImages(q)
}}};
var b={1:true,9:true,11:true};
f.prototype.addElementBackgroundImages=function(m){var l=getComputedStyle(m);
if(!l){return
}var o=/url\((['"])?(.*?)\1\)/gi;
var n=o.exec(l.backgroundImage);
while(n!==null){var k=n&&n[2];
if(k){this.addBackground(k,m)
}n=o.exec(l.backgroundImage)
}};
f.prototype.addImage=function(k){var l=new i(k);
this.images.push(l)
};
f.prototype.addBackground=function(k,m){var l=new j(k,m);
this.images.push(l)
};
f.prototype.check=function(){var l=this;
this.progressedCount=0;
this.hasAnyBroken=false;
if(!this.images.length){this.complete();
return
}function k(o,n,m){setTimeout(function(){l.progress(o,n,m)
})
}this.images.forEach(function(m){m.once("progress",k);
m.check()
})
};
f.prototype.progress=function(m,l,k){this.progressedCount++;
this.hasAnyBroken=this.hasAnyBroken||!m.isLoaded;
this.emitEvent("progress",[this,m,l]);
if(this.jqDeferred&&this.jqDeferred.notify){this.jqDeferred.notify(this,m)
}if(this.progressedCount==this.images.length){this.complete()
}if(this.options.debug&&d){d.log("progress: "+k,m,l)
}};
f.prototype.complete=function(){var l=this.hasAnyBroken?"fail":"done";
this.isComplete=true;
this.emitEvent(l,[this]);
this.emitEvent("always",[this]);
if(this.jqDeferred){var k=this.hasAnyBroken?"reject":"resolve";
this.jqDeferred[k](this)
}};
function i(k){this.img=k
}i.prototype=Object.create(c.prototype);
i.prototype.check=function(){var k=this.getIsImageComplete();
if(k){this.confirm(this.img.naturalWidth!==0,"naturalWidth");
return
}this.proxyImage=new Image();
this.proxyImage.addEventListener("load",this);
this.proxyImage.addEventListener("error",this);
this.img.addEventListener("load",this);
this.img.addEventListener("error",this);
this.proxyImage.src=this.img.src
};
i.prototype.getIsImageComplete=function(){return this.img.complete&&this.img.naturalWidth!==undefined
};
i.prototype.confirm=function(k,l){this.isLoaded=k;
this.emitEvent("progress",[this,this.img,l])
};
i.prototype.handleEvent=function(k){var l="on"+k.type;
if(this[l]){this[l](k)
}};
i.prototype.onload=function(){this.confirm(true,"onload");
this.unbindEvents()
};
i.prototype.onerror=function(){this.confirm(false,"onerror");
this.unbindEvents()
};
i.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this);
this.proxyImage.removeEventListener("error",this);
this.img.removeEventListener("load",this);
this.img.removeEventListener("error",this)
};
function j(k,l){this.url=k;
this.element=l;
this.img=new Image()
}j.prototype=Object.create(i.prototype);
j.prototype.check=function(){this.img.addEventListener("load",this);
this.img.addEventListener("error",this);
this.img.src=this.url;
var k=this.getIsImageComplete();
if(k){this.confirm(this.img.naturalWidth!==0,"naturalWidth");
this.unbindEvents()
}};
j.prototype.unbindEvents=function(){this.img.removeEventListener("load",this);
this.img.removeEventListener("error",this)
};
j.prototype.confirm=function(k,l){this.isLoaded=k;
this.emitEvent("progress",[this,this.element,l])
};
f.makeJQueryPlugin=function(k){k=k||g.jQuery;
if(!k){return
}e=k;
e.fn.imagesLoaded=function(m,n){var l=new f(this,m,n);
return l.jqDeferred.promise(e(this))
}
};
f.makeJQueryPlugin();
return f
});
(function(a){window.SCFCards=function(b,c){this.container=a(b);
var e=c||{};
this.cardMargin=e.cardMargin||10;
this.containerPadding=e.containerPadding||0;
this.cardWidth=e.cardWidth||154;
var d=this;
a(window).resize(function(){d.redraw(true)
});
this.redraw(false)
};
SCFCards.prototype.redraw=function(b){this.containerWidth=this.container.outerWidth(true);
this.cards=this.container.find(".scf-card");
this.columns=Math.floor((this.containerWidth-(2*this.containerPadding))/(this.cardWidth+(this.cardMargin*2)));
this.occupiedWidth=Math.ceil(this.columns*(this.cardWidth+(this.cardMargin*2)));
this.newSidePadding=Math.floor((this.containerWidth-this.occupiedWidth)/2);
this.container.css({"padding-left":this.newSidePadding,"padding-right":this.newSidePadding,position:"relative"});
var f=this;
var c=[];
for(var d=0;
d<this.columns;
d++){c[d]=f.newSidePadding
}var e=0;
var g=f.newSidePadding;
this.cards.each(function(o,j){var n=a(j);
var m=f.newSidePadding;
var h=0;
for(var k=0;
k<f.columns;
k++){if(c[k]<=g){g=c[k];
h=k;
if(o<=f.columns&&k>=o){break
}}}m=c[h];
var l=(h)*(f.cardWidth+(f.cardMargin*2))+f.newSidePadding;
if(b){n.animate({position:"absolute",left:l,top:m},500)
}else{n.css({position:"absolute",left:l,top:m})
}c[h]+=n.outerHeight(true);
if(c[h]>e){e=c[h]
}g=c[h]
});
this.container.css({display:"block",height:e+this.newSidePadding})
}
})($CQ);