#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_emu.h"
#include "em_rmu.h"
#include "em_timer.h"
#include <stdio.h>

#define EM4_PORT              gpioPortA
#define EM4_PIN               3
//#define EM4_WAKEUP_ENABLE     0x11  // Must change when changing w/u pin
#define BIT24 (1u << 24)

#define LED_PORT  gpioPortD
#define LED_PIN   14 //P 33

#define DEBUG_BREAK                                   __asm__("BKPT #0");
#define ONE_MILLISECOND_BASE_VALUE_COUNT             1000
#define ONE_SECOND_TIMER_COUNT                        13672
#define MILLISECOND_DIVISOR                           13.672//15.6250

volatile uint64_t base_value = 0;
volatile bool timer1_overflow = false;
volatile uint32_t msTicks = 0;
volatile bool light_on;
volatile uint16_t count;

void TIMER1_IRQHandler(void)
{
      timer1_overflow = true;
      base_value += ONE_MILLISECOND_BASE_VALUE_COUNT;
      TIMER_IntClear(TIMER1, TIMER_IF_OF);


//      light_on = !light_on;
//
//	  if (light_on==true)
//	  {
//		GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);
//
//	  }
//	  else
//	  {
//		GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 1);
//	  }
}

uint64_t get_time_in_ms()
{
      // Clear our overflow indicator
      timer1_overflow = false;

      // Get the current count
      //uint16_t count = TIMER1->CNT;
      uint32_t count = TIMER_CounterGet(TIMER1);

//      DEBUG_BREAK;

      // Get a copy of the base value
      uint64_t copy_of_base_value = base_value;

      // Now check to make sure that the ISR didn't fire while in here
      // If it did, then grab the values again
      if (timer1_overflow)
      {
            count = TIMER1->CNT;

            copy_of_base_value = base_value;
      }

//      if (count > 13400)
//      {
//    	  DEBUG_BREAK;
//      }

      // Now calculate the number of milliseconds the program has run
      return copy_of_base_value + count / MILLISECOND_DIVISOR;
}

void delay_ms(uint64_t milliseconds)
{
      uint64_t trigger_time = get_time_in_ms() + milliseconds;
//      uint64_t test = get_time_in_ms();
      while (get_time_in_ms() < trigger_time)
            ;

}

// Pass the ms_ticks when the timer started, get the number of ms since then
// Pass in a future value to get time until timeout occurs, a neg value
int64_t elapsed_ms(int64_t start_ms)
{
      return get_time_in_ms() - start_ms;
}

// Pass in the elapsed time before it times out, returns the future time
int64_t set_ms_timeout(int64_t timeout_ms)
{
      return get_time_in_ms() + timeout_ms;
}

// Check to see if the future time has elapsed.
int64_t expired_ms(int64_t timeout_ms)
{
      if (timeout_ms < get_time_in_ms())
      {
            return true;
      }
      return false;
}



int main(void)
{

	  uint32_t blah;
	  uint64_t blah2;

      CHIP_Init();

      CMU_ClockEnable(cmuClock_GPIO, true);
      CMU_ClockEnable(cmuClock_TIMER1, true);

//      bool light_on = false;

      // Create a timerInit object, based on the API default
      TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
      timerInit.prescale = timerPrescale1024;
      timerInit.debugRun = false;

      TIMER_IntEnable(TIMER1, TIMER_IF_OF);

      // Enable TIMER0 interrupt vector in NVIC
      NVIC_EnableIRQ(TIMER1_IRQn);

      TIMER_Init(TIMER1, &timerInit);
      // Set TIMER Top value
      TIMER_TopSet(TIMER1, ONE_SECOND_TIMER_COUNT);
      //GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);

      // Wait for the timer to get going
      while (TIMER1->CNT == 0)
    	  ;

//      GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 1);
//      printf("Hello, please enter your age");
      while (1)
      {
//    	  blah = TIMER1->CNT;
//    	  setvbuf (stdout, NULL, _IONBF, 0);
//    	  printf(blah);
    	  if (blah>14672)
    	  {
    		  blah2 = get_time_in_ms();
    		  DEBUG_BREAK;
    	  }
    	  //delay_ms(1000);
		  // Do something that will happen every second

    	  GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 1);
    	  delay_ms(1);
    	  GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);
    	  delay_ms(1);

      }

      // Code to use timer goes here
}
