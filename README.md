# This is the makers guide to IOT project files
* that was created when following along [here](https://www.silabs.com/products/mcu/32-bit/makers-guide-to-the-iot)
* individual folders/examples compile with GNU C. 
* Boards used for the examples are the EFR32MG BRD 4151A and EFM32 Wonder Gecko
