#include "em_device.h"
#include "em_chip.h"
#include "em_rtcc.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_gpio.h"
#include "em_pcnt.h"

#define DEBUG_BREAK 						__asm__("BKPT #0");
#define RTCC_PULSE_FREQUENCY    (64)


static volatile uint32_t curTime = 0;

 /* PCNT interrupt counter */
 static volatile int pcntIrqCount = 0;


int main(void)
{
  /* Chip errata */
  CHIP_Init();


  CMU_ClockEnable(cmuClock_CORELE, true);
  CMU_ClockEnable(cmuClock_GPIO, true);       /* Enable clock for GPIO module */

	// Enable LFACLK in CMU (will also enable oscillator if not enabled)
//	CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
//	CMU_OscillatorEnable(cmuOsc_LFXO, true, true);
//
//	CMU_LFXOInit_TypeDef lfxoInit = CMU_LFXOINIT_DEFAULT;
//	CMU_LFXOInit(&lfxoInit);
//
//	// Use the prescaler to reduce power consumption. 2^15
////	CMU_ClockDivSet(cmuClock_RTCC, cmuClkDiv_32768);
//
//	// Enable clock to RTC module
//	CMU_ClockEnable(cmuClock_RTCC, true);

	// Set up the Real Time Clock as long-time timekeeping
//	RTCC_Init_TypeDef init = RTCC_INIT_DEFAULT;
//	//      init.precntWrapOnCCV0 = false;
//	RTCC_Init(&init);


	// Enabling Interrupt from RTCC


//	RTCC_IntDisable(RTCC_IF_CC0);
//	RTCC_ChannelCCVSet(0,10);
//	RTCC_IntClear(RTCC_IF_CC0);
//
//	NVIC_EnableIRQ(RTCC_IRQn);
//	RTCC_IntEnable(RTCC_IF_CC0);


//	RTCC_Enable(true);

	pcntInit();



  /* Infinite loop */
  while (1) {
  }
}

void pcntInit(void)
 {
   PCNT_Init_TypeDef pcntInit = PCNT_INIT_DEFAULT;

   /* Enable PCNT clock */
  CMU_ClockEnable(cmuClock_PCNT0, true);
   /* Set up the PCNT to count RTC_PULSE_FREQUENCY pulses -> one second */

   pcntInit.mode = pcntModeOvsSingle;
   pcntInit.top = 1000;
   pcntInit.s1CntDir = false;
   /* The PRS channel used depends on the configuration and which pin the
   LCD inversion toggle is connected to. So use the generic define here. */
   pcntInit.s0PRS =DISP_EXTCOMIN; //(PCNT_PRSSel_TypeDef)LCD_AUTO_TOGGLE_PRS_CH;


   PCNT_Init(PCNT0, &pcntInit);
   DEBUG_BREAK;

   /* Select PRS as the input for the PCNT */
   PCNT_PRSInputEnable(PCNT0, pcntPRSInputS0, true);

   /* Enable PCNT interrupt every second */
   NVIC_EnableIRQ(PCNT0_IRQn);
   PCNT_IntEnable(PCNT0, PCNT_IF_OF);
 }

void PCNT0_IRQHandler(void)
 {

   DEBUG_BREAK;
   PCNT_IntClear(PCNT0, PCNT_IF_OF);

   pcntIrqCount++;


}




void RTCC_IRQHandler(void)
{
	DEBUG_BREAK;

}
