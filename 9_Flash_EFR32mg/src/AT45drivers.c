/*
 * AT45drivers.c
 *
 *  Created on: Feb 22, 2018
 *      Author: starrover
 */


// Reads the status

#include "AT45drivers.h"
#include "spidrv.h"

SPIDRV_HandleData_t handleData;
SPIDRV_Handle_t handle = &handleData;

static uint8_t read_status(StatusByte *status)
{
      const int size = 3;
      uint8_t result[size];
      uint8_t tx_data[size];

      tx_data[0] = STATUS;

      SPIDRV_MTransferB( handle, &tx_data, &result, size);

      status->statusbyte1 = result[1];
      status->statusbyte2 = result[2];

      return 0;
}

static void block_status_ready()
{
	StatusByte status_result;
	uint8_t byte1;
	uint8_t byte2;

	//8th most significant bit, if ==0, then operation is still busy
	read_status(&status_result);
	byte1 = status_result.statusbyte1;
	byte2 = status_result.statusbyte2;
	while (byte1>>7==0)
	{
		read_status(&status_result);
		byte1 = status_result.statusbyte1;
		byte2 = status_result.statusbyte2;
	}
}


void at45_write_buffer_address(uint8_t address, uint8_t value)
{
	block_status_ready();

	const int size = PAGE_SIZE+4;
    uint8_t result[size];
    uint8_t tx_data[size];

    tx_data[0] = WRITE_BUFFER;
    tx_data[1] = 0;
    tx_data[2] = 0;
    tx_data[3] = address;
    tx_data[4] = value;

    SPIDRV_MTransferB( handle, &tx_data, &result,5);

}

// pagenumber has to be between 0 and 1024
static void buffer_to_main_mem(uint16_t pagenumber)
{
	block_status_ready();

	const int size = 4;
	uint8_t result[size];
	uint8_t tx_data[size];

	tx_data[0] = BUFFER_TO_MEMORY;
	tx_data[1] = (pagenumber >> 8) & 0x3;
	tx_data[2] = pagenumber & 0xff;
	tx_data[3] = 0;

	SPIDRV_MTransferB( handle, &tx_data, &result,size);
}

void at45_chip_erase()
{
	  block_status_ready();

      const int size = 4;
	  uint8_t result[size];
	  uint8_t tx_data[size];

	  tx_data[0] = CHIP_ERASE_BYTE1;
	  tx_data[1] = CHIP_ERASE_BYTE2;
	  tx_data[2] = CHIP_ERASE_BYTE3;
	  tx_data[3] = CHIP_ERASE_BYTE4;

	  SPIDRV_MTransferB( handle, &tx_data, &result,size);
}

uint8_t at45_read_memory_address(uint16_t pagenumber, uint8_t address)
{
	block_status_ready();

    const int size = 9;
    uint8_t result[size];
    uint8_t tx_data[size];

    tx_data[0] = READ_MEMORY;
    tx_data[1] = (pagenumber >> 8) & 0x3;
    tx_data[2] = pagenumber & 0xff;
    tx_data[3] = address;
    //four dummy bytes
    tx_data[4] = 0;
	tx_data[5] = 0;
	tx_data[6] = 0;
	tx_data[7] = 0;

	SPIDRV_MTransferB( handle, &tx_data, &result,size);

	return result[8];
}

//writes memory from address forward.
void at45_write_memory(uint32_t address, uint8_t data_buffer[], uint32_t num_of_bytes)
{

	uint32_t page_number;
	uint8_t page_address;
	page_number = address >> 8;
	page_address = address & 0xff;

	if (page_number > 1024) {
		DEBUG_BREAK
	}

	if (address + num_of_bytes >  262144) DEBUG_BREAK; //  1024 (number of pages)* 256 (num of bytes per page)


	const int size = PAGE_SIZE+4;
	uint8_t result[size];
	uint8_t tx_data[size];

	tx_data[0] = WRITE_BUFFER;
	tx_data[1] = 0;
	tx_data[2] = 0;
	tx_data[3] = page_address;

	int i=0;
	uint8_t local_page_address = page_address;
	uint32_t page_number_counter = page_number;
	while (i < num_of_bytes)
	{
		int counter = 0;
		while (local_page_address + counter< PAGE_SIZE)
		{
			if (i == num_of_bytes) {
				break;
			}
			tx_data[counter+4] = data_buffer[i];
			counter++;
			i++;
		}

		block_status_ready();
		SPIDRV_MTransferB( handle, &tx_data, &result,counter+4);
		block_status_ready();
		buffer_to_main_mem(page_number_counter);

		local_page_address = 0;
		page_number_counter++;
	}
}


void at45_read_memory(uint32_t address, uint8_t *result, uint32_t num_of_bytes)
{
	uint32_t page_number;
	uint8_t page_address;
	page_number = address >> 8;
	page_address = address & 0xff;


	const int size = num_of_bytes+8;
	uint8_t tx_data[size];
	uint8_t rcv_data[size];

	tx_data[0] = CONT_ARRAY_READ;
	// 3 address bytes
	tx_data[1] = (page_number >> 8) & 0x3;
	tx_data[2] = page_number & 0xff;
	tx_data[3] = page_address;
	//one dummy byte
	tx_data[4] = 0;
	tx_data[5] = 0;
	tx_data[6] = 0;
	tx_data[7] = 0;

	block_status_ready();
	SPIDRV_MTransferB( handle, &tx_data, &rcv_data,size);

	int i;
	for (i=0;i<num_of_bytes;i++) {
		result[i] = rcv_data[i+8];
	}
}

