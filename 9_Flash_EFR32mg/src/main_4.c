/*
 * main3.c
 *
 *  Created on: Feb 13, 2018
 *      Author: starrover
 *      This code works with the external flash drive AT45DB021E from adesto
 */
#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"

#include "AT45drivers.h"
#include "lighting_functions.h"
#include "led_lookup_table.h"

#define JEDEC_ID_CMD	0x9F

#include "spidrv.h"

#define DEBUG_BREAK		__asm__("BKPT #0");

extern SPIDRV_Handle_t handle;

//****Check other configuration as onboard external FLASH may be interfering with signal
// in the C configuration

//PD10 - P4 - CS
//PD11 - P6 - TX
//PD12 - P8 - RX
//PD13 - P31 - CLK

//in order for this to run, need to disable CS control, eitherwise manual
// control of the CS pin doesn't work.


void spidrv_setup()
{
      // Set up the necessary peripheral clocks
      CMU_ClockEnable(cmuClock_GPIO, true);

      // Enable the GPIO pins for the misc signals, leave pulled high
//      GPIO_PinModeSet(gpioPortD, 4, gpioModePushPullDrive, 1);          // WP#
//      GPIO_PinModeSet(gpioPortD, 5, gpioModePushPullDrive, 1);          // HOLD#

      // Initialize and enable the SPIDRV
      SPIDRV_Init_t initData = SPIDRV_MASTER_USART1_CUSTOM;
      initData.clockMode = spidrvClockMode3;

      // Initialize a SPI driver instance
      SPIDRV_Init( handle, &initData );
}

int main(void)
{
      CHIP_Init();

      spidrv_setup();

//      setup_utilities();

      int i;
      	int b=0;
      	for( i = 0; i < 1000000; i++ ){
      		b++;
      	}

//      delay(100);

      uint8_t result[PAGE_SIZE];
      uint8_t tx_data[PAGE_SIZE];

      tx_data[0] = JEDEC_ID_CMD;

      SPIDRV_MTransferB( handle, &tx_data, &result, 4);

      // Check the result for what is expected from the Spansion spec
      if (result[1] != 1 || result[2] != 0x40 || result[3] != 0x13)
      {
//            DEBUG_BREAK
      }

      PWMChannels pwm;

      pwm.channel1 = 0xf0e;
      pwm.channel2 = 0x0f0;
      pwm.channel3 = 0x0f0;
      pwm.channel4 = 0x00f;
      pwm.channel5 = 0x00f;

      uint16_t hue = 10;
      uint8_t sat = 50;

      write_pwm_write_vs_hue_sat(hue,sat,pwm);

      PWMChannels output;

      output = get_pwm_from_hue_sat(hue,sat);

      DEBUG_BREAK;

      while (1)
            ;
}
