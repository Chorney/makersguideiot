/*
 * lighting_functions.c
 *
 *  Created on: Feb 22, 2018
 *      Author: starrover
 */
#include <stdio.h>
#include <stdint.h>
#include "lighting_functions.h"
#include <math.h>

//static uint8_t hue_to_integer_spot(uint8_t hue);
//static uint8_t sat_to_integer_spot(uint8_t sat);

static uint8_t hue_to_integer_spot(uint16_t hue) {

	float incr = (float)(HUE_HIGHER - HUE_LOWER)/(float)(NUM_POINTS_SQAURE_ARRAY-1);

	if (hue > HUE_HIGHER) {
		hue = HUE_HIGHER;
	} else if (hue < HUE_LOWER) {
		hue = HUE_LOWER;
	}

	float point_float = (float)hue/incr;
	double point_float_round = round(point_float);
	uint8_t point_float_int = (uint8_t) point_float_round;
	return point_float_int;
}

static uint8_t sat_to_integer_spot(uint8_t sat) {

	float incr = (float)(SAT_HIGHER - SAT_LOWER)/(float)(NUM_POINTS_SQAURE_ARRAY-1);

	if (sat > SAT_HIGHER) {
		sat = SAT_HIGHER;
	} else if (sat < SAT_LOWER) {
		sat = SAT_LOWER;
	}

	float point_float = (float)sat/incr;
	double point_float_round = round(point_float);
	uint8_t point_float_int = (uint8_t) point_float_round;
	return point_float_int;
}

//THIS is commented out as I had started to create functions for 8 mbit drivers, but have decided
//to migrate to a different project folder as not to convolute makers guide stuff
//static uint8_t bright_to_integer_spot(uint8_t bright) {
//
//	float incr = (float)(BRIGHT_HIGHER - BRIGHT_LOWER)/(float)(NUM_POINTS_SQAURE_ARRAY-1);
//
//	if (bright > BRIGHT_HIGHER) {
//		bright = BRIGHT_HIGHER;
//	} else if (bright < BRIGHT_LOWER) {
//		bright = BRIGHT_LOWER;
//	}
//
//	float point_float = (float)bright/incr;
//	double point_float_round = round(point_float);
//	uint8_t point_float_int = (uint8_t) point_float_round;
//	return point_float_int;
//}

uint32_t get_address_from_hue_sat(uint16_t hue, uint8_t sat) {
	uint32_t address;

	uint8_t hue_int = hue_to_integer_spot(hue);
	uint8_t sat_int = sat_to_integer_spot(sat);

	address =hue_int*NUM_BYTES_PER_POINT*NUM_POINTS_SQAURE_ARRAY+sat_int*NUM_BYTES_PER_POINT;
	return address;
}

//THIS is commented out as I had started to create functions for 8 mbit drivers, but have decided
//to migrate to a different project folder as not to convolute makers guide stuff

//uint32_t get_address_from_hue_sat_bright(uint16_t hue, uint8_t sat, uint8_t bright) {
//
//	 uint8_t hue_int,sat_int,bright_int;
//	 uint32_t address;
//
//     hue_int = hue_to_integer_spot(hue);
//     sat_int = sat_to_integer_spot(sat);
//     bright_int = bright_to_integer_spot(bright);
//
//     address = hue_int*NUM_BYTES_PER_POINT*NUM_POINTS_SAT*NUM_POINTS_BRIGHT+(sat_int*NUM_BYTES_PER_POINT*NUM_POINTS_BRIGHT + bright_int*NUM_BYTES_PER_POINT);
//
//	 return address;
//}

PWMChannels get_channel_values_from_byte_string(uint8_t input[]) {

	PWMChannels output;

	output.channel1 = (input[0] << 4) | (input[1] >> 4);
	output.channel2 = ((input[1] & 0x0f) << 8) | input[2];
	output.channel3 = (input[3] << 4) | (input[4] >> 4);
	output.channel4 = ((input[4] & 0x0f) << 8) | input[5];
	output.channel5 = (input[6] << 4) | (input[7] >> 4);

	return output;
}

void packup_pwms_byte_string(PWMChannels pwms,uint8_t *result,uint8_t num_bytes) {

	result[0] = pwms.channel1 >> 4;
	result[1] = ((pwms.channel1 & 0x00f) << 4) | ((pwms.channel2 & 0xf00) >> 8);
	result[2] = pwms.channel2 & 0x0ff;
	result[3] = pwms.channel3 >> 4;
	result[4] = ((pwms.channel3 & 0x00f) << 4) | ((pwms.channel4 & 0xf00) >> 8);
	result[5] = pwms.channel4 & 0x0ff;
	result[6] = pwms.channel5 >> 4;
	result[7] = ((pwms.channel5 & 0x00f) << 4);

}

//Wrapper test functions for static functions in order to import into main_test.c
uint8_t test_sat_to_integer_spot(uint8_t sat) {
	uint8_t out = sat_to_integer_spot(sat);
	return out;
}

uint8_t test_hue_to_integer_spot(uint16_t hue) {
	return hue_to_integer_spot(hue);
}

//uint8_t test_bright_to_integer_spot(uint8_t bright) {
//	return bright_to_integer_spot(bright);
//}

