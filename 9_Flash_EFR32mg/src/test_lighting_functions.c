/*
 * main_test.c
 *
 *  Created on: Feb 22, 2018
 *      Author: starrover
 */
#include <stdio.h>
#include <stdlib.h> /* for exit */
#include <stdint.h>

#include "lighting_functions.h"


typedef struct {
	uint16_t input;
	uint8_t expected_output;
}testcase;

typedef struct {
	uint16_t hue_input;
	uint8_t sat_input;
	uint32_t out_address;
}teststruct_get_address;

int main(void) {

	T_sat_to_integer_spot();
	T_hue_to_integer_spot();
//	T_bright_to_integer_spot();
	T_get_address_from_hue_sat();
	T_get_channel_values_from_byte_string();

	T_packup_pwms_byte_string();


	printf("success");
	exit(EXIT_SUCCESS);

}

void T_sat_to_integer_spot(void){
		printf("Running T_sat_to_integer_spot\n");
		uint8_t sat, sat_result;
		int num_tcs = 5;

		testcase tcs[num_tcs];
		//TEST CASE 1
		tcs[0].input = 80;
		tcs[0].expected_output = 144;

		//TEST CASE 2
		tcs[1].input = 99;
		tcs[1].expected_output = 178;

		//TEST CASE 3
		tcs[2].input = 0;
		tcs[2].expected_output = 0;

		//TEST CASE 4
		tcs[3].input = 100;
		tcs[3].expected_output = 180;

		//TEST CASE 5
		tcs[4].input = 105;
		tcs[4].expected_output = 180;

		int i;
		for (i=0;i<num_tcs;i++) {
			printf("Running Case %d \n",i+1);
			uint8_t val1 = test_sat_to_integer_spot(tcs[i].input);
			if (val1!=tcs[i].expected_output) {
				printf("failed test case %d \n",i+1);
				printf("error with function sat_to_integer_spot, expected %d, got %d",tcs[i].expected_output,val1);
				exit(EXIT_FAILURE);
			}
			printf("Case %d Passed!\n",i+1);
		}
}

void T_hue_to_integer_spot(void){
		printf("Running T_hue_to_integer_spot\n");
		uint8_t hue, hue_result;
		int num_tcs = 4;

		testcase tcs[num_tcs];
		//TEST CASE 1
		tcs[0].input = 80;
		tcs[0].expected_output = 40;

		//TEST CASE 2
		tcs[1].input = 360;
		tcs[1].expected_output = 180;

		//TEST CASE 3
		tcs[2].input = 0;
		tcs[2].expected_output = 0;

		//TEST CASE 4
		tcs[3].input = 365;
		tcs[3].expected_output = 180;

		int i;
		for (i=0;i<num_tcs;i++) {
			printf("Running Case %d \n",i+1);
			uint8_t val1 = test_hue_to_integer_spot(tcs[i].input);
			if (val1!=tcs[i].expected_output) {
				printf("failed test case %d \n",i+1);
				printf("error with function hue_to_integer_spot, expected %d, got %d",tcs[i].expected_output,val1);
				exit(EXIT_FAILURE);
			}
			printf("Case %d Passed!\n",i+1);
		}
}

//THIS is commented out as I had started to create functions for 8 mbit drivers, but have decided
//to migrate to a different project folder as not to convolute makers guide stuff

//void T_bright_to_integer_spot(void){
//		printf("Running T_bright_to_integer_spot\n");
//		uint8_t bright, bright_result;
//		int num_tcs = 4;
//
//		testcase tcs[num_tcs];
//		//TEST CASE 1
//		tcs[0].input = 0;
//		tcs[0].expected_output = 0;
//
//		//TEST CASE 2
//		tcs[1].input = 110;
//		tcs[1].expected_output = 49;
//
//		//TEST CASE 3
//		tcs[2].input = 100;
//		tcs[2].expected_output = 49;
//
//		//TEST CASE 4
//		tcs[3].input = 365;
//		tcs[3].expected_output = 180;
//
//		int i;
//		for (i=0;i<num_tcs;i++) {
//			printf("Running Case %d \n",i+1);
//			uint8_t val1 = test_bright_to_integer_spot(tcs[i].input);
//			if (val1!=tcs[i].expected_output) {
//				printf("failed test case %d \n",i+1);
//				printf("error with function hue_to_integer_spot, expected %d, got %d",tcs[i].expected_output,val1);
//				exit(EXIT_FAILURE);
//			}
//			printf("Case %d Passed!\n",i+1);
//		}
//}

void T_get_address_from_hue_sat(void) {
	printf("Running T_get_address_from_hue_sat\n");
	uint8_t hue, hue_result;
	int num_tcs = 6;

	teststruct_get_address tcs[num_tcs];
	//TEST CASE 1
	tcs[0].hue_input = 0;
	tcs[0].sat_input = 0;
	tcs[0].out_address = 0;

	//TEST CASE 2
	tcs[1].hue_input = 0;
	tcs[1].sat_input = 100;
	tcs[1].out_address = 1440;

	//TEST CASE 3
	tcs[2].hue_input = 2;
	tcs[2].sat_input = 0;
	tcs[2].out_address = 1440+8;

	//TEST CASE 4
	tcs[3].hue_input = 4;
	tcs[3].sat_input = 0;
	tcs[3].out_address = 1440+1440+8+8;

	//TEST CASE 5
	tcs[4].hue_input = 360;
	tcs[4].sat_input = 0;
	tcs[4].out_address = 260640;

	//TEST CASE 5
	tcs[5].hue_input = 360;
	tcs[5].sat_input = 100;
	tcs[5].out_address = 260640+180*8;

	int i;
	for (i=0;i<num_tcs;i++) {
		printf("Running Case %d \n",i+1);
		uint32_t val1 = get_address_from_hue_sat(tcs[i].hue_input,tcs[i].sat_input);
		if (val1!=tcs[i].out_address) {
			printf("failed test case %d \n",i+1);
			printf("error with function get_address_from_hue_sat, expected %d, got %d",tcs[i].out_address,val1);
			exit(EXIT_FAILURE);
		}
		printf("Case %d Passed!\n",i+1);
	}


}

void T_get_channel_values_from_byte_string(void) {
	printf("Running T_get_channel_values_from_byte_string\n");
	int i = 0;

	//TEST CASE 1

	// input = 11111111,11110000,00000000,11111111,11110000,00000000,11111111,1111#0000,
	uint8_t input[8];
	input[0] = 0b11111111;
	input[1] = 0b11110000;
    input[2] = 0b00000000;
    input[3] = 0b11111111;
    input[4] = 0b11110000;
    input[5] = 0b00000000;
    input[6] = 0b11111111;
    input[7] = 0b11110101;

    PWMChannels blah;

    blah = get_channel_values_from_byte_string(input);

    if (blah.channel1 != 0b111111111111) {
    	printf("failed test case %d , part1 \n",i+1);
    	exit(EXIT_FAILURE);
    } else if (blah.channel2 != 0b000000000000) {
    	printf("failed test case %d , part2 \n",i+1);
    	exit(EXIT_FAILURE);
    } else if (blah.channel3 != 0b111111111111) {
        printf("failed test case %d , part3 \n",i+1);
        exit(EXIT_FAILURE);
    } else if (blah.channel4 != 0b000000000000) {
        printf("failed test case %d , part4 \n",i+1);
        exit(EXIT_FAILURE);
    } else if (blah.channel5 != 0b111111111111) {
        printf("failed test case %d , part5 \n",i+1);
        exit(EXIT_FAILURE);
    }
    printf("Case %d Passed!\n",i+1);


    i = 1;

	//TEST CASE 2
	// input = 11011111,11110000,00001000,11101111,11110001,01000000,11101111,0111#0000,
	input[0] = 0b11011111;
	input[1] = 0b11110000;
	input[2] = 0b00001000;
	input[3] = 0b11101111;
	input[4] = 0b11110001;
	input[5] = 0b01000000;
	input[6] = 0b11101111;
	input[7] = 0b01110101;

	blah = get_channel_values_from_byte_string(input);

	if (blah.channel1 != 0b110111111111) {
		printf("failed test case %d , part1 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (blah.channel2 != 0b000000001000) {
		printf("failed test case %d , part2 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (blah.channel3 != 0b111011111111) {
		printf("failed test case %d , part3 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (blah.channel4 != 0b000101000000) {
		printf("failed test case %d , part4 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (blah.channel5 != 0b111011110111) {
		printf("failed test case %d , part5 \n",i+1);
		exit(EXIT_FAILURE);
	}
	printf("Case %d Passed!\n",i+1);

}

void T_packup_pwms_byte_string(void) {

	printf("Running T_packup_pwms_byte_string\n");
	int i = 0;

	//TEST CASE 1

	uint8_t result[8];

	PWMChannels pwms;

	pwms.channel1 = 0b000001010101;
	pwms.channel2 = 0b111111111111;
	pwms.channel3 = 0b000110001100;
	pwms.channel4 = 0b000000000000;
	pwms.channel5 = 0b100001000100;

	packup_pwms_byte_string(pwms,&result,8);

	if (result[0]!=  0b00000101) {
		printf("got %x\n",result[0]);
		printf("failed test case %d , part1 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[1]!=  0b01011111) {
		printf("got %x\n",result[1]);
		printf("failed test case %d , part2 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[2]!=  0b11111111) {
		printf("got %x\n",result[2]);
		printf("failed test case %d , part3 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[3]!=  0b00011000) {
		printf("got %x\n",result[3]);
		printf("failed test case %d , part4 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[4]!=  0b11000000) {
		printf("got %x\n",result[4]);
		printf("failed test case %d , part5 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[5]!=  0b00000000) {
		printf("got %x\n",result[5]);
		printf("failed test case %d , part6 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[6]!=  0b10000100) {
		printf("got %x\n",result[6]);
		printf("failed test case %d , part7 \n",i+1);
		exit(EXIT_FAILURE);
	} else if ((result[7]>>4)!=  0b0100) {
		printf("got %x\n",result[7]);
		printf("failed test case %d , part8 \n",i+1);
		exit(EXIT_FAILURE);
	}
	printf("Case %d Passed!\n",i+1);


	//TEST CASE 2
	i = 1;

	pwms.channel1 = 0b011001010100;
	pwms.channel2 = 0b111100011111;
	pwms.channel3 = 0b000110001100;
	pwms.channel4 = 0b010000000001;
	pwms.channel5 = 0b100111000100;

	packup_pwms_byte_string(pwms,&result,8);

	if (result[0]!=  0b01100101) {
		printf("got %x\n",result[0]);
		printf("failed test case %d , part1 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[1]!=  0b01001111) {
		printf("got %x\n",result[1]);
		printf("failed test case %d , part2 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[2]!=  0b00011111) {
		printf("got %x\n",result[2]);
		printf("failed test case %d , part3 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[3]!=  0b00011000) {
		printf("got %x\n",result[3]);
		printf("failed test case %d , part4 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[4]!=  0b11000100) {
		printf("got %x\n",result[4]);
		printf("failed test case %d , part5 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[5]!=  0b00000001) {
		printf("got %x\n",result[5]);
		printf("failed test case %d , part6 \n",i+1);
		exit(EXIT_FAILURE);
	} else if (result[6]!=  0b10011100) {
		printf("got %x\n",result[6]);
		printf("failed test case %d , part7 \n",i+1);
		exit(EXIT_FAILURE);
	} else if ((result[7]>>4)!=  0b0100) {
		printf("got %x\n",result[7]);
		printf("failed test case %d , part8 \n",i+1);
		exit(EXIT_FAILURE);
	}
	printf("Case %d Passed!\n",i+1);

}

