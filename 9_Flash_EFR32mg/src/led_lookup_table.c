/*
 * led_lookup_table.c
 *
 *  Created on: Feb 22, 2018
 *      Author: starrover
 */


#include "AT45drivers.h"


#include "lighting_functions.h"

//this first version is just hue/sat, formula for hue/sat to address

//get_address_from_hue_sat is a function to grab the address in flash given hue/sat
/* The addressing goes as follows:
 *  each hue/sat requires 12*5 = 60 bits. Which is 7.5 bytes, so require 8 bytes per point
 *  1024 pages in the at48 w/ 256 bytes per page = 262,144 bytes
 * at 8 bytes per point = 32768 possible points
 * squareroot = 181.0193359
 * so 181 points for a 'square' lookup table
 *
 *range of hue and sat:
 */

PWMChannels get_pwm_from_hue_sat(uint16_t hue, uint8_t sat){

	int num_bytes = NUM_BYTES_PER_POINT;
	uint32_t address;
	uint8_t result[num_bytes];

	address = get_address_from_hue_sat(hue,sat);
	at45_read_memory(address,result,num_bytes);

	PWMChannels output;

	output = get_channel_values_from_byte_string(result);

	return output;
}

void write_pwm_write_vs_hue_sat(uint16_t hue, uint8_t sat, PWMChannels pwms){

	int num_bytes = NUM_BYTES_PER_POINT;
	uint32_t address;
	uint8_t result[num_bytes];

	address = get_address_from_hue_sat(hue,sat);

	//packs the pwm byte strings into result array
	packup_pwms_byte_string(pwms,&result,num_bytes);

	at45_write_memory(address,result,num_bytes);
}

