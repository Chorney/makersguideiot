/*
 * main3.c
 *
 *  Created on: Feb 13, 2018
 *      Author: starrover
 *      This code works with the external flash drive AT45DB021E from adesto
 */
#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"
//#include "utilities.h"
#include "AT45drivers.h"

#define JEDEC_ID_CMD	0x9F

#include "spidrv.h"

#define DEBUG_BREAK		__asm__("BKPT #0");

extern SPIDRV_Handle_t handle;

void spidrv_setup()
{
      // Set up the necessary peripheral clocks
      CMU_ClockEnable(cmuClock_GPIO, true);

//      GPIO_DriveModeSet(gpioPortD, gpioDriveModeLow);

      // Enable the GPIO pins for the misc signals, leave pulled high
//      GPIO_PinModeSet(gpioPortD, 4, gpioModePushPullDrive, 1);          // WP#
//      GPIO_PinModeSet(gpioPortD, 5, gpioModePushPullDrive, 1);          // HOLD#

      // Initialize and enable the SPIDRV
      SPIDRV_Init_t initData = SPIDRV_MASTER_USART1_CUSTOM;
      initData.clockMode = spidrvClockMode3;

      // Initialize a SPI driver instance
      SPIDRV_Init( handle, &initData );
}

int main(void)
{
      CHIP_Init();

      spidrv_setup();

//      setup_utilities();

      int i;
      	int b=0;
      	for( i = 0; i < 1000000; i++ ){
      		b++;
      	}

//      delay(100);

      uint8_t result[PAGE_SIZE];
      uint8_t tx_data[PAGE_SIZE];

      tx_data[0] = JEDEC_ID_CMD;

      SPIDRV_MTransferB( handle, &tx_data, &result, 4);

      // Check the result for what is expected from the Spansion spec
      if (result[1] != 1 || result[2] != 0x40 || result[3] != 0x13)
      {
//            DEBUG_BREAK
      }

//	  at45_chip_erase();
      uint8_t valuebefore;
      uint8_t valueafter;
      valuebefore = at45_read_memory_address(0,18); //pagenumber,address

      uint8_t input_array[20] = {12,12,12,12,12,10,10,10,15,15,15,15,12,11,9,1,1,1,1,1};

//      DEBUG_BREAK;

      uint8_t read_array_before[41];
      uint8_t read_array_after[20];

      at45_read_memory(0,&read_array_before,41);
      at45_write_memory(0,input_array,20);
      at45_read_memory(0,&read_array_after,20);

      DEBUG_BREAK;

      while (1)
            ;
}
