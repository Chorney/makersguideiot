#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"

#include "InitDevice.h"

#define JEDEC_ID_CMD	0x9F

#define DEBUG_BREAK		__asm__("BKPT #0");

//Pins
//PD10 - P4 - Hold/reset. Note have better success permanenatly holding the RES pin high
		// Definitely some spurious activity happening with the efr32mg
//PD11 - P6 - WP

//*****The A ports configuration

//PA3 - P2 - CS
//PA0 - P9 - TX
//PA1 - P11 - RX
//PA2 - P0 - CLK

//****In the C configuration

//PC6 - P1 - TX
//PC7 - P3 - RX
//PC8 - P5 - CLK
//PC9 - P7 - CS

//****Check other configuration as onboard external FLASH may be interfering with signal
// in the C configuration

//PD10 - P4 - CS
//PD11 - P6 - TX
//PD12 - P8 - RX
//PD13 - P31 - CLK

//in order for this to run, need to disable CS control, eitherwise manual
// control of the CS pin doesn't work.


//**// Presently in the 3rd configuration
int main(void)
{
  /* Chip errata */
//  CHIP_Init();
	enter_DefaultMode_from_RESET();

	//manually set these by connecting to the 3.3v direct
//	GPIO_PinModeSet(gpioPortD, 10, gpioModePushPull, 1);         // WP#
//	GPIO_PinModeSet(gpioPortD, 11, gpioModePushPull, 1);         // HOLD#



//	delay(100);
	int i;
	int b=0;
	for( i = 0; i < 1000000; i++ ){
		b++;
	}

	uint8_t result[3];
	uint8_t index = 0;

	GPIO_PinModeSet(gpioPortD, 10, gpioModePushPull, 0);

	// Send the command, discard the first response
//	while (1) {
	USART_SpiTransfer(USART0, JEDEC_ID_CMD);
//	}
	// Now send garbage, but keep the results
	result[index++] = USART_SpiTransfer(USART0, 0);
	result[index++] = USART_SpiTransfer(USART0, 0);
	result[index++] = USART_SpiTransfer(USART0, 0);

	GPIO_PinModeSet(gpioPortD, 10, gpioModePushPull, 1);

	// Check the result for what is expected from the Spansion spec
	if (result[0] != 1 || result[1] != 0x40 || result[2] != 0x13)
	{
		DEBUG_BREAK
	}

  /* Infinite loop */
  while (1) {
  }
}
