/*
 * main_2.c
 *
 *  Created on: Feb 12, 2018
 *      Author: starrover
 */

#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"

#include "InitDevice.h"
#include "AT45drivers.h"


#include "spidrv.h"

#define JEDEC_ID_CMD	0x9F

#define DEBUG_BREAK		__asm__("BKPT #0");

extern SPIDRV_Handle_t handle;


// COuldn't get to work with the EFR32 mighty gecko, checking with oscilliscope
// don't see any clock cycle

#define SPIDRV_MASTER_USART1_CUSTOM                                              \
{                                                                         \
  USART1,                       /* USART port                       */    \
  _USART_ROUTELOC0_TXLOC_LOC19, /* USART Tx pin location number     */    \
  _USART_ROUTELOC0_RXLOC_LOC19, /* USART Rx pin location number     */    \
  _USART_ROUTELOC0_CLKLOC_LOC19,/* USART Clk pin location number    */    \
  _USART_ROUTELOC0_CSLOC_LOC15, /* USART Cs pin location number     */    \
  1000000,                      /* Bitrate                          */    \
  8,                            /* Frame length                     */    \
  0,                            /* Dummy tx value for rx only funcs */    \
  spidrvMaster,                 /* SPI mode                         */    \
  spidrvBitOrderMsbFirst,       /* Bit order on bus                 */    \
  spidrvClockMode0,             /* SPI clock/phase mode             */    \
  spidrvCsControlAuto,          /* CS controlled by the driver      */    \
  spidrvSlaveStartImmediate     /* Slave start transfers immediately*/    \
}

void spidrv_setup()
{
      // Set up the necessary peripheral clocks
      CMU_ClockEnable(cmuClock_GPIO, true);

//      GPIO_DriveModeSet(gpioPortD, gpioDriveModeLow);

      // Enable the GPIO pins for the misc signals, leave pulled high
//      GPIO_PinModeSet(gpioPortD, 10, gpioModePushPull, 1);         // WP#
//      GPIO_PinModeSet(gpioPortD, 11, gpioModePushPull, 1);         // HOLD#
      //manullay set high

      // Initialize and enable the SPIDRV

      SPIDRV_Init_t initData = SPIDRV_MASTER_USART1_CUSTOM;
      initData.clockMode = spidrvClockMode3;

      // Initialize a SPI driver instance
      SPIDRV_Init( handle, &initData );
}

int main(void)
{
  /* Chip errata */
//	CHIP_Init();
	enter_DefaultMode_from_RESET();

	spidrv_setup();

//	delay(100);

	int i;
	int b=0;
	for( i = 0; i < 1000000; i++ ){
		b++;
	}

	uint8_t result[4];
	uint8_t tx_data[4];

	tx_data[0] = JEDEC_ID_CMD;

//	while (1) {
	SPIDRV_MTransferB( handle, &tx_data, &result, 4);
//	}
	if (result[0] != 1 || result[1] != 0x40 || result[2] != 0x13)
	{
		DEBUG_BREAK
	}


  /* Infinite loop */
  while (1) {
  }
}
