/*
 * led_lookup_table.h
 *
 *  Created on: Feb 22, 2018
 *      Author: starrover
 */

#ifndef INC_LED_LOOKUP_TABLE_H_
#define INC_LED_LOOKUP_TABLE_H_

#include "lighting_functions.h"


PWMChannels get_pwm_from_hue_sat(uint16_t hue, uint8_t sat);
void write_pwm_write_vs_hue_sat(uint16_t hue, uint8_t sat, PWMChannels pwms);


#endif /* INC_LED_LOOKUP_TABLE_H_ */
