/*
 * lighting_functions.h
 *
 *  Created on: Feb 23, 2018
 *      Author: starrover
 */

#ifndef INC_LIGHTING_FUNCTIONS_H_
#define INC_LIGHTING_FUNCTIONS_H_

#define HUE_LOWER 0.0
#define HUE_HIGHER 360.0

#define SAT_LOWER 0
#define SAT_HIGHER 100


//THIS is commented out as I had started to create functions for 8 mbit drivers, but have decided
//to migrate to a different project folder as not to convolute makers guide stuff

//#define BRIGHT_LOWER 0
//#define BRIGHT_HIGHER 100
//
////This should be kept the same as the python drivers as writing/reading depends on these values
//#define NUM_POINTS_BRIGHT 50
//#define NUM_POINTS_SAT 26
//#define NUM_POINTS_HUE 100

#define NUM_BYTES_PER_POINT 8

//This is for the old atel 2kb flash driver
#define NUM_POINTS_SQAURE_ARRAY 181


typedef struct PWMChannelsTag
{
  uint16_t            channel1;      /// channel 1
  uint16_t            channel2;      /// channel 2
  uint16_t            channel3;      /// channel 3
  uint16_t            channel4;      /// channel 4
  uint16_t            channel5;      /// channel 5
} PWMChannels;

PWMChannels get_channel_values_from_byte_string(uint8_t input[]);

#endif /* INC_LIGHTING_FUNCTIONS_H_ */
