/*
 * AT45drivers.h
 *
 *  Created on: Feb 22, 2018
 *      Author: starrover
 */
#ifndef __AT45_DRIVERS_H__
#define __AT45_DRIVERS_H__

#include "spidrv.h"

#define STATUS                0xD7
#define WRITE_BUFFER          0x84
#define BUFFER_TO_MEMORY	  0x83
#define READ_MEMORY           0xD2

#define CHIP_ERASE_BYTE1      0xC7
#define CHIP_ERASE_BYTE2 	  0x94
#define CHIP_ERASE_BYTE3 	  0x80
#define CHIP_ERASE_BYTE4	  0x9A

#define CONT_ARRAY_READ		  0xE8

#define PAGE_SIZE             256

#ifndef DEBUG_BREAK
	#define DEBUG_BREAK	__asm__("BKPT #0");
#endif

#define SPIDRV_MASTER_USART1_CUSTOM                                              \
{                                                                         \
  USART1,                       /* USART port                       */    \
  _USART_ROUTELOC0_TXLOC_LOC19, /* USART Tx pin location number     */    \
  _USART_ROUTELOC0_RXLOC_LOC19, /* USART Rx pin location number     */    \
  _USART_ROUTELOC0_CLKLOC_LOC19,/* USART Clk pin location number    */    \
  _USART_ROUTELOC0_CSLOC_LOC15, /* USART Cs pin location number     */    \
  1000000,                      /* Bitrate                          */    \
  8,                            /* Frame length                     */    \
  0,                            /* Dummy tx value for rx only funcs */    \
  spidrvMaster,                 /* SPI mode                         */    \
  spidrvBitOrderMsbFirst,       /* Bit order on bus                 */    \
  spidrvClockMode0,             /* SPI clock/phase mode             */    \
  spidrvCsControlAuto,          /* CS controlled by the driver      */    \
  spidrvSlaveStartImmediate     /* Slave start transfers immediately*/    \
}

typedef struct StatusBytes
{
  uint8_t            statusbyte1;          /// byte1 (MSB)
  uint8_t            statusbyte2;      /// byte2
} StatusByte;

extern SPIDRV_HandleData_t handleData;
extern SPIDRV_Handle_t handle;



#endif // __AT45_DRIVERS_H__
