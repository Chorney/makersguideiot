#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "utilities.h"

#define TEST_JUMPER_PORT      gpioPortD
#define TEST_JUMPER_PIN       10

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
      // Chip errata
      CHIP_Init();

      setup_utilities();

      CMU_ClockEnable(cmuClock_GPIO, true);

      // Set up the user interface buttons
      GPIO_PinModeSet(TEST_JUMPER_PORT, TEST_JUMPER_PIN, gpioModeInput,  0);

      bool pressed = false;
      int number_of_presses __attribute__((unused)) = 0;

      while (1)
      {
            if (GPIO_PinInGet(TEST_JUMPER_PORT, 9))
            {
                  if (pressed == false)
                  {
                        number_of_presses++;
                        pressed = true;
                  }
            }
            else
            {
                  pressed = false;
            }
      }
}
