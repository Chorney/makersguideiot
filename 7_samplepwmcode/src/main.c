#include <stdlib.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_gpio.h"
#include "display.h"
#include "em_rtcc.h"
#include "graphics.h"
#include "em_gpio.h"
#include "em_pcnt.h"

/* Frequency of RTC clock. */
#define RTC_FREQUENCY    (128)

/* RTC callback parameters. */
//static void (*rtccCallback)(void*) = 0;
//static void * rtccCallbackArg         = 0;

#define END_OF_DAY	60*60*24

#define LED_PORT	gpioPortD
#define LED_PIN0	10
#define LED_PIN1	11
#define LED_PIN2	12

#define BUTTON_PORT					gpioPortF
#define SET_BUTTON1_PIN				6
#define SET_BUTTON2_PIN				7

#define DEBUG_BREAK 						__asm__("BKPT #0");
#define ONE_MILLISECOND_BASE_VALUE_COUNT 	1000
#define ONE_SECOND_TIMER_COUNT				13672
#define MILLISECOND_DIVISOR					13.672

uint64_t base_value = 0;
bool timer1_overflow = false;

#define TIMER_TOP				100
#define TIMER_CHANNEL			2

#define SPEED_MULTIPLIER		25
#define RAMP_UP_TIME_MS			100 * SPEED_MULTIPLIER
#define RAMP_DOWN_TIME_MS		100 * SPEED_MULTIPLIER
#define HIGH_DURATION_MS		100 * SPEED_MULTIPLIER
#define LOW_DURATION_MS			0
#define MAX_BRIGHTNESS			100
#define MIN_BRIGHTNESS			0

#define OVERLAP					125 * SPEED_MULTIPLIER

#define START_OFFSET_MS			RAMP_UP_TIME_MS + RAMP_DOWN_TIME_MS + HIGH_DURATION_MS + LOW_DURATION_MS - OVERLAP
#define DELTA					MAX_BRIGHTNESS - MIN_BRIGHTNESS
#define DEBOUNCE_TIME         300  // ms

enum mode_values { NOT_STARTED, RAMPING_UP, HIGH, RAMPING_DOWN, LOW};

enum segment_enum { NONE, TIME, HOURS, MINUTES };
// Storage for the time currently on the display
uint16_t display_hours = 0;
uint16_t display_minutes = 0;

// Storage for the program-to-program event memory
uint16_t start_hours = 12;
uint16_t start_minutes = 0;
uint16_t stop_hours = 12;
uint16_t stop_minutes = 0;

// Set the initial conditions for all LEDs
uint16_t mode[3] = {NOT_STARTED, NOT_STARTED, NOT_STARTED};
uint64_t next_step[3];
uint16_t brightness[3] = {MIN_BRIGHTNESS, MIN_BRIGHTNESS, MIN_BRIGHTNESS};
uint64_t mode_timeout[3];

volatile uint32_t msTicks = 0;
bool pressed1 = false;
bool pressed2 = false;
int debounce_pressed1_timeout = 0;
int debounce_released1_timeout = 0;
int debounce_pressed2_timeout = 0;
int debounce_released2_timeout = 0;

#define BLINK_HALF_PERIOD_MS        500
// Blink state storage
uint16_t blink_segment = NONE;
uint32_t blink_timer = 0;
bool blink_state = 1;

static volatile uint32_t curTime = 0;

 /* PCNT interrupt counter */
static volatile int pcntIrqCount = 0;

// Button states
typedef struct button_struct_type
{
      bool short_press;
      bool long_press;
      bool extra_long_press;
      uint32_t time_pressed;
      uint32_t debounce_timeout;
      uint16_t pin;
} button_struct ;

button_struct button1;
button_struct button2;

// Third button is virtual, formed by pressing two together
button_struct program_button;

button_struct * button_array[3];

#define BUTTON1_INDEX         0
#define BUTTON2_INDEX         1
#define PROGRAM_BUTTON_INDEX  2

#define LONG_PRESS_THRESH           1000
#define EXTRA_LONG_PRESS_THRESH     3000

#define BUTTON_DELAY			300
#define FASTER_BUTTON_DELAY	100

void TIMER1_IRQHandler(void)
{
	timer1_overflow = true;
	base_value += ONE_MILLISECOND_BASE_VALUE_COUNT;
	TIMER_IntClear(TIMER1, TIMER_IF_OF);
}

void delay(uint32_t milliseconds)
{
	uint32_t start = msTicks;
	while ((start + milliseconds) > msTicks)
		;
}

uint64_t get_time_in_ms()
{
	// Clear our overflow indicator
	timer1_overflow = false;

	// Get the current count
	uint16_t count = TIMER1->CNT;

	// Get a copy of the base value
	uint64_t copy_of_base_value = base_value;

	// Now check to make sure that the ISR didn't fire while in here
	// If it did, then grab the values again
	if (timer1_overflow)
	{
		count = TIMER1->CNT;
		copy_of_base_value = base_value;
	}

	// Now calculate the number of milliseconds the program has run
	return copy_of_base_value + count / MILLISECOND_DIVISOR;
}

// Pass in the elapsed time before it times out, returns the future time
int32_t set_timeout_ms(int32_t timeout_ms)
{
	return msTicks + timeout_ms;
}

int32_t expired_ms(int32_t timeout_ms)
{
	if (timeout_ms < msTicks)
	{
		return true;
	}
	return false;
}

void delay_ms(uint64_t milliseconds)
{
	uint64_t trigger_time = get_time_in_ms() + milliseconds;
	while (get_time_in_ms() < trigger_time)
		;
}

int RtccIntCallbackRegister(void (*pFunction)(void*),
                           void* argument,
                           unsigned int frequency)
{
  /* Verify that the requested frequency is the same as the RTC frequency.*/
  if (RTC_FREQUENCY != frequency)
  {
	  DEBUG_BREAK;
    return -1;
  }

//  rtccCallback    = pFunction;
//  rtccCallbackArg = argument;

  return 0;
}

typedef struct rtc_struct_type
{
      uint32_t timer_start_seconds;
      uint32_t timer_stop_seconds;
} rtc_struct;

rtc_struct time_keeper;

// This is the timekeeping clock
void setup_rtc()
{
      // Ensure LE modules are accessible

      CMU_ClockEnable(cmuClock_CORELE, true);

      CMU_LFXOInit_TypeDef lfxoInit = CMU_LFXOINIT_DEFAULT;
      CMU_LFXOInit(&lfxoInit);


      // Enable LFACLK in CMU (will also enable oscillator if not enabled)
      CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
      CMU_OscillatorEnable(cmuOsc_LFXO, true, true);

      // Use the prescaler to reduce power consumption. 2^15
      CMU_ClockDivSet(cmuClock_RTCC, cmuClkDiv_32768);

      // Enable clock to RTC module
      CMU_ClockEnable(cmuClock_RTCC, true);

      // Set up the Real Time Clock as long-time timekeeping
      RTCC_Init_TypeDef init = RTCC_INIT_DEFAULT;
//      init.precntWrapOnCCV0 = false;
      RTCC_Init(&init);


      // Enabling Interrupt from RTCC
      NVIC_EnableIRQ(RTCC_IRQn);

}

// Use buttons to set time, start, stop
// Stores those values to memory
uint16_t get_time(uint16_t segment)
{
	if (segment == HOURS)
	{
		return 0; // RTCC->CNT / 3600;  	// Translate seconds in RTC->CNT to hours
	}
	else if (segment == MINUTES)
	{
		uint16_t leftover_seconds = 0; //RTCC->CNT % 3600;
		return leftover_seconds / 60;
	}
	DEBUG_BREAK;
	return 0;
}

void program_timer()
{
	static bool initial_programming = true;

	// Disable RTC interrupts while in here...
//	RTCC_IntDisable(RTCC_IF_CC0);
//	RTCC_IntDisable(RTCC_IF_CC1);

	delay(BUTTON_DELAY);

//	DEBUG_BREAK;

	for (int i=0; i < 3; i++)
	{
		if (i == 0)
		{
//			SegmentLCD_Write("Clock");
		}
		else if (i == 1)
		{
//			SegmentLCD_Write("Start");
			if (initial_programming)
			{
				start_hours = display_hours;
				start_minutes = display_minutes;
			}
			display_hours = start_hours;
			display_minutes = start_minutes;
		}
		else if (i == 2)
		{
//			SegmentLCD_Write("Stop");
			if (initial_programming)
			{
				stop_hours = start_hours;
				stop_minutes = start_minutes;
			}
			display_hours = stop_hours;
			display_minutes = stop_minutes;
		}

		display_time();
		blink(HOURS);
		delay(BUTTON_DELAY);

		// Set the hours
		while (!button1.short_press)
		{
			if (button2.short_press)
			{
				display_hours++;
				if (display_hours > 23)
				{
					display_hours = 1;
				}
				display_time();
				// Delay so that we don't get double presses
				delay(BUTTON_DELAY);
			}
		}

		display_time();
		blink(MINUTES);
		delay(BUTTON_DELAY);

		// Set the minutes
		while (!button1.short_press)
		{
			if (button2.short_press)
			{
				display_minutes++;
				if (display_minutes > 59)
				{
					display_minutes = 0;
				}
				display_time();
				// Delay so that we don't get double presses
				delay(FASTER_BUTTON_DELAY);
			}
		}

		// Commit the clock hours and minutes to memory for the given index
		set_clock_time(i, display_hours, display_minutes);
	}

//	DEBUG_BREAK;
	display_hours = get_time(HOURS);
	display_minutes = get_time(MINUTES);
	blink(NONE);
	display_time();
	// Delay so that we don't get double presses
	delay(BUTTON_DELAY);

	initial_programming = false;

	// Trigger interrupt every 60 seconds to update the time
//	RTCC_ChannelCCVSet(1, RTCC->CNT + 60);

	// Now that programming is done, clear and enable interrupts
//	DEBUG_BREAK;
//	RTCC_IntClear(RTCC_IF_CC0);
//	RTCC_IntClear(RTCC_IF_CC1);
//	RTCC_IntEnable(RTCC_IF_CC0);
//	RTCC_IntEnable(RTCC_IF_CC1);

//	DEBUG_BREAK;
}



// Stores the hours/mins for clock time, start time, or stop time
void set_clock_time(int index, uint16_t hours, uint16_t minutes)
{
	// Set the time clock
	if (index == 0)
	{
		// Midnight is time zero
//		RTCC_Reset();
		// 3600 seconds per hour, 60 seconds per minute
//		RTCC->CNT = hours * 3600 + minutes * 60;
	}
	else if (index == 1)
	{
		// Add 1 second so that RTC interrupt source is clear,
		// and not to confuse with the clock minute updates
		time_keeper.timer_start_seconds = hours * 3600 + minutes * 60 + 1;

		// Set up the RTC to trigger a start event
//		RTCC_ChannelCCVSet(0, time_keeper.timer_start_seconds);

		// Save for next program event
		start_hours = hours;
		start_minutes = minutes;
	}
	else if (index == 2)
	{
		// Add 1 second so that RTC interrupt source is clear,
		// and not to confuse with the clock minute updates
		time_keeper.timer_stop_seconds = hours * 3600 + minutes * 60 + 1;

		// Save for next program event
		stop_hours = hours;
		stop_minutes = minutes;
	}
}

void start_fade(int led_id)
{
	if (mode[led_id] != NOT_STARTED)
	{
//		DEBUG_BREAK
	}

	uint64_t start_time = get_time_in_ms();
	next_step[led_id] = RAMP_UP_TIME_MS / DELTA + start_time;
	mode_timeout[led_id] = start_time + RAMP_UP_TIME_MS;
	brightness[led_id] = MIN_BRIGHTNESS;
	if (pressed1==true)
		{
		TIMER_CompareBufSet(TIMER0, led_id, brightness[led_id]);
		}
	else {
		TIMER_CompareBufSet(TIMER0, led_id, 0);
	}
	mode[led_id] = RAMPING_UP;
}

void title_screen()
{
	  char greeting[] = "";
	  char greeting3[] = "Hello\n";
	  char greeting2[] = "Mother Fucker\n";
	//  char greeting3[] = "\n";
	  char greeting4[] = "Hit PB0 to start/stop\n";
	  char greeting5[] = "LEDs\n";

	  int line_num = 0;

	  graphInit(greeting);
	  graphClear();
	  graphWriteString(greeting3,line_num);
	  line_num++;
	  graphWriteString(greeting2,line_num);
	  line_num++;
	  graphWriteString(greeting4,line_num);
	  line_num++;
	  graphWriteString(greeting5,line_num);
}

//void set_clock_time(const uint16_t set_hours, const uint16_t set_minutes)
//{
//	 display_hours = set_hours;
//	 display_minutes = set_minutes;
//}
// Stores the hours/mins for clock time, start time, or stop time

void display_time()
{
	char greeting[] = "";
	char greeting3[] = "Hello\n";
	char greeting2[] = "Mother Fucker\n";
		//  char greeting3[] = "\n";
	char greeting4[] = "Hit PB0 to start/stop\n";
	char greeting5[] = "LEDs\n";
	char greeting6[] = "Clock Time\n";

      char greeting7[15];
      sprintf(greeting7, "%d", display_hours);
      char greeting8[15];
      sprintf(greeting8, "%d", display_minutes);

      int line_num = 0;

      graphClear();
	  graphWriteString(greeting3,line_num);
	  line_num++;
	  graphWriteString(greeting2,line_num);
	  line_num++;
	  graphWriteString(greeting4,line_num);
	  line_num++;
	  graphWriteString(greeting5,line_num);
	  line_num++;
	  graphWriteString(greeting6,line_num);
	  line_num++;
	  graphWriteString(greeting7,line_num);
	  line_num++;
	  graphWriteString(greeting8,line_num);
}

void display_hour()
{
	char greeting[] = "";
	char greeting3[] = "Hello\n";
	char greeting2[] = "Mother Fucker\n";
		//  char greeting3[] = "\n";
	char greeting4[] = "Hit PB0 to start/stop\n";
	char greeting5[] = "LEDs\n";
	char greeting6[] = "Clock Time\n";

      char greeting7[15];
      sprintf(greeting7, "%d", display_hours);

      int line_num = 0;

      graphClear();
	  graphWriteString(greeting3,line_num);
	  line_num++;
	  graphWriteString(greeting2,line_num);
	  line_num++;
	  graphWriteString(greeting4,line_num);
	  line_num++;
	  graphWriteString(greeting5,line_num);
	  line_num++;
	  graphWriteString(greeting6,line_num);
	  line_num++;
	  graphWriteString(greeting7,line_num);
}

void display_minute()
{
	char greeting[] = "";
	char greeting3[] = "Hello\n";
	char greeting2[] = "Mother Fucker\n";
		//  char greeting3[] = "\n";
	char greeting4[] = "Hit PB0 to start/stop\n";
	char greeting5[] = "LEDs\n";
	char greeting6[] = "Clock Time\n";

      char greeting7[] = "";
      char greeting8[15];
      sprintf(greeting8, "%d", display_minutes);

      int line_num = 0;

      graphClear();
	  graphWriteString(greeting3,line_num);
	  line_num++;
	  graphWriteString(greeting2,line_num);
	  line_num++;
	  graphWriteString(greeting4,line_num);
	  line_num++;
	  graphWriteString(greeting5,line_num);
	  line_num++;
	  graphWriteString(greeting6,line_num);
	  line_num++;
	  graphWriteString(greeting7,line_num);
	  line_num++;
	  graphWriteString(greeting8,line_num);
}

// Hide the time segment given by segment_enum
void hide_segment(uint16_t segment)
{
      if (segment == TIME)
      {
            title_screen();
      }
      else if (segment == HOURS)
      {
    	  display_hour();

      }
      else if (segment == MINUTES)
      {
            display_minute();
      }
}

// Hide the time segment given by segment_enum

// blink Time
void blink(uint16_t segment)
{
      if (segment)
      {
            blink_timer = set_timeout_ms(BLINK_HALF_PERIOD_MS);
            blink_segment = segment;
      }
      else
      {
            blink_segment = NONE;
            display_time();
      }
}

// If a button is low, that means it is pressed, so return true
bool get_button1()
{
	if (GPIO_PinInGet(BUTTON_PORT, SET_BUTTON1_PIN))
	{
		return false;
	}
	return true;
}

bool get_button2()
{
	if (GPIO_PinInGet(BUTTON_PORT, SET_BUTTON2_PIN))
	{
		return false;
	}
	return true;
}

void SysTick_Handler(void)
{
	msTicks++;
	uint64_t curr_time = get_time_in_ms();

	// Handle blinking segments
  if (blink_segment)
  {
		if (expired_ms(blink_timer))
		{
			  if (blink_state)
			  {
					hide_segment(blink_segment);
			  }
			  else
			  {
					display_time();
			  }
			  blink_state = !blink_state;
			  blink_timer = set_timeout_ms(BLINK_HALF_PERIOD_MS);
		}
  }

//	if (get_button1())
//		{
//			  if (pressed1 == false && expired_ms(debounce_released1_timeout))
//			  {
//					// You could start some process here on the initial event
//					// and it would be immediate
////					number_of_presses++;
//					pressed1 = !pressed1;
//					debounce_pressed1_timeout = set_timeout_ms(DEBOUNCE_TIME);
//			  }
//			  else if (pressed1 == true && expired_ms(debounce_pressed1_timeout))
//			  {
//					// You could start some process here on the release event
//					// and it would be immediate
//					pressed1 = !pressed1;
//					debounce_released1_timeout = set_timeout_ms(DEBOUNCE_TIME);
//			  }
//		}
//
//	if (get_button2())
//			{
//				  if (pressed2 == false && expired_ms(debounce_released1_timeout))
//				  {
//						// You could start some process here on the initial event
//						// and it would be immediate
//	//					number_of_presses++;
//						pressed2 = !pressed2;
//						display_time();
//						blink(NONE);
//						debounce_pressed1_timeout = set_timeout_ms(DEBOUNCE_TIME);
//				  }
//				  else if (pressed2 == true && expired_ms(debounce_pressed1_timeout))
//				  {
//						// You could start some process here on the release event
//						// and it would be immediate
//						pressed2 = !pressed2;
//						blink(HOURS);
//						debounce_released1_timeout = set_timeout_ms(DEBOUNCE_TIME);
//				  }
//			}

  // Keep track of button presses
	// High state is unpressed.  Low state is pressed
	// Only need to loop over the two physical buttons
	for (int i=0; i < 2; i++)
	{
		  // A button high means it is unpressed, so clear things
		  if (GPIO_PinInGet(BUTTON_PORT, button_array[i]->pin))
		  {
				button_array[i]->short_press = false;
				button_array[i]->long_press = false;
				button_array[i]->time_pressed = 0;
				button_array[i]->debounce_timeout =0;
		  }
		  else  // A button was found low, meaning pressed
		  {
				// No need to do anything more if already found long_press
				if (!button_array[i]->long_press)
				{
					  // Only need to fire short_press once
					  if (!button_array[i]->time_pressed)
					  {
							button_array[i]->short_press = true;
					  }

					  button_array[i]->time_pressed++;

					  // Now check to see if we have a new long press
					  if (button_array[i]->time_pressed > LONG_PRESS_THRESH)
					  {
							button_array[i]->long_press = true;
							button_array[i]->debounce_timeout = set_timeout_ms(DEBOUNCE_TIME);
							pressed1 = !pressed1;
					  }
				}
		  }
	}

	// Set a long press on the program button if other buttons are both long
    if (button1.long_press && button2.long_press)
    {
		program_button.long_press = true;
    }
//    else if (button1.long_press && !button2.long_press && expired_ms(button1.debounce_timeout)) {
//    	pressed1 = !pressed1;
//    	program_button.long_press = false;
//    }
    else
    {
 		program_button.long_press = false;
    }


	for(int i=0; i<3; i++)
	{

		switch (mode[i])
		{
		case NOT_STARTED:
			continue;
			break;
		case RAMPING_UP:
			if (next_step[i] <= curr_time)
			{
				next_step[i] = RAMP_UP_TIME_MS / DELTA + curr_time;
				if (pressed1==true)
				{
				TIMER_CompareBufSet(TIMER0, i, brightness[i]++);
				}else
				{
					TIMER_CompareBufSet(TIMER0, i, 0);
				}
			}
			if (mode_timeout[i] <= curr_time)
			{
				mode[i] = HIGH;
				mode_timeout[i] = curr_time + HIGH_DURATION_MS;
				brightness[i] = MAX_BRIGHTNESS;
				if (pressed1==true)
				{
				TIMER_CompareBufSet(TIMER0, i, brightness[i]);
				}else
				{
					TIMER_CompareBufSet(TIMER0, i, 0);
				}
			}
			break;
		case HIGH:
			if (mode_timeout[i] <= curr_time)
			{
				mode[i] = RAMPING_DOWN;
				if (pressed1==true)
					{
				TIMER_CompareBufSet(TIMER0, i, brightness[i]--);
					}else
									{
										TIMER_CompareBufSet(TIMER0, i, 0);
									}
				next_step[i] = RAMP_DOWN_TIME_MS / DELTA + curr_time;
				mode_timeout[i] = curr_time + RAMP_DOWN_TIME_MS;
			}
			break;
		case RAMPING_DOWN:
			if (next_step[i] <= curr_time)
			{
				next_step[i] = RAMP_DOWN_TIME_MS / DELTA + curr_time;
				if (pressed1==true)
				{
				TIMER_CompareBufSet(TIMER0, i, brightness[i]--);
				}else
				{
					TIMER_CompareBufSet(TIMER0, i, 0);
				}
			}
			if (mode_timeout[i] <= curr_time)
			{
				mode[i] = LOW;
				brightness[i] = MIN_BRIGHTNESS;
				if (pressed1==true)
				{
				TIMER_CompareBufSet(TIMER0, i, brightness[i]);
				}else
				{
					TIMER_CompareBufSet(TIMER0, i, 0);
				}
				mode_timeout[i] = curr_time + LOW_DURATION_MS;
			}
			break;
		case LOW:
			if (mode_timeout[i] <= curr_time)
			{
				mode[i] = NOT_STARTED;
			}
			break;
		}
	}
}

void PCNT0_IRQHandler(void)
 {

//   DEBUG_BREAK;

   PCNT_IntClear(PCNT0, PCNT_IF_OF);

   pcntIrqCount++;
   display_minutes += 1;

   if (display_minutes >= 60) {
   	   display_hours +=1;
      }

   if (display_minutes>=60) {
	   display_minutes=0;
   }

   if (display_hours>=1 && display_hours <3) {
	   pressed1=true;
   } else {
	   pressed1=false;
   }



   display_time();

}

void pcntInit(void)
 {


   /* Enable PCNT clock */
   CMU_ClockEnable(cmuClock_PCNT0, true);

   PCNT_Init_TypeDef pcntInit = PCNT_INIT_DEFAULT;

   /* Set up the PCNT to count RTC_PULSE_FREQUENCY pulses -> one second */

   pcntInit.mode = pcntModeOvsSingle;
   pcntInit.top = RTC_FREQUENCY/2;
   pcntInit.s1CntDir = false;
   /* The PRS channel used depends on the configuration and which pin the
   LCD inversion toggle is connected to. So use the generic define here. */
   pcntInit.s0PRS = LCD_AUTO_TOGGLE_PRS_CH;//(PCNT_PRSSel_TypeDef)LCD_AUTO_TOGGLE_PRS_CH;


   PCNT_IntEnable(PCNT0, PCNT_IF_OF);
   /* Enable PCNT interrupt every second */
   NVIC_EnableIRQ(PCNT0_IRQn);

   /* Select PRS as the input for the PCNT */
   PCNT_PRSInputEnable(PCNT0, pcntPRSInputS0, true);

   PCNT_Init(PCNT0, &pcntInit);


 }

int main(void)
{
	CHIP_Init();

	CMU_OscillatorEnable(cmuOsc_LFRCO,true,true);
	CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFRCO);

	if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) // every ms
	{
		DEBUG_BREAK;
	}

	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_TIMER0, true);
	CMU_ClockEnable(cmuClock_TIMER1, true);

	// Set up TIMER0 for timekeeping
	TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
	timerInit.prescale = timerPrescale1024;

	TIMER_IntEnable(TIMER1, TIMER_IF_OF);

	// Enable TIMER0 interrupt vector in NVIC
	NVIC_EnableIRQ(TIMER1_IRQn);

	// Set TIMER Top value
	TIMER_TopSet(TIMER1, ONE_SECOND_TIMER_COUNT);

	TIMER_Init(TIMER1, &timerInit);

	// Wait for the timer to get going
//
	while (TIMER1->CNT == 0)

	// Enable LED output
	GPIO_PinModeSet(LED_PORT, LED_PIN0, gpioModePushPull, 0);
	GPIO_PinModeSet(LED_PORT, LED_PIN1, gpioModePushPull, 0);
	GPIO_PinModeSet(LED_PORT, LED_PIN2, gpioModePushPull, 0);

	// Enable Button
	GPIO_PinModeSet(BUTTON_PORT, SET_BUTTON1_PIN, gpioModeInput,  0);
	GPIO_PinModeSet(BUTTON_PORT, SET_BUTTON2_PIN, gpioModeInput,  0);

	button1.short_press = false;
    button1.long_press = false;
    button1.pin = SET_BUTTON1_PIN;

    button2.short_press = false;
    button2.long_press = false;
    button2.pin = SET_BUTTON2_PIN;

    program_button.short_press = false;
    program_button.long_press = false;

    button_array[BUTTON1_INDEX] = &button1;
    button_array[BUTTON2_INDEX] = &button2;
    button_array[PROGRAM_BUTTON_INDEX] = &program_button;

//    setup_rtc();
	title_screen();

	// Create the object initializer for LED PWM
	TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
	timerCCInit.mode = timerCCModePWM;
	timerCCInit.cmoa = timerOutputActionToggle;

	// Configure TIMER0 CC channels
	TIMER_InitCC(TIMER0, 0, &timerCCInit);
	TIMER_InitCC(TIMER0, 1, &timerCCInit);
	TIMER_InitCC(TIMER0, 2, &timerCCInit);

	// Set up routes for PD1, PD2, PD3
//	TIMER0->ROUTE |= (TIMER_ROUTE_CC0PEN | TIMER_ROUTE_CC1PEN
//			| TIMER_ROUTE_CC2PEN | TIMER_ROUTE_LOCATION_LOC3);

	TIMER0->ROUTEPEN |= TIMER_ROUTEPEN_CC0PEN | TIMER_ROUTEPEN_CC1PEN | TIMER_ROUTEPEN_CC2PEN;
	TIMER0->ROUTELOC0 |= TIMER_ROUTELOC0_CC0LOC_LOC18 | TIMER_ROUTELOC0_CC1LOC_LOC18 | TIMER_ROUTELOC0_CC2LOC_LOC18;

	// Set Top Value
	TIMER_TopSet(TIMER0, TIMER_TOP);

	// Set the PWM duty cycle here!
	TIMER_CompareBufSet(TIMER0, 0, 0);
	TIMER_CompareBufSet(TIMER0, 1, 0);
	TIMER_CompareBufSet(TIMER0, 2, 0);

	// Create a timerInit object, based on the API default
	TIMER_Init_TypeDef timerInit2 = TIMER_INIT_DEFAULT;
	timerInit2.prescale = timerPrescale256;

	TIMER_Init(TIMER0, &timerInit2);

	// Check for properly sized constants
	if ( DELTA == 0 || RAMP_UP_TIME_MS % DELTA || RAMP_DOWN_TIME_MS % DELTA)
	{
		DEBUG_BREAK
	}


	program_timer();

    pcntInit();

	while (1)
	{


		// Sweep forward
		for (int i = 0; i < 3; i++)
		{
			start_fade(i);
			delay_ms(START_OFFSET_MS);
		}

		// This is the unusual step backward in the sweep
		start_fade(1);
		delay_ms(START_OFFSET_MS);
	}
}




