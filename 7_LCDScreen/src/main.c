#include <stdio.h>
#include <stdlib.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_timer.h"
#include "em_gpio.h"
//#include "displaypalconfig.h"
#include "display.h"
//#include "textdisplay.h"
//#include "retargettextdisplay.h"
#include "graphics.h"
//#include "displaypalconfig.h"

#define DEBUG_BREAK 						__asm__("BKPT #0");

#define RTC_FREQUENCY    (128)

//#define  ARGLE_BLARGE
//#define PAL_CLOCK_RTC


void test(void)
{
}


int RepeatCallbackRegister(void (*pFunction)(void*),
						   void* argument,
						   unsigned int frequency)
{
  /* Verify that the requested frequency is the same as the RTC frequency.*/
  if (RTC_FREQUENCY != frequency)
  {
	  DEBUG_BREAK;
	return -1;
  }

//  rtccCallback    = pFunction;
//  rtccCallbackArg = argument;

  return 0;
}




//#define PAL_RTC_CLOCK_LFRCO
//#define PAL_CLOCK_RTC

int main(void)
{

  /* Chip errata */
  CHIP_Init();

  CMU_ClockEnable(cmuClock_GPIO, true);

  char greeting[] = "";
  char greeting3[] = "Hello\n";
  char greeting2[] = "Mother Fucker\n";
//  char greeting3[] = "\n";
  char greeting4[] = "Hit PB0 to start/stop\n";
  char greeting5[] = "LEDs\n";

  int line_num = 0;

  graphInit(greeting);
  graphClear();
  graphWriteString(greeting3,line_num);
  line_num++;
  graphWriteString(greeting2,line_num);
  line_num++;
  graphWriteString(greeting4,line_num);
  line_num++;
  graphWriteString(greeting5,line_num);
//  graphPrintCenter(GLIB_Context_t *pContext, greeting4)
//  graphWriteString(greeting2);
//  graphWriteString(greeting3);
//  graphWriteString(greeting4);


  /* Infinite loop */
  while (1) {


  }
}
