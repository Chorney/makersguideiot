#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_timer.h"

#define XLAT_PORT                            gpioPortA
#define XLAT_PIN								4   // extra  - P14
#define VPRG_PORT							gpioPortA
#define VPRG_PIN								1   // extra - P11
#define BLANK_PORT							gpioPortA
#define BLANK_PIN								5   // extra - P16
#define SIN_PORT							gpioPortA
#define SIN_PIN                                 0   //US1_TX loc 1 - P9

#define GSCLK_PORT							gpioPortC
#define GSCLK_PIN                               6   //Timer 1 loc 3 - P1
#define SCLK_PORT							gpioPortD
#define SCLK_PIN                                11   //US1_RX loc 1 - P6
#define TLC5940_N 1

#define TIMER_CHANNEL						0

#define pulse(port,pin) do { \
							GPIO_PinOutSet((port),(pin)); \
							GPIO_PinOutClear((port),(pin)); \
} while(0)

uint8_t gsData[192 * TLC5940_N] = {
// MSB							LSB
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 15
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 14
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 13
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,		// Channel 12
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,		// Channel 11
0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,		// Channel 10
0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,		// Channel 9
0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,		// Channel 8
0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,		// Channel 7
0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,		// Channel 6
0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,		// Channel 5
0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 4
0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 3
0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 2
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		// Channel 1
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,		// Channel 0
};

void TLC5940_Init(void) {

	 GPIO_DriveStrengthSet(gpioPortA, gpioDriveStrengthStrongAlternateWeak);
	  GPIO_PinModeSet(SIN_PORT, SIN_PIN, gpioModePushPullAlternate , 0);
	  GPIO_PinModeSet(SCLK_PORT, SCLK_PIN, gpioModePushPullAlternate , 0);
	  GPIO_PinModeSet(XLAT_PORT, XLAT_PIN, gpioModePushPullAlternate , 0);
	  GPIO_PinModeSet(VPRG_PORT, VPRG_PIN, gpioModePushPullAlternate, 0);
	  GPIO_PinModeSet(BLANK_PORT, BLANK_PIN, gpioModePushPullAlternate, 0);
	  GPIO_PinModeSet(GSCLK_PORT, GSCLK_PIN, gpioModePushPullAlternate, 0);

	  GPIO_PinOutClear(SIN_PORT, SIN_PIN);
	GPIO_PinOutClear(SCLK_PORT, SCLK_PIN);
	GPIO_PinOutClear(XLAT_PORT, XLAT_PIN);
	GPIO_PinOutSet(VPRG_PORT, VPRG_PIN);
	GPIO_PinOutSet(BLANK_PORT, BLANK_PIN);
	GPIO_PinOutClear(GSCLK_PORT, GSCLK_PIN);

}

void TLC5940_Timer1_Init(void) {

//	 Create a timerInit object, based on the API default
//	  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
//	  timerCCInit.mode = timerCCModeCompare;
//	  timerCCInit.cmoa = timerOutputActionToggle;
//	  timerCCInit.eventCtrl = timerEventEveryEdge;
	  //timerInit.prescale = timerPrescale512;
//
//	  TIMER_InitCC(TIMER1,TIMER_CHANNEL,&timerCCInit);
//
//	  TIMER1->ROUTEPEN |= TIMER_ROUTEPEN_CC0PEN;
//	  TIMER1->ROUTELOC0 |= TIMER_ROUTELOC0_CC0LOC_LOC11;


	  // Create a timerInit object, based on the API default
	TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
	timerInit.clkSel = timerClkSelHFPerClk;
	timerInit.prescale = timerPrescale1024;
	timerInit.debugRun = true;

	TIMER_IntEnable(TIMER1, TIMER_IF_OF);

	// Set TIMER Top value
    TIMER_TopSet(TIMER1,3);	//Timer value

	TIMER_Init(TIMER1, &timerInit);

}


void TLC5940_SetGS_And_GS_PWM(void) {
	uint8_t firstCycleFlag = 0;

	if (GPIO_PinOutGet(VPRG_PORT, VPRG_PIN)) {
		GPIO_PinOutClear(VPRG_PORT, VPRG_PIN);
		firstCycleFlag = 1;
	}

	uint16_t GSCLK_Counter = 0;
	uint8_t Data_Counter = 0;

	GPIO_PinOutClear(BLANK_PORT, BLANK_PIN);
	while (1) {
		if (GSCLK_Counter > 4095) {
			GPIO_PinOutSet(BLANK_PORT, BLANK_PIN);
			pulse(XLAT_PORT, XLAT_PIN);
			if (firstCycleFlag) {
				pulse(SCLK_PORT, SCLK_PIN);
				firstCycleFlag = 0;
			}
			break;
		} else {
			if (!(Data_Counter > TLC5940_N * 192 - 1)) {
				if (gsData[Data_Counter])
					GPIO_PinOutSet(SIN_PORT, SIN_PIN);
				else
					GPIO_PinOutClear(SIN_PORT, SIN_PIN);
				pulse(SCLK_PORT, SCLK_PIN);
				Data_Counter++;
			}
		}
		pulse(GSCLK_PORT, GSCLK_PIN);
		GSCLK_Counter++;
	}
}



int main(void)
{
  /* Chip errata */
  CHIP_Init();

//  CMU_HFRCOFreqSet(cmuHFRCOFreq_1M0Hz);
//  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
  CMU_ClockDivSet(cmuClock_HF,cmuClkDiv_8);
//////
  CMU->HFEXPPRESC = (CMU->HFEXPPRESC &~_CMU_HFEXPPRESC_MASK) | (1 << 8 );
  CMU->HFPERPRESC = (CMU->HFPERPRESC &~_CMU_HFPERPRESC_MASK) | (1 << 8 );
  CMU->CTRL =(CMU->CTRL &~_CMU_CTRL_CLKOUTSEL0_MASK)| CMU_CTRL_CLKOUTSEL0_HFEXPCLK;
//
  CMU->ROUTELOC0 = CMU_ROUTELOC0_CLKOUT0LOC_LOC2;
  CMU->ROUTEPEN = CMU_ROUTEPEN_CLKOUT0PEN;


  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_TIMER1, true);

//  GPIO_PinModeSet(GSCLK_PORT,GSCLK_PIN, gpioModePushPull ,0);

  TLC5940_Init();
  TLC5940_Timer1_Init();

  /* Infinite loop */

  // Enable TIMER0 interrupt vector in NVIC
  NVIC_EnableIRQ(TIMER1_IRQn);

  // Wait for the timer to get going
//  while (TIMER1->CNT == 0);
  // Code to use timer goes here

  while (1) {
//	  TLC5940_SetGS_And_GS_PWM();
  }
}

void TIMER1_IRQHandler(void)
{
	TIMER_IntClear(TIMER1, TIMER_IF_OF);
	uint8_t firstCycleFlag = 0;
	static uint8_t xlatNeedsPulse = 0;
	GPIO_PinOutSet(BLANK_PORT, BLANK_PIN);

	if (GPIO_PinOutGet(VPRG_PORT, VPRG_PIN)) {
		GPIO_PinOutClear(VPRG_PORT, VPRG_PIN);
		firstCycleFlag = 1;
	}
	if (xlatNeedsPulse) {
		pulse(XLAT_PORT, XLAT_PIN);
		xlatNeedsPulse = 0;
	}
	if (firstCycleFlag)
		pulse(SCLK_PORT, SCLK_PIN);
	GPIO_PinOutClear(BLANK_PORT, BLANK_PIN);

	// Below this we have 4096 cycles to shift in the data for the next cycle
	uint8_t Data_Counter = 0;
	while (1) {
		if (!(Data_Counter > TLC5940_N * 192 - 1)) {
			if (gsData[Data_Counter])
				GPIO_PinOutSet(SIN_PORT, SIN_PIN);
			else
				GPIO_PinOutClear(SIN_PORT, SIN_PIN);
			pulse(SCLK_PORT, SCLK_PIN);
			Data_Counter++;
		} else {
			xlatNeedsPulse = 1;
			break;
		}
	}
//	TIMER_IntClear(TIMER1, TIMER_IF_OF);
//
}
