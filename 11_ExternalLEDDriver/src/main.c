#include "em_device.h"
#include "em_chip.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_usart.h"
#include "em_system.h"
#include "em_emu.h"
#include "em_assert.h"
#include "utilities.h"

#define CONTROL_PORT                            gpioPortA
#define XLAT_PIN								4   // extra  - P14
#define VPRG_PIN								1   // extra - P11
#define BLANK_PIN								5   // extra - P16
#define SIN_PIN                                 0   //US1_TX loc 1 - P9
#define SCLK_PIN                                2   //US1_RX loc 1 - P0
#define CONTROL_PORT2							gpioPortD
#define GSCLK_PIN                               10   //Timer 1 loc 3 - P4

#define MAX_CHANNELS          16
#define DC_BIT_LEN            6
#define GS_BIT_LEN            12

#define MAX_GRAYSCALE               4095  // 4096 - 1
#define MAX_DOT_CORRECTION          63    // 64 - 1
#define DOT_CORRECTION_MODE         0
#define GRAYSCALE_MODE              1
#define MAX_STREAM_BIT_LEN			192
#define DC_STREAM_BIT_LEN	96	// DC stands for Dot Correction

#define DEBUG_BREAK __asm__("BKPT #0");

// Structs for LED driver
typedef struct LED_bit_fields
{                             // Number after colon is field width
      uint8_t dot_correction : DC_BIT_LEN;
      uint16_t grayscale      : GS_BIT_LEN;
} LED_data_struct;

typedef struct LED_stream
{
      LED_data_struct channel[MAX_CHANNELS];
      bool mode;
} LED_stream_struct;

LED_stream_struct stream;

// Pack dot correction byte for the serial stream
uint8_t pack_dc_byte(uint8_t byte_position)
{
      const uint8_t starting_bit = byte_position * 8;
      const uint8_t dc_start_position = starting_bit / DC_BIT_LEN;
      const uint8_t dc_end_position = dc_start_position + 1;

      const uint8_t dc_lo = stream.channel[dc_start_position].dot_correction;
      const uint8_t dc_hi = stream.channel[dc_end_position].dot_correction;

      const uint8_t slot = byte_position % 3;

      switch (slot)
      {
      case 0:
            return dc_lo | (dc_hi << 6);
            break;
      case 1:
            return (dc_lo >> 2) | (dc_hi << 4);
            break;
      case 2:
            return (dc_lo >> 4) | (dc_hi << 2);
            break;
      }
      return 0;  // Will never occur, but makes compiler happy
}

// Pack grayscale byte for the serial stream
uint8_t pack_gs_byte(uint8_t byte_position)
{
      const uint8_t start_channel = (uint32_t) (byte_position * 8) / GS_BIT_LEN;

      const uint8_t slot = byte_position % 3;

      uint16_t hi, lo;

      switch (slot)
      {
      case 0:
            return stream.channel[start_channel].grayscale;
            break;
      case 1:
            hi = ((uint16_t) stream.channel[start_channel+1].grayscale) << 4;
            lo = ((uint16_t) stream.channel[start_channel].grayscale) >> 4;
            return hi | lo;
            break;
      case 2:
            return stream.channel[start_channel].grayscale >> 4;
            break;
      }
      return 0;  // Will never occur, but makes the compiler happy
}

void write_serial_stream()
{
      uint8_t stream_buffer[MAX_STREAM_BIT_LEN/8];
      int length;

      if (stream.mode == DOT_CORRECTION_MODE)
      {
            length = DC_STREAM_BIT_LEN / 8;
            for (int i=0; i<length; i++)
            {
                  stream_buffer[i] = pack_dc_byte(i);
            }
      }
      else
      {
            length = MAX_STREAM_BIT_LEN/8;
            for (int i=0; i<length; i++)
            {
                  stream_buffer[i] = pack_gs_byte(i);
            }
      }

      // Set the VPRG pin
      if (stream.mode == DOT_CORRECTION_MODE)
      {
            GPIO_PinOutSet(CONTROL_PORT, VPRG_PIN);
      }
      else
      {
            GPIO_PinOutClear(CONTROL_PORT, VPRG_PIN);
      }

      // Now write the stream, MSByte first

      // Latch the data
            GPIO_PinOutSet(CONTROL_PORT, XLAT_PIN);
            for (volatile int i=0; i < 1000; i++)
                        ;
            GPIO_PinOutClear(CONTROL_PORT, XLAT_PIN);

      for (int i=length-1; i>=0; i--)
      {
            USART_Tx(USART1, stream_buffer[i]);
      }

      while (!(USART1->STATUS & USART_STATUS_TXC))
            ;

      for (volatile int i=0; i < 1000; i++)
            ;

      // Latch the data
      GPIO_PinOutSet(CONTROL_PORT, XLAT_PIN);
      for (volatile int i=0; i < 1000; i++)
                  ;
      GPIO_PinOutClear(CONTROL_PORT, XLAT_PIN);
}

int main(void)
{
  /* Chip errata */
  CHIP_Init();

  // Turn on the peripheral clocks
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_USART1, true);

  // Configure the USART peripheral
  USART_InitSync_TypeDef init = USART_INITSYNC_DEFAULT;
  init.baudrate = 115200;
  init.msbf = true;       // MSB first, per the TLC5940 spec
  init.master = 1;
  USART_InitSync(USART1, &init);

  // Route the peripheral to the GPIO block
  /* Set up CLK pin */
  	USART1->ROUTELOC0 = (USART1->ROUTELOC0 & (~_USART_ROUTELOC0_CLKLOC_MASK))
  			| USART_ROUTELOC0_CLKLOC_LOC0;
  	USART1->ROUTEPEN = USART1->ROUTEPEN | USART_ROUTEPEN_CLKPEN;

  	/* Set up TX pin */
	USART1->ROUTELOC0 = (USART1->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK))
			| USART_ROUTELOC0_TXLOC_LOC0;
	USART1->ROUTEPEN = USART1->ROUTEPEN | USART_ROUTEPEN_TXPEN;

  // Configure both the USART and control pins in GPIO
    GPIO_DriveStrengthSet(gpioPortA, gpioDriveStrengthStrongAlternateWeak);
    GPIO_PinModeSet(CONTROL_PORT, SIN_PIN, gpioModePushPullAlternate , 1);
    GPIO_PinModeSet(CONTROL_PORT, SCLK_PIN, gpioModePushPullAlternate , 1);
    GPIO_PinModeSet(CONTROL_PORT, XLAT_PIN, gpioModePushPullAlternate , 1);
    GPIO_PinModeSet(CONTROL_PORT, VPRG_PIN, gpioModePushPullAlternate, 1);
    GPIO_PinModeSet(CONTROL_PORT, BLANK_PIN, gpioModePushPullAlternate, 1);
    GPIO_PinModeSet(CONTROL_PORT2, GSCLK_PIN, gpioModePushPullAlternate, 1);

//    stream.mode = DOT_CORRECTION_MODE;      // Start with DC data
      for (int i=0; i<MAX_CHANNELS; i++)
      {
            stream.channel[i].dot_correction = 0b100001;        // Test pattern
            stream.channel[i].grayscale = 0b100000000001;       // Test pattern
      }

      // Write the DC Data
//      write_serial_stream();

      // Send the GS data
      stream.mode = GRAYSCALE_MODE;                 // Now write the GS data
      write_serial_stream();


    int count = 0;
	  while (1)
	  {
			if (count %2)
			{
				  GPIO_PinOutSet(CONTROL_PORT, GSCLK_PIN);
			}
			else
			{
				  GPIO_PinOutClear(CONTROL_PORT, GSCLK_PIN);
			}
			count++;

			if (count > 4096)
			{
				  GPIO_PinOutSet(CONTROL_PORT, BLANK_PIN);
				  for (volatile int i=0; i < 10; i++)
							  ;
				  GPIO_PinOutClear(CONTROL_PORT, BLANK_PIN);
				  count = 0;
			}

			for (volatile int i=0; i < 10; i++)
				  ;
	  }
}
